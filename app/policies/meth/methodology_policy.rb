module Meth
  class MethodologyPolicy < Meth::MethPolicy
    attr_reader :user, :record

    def initialize(user, record)
      @user = user
      @record = record
    end

    def edit_meth?
      edit?
    end

    # def index?
    #   show?
    # end

    # def show?
    #   user.has_role?(:admin) || user.has_role?(:super_admin)
    # end

    # def create?
    #   user.has_role?(:super_admin)
    # end

    # def new?
    #   create?
    # end

    # def update?
    #   create?
    # end

    def edit?
      user.has_role?(:admin) || user.has_role?(:super_admin)
    end

    def destroy?
      return false if @record.locked?

      super
    end

    def copy?
      create?
    end
  end
end
