class CreateLabelsQqoqcpCls < ActiveRecord::Migration[6.0]
  def change
    create_table :labels_qqoqcp_cls do |t|
      t.references :qqoqcp_config, null: false, foreign_key: { to_table: :labels_qqoqcp_configs }
      t.references :qqoqcp_line, null: false, foreign_key: { to_table: :labels_qqoqcp_lines }
      t.integer :pos, default: 0
      t.boolean :usable, default: true

      # t.timestamps
    end
  end
end
