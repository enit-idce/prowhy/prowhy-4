class AddNameToStepTool < ActiveRecord::Migration[6.0]
  def change
    add_column :meth_step_tools, :name, :string, after: :pos
  end
end
