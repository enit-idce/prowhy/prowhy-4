module Tools
  class TaskPolicy < ToolsPolicy
    attr_reader :user, :record, :rca

    def index_js?
      index?
    end

    def index_global?
      user.has_any_role?(:super_admin, :admin, :manager, :user)
    end

    def index_user?
      true
    end

    # TODO : check si user est responsable de l'action.
    def update_advance?
      # rca_author == user.person
      return user.has_role?(:admin) || user.has_role?(:super_admin) || record.responsibles.include?(user.person) || record.rca.rca_responsible?(user.person)
      # return record.responsibles.include?(user.person) || record.rca.rca_responsible?(user.person)
    end

    def update?
      update_advance? || super
    end

    def edit?
      update?
    end

    def create?
      user.has_any_role?(:admin, :super_admin, :manager) || user.user_authorized?(rca, 'write')
    end

    def new?
      create?
    end

    # TODO : CHECK si User est auteur ou pilote du RCA
    def send_emails?
      # puts 'CHECK SEND EMAILS IN TASK=========================================>'.red
      return user.has_role?(:admin) || user.has_role?(:super_admin) || user.user_authorized?(rca, 'email')
    end

    def check_list?
      index?
    end

    def check_list_valid?
      create?
    end

    def reschedule?
      update?
    end
  end
end
