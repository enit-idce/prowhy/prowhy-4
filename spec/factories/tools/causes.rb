FactoryBot.define do
  factory :cause, class: Tools::Cause do
    # name { 'Cause 1' }
    sequence(:name) { |n| "Cause #{n}" }
    tool_reference { 1 }
    association :rca, factory: :rca
  end
end
