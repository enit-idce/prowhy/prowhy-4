FactoryBot.define do
  factory :meth_step_tool, class: Meth::StepTool do
    name { 'StepTool 1' }
    association :step, factory: :meth_step
    association :tool, factory: :meth_tool
  end
end
