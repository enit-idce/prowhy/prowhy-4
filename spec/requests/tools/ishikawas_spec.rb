require 'rails_helper'

RSpec.describe '/tools/ishikawas', type: :request do
  let(:valid_attributes) { {rca_id: rca.id, tool_reference: 1, tool_num: 1} }
  let(:invalid_attributes) { {rca_id: nil, tool_reference: 1, tool_num: 1} }

  let(:rca) { FactoryBot.create(:rca, methodology: methodology, title: 'RCA1') }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:user) { FactoryBot.create(:user) }
  let(:tools_cause) { FactoryBot.create(:cause, rca: rca) }

  let(:ishikawa) { FactoryBot.create(:ishikawa, rca: rca) }
  let(:cause_type) { FactoryBot.create(:cause_type) }

  context 'From logged user' do
    before :each do
      # binding.pry
      user.usersession[:active_step_tool] = methodology.steps.first.step_tools.first.id # Meth::StepTool.first.id
      user.save!

      sign_in user
    end

    describe 'GET /index' do
      context 'without rca defined' do
        it 'renders a redirection response' do
          get '/tools/ishikawas'
          # redirection car rca non défini
          expect(response).to have_http_status(:redirect)
          expect(response).to redirect_to(rcas_path)
        end
      end

      context 'with rca and step_tool defined' do
        context 'without ishikawa id defined' do
          it 'renders a redirection response' do
            get '/tools/ishikawas', params: {rca: rca.id}
            # redirection car rca non défini
            expect(response).to have_http_status(:redirect)
            expect(response).to redirect_to(edit_rca_path(rca))
          end
        end
        context 'with cause tree head id defined' do
          it 'renders a successful response' do
            get '/tools/ishikawas', params: {id: ishikawa.id, rca: rca.id}
            expect(response).to be_successful
          end
        end
      end
    end

    describe 'GET /ishikawa_js_tree_data' do
      context 'with ishikawa' do
        it 'return ishikawa datas in Json format' do
          cause_type
          ishikawa.associate_labels(nil)
          Tools::CauseLink.create!(cause: tools_cause, cause_tool: ishikawa)
          cause2 = Tools::Cause.create!(rca: rca, parent: tools_cause, tool_reference: ishikawa.tool_reference, name: 'cause2')
          headers = { 'ACCEPT' => 'application/json' }
          get tools_ishikawa_js_tree_data_path, params: {id: ishikawa.id}, headers: headers
          expect(response.content_type).to eq('application/json; charset=utf-8')
          # ici ce serait d'ajouter un test pour vérifier que le retour json correspond bien au schémas (avec le bon contenu)
          response_at = {'options': {'causes': {'show_only_root': false, 'show_only_level1': false,
                                                'status': {'in_progress': true, 'validated': true, 'invalidated': true}},
                                     'zoom': 1.2, 'langue': I18n.locale, 'statuses': {'in_progress': 1, 'validated': 1, 'invalidated': 1},
                                     'effect': ['RCA1']},
                         'ishikawa': {'id': ishikawa.id, 'tool_ref': 1, 'tool_num': 1, 'rca': rca.id},
                         'branchs': {'list': [cause_type.name], 'list_id': [cause_type.id]},
                         'causes': {'list': {'0': [cause2.name], "#{cause_type.id}": []},
                                    'list_id': {'0': [cause2.id], "#{cause_type.id}": []},
                                    'list_v': {'0': [cause2.status_before_type_cast], "#{cause_type.id}": []}}}
          expect(JSON.parse(response.body)).to eq JSON.parse(response_at.to_json)
        end
      end

      context 'without ishikawa defined' do
        it 'return no datas in Json format' do
          headers = { 'ACCEPT' => 'application/json' }
          get tools_ishikawa_js_tree_data_path, params: {}, headers: headers
          expect(response.content_type).to eq('application/json; charset=utf-8')
          expect(response.body).to eq '{}'
        end
      end
    end

    describe 'POST /ishikawa_drag_cause' do
      context 'with ishikawa and cause' do
        it 'change cause class for selected cause' do
          cause_type
          ishikawa.associate_labels(nil)
          Tools::CauseLink.create!(cause: tools_cause, cause_tool: ishikawa)
          cause2 = Tools::Cause.create!(rca: rca, parent: tools_cause, tool_reference: ishikawa.tool_reference, name: 'cause2')

          post tools_ishikawa_drag_cause_path, params: {id: ishikawa.id, drag_id: cause2.id, drop_id: cause_type.id}

          expect(response).to have_http_status(:redirect)
          expect(response).to redirect_to(tools_ishikawas_path(id: ishikawa.id, rca: rca))
          expect(cause2.labels_cause_types).to eq [cause_type]
        end
      end

      # context 'without ishikawa defined' do
      #   it 'return no datas in Json format' do
      #     headers = { 'ACCEPT' => 'application/json' }
      #     get tools_ishikawa_js_tree_data_path, params: {}, headers: headers
      #     expect(response.content_type).to eq('application/json; charset=utf-8')
      #     expect(response.body).to eq '{}'
      #   end
      # end
    end

    describe 'GET /ishikawa_reset_branchs' do
      context 'with ishikawa and cause' do
        it 'reset Ishikawa branchs to step_tool options' do
          cause_type
          Labels::CauseType.create!([
                                      {name_fr: 'Type1'},
                                      {name_fr: 'Type2'}
                                    ])
          ishikawa.associate_labels(nil)
          step_tool = FactoryBot.create(:meth_step_tool,
                                        step: methodology.steps.first,
                                        tool_options: {labels: {'1': Labels::CauseType.where(name_fr: 'Type1').first.id}})
          get tools_ishikawa_reset_branchs_path, params: {id: ishikawa.id, step_tool: step_tool.reload.id}

          expect(response).to have_http_status(:redirect)
          expect(response).to redirect_to(tools_ishikawas_path(id: ishikawa.id, rca: rca))
          expect(ishikawa.tools_ishi_branchs.size).to eq 1
        end
      end
    end
  end
end
