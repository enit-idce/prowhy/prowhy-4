class RemoveStepLinksFromMethodology < ActiveRecord::Migration[6.0]
  def change
    remove_reference :meth_methodologies, :step_info, index: true, foreign_key: { to_table: :meth_steps }
    remove_reference :meth_methodologies, :step_trstools, index: true, foreign_key: { to_table: :meth_steps }
  end
end
