# spec/models/concept_spec.rb
# binding.pry
require 'rails_helper'

shared_examples_for Meth::Positions::MoveService, type: 'service' do # |params|
  # subject(:obj) do
  #   obj = if defined?(described_obj)
  #           described_obj
  #         else
  #           described_class.new
  #         end
  #   obj.save(validate: false)
  #   obj
  # end

  describe 'Moves position Up and Down' do
    it 'moves object up' do
      # obj_list = params[:obj_class].constantize.all
      Meth::Positions::MoveService.new(described_obj, @obj_list).call('up')
      expect(described_obj.reload.pos).to eq 2
      expect(@obj_list.order(:pos).map(&:pos)).to eq [1, 2, 3]

      Meth::Positions::MoveService.new(described_obj, @obj_list).call('up')
      expect(described_obj.reload.pos).to eq 3
      expect(@obj_list.order(:pos).map(&:pos)).to eq [1, 2, 3]

      Meth::Positions::MoveService.new(described_obj, @obj_list).call('up')
      expect(described_obj.reload.pos).to eq 3
      expect(@obj_list.order(:pos).map(&:pos)).to eq [1, 2, 3]
    end

    it 'moves object down' do
      Meth::Positions::MoveService.new(described_obj, @obj_list).call('up')
      Meth::Positions::MoveService.new(described_obj.reload, @obj_list).call('up')

      Meth::Positions::MoveService.new(described_obj.reload, @obj_list).call('down')
      expect(described_obj.reload.pos).to eq 2
      expect(@obj_list.order(:pos).map(&:pos)).to eq [1, 2, 3]

      Meth::Positions::MoveService.new(described_obj, @obj_list).call('down')
      expect(described_obj.reload.pos).to eq 1
      expect(@obj_list.order(:pos).map(&:pos)).to eq [1, 2, 3]

      Meth::Positions::MoveService.new(described_obj, @obj_list).call('down')
      # expect(described_obj.reload.pos).to eq 1
      expect(@obj_list.order(:pos).map(&:pos)).to eq [1, 2, 3]
    end
  end

  describe 'Reinit positions' do
    it 'reinit all positions' do
      @obj_list.each do |obj|
        obj.pos = 1
        obj.save
      end
      Meth::Positions::MoveService.new(described_obj, @obj_list.reload).call('reinit')

      expect(@obj_list.reload.map(&:pos)).to eq [1, 2, 3]
    end
  end
end

# Now list every model using Meth::Positions::MoveService to test them
# All describe must have :
# * described_obj => the object to move up and down
# * @obj_list => The list of objects

describe Meth::ListContent do
  it_behaves_like Meth::Positions::MoveService do # { obj_class: 'Meth::ListContent' } do
    subject(:described_obj) do
      Meth::ListContent.create!(custom_field_desc: custom_field_desc, name: 'Tarbes Ouest', pos: 1)
    end

    let(:custom_field_desc) { Meth::CustomFieldDesc.new(custom_field_type: cf_type, internal_name: 'custom_1', name: 'Custom Field Site') }
    let(:cf_type) { Meth::CustomFieldType.create!(field_type: 'string', field_size: nil, field_internal: 'select', field_name: 'Liste') }

    before :each do
      described_obj
      Meth::ListContent.create!([{custom_field_desc: custom_field_desc, name: 'Tarbes Nord', pos: 2},
                                 {custom_field_desc: custom_field_desc, name: 'Lourdes', pos: 3}])
      @obj_list = Meth::ListContent.all
    end
  end
end

describe Meth::Step do
  it_behaves_like Meth::Positions::MoveService do # { obj_class: 'Meth::ListContent' } do
    subject(:described_obj) do
      Meth::Step.create!(name: 'Step 1', methodology: methodology, pos: 1, step_role: step_role)
    end

    let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
    let(:step_role) { FactoryBot.create(:meth_step_role) }

    before :each do
      described_obj
      Meth::Step.create!([{name: 'Step 2', methodology: methodology, pos: 2, step_role: step_role},
                          {name: 'Step 3', methodology: methodology, pos: 3, step_role: step_role}])
      @obj_list = Meth::Step.all
    end
  end
end

describe Meth::ToolCustomField do
  it_behaves_like Meth::Positions::MoveService do # { obj_class: 'Meth::ListContent' } do
    subject(:described_obj) do
      Meth::ToolCustomField.create!(step_tool: step_tool, custom_field_desc: cf_desc_1, pos: 1)
    end

    let(:cf_desc_1) { Meth::CustomFieldDesc.create!(custom_field_type: cf_type, internal_name: 'custom_1', name: 'Custom Field 1') }
    let(:cf_desc_2) { Meth::CustomFieldDesc.create!(custom_field_type: cf_type, internal_name: 'custom_2', name: 'Custom Field 2') }
    let(:cf_desc_3) { Meth::CustomFieldDesc.create!(custom_field_type: cf_type, internal_name: 'custom_3', name: 'Custom Field 3') }
    let(:cf_type) { Meth::CustomFieldType.create!(field_type: 'string', field_size: nil, field_internal: 'select', field_name: 'Liste') }
    let(:step) { FactoryBot.create(:meth_step, name: 'Step 1', methodology: methodology) }
    let(:tool) { FactoryBot.create(:meth_tool, name: 'NC Team', tool_name: 'Team', activity: activity) }
    let(:activity) { FactoryBot.create(:meth_activity, name: 'Team') }
    let(:methodology) { FactoryBot.create(:methodology, name: 'Method 1') }
    let(:step_tool) { FactoryBot.create(:meth_step_tool, step: step, tool: tool) }

    before :each do
      described_obj
      Meth::ToolCustomField.create!([{step_tool: step_tool, custom_field_desc: cf_desc_1, pos: 2},
                                     {step_tool: step_tool, custom_field_desc: cf_desc_1, pos: 3}])
      @obj_list = Meth::ToolCustomField.all
    end
  end
end
