require_dependency 'tools'

module Tools
  class IndicatorMeasure < ApplicationRecord
    belongs_to :indicator
  end
end
