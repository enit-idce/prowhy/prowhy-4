module Tools
  class TaskPlansController < ApplicationController
    include ProcessConcern
    before_action :init_rca, only: [:index_local]

    # GET /tools/tasks
    # GET /tools/tasks.json
    def index_local
      if @rca.nil? || current_user.get_session(:active_step_tool).nil?
        redirect_to rcas_path
      else
        @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))

        # options_for_search = @step_tool.tool.tool_options.merge(tool_reference: @step_tool.tool_reference,
        #                                                         tool_num: @step_tool.tool_num,
        #                                                         ancestry: nil)
        options_for_search = @step_tool.tool_options.merge(tool_reference: @step_tool.tool_reference,
                                                           tool_num: @step_tool.tool_num,
                                                           ancestry: nil)

        @tools_tasks = @rca.tools_tasks.where(options_for_search)

        render :index
      end
    end
  end
end
