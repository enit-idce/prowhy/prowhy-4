json.extract! tool, :id, :name, :activity_id, :tool_name, :created_at, :updated_at
json.url tool_url(tool, format: :json)
