class PeopleController < ApplicationController
  include AdminConcern
  before_action :set_person, only: [:show, :edit, :update, :destroy]

  # GET /people
  # GET /people.json
  def index
    @people = Person.all
    authorize Person
  end

  # # GET /people/1
  # # GET /people/1.json
  # def show
  # end

  # GET /people/new
  def new
    @person = Person.new
    authorize @person
  end

  # GET /people/1/edit
  def edit
    if params[:from] == 'user'
      render :edit, layout: 'user_page', locals: {from: 'user'}
    else
      render :edit, locals: {from: 'admin'}
    end
  end

  # POST /people
  # POST /people.json
  def create
    @person = Person.new(person_params)
    authorize @person

    respond_to do |format|
      if @person.save
        format.html { redirect_to people_url, notice: t('.message_succes', person: Person.model_name.human) }
        format.json { render :show, status: :created, location: @person }
      else
        format.html { render :new }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    respond_to do |format|
      if @person.update(person_params)
        format.html do
          if params[:from] == 'user'
            redirect_to edit_person_url(@person, from: 'user'), notice: t('.message_succes', person: Person.model_name.human)
          else
            redirect_to people_url, notice: t('.message_succes', person: Person.model_name.human)
          end
        end
        format.json { render :show, status: :ok, location: @person }
      else
        format.html { render :edit }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    # Si la personne est enregistrée dans des champs de recherche sur la page d'accueil => modifier le champs usersearch
    User.all.each do |user|
      next unless user&.usersearch&.[]('rca_search')&.[]('person_id') == @person.id.to_s

      user.usersearch['rca_search'].delete('person_id')
      user.usersearch['rca_search'].delete('person_role')
      user.save!
    end
    # ok pour destroy

    if @person.destroy
      respond_to do |format|
        format.html { redirect_to people_url, notice: t('.message_succes', person: Person.model_name.human) }
        format.json { head :no_content }
      end
    else
      redirect_to people_url, flash: { error: "Person #{@person.name} is included in RCA teams, can not be deleted. You can delete associated user in User's page." }
    end
  end

  # GET / JS / Choose person helper
  def person_choose
    authorize Person
    @url_submit = params[:url_submit]
    render 'person_choose', locals: {person_id: params[:person_id], c_name: params[:c_name]}
  end

  def person_choose_valid
    authorize Person
    person = params[:list][:person] ? Person.find(params[:list][:person]) : nil
    person_text = person ? person.name : ''
    person_id = person ? person.id : 0
    c_name = params[:c_name]

    respond_to do |format|
      format.js { render 'person_choose_valid', locals: {person_id: person_id, person_text: person_text, c_name: c_name} }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_person
    @person = Person.find(params[:id])
    authorize @person
  end

  # Only allow a list of trusted parameters through.
  def person_params
    params.require(:person).permit(:firstname, :lastname, :email, :adress, :phone, :moreinfo)
  end
end
