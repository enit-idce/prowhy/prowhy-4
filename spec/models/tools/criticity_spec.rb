require 'rails_helper'

RSpec.describe Tools::Criticity, type: :model do
  subject(:criticity) do
    Tools::Criticity.create!(rca: rca, tool_reference: 1, labels_config: config1)
  end

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }

  let(:config1) { FactoryBot.create(:labels_config) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to labels_config' do
        assc = Tools::Criticity.reflect_on_association(:labels_config)
        expect(assc.macro).to eq :belongs_to
      end
      it 'has many criticity_values' do
        assc = Tools::Criticity.reflect_on_association(:criticity_values)
        expect(assc.macro).to eq :has_many
      end
    end
  end

  describe 'Create and search model : tool class functions' do
    let(:config2) { FactoryBot.create(:labels_config) }

    before :each do
      criticity.save!
      options = { rca: rca, tool_reference: 2 }
      @criticity_created = Tools::Criticity.create_default!(options, nil)
    end

    it 'create_default' do
      expect(Tools::Criticity.create_default!({}, nil)).to eq []
    end

    it 'search_default' do
      step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1)
      criticity_founded = Tools::Criticity.search_default(rca, 'tools_criticities', step_tool).first

      expect(criticity_founded.labels_config.name).to eq config1.name
      expect(criticity_founded.tool_reference).to eq 1

      step_tool.tool_reference = 2
      criticity_founded = Tools::Cause.search_default(rca, 'tools_causes', step_tool)

      expect(criticity_founded).to eq nil
    end
  end

  describe 'create_criticity_values' do
    before :each do
      @cr1 = FactoryBot.create(:labels_criterium, config_id: config1.id)
      @cr2 = FactoryBot.create(:labels_criterium, config_id: config1.id)
      @cr3 = FactoryBot.create(:labels_criterium, config_id: config1.id)

      criticity.save!
    end

    it 'creates a criticity value for each criterium of Tool::Criticity configuration' do
      criticity.create_criticity_values

      expect(criticity.criticity_values.count).to eq 3
      expect(criticity.criticity_values.map(&:labels_criterium_id)).to contain_exactly(@cr1.id, @cr2.id, @cr3.id)
    end
  end
end
