FactoryBot.define do
  factory :meth_custom_field_type, class: Meth::CustomFieldType do
    field_type { 'string' }
    field_size { nil }
    sequence(:field_internal) { |n| 'text_' + n.to_s }
    sequence(:field_name) { |n| 'Texte_' + n.to_s }

    trait :type_rich_text do
      field_type { 'text' }
      field_internal { 'rich_text' }
    end

    trait :type_tag do
      field_type { 'string' }
      field_internal { 'tag' }
    end

    trait :type_select do
      field_type { 'integer' }
      field_internal { 'select' }
    end

    trait :type_tree do
      field_type { 'integer' }
      field_internal { 'tree' }
    end
  end
end
