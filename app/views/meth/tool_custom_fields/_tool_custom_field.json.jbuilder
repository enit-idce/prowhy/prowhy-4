json.extract! tool_custom_field, :id, :step_tool_id, :custom_field_desc_id, :obligatory, :created_at, :updated_at
json.url meth_tool_custom_field_url(tool_custom_field, format: :json)
