module Tools
  class GenericPolicy < ToolsPolicy
    attr_reader :user, :record, :rca

    def download_last_doc?
      show?
    end
  end
end
