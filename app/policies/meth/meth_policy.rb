module Meth
  class MethPolicy < ApplicationPolicy
    attr_reader :user, :record

    def initialize(user, record)
      @user = user
      @record = record
    end

    def index?
      show?
    end

    def show?
      user.has_role?(:admin) || user.has_role?(:super_admin)
    end

    def create?
      !record.locked? && user.has_role?(:super_admin)
    end

    def new?
      user.has_role?(:super_admin)
    end

    def update?
      create?
    end

    def edit?
      new? # create?
    end

    def destroy?
      create?
    end
  end
end
