require 'rails_helper'

RSpec.describe Labels::QqoqcpConfig, type: :model do
  subject(:qqoqcp_config) do
    Labels::QqoqcpConfig.create!(name: 'QQOQCP')
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has many qqoqcl cl' do
        assc = Labels::QqoqcpConfig.reflect_on_association(:qqoqcp_cls)
        expect(assc.macro).to eq :has_many
      end
      it 'has many qqoqcl lines' do
        assc = Labels::QqoqcpConfig.reflect_on_association(:qqoqcp_lines)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools_qqoqcps' do
        assc = Labels::QqoqcpConfig.reflect_on_association(:tools_qqoqcps)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        qqoqcp_config.name = ''
        expect(qqoqcp_config).to_not be_valid
      end

      it 'is valid with name' do
        qqoqcp_config.name = 'QQOQCP'
        expect(qqoqcp_config).to be_valid
      end
    end
  end

  describe '#add_line' do
    before :each do
      @line1 = Labels::QqoqcpLine.create!(internal_name: 'who', name: 'QUI ?')
      @line2 = Labels::QqoqcpLine.create!(internal_name: 'what', name: 'QUOI ?')
    end
    context 'line is not already in qqoqcp.' do
      it 'adds a line to qqoqcp configuration.' do
        qqoqcp_config.add_line(@line1)
        expect(qqoqcp_config.qqoqcp_lines.size).to eq 1
        expect(qqoqcp_config.qqoqcp_cls.last.pos).to eq 1

        qqoqcp_config.add_line(@line2)
        expect(qqoqcp_config.qqoqcp_lines.size).to eq 2
        expect(qqoqcp_config.qqoqcp_cls.last.pos).to eq 2
      end
    end
    context 'line is already in qqoqcp.' do
      it 'do not adds the line to qqoqcp configuration.' do
        qqoqcp_config.add_line(@line1)
        qqoqcp_config.add_line(@line2)
        qqoqcp_config.add_line(@line1)

        expect(qqoqcp_config.qqoqcp_lines.size).to eq 2
      end
    end
  end

  describe '#delete_line' do
    before :each do
      @line1 = Labels::QqoqcpLine.create!(internal_name: 'who', name: 'QUI ?')
      @line2 = Labels::QqoqcpLine.create!(internal_name: 'what', name: 'QUOI ?')
    end
    context 'line is in qqoqcp.' do
      it 'deletes a line from qqoqcp configuration.' do
        qqoqcp_config.add_line(@line1)
        qqoqcp_config.add_line(@line2)

        qqoqcp_config.delete_line(@line1)
        expect(qqoqcp_config.qqoqcp_lines.size).to eq 1
      end
    end
    context 'line is already in qqoqcp.' do
      it 'do not adds the line to qqoqcp configuration.' do
        qqoqcp_config.add_line(@line1)

        qqoqcp_config.delete_line(@line2)
        expect(qqoqcp_config.qqoqcp_lines.size).to eq 1
      end
    end
  end
end
