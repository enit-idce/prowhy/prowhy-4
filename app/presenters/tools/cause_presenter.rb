module Tools
  class CausePresenter < BasePresenter
    include ActionView::Helpers::UrlHelper
    include IconsHelper
    # include Pundit
    # include FontAwesome::Rails::IconHelper
    # include FontAwesome::Sass::Rails::ViewHelpers

    def cause_level_bold
      return level && level_before_type_cast > 1 ? 'font-weight:bold;' : ''
    end

    def cause_delete_link
      cause_head_tree = parent.nil? ? 'cause-head-tree' : ''
      link_to fa_icon_delete_presenter,
              Rails.application.routes.url_helpers.tools_cause_path(id, locale: I18n.locale),
              method: :delete, remote: true,
              class: "icon-right #{cause_head_tree}",
              title: "Delete cause #{name.downcase}",
              data: { confirm: I18n.t('confirm.del_subtree') }
    end

    def cause_add_link(link_text = '')
      link_to fa_icon_add_presenter + ' ' + link_text,
              Rails.application.routes.url_helpers.new_tools_cause_path(locale: I18n.locale, tools_cause: {parent_id: id, rca_id: rca_id, tool_reference: tool_reference}),
              remote: true,
              class: 'icon-right',
              title: "Add cause after #{name.downcase}"
    end

    def cause_edit_link
      link_to fa_icon_edit_presenter,
              Rails.application.routes.url_helpers.edit_tools_cause_path(id, locale: I18n.locale),
              remote: true,
              class: 'icon-right',
              title: 'Edit cause'
    end

    # Création de la structure JSon pour jstree
    def cause_tree_line(ishikawas = [])
      cause_head_tree = parent.nil? ? 'cause-head-tree' : ''
      ret = "<span id='#{id}_link' class='cause-name" \
            "' data-fct-dnd='#{Rails.application.routes.url_helpers.tools_cause_path(id, locale: I18n.locale)}'>"
      ret += name ? name.downcase : '-'
      ret += '</span>'

      ret += "<span style='padding-right: 5px; float: right;'>"

      ishikawas.each do |ishikawa|
        ishi_txt = Labels::CauseType.find(ishi_cause_type_id(ishikawa)).name
        ret += "<span style='display: inline-block;width:100px;text-align:center;'>"
        ret += ishi_txt
        ret += '</span>'
      rescue StandardError
        ret += "<span style='display: inline-block;width:100px;text-align:center;'>"
        ret += ''
        ret += '</span>'
      end
      # ret += "<span style='display: inline-block;width:50px;text-align:center;'>"
      # ret += cause_icon_status
      # # ret += cause_icon_status_editable
      # ret += '</span>'
      ret += "<span style='display: inline-block;width:50px;text-align:center;#{cause_level_bold}'>#{level}</span>"
      # if policy(self).destroy?
      # if authorize self, 'destroy'
      # if cause_links.empty?
      ret += if cause_links.empty?
               link_to fa_icon_delete_presenter,
                       Rails.application.routes.url_helpers.tools_cause_path(id, locale: I18n.locale),
                       method: :delete, remote: true,
                       class: "icon-right #{cause_head_tree}",
                       title: "Delete cause #{name.downcase}",
                       data: { confirm: I18n.t('confirm.del_subtree') }
             else
               fa_icon_hidden_presenter('icon-right')
             end
      ret += link_to fa_icon_add_presenter,
                     Rails.application.routes.url_helpers.new_tools_cause_path(locale: I18n.locale, tools_cause: {parent_id: id, rca_id: rca_id, tool_reference: tool_reference}),
                     remote: true,
                     class: 'icon-right',
                     title: "Add cause after #{name.downcase}"

      ret += link_to fa_icon_edit_presenter,
                     Rails.application.routes.url_helpers.edit_tools_cause_path(id, locale: I18n.locale),
                     remote: true,
                     class: 'icon-right',
                     title: 'Edit cause'
      ret += '</span>'
      return ret
    end

    def cause_icon_status
      # return export_status_icon(status) if @export_pdf

      ret = if status_in_progress?
              fa_icon_status_presenter('icon-status-inprogress')
            elsif status_validated?
              fa_icon_status_presenter('icon-status-validated')
            else
              fa_icon_status_presenter('icon-status-invalidated') # status_invalidated?
            end
      return ret
    end

    def cause_icon_status_editable
      # ret = cause_icon_status
      ret = "<span id='cause-edit-status-#{id}' style='color:#228;padding-left:5px;font-size:smaller;'>"
      ret += link_to fa_icon_pen_presenter, Rails.application.routes.url_helpers.tools_cause_edit_status_path(id: id, locale: I18n.locale),
                     remote: true, title: 'edit status'
      ret += '</span>'

      return ret.html_safe
    end

    def cause_tree_infos(ishikawas = [])
      data = { text: cause_tree_line(ishikawas),
               id: id,
               type: status,
               a_attr: { style: 'width: calc(100% - 25px);' } # class: 'tree_alt_a' }
               # 'state' => {'concept_status' => concept.status }
             }

      # data['state'] = {'selected' => false, 'concept_status' => concept.status }
      return data
    end

    def cause_subtree_datas(ishikawas = [])
      data = cause_tree_infos(ishikawas)

      unless children.empty?
        data[:children] = []

        children.includes(:cause_links).each do |child|
          data[:children] << Tools::CausePresenter.new(child, @view).cause_subtree_datas(ishikawas)
        end
      end

      return data
    end
  end
end
