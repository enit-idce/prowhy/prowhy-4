require_dependency 'meth'

module Meth
  class Step < ApplicationRecord
    include MethConcern

    before_destroy :check_advance_rca_before_destroy, prepend: true

    translates :name, fallback: :any

    belongs_to :methodology
    has_many :step_tools, -> { where(step_tool_type: 1).order(pos: :asc) }, dependent: :destroy
    has_many :step_tools_with_desc, lambda {
                                      where(step_tool_type: 1).includes(tool_custom_fields: [custom_field_desc: [:custom_field_type]])
                                                              .order(pos: :asc)
                                    }, class_name: 'StepTool'
    has_many :step_tools_with_list, lambda {
                                      where(step_tool_type: 1).includes(tool_custom_fields: [custom_field_desc: [:custom_field_type, :list_contents]])
                                                              .order(pos: :asc)
                                    }, class_name: 'StepTool'
    has_many :tools, through: :step_tools

    belongs_to :step_role

    has_one :step_tool_doc, -> { where(step_tool_type: 2) }, class_name: 'StepTool', dependent: :destroy
    has_one :step_tool_img, -> { where(step_tool_type: 3) }, class_name: 'StepTool', dependent: :destroy

    # Advancement
    has_many :rca_advances, foreign_key: :meth_step, dependent: :destroy
    has_many :rcas, foreign_key: :advance_step_id # , dependent: :nullify
    # Attention : surtout pas de destroy ici => link les rcas dont le step advance est = step

    validates :name, presence: true
    validates :step_type, inclusion: { in: [1, 2, 3] }

    after_create :add_default_step_tools
    # 1: menu / 2: step0_infos / 3: step_trstools
    # A ajouter (a voir) : 4: step pour User PA
    # Voir commentaires step_trstools dans classe Methodology

    def locked?
      methodology&.locked?
    end

    def add_tool(tool, options = {})
      # StepTool.create!(options.merge!(tool: tool, step: self, pos: step_tools.count+1,
      #                                 name_fr: tool.name_fr, name_en: tool.name_en, tool_options: tool.tool_options))
      StepTool.create!({tool: tool, step: self, pos: step_tools.count+1,
                        name_fr: tool.name_fr, name_en: tool.name_en, tool_options: tool.tool_options}.merge(options))
    end

    def name_show
      methodology.name + ' / ' + name
    end

    def name_show_html
      '<b>' + methodology.name + '</b> / ' + name
    end

    def copy(methodo2)
      step = Step.create!(methodology: methodo2,
                          name_fr: name_fr, name_en: name_en,
                          pos: pos,
                          step_type: step_type,
                          step_role_id: step_role_id,
                          locked: locked)
      # step_tool_doc&.copy(step)
      # step_tool_img&.copy(step)

      # step_tools_with_list.each do |step_tool|
      #   step_tool.copy(step)
      # end
      copy_step_tools(step)

      return step
    end

    def copy_step_tools(step)
      step_tools_with_list.each do |step_tool|
        step_tool.copy(step)
      end
    end

    private

    def add_default_step_tools
      return unless step_type == 1

      tool = Tool.find_or_create_tool(tool_name: 'Document')
      add_tool(tool, step_tool_type: 2, tool_reference: step_role.id) if tool

      tool = Tool.find_or_create_tool(tool_name: 'Image')
      add_tool(tool, step_tool_type: 3, tool_reference: step_role.id) if tool
    end

    def check_advance_rca_before_destroy
      rcas.each do |rca|
        next unless rca.advance_step == self

        rca.advance_step = nil
        rca.advance = 0

        rca.save!
      end
    end
  end
end
