class DataQqoqcpConvert < ActiveRecord::Migration[6.0]
  def up
    return unless Labels::QqoqcpConfig.all.empty?

    # Create Qqoqcp config and lines
    say '-- Create Qqoqcpc configuration.'
    qqoqcp_config = Labels::QqoqcpConfig.create!(name_fr: 'QQOQCPC', name_en: 'WWWWHWH')

    values = [{internal_name: 'qui', name_fr: 'QUI ?', name_en: 'WHO ?'},
              {internal_name: 'quoi', name_fr: 'QUOI ?', name_en: 'WHAT ?'},
              {internal_name: 'ou', name_fr: 'OU ?', name_en: 'WHERE ?'},
              {internal_name: 'quand', name_fr: 'QUAND ?', name_en: 'WHEN ?'},
              {internal_name: 'comment', name_fr: 'COMMENT ?', name_en: 'HOW ?'},
              {internal_name: 'pourquoi', name_fr: 'POURQUOI ?', name_en: 'WHY ?'},
              {internal_name: 'combien', name_fr: 'COMBIEN ?', name_en: 'HOWMUCH ?'}]

    values.each do |val|
      qq_line = Labels::QqoqcpLine.find_or_create_by(val)
      qqoqcp_config.reload.add_line(qq_line)
    end

    # TODO : ICI PB
    unless Meth::StepTool.all.empty?
      say '-- Associate Qqoqcpc config to step tools.'
      Meth::StepTool.includes(:tool).where(meth_tools: {tool_name: 'Qqoqcp'}).each do |step_tool|
        step_tool.tool_options ||= {}
        step_tool.tool_options['qqoqcp_config'] = qqoqcp_config.id
        step_tool.save!
      end
    end

    # Transfert datas (old Qqoqcp => new)
    # qqoqcp_config = Labels::QqoqcpConfig.first
    return if Tools::Qqoqcp.all.empty?

    say '-- Transfer old format datas.'
    Tools::Qqoqcp.all.each do |qqoqcp|
      next unless qqoqcp.rca

      qqoqcp.labels_qqoqcp_config = qqoqcp_config

      qqoqcp.save!
      qqoqcp.initialize_qqoqcp

      qqoqcp.qqoqcp_values.each do |qqoqcp_value|
        qqoqcp_value.is_value = qqoqcp.send(qqoqcp_value.labels_qqoqcp_line.internal_name)
        qqoqcp_value.isnot_value = qqoqcp.send("#{qqoqcp_value.labels_qqoqcp_line.internal_name}_not")
        qqoqcp_value.info_value = qqoqcp.send("#{qqoqcp_value.labels_qqoqcp_line.internal_name}_info")

        qqoqcp_value.save!
      end
    end
  end

  def down
    # raise ActiveRecord::IrreversibleMigration
  end
end
