require 'rails_helper'

RSpec.describe Post, type: :model do
  subject(:post) do
    FactoryBot.create(:post)
  end

  context 'describes model associations and validations' do
    # Tests belongs_to à faire quand les Post seront associés à quelque chose.
    # describe 'Associations' do
    # end

    describe 'Validations' do
      it 'is not valid without title' do
        post.title = ''
        expect(post).to_not be_valid
      end

      it 'is valid with title' do
        post.title = 'Team'
        expect(post).to be_valid
      end
    end
  end
end
