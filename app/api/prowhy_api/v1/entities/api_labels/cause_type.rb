module ProwhyApi
  module V1
    module Entities
      module ApiLabels
        class CauseType < Grape::Entity
          expose :id
          expose :name
        end
      end
    end
  end
end
