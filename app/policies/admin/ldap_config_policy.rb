module Admin
  class LdapConfigPolicy < AdminPolicy
    attr_reader :user, :record

    def initialize(user, record)
      @user = user
      @record = record
    end

    def ldap_config_test?
      edit?
    end
  end
end
