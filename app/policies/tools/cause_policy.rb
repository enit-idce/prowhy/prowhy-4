module Tools
  class CausePolicy < ToolsPolicy
    attr_reader :user, :record, :rca

    def edit_status?
      return false unless update?

      return record.parent && record.tools_tasks.where(task_detail_type: 'Tools::TaskValidation').empty?
    end

    def destroy?
      # if cause is head of a Tool => no destroy !
      return false unless record.cause_links.empty?

      super
    end
  end
end
