require 'rails_helper'

RSpec.describe Meth::ListContent, type: :model do
  subject(:list_content) do
    FactoryBot.create(:meth_list_content, custom_field_desc: custom_field_desc)
  end

  let(:custom_field_desc) { FactoryBot.create(:meth_custom_field_desc, custom_field_type: custom_field_type) }
  let(:custom_field_type) { FactoryBot.create(:meth_custom_field_type, :type_select) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to custom_field_desc' do
        assc = Meth::ListContent.reflect_on_association(:custom_field_desc)
        expect(assc.macro).to eq :belongs_to
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        list_content.name = ''
        expect(list_content).to_not be_valid
      end

      it 'is valid with name' do
        expect(list_content).to be_valid
      end

      it 'is not valid with custom field desc with type:field_internal different from select' do
        list_content
        custom_field_type.field_internal = 'text'
        custom_field_type.save!
        expect(list_content).to_not be_valid
      end

      it 'is valid with custom field desc with type:field_internal = select' do
        expect(list_content).to be_valid
      end
    end
  end

  context 'References' do
    describe 'shows reference if exists' do
      it 'shows nothing (no reference)' do
        expect(list_content.reference_show).to eq ''

        expect(list_content.reference_show_field).to eq ''
      end

      it 'shows reference custom field and value' do
        cfdesc2 = FactoryBot.create(:meth_custom_field_desc, custom_field_type: custom_field_type, name: 'CFDReference')
        custom_field_desc.ref_field = cfdesc2
        custom_field_desc.save!

        # return '' if no ref_value
        expect(list_content.reference_show).to eq ''

        # return ref field with ref_value
        FactoryBot.create(:meth_list_content, custom_field_desc: cfdesc2, name: 'Content1')
        cfdesc2_lc2 = FactoryBot.create(:meth_list_content, custom_field_desc: cfdesc2, name: 'Content2')

        list_content.ref_value = cfdesc2_lc2.id
        list_content.save!

        expect(list_content.reference_show).to eq 'CFDReference / Content2'
      end

      it 'shows reference custom field' do
        cfdesc2 = FactoryBot.create(:meth_custom_field_desc, custom_field_type: custom_field_type, name: 'CFDReference')
        custom_field_desc.ref_field = cfdesc2
        custom_field_desc.save!

        # return '' if no ref_value
        expect(list_content.reference_show_field).to eq 'CFDReference'
      end

      it 'shows reference collection if exists' do
        expect(list_content.reference_collection).to eq nil

        cfdesc2 = FactoryBot.create(:meth_custom_field_desc, custom_field_type: custom_field_type, name: 'CFDReference')
        custom_field_desc.ref_field = cfdesc2
        custom_field_desc.save!

        # return '' if no collection
        expect(list_content.reference_show).to eq ''

        # return collection
        lc1 = FactoryBot.create(:meth_list_content, custom_field_desc: cfdesc2, name: 'Content1')
        lc2 = FactoryBot.create(:meth_list_content, custom_field_desc: cfdesc2, name: 'Content2')

        expect(list_content.reference_collection).to contain_exactly([lc1.id, 'Content1'], [lc2.id, 'Content2'])
      end
    end
  end

  # TODO: Peut-être à supprimer car pas sur que ça fonctionne avec les colonnes du Custom Field qui ne devraient pas exister.
  # context 'describe finalize class' do
  #   it '#finalize class' do
  #     list_content
  #     lc1 = FactoryBot.create(:meth_list_content, custom_field_desc: custom_field_desc, name: 'Content1')
  #     # lc2 = FactoryBot.create(:meth_list_content, custom_field_desc: custom_field_desc, name: 'Content2')
  #     cf1 = CustomField.create!(rca: FactoryBot.create(:rca))

  #     Meth::ListContent.finalize

  #     cf1.update(custom_field_desc.internal_name.intern => lc1.id)
  #     # binding.pry
  #     expect(cf1.send(custom_field_desc.internal_name.intern).to_s).to eq lc1.id.to_s

  #     lc1.destroy

  #     expect(cf1.reload.send(custom_field_desc.internal_name.intern)).to eq nil
  #   end
  # end
end
