require 'rails_helper'

RSpec.describe Person, type: :model do
  subject(:person) do
    FactoryBot.create(:person)
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has one user' do
        assc = Person.reflect_on_association(:user)
        expect(assc.macro).to eq :has_one
      end
    end

    describe 'Validations' do
      it 'is not valid without firstanme and lastname' do
        person.firstname = ''
        expect(person).to_not be_valid
        person.reload
        person.lastname = ''
        expect(person).to_not be_valid
      end

      it 'is valid with firstname and lastname' do
        person.firstname = 'Toto'
        person.lastname = 'Titi'
        expect(person).to be_valid
      end
    end
  end

  it 'name return full person name (lastname first)' do
    person.firstname = 'Alfred'
    person.lastname = 'Titi'
    expect(person.name).to eq 'Titi Alfred'
  end

  it 'name return full person name (firstname first)' do
    person.firstname = 'Alfred'
    person.lastname = 'Titi'
    expect(person.name_fl).to eq 'Alfred Titi'
  end

  describe 'returns email' do
    it 'returns email from user if user exists' do
      person.user = FactoryBot.create(:user)
      person.save!
      expect(person.the_email).to eq person.user.email
    end
    it 'returns email from person if user does not exist' do
      expect(person.the_email).to eq person.email
    end
  end
end
