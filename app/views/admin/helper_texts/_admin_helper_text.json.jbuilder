json.extract! admin_helper_text, :id, :tool_class, :reference, :help_text, :created_at, :updated_at
json.url admin_helper_text_url(admin_helper_text, format: :json)
