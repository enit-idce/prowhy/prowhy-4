class AddTaskDetailToToolsTask < ActiveRecord::Migration[6.0]
  def change
    add_reference :tools_tasks, :task_detail, polymorphic: true, after: :tool_reference # , null: false
  end
end
