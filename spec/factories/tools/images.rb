FactoryBot.define do
  factory :tools_image, class: Tools::Image do
    name { 'Image 1' }
    tool_reference { 1 }
    tool_num { 1 }
    association :rca, factory: :rca
  end
end
