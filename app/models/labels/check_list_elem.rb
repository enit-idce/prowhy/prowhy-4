module Labels
  class CheckListElem < ApplicationRecord
    validates :name, presence: true
    validates :task_detail_type, presence: true

    has_many :tools_tasks, class_name: 'Tools::Task', dependent: :nullify
  end
end
