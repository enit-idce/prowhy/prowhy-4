class CreateMethStepRoles < ActiveRecord::Migration[6.0]
  def change
    create_table :meth_step_roles do |t|
      t.string :role_name
      t.string :name_fr
      t.string :name_en
    end
  end
end
