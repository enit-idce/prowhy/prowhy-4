class RcasController < ApplicationController
  include StandardConcern
  include CustomFieldsHelper
  before_action :set_rca, only: [:show, :edit, :update, # :destroy,
                                 :advancement_set, :assign_perimeters, :assign_perimeters_valid,
                                 :rca_add_parent, :rca_add_parent_valid, :rca_delete_parent]
  # skip_before_action :init_rca # , except: [:step_menu_tool]

  after_action :call_track_action, only: [:create, :destroy, :update, :advancement_set, :recovery_do]

  # GET /rcas
  # GET /rcas.json
  def rcas_list(params = {})
    # authorize Rca
    @rca = nil
    # @rcas = policy_scope(Rca).includes(:custom_field, :methodology, :action_text_rich_text, rca_advances: [meth_step: [:step_role]])

    # Si on vient de search => on charge les params dans usersearch
    # Si on vient de reinit => reinitialise usersearch
    Users::UpdateUserSearch.new(current_user).call(params)

    # Params de recherche copiés dans search_params
    search_params = current_user.usersearch.deep_symbolize_keys
    @something_to_search = Users::UpdateUserSearch.new(current_user).something_to_search?

    # Objets de recherche pour le formulaire dans @serach_custom_field et @rca_search
    @search_custom_field = CustomField.new(search_params[:custom_field]) # @search_rca.build_custom_field
    @rca_search = RcaSearch.new(search_params[:rca_search])
    # TEST
    # @rca_search.words_one ||= true
    # @rca_search.words_type ||= ['','1']
    # End test

    @items = params[:items]
    @items ||= Pagy::DEFAULT[:items]
    @page_number = params[:page]
    @page_number ||= 1

    # On inclut dans la requête les custom fields de type liste et taxonomie
    custom_preload_list = Meth::CustomFieldDesc.custom_preload_list_values

    # binding.pry

    @rcas = policy_scope(Rca).includes(:methodology, :tools_rca_list, custom_field: custom_preload_list, advance_step: [:step_role]).with_rich_text_description.order(created_at: :desc)

    order = Admin::AdminConfig.first&.home_options&.[]('show_custom_order')
    order ||= []
    cfd_list = Meth::CustomFieldDesc.where(show_list: true)
    order += cfd_list.map(&:id).map(&:to_s)

    @custom_field_descs = cfd_list.sort { |a, b| order.find_index(a.id.to_s) - order.find_index(b.id.to_s) }

    # Filtrage des Rcas (search)
    @rcas = Rcas::FilterRcas.new(@rcas).call(search_params) unless params[:reinit_filters]

    # Graphs : get datas
    @advance_labels, @advance_values = Rcas::DataGraph.new(@rcas).create_datas
    # binding.pry

    # Pagy : pagination
    @pagy, @rcas = pagy(@rcas, items: @items,
                               steps: { 0 => [1, 2, 2, 1],
                                        540 => [2, 3, 3, 2],
                                        720 => [3, 5, 5, 3] })
  end

  # GET /rcas
  # GET /rcas.json
  def index
    rcas_list(params)
  end

  # PATCH
  def reinit_filters_index
    authorize Rca
    redirect_to rcas_path(items: params[:items], reinit_filters: true), method: :patch
  end

  # GET
  def reinit_filters
    authorize Rca

    Users::UpdateUserSearch.new(current_user).call(reinit_filters: true)

    search_params = current_user.usersearch.deep_symbolize_keys
    # @something_to_search = true
    # @something_to_search = Users::UpdateUserSearch.new(current_user).something_to_search?

    @search_custom_field = CustomField.new(search_params[:custom_field])
    @rca_search = RcaSearch.new(search_params[:rca_search])
    @items = params[:items]

    respond_to do |format|
      format.js # { render js: "$('#collapse-rcasearch').html('<%= escape_javascript(render partial: \'rcas_seach\') %>');" }
    end
    # redirect_to rcas_path(reinit_filters: true), method: :patch
  end

  # PATCH /rca_reinit_all_advancements
  def reinit_all_advancements
    authorize Rca
    @rca = nil

    @rcas = policy_scope(Rca)
    @rcas.each do |rca|
      Rcas::UpdateAdvance.new(rca).call
    end

    redirect_to rcas_path
  end

  # GET /rcas/1
  # GET /rcas/1.json
  def show
    authorize @rca
  end

  # GET /rcas/new
  def new
    @rca = params[:rca] ? Rca.new(rca_params) : Rca.new
    admin_config = Admin::AdminConfig.first

    @rca.methodology_id = admin_config.options&.[]('default_methodology') if admin_config && !@rca.methodology_id

    authorize @rca
  end

  # GET /rcas/1/edit
  def edit
    step_tool = @rca.methodology&.step_info&.step_tools&.first

    current_user.set_session(:active_step_tool, step_tool&.id || nil) # @rca.methodology.step_info.step_tools.first.id)

    authorize @rca
    render 'edit', layout: 'processus'
  end

  # POST /rcas
  # POST /rcas.json
  def create
    @rca = Rca.new(rca_params)
    authorize @rca

    respond_to do |format|
      if @rca.save
        @rca.rca_creation_init(current_user)
        # # Init Author
        # @rca.add_rca_author(current_user.person)
        # @rca.add_rca_responsible(current_user.person)
        # # Initialisation rca
        # @rca.initialize_rca_infos(current_user.preferences)
        # # Init default perimeter
        # default_perimeter_id = current_user.get_preference(:perimeter)
        # # si pas de perimetre utilisateur par défaut : ALL
        # default_perimeter_id ||= Admin::Perimeter.where(ancestry: nil)&.first&.id
        # if default_perimeter_id
        #   default_perimeter = Admin::Perimeter.find(default_perimeter_id)
        #   @rca.add_perimeter_and_childs(default_perimeter)
        # end

        if params[:criticity_button]
          format.html do
            step_tool = @rca.methodology.step_trstools.step_tools.first
            current_user.set_session(:active_step_tool, step_tool.id) if step_tool
            redirect_to new_tools_criticity_path(tools_criticity: { rca_id: @rca.id,
                                                                    tool_reference: step_tool.tool_reference,
                                                                    tool_num: step_tool.tool_num })
          end
        else
          format.html { redirect_to edit_rca_path(@rca), notice: 'Rca was successfully created.' }
          format.json { render :show, status: :created, location: @rca }
        end
      else
        format.html { render :new }
        format.json { render json: @rca.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rcas/1
  # PATCH/PUT /rcas/1.json
  def update
    authorize @rca

    methodology_id = @rca.methodology_id
    respond_to do |format|
      if @rca.update(rca_params)
        if @rca.methodology_id != methodology_id
          @rca.reinitialize_rca_infos
          Rcas::UpdateAdvance.new(@rca).call
        end
        if params[:set_cause_title]
          @rca.tools_cause_trees.each do |cause_tree|
            cause_tree.cause_link.cause.update(name: @rca.long_title)
          end
        end
        format.html { redirect_to edit_rca_path(@rca), notice: 'Rca was successfully updated.' }
        format.json { render :show, status: :ok, location: @rca }
      else
        @rca.reload
        format.html { render :edit, layout: 'processus' }
        format.json { render json: @rca.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rcas/1
  # DELETE /rcas/1.json
  def destroy
    @rca = Rca.unscoped.find(params[:id])
    authorize @rca

    if params[:mode] == 'destroy'
      @rca.destroy
    else
      @rca.status_deleted!
      @rca.save!
    end

    respond_to do |format|
      format.html do
        if params[:mode] == 'destroy'
          redirect_to rca_recovery_path, notice: 'Rca was successfully destroyed.'
        else
          redirect_to rcas_url, notice: 'Rca was successfully destroyed.'
        end
      end
      format.json { head :no_content }
    end
  end

  # PATCH
  def advancement_set
    # authorize @rca

    step = Meth::Step.find(params[:step])
    skip_authorization && RcaPolicy.new(current_user, @rca).advancement_set_test?(step)

    @rca.advancement_set(step, params[:advance])
    # update global rca advancement
    Rcas::UpdateAdvance.new(@rca).call

    respond_to do |format|
      format.html { redirect_back(fallback_location: edit_rca_path(@rca)) }
      format.json { head :no_content }
    end
  end

  # GET
  def rca_closure
  end

  # PERIMETERS associations
  # GET
  def assign_perimeters
    authorize @rca
    # binding.pry
    render 'assign_perimeters', layout: 'processus'
  end

  # PATCH
  def assign_perimeters_valid
    authorize @rca
    associate_perimeters_to_rcas

    redirect_to edit_rca_path(id: @rca.id)
  end

  # GET /recovery
  def recovery
    authorize Rca

    @rcas = policy_scope(Rca.unscoped.deleted, policy_scope_class: RcaPolicy::RecoveryScope).includes(:methodology, tools_teams: [:person])
    # binding.pry
  end

  # PATCH
  def recovery_do
    @rca = Rca.unscoped.find(params[:id])
    authorize @rca, :destroy?

    @rca.status_active!
    @rca.save!

    respond_to do |format|
      format.html { redirect_to rca_recovery_path, notice: 'Rca was successfully recovered.' }
      format.json { head :no_content }
    end
  end

  # GET /rcas
  # GET /rcas.json
  def export_ods
    authorize Rca, :index?

    rcas_list(params)
    workbook = Rspreadsheet.new
    sheet = workbook.create_worksheet 'Rcas list'
    row = sheet.row(1)

    row[1] = 'ID'
    row[2] = Rca.human_attribute_name :title # 'TITLE'
    row[3] = Rca.human_attribute_name :description # 'DESCRIPTION'
    row[4] = Rca.human_attribute_name :created_at # 'CREATED_AT'
    row[5] = Rca.human_attribute_name :closed_at # 'CLOSED_AT'
    row[6] = Rca.human_attribute_name :methodology # 'METHODOLOGY'

    # TODO : paramétrer les colonnes exportées en ODS dans l'administration
    columns = []
    columns_titles = []
    numcol = 7
    CustomField.column_names.each do |cf|
      next unless cf.include? 'custom_'

      cfdesc = Meth::CustomFieldDesc.where(internal_name: cf)&.first
      next unless cfdesc

      row[numcol] = cfdesc.name
      numcol += 1
      columns << cf
      columns_titles << cfdesc.name
    end

    # Title style
    (1..numcol).each do |i|
      row.cells(i).format.bold = true
    end

    # binding.pry
    @rcas.each_with_index do |rca, idx|
      row = sheet.row(idx+2)

      row[1] = rca.id
      row[2] = rca.title
      row[3] = rca.description&.body&.to_plain_text || ''
      row[4] = rca.created_at&.to_date&.to_s || ''
      row[5] = rca.closed_at&.to_s || ''
      row[6] = rca.methodology.name

      columns.each_with_index do |cf, numc|
        cfdesc = Meth::CustomFieldDesc.where(internal_name: cf)&.first
        row[numc+7] = show_custom_field_as_plain_text(rca.custom_field, cfdesc) # rca.custom_field.send(cf)
      end
    end

    # workbook.save('testfile.ods')

    send_data workbook.to_io.read.force_encoding('binary'), filename: 'rcas_export.ods'

    # render :index
    # redirect_to rcas_url
  end

  # GET
  def rca_add_parent
    authorize @rca

    @items = params[:items]
    @items ||= 100
    @page_number = params[:page]
    @page_number ||= 1

    @rcas = Rca.all.where.not(id: @rca.id).includes(:methodology).order(created_at: :desc)

    # Pagy : pagination
    @pagy, @rcas = pagy(@rcas, link_extra: "data-remote='true'",
                               items: @items,
                               steps: { 0 => [1, 2, 2, 1] })
  end

  # PATCH
  def rca_add_parent_valid
    authorize @rca, :rca_add_parent?
    message = ''

    begin
      rca_parent = Rca.find(params[:rcas])
      message = "Set RCA #{rca_parent.reference} as new parent."
    rescue StandardError
      message = 'No RCA parent selected.'
    end
    @rca.set_rca_parent(rca_parent) if rca_parent

    redirect_to edit_rca_path(@rca), notice: message
  end

  # DELETE
  def rca_delete_parent
    authorize @rca, :rca_add_parent?

    @rca.del_rca_parent

    redirect_to edit_rca_path(@rca), notice: 'RCA parent deleted.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_rca
    # @rca = Rca.find(params[:id])
    @rca = policy_scope(Rca).find(params[:id])

    # authorize @rca => déplacé dans les fonctions car pour edit nécessaire d'appliquer le current_session AVANT de faire l'autorisation.
  end

  # Only allow a list of trusted parameters through.
  def rca_params
    # puts "RCA PARAMS===================================>".yellow
    # puts p(params)
    # puts "<=============================================".yellow

    tab_permit = create_tab_permit_custom
    tab_permit << 'id'
    params.require(:rca).permit(:reference, :title, :methodology_id, :description,
                                :created_at, :updated_at,
                                :tools_rca_list_id,
                                custom_field_attributes: tab_permit)
  end

  # TODO: à déplacer dans model ou service
  def associate_perimeters_to_rcas
    perimeters_in = params['perimeter_ids']&.map(&:to_i) # params&.[]('perimeter_ids')&.map(&:to_i)
    rca_perimeters = @rca.perimeters.map(&:id)

    # Ajout des perimeters sélectionnés :
    perimeters_in&.each do |perimeter_id|
      next if rca_perimeters.include?(perimeter_id)

      @rca.add_perimeter(perimeter_id)
      # perimeter = Perimeter.find(perimeter_id)
      # puts "ADD Perimeter #{perimeter.name}".green
    end

    # Suppression des périmètres non sélectionnés :
    rca_perimeters&.each do |perimeter_id|
      next if perimeters_in&.include?(perimeter_id)

      @rca.remove_perimeter(perimeter_id)
      # perimeter = Perimeter.find(perimeter_id)
      # puts "DEL Perimeter #{perimeter.name}".red
    end
  end
end
