require_dependency 'tools'

module Tools
  class Indicator < ApplicationRecord
    include ToolConcern

    has_many :indicator_measures, -> { order(pos: :asc) }, dependent: :destroy
    validates :name, presence: true

    after_initialize do
      if new_record?
        # Init configurations
        self.graph_options = { type: 'line',
                               color: 0 }
      end
    end

    def self.create_default!(_options, _step_tool)
      []
    end

    # def self.search_default(rca, _tool_name, step_tool)
    #   step_tool.tool_options ||= {}
    #   return rca.tools_indicators.where(tool_reference: step_tool.tool_reference,
    #                                     tool_num: step_tool.tool_num)
    # end

    def self.create_search_multiple?
      return true
    end

  end
end
