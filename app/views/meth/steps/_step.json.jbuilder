json.extract! step, :id, :name, :pos, :methodology_id, :created_at, :updated_at
json.url meth_step_url(step, format: :json)
