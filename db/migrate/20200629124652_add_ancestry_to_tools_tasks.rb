class AddAncestryToToolsTasks < ActiveRecord::Migration[6.0]
  def change
    add_column :tools_tasks, :ancestry, :string, after: :tools_cause_id
    add_index :tools_tasks, :ancestry
  end
end
