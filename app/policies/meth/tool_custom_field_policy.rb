module Meth
  class ToolCustomFieldPolicy < Meth::MethPolicy
    attr_reader :user, :record

    def initialize(user, record)
      @user = user
      @record = record
    end

    # def show_tool?
    #   return true # TODO : check if this is ok
    # end

    def tool_custom_field_up?
      edit?
    end

    def tool_custom_field_down?
      edit?
    end

    # def index?
    #   show?
    # end

    # def show?
    #   user.has_role?(:admin) || user.has_role?(:super_admin)
    # end

    def create?
      user.has_role?(:admin) || user.has_role?(:super_admin)
    end

    def new?
      create?
    end

    def update?
      create?
    end

    def edit?
      create?
    end

    def destroy?
      create?
    end
  end
end
