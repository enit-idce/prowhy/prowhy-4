class ApplicationMailer < ActionMailer::Base
  # default from: 'no-reply@prowhy.org'
  layout 'mailer'
end
