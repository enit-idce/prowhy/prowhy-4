module ProwhyApi
  module V1
    module Entities
      module ApiTools
        class Image < Grape::Entity
          expose :id, unless: { with_id: 'false'}
          expose :name
          expose :file

          private

          def file
            Rails.application.routes.url_helpers.rails_blob_path(object.file, only_path: true)
          end
        end
      end
    end
  end
end
