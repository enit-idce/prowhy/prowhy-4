module Meth
  class StepToolsController < ApplicationController
    before_action :set_meth_step_tool

    # GET /step_tools/1/edit
    def edit
      @modal_title = @step_tool&.tool&.name
    end

    # PATCH/PUT /step_tools/1
    # PATCH/PUT /step_tools/1.json
    def update
      # binding.pry
      respond_to do |format|
        # binding.pry
        if @step_tool.update_with_options(meth_step_tool_params, params[:tool_options] || {})
          # @step_tool.tool_options = params[:tool_options]
          # @step_tool.save!
          # binding.pry
          format.html { redirect_to edit_meth_methodology_path(id: @step_tool.step.methodology, step: @step_tool.step) }
          format.json { render :show, status: :ok, location: @step_tool }
        else
          flash[:error] = 'Update Failed : ' + @step_tool.errors.full_messages.to_s
          format.html { redirect_to edit_meth_methodology_path(id: @step_tool.step.methodology, step: @step_tool.step) }
          format.js { render :edit }
          format.json { render json: @step_tool.errors, status: :unprocessable_entity }
        end
      end
    end

    def step_tool_show
      # Initialize RCA unless step_tool.tool_options[:step_options][:rca] = "all" => @rca = nil
      @rca = Rca.find(params[:rca]) unless params[:rca].nil? || @step_tool&.tool_options&.[](:step_options)&.[](:rca) == 'all'
      current_user.set_session(:active_step_tool, @step_tool.id) if @step_tool.id

      # Test autorisation version READ du step_tool
      # authorize @rca, :show?
      # binding.pry

      if @step_tool.tool.tool_name == 'CustomField'
        redirect_to meth_show_tool_custom_field_path(step_tool: @step_tool, rca: @rca)
      elsif @step_tool.tool.tool_name == 'Qqoqcp'
        qqoqcp = @rca.find_or_create_tool('tools_qqoqcps', 'Tools::Qqoqcp', @step_tool)
        redirect_to edit_tools_qqoqcp_path(id: qqoqcp.id, step_tool: @step_tool)
        # redirect_to public_send('edit_tools_qqoqcp_path', id: qqoqcp.id, step_tool: @step_tool, rca: @rca)
      elsif @step_tool.tool.tool_name == 'CauseTree'
        # NEW VERSION
        cause_tree = @rca.find_or_create_tool('tools_cause_trees', 'Tools::CauseTree', @step_tool)
        redirect_to tools_cause_trees_path(id: cause_tree.id, step_tool: @step_tool, rca: @rca)
      elsif @step_tool.tool.tool_name == 'Why'
        # binding.pry
        cause_tree = @rca.find_or_create_tool('tools_cause_trees', 'Tools::CauseTree', @step_tool)
        redirect_to tools_cause_trees_index_5why_path(id: cause_tree.id, step_tool: @step_tool, rca: @rca)
      elsif @step_tool.tool.tool_name == 'Action'
        # tasks = @rca.find_or_create_tool('tools_tasks', 'Tools::Task', @step_tool)
        redirect_to tools_tasks_path(step_tool: @step_tool, rca: @rca)
      elsif @step_tool.tool.tool_name == 'Ishikawa'
        ishikawa = @rca.find_or_create_tool('tools_ishikawas', 'Tools::Ishikawa', @step_tool)
        redirect_to tools_ishikawas_path(id: ishikawa.id, step_tool: @step_tool, rca: @rca)
        # redirect_to public_send("edit_tools_cause_path", id: cause.id, step_tool: @step_tool)
      elsif @step_tool.tool.tool_name == 'DecisionMat'
        redirect_to tools_decision_mats_path(step_tool: @step_tool, rca: @rca)
        # redirect_to public_send("edit_tools_cause_path", id: cause.id, step_tool: @step_tool)
      elsif @step_tool.tool.tool_name == 'Team'
        redirect_to tools_teams_path(step_tool: @step_tool, rca: @rca)
      elsif @step_tool.tool.tool_name == 'Document'
        redirect_to tools_documents_path(step_tool: @step_tool, rca: @rca)
      elsif @step_tool.tool.tool_name == 'Image'
        redirect_to tools_images_path(step_tool: @step_tool, rca: @rca)
      elsif @step_tool.tool.tool_name == 'Report'
        redirect_to tools_rca_reports_path(step_tool: @step_tool, rca: @rca)
      elsif @step_tool.tool.tool_name == 'Criticity'
        redirect_to tools_criticities_path(step_tool: @step_tool, rca: @rca)
      elsif @step_tool.tool.tool_name == 'Graph'
        redirect_to tools_indicators_path(step_tool: @step_tool, rca: @rca)
      elsif @step_tool.tool.tool_name == 'ActionCheck'
        redirect_to tools_check_efficacity_index_path(step_tool: @step_tool, rca: @rca)
      elsif @step_tool.tool.tool_name == 'Generic'
        generic = @rca.find_or_create_tool('tools_generics', 'Tools::Generic', @step_tool)
        redirect_to edit_tools_generic_path(id: generic.id, step_tool: @step_tool, rca: @rca)
      elsif @step_tool.tool.tool_name == 'RcaList'
        rca_list = @rca.find_or_create_tool('tools_rca_lists', 'Tools::RcaList', @step_tool)
        redirect_to edit_tools_rca_list_path(id: rca_list.id, step_tool: @step_tool, rca: @rca)
      else # render JS
        render 'layouts/step_menu_tool', layout: 'processus', locals: {step: @step_tool.step, tool: @step_tool.tool}
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_meth_step_tool
      @step_tool = StepTool.find(params[:id])
      authorize @step_tool
    end

    # Only allow a list of trusted parameters through.
    def meth_step_tool_params
      params.require(:meth_step_tool).permit(:name, :name_fr, :name_en, :description,
                                             :tool_reference, :tool_num, :step_id, :tool_id,
                                             :tool_options, :generic_file)
    end
  end
end
