json.extract! tools_rca_list, :id, :rca_id, :tool_reference, :description, :created_at, :updated_at
json.url tools_rca_list_url(tools_rca_list, format: :json)
