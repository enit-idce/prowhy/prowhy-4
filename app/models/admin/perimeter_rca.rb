require_dependency 'admin'

module Admin
  class PerimeterRca < ApplicationRecord
    belongs_to :perimeter
    belongs_to :rca
  end
end
