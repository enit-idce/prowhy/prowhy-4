class DataCreateHelperText < ActiveRecord::Migration[6.0]
  class MigrationHelperText < ApplicationRecord
    self.table_name = :admin_helper_texts
  end

  def up
    say 'Create Helper texts.'
    return unless Admin::HelperText.all.empty?

    say '==> for Qqoqcps.'
    MigrationHelperText.create!(tool_class: 'Tools::Qqoqcp', reference: 'who', help_text_fr: 'Quel est le défaut ? Décrivez le, ses caractéristiques ?')
    MigrationHelperText.create!(tool_class: 'Tools::Qqoqcp', reference: 'what', help_text_fr: 'Qui à détecter le défaut ? Qui est impacté par le défaut ?')
    MigrationHelperText.create!(tool_class: 'Tools::Qqoqcp', reference: 'where', help_text_fr: 'Où le défaut a-t-il été détecté ? Où sur la pièce ?')
    MigrationHelperText.create!(tool_class: 'Tools::Qqoqcp', reference: 'when', help_text_fr: 'Quand a-t-il été détecté ?')
    MigrationHelperText.create!(tool_class: 'Tools::Qqoqcp', reference: 'how', help_text_fr: 'Comment a-t-il été détecté ?')
    MigrationHelperText.create!(tool_class: 'Tools::Qqoqcp', reference: 'why', help_text_fr: 'Pourquoi est-ce un problème pour le client ?')
    MigrationHelperText.create!(tool_class: 'Tools::Qqoqcp', reference: 'howmuch', help_text_fr: 'Combien de produits sont concernés ?')
  end

  def down
    say 'Do not delete helper texts.'
    # raise ActiveRecord::IrreversibleMigration
  end
end
