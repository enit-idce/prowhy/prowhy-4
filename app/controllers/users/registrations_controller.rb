# frozen_string_literal: true

module Users
  class RegistrationsController < Devise::RegistrationsController
    layout 'user_page'

    # Custom redirection after user update
    def after_update_path_for(_resource)
      edit_user_registration_path
    end

    # before_action :configure_sign_up_params, only: [:create]
    # before_action :configure_account_update_params, only: [:update]

    # GET /resource/sign_up
    def new
      # Check if admin configuration authorize signup
      admin_config = Admin::AdminConfig.first
      redirect_to new_user_session_path and return if admin_config.connexion_attribute(:database_signup) == false

      super
    end

    # POST /resource
    # def create
    #   super
    # end

    # GET /resource/edit
    def edit
      redirect_to edit_person_path(resource.person, from: :user) and return if resource.ldap_config

      super
    end

    # User preferences
    # GET
    def edit_preferences
      @user = current_user
    end

    # PATCH
    def update_preferences
      @user = current_user
      params[:user][:preferences].each do |pref_key, pref_value|
        @user.set_preference(pref_key, pref_value) # if pref_value && pref_value != ''
      end

      redirect_to edit_user_preferences_registration_path
    end

    # PUT /resource
    # def update
    #   super
    # end

    # DELETE /resource
    # def destroy
    #   super
    # end

    # GET /resource/cancel
    # Forces the session data which is usually expired after sign
    # in to be expired now. This is useful if the user wants to
    # cancel oauth signing in/up in the middle of the process,
    # removing all OAuth session data.
    # def cancel
    #   super
    # end

    # protected

    # If you have extra params to permit, append them to the sanitizer.
    # def configure_sign_up_params
    #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
    # end

    # If you have extra params to permit, append them to the sanitizer.
    # def configure_account_update_params
    #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
    # end

    # The path used after sign up.
    # def after_sign_up_path_for(resource)
    #   super(resource)
    # end

    # The path used after sign up for inactive accounts.
    # def after_inactive_sign_up_path_for(resource)
    #   super(resource)
    # end
  end
end
