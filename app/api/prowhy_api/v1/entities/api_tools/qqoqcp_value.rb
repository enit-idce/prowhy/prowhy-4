module ProwhyApi
  module V1
    module Entities
      module ApiTools
        class QqoqcpValue < Grape::Entity
          expose :line_name, merge: true

          expose :all_values, merge: true
          # expose :is_value if object&.qqoqcp&.labels_qqoqcp_config&.show_is
          # expose :isnot_value if object&.qqoqcp&.labels_qqoqcp_config&.show_isnot
          # expose :info_value if object&.qqoqcp&.labels_qqoqcp_config&.show_info

          private

          def line_name
            { name: object&.labels_qqoqcp_line&.internal_name || 'no name' }
          end

          def all_values
            values = {}

            values[:is_value] = object.is_value if object&.qqoqcp&.labels_qqoqcp_config&.show_is
            values[:isnot_value] = object.isnot_value if object&.qqoqcp&.labels_qqoqcp_config&.show_isnot
            values[:info_value] = object.info_value if object&.qqoqcp&.labels_qqoqcp_config&.show_info

            values
          end
        end
      end
    end
  end
end
