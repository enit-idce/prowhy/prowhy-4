class AddDatesToAdvancements < ActiveRecord::Migration[6.0]
  def change
    add_column :rca_advances, :date_start, :date, after: :advance, default: nil
    add_column :rca_advances, :date_finish, :date, after: :date_start, default: nil
  end
end
