module AncestryConcern
  extend ActiveSupport::Concern

  included do
    has_ancestry

    scope :order_for_list, -> { sort { |a, b| "#{a.ancestry}/#{a.id}" <=> "#{b.ancestry}/#{b.id}" } }
  end

  # Class Methods
  module ClassMethods
    # Ancestry
    def arrange_as_array(options = {}, hash = nil)
      hash ||= arrange(options)
      arr = []
      hash.each do |node, children|
        arr << node
        arr += arrange_as_array(options, children) unless children.empty?
      end
      arr
    end
  end

  def dec_level(output_type = 0)
    lenom = ''
    (1..depth).each do
      lenom += output_type == 1 ? '==> ' : '&#8618; '
    end
    lenom
  end

  # Methods
  def name_level(output_type = 0)
    dec_level(output_type) + (name || '-')
  end

  # Get causes racines
  def root_descendants
    datas = children.empty? ? [self] : []

    children.each do |child|
      datas += child.root_descendants
    end

    return datas
  end
end
