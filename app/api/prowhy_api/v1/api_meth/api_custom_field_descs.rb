require 'doorkeeper/grape/helpers'
require 'devise'

module ProwhyApi
  module V1
    module ApiMeth
      class ApiCustomFieldDescs < V1::Api
        resource :custom_field_descs do
          desc 'Return all custom fields labels.'
          # params do
          #   use :locale
          # end
          get do
            @custom_field_descs = Meth::CustomFieldDesc.joins(:custom_field_type).where.not(meth_custom_field_types: {field_internal: ['tree', 'tag']})

            present @custom_field_descs, with: Entities::ApiMeth::CustomFieldDesc
          end

          desc 'Return CustomFieldDesc (label) :id'
          params do
            requires :id, type: Integer, desc: 'CustomFieldDesc ID.'
            # use :locale
          end
          route_param :id do
            get do
              @custom_field_desc = Meth::CustomFieldDesc.find(params[:id])

              # authorize @rca, :show?

              present @custom_field_desc, with: Entities::ApiMeth::CustomFieldDesc, with_id: 'false'
            end
          end
        end
      end
    end
  end
end
