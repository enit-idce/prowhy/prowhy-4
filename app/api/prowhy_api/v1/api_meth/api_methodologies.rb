require 'doorkeeper/grape/helpers'
require 'devise'

module ProwhyApi
  module V1
    module ApiMeth
      class ApiMethodologies < V1::Api
        resource :methodologies do
          desc 'Return methodologies list (id and names).'
          # params do
          #   use :locale
          # end
          get do
            @methodologies = Meth::Methodology.rca_methodologies_all # .all.rca_methodologies
            # binding.pry
            # authorize Meth::Methodology, :index?

            present @methodologies, with: Entities::ApiMeth::Methodology
          end

          desc 'Return name for Methodology #id.'
          params do
            # use :locale
            requires :id, type: Integer, desc: 'Methodology ID.'
          end
          route_param :id do
            get do
              @methodology = Meth::Methodology.find(params[:id])

              # authorize @methodology, :show?

              present @methodology, with: Entities::ApiMeth::Methodology
            end
          end
        end
      end
    end
  end
end
