FactoryBot.define do
  factory :meth_tree_content, class: Meth::TreeContent do
    sequence(:name) { |n| 'Content_' + n.to_s }
    association :custom_field_desc, factory: :meth_custom_field_desc
  end
end
