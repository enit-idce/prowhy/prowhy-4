module Tools
  module Tasks
    class TaskPlanGlobal
      attr_accessor :current_user, :step_tool

      def initialize(c_user, step_tool_id)
        @current_user = c_user
        @step_tool = Meth::StepTool.find(step_tool_id)
      end

      def init_rcas(params)
        # binding.pry
        current_user.set_session(:active_step_tool, step_tool.id) if step_tool&.id

        rcas = params.to_unsafe_h[:rcas]
        rcas_string = ''
        rcas&.each_key do |k|
          rcas_string += k + ' '
        end
        # @rcas = rcas_string

        # step_tool.tool_options ||= {}
        # step_tool.tool_options[:plan_options] ||= {}
        # step_tool.tool_options[:plan_options][:rcas_ids] = rcas_string.split(' ')&.map(&:to_i) || []
        # step_tool.save!

        rca_ids = rcas_string.split(' ')&.map(&:to_i) || []
        current_user.set_session(:plan_rcas_ids, rca_ids)
      end
    end
  end
end
