class CreateProfils < ActiveRecord::Migration[6.0]
  def change
    create_table :profils do |t|
      t.string :profil_name
      t.string :name_fr
      t.string :name_en
      t.boolean :profil_locked

      t.timestamps
    end
  end
end
