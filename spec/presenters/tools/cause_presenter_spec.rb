require 'rails_helper'

RSpec.describe Tools::CausePresenter, type: 'presenter' do
  subject(:cause_presenter) { Tools::CausePresenter.new(cause, view) }

  let(:view) { ActionController::Base.new.view_context }
  let(:user) { FactoryBot.create(:user) }
  let(:cause) { FactoryBot.create(:cause, rca_id: rca.id) }
  let(:rca) { FactoryBot.create(:rca) }

  # before :all do
  #   I18n.locale = 'en'
  # end

  describe '#cause_tree_line' do
    it 'returns one cause tree line text' do
      cause.name = 'Le nom de La Cause'
      cause.status_validated!
      cause.save!
      result = cause_presenter.cause_tree_line

      expect(result).to include('le nom de la cause')
      # supprimé du presenter.
      # expect(result).to include('icon-status-validated') # I18n.t(:validated, scope: [:simple_form, :options, :tools_cause, :status])) # 'validated')
      expect(result).to include(cause.level || '')

      expect(result).to include("data-method=\"delete\" href=\"/en/tools/causes/#{cause.id}\"")
      expect(result).to include('href="/en/tools/causes/new?')
      expect(result).to include("href=\"/en/tools/causes/#{cause.id}/edit\"")
    end
  end

  describe '#cause_tree_infos' do
    it 'returns json infos for one tree cause' do
      cause.status_validated!
      cause.save!

      result = cause_presenter.cause_tree_infos
      expect(result[:text]).to eq(cause_presenter.cause_tree_line)
      expect(result[:id]).to eq cause.id
      expect(result[:type]).to eq 'validated'
    end
  end

  describe '#cause_subtree_datas' do
    before :each do
      @cause1 = FactoryBot.create(:cause, rca_id: rca.id, name: 'cause1', parent: cause)
      @cause2 = FactoryBot.create(:cause, rca_id: rca.id, name: 'cause2', parent: cause)
    end

    it 'returns json infos for cause_subtree_datas' do
      result = cause_presenter.cause_subtree_datas

      expect(result[:text]).to eq(cause_presenter.cause_tree_line)
      expect(result[:children].size).to eq 2
      expect(result[:children][0][:text]).to include('cause1')
      expect(result[:children][1][:text]).to include('cause2')
    end
  end
end
