module Tools
  class CheckEfficacityController < ApplicationController
    # TODO: Changer partout Task par Element pour permettre Efficacity sur autre chose (relation polymorphique dans le modèle Efficacity)
    include ProcessConcern

    before_action :init_rca, only: [:index]

    def index
      # authorize Rca
      skip_authorization

      redirect_to rcas_path and return if @rca.nil? || current_user.get_session(:active_step_tool).nil?

      # authorize @rca # TODO : à améliorer
      authorize @rca, :show?, policy_class: Tools::ToolsPolicy

      # Get datas
      # Get options to get datas
      @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
      task_options = @step_tool.tool_options[:task_options] || {}
      task_options = if @step_tool.tool_reference != 0
                       task_options.merge!(tool_reference: @step_tool.tool_reference,
                                           tool_num: @step_tool.tool_num,
                                           ancestry: nil)
                     else
                       task_options.merge!(ancestry: nil)
                     end
      more_options = @step_tool.tool_options[:more_options] || {}

      # Load tasks : more_options = where matrix_status => Implemented
      @tools_tasks = Tools::TaskQuery.relation.rca_tasks_by_column(@rca.id, task_options, {})
                                     .includes(:efficacity)
                                     .left_joins(:matrix_stat)
                                     .where(more_options)
      # .where(tools_matrix_stats: {matrix_status: 1})
      # binding.pry

      # Create efficacity if it does not exist
      @tools_tasks.each do |tools_task|
        next if tools_task.efficacity

        tools_task.create_efficacity
      end

      # Show Tool
      respond_to do |format|
        format.html { render 'index' }
      end
    end

    # GET /tools/edit_efficacity
    def edit_efficacity
      skip_authorization

      @tools_task = Tools::Task.find(params[:id])
      render :edit_efficacity
    end

  end
end
