module Tools
  class CauseTreePolicy < ToolsPolicy
    attr_reader :user, :record, :rca

    def index?
      show?
    end

    def index_5why?
      index?
    end

    def cause_trees_js_tree_data?
      show?
    end
  end
end
