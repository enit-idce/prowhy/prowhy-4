class CreateMethMethodologies < ActiveRecord::Migration[6.0]
  def change
    create_table :meth_methodologies do |t|
      t.string :name

      t.timestamps
    end
  end
end
