module Tools
  class IndicatorPolicy < ToolsPolicy
    attr_reader :user, :record, :rca

    def save_image?
      index?
    end
  end
end
