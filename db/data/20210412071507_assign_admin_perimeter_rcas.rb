class AssignAdminPerimeterRcas < ActiveRecord::Migration[6.0]
  class MigrationAdminPerimeter < ApplicationRecord
    self.table_name = :admin_perimeters
  end

  class MigrationAdminPerimeterRca < ApplicationRecord
    self.table_name = :admin_perimeter_rcas
  end

  class MigrationRca < ApplicationRecord
    self.table_name = :rcas
  end

  def up
    perimeter = MigrationAdminPerimeter.where(ancestry: nil).first
    if perimeter
      say 'Assign first perimeter to all rcas.'
      MigrationRca.all.each do |rca|
        MigrationAdminPerimeterRca.create!(rca_id: rca.id, perimeter_id: perimeter.id)
      end
    else
      say 'First perimeter do not exists : creates nothing.'
    end
  end

  def down
    say 'Do not destroy rcas perimeter links.'
  end
end
