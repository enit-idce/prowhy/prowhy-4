require 'rails_helper'

RSpec.describe 'tools/rca_reports', type: :request do
  let(:rca) { FactoryBot.create(:rca, methodology: methodology) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:user) { FactoryBot.create(:user) }

  context 'From logged user' do
    before :each do
      user.set_session(:active_step_tool, methodology.steps.first.step_tools.first.id)
      user.save!

      sign_in user
    end

    describe 'GET /index' do
      context 'without rca defined' do
        it 'renders a redirection response' do
          get tools_rca_reports_url

          expect(response).to have_http_status(:redirect)
          expect(response).to redirect_to(rcas_path)
        end
      end

      context 'with rca and step_tool defined' do
        it 'renders a successful response' do
          get('/tools/rca_reports', params: {rca: rca.id})
          expect(response).to be_successful
        end
      end
    end
  end
end
