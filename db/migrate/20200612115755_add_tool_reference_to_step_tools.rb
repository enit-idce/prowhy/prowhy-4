class AddToolReferenceToStepTools < ActiveRecord::Migration[6.0]
  def change
    add_column :meth_step_tools, :tool_reference, :integer, default: 1, after: :tool_id
  end
end
