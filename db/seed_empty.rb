ActiveRecord::Base.establish_connection
ActiveRecord::Base.connection.execute('SET FOREIGN_KEY_CHECKS = 0')

ActiveRecord::Base.connection.execute('TRUNCATE action_text_rich_texts')
ActiveRecord::Base.connection.execute('TRUNCATE active_storage_attachments')
ActiveRecord::Base.connection.execute('TRUNCATE active_storage_blobs')
ActiveRecord::Base.connection.execute('TRUNCATE ar_internal_metadata')
ActiveRecord::Base.connection.execute('TRUNCATE admin_admin_configs')
ActiveRecord::Base.connection.execute('TRUNCATE admin_helper_texts')
ActiveRecord::Base.connection.execute('TRUNCATE admin_ldap_configs')
ActiveRecord::Base.connection.execute('TRUNCATE admin_perimeters')
ActiveRecord::Base.connection.execute('TRUNCATE admin_perimeter_rcas')
ActiveRecord::Base.connection.execute('TRUNCATE admin_perimeter_users')
ActiveRecord::Base.connection.execute('TRUNCATE custom_fields')
ActiveRecord::Base.connection.execute('TRUNCATE labels_cause_types')
ActiveRecord::Base.connection.execute('TRUNCATE labels_criteria')
ActiveRecord::Base.connection.execute('TRUNCATE labels_values')
ActiveRecord::Base.connection.execute('TRUNCATE labels_configs')
ActiveRecord::Base.connection.execute('TRUNCATE meth_activities')
ActiveRecord::Base.connection.execute('TRUNCATE meth_custom_field_descs')
ActiveRecord::Base.connection.execute('TRUNCATE meth_custom_field_types')
ActiveRecord::Base.connection.execute('TRUNCATE meth_list_contents')
ActiveRecord::Base.connection.execute('TRUNCATE meth_methodologies')
ActiveRecord::Base.connection.execute('TRUNCATE meth_steps')
ActiveRecord::Base.connection.execute('TRUNCATE meth_step_tools')
ActiveRecord::Base.connection.execute('TRUNCATE meth_tools')
ActiveRecord::Base.connection.execute('TRUNCATE meth_tool_custom_fields')
ActiveRecord::Base.connection.execute('TRUNCATE meth_step_roles')
ActiveRecord::Base.connection.execute('TRUNCATE posts')
ActiveRecord::Base.connection.execute('TRUNCATE rcas')
ActiveRecord::Base.connection.execute('TRUNCATE rca_advances')
ActiveRecord::Base.connection.execute('TRUNCATE taggings')
ActiveRecord::Base.connection.execute('TRUNCATE tags')
ActiveRecord::Base.connection.execute('TRUNCATE tools_causes')
ActiveRecord::Base.connection.execute('TRUNCATE tools_cause_classes')
ActiveRecord::Base.connection.execute('TRUNCATE tools_cause_links')
ActiveRecord::Base.connection.execute('TRUNCATE tools_cause_trees')
ActiveRecord::Base.connection.execute('TRUNCATE tools_criticities')
ActiveRecord::Base.connection.execute('TRUNCATE tools_criticity_values')
ActiveRecord::Base.connection.execute('TRUNCATE tools_crit_dec_mats')
ActiveRecord::Base.connection.execute('TRUNCATE tools_decision_mats')
ActiveRecord::Base.connection.execute('TRUNCATE tools_dec_mat_links')
ActiveRecord::Base.connection.execute('TRUNCATE tools_dec_mat_vals')
ActiveRecord::Base.connection.execute('TRUNCATE tools_documents')
ActiveRecord::Base.connection.execute('TRUNCATE tools_efficacities')
ActiveRecord::Base.connection.execute('TRUNCATE tools_images')
ActiveRecord::Base.connection.execute('TRUNCATE tools_indicators')
ActiveRecord::Base.connection.execute('TRUNCATE tools_indicator_measures')
ActiveRecord::Base.connection.execute('TRUNCATE tools_ishikawas')
ActiveRecord::Base.connection.execute('TRUNCATE tools_ishi_branchs')
ActiveRecord::Base.connection.execute('TRUNCATE tools_matrix_stats')
ActiveRecord::Base.connection.execute('TRUNCATE tools_qqoqcps')
ActiveRecord::Base.connection.execute('TRUNCATE tools_tasks')
ActiveRecord::Base.connection.execute('TRUNCATE tools_task_validations')
ActiveRecord::Base.connection.execute('TRUNCATE tools_teams')
ActiveRecord::Base.connection.execute('TRUNCATE users')
ActiveRecord::Base.connection.execute('TRUNCATE users_roles')
ActiveRecord::Base.connection.execute('TRUNCATE people')
ActiveRecord::Base.connection.execute('TRUNCATE profils')
ActiveRecord::Base.connection.execute('TRUNCATE profil_authorizes')
ActiveRecord::Base.connection.execute('TRUNCATE roles')

ActiveRecord::Base.connection.execute('SET FOREIGN_KEY_CHECKS = 1')
