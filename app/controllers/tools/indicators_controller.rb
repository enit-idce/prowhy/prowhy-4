module Tools
  class IndicatorsController < ApplicationController
    include ProcessConcern

    before_action :set_tools_indicator, only: [:show, :edit, :update, :destroy, :save_image]
    before_action :init_rca, only: [:index, :new, :create]

    # GET /tools/indicators
    # GET /tools/indicators.json
    def index
      # authorize Indicator
      skip_authorization

      if @rca.nil? || current_user.get_session(:active_step_tool).nil?
        redirect_to rcas_path
      else
        authorize @rca, :show?, policy_class: Tools::ToolsPolicy

        @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
        @step_tool.tool_options ||= {}

        @tools_indicators = Tools::Indicator.search_default(@rca, 'tools_indicators', @step_tool).includes(:indicator_measures)
        # @rca.tools_indicators.where(tool_reference: @step_tool.tool_reference, tool_num: @step_tool.tool_num)

        show_measures = params[:show_measures] || 'true'
        show_graph = params[:show_graph] || 'true'
        show_buttons = params[:show_buttons] || 'true'

        render :index, locals: {show_buttons: show_buttons, show_graph: show_graph, show_measures: show_measures}
      end
    end

    # GET /tools/indicators/1
    # GET /tools/indicators/1.json
    def show
    end

    # GET /tools/indicators/new
    def new
      @tools_indicator = Tools::Indicator.new(tools_indicator_params)
      authorize @tools_indicator
    end

    # GET /tools/indicators/1/edit
    def edit
      render :new
    end

    # POST /tools/indicators
    # POST /tools/indicators.json
    def create
      @tools_indicator = Tools::Indicator.new(tools_indicator_params)
      authorize @tools_indicator

      respond_to do |format|
        if @tools_indicator.save
          format.html { redirect_to tools_indicators_url(rca: @rca), notice: 'Indicator was successfully created.' }
          format.json { render :show, status: :created, location: @tools_indicator }
        else
          format.js   { render :new }
          format.html { render :new }
          format.json { render json: @tools_indicator.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /tools/indicators/1
    # PATCH/PUT /tools/indicators/1.json
    def update
      respond_to do |format|
        if @tools_indicator.update(tools_indicator_params)
          format.html { redirect_to tools_indicators_url(rca: @rca), notice: 'Indicator was successfully updated.' }
          format.json { render :show, status: :ok, location: @tools_indicator }
        else
          format.js   { render :new }
          format.html { render :edit }
          format.json { render json: @tools_indicator.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /tools/indicators/1
    # DELETE /tools/indicators/1.json
    def destroy
      @tools_indicator.destroy

      respond_to do |format|
        format.html { redirect_to tools_indicators_url(rca: @rca), notice: 'Indicator was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    # POST save_image
    def save_image
      # authorize Indicator

      Tools::SaveImageService.new(current_user).save_image_dataurl({image: params[:image], subdir: 'indicators', filename: params[:filename]})

      render plain: 'OK'
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_indicator
      @tools_indicator = Tools::Indicator.find(params[:id])
      @rca = @tools_indicator.rca

      authorize @tools_indicator
    end

    # Only allow a list of trusted parameters through.
    def tools_indicator_params
      params.require(:tools_indicator).permit(:rca_id, :tool_reference, :tool_num, :name, :description, :target,
                                              graph_options: [:type, :color])
    end
  end
end
