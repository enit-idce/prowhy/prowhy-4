class DataChangeUserPrefsToJson < ActiveRecord::Migration[6.0]
  class MigrationUser < ApplicationRecord
    self.table_name = :users

    def set_preference(key, value)
      self.preferences ||= {}
      self.preferences[key.to_s] = value
      save!
    end

    def del_preference(key)
      self.preferences&.except!(key.to_s)
      save!
    end
  end

  def up
    # admin_config = Admin::AdminConfig.first
    # if admin_config&.default_perimeter
    #   say 'Add defaut perimeter to all users.'
    #   MigrationUser.all.each do |user|
    #     user.set_preference(:perimeter, admin_config.default_perimeter.id)
    #   end
    # else
    #   say 'No default perimeter to add.'
    # end
  end

  def down
    # say 'Del default perimeter from user preferences.'

    # MigrationUser.all.each do |user|
    #   user.del_preference(:perimeter) if user.preferences
    # end
    # raise ActiveRecord::IrreversibleMigration
  end
end
