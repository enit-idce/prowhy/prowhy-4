FactoryBot.define do
  factory :meth_custom_field_desc, class: Meth::CustomFieldDesc do
    sequence(:internal_name) { |n| 'custom_' + n.to_s }
    sequence(:name, 'a') { |n| 'name_' + n }
    association :custom_field_type, factory: :meth_custom_field_type

    trait :type_rich_text do
      association :custom_field_type, factory: [:meth_custom_field_type, :type_rich_text]
    end

    trait :type_tag do
      association :custom_field_type, factory: [:meth_custom_field_type, :type_tag]
    end

    trait :type_select do
      association :custom_field_type, factory: [:meth_custom_field_type, :type_select]
    end

    trait :type_tree do
      association :custom_field_type, factory: [:meth_custom_field_type, :type_tree]
    end
  end
end
