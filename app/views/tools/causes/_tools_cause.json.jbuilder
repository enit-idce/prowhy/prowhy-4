json.extract! tools_cause, :id, :rca_id, :tool_reference, :name, :ancestry, :level, :status, :created_at, :updated_at
json.url tools_cause_url(tools_cause, format: :json)
