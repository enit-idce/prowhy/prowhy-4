require 'rails_helper'

RSpec.describe Tools::IndicatorsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/tools/indicators').to route_to('tools/indicators#index')
    end

    it 'routes to #new' do
      expect(get: '/tools/indicators/new').to route_to('tools/indicators#new')
    end

    it 'routes to #show' do
      expect(get: '/tools/indicators/1').to route_to('tools/indicators#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/tools/indicators/1/edit').to route_to('tools/indicators#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/tools/indicators').to route_to('tools/indicators#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/tools/indicators/1').to route_to('tools/indicators#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/tools/indicators/1').to route_to('tools/indicators#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/tools/indicators/1').to route_to('tools/indicators#destroy', id: '1')
    end
  end
end
