module Tools
  class CauseTreesController < ApplicationController
    include ProcessConcern
    before_action :set_tools_cause_tree, only: [:show, :edit, :update, :destroy]
    before_action :init_rca, only: [:index, :index_5why]

    # GET /tools/cause_trees
    # GET /tools/cause_trees.json
    def index
      # authorize CauseTree
      skip_authorization
      # authorize @rca, :show?

      if @rca.nil? || current_user.get_session(:active_step_tool).nil?
        redirect_to rcas_path
      elsif params[:id].nil?
        redirect_to edit_rca_path(@rca)
      else
        # @cause_tree = params[:id]
        set_tools_cause_tree
        set_ishikawas_list
      end
    end

    # Création des données JSon pour jstree
    def cause_trees_js_tree_data
      if params[:cause_tree]
        @cause_tree = Tools::CauseTree.find(params[:cause_tree].to_i)
        set_ishikawas_list

        authorize @cause_tree

        @cause = @cause_tree.cause

        data = []
        # binding.pry
        cause_presenter = Tools::CausePresenter.new(@cause, view_context)
        data << cause_presenter.cause_subtree_datas(@ishikawas)
      else
        # authorize CauseTree
        skip_authorization

        data = [{text: 'no tree'}]
      end
      render json: data
    end

    def index_5why
      # authorize CauseTree
      skip_authorization

      if @rca.nil? || current_user.get_session(:active_step_tool).nil?
        redirect_to rcas_path
      elsif params[:id].nil?
        redirect_to edit_rca_path(@rca)
      else
        set_tools_cause_tree
      end
    end

    # private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_cause_tree
      @cause_tree = Tools::CauseTree.find(params[:id])

      authorize @cause_tree
    end

    def set_ishikawas_list
      # @ishikawas = []
      # @ishikawa_names = []
      # @cause_tree.rca.methodology.steps.each do |step|
      #   step.step_tools.each do |step_tool|
      #     next unless step_tool.tool.tool_name == 'Ishikawa' && step_tool.tool_reference == @cause_tree.tool_reference

      #     ishikawa = @cause_tree.rca.find_or_create_tool('tools_ishikawas', 'Tools::Ishikawa', step_tool)
      #     @ishikawas << ishikawa
      #     @ishikawa_names << step_tool.name
      #   end
      # end

      @ishikawas, @ishikawa_names = @cause_tree.cause_link.cause.init_ishikawas_list if @cause_tree&.cause_link&.cause
      @ishikawas ||= []
      @ishikawa_names ||= []
    end

    # Only allow a list of trusted parameters through.
    # def tools_cause_tree_params
    #   params.require(:tools_cause_tree).permit(:rca_id, :tool_reference, :tool_num)
    # end
  end
end
