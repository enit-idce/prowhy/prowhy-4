require 'doorkeeper/grape/helpers'
require 'devise'

module ProwhyApi
  module V1
    module ApiTools
      class ApiTeams < V1::Api
        # before do
        #   doorkeeper_authorize! unless user_signed_in?
        # end

        resource :teams do
          desc 'Return Team associated to Rca #rca_id.'
          params do
            requires :rca_id, type: Integer, desc: 'Rca ID.'
            optional :tool_reference, type: Integer, desc: 'Team reference (default=1).', default: 1
            optional :tool_num, type: Integer, desc: 'Team num (default=1).', default: 1
          end
          get do
            @rca = Rca.find(params[:rca_id])
            raise ActiveRecord::RecordNotFound unless @rca

            authorize @rca, :show?

            @teams = Tools::Team.search_default(@rca, '', Meth::StepTool.new(tool_reference: params[:tool_reference] || 1, tool_num: params[:tool_num] || 1)) # .order(:profil_id)

            present @teams, with: Entities::ApiTools::Team
          end

          desc 'Create a Team (a person with a profil in a Rca).'
          params do
            requires :rca_id, type: Integer, desc: 'Rca ID.', documentation: {param_type: 'body'}
            requires :tool_reference, type: Integer, desc: 'Profil ID.', default: 1, documentation: {param_type: 'body'}
            requires :tool_num, type: Integer, desc: 'Profil ID.', default: 1, documentation: {param_type: 'body'}
            requires :profil_id, type: Integer, desc: 'Profil ID.', documentation: {param_type: 'body'}
            requires :person_id, type: Integer, desc: 'Person ID (Att : from People table, not Users).', documentation: {param_type: 'body'}
          end
          post do
            @team = Tools::Team.new(params)
            authorize @team, :create?
            @team.save!

            present @team, with: Entities::ApiTools::Team
          end
        end
      end
    end
  end
end
