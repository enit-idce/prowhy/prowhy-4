require_dependency 'tools'

module Tools
  class Criticity < ApplicationRecord
    include ToolConcern
    # belongs_to :rca
    belongs_to :labels_config, class_name: 'Labels::Config'
    has_many :criticity_values, dependent: :destroy

    accepts_nested_attributes_for :criticity_values

    def self.create_default!(_options, _step_tool)
      []
    end

    # def self.search_default(rca, _tool_name, step_tool)
    # end

    def self.create_search_multiple?
      return true
    end

    def create_criticity_values
      labels_config.criteria.each do |criterium|
        Tools::CriticityValue.create!(criticity: self, labels_criterium: criterium)
      end
    end
  end
end
