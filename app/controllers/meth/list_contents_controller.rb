require 'csv'

module Meth
  class ListContentsController < ApplicationController
    include AdminConcern
    # layout 'admin'
    before_action :set_meth_list_content, only: [:show, :edit, :update, :destroy, :list_content_up, :list_content_down]

    # GET /list_contents
    # GET /list_contents.json
    def index
      authorize ListContent
      list_all_contents(params)

      respond_to do |format|
        format.html { render 'index' }
        format.js { render 'index' }
      end
    end

    # GET /list_contents/1
    # GET /list_contents/1.json
    def show
    end

    # GET /list_contents/new
    def new
      @list_content = if params[:meth_list_content]
                        ListContent.new(meth_list_content_params)
                      else
                        ListContent.new
                      end
      authorize @list_content
    end

    # GET /list_contents/1/edit
    def edit
    end

    # POST /list_contents
    # POST /list_contents.json
    def create
      @list_content = ListContent.new(meth_list_content_params)
      authorize @list_content

      respond_to do |format|
        if @list_content.save
          format.html { redirect_to @list_content, notice: t('.message_succes', list_content: Meth::ListContent.model_name.human) }
          format.json { render :show, status: :created, location: @list_content }
          format.js do
            list_all_contents(custom_field_desc: @list_content.custom_field_desc_id)
            render 'create'
          end
        else
          format.html { render :new }
          format.json { render json: @list_content.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /list_contents/1
    # PATCH/PUT /list_contents/1.json
    def update
      respond_to do |format|
        if @list_content.update(meth_list_content_params)
          format.html { redirect_to @list_content, notice: t('.message_succes', list_content: Meth::ListContent.model_name.human) }
          format.json { render :show, status: :ok, location: @list_content }
        else
          format.html { render :edit }
          format.json { render json: @list_content.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /list_contents/1
    # DELETE /list_contents/1.json
    def destroy
      list_contents = @list_content.custom_field_desc.list_contents
      @list_content.destroy
      Positions::MoveService.new(nil, list_contents).call('reinit')

      respond_to do |format|
        format.html { redirect_to meth_list_contents_url, notice: t('.message_succes', list_content: Meth::ListContent.model_name.human) }
        format.json { head :no_content }
      end
    end

    # PATCH
    def list_content_up
      list_contents = @list_content.custom_field_desc.list_contents
      Positions::MoveService.new(@list_content, list_contents).call('up')

      redirect_to meth_list_contents_path
    end

    def list_content_down
      list_contents = @list_content.custom_field_desc.list_contents
      Positions::MoveService.new(@list_content, list_contents).call('down')

      redirect_to meth_list_contents_path
    end

    # GET/NEW FROM CSV
    def new_from_csv
      authorize ListContent, :index?

      @custom_field_desc_id = params[:meth_list_content] ? params[:meth_list_content][:custom_field_desc_id] : nil
    end

    # POST/create from csv
    def create_from_csv
      authorize ListContent, :index?

      if params[:file_csv] && params[:custom_field_desc_id] # && !params[:file_csv].empty?
        file_read_original = File.read(params[:file_csv])
        file_read = file_read_original.encode(invalid: :replace, undef: :replace, replace: '%')

        # puts 'Encoding'.yellow
        # puts file_read_original.encoding.name
        # puts file_read.encoding.name

        # binding.pry
        datas = CSV.parse(file_read, headers: true, col_sep: params[:sep_csv] || ';')
        # binding.pry
        cf_desc = Meth::CustomFieldDesc.find(params[:custom_field_desc_id])

        # Dependences
        headers = datas.headers
        dep_field_name = headers.grep(/^custom_/)
        dep_field_name = dep_field_name.length ? dep_field_name[0] : nil
        cfdep = nil

        if dep_field_name
          cfdep = Meth::CustomFieldDesc.where(internal_name: dep_field_name)&.first

          if cfdep
            # puts "SAVE CFDEP !!!".green
            cf_desc.ref_field = cfdep
            cf_desc.save!
          end
        end
        # End dependences

        pos = cf_desc.list_contents.length+1
        datas.each do |row|
          if row['ID']
            list_content = Meth::ListContent.find(row['ID'].to_i)
            attributes = {} # custom_field_desc_id: cf_desc.id }
            puts "LIST CONTENT #{row['ID'].to_i} / CUSTOMFIELD DESC #{cf_desc.id}".yellow
          else
            attributes = { custom_field_desc_id: cf_desc.id, pos: pos }
            pos += 1
          end

          Meth::ListContent.locale_columns(:name).each do |name_l|
            l = name_l.to_s.split('_')[1]
            attributes[name_l] = (row[l] || row[l.upcase])
          end

          # Dependences
          attributes[:ref_value] = row[dep_field_name]&.to_i if cfdep
          # End dependences

          puts attributes.to_s.green
          # begin
          if list_content
            list_content.update(attributes)
          else
            Meth::ListContent.create!(attributes)
          end
          # rescue StandardError
          #   puts 'NO DATAS !'
          # end
        end
      end
      # binding.pry
      redirect_to meth_list_contents_url
    rescue StandardError => e
      redirect_to meth_list_contents_url, flash: { error: e.to_s + " \nCSV Format must be UTF-8 compatible." }
    end

    # GET/export csv
    # def export_csv
    #   authorize ListContent, :index?

    #   @custom_field_desc_id = params[:meth_list_content][:custom_field_desc_id]
    # end

    # def export_save_csv
    def export_csv
      authorize ListContent, :index?

      report_csv = ''
      cf_desc = params[:custom_field_desc_id] ? Meth::CustomFieldDesc.find(params[:custom_field_desc_id]) : nil

      if cf_desc # && !params[:file_csv].empty?
        cf_desc = Meth::CustomFieldDesc.find(params[:custom_field_desc_id])

        report_csv = CSV.generate(col_sep: ';') do |csv|
          line = ['ID']
          Meth::ListContent.locale_columns(:name).each do |name_l|
            line << name_l.to_s.split('_')[1].upcase
          end

          # Dependencies
          line << cf_desc.ref_field.internal_name if cf_desc.ref_field
          # End dependencies
          csv << line

          cf_desc.list_contents.each do |lc|
            line = [lc.id.to_s]
            Meth::ListContent.locale_columns(:name).each do |name_l|
              # n = lc.send(name_l)
              line << lc.send(name_l) # (n != '' ? n : lc.name)
            end

            # Dependencies
            line << lc.ref_value if cf_desc.ref_field && lc.ref_value
            # End dependencies

            csv << line
          end
        end
      end

      send_data report_csv, type: 'text/csv; charset=utf-8; header=present', disposition: 'attachment; filename=banana.csv', filename: "#{cf_desc&.internal_name || 'custom'}.csv"
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_meth_list_content
      @list_content = ListContent.find(params[:id])
      authorize @list_content
    end

    # Only allow a list of trusted parameters through.
    def meth_list_content_params
      params.require(:meth_list_content).permit(:custom_field_desc_id, :name, :name_fr, :name_en, :pos, :ref_value)
    end

    def list_all_contents(params)
      @custom_field_descs = if params[:custom_field_desc]
                              [CustomFieldDesc.find(params[:custom_field_desc])]
                            else
                              CustomFieldDesc.includes([:custom_field_type, :list_contents])
                                             .where(meth_custom_field_types: {field_internal: 'select'})
                            end
      # TODO: APPAREMMENT INUTILE DONC COMMENTE => A VERIFIER
      # @list_contents = if params[:custom_field_desc]
      #                    ListContent.where(custom_field_desc_id: params[:custom_field_desc]).order(:pos)
      #                  else
      #                    ListContent.all.order(:custom_field_desc_id).order(:pos)
      #                  end
    end
  end
end
