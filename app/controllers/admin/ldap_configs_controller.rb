module Admin
  class LdapConfigsController < ApplicationController
    include AdminConcern # à virer plus tard
    before_action :set_ldap_config, only: [:show, :edit, :update, :destroy, :ldap_config_test]

    # GET /ldap_configs
    # GET /ldap_configs.json
    def index
      authorize LdapConfig
      @ldap_configs = Admin::LdapConfig.all
    end

    # GET /ldap_configs/1
    # GET /ldap_configs/1.json
    # def show
    # end

    # GET /ldap_configs/new
    def new
      @ldap_config = Admin::LdapConfig.new
      authorize @ldap_config
    end

    # GET /ldap_configs/1/edit
    def edit
    end

    # POST /ldap_configs
    # POST /ldap_configs.json
    def create
      @ldap_config = Admin::LdapConfig.new(ldap_config_params)
      authorize @ldap_config

      respond_to do |format|
        if @ldap_config.save
          format.html { redirect_to admin_ldap_configs_url, notice: 'Ldap config was successfully created.' }
          format.json { render :show, status: :created, location: @ldap_config }
        else
          format.html { render :new }
          format.json { render json: @ldap_config.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /ldap_configs/1
    # PATCH/PUT /ldap_configs/1.json
    def update
      respond_to do |format|
        if @ldap_config.update(ldap_config_params)
          format.html { redirect_to admin_ldap_configs_url, notice: 'Ldap config was successfully updated.' }
          format.json { render :show, status: :ok, location: @ldap_config }
        else
          format.html { render :edit }
          format.json { render json: @ldap_config.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /ldap_configs/1
    # DELETE /ldap_configs/1.json
    def destroy
      @ldap_config.destroy
      respond_to do |format|
        format.html { redirect_to admin_ldap_configs_url, notice: 'Ldap config was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    # Test connexion
    def ldap_config_test
      begin
        @ldap_config.connect_test
        @message = 'Connection succeed !'
      rescue StandardError => e
        @message =  "Connexion failed : #{e.message}"
      end
      respond_to do |format|
        format.js { render 'ldap_config_message' }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_ldap_config
      @ldap_config = Admin::LdapConfig.find(params[:id])
      authorize @ldap_config
    end

    # Only allow a list of trusted parameters through.
    # def ldap_config_params
    #   params.fetch(:ldap_config, {})
    # end
    # Only allow a list of trusted parameters through.
    def ldap_config_params
      params.require(:admin_ldap_config).permit(:name, :host, :port, :base_dn, :admin_login, :admin_password,
                                                :attr_uid, :attr_firstname, :attr_lastname, :attr_email, :attr_adress, :attr_phone,
                                                :authorize_signup)
    end
  end
end
