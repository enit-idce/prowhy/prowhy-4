require 'rails_helper'

RSpec.describe ProfilAuthorizesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/profil_authorizes').to route_to('profil_authorizes#index')
    end

    it 'routes to #update via PUT' do
      expect(put: '/profil_authorizes/1').to route_to('profil_authorizes#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/profil_authorizes/1').to route_to('profil_authorizes#update', id: '1')
    end
  end
end
