require 'rails_helper'

RSpec.describe 'Roles', type: :request do
  let(:valid_attributes) { { name: 'le_role'} }
  let(:invalid_attributes) { { name: nil } }

  before :each do
    Role.create!([{id: 1, name: 'super_admin'},
                  {id: 2, name: 'admin'},
                  {id: 3, name: 'manager'},
                  {id: 4, name: 'user'},
                  {id: 5, name: 'visitor'}])
    @user = FactoryBot.create(:user, id: 1)
    @user2 = FactoryBot.create(:user, id: 2)
    @user3 = FactoryBot.create(:user, id: 3)
    @user4 = FactoryBot.create(:user, id: 4)
    sign_in @user
  end

  describe 'GET /index' do
    it 'renders a successful response' do
      get roles_url
      expect(response).to be_successful
    end

    it 'renders a successful response with by_user option' do
      get roles_url(by_user_role: 'by_user')
      expect(response).to be_successful
    end
  end

  describe 'POST /create : associate users to role manager' do
    context 'by role' do
      it 'assigns new role to users' do
        post roles_url, params: { attrib_role: 3, users_ids: [1, 3] }

        expect(@user.has_role?(:manager)).to be true
        expect(@user2.has_role?(:manager)).to be false
        expect(@user3.has_role?(:manager)).to be true
        expect(@user4.has_role?(:manager)).to be false
      end

      it 'removes role to users' do
        @user.add_role :manager
        @user3.add_role :manager
        post roles_url, params: { attrib_role: 3, users_ids: [2, 3] }

        expect(@user.has_role?(:manager)).to be false
        expect(@user2.has_role?(:manager)).to be true
        expect(@user3.has_role?(:manager)).to be true
        expect(@user4.has_role?(:manager)).to be false
      end

      it 'redirects to the roles list with current role opened' do
        post roles_url, params: { attrib_role: 3, users_ids: [1, 2, 3] }

        expect(response).to redirect_to(roles_url(attrib_role: 3, by_user_role: 'by_role'))
      end
    end

    context 'by user' do
      it 'assigns new roles to one user' do
        post roles_url, params: { by_user_role: 'by_user', attrib_user: 1, roles_ids: [2, 4] }

        expect(@user.has_role?(:super_admin)).to be false
        expect(@user.has_role?(:admin)).to be true
        expect(@user.has_role?(:manager)).to be false
        expect(@user.has_role?(:user)).to be true
        expect(@user.has_role?(:visitor)).to be false
      end
    end
  end
end
