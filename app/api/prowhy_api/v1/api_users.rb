require 'doorkeeper/grape/helpers'
require 'devise'

module ProwhyApi
  module V1
    class ApiUsers < Grape::API
      resource :users do
        desc 'Return all Users.'
        params do
          optional :role, type: String, desc: 'Option : Get users with role :role.'
        end
        get do
          @users = if params[:role]
                     User.with_role(params[:role])
                   else
                     User.all
                   end

          present @users, with: Entities::User, with_id: true, with_person: true
        end

        desc 'Set default locale for current user (connected user).'
        params do
          requires :locale, type: String, default: 'fr', desc: 'Language for custom fields : fr for French / en for English', documentation: {param_type: 'body'}
        end
        patch :locale do
          parsed_locale = I18n.available_locales.map(&:to_s).include?(params[:locale]) ? params[:locale] : nil

          raise ArgumentError, "Invalid locale. Valid locales are #{I18n.available_locales.map(&:to_s)}" unless parsed_locale

          current_user&.set_preference(:locale, parsed_locale)

          { locale: parsed_locale }
        end

        desc 'Get default locale for current user (connected user).'
        get :locale do
          locale = current_user&.get_preference(:locale)

          { locale: locale }
          # { locale: !locale.to_s.empty? ? locale : I18n.default_locale }
        end

        desc 'Get all available locales.'
        get :locales do
          { locales: I18n.available_locales }
        end
      end
    end
  end
end
