class CreateToolsFileVersions < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_file_versions do |t|
      t.references :element, polymorphic: true, null: false
      t.datetime :date
      t.references :author, foreign_key: { to_table: :people }

      # à ajouter si on souhaite lier à un model ou le attached ne s'appelle pas file
      # t.string :element_name, default: 'file'
    end
  end
end
