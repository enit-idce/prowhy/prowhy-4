FactoryBot.define do
  factory :ishikawa, class: Tools::Ishikawa do
    tool_reference { 1 }
    tool_num { 1 }
  end
end
