class CreateToolsDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_documents do |t|
      t.references :rca, null: false, foreign_key: true
      t.integer :tool_reference
      t.integer :tool_num, default: 1
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
