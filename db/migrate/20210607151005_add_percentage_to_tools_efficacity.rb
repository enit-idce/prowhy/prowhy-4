class AddPercentageToToolsEfficacity < ActiveRecord::Migration[6.0]
  def change
    add_column :tools_efficacities, :percentage, :integer, after: :check_status, null: true
  end
end
