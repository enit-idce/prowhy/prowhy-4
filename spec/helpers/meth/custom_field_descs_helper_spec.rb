require 'rails_helper'

RSpec.describe Meth::CustomFieldDescsHelper, type: :helper do
  let(:custom_field_desc) { FactoryBot.create(:meth_custom_field_desc, custom_field_type_id: custom_field_type.id) }
  let(:custom_field_type) { FactoryBot.create(:meth_custom_field_type) }

  describe '#custom_field_desc_used' do
    context 'when it is not used' do
      it 'returns an empty list of steps using the custom field with count = 0' do
        helper_content = helper.custom_field_desc_used(custom_field_desc)

        expect(helper_content).to include('0 ')
      end
    end

    context 'when it is used' do
      before :each do
        @methodology = FactoryBot.create(:methodology, :with_step_info)
        @step1 = FactoryBot.create(:meth_step, methodology_id: @methodology.id)
        @step1.add_tool(FactoryBot.create(:meth_tool, :custom_field))
        FactoryBot.create(:meth_tool_custom_field, step_tool_id: @step1.step_tools.first.id, custom_field_desc_id: custom_field_desc.id)
      end
      it 'returns an list of steps using the custom field with count = 0' do
        helper_content = helper.custom_field_desc_used(custom_field_desc)

        expect(helper_content).to include('1 ')
        expect(helper_content).to include(@methodology.name)
        expect(helper_content).to include(@step1.name)
      end
    end
  end

  describe '#custom_field_desc_name_edit' do
    context 'when custom field type is not a select' do
      it 'returns the custom_field_desc name' do
        helper_content = helper.custom_field_desc_name_edit(custom_field_desc)

        expect(helper_content).to eq(custom_field_type.name)
      end
    end

    context 'when custom field type is a select with 2 list content' do
      let(:custom_field_type2) { FactoryBot.create(:meth_custom_field_type, :type_select) }
      before :each do
        custom_field_desc.custom_field_type = custom_field_type2
        custom_field_desc.save!
        FactoryBot.create(:meth_list_content, custom_field_desc_id: custom_field_desc.id)
        FactoryBot.create(:meth_list_content, custom_field_desc_id: custom_field_desc.id)
      end
      it 'returns custom field desc name and the content list names' do
        helper_content = helper.custom_field_desc_name_edit(custom_field_desc)

        expect(helper_content).to include(custom_field_type2.name)
        expect(helper_content).to include('Content_1')
        expect(helper_content).to include('Content_2')
      end
    end
  end
end
