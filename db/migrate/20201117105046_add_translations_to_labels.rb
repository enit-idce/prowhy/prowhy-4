class AddTranslationsToLabels < ActiveRecord::Migration[6.0]
  def change
    # Labels::CauseType
    add_column :labels_cause_types, :name_fr, :string, after: :name
    add_column :labels_cause_types, :name_en, :string, after: :name_fr

    Labels::CauseType.all.each do |lct|
      lct.name_fr = lct.name
      lct.save!
    end

    remove_column :labels_cause_types, :name

    # Labels::Criteria
    add_column :labels_criteria, :name_fr, :string, after: :name
    add_column :labels_criteria, :name_en, :string, after: :name_fr

    Labels::Criterium.all.each do |lct|
      lct.name_fr = lct.name
      lct.save!
    end

    remove_column :labels_criteria, :name

    add_column :labels_criteria, :description_fr, :text, after: :name_en
    add_column :labels_criteria, :description_en, :text, after: :description_fr
  end
end
