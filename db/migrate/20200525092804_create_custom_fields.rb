class CreateCustomFields < ActiveRecord::Migration[6.0]
  def change
    create_table :custom_fields do |t|
      t.references :rca, null: false, foreign_key: true

      t.timestamps
    end
  end
end
