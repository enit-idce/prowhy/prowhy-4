# frozen_string_literal: true

require 'rubygems'
require 'net/ldap'

host = 'ldap.xxx.fr'
port = 389
base_dn = 'ou=people,dc=xxx,dc=fr'
admin_login = ''
admin_password = ''

options = if admin_login && admin_password && admin_login != ''
            {
              host: host,
              port: port,
              auth: {
                method: :simple,
                username: admin_login,
                password: admin_password
              }
            }
          else
            {
              host: host,
              port: port
            }
          end
ldap = Net::LDAP.new options

puts 'TEST CONNEXION ET AUTHENTIFICATION :'
if ldap.bind
  puts 'Connexion & Authentification ok'

  puts 'TEST SEARCH USER'

  filter = nil
  # filter = Net::LDAP::Filter.eq('uid', 'e*')	# Recherche sur le login
  # filter = Net::LDAP::Filter.eq('sn','D*')	# Recherche sur le NOM
  # filter = Net::LDAP::Filter.eq('givenname','E*')	# Recherche sur le PRENOM
  # filter = Net::LDAP::Filter.eq('mail','e*')	# Recherche sur le EMAIL

  treebase = base_dn
  puts '---------------------------------------------------------------------------'
  count_result = 0
  ldap.search(base: treebase, filter: filter) do |entry|
    count_result += 1

    next if count_result > 5

    puts "DN: #{entry.dn}"
    entry.each do |attr, values|
      puts "#{attr}:"
      values.each do |value|
        puts "    ---> #{value}"
      end
    end
    puts '--------------------------------------------------------------------------'
  end

  puts "NB Entries = #{count_result}"
else
  puts 'Connexion failed'
end
