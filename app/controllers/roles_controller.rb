class RolesController < ApplicationController
  include AdminConcern
  # before_action :set_role, only: [:show, :edit, :update, :destroy]
  before_action :set_attrib_role, only: [:index, :create]

  # GET /roles
  # GET /roles.json
  def index
    authorize Role
    @roles = Role.all
    @users = User.all

    set_attrib_role
    set_attrib_user

    if params[:by_user_role] == 'by_user'
      @roles_in = @attrib_user.roles
    else
      @users_in = User.with_role(@attrib_role.name)
    end
    # # if @step_tool.tool_options[:labels]
    # @users.each do |user|
    #   @users_in << user if user.has_role? @attrib_role.name
    # end

    respond_to do |format|
      format.html { render :index, locals: {by_user_role: params[:by_user_role] || 'by_role'} }
      # format.js { render js: "window.location='#{roles_path(attrib_role: @attrib_role.id)}'" }
      # format.js { render html: 'index' }
    end
  end

  # # GET /roles
  # # GET /roles.json
  # def index_by_user
  #   authorize Role

  #   @roles = Role.all
  #   @users = User.all

  #   set_attrib_role
  #   set_attrib_user

  #   @roles_in = @attrib.user.roles

  #   respond_to do |format|
  #     format.html { render :index, locals: {by_user_role: params[:by_user_role] || 'by_user'} }
  #     # format.js { render js: "window.location='#{roles_path(attrib_role: @attrib_role.id)}'" }
  #     # format.js { render html: 'index' }
  #   end
  # end

  # POST
  def create
    authorize Role

    # binding.pry
    if params[:attrib_role]
      associate_users_to_role
      by_user_role = 'by_role'
    else
      associate_roles_to_users
      by_user_role = 'by_user'
    end

    redirect_to roles_url(attrib_role: @attrib_role&.id, attrib_user: @attrib_user&.id, by_user_role: by_user_role)
  end

  private

  def associate_users_to_role
    set_attrib_role
    users_in = params&.[]('users_ids')&.map(&:to_i)

    # Ajout des utilisateurs sélectionnés :
    users_in&.each do |user_id|
      authorize @attrib_role, :add_role?
      User.find(user_id)&.add_role(@attrib_role.name)
    end

    # Suppression des utilisateurs non sélectionnés :
    # TODO : ne pas supprimer le role si seul super_admin !!! => géré dans Pundit RolePolicy
    users_with_role = User.with_role(@attrib_role.name)
    users_with_role&.each do |user|
      next if users_in&.include?(user.id)

      # puts ("Delete #{@attrib_role.name} for user #{user.name} : #{users_in&.include?(user.id)}").yellow
      authorize @attrib_role, :remove_role?
      user.remove_role(@attrib_role.name)
    end
  end

  def associate_roles_to_users
    # binding.pry
    set_attrib_user
    roles_in = params&.[]('roles_ids')&.map(&:to_i)

    # Ajout des utilisateurs sélectionnés :
    roles_in&.each do |role_id|
      role = Role.find(role_id)
      authorize role, :add_role?
      @attrib_user.add_role(role.name)
    end

    # Suppression des utilisateurs non sélectionnés :
    # TODO : ne pas supprimer le role si seul super_admin !!! => géré dans Pundit RolePolicy
    user_roles = @attrib_user.roles
    user_roles&.each do |role|
      next if roles_in&.include?(role.id)

      # puts ("Delete #{@attrib_role.name} for user #{user.name} : #{users_in&.include?(user.id)}").yellow
      authorize role, :remove_role?
      @attrib_user.remove_role(role.name)
    end
  end

  def set_attrib_role
    @attrib_role = Role.find(params[:attrib_role])
  rescue ActiveRecord::RecordNotFound # StandardError
    @attrib_role = Role.first
  rescue StandardError
    @attrib_role = Role.create!(name: 'super_admin')
  end

  def set_attrib_user
    @attrib_user = User.find(params[:attrib_user])
  rescue StandardError
    @attrib_user = User.first
  end

  # def all_roles
  #   [:super_admin, :admin, :manager, :user, :visitor]
  # end

  # Use callbacks to share common setup or constraints between actions.
  # def set_role
  #   @role = Role.find(params[:id])
  # end

  # Only allow a list of trusted parameters through.
  # def role_params
  #   params.require(:role).permit(:name)
  # end
end
