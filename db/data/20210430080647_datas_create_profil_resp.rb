class DatasCreateProfilResp < ActiveRecord::Migration[6.0]
  class MigrationProfil < ApplicationRecord
    self.table_name = :profils
  end
  class MigrationToolsTeam < ApplicationRecord
    self.table_name = :tools_teams
  end
  class MigrationRca < ApplicationRecord
    self.table_name = :rcas
  end
  class MigrationRca < ApplicationRecord
    self.table_name = :rcas
  end

  def up
    say 'Set profil modifiable false to Author and Action responsible.'
    # Set profil modifiable false to author
    author_profil = MigrationProfil.where(profil_name: 'author')&.first
    if author_profil
      author_profil.profil_modifiable = false
      author_profil.save!
    end

    # Set profil modifiable false to action responsible
    action_profil = MigrationProfil.where(profil_name: 'resp_act')&.first
    if action_profil
      action_profil.profil_modifiable = false
      action_profil.save!
    end

    # Add rca_responsible profil if it does not exist
    resp_profil = MigrationProfil.where(profil_name: 'resp_rca')&.first
    if resp_profil.nil?
      say 'Create profil Rca responsible.'
      resp_profil = MigrationProfil.create!(profil_name: 'resp_rca', name_fr: 'Responsable RCA', name_en: 'RCA Responsible', profil_locked: true, profil_modifiable: true)

      say 'Associate profil Rca responsible to Rcas.'
      MigrationRca.all.each do |rca|
        # author = rca.rca_author
        # rca.add_person_to_team(author, 'resp_rca') if author
        author = MigrationToolsTeam.where(rca_id: rca.id, profil_id: author_profil.id).first
        MigrationToolsTeam.create!(rca_id: rca.id, person_id: author.id, profil_id: resp_profil.id, tool_reference: 1, tool_num: 1) if author
      end
    else
      say 'Rca responsible profil exists in database.'
    end

    # Set pos to profils
    say 'Set pos to profils'
    pos = 1
    if author_profil
      author_profil.pos = pos
      author_profil.save!
      pos += 1
    end
    if resp_profil
      resp_profil.pos = pos
      resp_profil.save!
      pos += 1
    end

    MigrationProfil.all.each do |profil|
      next if profil.profil_name == 'author' || profil.profil_name == 'resp_rca'

      profil.pos = pos
      profil.save!
      pos += 1
    end
  end

  def down
    say 'Destroy profil Rca responsible.'
    resp_profil = MigrationProfil.where(profil_name: 'resp_rca')&.first
    resp_profil&.destroy
  rescue StandardError
    say 'Can not destroy profil.'
    raise ActiveRecord::IrreversibleMigration
  end
end
