require 'rails_helper'

RSpec.describe Admin::LdapConfigsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/ldap_configs').to route_to('admin/ldap_configs#index')
    end

    it 'routes to #new' do
      expect(get: '/admin/ldap_configs/new').to route_to('admin/ldap_configs#new')
    end

    it 'routes to #show' do
      expect(get: '/admin/ldap_configs/1').to route_to('admin/ldap_configs#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/admin/ldap_configs/1/edit').to route_to('admin/ldap_configs#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/admin/ldap_configs').to route_to('admin/ldap_configs#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/admin/ldap_configs/1').to route_to('admin/ldap_configs#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/admin/ldap_configs/1').to route_to('admin/ldap_configs#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/admin/ldap_configs/1').to route_to('admin/ldap_configs#destroy', id: '1')
    end
  end
end
