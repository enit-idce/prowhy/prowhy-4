module MyFormHelper
  def simple_form_for_horizontal(resource, options = {}, &block)
    options[:html] ||= {}
    options[:html][:class] = 'form-horizontal'
    options[:wrapper] = :horizontal_form
    options[:wrapper_mappings] = {
      boolean: :horizontal_boolean,
      check_boxes: :horizontal_collection,
      date: :horizontal_multi_select,
      datetime: :horizontal_multi_select,
      file: :horizontal_file,
      radio_buttons: :horizontal_collection,
      range: :horizontal_range,
      time: :horizontal_multi_select
    }
    simple_form_for(resource, options, &block)
  end
end
