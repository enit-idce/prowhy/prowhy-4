require_dependency 'meth'

module Meth
  class Methodology < ApplicationRecord
    include MethConcern

    translates :name, fallback: :any

    # doit impérativement être positionné AVANT has_many :steps sinon la fonction est appelée APRES le destroy des steps.
    before_destroy :destroy_methodology, prepend: true

    has_many :steps, dependent: :destroy
    has_many :steps_menu, -> { where(step_type: 1).order(:pos) }, class_name: 'Step'
    has_many :rcas
    has_one :step_info, -> { where(step_type: 2) }, class_name: 'Step'
    has_one :step_trstools, -> { where(step_type: 3) }, class_name: 'Step'

    validates :name, presence: true

    # scope pour lister les methodologies utilisables dans les rcas
    scope :rca_methodologies, -> { where(usable: true) }
    # scope pour lister les methodologies utilisables dans les rcas + methodo en cours (si methodo not usable)
    scope :rca_methodologies_for_rca, -> (m_id) { where(usable: true).or(where(id: m_id)) }
    # scope pour lister les méthodologies de rca (usable OR not usable & !lock)
    scope :rca_methodologies_all, -> { where(locked: false).or(where(usable: true)) }

    def locked?
      locked
    end

    def name
      ra_name = super # read_attribute(:name) / super appelle la fonction name de traco (translation)
      return usable ? ra_name : "#{ra_name} [NOT USABLE]"
    end

    def destroy_methodology
      # self.step_info = nil
      # self.step_trstools = nil
      # save!
    end

    def create_methodology
      return false unless save

      # Add step Info to methodology
      step = Step.create!(step_type: 2, name_fr: 'step0_infos', name_en: 'step0_infos', methodology_id: id, pos: 0,
                          step_role: Meth::StepRole.find_or_create_role_info)
      tool = Tool.find_or_create_custom_field
      # binding.pry
      step.add_tool(tool) if step && tool
      self.step_info = step

      # TO DO : ajout step_trstools => format à définir.
      # Add step TrsTools to methodology
      step = Step.create!(step_type: 3, name_fr: 'trs_tools', name_en: 'trs_tools', methodology_id: id, pos: 0,
                          step_role: Meth::StepRole.find_or_create_role_trs)

      # Criticity
      tool = Tool.find_or_create_tool(tool_name: 'Criticity')
      step.add_tool(tool) if step && tool
      # Action Plan
      tool = Tool.find_or_create_tool(tool_name: 'Action', name_en: 'Action Plan')
      step.add_tool(tool, tool_reference: 0, tool_num: 0) if step && tool
      # Report
      tool = Tool.find_or_create_tool(tool_name: 'Report')
      step.add_tool(tool, tool_reference: 0) if step && tool
      # Images
      tool = Tool.find_or_create_tool(tool_name: 'Image')
      step.add_tool(tool, tool_reference: 0) if step && tool
      # Documents
      tool = Tool.find_or_create_tool(tool_name: 'Document')
      step.add_tool(tool, tool_reference: 0) if step && tool

      self.step_trstools = step

      # Add step CLOTURE to methodology
      step = Step.create!(step_type: 1, name_fr: 'Cloture', name_en: 'Closure', methodology_id: id, pos: 1,
                          step_role: Meth::StepRole.find_or_create_role_close)
      tool = Tool.find_or_create_custom_field
      step.add_tool(tool, name_fr: 'Cloture', name_en: 'Closure', tool_options: {closure: true}) if step && tool

      return save
    end

    def copy
      methodo2 = Methodology.new(name: "#{name}_copy")
      # methodology.create_methodology

      # Copy step info
      step_info.copy(methodo2)

      # Copy step_trs
      step_trstools.copy(methodo2)

      # Copy les steps
      steps_menu.each do |step|
        step.copy(methodo2)
      end

      return methodo2
    end

    # # API documentation
    # def self.api_documentation
    #   ret = "Methdology ID : \n"

    #   rca_methodologies.each do |methodo|
    #     ret += "- #{methodo.id} : #{methodo.name}\n"
    #   end

    #   return ret
    # end
  end
end
