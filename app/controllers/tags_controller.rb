class TagsController < ApplicationController
  include AdminConcern
  # layout 'admin'

  before_action :set_tag, only: [:edit, :update, :destroy]

  def index
  end

  # Création des données JSon pour jstree
  def tags_js_tree_data
    tags = Tag.where(ancestry: nil)

    data = []

    tags.each do |tag|
      data << TagPresenter.new(tag, view_context).tag_subtree_datas
    end

    render json: data
  end

  def tags_list
    # puts "Return tags =======================>"
    # p(@tags)
    # render json: @tags
    #
    # tags = Tag.all
    # tags.each do |tag|
    #   # tag.tag_show_name
    #   puts tag.name
    # end

    tags = ActsAsTaggableOn::Tag.where('name LIKE ?', '%' + params[:term] + '%').collect(&:name)
    autocomplete_tags = { suggestions: tags }

    # puts 'Return tags =======================>'
    # p(autocomplete_tags)

    render json: autocomplete_tags
  end

  # PATCH/PUT /tags/1
  # PATCH/PUT /tags/1.json
  def update
    respond_to do |format|
      if @tag.update(tag_params)
        format.html { redirect_to tags_path, notice: 'Tag was successfully updated.' }
        format.js do
          flash.now[:notice] = 'Tag was successfully updated.'
          render :index
        end
      else
        format.html { render :index }
        format.js   { render :index }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_tag
    @tag = Tag.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def tag_params
    params.require(:tag).permit(:parent_id)
  end
end
