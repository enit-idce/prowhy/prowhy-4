require_dependency 'tools'

module Tools
  class IshiBranch < ApplicationRecord
    belongs_to :tools_ishikawa, class_name: 'Tools::Ishikawa', foreign_key: :tools_ishikawa_id
    belongs_to :labels_cause_type, class_name: 'Labels::CauseType', foreign_key: :labels_cause_type_id
  end
end
