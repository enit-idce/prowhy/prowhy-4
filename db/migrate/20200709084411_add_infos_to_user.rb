class AddInfosToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :preferences, :text
    add_column :users, :usersession, :text
  end
end
