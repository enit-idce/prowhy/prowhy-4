module AdminConcern
  extend ActiveSupport::Concern
  include PolicyAdminConcern

  included do
    layout 'admin'
  end

  # layout 'admin'
  # Ici : autorized_admin?
end
