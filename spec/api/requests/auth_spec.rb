require 'rails_helper'

describe '/oauth/token', type: :request do
  # let(:email) { user.email }
  # let(:password) { user.password }
  # let(:user) { FactoryBot.create(:user) }

  # let(:valid_attributes) { {grant_type: 'password', email: user.email, password: user.password} }

  before :each do
    @user = User.create!(username: 'admin', email: 'admin@prowhy.org',
                         password: 'admin++', password_confirmation: 'admin++', person: Person.create!(firstname: 'Admin', lastname: 'Bidule'))
  end

  # context 'negative tests' do
  #   context 'missing params' do
  #     context 'password' do
  #       post '/oauth/token', params: {username: user.email, password: 'toto'}
  #     end
  #     context 'email' do
  #       post '/oauth/token', params: {username: user.email, password: 'toto'}
  #     end
  #   end
  #   context 'invalid params' do
  #     context 'incorrect password' do
  #     end
  #     context 'with a non-existent login' do
  #     end
  #   end
  # end
  context 'positive tests' do
    context 'valid params' do
      it 'login user and returns token' do
        # headers = { 'ACCEPT' => 'application/json', 'Content-Type' => 'application/json' }
        post '/api/oauth/token', headers: headers, params: {grant_type: 'password', username: @user.username, password: @user.password}, as: :json

        json = JSON.parse(response.body)
        puts json['error_description']&.red
        # binding.pry
        expect(response.status).to eq(200)
      end
    end
  end
end
