class CreateToolsQqoqcpValues < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_qqoqcp_values do |t|
      t.references :qqoqcp, null: false, foreign_key: { to_table: :tools_qqoqcps }
      t.references :labels_qqoqcp_line, null: false, foreign_key: true
      t.text :is_value
      t.text :isnot_value
      t.text :info_value

      t.timestamps
    end

    add_reference :tools_qqoqcps, :labels_qqoqcp_config, null: true, foreign_key: { to_table: :labels_qqoqcp_configs }, after: :tool_num
  end
end
