class Profil < ApplicationRecord
  translates :name, fallback: :any
  validates :name, presence: { message: "can't be blanck : At leat one name must be completed." }
  validates :profil_name, presence: true, uniqueness: { case_sensitive: false }, format: { with: /\A[a-zA-Z0-9_]+\z/, message: 'only allows letters, numbers and underscore' }

  has_many :tools_teams, class_name: 'Tools::Team', dependent: :destroy
  has_many :people, through: :tools_teams
  has_many :rcas, through: :tools_teams

  default_scope { order(pos: :asc) }

  # Authorizations
  has_many :profil_authorizes, dependent: :destroy

  # TODO : ajouter un after create : créer tous les profil_authorize
  after_create :raz_authorizations

  def raz_authorizations
    step_roles = Meth::StepRole.all

    step_roles.each do |step_role|
      profil_auth = profil_authorizes.where(meth_step_role_id: step_role.id)&.first

      if profil_auth
        profil_auth.update!(read: true, write: false, advance: false, email: false)
        # binding.pry
      else
        ProfilAuthorize.create!(profil_id: id, meth_step_role_id: step_role.id)
      end
    end
  end
end
