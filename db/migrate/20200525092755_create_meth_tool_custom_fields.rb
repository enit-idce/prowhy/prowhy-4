class CreateMethToolCustomFields < ActiveRecord::Migration[6.0]
  def change
    create_table :meth_tool_custom_fields do |t|
      t.references :step_tool, null: false, foreign_key: { to_table: :meth_step_tools }
      t.references :custom_field_desc, null: false, foreign_key: { to_table: :meth_custom_field_descs }

      t.timestamps
    end
  end
end
