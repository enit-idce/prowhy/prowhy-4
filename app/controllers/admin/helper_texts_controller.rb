module Admin
  class HelperTextsController < ApplicationController
    include AdminConcern
    before_action :set_admin_helper_text, only: [:show, :edit, :update, :destroy]
    before_action :call_admin_policy

    def call_admin_policy
      authorize Admin, policy_class: AdminPolicy
    end

    # GET /admin/helper_texts
    # GET /admin/helper_texts.json
    def index
      @admin_helper_texts = Admin::HelperText.all
    end

    # GET /admin/helper_texts/1
    # GET /admin/helper_texts/1.json
    def show
    end

    # GET /admin/helper_texts/new
    def new
      @admin_helper_text = Admin::HelperText.new
    end

    # GET /admin/helper_texts/1/edit
    def edit
    end

    # POST /admin/helper_texts
    # POST /admin/helper_texts.json
    def create
      @admin_helper_text = Admin::HelperText.new(admin_helper_text_params)

      respond_to do |format|
        if @admin_helper_text.save
          format.html { redirect_to admin_helper_texts_url, notice: 'Helper text was successfully created.' }
          format.json { render :show, status: :created, location: @admin_helper_text }
        else
          format.html { render :new }
          format.json { render json: @admin_helper_text.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/helper_texts/1
    # PATCH/PUT /admin/helper_texts/1.json
    def update
      respond_to do |format|
        if @admin_helper_text.update(admin_helper_text_params)
          format.html { redirect_to admin_helper_texts_url, notice: 'Helper text was successfully updated.' }
          format.json { render :show, status: :ok, location: @admin_helper_text }
        else
          format.html { render :edit }
          format.json { render json: @admin_helper_text.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/helper_texts/1
    # DELETE /admin/helper_texts/1.json
    def destroy
      @admin_helper_text.destroy
      respond_to do |format|
        format.html { redirect_to admin_helper_texts_url, notice: 'Helper text was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_admin_helper_text
      @admin_helper_text = Admin::HelperText.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def admin_helper_text_params
      params.require(:admin_helper_text).permit(:tool_class, :reference, :help_text, :help_text_fr, :help_text_en)
    end
  end
end
