module ToolConcern
  extend ActiveSupport::Concern

  included do
    belongs_to :rca
  end

  # Class Methods
  module ClassMethods
    # options = { rca: rca, tool_reference: tool_reference[, tool_num: tool_num] }
    def create_default!(options, _step_tool)
      create!(options)
    end

    def search_default(rca, tool_name, step_tool)
      return nil unless rca.send(tool_name) && step_tool

      # Multiple ?
      return rca.send(tool_name).where(tool_reference: step_tool.tool_reference, tool_num: step_tool.tool_num) if create_search_multiple?

      # Non Multiple
      return rca.send(tool_name).where(tool_reference: step_tool.tool_reference, tool_num: step_tool.tool_num).first
    end

    def create_search_multiple?
      return false
    end
  end

  # Instance Methods
  def matrixable?
    false
  end
end
