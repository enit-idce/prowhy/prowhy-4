module Admin
  class AdminConfigPolicy < AdminPolicy
    attr_reader :user, :record

    def initialize(user, record)
      @user = user
      @record = record
    end

    def update?
      return false if record.next_default_role && !user.has_role?(:super_admin) && record.next_default_role == 1

      super
    end
  end
end
