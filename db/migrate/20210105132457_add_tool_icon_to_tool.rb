class AddToolIconToTool < ActiveRecord::Migration[6.0]
  def change
    add_column :meth_tools, :tool_icon, :string, after: :tool_options
  end
end
