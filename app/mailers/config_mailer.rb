class ConfigMailer < InfoconnectMailer
  before_action :init_smtp

  def init_smtp
    # # puts 'INIT MAILER========================================>'.yellow
    config = Admin::AdminConfig.all.first

    return unless config&.mailer_options

    options = config.mailer_options.deep_symbolize_keys!.deep_true_false_values

    ConfigMailer.delivery_method = :smtp
    ConfigMailer.default_options = {from: options[:from]}

    ConfigMailer.smtp_settings = {domain: options[:domain],
                                  port: options[:port].to_i,
                                  address: options[:address]}

    if options[:authentication] != 'none'
      ConfigMailer.smtp_settings.merge!({authentication: options[:authentication].intern,
                                         user_name: options[:user_name],
                                         password: options[:password]})
    end

    return unless options[:more_options] == true

    ConfigMailer.smtp_settings.merge!({enable_starttls_auto: options[:enable_starttls_auto],
                                       openssl_verify_mode: options[:openssl_verify_mode]})

    ConfigMailer.smtp_settings.merge!({ssl: true}) if options[:ssl_tls] == 'ssl'
    ConfigMailer.smtp_settings.merge!({tls: true}) if options[:ssl_tls] == 'tls'

    # binding.pry

    # puts '=====================CONFIGURATION==================='.red
    # p(ConfigMailer.smtp_settings)
    # puts '=========================END========================='.red
  end
end
