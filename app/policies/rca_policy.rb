# Attention :
# les changements effectués dans cette classe sont répercutés sur ToolsPolicy
class RcaPolicy < ApplicationPolicy
  attr_reader :user, :rca

  def initialize(user, rca)
    @user = user
    @rca = rca
  end

  def index?
    true
  end

  def recovery?
    user.has_any_role?(:admin, :super_admin, :manager, :user, :external)
  end

  def reinit_filters?
    index?
  end

  def reinit_filters_index?
    index?
  end

  def communication?
    user.has_any_role?(:admin, :super_admin, :manager) || user.user_authorized?(rca, 'email') # rca&.people&.include?(user.person)
  end

  def show?
    # binding.pry
    user.has_any_role?(:admin, :super_admin, :manager, :user) || user.user_authorized?(rca, 'read') # rca&.people&.include?(user.person)
  end

  def create?
    user.has_any_role?(:admin, :super_admin, :manager, :user, :external)
  end

  def new?
    create?
  end

  # TODO: tester les droits en fonction du rôle (profil) dans le RCA
  def update?
    user.has_any_role?(:admin, :super_admin, :manager) || user.user_authorized?(rca, 'write')
    # rca&.people&.include?(user.person)
  end

  def edit?
    show?
  end

  def destroy?
    # user.has_any_role?(:admin, :super_admin, :manager) || rca&.rca_responsible?(user.person)
    user.has_any_role?(:admin, :super_admin) || rca&.rca_responsible?(user.person)
    # TODO : ajouter OU Responsable du RCA => OK DONE
  end

  # def advancement_set?
  #   user.has_any_role?(:admin, :super_admin, :manager) || user.user_authorized?(rca, 'advance')
  #   # update?
  # end

  # En remplacement de la fonction advancement_set => appel spécifique permettant de passer un paramètre.
  def advancement_set_test?(step)
    raise Pundit::NotAuthorizedError, 'not authorized to set advancement' unless user.has_any_role?(:admin, :super_admin, :manager) ||
                                                                                 user.user_authorized?(rca, 'advance', step)
  end

  # Used in views => return true or false / do not raise error
  def rca_show_step?(step)
    # raise Pundit::NotAuthorizedError, 'not authorized to test' unless user.has_any_role?(:admin, :super_admin, :manager) ||
    #                                                                   user.user_authorized?(rca, 'read', step)
    return user.has_any_role?(:admin, :super_admin, :manager, :user) || user.user_authorized?(rca, 'read', step)
  end

  def rca_show_step_with_raise?(step)
    # raise Pundit::NotAuthorizedError, 'not authorized to test' unless user.has_any_role?(:admin, :super_admin, :manager) ||
    #                                                                   user.user_authorized?(rca, 'read', step)
    raise Pundit::NotAuthorizedError, 'not authorized to show' unless user.has_any_role?(:admin, :super_admin, :manager) ||
                                                                      user.user_authorized?(rca, 'read', step)
  end

  def reinit_all_advancements?
    user.has_any_role?(:admin, :super_admin, :manager)
  end

  def assign_perimeters?
    update?
  end

  def assign_perimeters_valid?
    update?
  end

  def send_emails?
    # puts 'CHECK SEND EMAILS IN RCA=========================================>'.blue
    return user.has_role?(:admin) || user.has_role?(:super_admin) || user.user_authorized?(rca, 'email')
  end

  def rca_add_parent?
    update?
  end

  class Scope
    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      # puts 'IN RESOLVE RCA SCOPE================================================>'.red
      if user.has_any_role?(:super_vision)
        scope.all
      elsif user.has_any_role?(:admin, :super_admin, :manager, :user, :external)
        # Filtrage : perimeter OK ou user fait partie de la tools_team
        scope.where(admin_perimeter_rcas: {perimeter_id: user.perimeters.map(&:id)})
             .or(scope.where(tools_teams: {person_id: user.person_id}))
             .includes(:perimeter_rcas, :tools_teams)
        # scope.where(admin_perimeter_rcas: {perimeter_id: user.perimeters.map(&:id)}).or(user.person.rcas).joins(:perimeter_rcas, tools_teams: :person)
      else
        user.person.rcas.distinct
      end
    end

    private

    attr_reader :user, :scope
  end

  class RecoveryScope
    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      if user.has_any_role?(:super_vision)
        scope.all
      elsif user.has_any_role?(:admin, :super_admin, :manager)
        scope.where(admin_perimeter_rcas: {perimeter_id: user.perimeters.map(&:id)})
             .or(scope.where(tools_teams: {person_id: user.person_id}))
             .includes(:perimeter_rcas, :tools_teams)
      else # user.has_any_role?(:user, :external)
        scope.includes(tools_teams: [:profil]).where(tools_teams: {person_id: user.person_id, profils: {profil_name: %w[author resp_rca]}})
      end
    end

    private

    attr_reader :user, :scope
  end
end
