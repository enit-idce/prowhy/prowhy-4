require 'rails_helper'

RSpec.describe Tools::Efficacity, type: :model do
  subject(:efficacity) do
    Tools::Efficacity.create!(element: task)
  end
  let(:task) do
    FactoryBot.create(:task, id: 1, rca: rca, name: 'Task 1', tools_cause: cause,
                             responsible_action: person, responsible_rca: person,
                             advancement: 0)
  end
  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
  let(:cause) { Tools::Cause.create!(rca: rca, tool_reference: 1, name: 'Cause 1') }
  let(:person) { FactoryBot.create(:person) }
  let(:user) { FactoryBot.create(:user, person_id: person.id) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to element' do
        assc = Tools::Efficacity.reflect_on_association(:element)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end

  describe 'efficacity status' do
    it 'check efficacity' do
      efficacity.check_status_in_progress!
      expect(efficacity.reload.efficacity_checked?).to be false

      efficacity.check_status_validated!
      expect(efficacity.reload.efficacity_checked?).to be true

      efficacity.check_status_invalidated!
      expect(efficacity.reload.efficacity_checked?).to be true
    end

    it 'update_date_done when save' do
      efficacity.check_status_validated!
      expect(efficacity.reload.date_done_check).to eq Date.today

      efficacity.check_status_in_progress!
      expect(efficacity.reload.date_done_check).to eq nil

      efficacity.check_status_invalidated!
      expect(efficacity.reload.date_done_check).to eq Date.today
    end
  end

  describe 'set_percentage' do
    it 'initialize percentage after creation' do
      expect(efficacity.percentage).to be_nil

      eff2 = Tools::Efficacity.create!(element: task, check_status: 1)
      expect(eff2.percentage).to eq 100
    end
  end
end
