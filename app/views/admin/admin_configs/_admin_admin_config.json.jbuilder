json.extract! admin_admin_config, :id, :connexions, :default_role, :task_mails, :created_at, :updated_at
json.url admin_admin_config_url(admin_admin_config, format: :json)
