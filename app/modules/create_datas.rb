module CreateDatas
  def self.create_rcas(nb = 100)
    i = 1000
    methodos = Meth::Methodology.where(locked: false)
    persons = Person.all
    # nb_persons = persons.size

    nb.times do
      rca = Rca.create!(title: "Rca #{i}", description: "Description du Rca #{i}", methodology: methodos[i%2])
      rca.initialize_rca_infos
      rca.add_person_to_team(persons.sample, 'author')
      rca.add_person_to_team(persons.sample, 'pilote')

      # rca.custom_field.custom_1 = rand 1..6
      # rca.custom_field.custom_2 = rand 7..11
      # rca.custom_field.custom_4 = rand 12..15
      # rca.custom_field.custom_9 = rand 26..30
      rca.custom_field.custom_1 = rand 1..4
      rca.custom_field.custom_2 = rand 5..8
      # rca.custom_field.custom_5 = rand 65..74
      # rca.custom_field.custom_9 = rand 93..96
      rca.custom_field.save!

      cause = Tools::Cause.create!(rca: rca, tool_reference: 1, name: "Cause #{rca.id}")
      Tools::Cause.create!(rca: rca, tool_reference: 1, name: "Cause 1 - #{rca.id}", parent: cause)
      Tools::Cause.create!(rca: rca, tool_reference: 1, name: "Cause 2 - #{rca.id}", parent: cause)
      Tools::Task.create!(rca: rca, tool_reference: 1, tool_num: 1, task_detail_type: 'Tools::TaskCorrective',
                          name: "Task 1-#{rca.id}", tools_cause: cause,
                          responsible_action: persons.sample, responsible_rca: persons.sample,
                          advancement: (i%5))
      Tools::Task.create!(rca: rca, tool_reference: 1, tool_num: 1, task_detail_type: 'Tools::TaskCorrective',
                          name: "Task 2-#{rca.id}", tools_cause: cause,
                          responsible_action: persons.sample, responsible_rca: persons.sample,
                          advancement: (i%5))

      Rcas::UpdateActionsTeam.new(rca).call
      i += 1
    end
  end
end
