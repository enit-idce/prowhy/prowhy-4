FactoryBot.define do
  factory :meth_list_content, class: Meth::ListContent do
    sequence(:name) { |n| 'Content_' + n.to_s }
    sequence(:pos) { |n| n }
    association :custom_field_desc, factory: :meth_custom_field_desc
  end
end
