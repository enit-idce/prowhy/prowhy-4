module Meth
  class MethodologiesController < ApplicationController
    include AdminConcern
    # layout 'admin'
    before_action :set_meth_methodology, only: [:show, :edit, :edit_meth, :update, :destroy, :copy]

    # GET /methodologies
    # GET /methodologies.json
    def index
      authorize Methodology
      @methodologies = Methodology.all
    end

    # GET /methodologies/1
    # GET /methodologies/1.json
    def show
    end

    # GET /methodologies/new
    def new
      @methodology = Methodology.new
      authorize @methodology
    end

    # GET /methodologies/1/edit
    def edit
      @activities = Activity.all.includes(:tools).order(:pos)
      @step = params[:step] ? Step.find(params[:step]) : @methodology.steps.first
    end

    # GET /methodologies/1/edit_meth
    def edit_meth
      render :new
    end

    # POST /methodologies
    # POST /methodologies.json
    def create
      @methodology = Methodology.new(meth_methodology_params)
      authorize @methodology

      respond_to do |format|
        if @methodology.create_methodology # save
          format.html { redirect_to edit_meth_methodology_path(@methodology), notice: t('.message_succes', methodo: Meth::Methodology.model_name.human) }
          # format.html { redirect_to @methodology, notice: 'Methodology was successfully created.' }
          format.json { render :show, status: :created, location: @methodology }
        else
          format.html { render :new }
          format.json { render json: @methodology.errors, status: :unprocessable_entity }
        end
      end
    end

    # POST /methodologies
    # POST /methodologies.json
    def copy
      methodology2 = @methodology.copy

      respond_to do |format|
        if methodology2.save!
          format.html { redirect_to edit_meth_methodology_path(methodology2), notice: t('.message_succes', methodo: Meth::Methodology.model_name.human) }
          # format.html { redirect_to @methodology, notice: 'Methodology was successfully created.' }
          format.json { render :show, status: :created, location: @methodology }
        else
          format.html { redirect_to meth_methodologies_url}
          format.json { render json: methodology2.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /methodologies/1
    # PATCH/PUT /methodologies/1.json
    def update
      respond_to do |format|
        if @methodology.update(meth_methodology_params)
          format.html { redirect_to meth_methodologies_url, notice: t('.message_succes', methodo: Meth::Methodology.model_name.human) }
          format.json { render :show, status: :ok, location: @methodology }
        else
          format.html { render :edit }
          format.json { render json: @methodology.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /methodologies/1
    # DELETE /methodologies/1.json
    def destroy
      @methodology.destroy
      respond_to do |format|
        format.html { redirect_to meth_methodologies_url, notice: t('.message_succes', methodo: Meth::Methodology.model_name.human) }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_meth_methodology
      @methodology = Methodology.find(params[:id])
      authorize @methodology
    end

    # Only allow a list of trusted parameters through.
    def meth_methodology_params
      params.require(:meth_methodology).permit(:name, :name_fr, :name_en, :usable)
    end
  end
end
