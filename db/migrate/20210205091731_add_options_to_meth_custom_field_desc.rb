class AddOptionsToMethCustomFieldDesc < ActiveRecord::Migration[6.0]
  def change
    add_column :meth_custom_field_descs, :show_list, :boolean, default: false, after: :internal_name
    add_column :meth_custom_field_descs, :show_search, :boolean, default: true, after: :show_list
  end
end
