module PeopleHelper
  def helper_person_choose(person = nil, c_name = 'person', c_name2 = nil, options = {})
    helper_class = options[:helper_class]
    url_submit = options[:url_submit]
    # url_obs = params[:url_obs]

    n_id = c_name2 ? c_name+'['+c_name2+'_id]' : c_name+'_id'
    n_txt = c_name2 ? c_name+'_'+c_name2+'_text' : c_name+'_text'
    n_snd = c_name2 ? c_name+'_'+c_name2 : c_name

    ret = hidden_field_tag(n_id, (person ? person.id.to_s : '0'))

    # Ajoute le lien JS sur l'event change du champs hidden
    # ret += javascript_tag("set_person_choose_function('#"+n_snd+"_id', '#{url_obs}');") if url_obs

    ret += if person
             ("<input type='text' readonly id='"+n_txt+"' class='form-control text_person #{helper_class}' value='" + person.name + "'></input>").html_safe
           else
             # ("<span id='"+n_txt+"' class='helper_person_choose #{helper_class}'><i style='color: grey;'>clic to choose</i></span>").html_safe
             ("<input type='text' readonly id='"+n_txt+"' class='form-control text_person #{helper_class}' value='" + 'clic to choose' + "'></input>").html_safe
           end

    ret += link_to fa_icon('list'),
                   person_choose_path(person_id: (person ? person.id : 0), c_name: n_snd, url_submit: url_submit),
                   remote: true,
                   id: n_snd+'_link'

    return ret.html_safe
  end
end
