json.extract! tools_task, :id,
              :rca_id, :tool_reference, :name, :tools_cause_id, :advancement,
              :responsible_action_id, :responsible_rca_id,
              :result, :priority, :date_due, :date_completion, :created_at, :updated_at
json.url tools_task_url(tools_task, format: :json)
