class RolePolicy < AdminPolicy
  attr_reader :user, :role

  def initialize(user, role)
    @user = user
    @role = role
  end

  def remove_role?
    if (user.has_role?(:super_admin) || (user.has_role?(:admin) && role.name != 'super_admin'))
      return role_is_deletable?
      # return true unless [:super_admin, :admin].include?(role.name.intern) && User.with_role(role.name).count == 1
    end

    return false
  end

  def add_role?
    user.has_role?(:super_admin) || (user.has_role?(:admin) && role.name != 'super_admin' && role.name != 'super_vision')
  end

  # private

  def role_is_deletable?
    return true unless [:super_admin, :admin].include?(role.name.intern) && User.with_role(role.name).count == 1
  end
end
