require 'rails_helper'

RSpec.describe Rcas::FilterRcas, type: :service do
  subject(:service) do
    Rcas::FilterRcas.new(@rcas)
  end

  let(:methodology1) { FactoryBot.create(:methodology, id: 1) }
  let(:methodology2) { FactoryBot.create(:methodology, id: 2) }
  let(:methodology3) { FactoryBot.create(:methodology, id: 3) }

  before :each do
    Rca.create!([{id: 1, title: 'Rca 1 toto', methodology: methodology1, created_at: Date.today},
                 {id: 2, title: 'Rca 2 bidule', methodology: methodology1, created_at: Date.today},
                 {id: 3, title: 'Rca 3 toto', methodology: methodology1, created_at: Date.yesterday},
                 {id: 4, title: 'Rca 4 machine', methodology: methodology2, created_at: Date.yesterday},
                 {id: 5, title: 'Rca 5 truc', methodology: methodology2, created_at: Date.tomorrow}])
    @rcas = Rca.all
  end

  it 'filters rca with no filters' do
    search_params = {custom_field: {},
                     rca_search: {}}

    service.call(search_params)
    expect(@rcas.map(&:id)).to contain_exactly(1, 2, 3, 4, 5)
  end

  it 'filters rca with methodology filter' do
    search_params = {custom_field: {},
                     rca_search: {methodology: ['1']}}

    rcas = service.call(search_params)
    expect(rcas.map(&:id)).to contain_exactly(1, 2, 3)
  end

  describe 'dates filters' do
    it 'filters rca with date begin filter' do
      search_params = {custom_field: {},
                       rca_search: {date_begin: Date.today}}

      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to contain_exactly(1, 2, 5)
    end

    it 'filters rca with date end filter' do
      search_params = {custom_field: {},
                       rca_search: {date_end: Date.today}}

      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to contain_exactly(1, 2, 3, 4)
    end

    it 'filters rca with date begin and date end filter' do
      search_params = {custom_field: {},
                       rca_search: {date_begin: Date.today, date_end: Date.today}}

      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to contain_exactly(1, 2)
    end
  end

  describe 'duration filters' do
    it 'filters rca with duration filter' do
      search_params = {custom_field: {},
                       rca_search: {adv_duration: 0}}

      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to contain_exactly(1, 2, 3, 4, 5)

      # return nothing
      search_params = {custom_field: {},
                       rca_search: {adv_duration: 10}}

      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to eq []
    end
  end

  describe 'advancement filters' do
    let(:role1) { FactoryBot.create(:meth_step_role, advance_level: 1) }
    let(:role_close) { FactoryBot.create(:meth_step_role, role_name: 'Close', advance_level: 10) }
    let(:activity) { Meth::Activity.create!(name: 'Activity 1') }
    let(:tool1) { Meth::Tool.create!(name: 'Tool 1', tool_name: 'Action', activity: activity) }
    let(:tool2) { Meth::Tool.create!(name: 'Tool 2', tool_name: 'Cloture', activity: activity) }

    before :each do
      step11 = FactoryBot.create(:meth_step, methodology_id: methodology1.id, step_role: role1)
      step12 = FactoryBot.create(:meth_step, methodology_id: methodology1.id, step_role: role_close)
      step21 = FactoryBot.create(:meth_step, methodology_id: methodology2.id, step_role: role1)
      step22 = FactoryBot.create(:meth_step, methodology_id: methodology2.id, step_role: role_close)
      step11.add_tool(tool1)
      step12.add_tool(tool2)
      step21.add_tool(tool1)
      step22.add_tool(tool2)

      # Rca.all.map(&:initialize_rca_infos)
      # 1 => closed
      Rca.find(1).advancement_set(step11, 2)
      Rca.find(1).advancement_set(step12, 2)
      # 2 => not closed
      Rca.find(2).advancement_set(step11, 2)
      Rca.find(2).advancement_set(step12, 1)
      # 3 => closed
      Rca.find(3).advancement_set(step11, 1)
      Rca.find(3).advancement_set(step12, 2)
      # 4 => closed
      Rca.find(4).advancement_set(step21, 2)
      Rca.find(4).advancement_set(step22, 2)
      # 5 => not closed
      Rca.find(5).advancement_set(step21, 2)
      Rca.find(5).advancement_set(step22, 0)

      Rca.all.each do |rca|
        Rcas::UpdateAdvance.new(rca).call
      end
    end

    it 'filters all advancement status Rca' do
      search_params = {custom_field: {},
                       rca_search: {status_adv: '1'}}

      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to contain_exactly(1, 2, 3, 4, 5)
    end

    it 'filters Rcas closed' do
      search_params = {custom_field: {},
                       rca_search: {status_adv: '3'}}

      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to contain_exactly(1, 4)
    end

    it 'filters Rcas not closed' do
      search_params = {custom_field: {},
                       rca_search: {status_adv: '2'}}

      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to contain_exactly(2, 3, 5)
    end
  end

  describe 'team filters' do
    before :each do
      @p1 = FactoryBot.create(:person)
      @p2 = FactoryBot.create(:person)
      @p3 = FactoryBot.create(:person)

      @profil1 = FactoryBot.create(:profil, profil_name: 'author', id: 1)
      @profil2 = FactoryBot.create(:profil, profil_name: 'member', id: 2)

      @rcas.each do |rca|
        rca.add_person_to_team_by_id(@p1, 1)
      end

      @rcas.first.add_person_to_team_by_id(@p2, 2)
      @rcas.first.add_person_to_team_by_id(@p3, 2)
      @rcas.second.add_person_to_team_by_id(@p2, 2)
      @rcas.last.add_person_to_team_by_id(@p3, 2)
    end
    it 'filters P1 as author' do
      search_params = {custom_field: {},
                       rca_search: {person_role: 1, person_id: @p1.id}}
      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to contain_exactly(1, 2, 3, 4, 5)
    end
    it 'filters P2 as author' do
      search_params = {custom_field: {},
                       rca_search: {person_role: 1, person_id: @p2.id}}
      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to eq []
    end

    it 'filters P2 as member' do
      search_params = {custom_field: {},
                       rca_search: {person_role: 2, person_id: @p2.id}}
      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to contain_exactly(1, 2)
    end
    it 'filters P3 as member' do
      search_params = {custom_field: {},
                       rca_search: {person_role: 2, person_id: @p3.id}}
      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to contain_exactly(1, 5)
    end
    it 'filters P1 as member' do
      search_params = {custom_field: {},
                       rca_search: {person_role: 2, person_id: @p1.id}}
      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to eq []
    end

    it 'filters without profil' do
      @rcas.last.add_person_to_team_by_id(@p2, 1)

      search_params = {custom_field: {},
                       rca_search: {person_id: @p2.id}}
      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to contain_exactly(1, 2, 5)
    end
  end

  describe 'words filters' do
    it 'filters words in title and descriptions' do
      search_params = {custom_field: {},
                       rca_search: {words: 'toto', words_type: ['1']}}

      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to contain_exactly(1, 3)
    end

    it 'filters words in actions' do
      search_params = {custom_field: {},
                       rca_search: {words: 'toto', words_type: ['2']}}

      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to be_empty
    end

    it 'filters words in causes' do
      search_params = {custom_field: {},
                       rca_search: {words: 'toto', words_type: ['3']}}

      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to be_empty
    end

    it 'filters words in all' do
      search_params = {custom_field: {},
                       rca_search: {words: 'toto', words_type: ['1', '2', '3']}}

      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to contain_exactly(1, 3)
    end

    it 'filters words at least one' do
      search_params = {custom_field: {},
                       rca_search: {words: 'toto truc', words_one: 'true', words_type: ['1', '2', '3']}}

      rcas = service.call(search_params)
      expect(rcas.map(&:id)).to contain_exactly(1, 3, 5)
    end
  end
end
