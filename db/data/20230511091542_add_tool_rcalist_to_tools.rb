# frozen_string_literal: true

class AddToolRcalistToTools < ActiveRecord::Migration[6.1]
  class MigrationTools < ApplicationRecord
    self.table_name = :meth_tools
    # serialize :connexions_backup
    # serialize :task_mails_backup
  end

  def up
    listrca_tool = MigrationTools.where(tool_name: 'RcaList') # name: 'external')
    if listrca_tool.empty?
      say 'Create tool ListRca.'
      MigrationTools.create!(id: 32, name_fr: 'Liste RCA', name_en: 'RCA list', activity_id: 12, tool_name: 'RcaList', tool_icon: 'list', tool_options: {})
    else
      say 'Role tool RcaList exists in database.'
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
