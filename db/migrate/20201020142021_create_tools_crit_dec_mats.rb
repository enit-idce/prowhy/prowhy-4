class CreateToolsCritDecMats < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_crit_dec_mats do |t|
      t.references :labels_criterium, null: false, foreign_key: true
      t.references :decision_mat, null: false, foreign_key: { to_table: :tools_decision_mats }
      t.integer :weight

      t.timestamps
    end
  end
end
