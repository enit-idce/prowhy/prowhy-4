FactoryBot.define do
  factory :labels_value, class: Labels::Value do
    sequence(:title) { |n| "Value #{n}" }
    sequence(:value) { |n| n }

    association :criterium, factory: :labels_criterium
  end
end
