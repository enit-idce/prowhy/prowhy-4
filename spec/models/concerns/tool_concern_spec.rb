# binding.pry
require 'rails_helper'

shared_examples_for ToolConcern, type: 'model' do
  # subject(:obj) do
  #   obj = if defined?(described_obj)
  #           described_obj
  #         else
  #           described_class.new
  #         end
  #   obj.save(validate: false)
  #   obj
  # end
  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
  # let(:step_tool) { defined in models }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to rca' do
        assc = described_class.reflect_on_association(:rca)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end

  describe 'Create and search model : tool concern functions' do
    it 'create_default' do
      obj_created = described_class.create_default!({rca: rca, tool_reference: 1}, step_tool)

      if described_class.create_search_multiple?
        expect(obj_created).to eq []
      else
        expect(obj_created.class).to eq described_class
        expect(obj_created.rca_id).to eq rca.id
      end
    end

    it 'search_default when object does not exist' do
      obj_founded = described_class.search_default(rca, described_class.table_name, step_tool)

      if described_class.create_search_multiple?
        expect(obj_founded.length).to eq 0
      else
        expect(obj_founded).to eq nil
      end
    end

    it 'search_default when object exists' do
      described_class.create_default!({rca: rca, tool_reference: 1}, step_tool)
      step_tool.tool_reference = 2
      obj2 = described_class.create_default!({rca: rca, tool_reference: 2}, step_tool)
      obj_founded = described_class.search_default(rca, described_class.table_name, step_tool)

      # binding.pry

      if described_class.create_search_multiple?
        # Cas des Tasks & DecMat => retourne [] car aucun Tool créé par create_default.
        expect(obj_founded).to eq []
      else
        expect(obj_founded).to eq obj2
      end
    end

    it 'search_default with tool_reference = 0' do
      described_class.create_default!({rca: rca, tool_reference: 1}, step_tool)
      step_tool.tool_reference = 2
      described_class.create_default!({rca: rca, tool_reference: 2}, step_tool)
      step_tool.tool_reference = 0
      obj_founded = described_class.search_default(rca, described_class.table_name, step_tool)

      # binding.pry

      if described_class.create_search_multiple?
        # Cas des Tasks & DecMat => retourne [] car aucun Tool créé par create_default.
        expect(obj_founded).to eq []
      else
        expect(obj_founded).to eq nil
      end
    end
  end
end

# Without qqoqcp config
describe Tools::Qqoqcp do
  it_behaves_like ToolConcern do
    let(:step_tool) { Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1) }
  end
end

# With Qqoqcp config
describe Tools::Qqoqcp do
  it_behaves_like ToolConcern do
    let(:qqoqcp_config) { Labels::QqoqcpConfig.create!(name: 'QQOQCP') }
    let(:step_tool) do
      Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1,
                         tool_options: {qqoqcp_config: qqoqcp_config.id})
    end
  end
end

describe Tools::Cause do
  it_behaves_like ToolConcern do
    let(:step_tool) { Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1) }
  end
end

describe Tools::Task do
  it_behaves_like ToolConcern do
    let(:step_tool) do
      Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1,
                         tool_options: {task_detail_type: 'Corrective'})
    end
  end
end

describe Tools::Ishikawa do
  it_behaves_like ToolConcern do
    let(:step_tool) { Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1) }
  end
end

describe Tools::CauseTree do
  it_behaves_like ToolConcern do
    let(:step_tool) { Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1) }
  end
end

describe Tools::DecisionMat do
  it_behaves_like ToolConcern do
    let(:step_tool) do
      Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1,
                         tool_options: {criteria: [{id: 1, weight: 3, values: [1, 2, 3]},
                                                   {id: 2, weight: 1, values: [1, 2, 3]},
                                                   {id: 3, weight: 2, values: [1, 2, 3]}],
                                        entry_class: 'Tools::Task'})
    end
  end
end

describe Tools::Team do
  it_behaves_like ToolConcern do
    let(:step_tool) do
      Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1)
    end
  end
end

describe Tools::Document do
  it_behaves_like ToolConcern do
    let(:step_tool) do
      Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1)
    end
  end
end

describe Tools::Image do
  it_behaves_like ToolConcern do
    let(:step_tool) do
      Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1)
    end
  end
end

describe Tools::Criticity do
  it_behaves_like ToolConcern do
    let(:step_tool) do
      Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1)
    end
  end
end
