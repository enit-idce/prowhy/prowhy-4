require 'rails_helper'

RSpec.describe '/people', type: :request do
  let(:valid_attributes) { {firstname: 'Toto', lastname: 'Titi'} }
  let(:invalid_attributes) { {firstname: nil, lastname: 'Titi'} }

  let(:user) { FactoryBot.create(:user) }

  before :each do
    sign_in user
  end

  describe 'GET /index' do
    it 'renders a successful response' do
      Person.create! valid_attributes
      get people_url
      expect(response).to be_successful
    end
  end

  # describe 'GET /show' do
  #   it 'renders a successful response' do
  #     person = Person.create! valid_attributes
  #     get person_url(person)
  #     expect(response).to be_successful
  #   end
  # end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_person_url
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      person = Person.create! valid_attributes
      get edit_person_url(person)
      expect(response).to be_successful
    end
    it 'render a successful response with param from=user' do
      person = Person.create! valid_attributes
      get edit_person_url(person, from: 'user')
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Person' do
        expect {
          post people_url, params: { person: valid_attributes }
        }.to change(Person, :count).by(1)
      end

      it 'redirects to the people list' do
        post people_url, params: { person: valid_attributes }
        expect(response).to redirect_to(people_url)
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Person' do
        expect {
          post people_url, params: { person: invalid_attributes }
        }.to change(Person, :count).by(0)
      end

      it 'renders a successful response (i.e. to display the new template)' do
        post people_url, params: { person: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      let(:new_attributes) { {firstname: 'Jonnhy'} }

      it 'updates the requested person' do
        person = Person.create! valid_attributes
        patch person_url(person), params: { person: new_attributes }
        person.reload
        expect(person.firstname).to eq 'Jonnhy'
      end

      it 'redirects to the people list without from attribute' do
        person = Person.create! valid_attributes
        patch person_url(person), params: { person: new_attributes }
        person.reload
        expect(response).to redirect_to(people_url)
      end

      it 'redirects to edit person with from attribute=user' do
        person = Person.create! valid_attributes
        patch person_url(person), params: { person: new_attributes, from: 'user'}
        person.reload
        expect(response).to redirect_to(edit_person_url(person, from: 'user'))
      end
    end

    context 'with invalid parameters' do
      it 'renders a successful response (i.e. to display the edit template)' do
        person = Person.create! valid_attributes
        patch person_url(person), params: { person: invalid_attributes }
        expect(person.firstname).to eq 'Toto'
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested person' do
      person = Person.create! valid_attributes
      expect {
        delete person_url(person)
      }.to change(Person, :count).by(-1)
    end

    it 'redirects to the people list' do
      person = Person.create! valid_attributes
      delete person_url(person)
      expect(response).to redirect_to(people_url)
    end
  end
end
