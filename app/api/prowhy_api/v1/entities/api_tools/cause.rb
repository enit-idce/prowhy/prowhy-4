module ProwhyApi
  module V1
    module Entities
      module ApiTools
        class Cause < Grape::Entity
          expose :id
          expose :parent_id, if: { with_parent_id: true }
          expose :name
          expose :status
          expose :level

          expose :ishikawas, if: { with_ishikawas: true }
          expose :ishikawas_inline, merge: true, if: { with_ishikawas_inline: true }

          expose :children, using: Entities::ApiTools::Cause, if: { with_child: 'true'}

          private

          def ishikawas
            ret = {}
            object.tools_cause_classes.each do |ishi|
              ret["ishikawa_#{ishi.tools_ishikawa.id}_ID"] = ishi.labels_cause_type_id
              ret["ishikawa_#{ishi.tools_ishikawa.id}"] = ishi.labels_cause_type&.name
            end

            ret
          end

          def ishikawas_inline
            ret = {}
            object.tools_cause_classes.each do |ishi|
              ret["ishikawa_#{ishi.tools_ishikawa.id}"] = ishi.labels_cause_type&.name
            end

            ret
          end
        end
      end
    end
  end
end
