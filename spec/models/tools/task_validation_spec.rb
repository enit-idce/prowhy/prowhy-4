require 'rails_helper'

RSpec.describe Tools::TaskValidation, type: :model do
  subject(:task_validation) do
    Tools::TaskValidation.create!(task: task)
  end

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
  let(:cause) { Tools::Cause.create!(rca: rca, tool_reference: 1, name: 'Cause 1') }
  let(:person) { FactoryBot.create(:person) }
  let(:user) { FactoryBot.create(:user, person_id: person.id) }
  let(:task) { FactoryBot.create(:task, id: 1, rca: rca, name: 'Task 1', tools_cause: cause) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has_one task' do
        assc = Tools::TaskValidation.reflect_on_association(:task)
        expect(assc.macro).to eq :has_one
      end
    end

    describe 'Validations' do
      it 'is valid with status in_progress, validated, or invalidated' do
        task_validation.status = 'in_progress'
        expect(task_validation).to be_valid

        task_validation.status = 'validated'
        expect(task_validation).to be_valid

        task_validation.status = 'invalidated'
        expect(task_validation).to be_valid
      end

      it 'is not valid with invalid status' do
        expect { task_validation.status = 'toto' }.to raise_error(ArgumentError)
      end
    end
  end
end
