module ProwhyApi
  module V1
    module Entities
      module ApiMeth
        class Methodology < Grape::Entity
          expose :id
          expose :name
          expose :usable
        end
      end
    end
  end
end
