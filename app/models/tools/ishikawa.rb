require_dependency 'tools'

module Tools
  class Ishikawa < ApplicationRecord
    include ToolConcern
    # belongs_to :rca
    # Head Cause
    # OLD VERSION
    # belongs_to :tools_cause, class_name: 'Tools::Cause', optional: true

    serialize :options

    # NEW VERSION
    has_one :cause_link, as: :cause_tool, dependent: :destroy
    has_one :cause, through: :cause_link

    # Liens vers les branches
    has_many :tools_ishi_branchs, class_name: 'Tools::IshiBranch', foreign_key: :tools_ishikawa_id, dependent: :destroy
    has_many :labels_cause_types, class_name: 'Labels::CauseType', through: :tools_ishi_branchs

    # Lien vers les causes par branches
    has_many :tools_cause_classes, class_name: 'Tools::CauseClass', foreign_key: :tools_ishikawa_id, dependent: :destroy
    has_many :labels_cause_types_used, class_name: 'Labels::CauseType', through: :tools_cause_classes, foreign_key: :labels_cause_type_id
    has_many :tools_causes, class_name: 'Tools::Cause', through: :tools_cause_classes

    after_initialize do
      if new_record?
        # Initialisation des options par défaut
        initialize_options
      end
    end

    def initialize_options
      self.options ||= {}
      self.options[:zoom] ||= 1.2
      self.options[:causes] ||= { status: {validated: true, in_progress: true, invalidated: true},
                                  show_only_root: false,
                                  show_only_level1: false }
    end

    # before_destroy :del_tool_cause
    # after_create :associate_labels

    # Create and search default => call Tools::Cause functions
    # def self.create_default!(options, step_tool)
    #   options_cause = options.clone
    #   # options_cause = Marshal.load(Marshal.dump(options))
    #   rca = options[:rca]
    #   cause_first = Tools::Cause.search_default(rca, 'tools_causes', Meth::StepTool.new(tool_reference: options[:tool_reference]))

    #   options_cause[:tool_num] = 1 # Si on crée la cause, on la crée avec num=1 impérativement.
    #   cause_first ||= Tools::Cause.create_default!(options_cause, step_tool)

    #   Tools::Ishikawa.create!(options.merge!(tools_cause_id: cause_first.id))
    #   # tool_options: step_tool.tool_options))
    # end
    def self.create_default!(options, step_tool)
      # options_cause = options.clone
      # # options_cause = Marshal.load(Marshal.dump(options))
      # rca = options[:rca]
      # cause_first = Tools::Cause.search_default(rca, 'tools_causes', Meth::StepTool.new(tool_reference: options[:tool_reference]))
      # Cause Tree creation
      ishikawa = create!(options)
      ishikawa.associate_labels(step_tool)

      # options_cause[:tool_num] = 1 # Si on crée la cause, on la crée avec num=1 impérativement.
      # cause_first ||= Tools::Cause.create_default!(options_cause, step_tool)
      # Assign or Create Head cause to Ishikawa
      la_cause = options[:rca].find_or_create_tool('tools_causes', 'Tools::Cause', step_tool)
      # ishikawa.cause_link = Tools::CauseLink.create!(cause: la_cause, cause_tool: ishikawa)
      ishikawa.assign_root_cause(la_cause)

      ishikawa
    end

    def assign_root_cause(root_cause)
      if cause_link
        cause_link.cause = root_cause
        cause_link.save!
      else
        Tools::CauseLink.create!(cause: root_cause, cause_tool: self)
      end
    end

    # def self.search_default(rca, _tool_name, step_tool)
    #   Tools::Cause.search_default(rca, 'tools_causes', step_tool)
    #   # rca.send('tools_causes').where(tool_reference: step_tool.tool_reference, tree_head: true).first
    # end

    def add_ishi_branch(cause_type)
      Tools::IshiBranch.create!(labels_cause_type: cause_type, tools_ishikawa: self)
    end

    # MAJ des branches de l'ishikawa :
    # * si step_tool => utilise tools_options[:labels] pour mettre à jour le branches
    # * sinon : tous les CauseType
    def associate_labels(step_tool)
      # If labels defined in tool_options
      if step_tool&.tool_options&.[](:labels)
        labels = []
        step_tool.tool_options[:labels].each_key do |key|
          begin
            cause_type = Labels::CauseType.find(step_tool.tool_options[:labels][key])
          rescue StandardError
            puts "No label cause type with id = #{step_tool.tool_options[:labels][key]} => pass.".blue
            next
          end
          labels << step_tool.tool_options[:labels][key]
          add_ishi_branch(cause_type) if cause_type && labels_cause_types.where(id: cause_type.id).empty?
        end
        # Suppression des branches en trop => + suppression des tools_cause_classes
        tools_ishi_branchs.each do |branch|
          next if labels.include?(branch.labels_cause_type.id)

          tools_cause_classes.where(labels_cause_type_id: branch.labels_cause_type.id).each(&:destroy)
          branch.destroy
        end
      else
        associate_all_labels
      end
    end

    private

    def associate_all_labels
      Labels::CauseType.all.each do |cause_type|
        add_ishi_branch(cause_type) if cause_type && labels_cause_types.where(id: cause_type.id).empty?
      end
    end
    # def del_tool_cause
    #   self.tools_cause = nil
    #   save!
    # end
  end
end
