module Labels
  class CheckListElemsController < ApplicationController
    include AdminConcern
    before_action :set_labels_check_list_elem, only: %i[show edit update destroy]

    # GET /labels/check_list_elems or /labels/check_list_elems.json
    def index
      authorize CheckListElem
      @labels_check_list_elems = Labels::CheckListElem.all
    end

    # # GET /labels/check_list_elems/1 or /labels/check_list_elems/1.json
    # def show
    # end

    # GET /labels/check_list_elems/new
    def new
      @labels_check_list_elem = Labels::CheckListElem.new
      authorize @labels_check_list_elem
    end

    # GET /labels/check_list_elems/1/edit
    def edit
    end

    # POST /labels/check_list_elems or /labels/check_list_elems.json
    def create
      @labels_check_list_elem = Labels::CheckListElem.new(labels_check_list_elem_params)
      authorize @labels_check_list_elem

      respond_to do |format|
        if @labels_check_list_elem.save
          format.html { redirect_to labels_check_list_elems_url, notice: 'Check list elem was successfully created.' }
          # { redirect_to labels_check_list_elem_url(@labels_check_list_elem), notice: 'Check list elem was successfully created.' }
          format.json { render :show, status: :created, location: @labels_check_list_elem }
        else
          format.html { render :new, status: :unprocessable_entity }
          format.json { render json: @labels_check_list_elem.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /labels/check_list_elems/1 or /labels/check_list_elems/1.json
    def update
      respond_to do |format|
        if @labels_check_list_elem.update(labels_check_list_elem_params)
          format.html { redirect_to labels_check_list_elems_url, notice: 'Check list elem was successfully updated.' }
          format.json { render :show, status: :ok, location: @labels_check_list_elem }
        else
          format.html { render :edit, status: :unprocessable_entity }
          format.json { render json: @labels_check_list_elem.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /labels/check_list_elems/1 or /labels/check_list_elems/1.json
    def destroy
      @labels_check_list_elem.destroy

      respond_to do |format|
        format.html { redirect_to labels_check_list_elems_url, notice: 'Check list elem was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_labels_check_list_elem
      @labels_check_list_elem = Labels::CheckListElem.find(params[:id])
      authorize @labels_check_list_elem
    end

    # Only allow a list of trusted parameters through.
    def labels_check_list_elem_params
      params.require(:labels_check_list_elem).permit(:name, :task_detail_type, :usable)
    end
  end
end
