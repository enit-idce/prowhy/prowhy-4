module Rcas
  class GenerateBarcode
    attr_accessor :code_type, :correspondance

    def initialize(code_type)
      @code_type = code_type

      @correspondance = {
        'Code25': 'code_25',
        'Code25Interleaved': 'code_25_interleaved',
        'Code25IATA': 'code_25_iata',
        'Code39': 'code_39',
        'Code39Extended': 'code_39',
        'Code128A': 'code_128',
        'Code128B': 'code_128',
        'Code128C': 'code_128',
        'EAN13': 'ean_13',
        'EAN8': 'ean_8',
        'QrCode': 'qr_code'
      }
    end

    require 'barby'
    # require 'barby/barcode/code_128'
    require 'barby/outputter/ascii_outputter'
    require 'barby/outputter/png_outputter'
    require 'barby/outputter/html_outputter'

    def call(text_to_encode)
      return "| Invalid Barcode Format (#{code_type})" unless (correspondance[code_type.intern])
      
      require "barby/barcode/#{correspondance[code_type.intern]}"
      
      begin
        barby_class = "Barby::#{code_type}".constantize

        if code_type == 'QrCode'
          barcode = Barby::QrCode.new(text_to_encode, level: :q, size: 5)
          xdim = 3
        else
          barcode = barby_class.new(text_to_encode)
          xdim = 2
        end

        # return barcode.to_html.html_safe
        base64_output = Base64.encode64(barcode.to_png({ height: 50, xdim: xdim }))
        return "<img src='data:image/png;base64,#{base64_output}'>".html_safe
      rescue
        return "| Data not valid with this barcode format (#{code_type}) : #{text_to_encode}"
      end
    end

    # def call_qrcode_generate(text_to_encode)
    #   require 'barby/barcode/qr_code'

    #   barcode = Barby::QrCode.new(text_to_encode, level: :q, size: 5)
    #   # return barcode.to_html({ xdim: 5, height: 50 }).html_safe
    
    #   base64_output = Base64.encode64(barcode.to_png({ xdim: 3 }))
    #   return "<img src='data:image/png;base64,#{base64_output}'>".html_safe
    # end

    def types_list
      types = []
      correspondance.each_key do |k|
        types << [k, k]
      end

      # puts "============================================================"
      # puts types

      types
    end

  end
end
