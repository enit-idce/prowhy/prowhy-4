module ProwhyApi
  module V1
    module Entities
      class CustomField < Grape::Entity
        include CustomFieldsHelper
        # expose custom fields

        # CustomField.column_names.each do |cf|
        #   next unless cf.include? 'custom_'

        #   cfdesc = Meth::CustomFieldDesc.where(internal_name: cf)&.first
        #   next unless cfdesc

        #   expose cf.intern, as: cfdesc.name.intern
        # end

        expose :expose_custom_tools, merge: true

        private

        def expose_custom_tools
          ret = {}

          # object.rca.methodology&.step_info&.step_tools_with_list&.each do |step_tool|
          object.rca.methodology&.steps&.each do |step|
            step&.step_tools_with_list&.each do |step_tool|
              step_tool.tool_custom_fields&.each do |tool_custom_field|
                cf_internal_name = tool_custom_field.custom_field_desc.internal_name.intern
                # cf_name = tool_custom_field.custom_field_desc.name.intern

                begin
                  ret[cf_internal_name] = object.send(cf_internal_name) # show_custom_field(object, tool_custom_field.custom_field_desc) # object.send(cf_internal_name)
                rescue StandardError
                  ret[cf_internal_name] = 'This field does not exist in db.'
                end
              end
            end
          end
          # puts ret
          ret
        end
      end
    end
  end
end
