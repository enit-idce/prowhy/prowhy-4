json.extract! labels_config, :id, :name_fr, :name_en, :formula, :usable, :created_at, :updated_at
json.url labels_config_url(labels_config, format: :json)
