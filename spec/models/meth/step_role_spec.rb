require 'rails_helper'

RSpec.describe Meth::StepRole, type: :model do
  subject(:step_role) do
    # Meth::Step.new(name: 'Step 1', methodology: methodology)
    FactoryBot.create(:meth_step_role)
  end

  # let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  # let(:activity) { Meth::Activity.create!(name: 'Activity 1') }
  # let(:tool1) { Meth::Tool.create!(name: 'Tool 1', tool_name: 'Action', activity: activity) }
  # let(:tool2) { Meth::Tool.create!(name: 'Tool 2', tool_name: 'Action', activity: activity) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has many steps' do
        assc = Meth::StepRole.reflect_on_association(:steps)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        step_role.name = ''
        expect(step_role).to_not be_valid
      end

      it 'is valid with name' do
        expect(step_role).to be_valid
      end

      it 'is not valid without role_name' do
        step_role.role_name = ''
        expect(step_role).to_not be_valid
      end

      it 'is valid with role_name' do
        expect(step_role).to be_valid
      end
    end
  end

  describe 'Find or create Roles' do
    it 'find_or_create_role_info' do
      role_info = Meth::StepRole.find_or_create_role_info
      expect(role_info).to be_valid
      expect(role_info.role_name).to eq 'Info'
    end

    it 'find_or_create_role_trs' do
      role_trs = Meth::StepRole.find_or_create_role_trs
      expect(role_trs).to be_valid
      expect(role_trs.role_name).to eq 'Trs'
    end
  end
end
