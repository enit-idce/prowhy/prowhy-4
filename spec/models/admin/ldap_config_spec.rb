require 'rails_helper'

RSpec.describe Admin::LdapConfig, type: :model do
  subject(:ldap_config) do
    FactoryBot.create(:ldap_config)
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has many users' do
        assc = Admin::LdapConfig.reflect_on_association(:users)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        ldap_config.name = nil
        expect(ldap_config).to_not be_valid
      end
      it 'is not valid without host' do
        ldap_config.host = nil
        expect(ldap_config).to_not be_valid
      end
      it 'is not valid without port' do
        ldap_config.port = nil
        expect(ldap_config).to_not be_valid
      end
      it 'is not valid without attr_uid' do
        ldap_config.attr_uid = nil
        expect(ldap_config).to_not be_valid
      end
      it 'is not valid without attr_email' do
        ldap_config.attr_email = nil
        expect(ldap_config).to_not be_valid
      end

      it 'is valid with all valid attributes' do
        expect(ldap_config).to be_valid
      end
    end
  end
end
