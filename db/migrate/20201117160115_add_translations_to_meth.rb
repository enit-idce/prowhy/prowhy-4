class AddTranslationsToMeth < ActiveRecord::Migration[6.0]
  def change
    # Meth::Activity
    add_column :meth_activities, :name_fr, :string, after: :name
    add_column :meth_activities, :name_en, :string, after: :name_fr

    Meth::Activity.all.each do |elem|
      elem.name_fr = elem.name
      elem.save!
    end

    remove_column :meth_activities, :name

    # Meth::Tool
    add_column :meth_tools, :name_fr, :string, after: :name
    add_column :meth_tools, :name_en, :string, after: :name_fr

    Meth::Tool.all.each do |elem|
      elem.name_fr = elem.name
      elem.save!
    end

    remove_column :meth_tools, :name

    # Meth::Step
    add_column :meth_steps, :name_fr, :string, after: :name
    add_column :meth_steps, :name_en, :string, after: :name_fr

    Meth::Step.all.each do |elem|
      elem.name_fr = elem.name
      elem.save!
    end

    remove_column :meth_steps, :name

    # Meth::StepTool
    add_column :meth_step_tools, :name_fr, :string, after: :name
    add_column :meth_step_tools, :name_en, :string, after: :name_fr

    Meth::StepTool.all.each do |elem|
      elem.name_fr = elem.name
      elem.save!
    end

    remove_column :meth_step_tools, :name

    # Meth::Methodology
    add_column :meth_methodologies, :name_fr, :string, after: :name
    add_column :meth_methodologies, :name_en, :string, after: :name_fr

    Meth::Methodology.all.each do |elem|
      elem.name_fr = elem.name
      elem.save!
    end

    remove_column :meth_methodologies, :name
  end
end
