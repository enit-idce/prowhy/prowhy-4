require 'rails_helper'

RSpec.describe Labels::QqoqcpLine, type: :model do
  subject(:qqoqcp_line) do
    Labels::QqoqcpLine.create!(internal_name: 'who', name: 'QUI ?')
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has many qqoqcl_cls' do
        assc = Labels::QqoqcpLine.reflect_on_association(:qqoqcp_cls)
        expect(assc.macro).to eq :has_many
      end
      it 'has many qqoqcp_configs' do
        assc = Labels::QqoqcpLine.reflect_on_association(:qqoqcp_configs)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools_qqoqcp_values' do
        assc = Labels::QqoqcpLine.reflect_on_association(:tools_qqoqcp_values)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        qqoqcp_line.name = ''
        expect(qqoqcp_line).to_not be_valid
      end
      it 'is valid with name' do
        qqoqcp_line.name = 'QUI ?'
        expect(qqoqcp_line).to be_valid
      end

      it 'is not valid without internal_name' do
        qqoqcp_line.internal_name = ''
        expect(qqoqcp_line).to_not be_valid
      end
      it 'is not valid with invalid internal_name' do
        qqoqcp_line.internal_name = 'Bla bla bla'
        expect(qqoqcp_line).to_not be_valid
      end
      it 'is not valid with duplicated internal_name' do
        qqoqcp_line
        qqoqcp_line2 = Labels::QqoqcpLine.new(internal_name: 'who', name: 'QUOI ?')
        expect(qqoqcp_line2).to_not be_valid
      end
      it 'is valid with valid internal_name' do
        qqoqcp_line.internal_name = 'bla_bla_bla'
        expect(qqoqcp_line).to be_valid
      end
    end
  end
end
