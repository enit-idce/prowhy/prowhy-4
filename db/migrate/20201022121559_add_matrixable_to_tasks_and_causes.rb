class AddMatrixableToTasksAndCauses < ActiveRecord::Migration[6.0]
  def change
    add_column :tools_tasks, :matrix_status, :integer, default: 0, after: :date_completion
    add_column :tools_causes, :matrix_status, :integer, default: 0, after: :status
    add_column :tools_dec_mat_links, :matrix_status, :integer, default: 0, after: :entry_id
  end
end
