FactoryBot.define do
  factory :meth_step_role, class: Meth::StepRole do
    name { 'StepRole 1' }
    sequence(:role_name) { |n| "role_#{n}" }
  end
end
