class ProfilAuthorizesController < ApplicationController
  include AdminConcern
  before_action :set_profil_authorize, only: [:show, :edit, :update, :destroy]

  # GET /profil_authorizes
  # GET /profil_authorizes.json
  def index
    authorize Profil

    @profils = Profil.all
    @profil = params[:profil_id] ? Profil.find(params[:profil_id]) : Profil.first

    @profil_authorizes = @profil ? @profil.profil_authorizes : []
  end

  # GET /profil_authorizes/1
  # GET /profil_authorizes/1.json
  # def show
  # end

  # GET /profil_authorizes/new
  # def new
  #   @profil_authorize = ProfilAuthorize.new
  # end

  # GET /profil_authorizes/1/edit
  # def edit
  # end

  # POST /profil_authorizes
  # POST /profil_authorizes.json
  # def create
  #   @profil_authorize = ProfilAuthorize.new(profil_authorize_params)

  #   respond_to do |format|
  #     if @profil_authorize.save
  #       format.html { redirect_to @profil_authorize, notice: 'Profil authorize was successfully created.' }
  #       format.json { render :show, status: :created, location: @profil_authorize }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @profil_authorize.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PATCH/PUT /profil_authorizes/1
  # PATCH/PUT /profil_authorizes/1.json
  def update
    respond_to do |format|
      if @profil_authorize.update(profil_authorize_params)
        format.html { redirect_to @profil_authorize, notice: 'Profil authorize was successfully updated.' }
        format.json { render :show, status: :ok, location: @profil_authorize }
      else
        format.html { render :edit }
        format.json { render json: @profil_authorize.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profil_authorizes/1
  # DELETE /profil_authorizes/1.json
  # def destroy
  #   @profil_authorize.destroy
  #   respond_to do |format|
  #     format.html { redirect_to profil_authorizes_url, notice: 'Profil authorize was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_profil_authorize
    @profil_authorize = ProfilAuthorize.find(params[:id])

    authorize @profil_authorize
  end

  # Only allow a list of trusted parameters through.
  def profil_authorize_params
    params.require(:profil_authorize).permit(:profil_id, :meth_step_role_id, :read, :write, :advance, :email)
  end
end
