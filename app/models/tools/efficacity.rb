require_dependency 'tools'

module Tools
  class Efficacity < ApplicationRecord
    belongs_to :element, polymorphic: true

    enum check_status: { in_progress: 0, validated: 1, invalidated: 2 }, _prefix: true

    before_save :update_date_done, if: :will_save_change_to_check_status?
    after_create :set_percentage

    def efficacity_checked?
      return !check_status_in_progress?
    end

    private

    def update_date_done
      if efficacity_checked? & !date_done_check
        self.date_done_check = Date.today
        # save!
      elsif !efficacity_checked? && date_done_check
        self.date_done_check = nil
        # save!
      end

      if check_status_validated?
        self.percentage = 100 unless percentage
      else
        self.percentage = nil
      end
    end

    # TODO : supprimer cette fonction (vérifier les create dans l'application)
    # MAJ du percentage en fonction du status => à priori fonction inutile car percentage maj par update_date_done
    def set_percentage
      return unless check_status_validated? && !percentage

      self.percentage = 100
      save!
    end
  end
end
