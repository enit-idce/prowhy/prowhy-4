module Meth
  class StepPolicy < Meth::MethPolicy
    attr_reader :user, :record

    def initialize(user, record)
      @user = user
      @record = record
    end

    def step_del_tool?
      destroy?
    end

    def step_add_tool?
      create?
    end

    def step_tool_up?
      update?
    end

    def step_tool_down?
      update?
    end

    def step_up?
      update?
    end

    def step_down?
      update?
    end

    # def step_import?
    #   update?
    # end

    # def step_import_do?
    #   update?
    # end

    # def index?
    #   show?
    # end

    # def show?
    #   user.has_role?(:admin) || user.has_role?(:super_admin)
    # end

    # def create?
    #   user.has_role?(:super_admin)
    # end

    # def new?
    #   create?
    # end

    # def update?
    #   create?
    # end

    # def edit?
    #   create?
    # end

    # def destroy?
    #   create?
    # end
  end
end
