module Tools
  class ImagesController < ApplicationController
    include ProcessConcern

    before_action :set_tools_image, only: [:show, :edit, :update, :destroy]
    before_action :init_rca, only: [:index, :new, :create]

    # GET /tools/images
    # GET /tools/images.json
    def index
      # @tools_images = Tools::Image.all
      # authorize Image
      skip_authorization

      redirect_to rcas_path and return if @rca.nil? || current_user.get_session(:active_step_tool).nil?

      authorize @rca, :show?, policy_class: Tools::ToolsPolicy

      @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
      @tools_images = Tools::Image.search_default(@rca, 'tools_images', @step_tool)
    end

    # # GET /tools/images/1
    # # GET /tools/images/1.json
    # def show
    # end

    # GET /tools/images/new
    def new
      @tools_image = Tools::Image.new(tools_image_params)
      all_steps_types

      authorize @tools_image
    end

    # GET /tools/images/1/edit
    def edit
      all_steps_types
    end

    # POST /tools/images
    # POST /tools/images.json
    def create
      @tools_image = Tools::Image.new(tools_image_params)
      authorize @tools_image

      respond_to do |format|
        if @tools_image.save
          format.html { redirect_to tools_images_url(rca: @rca), notice: 'Image was successfully created.' }
          format.json { render :show, status: :created, location: @tools_image }
        else
          format.html { render :new }
          format.json { render json: @tools_image.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /tools/images/1
    # PATCH/PUT /tools/images/1.json
    def update
      respond_to do |format|
        if @tools_image.update(tools_image_params)
          format.html { redirect_to tools_images_url(rca: @rca), notice: 'Image was successfully updated.' }
          format.json { render :show, status: :ok, location: @tools_image }
        else
          format.html { render :edit }
          format.json { render json: @tools_image.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /tools/images/1
    # DELETE /tools/images/1.json
    def destroy
      @tools_image.file.purge
      @tools_image.destroy
      respond_to do |format|
        format.html { redirect_to tools_images_url(rca: @rca), notice: 'Image was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_image
      @tools_image = Tools::Image.find(params[:id])
      @rca = @tools_image.rca

      # authorize @rca, :show?
      authorize @tools_image
    end

    # Only allow a list of trusted parameters through.
    def tools_image_params
      params.require(:tools_image).permit(:rca_id, :tool_reference, :tool_num, :name, :description, :file)
    end

    def all_steps_types
      @all_steps_types = []
      return unless @tools_image&.rca

      @tools_image&.rca&.methodology&.steps_menu&.includes(:step_role)&.each do |step|
        @all_steps_types << [step.name + ' ' + step.step_role.name, step.step_role.id]
      end
    end
  end
end
