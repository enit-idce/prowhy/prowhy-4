require 'rails_helper'

RSpec.describe Meth::Tool, type: :model do
  subject(:tool) do
    Meth::Tool.create!(name: 'NC Team', tool_name: 'Team', activity: activity)
  end

  let(:activity) { Meth::Activity.create!(name: 'Team') }

  # Tests belongs_to :activity + dependant: destroy
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to activity' do
        assc = Meth::Tool.reflect_on_association(:activity)
        expect(assc.macro).to eq :belongs_to
      end

      it 'has many step_tools' do
        assc = Meth::Tool.reflect_on_association(:step_tools)
        expect(assc.macro).to eq :has_many
      end

      it 'has many steps' do
        assc = Meth::Tool.reflect_on_association(:steps)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        tool.name = ''
        expect(tool).to_not be_valid
      end

      it 'is not valid without tool name' do
        tool.tool_name = ''
        expect(tool).to_not be_valid
      end

      it 'is valid with name & tool_name' do
        # activity.name = 'Team'
        expect(tool).to be_valid
      end
    end
  end
end
