class CreateMethSteps < ActiveRecord::Migration[6.0]
  def change
    create_table :meth_steps do |t|
      t.string :name
      t.integer :pos
      t.references :methodology, null: false, foreign_key: { to_table: :meth_methodologies }

      t.timestamps
    end
  end
end
