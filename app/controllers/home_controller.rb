class HomeController < ApplicationController
  # layout 'home'

  def index
  end

  def prowhy_version
    @prowhy_version = '4'

    return unless (file_data = File.read("#{Rails.root}/.prowhy-version")&.split)

    @prowhy_version = file_data.first

    # binding.pry
    # puts @prowhy_version
  end

end
