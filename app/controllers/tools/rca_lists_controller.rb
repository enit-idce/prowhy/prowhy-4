module Tools
  class RcaListsController < ApplicationController
    include ProcessConcern
    before_action :set_tools_rca_list, only: [:show, :edit, :update, :destroy]
    before_action :init_rca, only: [:index, :new, :create]

    after_action :call_track_action, only: [:create, :destroy, :update]

    # GET /tools/rca_lists or /tools/rca_lists.json
    # def index
    #   @tools_rca_lists = Tools::RcaList.all
    # end

    # GET /tools/rca_lists/1 or /tools/rca_lists/1.json
    # def show
    # end

    # GET /tools/rca_lists/new
    # def new
    #   @tools_rca_list = Tools::RcaList.new
    # end

    # GET /tools/rca_lists/1/edit
    def edit
      redirect_to rcas_path and return if @rca.nil? || current_user.get_session(:active_step_tool).nil?

      @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
      # @rcas = @tools_rca_list.rcas

      @step_tool.tool_options ||= {}

      @default_methodology = @step_tool.tool_options[:methodology]&.split(' ')&.first&.to_i
      # binding.pry
    end

    # POST /tools/rca_lists or /tools/rca_lists.json
    # def create
    #   @tools_rca_list = Tools::RcaList.new(tools_rca_list_params)

    #   respond_to do |format|
    #     if @tools_rca_list.save
    #       format.html { redirect_to tools_rca_list_url(@tools_rca_list), notice: 'Rca list was successfully created.' }
    #       format.json { render :show, status: :created, location: @tools_rca_list }
    #     else
    #       format.html { render :new, status: :unprocessable_entity }
    #       format.json { render json: @tools_rca_list.errors, status: :unprocessable_entity }
    #     end
    #   end
    # end

    # PATCH/PUT /tools/rca_lists/1 or /tools/rca_lists/1.json
    def update
      respond_to do |format|
        if @tools_rca_list.update(tools_rca_list_params)
          format.html { redirect_to edit_tools_rca_list_path(@tools_rca_list), notice: 'Rca list was successfully updated.' }
          format.json { render :show, status: :ok, location: @tools_rca_list }
        else
          format.html { render :edit } # , status: :unprocessable_entity }
          format.json { render json: @tools_rca_list.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /tools/rca_lists/1 or /tools/rca_lists/1.json
    # def destroy
    #   @tools_rca_list.destroy

    #   respond_to do |format|
    #     format.html { redirect_to tools_rca_lists_url, notice: 'Rca list was successfully destroyed.' }
    #     format.json { head :no_content }
    #   end
    # end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_rca_list
      @tools_rca_list = Tools::RcaList.find(params[:id])
      @rca = @tools_rca_list.rca
      authorize @tools_rca_list
    end

    # Only allow a list of trusted parameters through.
    def tools_rca_list_params
      params.require(:tools_rca_list).permit(:rca_id, :tool_reference, :description)
    end
  end
end
