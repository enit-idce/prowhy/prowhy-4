class AddCToQqoqcps < ActiveRecord::Migration[6.0]
  def change
    add_column :tools_qqoqcps, :combien, :text, after: :pourquoi_info
    add_column :tools_qqoqcps, :combien_not, :text, after: :combien
    add_column :tools_qqoqcps, :combien_info, :text, after: :combien_not
  end
end
