require 'rails_helper'

RSpec.describe Profil, type: :model do
  subject(:profil) do
    FactoryBot.create(:profil)
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has many tools_teams' do
        assc = Profil.reflect_on_association(:tools_teams)
        expect(assc.macro).to eq :has_many
      end

      it 'has many people' do
        assc = Profil.reflect_on_association(:people)
        expect(assc.macro).to eq :has_many
      end

      it 'has many rcas' do
        assc = Profil.reflect_on_association(:tools_teams)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without profil_name or name' do
        profil.profil_name = ''
        expect(profil).to_not be_valid
        profil.reload
      end

      it 'is valid with profil_name and name' do
        expect(profil).to be_valid
      end
    end
  end
end
