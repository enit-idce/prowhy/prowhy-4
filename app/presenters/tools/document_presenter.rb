module Tools
  # < BasePresenter
  class DocumentPresenter < SimpleDelegator

    # def initialize(document)
    #   @document = document
    #   super(document)
    #   # ::Kernel.binding.pry
    # end

    def show_document_step_role
      Meth::StepRole.find(tool_reference).name
    rescue StandardError
      'Other'
    end

    # def show_document_step_role
    #   Meth::StepRole.find(@document.tool_reference).name
    # rescue StandardError
    #   'Other'
    # end
  end
end
