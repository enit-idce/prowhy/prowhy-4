class AddMaxValueToDecisionMat < ActiveRecord::Migration[6.0]
  def change
    add_column :tools_decision_mats, :max_value, :integer, default: 3, after: :entry_class
  end
end
