module ProwhyApi
  module V1
    module Entities
      module ApiTools
        class Task < Grape::Entity
          expose :id, if: { with_id: true }
          expose :rca_id
          expose :parent_id, if: { with_parent_id: true }
          expose :name
          expose :reference
          expose :task_detail_type
          expose :advancement
          expose :result
          expose :priority
          expose :created_at, format_with: :date_ok
          expose :date_due, format_with: :date_ok
          expose :reschedules, if: { with_reschedules: true}
          expose :date_completion, format_with: :date_ok

          expose :responsible_action, using: Entities::Person, if: {reponsible_names: false}
          expose :responsible_rca, using: Entities::Person, if: {reponsible_names: false}

          expose :responsible_fct_names, merge: true, if: {reponsible_names: true}

          expose :associated_cause, if: { with_cause: true }

          expose :children, using: Entities::ApiTools::Task, if: { with_child: true}

          expose :efficacity, merge: true, if: { with_efficacity: true }

          private

          def reference
            return object.reference
          end

          def associated_cause
            Entities::ApiTools::Cause.represent(object.tools_cause, { with_child: false, with_id: true })
          end

          def responsible_fct_names
            {
              responsible_action_name: object&.responsible_action&.name || '',
              responsible_rca_name: object&.responsible_rca&.name || ''
            }
          end

          def efficacity
            eff = object.efficacity
            return {} unless eff

            {
              efficacity_date_due: eff.date_due_check,
              efficacity_date_done: eff.date_done_check,
              efficacity_status: eff.check_status,
              efficacity_percentage: eff.percentage
            }
          end

          def reschedules
            ret = []
            object.tools_reschedules.each do |reschedule|
              ret << { date: reschedule.date,
                       comment: reschedule.comment }
            end

            ret
          end
        end
      end
    end
  end
end
