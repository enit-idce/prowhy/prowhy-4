require 'rails_helper'

RSpec.describe Tools::Qqoqcp, type: :model do
  subject(:qqoqcp) do
    Tools::Qqoqcp.create!(rca: rca, tool_reference: 1)
  end

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
  let(:qqoqcp_config) { Labels::QqoqcpConfig.create!(name: 'QQOQCP') }
  let(:qqoqcp_config2) { Labels::QqoqcpConfig.create!(name: 'QQOQCP2') }
  let(:qqoqcp_line1) { Labels::QqoqcpLine.create!(internal_name: 'who', name: 'QUI ?') }
  let(:qqoqcp_line2) { Labels::QqoqcpLine.create!(internal_name: 'what', name: 'QUOI ?') }
  let(:qqoqcp_line3) { Labels::QqoqcpLine.create!(internal_name: 'where', name: 'OU ?') }
  let(:qqoqcp_line4) { Labels::QqoqcpLine.create!(internal_name: 'when', name: 'QUAND ?') }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to labels_qqoqcp_config' do
        assc = Tools::Qqoqcp.reflect_on_association(:labels_qqoqcp_config)
        expect(assc.macro).to eq :belongs_to
      end
      it 'has many qqoqcp_values' do
        assc = Tools::Qqoqcp.reflect_on_association(:qqoqcp_values)
        expect(assc.macro).to eq :has_many
      end
    end
  end

  context 'without associated qqoqcp configuration' do
    before :each do
      qqoqcp.initialize_qqoqcp
    end

    describe 'initialize_qqoqcp' do
      it 'does nothing' do
        expect(qqoqcp.qqoqcp_values).to be_empty
      end
    end

    describe 'qqoqcp_values_from_config' do
      it 'returns nothing' do
        expect(qqoqcp.qqoqcp_values_from_config).to be_empty
      end
    end
  end

  context 'with associated qqoqcp configuration' do
    before :each do
      qqoqcp_config.add_line(qqoqcp_line1)
      qqoqcp_config.add_line(qqoqcp_line2)
      qqoqcp.labels_qqoqcp_config = qqoqcp_config
      qqoqcp.save!

      qqoqcp.initialize_qqoqcp
    end

    describe 'initialize_qqoqcp' do
      it 'initialize all qqoqcp values' do
        expect(qqoqcp.qqoqcp_values.length).to eq 2
      end
    end

    describe 'qqoqcp_values_from_config' do
      it 'filters qqoqcp_values from configuration' do
        qqoqcp_config2.add_line(qqoqcp_line3)
        qqoqcp_config2.add_line(qqoqcp_line4)

        qqoqcp.labels_qqoqcp_config = qqoqcp_config2
        qqoqcp.initialize_qqoqcp

        expect(qqoqcp.qqoqcp_values.length).to eq 4

        expect(qqoqcp.qqoqcp_values_from_config.length).to eq 2
        expect(qqoqcp.qqoqcp_values_from_config.map(&:labels_qqoqcp_line_id)).to contain_exactly(qqoqcp_line3.id, qqoqcp_line4.id)
      end
    end
  end
end
