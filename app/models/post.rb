class Post < ApplicationRecord
  has_rich_text :body
  validates :title, presence: true

  acts_as_taggable # Alias for acts_as_taggable_on :tags
end
