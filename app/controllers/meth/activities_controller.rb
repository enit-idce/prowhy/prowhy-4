# require_dependency 'meth'

module Meth
  class ActivitiesController < ApplicationController
    include AdminConcern
    # layout 'admin'
    before_action :set_meth_activity, only: [:show, :edit, :update, :destroy]

    # GET /activities
    # GET /activities.json
    def index
      @activities = Activity.all.includes(:tools).order(:pos)
      authorize Activity
    end

    # GET /activities/1
    # GET /activities/1.json
    def show
    end

    # GET /activities/new
    def new
      @activity = Activity.new
      authorize @activity
    end

    # GET /activities/1/edit
    def edit
    end

    # POST /activities
    # POST /activities.json
    def create
      @activity = Activity.new(meth_activity_params)
      authorize @activity

      respond_to do |format|
        if @activity.save
          format.html { redirect_to @activity, notice: 'Activity was successfully created.' }
          format.json { render :show, status: :created, location: @activity }
        else
          format.html { render :new }
          format.json { render json: @activity.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /activities/1
    # PATCH/PUT /activities/1.json
    def update
      respond_to do |format|
        if @activity.update(meth_activity_params)
          format.html { redirect_to @activity, notice: 'Activity was successfully updated.' }
          format.json { render :show, status: :ok, location: @activity }
        else
          format.html { render :edit }
          format.json { render json: @activity.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /activities/1
    # DELETE /activities/1.json
    def destroy
      @activity.destroy
      respond_to do |format|
        format.html { redirect_to meth_activities_url, notice: 'Activity was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_meth_activity
      @activity = Activity.find(params[:id])
      authorize @activity
    end

    # Only allow a list of trusted parameters through.
    def meth_activity_params
      params.require(:meth_activity).permit(:name, :name_fr, :name_en, :pos)
    end
  end
end
