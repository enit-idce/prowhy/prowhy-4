require 'rails_helper'

RSpec.describe Meth::StepToolsHelper, type: :helper do
  let(:step_tool) { FactoryBot.create(:meth_step_tool, step_id: step.id, tool_id: tool.id) }
  let(:methodology) { FactoryBot.create(:methodology) }
  let(:step) { FactoryBot.create(:meth_step, methodology_id: methodology.id) }
  let(:tool) { FactoryBot.create(:meth_tool) }

  describe '#step_tool_edit_custom_field' do
    context 'when Tool is not a CustomField' do
      it 'returns an empty string' do
        helper_content = helper.step_tool_edit_custom_field(step_tool)

        expect(helper_content).to eq ''
      end
    end

    context 'when Tool is a CustomField' do
      let(:tool_cf) { FactoryBot.create(:meth_tool, :custom_field) }
      let(:custom_field_desc) { FactoryBot.create(:meth_custom_field_desc, custom_field_type_id: custom_field_type.id) }
      let(:custom_field_type) { FactoryBot.create(:meth_custom_field_type) }

      before :each do
        step_tool.tool = tool_cf
        step_tool.save!

        FactoryBot.create(:meth_tool_custom_field, custom_field_desc_id: custom_field_desc.id,
                                                   step_tool_id: step_tool.id)
      end
      it 'returns info link (list content) and edit link (edit list content)' do
        helper_content = helper.step_tool_edit_custom_field(step_tool)

        expect(helper_content).to include('Custom Fields')
        expect(helper_content).to include(meth_tool_custom_fields_path(step_tool: step_tool.id))
        expect(helper_content).to include(custom_field_desc.name)
      end
    end
  end

  describe '#step_tool_show_name' do
    it 'returns the name and edit setp_tool link' do
      helper_content = helper.step_tool_show_name(step_tool)

      expect(helper_content).to include('StepTool 1')
      expect(helper_content).to include(edit_meth_step_tool_path(step_tool))
    end
  end
end
