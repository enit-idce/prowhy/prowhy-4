module Tools
  class IshikawasController < ApplicationController
    include ProcessConcern
    before_action :set_tools_ishikawa, only: [:ishikawa_drag_cause, :ishikawa_reset_branchs, :ishikawa_zoom, :ishikawa_update_option]
    before_action :init_rca, only: [:index, :new, :create]

    after_action :call_track_action, only: [:ishikawa_drag_cause, :ishikawa_reset_branchs]

    # GET /tools/ishikawas
    # GET /tools/ishikawas.json
    def index
      # authorize Ishikawa
      skip_authorization

      # @tools_ishikawas = Tools::Ishikawa.all
      # binding.pry
      if @rca.nil? || current_user.get_session(:active_step_tool).nil?
        redirect_to rcas_path
      elsif params[:id].nil?
        redirect_to edit_rca_path(@rca)
      else
        # authorize Ishikawa

        @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
        set_tools_ishikawa
        @cause_tree_head = @tools_ishikawa.cause&.id # params[:id]
        @causes = @tools_ishikawa.cause&.descendants # cause_head.descendants

        if @tools_ishikawa.options.nil? || @tools_ishikawa.options[:causes].nil?
          @tools_ishikawa.initialize_options
          @tools_ishikawa.save!
        end
        # @tools_ishikawa.options = {zoom: 1.2}
        # @tools_ishikawa.save!
        # @zoom_ishikawa = params[:options][:zoom] if params[:options]
        # @zoom_ishikawa = 1.2 unless @zoom_ishikawa
        # @zoom_ishikawa = @zoom_ishikawa.to_f
      end
    end

    # Création des données JSon pour ishikawa
    def ishikawa_js_tree_data
      # authorize Ishikawa
      skip_authorization

      data = {}

      if params[:id]
        set_tools_ishikawa
        data[:options] = { zoom: @tools_ishikawa.options[:zoom], langue: I18n.locale,
                           statuses: {in_progress: 1, validated: 1, invalidated: 1},
                           effect: [''],
                           causes: @tools_ishikawa.options[:causes] }
        data[:ishikawa] = { id: @tools_ishikawa.id,
                            tool_ref: @tools_ishikawa.tool_reference,
                            tool_num: @tools_ishikawa.tool_num,
                            rca: @tools_ishikawa.rca_id }
        data[:branchs] = { list: [], list_id: [] }
        data[:causes] = { list: {}, list_id: {}, list_v: {} }

        i = 1

        @tools_ishikawa.labels_cause_types.each do |ctype|
          data[:branchs][:list] << ctype.name
          data[:branchs][:list_id] << ctype.id

          data[:causes][:list][ctype.id] = []
          data[:causes][:list_id][ctype.id] = []
          data[:causes][:list_v][ctype.id] = []
          i += 1
        end
        # Ajout des emplacements pour causes sans 'M'
        data[:causes][:list][0] = []
        data[:causes][:list_id][0] = []
        data[:causes][:list_v][0] = []

        cause_head = @tools_ishikawa.cause

        @causes = if @tools_ishikawa.options[:causes][:show_only_root]
                    cause_head&.root_descendants || []
                  elsif @tools_ishikawa.options[:causes][:show_only_level1]
                    cause_head&.children || []
                  else
                    cause_head&.descendants || []
                  end

        data[:options][:effect] = [@tools_ishikawa.rca.title] if @tools_ishikawa.rca
        # [cause_head.rca.title] if cause_head&.rca

        i = 0
        @causes.each do |cause|
          # binding.pry
          next unless @tools_ishikawa.options[:causes][:status][cause.status.intern] == true

          num_m = cause.ishi_cause_type_id(@tools_ishikawa)
          data[:causes][:list][num_m] << cause.name_with_level # (cause.level.nil? ? cause.name : "(#{cause.level}) #{cause.name}")
          data[:causes][:list_id][num_m] << cause.id
          data[:causes][:list_v][num_m] << cause.read_attribute_before_type_cast(:status)
          i += 1
        end
      end

      render json: data
    end

    def ishikawa_drag_cause
      cause = Tools::Cause.find(params[:drag_id])

      authorize cause, :update? # demande autorisation de modif de la cause en plus de la modif de l'ishi. A voir si modif ishi ou pas.

      cause.add_ishi_cause_type(@tools_ishikawa, params[:drop_id].to_i)

      redirect_to tools_ishikawas_path(id: @tools_ishikawa.id, rca: @tools_ishikawa.rca)
    end

    def ishikawa_reset_branchs
      @step_tool = Meth::StepTool.find(params[:step_tool])
      @tools_ishikawa.associate_labels(@step_tool)

      redirect_to tools_ishikawas_path(id: @tools_ishikawa.id, rca: @tools_ishikawa.rca)
    end

    def ishikawa_zoom
      # @step_tool = Meth::StepTool.find(params[:step_tool])
      @tools_ishikawa.options[:zoom] = @tools_ishikawa.options[:zoom] + (params[:zoom]=='1' ? 0.1 : -0.1)
      @tools_ishikawa.options[:zoom] = 0.8 if @tools_ishikawa.options[:zoom] < 0.8
      @tools_ishikawa.save!
      redirect_to tools_ishikawas_path(id: @tools_ishikawa.id, rca: @tools_ishikawa.rca)
    end

    def ishikawa_update_option
      # @step_tool = Meth::StepTool.find(params[:step_tool])
      if params[:groption]
        @tools_ishikawa.options[:causes][params[:groption].intern][params[:option].intern] = !@tools_ishikawa.options[:causes][params[:groption].intern][params[:option].intern]
      else
        @tools_ishikawa.options[:causes][params[:option].intern] = !@tools_ishikawa.options[:causes][params[:option].intern]
      end

      @tools_ishikawa.save!
      redirect_to tools_ishikawas_path(id: @tools_ishikawa.id, rca: @tools_ishikawa.rca)
    end

    # # GET /tools/ishikawas/1
    # # GET /tools/ishikawas/1.json
    # def show
    # end

    # # GET /tools/ishikawas/new
    # def new
    #   @tools_ishikawa = Tools::Ishikawa.new
    # end

    # # GET /tools/ishikawas/1/edit
    # def edit
    # end

    # # POST /tools/ishikawas
    # # POST /tools/ishikawas.json
    # def create
    #   @tools_ishikawa = Tools::Ishikawa.new(tools_ishikawa_params)

    #   respond_to do |format|
    #     if @tools_ishikawa.save
    #       format.html { redirect_to @tools_ishikawa, notice: 'Ishikawa was successfully created.' }
    #       format.json { render :show, status: :created, location: @tools_ishikawa }
    #     else
    #       format.html { render :new }
    #       format.json { render json: @tools_ishikawa.errors, status: :unprocessable_entity }
    #     end
    #   end
    # end

    # # PATCH/PUT /tools/ishikawas/1
    # # PATCH/PUT /tools/ishikawas/1.json
    # def update
    #   respond_to do |format|
    #     if @tools_ishikawa.update(tools_ishikawa_params)
    #       format.html { redirect_to @tools_ishikawa, notice: 'Ishikawa was successfully updated.' }
    #       format.json { render :show, status: :ok, location: @tools_ishikawa }
    #     else
    #       format.html { render :edit }
    #       format.json { render json: @tools_ishikawa.errors, status: :unprocessable_entity }
    #     end
    #   end
    # end

    # # DELETE /tools/ishikawas/1
    # # DELETE /tools/ishikawas/1.json
    # def destroy
    #   @tools_ishikawa.destroy
    #   respond_to do |format|
    #     format.html { redirect_to tools_ishikawas_url, notice: 'Ishikawa was successfully destroyed.' }
    #     format.json { head :no_content }
    #   end
    # end

    # POST save_canvas_to_png
    # def save_canvas_to_png
    #   @tools_ishikawa = Tools::Ishikawa.find(params[:id])
    #   @rca = @tools_ishikawa.rca
    #   authorize @rca, :show?

    #   data = params[:image]
    #   # remove all extras except data
    #   image_data = Base64.decode64(data['data:image/png;base64,'.length .. -1])
    #   dirname = "#{Rails.root}/public/ishikawas/"
    #   FileUtils.mkdir(dirname) unless File.directory?(dirname)
    #   # filename = dirname + 'ishikawa_' + params[:id].to_s + '.png'
    #   filename = dirname + 'ishikawa_test.png'
    #   File.delete(filename) if File.exist?(filename)
    #   File.open(filename, 'wb') do |f|
    #     f.write image_data
    #   end
    #   render plain: 'OK'
    # end

    # POST save_image
    def save_image
      authorize Ishikawa
      # @tools_ishikawa = Tools::Ishikawa.find(params[:id])
      # @rca = @tools_ishikawa.rca
      # authorize @rca, :show?

      login_user = current_user.username
      dirname = 'public/ishikawas'
      img_name = (params[:imgname] || 'ishikawa_img') + '.png'

      FileUtils.mkdir(dirname) unless File.directory?(dirname)
      FileUtils.mkdir("#{dirname}/#{login_user}") unless File.directory?("#{dirname}/#{login_user}")

      filename = "#{dirname}/#{login_user}/#{img_name}"
      File.delete(filename) if File.exist?(filename)

      File.open(filename, 'wb') do |f|
        f.write params[:image].read
      end
      # binding.pry
      img_data = {image: ActionController::Base.helpers.asset_path("/ishikawas/#{login_user}/#{img_name}") }
      # render plain: 'OK'
      render json: img_data
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_ishikawa
      @tools_ishikawa = Tools::Ishikawa.find(params[:id])
      @rca = @tools_ishikawa.rca

      # authorize @rca, :show?
      authorize @tools_ishikawa
    end

    # Only allow a list of trusted parameters through.
    # def tools_ishikawa_params
    #   params.require(:tools_ishikawa).permit(:rca_id, :tool_reference, :tool_num)
    # end
  end
end
