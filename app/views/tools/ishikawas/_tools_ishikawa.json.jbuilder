json.extract! tools_ishikawa, :id, :rca_id, :tool_reference, :created_at, :updated_at
json.url tools_ishikawa_url(tools_ishikawa, format: :json)
