require 'rails_helper'

RSpec.feature 'Rca creation', type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  # let(:rca) { FactoryBot.create(:rca, methodology_id: methodology.id) }

  before :each do
    user.usersession[:active_step_tool] = methodology.steps.first.step_tools.first.id
    user.save!
    sign_in user

    FactoryBot.create(:methodology, :with_step_1, name: '8D')
  end

  scenario 'User creates a new rca' do
    visit '/rcas'
    # save_and_open_page
    click_link 'New Rca'

    # save_and_open_page

    fill_in 'Title', with: 'Mon premier Rca'
    select '8D', from: 'Methodology'
    click_button 'Save RCA', match: :first

    expect(page).to have_text('Rca was successfully created.')
    rca = Rca.first
    expect(rca.reference.to_i).to eq rca.id
    expect(page).to have_text("#{rca.id} - Mon premier Rca")

    expect(page).to have_current_path(edit_rca_path(Rca.last))
  end
end
