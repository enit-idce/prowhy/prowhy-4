require 'rails_helper'

RSpec.describe Meth::Activity, type: :model do
  subject(:activity) do
    Meth::Activity.create!(name: 'Team', pos: 1)
  end

  # Tests has_many :tools + dependant: destroy
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has many tools' do
        assc = Meth::Activity.reflect_on_association(:tools)
        expect(assc.macro).to eq :has_many
      end

      it 'destroy tools when destroy activity' do
        Meth::Tool.create!(name: 'Team', tool_name: 'Person', activity: activity)
        Meth::Tool.create!(name: 'Action Plan', tool_name: 'Action', activity: activity)

        expect(activity.tools).to eq Meth::Tool.all
        expect(activity.tools.size).to eq 2

        activity.destroy
        expect(Meth::Tool.all).to be_empty
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        activity.name = ''
        expect(activity).to_not be_valid
      end

      it 'is valid with name' do
        activity.name = 'Team'
        expect(activity).to be_valid
      end
    end
  end
end
