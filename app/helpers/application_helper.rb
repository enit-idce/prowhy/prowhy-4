module ApplicationHelper
  include FontAwesome5::Rails::IconHelper
  include Pagy::Frontend

  # Menu up
  def active_for_helper(options = {})
    name_of_controller = options[:controller] || nil
    name_of_action     = options[:action]     || nil
    request_path       = options[:path]       || nil
    # puts "ACTIVE FOR=================".yellow
    # p(params)
    # puts "OPTIONS".green
    # p(options)
    # puts request.path.red
    # binding.pry
    # puts "===========================================> #{controller_name} - #{action_name}".yellow
    # p(options)
    if request_path.nil?
      if (name_of_action.nil? || name_of_action == action_name) && name_of_controller == controller_name
        'btn-menu-active'
      else
        ''
      end
    else
      request_path == request.path ? 'btn-menu-active' : ''
    end
  end

  def active_for_tools_helper(step_tool)
    # binding.pry
    current_step_tool = current_user.get_session(:active_step_tool)&.to_i

    return current_step_tool == step_tool&.id ? 'btn-menu-active' : ''

    # return '' unless step_tool.step == step_tool.step&.methodology&.step_trstools

    # controller = step_tool.tool.tool_name.tableize
    # controller = 'tasks' if controller == 'actions'
    # controller = 'indicators' if controller == 'graphs'
    # controller = 'rca_reports' if controller == 'reports'

    # active_for_helper(controller: controller)
  end

  # Links
  def home_link
    link_to fa_icon('home'), rcas_path, class: 'btn btn-outline-dark icon-btn btn-navbar'
  end

  def userpage_link
    link_to fa_icon('user'), edit_user_registration_path, method: :get, class: 'btn icon-btn btn-navbar'
  end

  def admin_link
    link_to fa_icon('toolbox'), admin_index_path, class: 'btn btn-outline-dark icon-btn btn-navbar'
  end

  def user_taskplan_path
    # meth_step_tool_show_url(id: 1, rca: nil)
    meth_step_tool_show_path(id: 1, rca: nil)
  end

  def user_taskplan_url
    meth_step_tool_show_url(id: 1, rca: nil)
  end

  def global_taskplan_path
    tools_tasks_index_posts_path(step_tool: 2)
  end

  def prowhy_logo
    if @export_pdf
      # wicked_pdf_image_tag('media/application/images/logo_v2.png')
      url_image = "#{request.base_url}/#{ENV['RAILS_RELATIVE_URL_ROOT']}"
      image_tag "#{url_image}/pdf/logo_v1.png"
    else
      image_pack_tag('media/application/images/logo_v1.png')
    end
  end

  def prowhy_logo_carre
    if @export_pdf
      # wicked_pdf_image_tag('media/application/images/logo_v2.png')
      url_image = "#{request.base_url}/#{ENV['RAILS_RELATIVE_URL_ROOT']}"
      image_tag "#{url_image}/pdf/logo_carre.png"
    else
      image_pack_tag('media/application/images/logo_carre.png', style: 'max-width:140px;max-height:140px;')
    end
  end

  def customer_logo_intern
    url_image = "#{request.base_url}/#{ENV['RAILS_RELATIVE_URL_ROOT']}"
    image_tag "#{url_image}/pdf/logo_customer_demo.png", style: 'max-width:120px;max-height:80px;', alt: ''
  end

  def customer_image_intern
    url_image = "#{request.base_url}/#{ENV['RAILS_RELATIVE_URL_ROOT']}"
    image_tag "#{url_image}/pdf/image_customer_demo.png", style: 'max-width:90%;', alt: ''
  end

  def customer_logo
    config = Admin::AdminConfig.first
    return customer_logo_intern unless config&.customer_logo&.attached?

    image_tag config&.customer_logo, style: 'max-width:120px;max-height:80px;', alt: ''
  end

  def customer_image
    config = Admin::AdminConfig.first
    return customer_image_intern unless config&.customer_image&.attached?

    image_tag config&.customer_image, style: 'max-width:90%;', alt: ''
  end

  def export_task_adv_icon(adv)
    url_image = "#{request.base_url}/#{ENV['RAILS_RELATIVE_URL_ROOT']}"
    image_tag "#{url_image}/pdf/icons/adv_#{adv}.png", size: '16'
  end

  def export_rca_adv_icon(adv)
    url_image = "#{request.base_url}/#{ENV['RAILS_RELATIVE_URL_ROOT']}"
    image_tag "#{url_image}/pdf/icons/rca_adv_#{adv}.png", size: '48x16'
  end

  def export_status_icon(stat)
    url_image = "#{request.base_url}/#{ENV['RAILS_RELATIVE_URL_ROOT']}"
    image_tag "#{url_image}/pdf/icons/#{stat}.png", size: '16'
  end

  # # Icons
  # def fa_icon_edit(more_class = '')
  #   fa_icon('edit', class: "icon-blue #{more_class}")
  # end

  # def fa_icon_edit_presenter(more_class = '')
  #   content_tag(:i, '', class: "fa fa-edit icon-blue #{more_class}")
  # end

  # def fa_icon_list(more_class = '')
  #   fa_icon('list-alt', type: :regular, class: "icon-blue #{more_class}")
  # end

  # def fa_icon_delete(more_class = '')
  #   fa_icon('trash-alt', type: :regular, class: "icon-red #{more_class}")
  # end

  # def fa_icon_delete_presenter(more_class = '')
  #   content_tag(:i, '', class: "far fa-trash-alt icon-red #{more_class}")
  # end

  # def fa_icon_hidden_presenter(more_class = '')
  #   # fa_icon('edit', style: 'visibility: none;')
  #   content_tag(:i, '', class: "far fa-trash-alt #{more_class}", style: 'visibility: hidden;')
  # end

  # def fa_icon_add(more_class = '')
  #   fa_icon('plus-circle', class: "icon-blue #{more_class}")
  # end

  # def fa_icon_add_presenter(more_class = '')
  #   content_tag(:i, '', class: "fa fa-plus-circle icon-blue #{more_class}")
  # end

  # def fa_icon_arrow_up(more_class = '')
  #   fa_icon('arrow-up', class: "icon-blue #{more_class}")
  # end

  # def fa_icon_arrow_down(more_class = '')
  #   fa_icon('arrow-down', class: "icon-blue #{more_class}")
  # end

  # def fa_icon_help(more_class = '')
  #   fa_icon('question-circle', class: "icon-blue #{more_class}")
  # end

  # def fa_icon_info(more_class = '')
  #   fa_icon('info-circle', class: "icon-blue #{more_class}")
  # end

  # def fa_icon_zoom_p(more_class = '')
  #   fa_icon('search-plus', class: "icon-blue #{more_class}")
  # end

  # def fa_icon_zoom_m(more_class = '')
  #   fa_icon('search-minus', class: "icon-blue #{more_class}")
  # end

  # def fa_icon_open_door(more_class = '')
  #   fa_icon('door-open', class: "icon-blue #{more_class}")
  # end

  # def fa_icon_status_presenter(more_class = '')
  #   content_tag(:i, '', class: "fa fa-circle #{more_class}")
  # end

  # def fa_icon_pen_presenter(more_class = '')
  #   content_tag(:i, '', class: "fa fa-pen icon-blue #{more_class}")
  # end

  # Best in place
  def best_in_place_display(name)
    !name.nil? ? (name.to_s + ' ' + fa_icon('edit', class: 'icon-blue')).html_safe : fa_icon('edit', class: 'icon-blue').html_safe
  end

  # Help popups
  def help_popup(title, content, more_class = '')
    content_tag(:a, tabindex: '0', role: 'button',
                    'data-toggle': 'popover-html',
                    title: title, 'data-content': content,
                    'data-trigger': 'focus',
                    class: 'btn-help') do
      fa_icon_help(more_class).html_safe
    end
  end

  # Info popups
  def info_popup(title, content, more_class = '')
    content_tag(:a, tabindex: '0', role: 'button',
                    'data-toggle': 'popover-html',
                    title: title, 'data-content': content,
                    # 'data-popover-class': 'popover-info',
                    'data-trigger': 'focus',
                    class: 'btn-help') do
      fa_icon_info(more_class).html_safe
    end
  end

  # Warning popups
  def warning_popup(title, content, more_class = '')
    content_tag(:a, tabindex: '0', role: 'button',
                    'data-toggle': 'popover-html',
                    title: title, 'data-content': content,
                    # 'data-popover-class': 'popover-warning',
                    'data-trigger': 'focus',
                    class: 'btn-help') do
      fa_icon_warning(more_class).html_safe
    end
  end

  def tool_help_text(tool, reference, title = nil)
    helper_text = Admin::HelperText.where(tool_class: tool, reference: reference)&.first
    return '' unless helper_text

    return help_popup(title || t(reference, scope: [:help_text]), helper_text.help_text)
  end

  # Button send mails
  def send_mail_submit_tag
    # ret = submit_tag fa_icon('arrow-circle-down', class: 'icon-grey icon-btn icon-margin') + t('tasks.send_emails'),
    #                  class: 'btn btn-outline-dark',
    #                  data: { disable_with: 'Sending mail....... please wait......'.html_safe }
    ret = button_tag(type: 'submit', class: 'btn btn-outline-dark') do
            fa_icon('arrow-circle-down', class: 'icon-grey icon-btn icon-margin') + t('tasks.send_emails')
          end
    ret += "<div id='email_response'></div>".html_safe
    return ret.html_safe
  end
end
