FactoryBot.define do
  factory :indicator, class: Tools::Indicator do
    tool_reference { 1 }
    tool_num { 1 }
    name { 'Indicator 1' }
  end
end
