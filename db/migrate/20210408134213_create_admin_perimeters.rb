class CreateAdminPerimeters < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_perimeters do |t|
      t.string :name
      t.string :ancestry

      t.timestamps
    end

    add_index :admin_perimeters, :ancestry
  end
end
