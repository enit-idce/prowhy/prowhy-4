require_dependency 'tools'

module Tools
  class DecMatLink < ApplicationRecord
    # include ToolMatrixable
    enum matrix_status: { in_progress: 0, validated: 1, invalidated: 2 }, _prefix: true

    belongs_to :decision_mat
    belongs_to :entry, polymorphic: true

    has_many :dec_mat_vals, dependent: :destroy
  end
end
