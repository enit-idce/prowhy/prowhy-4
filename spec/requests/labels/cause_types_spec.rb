require 'rails_helper'

RSpec.describe '/labels/cause_types', type: :request do
  # Labels::CauseType. As you add validations to Labels::CauseType, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) { {name: 'Label cause type 1'} }
  let(:invalid_attributes) { {name: nil} }

  let(:rca) { FactoryBot.create(:rca, methodology: methodology) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:user) { FactoryBot.create(:user) }

  context 'From logged user' do
    before :each do
      # binding.pry
      user.set_session(:active_step_tool, methodology.steps.first.step_tools.first.id)
      user.save!

      sign_in user
    end

    describe 'GET /index' do
      it 'renders a successful response' do
        get labels_cause_types_url, params: {rca: rca.id}
        expect(response).to be_successful
      end
    end

    describe 'GET /new' do
      it 'renders a successful response' do
        get new_labels_cause_type_url, params: { rca: rca.id }
        expect(response).to be_successful
      end
    end

    describe 'GET /edit' do
      it 'render a successful response' do
        labels_cause_type = Labels::CauseType.create! valid_attributes
        get edit_labels_cause_type_url(labels_cause_type)
        expect(response).to be_successful
      end
    end

    describe 'POST /create' do
      context 'with valid parameters' do
        it 'creates a new Labels::CauseType' do
          expect {
            post labels_cause_types_url, params: { labels_cause_type: valid_attributes }
          }.to change(Labels::CauseType, :count).by(1)
        end

        it 'redirects to the cause types list' do
          post labels_cause_types_url, params: { labels_cause_type: valid_attributes }
          expect(response).to redirect_to(labels_cause_types_url)
        end
      end

      context 'with invalid parameters' do
        it 'does not create a new Labels::CauseType' do
          expect {
            post labels_cause_types_url, params: { rca: rca.id, labels_cause_type: invalid_attributes }
          }.to change(Labels::CauseType, :count).by(0)
        end

        it 'renders a successful response (i.e. to display the new template)' do
          post labels_cause_types_url, params: { rca: rca.id, labels_cause_type: invalid_attributes }
          expect(response).to be_successful
        end
      end
    end

    describe 'PATCH /update' do
      context 'with valid parameters' do
        let(:new_attributes) { {name: 'ze new name'} }

        it 'updates the requested labels_cause_type' do
          labels_cause_type = Labels::CauseType.create! valid_attributes
          patch labels_cause_type_url(labels_cause_type), params: { labels_cause_type: new_attributes }
          labels_cause_type.reload
          expect(labels_cause_type.name).to eq('ze new name')
        end

        it 'redirects to the cause types list' do
          labels_cause_type = Labels::CauseType.create! valid_attributes
          patch labels_cause_type_url(labels_cause_type), params: { labels_cause_type: new_attributes }
          labels_cause_type.reload
          expect(response).to redirect_to(labels_cause_types_url)
        end
      end

      context 'with invalid parameters' do
        it 'renders a successful response (i.e. to display the edit template)' do
          labels_cause_type = Labels::CauseType.create! valid_attributes
          patch labels_cause_type_url(labels_cause_type), params: { labels_cause_type: invalid_attributes }
          expect(response).to be_successful
        end
      end
    end

    describe 'DELETE /destroy' do
      it 'destroys the requested labels_cause_type' do
        labels_cause_type = Labels::CauseType.create! valid_attributes
        expect {
          delete labels_cause_type_url(labels_cause_type)
        }.to change(Labels::CauseType, :count).by(-1)
      end

      it 'redirects to the labels_cause_types list' do
        labels_cause_type = Labels::CauseType.create! valid_attributes
        delete labels_cause_type_url(labels_cause_type)
        expect(response).to redirect_to(labels_cause_types_url)
      end
    end

    describe 'GET /cause_types_list' do
      it 'creates labels list and open js modal' do
        Labels::CauseType.create!(id: 1, name: 'Type1')
        step_tool = FactoryBot.create(:meth_step_tool,
                                      step: methodology.steps.first,
                                      tool_options: {labels: {'1': 1}})
        get labels_cause_types_list_url, params: {step_tool: step_tool.id}, xhr: true
        expect(response).to be_successful
      end
    end

    describe 'POST /cause_types_list_set' do
      it 'redirects to edit methodology step page' do
        Labels::CauseType.create!(id: 1, name: 'Type1')
        step_tool = FactoryBot.create(:meth_step_tool,
                                      step: methodology.steps.first,
                                      tool_options: {labels: {'1': 1}})
        post labels_cause_types_list_set_url, params: {step_tool: step_tool.id, labels_ids: [1]}
        expect(response).to redirect_to(edit_meth_methodology_path(step_tool.step.methodology, step: step_tool.step))
      end
    end
  end
end
