module ProwhyApi
  module V1
    module ApiDocs
      class RcaDoc
        def self.methodologies
          ret = "Methdology ID : \n"
          begin
            Meth::Methodology.rca_methodologies_all.each do |methodo|
              ret += "- #{methodo.id} : #{methodo.name}\n"
            end
          rescue StandardError
            ret += 'No methodology defined.'
          end

          return ret
        end
      end

      class CustomFieldDescDoc
        attr_accessor :custom_field_desc

        def initialize(custom_field_desc)
          @custom_field_desc = custom_field_desc
        end

        def type_api
          if %w[bigint integer].include?(custom_field_desc.custom_field_type.field_type)
            Integer
          elsif custom_field_desc.custom_field_type.field_type == 'date'
            Date
          else
            String
          end
        end

        def desc_api
          return "Custom field #{custom_field_desc.name}."
        end
      end

      class ProfilDoc
        def self.profils
          ret = "Profil ID : \n"
          begin
            Profil.all.each do |profil|
              ret += "- #{profil.id} : #{profil.name}\n"
            end
          rescue StandardError
            ret += 'No profil defined.'
          end

          return ret
        end
      end
    end
  end
end
