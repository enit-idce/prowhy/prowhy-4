require 'rails_helper'

RSpec.describe Rcas::UpdateAdvance, type: :service do
  subject(:service) do
    Rcas::UpdateAdvance.new(rca)
  end

  let(:rca) { FactoryBot.create(:rca, methodology: methodology) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }

  context 'without initializations' do
    describe 'update rca advancement' do
      it 'update advance_step and advance value' do
        service.call

        expect(rca.advance_step).to eq nil
        expect(rca.advance).to eq 0
      end
    end
  end

  context 'with initializations' do
    before :each do
      # add step2 to methodology
      # methodology.steps << FactoryBot.create(:meth_step, :with_tool, methodology: methodology)
      @step1 = methodology.steps_menu.first
      @step2 = methodology.steps_menu.second

      rca.initialize_rca_infos
      @rca_advance1 = rca.rca_advances.where(meth_step: @step1).first
      @rca_advance1.update(advance: 2)

      @rca_advance2 = rca.rca_advances.where(meth_step: @step2).first
      @rca_advance2.update(advance: 1)

      @step1.step_role = FactoryBot.create(:meth_step_role, name: 'Team', advance_level: 2)
      @step1.save
      @step2.step_role = FactoryBot.create(:meth_step_role, name: 'Closure', advance_level: 8)
      @step2.save
    end

    describe 'update rca advancement' do
      it 'update advance_step and advance value' do
        service.call

        expect(rca.advance_step).to eq @step1
        expect(rca.advance).to eq 2

        @rca_advance2.update(advance: 2)
        service.call

        expect(rca.advance_step).to eq @step2
        expect(rca.advance).to eq 8
      end
    end
  end
end
