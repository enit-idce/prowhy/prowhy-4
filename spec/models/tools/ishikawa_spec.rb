require 'rails_helper'

RSpec.describe Tools::Ishikawa, type: :model do
  subject(:ishikawa) do
    Tools::Ishikawa.create!(rca: rca, tool_reference: 1)
  end

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
  let(:cause) { Tools::Cause.create!(rca: rca, tool_reference: 1, name: 'Cause 1') }

  before :each do
    ishikawa.cause_link = Tools::CauseLink.create!(cause: cause, cause_tool: ishikawa)
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has one cause_link' do
        assc = Tools::Ishikawa.reflect_on_association(:cause_link)
        expect(assc.macro).to eq :has_one
      end
      it 'has one cause (=cause_head through cause_link)' do
        assc = Tools::Ishikawa.reflect_on_association(:cause)
        expect(assc.macro).to eq :has_one
      end

      it 'has many tools_ishi_branchs' do
        assc = Tools::Ishikawa.reflect_on_association(:tools_ishi_branchs)
        expect(assc.macro).to eq :has_many
      end
      it 'has many labels_cause_types (through tools_ishi_branchs)' do
        assc = Tools::Ishikawa.reflect_on_association(:labels_cause_types)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools_cause_classes' do
        assc = Tools::Ishikawa.reflect_on_association(:tools_cause_classes)
        expect(assc.macro).to eq :has_many
      end
      it 'has many labels_cause_types_used (through tools_cause_classes)' do
        assc = Tools::Ishikawa.reflect_on_association(:labels_cause_types_used)
        expect(assc.macro).to eq :has_many
      end
      it 'has many tools_causes (through tools_cause_classes)' do
        assc = Tools::Ishikawa.reflect_on_association(:tools_causes)
        expect(assc.macro).to eq :has_many
      end
    end
  end

  describe 'Create and search model : tool class functions' do
    before :each do
      ishikawa

      options = { rca: rca, tool_reference: 2 }
      step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 2, tool_num: 1)
      @ishikawa = Tools::Ishikawa.create_default!(options, step_tool)
    end

    it 'create_default : create ishikawa and first cause' do
      expect(@ishikawa.cause.name).to eq "#{rca.reference} - Rca 1"
      expect(@ishikawa.cause.parent).to be nil
      expect(@ishikawa.cause.tool_reference).to eq 2
      expect(@ishikawa.tool_reference).to eq 2
    end

    it 'search_default' do
      step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1)
      ishikawa_founded = Tools::Ishikawa.search_default(rca, 'tools_ishikawas', step_tool)

      expect(ishikawa_founded.rca).to eq rca
      expect(ishikawa_founded.tool_reference).to eq 1

      step_tool.tool_reference = 2
      ishikawa_founded = Tools::Ishikawa.search_default(rca, 'tools_ishikawas', step_tool)

      expect(ishikawa_founded.rca).to eq rca
      expect(ishikawa_founded.tool_reference).to eq 2
    end
  end

  it 'Assign Root cause to ishikawa' do
    expect(ishikawa.cause).to eq cause

    cause2 = Tools::Cause.create!(rca: rca, tool_reference: 1, name: 'Cause 2')
    ishikawa.assign_root_cause(cause2)
    expect(ishikawa.reload.cause).to eq cause2
  end

  describe 'Cause type for ishikawa' do
    before :each do
      ishikawa
      # Create Labels
      Labels::CauseType.create!([
                                  {id: 1, name: 'Mesure'},
                                  {id: 2, name: 'Matière'},
                                  {id: 3, name: 'Méthode'},
                                  {id: 4, name: 'Main d\'oeuvre'}
                                ])
    end

    it 'creates ishi branch' do
      # ishi_cause_type_id
      ishikawa.add_ishi_branch(Labels::CauseType.find(2))
      expect(ishikawa.tools_ishi_branchs.size).to eq 1
      expect(ishikawa.tools_ishi_branchs.first.labels_cause_type.name).to eq 'Matière'
    end

    context 'without step_tool' do
      it 'associate all labels' do
        ishikawa.associate_labels(nil)
        expect(ishikawa.tools_ishi_branchs.size).to eq 4
      end
    end

    context 'with step_tool' do
      it 'associate_labels with step_tool options (no delete)' do
        step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new,
                                       tool_reference: 1, tool_num: 1,
                                       tool_options: {labels: {'1': 1, '2': 3}})
        ishikawa.associate_labels(step_tool)

        expect(ishikawa.tools_ishi_branchs.size).to eq 2
        expect(ishikawa.labels_cause_types.map(&:id)).to eq [1, 3]
      end

      it 'associate_labels with step_tool options (no delete) with non existing label' do
        step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new,
                                       tool_reference: 1, tool_num: 1,
                                       tool_options: {labels: {'1': 1, '2': 12}})
        ishikawa.associate_labels(step_tool)

        expect(ishikawa.tools_ishi_branchs.size).to eq 1
        expect(ishikawa.labels_cause_types.map(&:id)).to eq [1]
      end

      it 'associate_labels with step_tool options (with delete)' do
        step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new,
                                       tool_reference: 1, tool_num: 1,
                                       tool_options: {labels: {'1': 1, '2': 3}})
        ishikawa.associate_labels(nil) # associate all labels

        cause2 = Tools::Cause.create!(rca: rca, tool_reference: 1, name: 'Cause 2', parent: cause)
        cause2.add_ishi_cause_type(ishikawa, 2)
        ishikawa.associate_labels(step_tool)

        expect(ishikawa.reload.tools_ishi_branchs.size).to eq 2
        expect(ishikawa.labels_cause_types.map(&:id)).to eq [1, 3]
      end
    end
  end
end
