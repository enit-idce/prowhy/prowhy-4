require 'rails_helper'

RSpec.feature 'Rca edition', type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:profil) { FactoryBot.create(:profil) }

  before :each do
    sign_in user
    Meth::StepRole.create!([
                             {id: 1, role_name: 'Info', name_fr: 'Info', name_en: 'Info', advance_level: 0},
                             {id: 2, role_name: 'Trs', name_fr: 'Outils Transversaux', name_en: 'Tranverse Tools', advance_level: 0},
                             {id: 3, role_name: 'Emergency', name_fr: 'Urgence', name_en: 'Emergency', advance_level: 1},
                             {id: 4, role_name: 'Team', name_fr: 'Equipe', name_en: 'Team', advance_level: 2},
                             {id: 5, role_name: 'Description', name_fr: 'Description', name_en: 'Description', advance_level: 3},
                             {id: 6, role_name: 'Containment', name_fr: 'Conservatoire', name_en: 'Containment', advance_level: 4},
                             {id: 7, role_name: 'Cause', name_fr: 'Recherche de causes', name_en: 'Cause Search', advance_level: 5},
                             {id: 8, role_name: 'Correction', name_fr: 'Correction', name_en: 'Correction', advance_level: 6},
                             {id: 9, role_name: 'Check', name_fr: 'Vérifiation', name_en: 'Check', advance_level: 7},
                             {id: 10, role_name: 'Prevent', name_fr: 'Prévention', name_en: 'Prevent recurency', advance_level: 8},
                             {id: 11, role_name: 'Close', name_fr: 'Clôture', name_en: 'Close', advance_level: 9},
                             {id: 12, role_name: 'Improve', name_fr: 'Améliorations', name_en: 'Improvement', advance_level: 0},
                             {id: 13, role_name: 'Other', name_fr: 'Autre', name_en: 'Something Else', advance_level: 0}
                           ])
    profil
  end

  scenario 'User edit profil authorizations', js: true do
    visit '/profil_authorizes'

    # Update
    # bip_bool(profil.profil_authorizes.first, :write)
    # bip_bool(profil.profil_authorizes.first, :read)
    find("span[id='best_in_place_profil_authorize_#{profil.profil_authorizes.first.id}_write']").click
    find("span[id='best_in_place_profil_authorize_#{profil.profil_authorizes.first.id}_read']").click

    # save_and_open_page

    expect(profil.profil_authorizes.first.read).to be false
    expect(profil.profil_authorizes.first.write).to be true

    page.accept_alert 'Are you sur you want to initialize this authorizations ?' do
      find("a[id='raz']").click
    end

    # binding.pry
    expect(page).to have_text('Done.')
    expect(profil.profil_authorizes.first.reload.read).to be true
    # save_and_open_page
    expect(profil.profil_authorizes.first.reload.write).to be false
  end
end
