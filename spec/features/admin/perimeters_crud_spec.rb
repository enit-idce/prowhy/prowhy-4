require 'rails_helper'

RSpec.feature 'Perimeters CRUD', type: :feature do
  let(:user) { FactoryBot.create(:user) }
  # let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:perim_all) { FactoryBot.create(:perimeter, name: 'ALL') }
  let(:perimeter) { FactoryBot.create(:perimeter, name: 'Interne', parent_id: perim_all.id) }

  before :each do
    user.add_perimeter perimeter.id
    user.set_preference(:perimeter, perimeter.id)
    user.reload
    user.save!
    sign_in user
  end

  scenario 'User create a perimeter', js: true do
    visit '/admin/perimeters'

    # click_link 'New Methodology'
    find("a[href='#{new_admin_perimeter_path(admin_perimeter: {parent_id: perim_all.id})}']").click

    fill_in 'Name', with: 'CUSTOMERS'
    click_button 'Create Scope'

    expect(page).to have_text('Scope was successfully created.')
    expect(page).to have_current_path(admin_perimeters_path)
  end

  scenario 'Edit and update a perimeter', js: true do
    visit '/admin/perimeters'
    # save_and_open_page

    # click_link 'New Methodology'
    find("a[href='#{edit_admin_perimeter_path(id: perimeter.id)}']").click

    fill_in 'Name', with: 'INTERNAL (in english)'
    click_button 'Update Scope'

    expect(page).to have_text('Scope was successfully updated.')
    expect(page).to have_text('INTERNAL (in english)')
    expect(page).to have_current_path(admin_perimeters_path)
  end

  scenario 'Delete a perimeter', js: true do
    visit '/admin/perimeters'

    # click_link 'New Methodology'
    page.accept_alert 'Are you sure ?' do
      find("a[href='#{admin_perimeter_path(id: perimeter.id)}'][data-method='delete']").click
    end

    expect(page).to have_text('Scope was successfully destroyed.')
    expect(page).to have_current_path(admin_perimeters_path)
  end
end
