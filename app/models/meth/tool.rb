require_dependency 'meth'

module Meth
  class Tool < ApplicationRecord
    include MethConcern

    translates :name, fallback: :any

    belongs_to :activity # , class_name: 'Meth::Activity'
    has_many :step_tools, dependent: :destroy
    has_many :steps, through: :step_tools

    validates :name, presence: true
    validates :tool_name, presence: true

    # serialize :tool_options
    def tool_options
      @tool_options ||= super&.deep_symbolize_keys!
    end

    after_initialize do
      if new_record?
        # Initialisation des tool_options
        self.tool_options ||= {}
      end
    end

    def self.find_or_create_custom_field
      return Tool.where(tool_name: 'CustomField').first || Tool.create!(name_fr: 'Champs personnalisés',
                                                                        name_en: 'Custom Fields',
                                                                        tool_name: 'CustomField',
                                                                        activity: Activity.find_or_create_infos)
    end

    def self.find_or_create_tool(options)
      return Tool.where(options).first || Tool.create!(name_fr: options[:tool_name].capitalize,
                                                       name_en: options[:tool_name].capitalize,
                                                       tool_name: options[:tool_name],
                                                       activity: Activity.find_or_create_infos)
    end
  end
end
