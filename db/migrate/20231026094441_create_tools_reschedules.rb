class CreateToolsReschedules < ActiveRecord::Migration[6.1]
  def change
    create_table :tools_reschedules do |t|
      t.references :tools_task, null: false, foreign_key: true
      t.date :date
      t.text :comment

      t.timestamps
    end
  end
end
