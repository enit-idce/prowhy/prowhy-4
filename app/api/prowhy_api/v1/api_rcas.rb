require 'doorkeeper/grape/helpers'
require 'devise'
require_dependency 'rcas/filter_rcas'

module ProwhyApi
  module V1
    class ApiRcas < Grape::API
      # helpers Helpers::ApiHelpers

      # before do
      #   doorkeeper_authorize! unless user_signed_in?
      #   # Pour l'application (via React) : test si user is logged with devise (user_signed_in)
      #   # Sinon : pour l'Api en externe (api_doc) test le token (doorkeeper)
      # end

      resource :rcas do
        desc 'Return nb Rcas'
        get '/nb' do
          nb_rcas = policy_scope(Rca).count

          authorize Rca, :index?

          { nb_rcas: nb_rcas }
        end

        desc 'Return all Rcas.'
        paginate
        params do
          optional :options, type: Hash, desc: 'Options : {custom_field: true, referent: profil_ID}.' do
            optional :custom_field, type: Boolean, desc: 'Option : Expose custom field attributes.', default: true
            optional :perimeters, type: Boolean, desc: 'Option : Expose perimeters associated to Rca.', default: false
            optional :referent, type: Integer, documentation: {desc: 'Option : Expose referent : enter profil ID.'}, default: 0
            optional :advancement, type: Boolean, desc: 'Option : Expose advancement (for rca and each step).', default: false
            optional :teams, type: Boolean, desc: 'Option : Expose team.', default: false
          end
        end
        get do
          # @rcas = policy_scope(Rca)
          @rcas = if !params['per_page'].nil?
                    paginate policy_scope(Rca).order(id: :asc)
                  else
                    policy_scope(Rca)
                  end

          authorize Rca, :index?

          options = params[:options] || {}

          present @rcas, with: Entities::Rca, with_id: true,
                         with_referent: options[:referent],
                         with_custom_field: options[:custom_field],
                         with_perimeters: options[:perimeters],
                         with_advancement: options[:advancement],
                         with_teams: options[:teams]
        end

        # FILTER RCAS
        desc 'Return Filtered list of Rcas.'
        params do
          optional :rca, type: Hash, desc: 'Rca filters.' do
            optional :reference, type: String
            optional :methodology, type: Integer
            optional :status_adv, type: String, desc: 'Advancement : 1=ALL, 2=OPEN, 3=CLOSED'
            optional :adv_duration, type: Integer, desc: 'Duration max'
            optional :person_id, type: Integer, desc: 'Search person / linked with person_role'
            optional :person_role, type: Integer
            optional :date_begin, type: Date, desc: 'Search Rcas created after date (DD/MM/YYYY).'
            optional :date_end, type: Date, desc: 'Search Rcas created before date (DD/MM/YYYY).'
          end
          optional :custom_field, type: Hash, desc: 'Custom fields filters.' do
            CustomField.column_names.each do |cf|
              next unless cf.include? 'custom_'

              cfdesc = Meth::CustomFieldDesc.where(internal_name: cf)&.first
              next unless cfdesc

              cfdescdoc = ApiDocs::CustomFieldDescDoc.new(cfdesc)

              optional cf.intern,
                       type: cfdescdoc.type_api,
                       desc: cfdescdoc.desc_api + " (#{cfdescdoc.type_api})"
            end
          rescue StandardError
            puts 'INFO: error init Rcas API.'.blue
          end
        end
        get :filter do
          authorize Rca, :index?

          custom_preload_list = Meth::CustomFieldDesc.custom_preload_list_values
          @rcas = policy_scope(Rca).includes(:methodology, custom_field: custom_preload_list, advance_step: [:step_role]).with_rich_text_description.order(created_at: :desc)

          # Filtrage des Rcas (search)
          search_params = {rca_search: params[:rca] || {}, custom_field: params[:custom_field] || {}}

          search_params[:rca_search][:methodology] = [search_params[:rca_search][:methodology]] if search_params[:rca_search][:methodology]
          @rcas = Rcas::FilterRcas.new(@rcas).call(search_params)

          present @rcas.map(&:id)
        end

        # LIST RCAS
        desc 'Return Rcas [ids]'
        paginate
        params do
          requires :rca_ids, type: String, desc: 'Rca IDs. Exemple : [1, 2, 10] OR 1, 10, 12'
          optional :options, type: Hash, desc: 'Options : {custom_field: true, referent: profil_ID}.' do
            optional :custom_field, type: Boolean, desc: 'Option : Expose custom field attributes.', default: true
            optional :perimeters, type: Boolean, desc: 'Option : Expose perimeters associated to Rca.', default: false
            optional :referent, type: Integer, documentation: {desc: 'Option : Expose referent : enter profil ID.'}, default: 0
            optional :advancement, type: Boolean, desc: 'Option : Expose advancement (for rca and each step).', default: false
            optional :teams, type: Boolean, desc: 'Option : Expose team.', default: false
          end
        end
        get :list do
          rca_ids = params[:rca_ids].gsub('[', '')
          rca_ids.gsub!(']', '')
          rca_ids = rca_ids.split(',')

          @rcas = if !params['per_page'].nil?
                    paginate policy_scope(Rca).where(id: rca_ids)
                  else
                    policy_scope(Rca).where(id: rca_ids)
                  end

          options = params[:options] || {}

          present @rcas, with: Entities::Rca, with_id: true,
                         with_referent: (options[:referent] || 0),
                         with_custom_field: options[:custom_field],
                         with_perimeters: options[:perimeters],
                         with_advancement: options[:advancement],
                         with_teams: options[:teams]
        end

        # GET ONE RCA
        desc 'Return Rca :id'
        params do
          requires :id, type: Integer, desc: 'Rca ID.'
          optional :options, type: Hash, desc: 'Options : {custom_field: true, referent: profil_ID}.' do
            optional :custom_field, type: Boolean, desc: 'Option : Expose custom field attributes.', default: true
            optional :perimeters, type: Boolean, desc: 'Option : Expose perimeters associated to Rca.', default: false
            optional :referent, type: Integer, documentation: {desc: 'Option : Expose referent : enter profil ID.'}, default: 0
            optional :advancement, type: Boolean, desc: 'Option : Expose advancement (for rca and each step).', default: false
            optional :teams, type: Boolean, desc: 'Option : Expose team.', default: false
          end
        end
        route_param :id do
          get do
            # custom_preload_list = Meth::CustomFieldDesc.custom_preload_list_values

            # @rca = Rca.find(params[:id]) # where(id: params[:id])&.includes(:methodology, custom_field: custom_preload_list)&.with_rich_text_description&.first
            @rca = policy_scope(Rca).find(params[:id])

            authorize @rca, :show?

            # binding.pry
            options = params[:options] || {}

            present @rca, with: Entities::Rca, with_id: true,
                          with_referent: (options[:referent] || 0),
                          with_custom_field: options[:custom_field],
                          with_perimeters: options[:perimeters],
                          with_advancement: options[:advancement],
                          with_teams: options[:teams]
          end
        end

        desc 'Create a Rca.'
        params do
          # requires :rca, type: Hash, documentation: {param_type: 'body'} do
          requires :title, type: String, desc: 'Rca ID.', documentation: {param_type: 'body'}
          requires :methodology_id, type: Integer, desc: ApiDocs::RcaDoc.methodologies, documentation: {param_type: 'body'}
          optional :description, type: String, desc: 'Rca Detailed description.', documentation: {param_type: 'body'}

          optional :custom_field_attributes, type: Hash, documentation: {param_type: 'body'} do
            CustomField.column_names.each do |cf|
              next unless cf.include? 'custom_'

              cfdesc = Meth::CustomFieldDesc.where(internal_name: cf)&.first
              next unless cfdesc

              cfdescdoc = ApiDocs::CustomFieldDescDoc.new(cfdesc)

              # optional (cf+"-#{cfdesc.name}").intern,
              #          type: cfdescdoc.type_api, # cfdesc.meth_custom_field_type_api,
              #          documentation: { desc: cfdescdoc.desc_api, # cfdesc.meth_custom_field_desc_api,
              #                           param_type: 'body' }

              optional cf.intern,
                       type: cfdescdoc.type_api,
                       documentation: { desc: cfdescdoc.desc_api,
                                        param_type: 'body' }
            end
          rescue StandardError
            puts 'INFO: error init Rcas API.'.blue
          end
          # end
        end
        post do
          @rca = Rca.new(params) # [:rca])
          authorize @rca, :create?
          # puts params
          @rca.save!

          @rca.rca_creation_init(current_user)

          present @rca, with: Entities::Rca, with_id: true, with_custom_field: true
        end
        # desc 'Create a Rca.',
        #   entity: RcasPresenter,
        #   params: RcasPresenter.documentation
        # post do
        #   # binding.pry
        #   @rca = Rca.new(params[:rca])
        #   authorize @rca, :create?
        #   puts params
        #   @rca.save!

        #   @rca.rca_creation_init(current_user)

        #   present @rca, with: RcasPresenter, with_id: 'true'
        # end

        desc "Update Rca :id.\n" \
             'Warning : only send attributes you want to update.'
        params do
          requires :id, type: Integer, desc: 'Rca ID.'

          optional :title, type: String, desc: 'Rca ID.', documentation: {param_type: 'body', desc: 'Rca ID.'}
          optional :description, desc: 'Rca Detailed description.', documentation: {param_type: 'body'}

          optional :custom_field_attributes, type: Hash, documentation: {param_type: 'body'} do
            CustomField.column_names.each do |cf|
              next unless cf.include? 'custom_'

              cfdesc = Meth::CustomFieldDesc.where(internal_name: cf)&.first
              next unless cfdesc

              cfdescdoc = ApiDocs::CustomFieldDescDoc.new(cfdesc)

              # optional (cf+"-#{cfdesc.name}").intern,
              #          type: cfdescdoc.type_api, # cfdesc.meth_custom_field_type_api,
              #          documentation: { desc: cfdescdoc.desc_api, # cfdesc.meth_custom_field_desc_api,
              #                           param_type: 'body' }

              optional cf.intern,
                       type: cfdescdoc.type_api,
                       documentation: { desc: cfdescdoc.desc_api,
                                        param_type: 'body' }
            end
          rescue StandardError
            puts 'INFO: error init Rcas API.'.blue
          end

          optional :referent, type: Hash, documentation: {param_type: 'body'} do
            optional :person_id, type: Integer, desc: 'Peson ID.'
            optional :profil_id, type: Integer, desc: ApiDocs::ProfilDoc.profils
          end

          # optional :teams_attributes, type: Array[JSON], documentation: {param_type: 'body'} do
          #   optional :person_id, type: Integer, desc: 'Peson ID.'
          #   optional :profil_id, type: Integer, desc: ApiDocs::ProfilDoc.profils
          #   optional :tool_reference, type: Integer, desc: 'Tool reference (default=1).', default: 1
          #   optional :tool_num, type: Integer, desc: 'Tool num (default=1).', default: 1
          # end
        end
        route_param :id do
          patch do
            # binding.pry
            @rca = Rca.find(params[:id])

            new_params = {}
            new_params[:title] = params[:title] if params.key?(:title)
            new_params[:description] = params[:description] if params.key?(:description)
            new_params[:custom_field_attributes] = params[:custom_field_attributes] || {}
            new_params[:custom_field_attributes][:id] = @rca.custom_field&.id

            # if params[:teams_attributes]
            #   new_params[:tools_teams_attributes] = params[:teams_attributes]
            #   new_params[:tools_teams_attributes]&.each do |team|
            #     team[:rca_id] = @rca.id
            #   end
            # end

            if @rca.update(new_params)

              # binding.pry

              if params[:referent]
                referent = @rca.get_person_from_team(params[:referent][:profil_id])
                person = Person.find(params[:referent][:person_id])

                if referent
                  referent.person = person
                  referent.save!
                else
                  @rca.add_person_to_team_by_id(person, params[:referent][:profil_id])
                end

                # binding.pry
              end

              present @rca, with: Entities::Rca, with_id: true,
                            with_custom_field: true,
                            with_referent: (params[:referent] ? params[:referent][:profil_id] : 0)
            else
              error!({ error: @rca.errors.full_messages}, 400)
            end
          end
        end

        # DELETE
        desc 'Delete Rca :id. Be carefull, no undo !'
        params do
          requires :id, type: Integer, desc: 'Rca ID.'
        end
        route_param :id do
          delete do
            @rca = Rca.find(params[:id])
            authorize @rca, :destroy?

            # binding.pry

            # @rca.destroy
            @rca.status_deleted!
            @rca.save!
          end
        end
      end
    end
  end
end
