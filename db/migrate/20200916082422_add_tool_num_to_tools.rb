class AddToolNumToTools < ActiveRecord::Migration[6.0]
  def change
    add_column :meth_step_tools, :tool_num, :integer, default: 1, after: :tool_reference

    add_column :tools_qqoqcps, :tool_num, :integer, default: 1, after: :tool_reference
    # add_column :tools_causes, :tool_num, :integer, default: 1, after: :tool_reference
    add_column :tools_tasks, :tool_num, :integer, default: 1, after: :tool_reference
    add_column :tools_ishikawas, :tool_num, :integer, default: 1, after: :tool_reference
  end
end
