module RcasHelper
  def step_advancement(rca, step)
    adv = rca.advancement(step)

    return export_rca_adv_icon(adv) if @export_pdf

    ret = '<span style="padding:2px;background-color:#eee;border-radius:3px;">'
    ret += fa_icon('circle', style: 'margin-right:3px;', class: adv==0 ? 'rca-advance-red' : 'rca-advance-grey')
    ret += fa_icon('play-circle', style: 'margin-right:3px;', class: adv==1 ? 'rca-advance-orange' : 'rca-advance-grey')
    ret += fa_icon('check-circle', class: adv==2 ? 'rca-advance-green' : 'rca-advance-grey')

    ret.html_safe
  end

  def helper_advance_rca_show(rca)
    return '' unless rca

    return '' unless rca.methodology.steps_menu

    return '' if rca.methodology.steps_menu.empty?

    level = 0
    if rca.advance_step_id
      rca.methodology.steps_menu.each do |step|
        level += 1
        break if step.id == rca.advance_step_id
      end
    end

    step_perc = ((level.to_f / rca.methodology.steps_menu.size) * 100).to_i

    color_green = '#27AD0B' # #05ae05'
    color_red = 'rgba(62, 74, 91, 0.20)' # #e6b3b3'

    ret = "<span style='width:152px;height:5px;border:none;border-radius:3px;background:linear-gradient(to right, #{color_green} 0%, #{color_green} #{step_perc}%, #{color_red} #{step_perc}%, #{color_red} 100%);display: inline-block;'>"
    ret +='</span>'

    ret.html_safe
  end

  def rca_reference_show(rca)
    return unless rca.tools_rca_list&.rca

    info_popup('Reference', rca.tools_rca_list.rca.reference+' '+rca.tools_rca_list.rca.title)
  end
end
