module SendReminders
  def self.run_reminders
    admin_config = Admin::AdminConfig.first
    return unless admin_config&.task_mails

    mails_config = admin_config.task_mails.deep_symbolize_keys
    return unless mails_config[:activate] == 'true'

    I18n.locale = mails_config[:locale]&.intern || :fr

    tasks = Tools::Task.from_valid_rca.includes(:responsible_action, :responsible_rca).where.not(advancement: 4).where.not(date_due: nil)

    # Tasks late
    if mails_config[:late].to_i > 0
      puts '=============================================== Actions en retard'.green
      tasks_late = tasks.where('date_due < ?', Date.today)

      tasks_late.each do |task|
        next unless task.responsible_rca&.email || task.responsible_action&.email

        next unless (Date.today-task.date_due)%mails_config[:late].to_i == 0

        puts "Send an email for task #{task.name} : #{task.date_due} / Advancement #{task.advancement}".yellow
        puts "#{task.responsible_rca&.email} / #{task.responsible_action&.email}"

        SendMails.task_send_email(task.id, rca: true, action: true, message: I18n.t('task_late', scope: ['mailer', 'task'], days: (Date.today - task.date_due).to_i))
      end
    end

    # Tasks à échéance aujourd'hui
    if mails_config[:dday] == 'true'
      puts "=============================================== Action à échéance aujourd'hui".green
      tasks_late = tasks.where(date_due: Date.today)

      tasks_late.each do |task|
        puts "Send an email for TODAY task #{task.name} : #{task.date_due} / Advancement #{task.advancement}".yellow
        puts task.responsible_rca&.email
        puts task.responsible_action&.email

        SendMails.task_send_email(task.id, rca: true, action: true, message: I18n.t('task_today', scope: ['mailer', 'task']))
      end
    end

    # Tasks à échéence dans N days
    ndays = mails_config[:ndays]&.split(' ')&.map(&:to_i)

    return if ndays.empty?

    dates = []
    ndays.each do |nday|
      dates << Date.today+nday
    end
    puts '=============================================== Action à échéance bientôt'.green

    tasks_late = tasks.where(date_due: dates)

    tasks_late.each do |task|
      puts "Send an email for BIENTOT task #{task.name} : #{task.date_due} / Advancement #{task.advancement}".yellow
      puts task.responsible_rca&.email
      puts task.responsible_action&.email

      SendMails.task_send_email(task.id, rca: true, action: true, message: I18n.t('task_soon', scope: ['mailer', 'task'], days: (task.date_due - Date.today).to_i))
    end
  end
end
