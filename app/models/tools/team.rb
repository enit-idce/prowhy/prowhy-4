require_dependency 'tools'

module Tools
  class Team < ApplicationRecord
    include ToolConcern
    # belongs_to :rca
    belongs_to :person
    belongs_to :profil

    # TODO : create a Team (profil=author) when creating a RCA
    def self.create_default!(_options, _step_tool)
      []
    end

    def self.search_default(rca, _tool_name, step_tool)
      # step_tool.tool_options ||= {}
      # options_for_search = step_tool.tool_options.merge(tool_reference: step_tool.tool_reference,
      #                                                   tool_num: step_tool.tool_num)
      return rca.tools_teams.includes(:person, :profil).where(tool_reference: step_tool.tool_reference, tool_num: step_tool.tool_num)
    end

    def self.create_search_multiple?
      return true
    end
  end
end
