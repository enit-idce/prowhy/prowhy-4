module Rcas
  class UpdateActionsTeam
    def initialize(obj)
      @obj = obj
    end

    def call
      team_actions = all_team_actions
      people_actions = team_actions.map(&:person)

      # all_resp_act = @obj.tools_task.joins(:responsible_action).where(responsible_actions: {})
      # all_resp_act = Person.joins(:tasks_resp_action, :tasks_resp_rca).where(tasks_resp_actions: {rca_id: @obj.id}).or(tasks_resp_rcas: {rca_id: @obj.id})
      all_resps = all_team_resp

      team_actions.each do |team|
        next if all_resps&.include?(team.person)

        # La personne n'est pas resp actions => destroy le team
        team.destroy
      end

      profil_res_act = nil
      all_resps.each do |resp|
        next if people_actions&.include?(resp)

        # La personne est resp action => on l'ajoute dans la Team
        profil_res_act ||= Profil.where(profil_name: 'resp_act').first
        Tools::Team.create!(rca: @obj, tool_reference: 1, tool_num: 1, person_id: resp.id, profil: profil_res_act)
      end

      # binding.pry
    end

    private

    def all_team_actions
      return @obj.tools_teams.includes(:person).joins(:profil).where(profils: { profil_name: 'resp_act'})
    end

    def all_team_resp
      all_resps = @obj.tools_tasks.includes(:responsible_action, :responsible_rca).flat_map { |t| [t.responsible_action, t.responsible_rca] }
      all_resps&.uniq!
      all_resps&.delete(nil)

      return all_resps
    end
  end
end
