require 'rails_helper'

RSpec.describe Tools::TeamsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/tools/teams').to route_to('tools/teams#index')
    end

    it 'routes to #new' do
      expect(get: '/tools/teams/new').to route_to('tools/teams#new')
    end

    it 'routes to #show' do
      expect(get: '/tools/teams/1').to route_to('tools/teams#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/tools/teams/1/edit').to route_to('tools/teams#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/tools/teams').to route_to('tools/teams#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/tools/teams/1').to route_to('tools/teams#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/tools/teams/1').to route_to('tools/teams#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/tools/teams/1').to route_to('tools/teams#destroy', id: '1')
    end
  end
end
