module Tools
  class SaveImageService
    attr_accessor :current_user

    def initialize(c_user)
      @current_user = c_user
    end

    # def save_image(params)
    #   login_user = current_user.username
    #   sub_dir = params[:subdir] || 'other'
    #   dirname = "public/images/#{sub_dir}"
    #   img_name = (params[:imgname] || 'other_img') + '.png'

    #   FileUtils.mkdir('public/images') unless File.directory?('public/images')
    #   FileUtils.mkdir(dirname) unless File.directory?(dirname)
    #   FileUtils.mkdir("#{dirname}/#{login_user}") unless File.directory?("#{dirname}/#{login_user}")

    #   filename = "#{dirname}/#{login_user}/#{img_name}"
    #   File.delete(filename) if File.exist?(filename)

    #   File.open(filename, 'wb') do |f|
    #     f.write params[:image].read
    #   end
    #   # binding.pry
    #   img_data = {image: ActionController::Base.helpers.asset_path("/ishikawas/#{login_user}/#{img_name}") }

    #   return img_data
    # end

    def save_image_dataurl(params)
      data = params[:image]
      # remove all extras except data
      # binding.pry

      return unless data['data:image/png;base64,'.length..]

      image_data = Base64.decode64(data['data:image/png;base64,'.length..])

      login_user = current_user.username
      sub_dir = params[:subdir] || 'other'
      dirname = "public/images/#{sub_dir}"
      img_name = (params[:filename] || 'other_img') + '.png'

      FileUtils.mkdir('public/images') unless File.directory?('public/images')
      FileUtils.mkdir(dirname) unless File.directory?(dirname)
      FileUtils.mkdir("#{dirname}/#{login_user}") unless File.directory?("#{dirname}/#{login_user}")

      filename = "#{dirname}/#{login_user}/#{img_name}"
      File.delete(filename) if File.exist?(filename)

      File.open(filename, 'wb') do |f|
        f.write image_data
      end
    end
  end
end
