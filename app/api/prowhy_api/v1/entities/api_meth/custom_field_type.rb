module ProwhyApi
  module V1
    module Entities
      module ApiMeth
        class CustomFieldType < Grape::Entity
          expose :field_type, as: 'type'
        end
      end
    end
  end
end
