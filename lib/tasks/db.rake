namespace :db do
  # desc 'Checks to see if the database exists'
  # task :exists do
  #   Rake::Task['environment'].invoke
  #   ActiveRecord::Base.connection
  # rescue
  #   exit 3
  # else
  #   exit 4
  # end

  desc 'Create/Migrate/Seed if db do not exists, Migrate-with-data otherwise'
  task :docker_update do
    Rake::Task['environment'].invoke

    ActiveRecord::Base.connection
  rescue
    puts '************************************ Create database ***********************'
    Rake::Task['db:create'].execute
    puts '************************************ Migrate ***********************'
    Rake::Task['db:migrate:with_data'].execute
    puts '************************************ Seed datas ***********************'
    Rake::Task['db:seed'].execute
    puts 'Database done.'
  else
    puts '************************************ Update DB : migrate with datas ***********************'
    Rake::Task['db:migrate:with_data'].invoke
    # puts '************************************ Seed datas ***********************'
    # Rake::Task['db:seed'].invoke
    puts 'Update database done.'
  end
end
