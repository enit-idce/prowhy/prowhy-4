require 'doorkeeper/grape/helpers'
require 'devise'

module ProwhyApi
  module V1
    module ApiTools
      class ApiImages < Grape::API
        # helpers Helpers::ApiHelpers

        # before do
        #   doorkeeper_authorize! unless user_signed_in?
        # end

        desc "ToolImage requests\n" \
             " - create an image linked to Rca\n"
        resource :images do
          desc 'Return all Images linked to Rca #rca_id.'
          params do
            requires :rca_id, type: Integer, desc: 'Rca ID.'
          end
          get do
            @rca = Rca.find(params[:rca_id])
            authorize @rca, :show?

            @images = @rca.tools_images

            present @images, with: Entities::ApiTools::Image, with_id: 'true'
          end

          desc 'Return Image :id'
          params do
            requires :id, type: Integer, desc: 'Image ID.'
          end
          route_param :id do
            get do
              @image = Tools::Image.find(params[:id])
              authorize @image, :show?

              present @image, with: Entities::ApiTools::Image # , with_id: 'false'
            end
          end

          desc 'Create an image'
          params do
            # requires :name, type: String, desc: 'Image title.', documentation: {param_type: 'body'}
            # requires :rca_id, type: Integer, desc: 'Rca ID.', documentation: {param_type: 'body'}
            # requires :file, type: File # , documentation: {param_type: 'body'}
            requires :name, type: String, desc: 'Image title.'
            requires :rca_id, type: Integer, desc: 'Rca ID.'
            requires :file, type: File
            optional :resize, type: Boolean, desc: 'Resize image if too big ?', default: false
          end
          # curl -v -X POST -F name=BLABLA -F rca_id=1 -F file=@spec/fixtures/images/img1.png "http://localhost:3000/api/v1/images"
          post do
            # if params[:resize]
            config = Admin::AdminConfig.first
            max_size = config&.options&.[]('image_max_size') || 1
            size = File.size(params[:file][:tempfile])

            if size / 1_000_000.0 > max_size.to_f
              image = MiniMagick::Image.new(params[:file][:tempfile].path)
              while (image.size / 1_000_000.0 > max_size.to_f)
                puts 'Resize image'.yellow
                image.resize "#{image.width/2}x#{image.height/2}"
              end
            end
            # end

            params_image = {}
            params_image[:name] = params[:name]
            params_image[:rca_id] = params[:rca_id]
            params_image[:file] = {io: File.open(params[:file][:tempfile]), filename: params[:file][:filename], content_type: params[:file][:type]}

            @tools_image = Tools::Image.new(params_image)

            authorize @tools_image, :create?

            @tools_image.save!

            present @tools_image, with: Entities::ApiTools::Image
          end

          # DELETE
          desc 'Delete Image :id. Be carefull, no undo !'
          params do
            requires :id, type: Integer, desc: 'Image ID.'
          end
          route_param :id do
            delete do
              @image = Tools::Image.find(params[:id])

              # binding.pry
              authorize @image, :destroy?

              @image.destroy
            end
          end
        end

      end
    end
  end
end
