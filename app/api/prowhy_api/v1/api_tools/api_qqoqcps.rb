require 'doorkeeper/grape/helpers'
require 'devise'

module ProwhyApi
  module V1
    module ApiTools
      class ApiQqoqcps < Grape::API
        # helpers Helpers::ApiHelpers

        # before do
        #   doorkeeper_authorize! unless user_signed_in?
        # end

        desc "ToolQqoqcp requests\n" \
             " - get qqoqcp linked to Rca\n" \
             " - create a qqoqcp linked to Rca\n"
        resource :qqoqcps do
          desc 'Return all Qqoqcps linked to Rca #rca_id.'
          params do
            requires :rca_id, type: Integer, desc: 'Rca ID.'
          end
          get do
            @rca = Rca.find(params[:rca_id])
            authorize @rca, :show?

            @qqoqcps = @rca.tools_qqoqcps

            present @qqoqcps, with: Entities::ApiTools::Qqoqcp, with_id: 'true'
          end

          desc 'Return Qqoqcp :id'
          params do
            requires :id, type: Integer, desc: 'Qqoqcp ID.'
          end
          route_param :id do
            get do
              @qqoqcp = Tools::Qqoqcp.find(params[:id])
              authorize @qqoqcp, :show?

              present @qqoqcp, with: Entities::ApiTools::Qqoqcp, with_rca_id: 'true'
            end
          end

          # CREATE
          # desc 'Create a qqoqcp'
          # params do
          #   # requires :name, type: String, desc: 'Qqoqcp title.', documentation: {param_type: 'body'}
          #   # requires :rca_id, type: Integer, desc: 'Rca ID.', documentation: {param_type: 'body'}
          #   # requires :file, type: File # , documentation: {param_type: 'body'}
          #   requires :name, type: String, desc: 'Qqoqcp title.'
          #   requires :rca_id, type: Integer, desc: 'Rca ID.'
          # end
          # # curl -v -X POST -F name=BLABLA -F rca_id=1 -F file=@spec/fixtures/qqoqcps/img1.png "http://localhost:3000/api/v1/qqoqcps"
          # post do
          #   params_qqoqcp = {}
          #   params_qqoqcp[:name] = params[:name]
          #   params_qqoqcp[:rca_id] = params[:rca_id]

          #   @tools_qqoqcp = Tools::Qqoqcp.new(params_qqoqcp)

          #   authorize @tools_qqoqcp, :create?

          #   @tools_qqoqcp.save!

          #   present @tools_qqoqcp, with: Entities::ApiTools::Qqoqcp
          # end

          # DELETE
          # desc 'Delete Qqoqcp :id. Be carefull, no undo !'
          # params do
          #   requires :id, type: Integer, desc: 'Qqoqcp ID.'
          # end
          # route_param :id do
          #   delete do
          #     @qqoqcp = Tools::Qqoqcp.find(params[:id])

          #     # binding.pry
          #     authorize @qqoqcp, :destroy?

          #     @qqoqcp.destroy
          #   end
          # end
        end

      end
    end
  end
end
