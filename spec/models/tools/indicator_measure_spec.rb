require 'rails_helper'

RSpec.describe Tools::IndicatorMeasure, type: :model do
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to indicator' do
        assc = Tools::IndicatorMeasure.reflect_on_association(:indicator)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end
end
