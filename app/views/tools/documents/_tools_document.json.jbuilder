json.extract! tools_document, :id, :rca_id, :tool_reference, :tool_num, :name, :file_name, :created_at, :updated_at
json.url tools_document_url(tools_document, format: :json)
