module Meth
  class ListContentPolicy < Meth::MethPolicy
    attr_reader :user, :record

    def initialize(user, record)
      @user = user
      @record = record
    end

    def list_content_up?
      update?
    end

    def list_content_down?
      update?
    end

    # def index?
    #   show?
    # end

    # def show?
    #   user.has_role?(:admin) || user.has_role?(:super_admin)
    # end

    def create?
      user.has_role?(:admin) || user.has_role?(:super_admin)
    end

    # def new?
    #   create?
    # end

    # def update?
    #   create?
    # end

    # def edit?
    #   create?
    # end

    # def destroy?
    #   create?
    # end
  end
end
