require_dependency 'meth'

module Meth
  class ToolCustomField < ApplicationRecord
    include MethConcern

    belongs_to :step_tool, class_name: 'Meth::StepTool' # , -> { includes(:tool).where(tools: {tool_name: 'CustomField'}) }
    belongs_to :custom_field_desc

    def locked?
      step_tool&.locked?
    end

    def copy(step_tool2)
      tcf = ToolCustomField.create!(step_tool: step_tool2,
                                    custom_field_desc_id: custom_field_desc_id,
                                    pos: pos,
                                    visible: visible,
                                    obligatory: obligatory,
                                    default_value: default_value)
      return tcf
    end
  end
end
