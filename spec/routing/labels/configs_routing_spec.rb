require 'rails_helper'

RSpec.describe Labels::ConfigsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/labels/configs').to route_to('labels/configs#index')
    end

    it 'routes to #new' do
      expect(get: '/labels/configs/new').to route_to('labels/configs#new')
    end

    it 'routes to #show' do
      expect(get: '/labels/configs/1').to route_to('labels/configs#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/labels/configs/1/edit').to route_to('labels/configs#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/labels/configs').to route_to('labels/configs#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/labels/configs/1').to route_to('labels/configs#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/labels/configs/1').to route_to('labels/configs#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/labels/configs/1').to route_to('labels/configs#destroy', id: '1')
    end
  end
end
