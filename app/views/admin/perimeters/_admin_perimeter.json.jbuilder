json.extract! admin_perimeter, :id, :name, :ancestry, :created_at, :updated_at
json.url admin_perimeter_url(admin_perimeter, format: :json)
