class AddCheckEfficacityToMethodo < ActiveRecord::Migration[6.0]
  class MigrationTool < ApplicationRecord
    self.table_name = :meth_tools
    serialize :tool_options
  end

  class MigrationStep < ApplicationRecord
    self.table_name = :meth_steps
  end

  class MigrationStepTool < ApplicationRecord
    self.table_name = :meth_step_tools
    serialize :tool_options
  end

  def up
    # # Update Tools options
    # # Check Urgence
    # tool_d0 = set_tool_options(1, {task_options: {task_detail_type: 'Tools::TaskEmergency'}})
    # # Check Conservatoire
    # tool_d3 = set_tool_options(2, {task_options: {task_detail_type: 'Tools::TaskContainment'}})
    # # Check Corrective
    # tool_d6 = set_tool_options(3, {task_options: {task_detail_type: 'Tools::TaskCorrective'},
    #                                more_options: {tools_matrix_stats: {matrix_status: '1'}}})
    # # Check Preventive
    # tool_d7 = set_tool_options(4, {task_options: {task_detail_type: 'Tools::TaskPreventive'}})
    # # Check Amélio
    # tool_d11 = set_tool_options(11, {task_options: {task_detail_type: 'Tools::TaskImprovement'}})

    # # Update StepTool options
    # step_tool_options(tool_d0)
    # step_tool_options(tool_d3)
    # step_tool_options(tool_d6)
    # step_tool_options(tool_d7)
    # step_tool_options(tool_d11)

    # # Create new step Tools for D6 Steps
    # steps_d6 = MigrationStep.where(step_role_id: 9)
    # steps_d6.each do |step|
    #   MigrationStepTool.create!(step_id: step.id, tool_id: tool_d6.id, step_tool_type: 1, locked: 0,
    #                             tool_reference: 1, tool_num: 1, pos: 2, name_fr: tool_d6.name_fr, name_en: tool_d6.name_en,
    #                             tool_options: tool_d6.tool_options)
    # end
    # # Create new step Tools for D6 Steps
    # steps_d7 = MigrationStep.where(step_role_id: 10)
    # steps_d7.each do |step|
    #   MigrationStepTool.create!(step_id: step.id, tool_id: tool_d7.id, step_tool_type: 1, locked: 0,
    #                             tool_reference: 1, tool_num: 1, pos: 2, name_fr: tool_d7.name_fr, name_en: tool_d7.name_en,
    #                             tool_options: tool_d7.tool_options)
    # end
  end

  def down
    say 'Do not delete Tools options and StepTools.'
    # raise ActiveRecord::IrreversibleMigration
  end

  def set_tool_options(activity_id, options)
    tool_eff = MigrationTool.where(tool_name: 'ActionCheck', activity_id: activity_id)&.first
    # return unless tool_eff

    # If tool do not exist => create it !
    tool_eff ||= MigrationTool.create!(tool_name: 'ActionCheck', activity_id: activity_id,
                                       name_fr: 'Vérification efficacité', name_en: 'Check efficiency',
                                       tool_icon: 'check-double')

    tool_eff.tool_options = options
    tool_eff.save!

    return tool_eff
  end

  def step_tool_options(tool)
    step_tools = MigrationStepTool.where(tool_id: tool.id)

    step_tools.each do |step_tool|
      step_tool.tool_options = tool.tool_options
      step_tool.save!
    end
  end
end
