class AddModifiableToProfil < ActiveRecord::Migration[6.0]
  def up
    add_column :profils, :profil_modifiable, :boolean, after: :profil_locked, default: true
    change_column :profils, :profil_locked, :boolean, default: false

    add_column :profils, :pos, :integer, after: :profil_modifiable, default: 0
  end

  def down
    remove_column :profils, :profil_modifiable
    remove_column :profils, :pos
  end
end
