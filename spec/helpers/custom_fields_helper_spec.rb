require 'rails_helper'

RSpec.describe CustomFieldsHelper, type: :helper do
  include ActionText::TagHelper

  let(:custom_field) { CustomField.create!(rca_id: rca.id) }

  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:rca) { FactoryBot.create(:rca, methodology_id: methodology.id) }

  let(:custom_field_desc) { FactoryBot.create(:meth_custom_field_desc, id: 100, custom_field_type_id: custom_field_type.id) }
  let(:custom_field_type) { FactoryBot.create(:meth_custom_field_type) }
  let(:custom_field_type_tag) { FactoryBot.create(:meth_custom_field_type, :type_tag) }
  let(:custom_field_type_rich) { FactoryBot.create(:meth_custom_field_type, :type_rich_text) }

  describe '#show_custom_field' do
    context 'when custom field exists in database' do
      context 'when custom field is a tag' do
        pending('En attente de solution pour update_db')
      end
      context 'when custom field is a rich text' do
        pending('En attente de solution pour update_db')
      end
      context 'when custom field is something else' do
        pending('En attente de solution pour update_db')
      end
    end

    context 'when custom field does not exists in database' do
      context 'when custom field is a tag field' do
        before :each do
          custom_field_desc.custom_field_type = custom_field_type_tag
          custom_field_desc.save!

          CustomField.finalize
        end
        it 'returns tags list []' do
          helper_content = helper.show_custom_field(custom_field, custom_field_desc)

          expect(helper_content).to eq []
        end
      end
      context 'when custom field is a rich text field' do
        before :each do
          custom_field_desc.custom_field_type = custom_field_type_rich
          custom_field_desc.save!

          CustomField.finalize
        end
        it 'returns tags list []' do
          helper_content = helper.show_custom_field(custom_field, custom_field_desc)

          expect(helper_content).to include('prowhy-openclose')
          expect(helper_content).to include("custom_#{custom_field_desc.internal_name}_#{custom_field.id}")
          expect(helper_content).to include('Details')
        end
      end
      context 'when custom field is something else' do
        it 'returns html text => custom field not in db' do
          helper_content = helper.show_custom_field(custom_field, custom_field_desc)
          # binding.pry
          expect(helper_content).to include(custom_field_desc.internal_name)
          expect(helper_content).to include("doesn't exist")
        end
      end
    end
  end

  describe '#input_custom_field' do
    context 'when custom field exists in database' do
      context 'when custom field is a tag' do
        pending('En attente de solution pour update_db')
      end
      context 'when custom field is a rich text' do
        pending('En attente de solution pour update_db')
      end
      context 'when custom field is something else' do
        pending('En attente de solution pour update_db')
      end
    end

    context 'when custom field does not exists in database' do
      context 'when custom field is a tag field' do
        before :each do
          custom_field_desc.custom_field_type = custom_field_type_tag
          custom_field_desc.save!

          CustomField.finalize
        end
        it 'returns input type tags list' do
          helper_content = ''
          form_for custom_field do |form|
            helper_content = helper.input_custom_field(form, custom_field, custom_field_desc)
          end
          expect(helper_content).to include(custom_field_desc.internal_name+'_list')
          expect(helper_content).to include('data-role')
          expect(helper_content).to include('tagsinput')
        end
      end
      context 'when custom field is a rich text field' do
        before :each do
          custom_field_desc.custom_field_type = custom_field_type_rich
          custom_field_desc.save!

          CustomField.finalize
        end
        it 'returns input type rich text' do
          helper_content = ''
          @custom_field = custom_field
          form_for custom_field do |form|
            helper_content = helper.input_custom_field(form, @custom_field, custom_field_desc)
          end
          # binding.pry
          expect(helper_content).to include(custom_field_desc.internal_name)
          expect(helper_content).to include('custom_field_'+custom_field_desc.internal_name+'_trix_input')
        end
      end
      context 'when custom field is something else' do
        it 'returns html text => custom field not in db' do
          helper_content = ''
          form_for custom_field do |form|
            helper_content = helper.input_custom_field(form, custom_field, custom_field_desc)
          end

          expect(helper_content).to include(custom_field_desc.name)
          expect(helper_content).to include(custom_field_desc.internal_name)
          expect(helper_content).to include("doesn't exist")
        end
      end
    end
  end
end
