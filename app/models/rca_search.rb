class RcaSearch
  include ActiveModel::Model
  attr_accessor :words, :words_type, :words_one,
                :date_begin, :date_end, :status_adv, :adv_duration,
                :methodology,
                :person_id, :person_role,
                :reference

  def initialize(attributes = {})
    super(attributes)

    after_initialize
  end

  def after_initialize
    self.status_adv = '1' unless status_adv
    self.words_one ||= 'true'
    self.words_type ||= ['', '1']
    self.person_id ||= '0'
    self.methodology ||= ['']

    self.date_begin = date_begin.to_date if date_begin
    self.date_end = date_end.to_date if date_end

    # binding.pry
  end
end
