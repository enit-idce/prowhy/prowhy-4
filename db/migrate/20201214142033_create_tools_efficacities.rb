class CreateToolsEfficacities < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_efficacities do |t|
      t.references :element, polymorphic: true, null: false
      t.integer :check_status, null: false, default: 0
      t.date :date_due_check
      t.date :date_done_check

      t.timestamps
    end
  end
end
