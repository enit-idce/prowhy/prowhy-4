require_dependency 'meth'

module Meth
  class StepRole < ApplicationRecord
    include MethConcern

    translates :name, fallback: :any

    has_many :steps

    validates :name, presence: true
    validates :role_name, presence: true, uniqueness: { case_sensitive: false }

    has_many :profil_authorizes, dependent: :destroy

    def self.find_or_create_role_info
      return StepRole.where(role_name: 'Info').first || StepRole.create!(name_fr: 'Info',
                                                                         name_en: 'Info',
                                                                         role_name: 'Info',
                                                                         advance_level: 0)
    end

    def self.find_or_create_role_trs
      return StepRole.where(role_name: 'Trs').first || StepRole.create!(name_fr: 'Outils transversaux',
                                                                        name_en: 'Transvers Tools',
                                                                        role_name: 'Trs',
                                                                        advance_level: 0)
    end

    def self.find_or_create_role_close
      return StepRole.where(role_name: 'Close').first || StepRole.create!(name_fr: 'Clôture',
                                                                          name_en: 'Close',
                                                                          role_name: 'Close',
                                                                          advance_level: 9)
    end
  end
end
