require 'rails_helper'

RSpec.describe Tools::DecisionMat, type: :model do
  subject(:decision_mat) do
    Tools::DecisionMat.create!(rca: rca, tool_reference: 1, name: 'Matrix 1', entry_class: 'Tools::Task')
  end

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has many crit_dec_mats' do
        assc = Tools::DecisionMat.reflect_on_association(:crit_dec_mats)
        expect(assc.macro).to eq :has_many
      end

      it 'has many labels_criteria' do
        assc = Tools::DecisionMat.reflect_on_association(:labels_criteria)
        expect(assc.macro).to eq :has_many
      end

      it 'has many dec_mat_links' do
        assc = Tools::DecisionMat.reflect_on_association(:dec_mat_links)
        expect(assc.macro).to eq :has_many
      end

      # Tools => entries (has many through polymorphic impossible!)
      it 'has many tools_tasks' do
        assc = Tools::DecisionMat.reflect_on_association(:tools_tasks)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools_causes' do
        assc = Tools::DecisionMat.reflect_on_association(:tools_causes)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        decision_mat.name = ''
        expect(decision_mat).to_not be_valid
      end

      it 'is valid with name' do
        decision_mat.name = 'La cause'
        expect(decision_mat).to be_valid
      end

      it 'is not valid without entry_class' do
        decision_mat.entry_class = ''
        expect(decision_mat).to_not be_valid
      end

      it 'is valid with a valid entry_class' do
        decision_mat.entry_class = 'Tools::Cause'
        expect(decision_mat).to be_valid
      end

      it 'is not valid with an invalid entry_class' do
        decision_mat.entry_class = 'Blablabla'
        expect(decision_mat).to_not be_valid
      end
    end
  end

  describe 'adds and deletes criteria' do
    let(:criterium) { Labels::Criterium.create(name: 'Efficacity', threshold: 1) }
    let(:criterium2) { Labels::Criterium.create(name: 'Duration', threshold: 1) }

    it 'adds criterium' do
      decision_mat.add_criterium(criterium)

      expect(decision_mat.labels_criteria).to eq [criterium]
      expect(decision_mat.crit_dec_mats.length).to eq 1
      expect(decision_mat.crit_dec_mats.map(&:labels_criterium)).to eq [criterium]
    end

    it 'deletes criterium' do
      decision_mat.add_criterium(criterium)
      decision_mat.add_criterium(criterium2)

      decision_mat.del_criterium(criterium)

      expect(decision_mat.labels_criteria).to eq [criterium2]
      expect(decision_mat.crit_dec_mats.length).to eq 1
    end
  end

  describe 'adds and deletes entries' do
    let(:criterium) { Labels::Criterium.create(name: 'Efficacity', threshold: 1) }
    let(:task1) { FactoryBot.create('task', name: 'Task 1', rca: rca) }
    let(:task2) { FactoryBot.create('task', name: 'Task 2', rca: rca) }

    before :each do
      decision_mat.add_criterium(criterium)
    end

    it 'list entries' do
      decision_mat.add_entry(task1)
      decision_mat.add_entry(task2)

      expect(decision_mat.entries).to contain_exactly(task1, task2)
    end

    it 'adds entry' do
      decision_mat.add_entry(task1)

      expect(decision_mat.entries).to eq [task1]
      expect(decision_mat.reload.dec_mat_links.length).to eq 1
      expect(decision_mat.dec_mat_links.map(&:entry)).to eq [task1]
    end

    it 'deletes entry' do
      decision_mat.add_entry(task1)
      decision_mat.add_entry(task2)
      decision_mat.del_entry(task1)

      expect(decision_mat.entries).to eq [task2]
      expect(decision_mat.reload.dec_mat_links.length).to eq 1
    end

    it 'deletes an entry from a entry_link' do
      decision_mat.add_entry(task1)
      expect(decision_mat.reload.entries).to include(task1)

      link = Tools::DecMatLink.where(decision_mat: decision_mat, entry: task1).first
      decision_mat.del_entry_from_link(link)

      expect(decision_mat.reload.entries).to_not include(task1)
    end

    it 'adds criterium with existing entry : creates val' do
      decision_mat.reload.add_entry(task1)

      criterium2 = Labels::Criterium.create(name: 'Duration', threshold: 1)
      decision_mat.add_criterium(criterium2)

      expect(decision_mat.labels_criteria).to contain_exactly(criterium, criterium2)
      expect(decision_mat.reload.crit_dec_mats.length).to eq 2
      expect(decision_mat.crit_dec_mats.map(&:labels_criterium)).to contain_exactly(criterium, criterium2)

      expect(Tools::DecMatVal.all.length).to eq 2
    end
  end
end
