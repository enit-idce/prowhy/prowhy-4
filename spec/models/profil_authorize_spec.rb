require 'rails_helper'

RSpec.describe ProfilAuthorize, type: :model do
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to meth_step_role' do
        assc = ProfilAuthorize.reflect_on_association(:meth_step_role)
        expect(assc.macro).to eq :belongs_to
      end

      it 'belongs_to profil' do
        assc = ProfilAuthorize.reflect_on_association(:profil)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end
end
