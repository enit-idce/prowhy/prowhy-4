require_dependency 'tools'

module Tools
  class TaskValidation < ApplicationRecord
    has_one :task, as: :task_detail
    enum status: { in_progress: 0, validated: 1, invalidated: 2 }, _prefix: true
  end
end
