FactoryBot.define do
  factory :cause_tree, class: Tools::CauseTree do
    tool_reference { 1 }
    tool_num { 1 }
  end
end
