FactoryBot.define do
  factory :ldap_config, class: Admin::LdapConfig do
    name { 'LDap' }
    host { 'ldap.domain.fr' }
    port { 123 }
    base_dn  { 'ou=people,dc=deomain,dc=fr' }
    attr_uid { 'uid' }
    attr_email { 'mail' }
    attr_lastname { 'sn' }
    attr_firstname { 'givenName' }
  end
end
