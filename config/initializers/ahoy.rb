module Ahoy
  class Store < Ahoy::DatabaseStore
    # def track_event(data)
    #   # new event
    #   puts "HERE I TRACK EVENT !!!!!".yellow
    #   puts data
    #   puts "OK DONE.".red
    # end
  end
end

# set to true for JavaScript tracking
Ahoy.api = false

# set to true for geocoding
# we recommend configuring local geocoding first
# see https://github.com/ankane/ahoy#geocoding
Ahoy.geocode = false
