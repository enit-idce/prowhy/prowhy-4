module Labels
  class QqoqcpConfigsController < ApplicationController
    include AdminConcern
    before_action :set_labels_qqoqcp_config, only: [:show, :edit, :update, :destroy, :add_line, :add_line_valid]

    # GET /labels/qqoqcp_configs
    # GET /labels/qqoqcp_configs.json
    def index
      authorize QqoqcpConfig
      @labels_qqoqcp_configs = Labels::QqoqcpConfig.all
    end

    # GET /labels/qqoqcp_configs/1
    # GET /labels/qqoqcp_configs/1.json
    # def show
    # end

    # GET /labels/qqoqcp_configs/new
    def new
      @labels_qqoqcp_config = Labels::QqoqcpConfig.new
      authorize @labels_qqoqcp_config

      @labels_qqoqcp_config.qqoqcp_cls.build
    end

    # GET /labels/qqoqcp_configs/1/edit
    def edit
    end

    # POST /labels/qqoqcp_configs
    # POST /labels/qqoqcp_configs.json
    def create
      @labels_qqoqcp_config = Labels::QqoqcpConfig.new(labels_qqoqcp_config_params)
      authorize @labels_qqoqcp_config

      respond_to do |format|
        if @labels_qqoqcp_config.save
          format.html { redirect_to labels_qqoqcp_configs_url, notice: 'Qqoqcp config was successfully created.' }
          format.json { render :show, status: :created, location: @labels_qqoqcp_config }
        else
          format.html { render :new }
          format.json { render json: @labels_qqoqcp_config.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /labels/qqoqcp_configs/1
    # PATCH/PUT /labels/qqoqcp_configs/1.json
    def update
      respond_to do |format|
        if @labels_qqoqcp_config.update(labels_qqoqcp_config_params)
          format.html { redirect_to labels_qqoqcp_configs_url, notice: 'Qqoqcp config was successfully updated.' }
          format.json { render :show, status: :ok, location: @labels_qqoqcp_config }
        else
          format.html { render :edit }
          format.json { render json: @labels_qqoqcp_config.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /labels/qqoqcp_configs/1
    # DELETE /labels/qqoqcp_configs/1.json
    def destroy
      @labels_qqoqcp_config.destroy
      respond_to do |format|
        format.html { redirect_to labels_qqoqcp_configs_url, notice: 'Qqoqcp config was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    # GET
    def add_line
      qqoqcp_lines = Labels::QqoqcpLine.all
      render 'add_line', locals: {qqoqcp_config: @labels_qqoqcp_config, qqoqcp_lines: qqoqcp_lines}
    end

    # PATCH
    def add_line_valid
      qqoqcp_lines = Labels::QqoqcpLine.all
      qqoqcp_lines.each do |line|
        line_in_config =  @labels_qqoqcp_config.qqoqcp_lines.include?(line)
        # Ajout / Supression de critères
        if line_in_config && params[:dec_mat_crit][line.id.to_s] == 'false'
          # Delete criterium
          @labels_qqoqcp_config.delete_line(line)
        elsif !line_in_config && params[:dec_mat_crit][line.id.to_s] == 'true'
          # Add criterium
          # @labels_config.add_criterium(crit, weight)
          @labels_qqoqcp_config.add_line(line)
        end
      end

      respond_to do |format|
        format.html { redirect_to edit_labels_qqoqcp_config_path(@labels_qqoqcp_config) }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_labels_qqoqcp_config
      @labels_qqoqcp_config = Labels::QqoqcpConfig.find(params[:id])
      authorize @labels_qqoqcp_config
    end

    # Only allow a list of trusted parameters through.
    def labels_qqoqcp_config_params
      params.require(:labels_qqoqcp_config).permit(:name, :name_fr, :name_en, :show_is, :show_isnot, :show_info, :usable, :nb_lines,
                                                   qqoqcp_cls_attributes: [:id, :qqoqcp_config_id, :qqoqcp_line_id, :pos])
    end
  end
end
