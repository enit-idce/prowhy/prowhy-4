require 'rails_helper'

RSpec.describe Tools::FileVersion, type: :model do
  subject(:file_version) do
    Tools::FileVersion.create!(element: generic, author: Person.new(firstname: 'Yo', lastname: 'Boo'), file: doc_ok)
  end
  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
  let(:generic) do
    Tools::Generic.create!(rca: rca, tool_reference: 1, tool_num: 1, doc: doc_ok)
  end
  let(:doc_ok) do
    ActiveStorage::Blob.create_and_upload!(
      io: File.open(file_ok, 'rb'),
      filename: 'not_image.pdf',
      content_type: 'file/pdf'
    ).signed_id
  end
  let(:file_ok) { Rails.root.join('spec', 'support', 'assets', 'images', 'not_image.pdf') }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to element' do
        assc = Tools::FileVersion.reflect_on_association(:element)
        expect(assc.macro).to eq :belongs_to
      end

      it 'belongs_to author' do
        assc = Tools::FileVersion.reflect_on_association(:author)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end

  it 'creates a file version object with attachment' do
    expect(file_version.file.attached?).to be true
    expect(file_version.file.filename).to eq 'not_image.pdf'
  end
end
