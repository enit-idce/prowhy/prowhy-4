class CreateAdminLdapConfigs < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_ldap_configs do |t|
      t.string :name, default: 'Ldap'
      t.string :host
      t.integer :port
      t.string :base_dn

      t.string :admin_login, null: true
      t.string :admin_password, null: true

      t.string :attr_uid, default: 'uid'
      t.string :attr_firstname
      t.string :attr_lastname, default: 'sn'
      t.string :attr_email, default: 'mail'

      t.string :attr_adress
      t.string :attr_phone
      # t.column :attr_fonction,  :string

      t.boolean :authorize_signup, default: 0

      t.timestamps
    end

    add_reference :users, :ldap_config, after: :username, foreign_key: { to_table: :admin_ldap_configs }
  end
end
