require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to test the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator. If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails. There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.

RSpec.describe '/meth/tool_custom_fields', type: :request do
  let(:valid_attributes) { FactoryBot.attributes_for(:meth_tool_custom_field, custom_field_desc_id: custom_field_desc.id, step_tool_id: step_tool.id) }
  let(:invalid_attributes) { FactoryBot.attributes_for(:meth_tool_custom_field, custom_field_type_id: 0, step_tool_id: step_tool.id) }

  let(:custom_field_desc) { FactoryBot.create(:meth_custom_field_desc, custom_field_type: custom_field_type) }
  let(:custom_field_type) { FactoryBot.create(:meth_custom_field_type) }

  let(:user) { FactoryBot.create(:user) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:step_tool) { methodology.steps.first.step_tools.first }
  let(:step_tool2) { methodology.steps.last.step_tools.first }

  before :each do
    user.set_session(:active_step_tool, methodology.steps.first.step_tools.first.id)
    user.save!
    sign_in user
  end

  describe 'GET /index' do
    before :each do
      Meth::ToolCustomField.create! valid_attributes
      Meth::ToolCustomField.create! valid_attributes.merge(step_tool_id: step_tool2.id)
      Meth::ToolCustomField.create! valid_attributes
    end

    context 'without step_tool params' do
      it 'renders a successful response' do
        get meth_tool_custom_fields_url
        expect(response).to be_successful
        expect(@controller.instance_variable_get(:@tool_custom_fields).size).to eq 3
      end
    end

    context 'with step_tool params' do
      it 'renders a successful response' do
        get meth_tool_custom_fields_url(step_tool: step_tool2.id)
        expect(response).to be_successful
        expect(@controller.instance_variable_get(:@tool_custom_fields).size).to eq 1
      end
    end
  end

  describe 'GET /show' do
    it 'renders a successful response' do
      meth_tool_custom_field = Meth::ToolCustomField.create! valid_attributes
      get meth_tool_custom_field_url(meth_tool_custom_field)
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_meth_tool_custom_field_url
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      meth_tool_custom_field = Meth::ToolCustomField.create! valid_attributes
      get edit_meth_tool_custom_field_url(meth_tool_custom_field)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Meth::ToolCustomField' do
        expect {
          post meth_tool_custom_fields_url, params: { meth_tool_custom_field: valid_attributes }
        }.to change(Meth::ToolCustomField, :count).by(1)
      end

      it 'redirects to the tool_custom_fields list' do
        post meth_tool_custom_fields_url, params: { meth_tool_custom_field: valid_attributes }
        expect(response).to redirect_to(meth_tool_custom_fields_url(step_tool: step_tool.id))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Meth::ToolCustomField' do
        expect {
          post meth_tool_custom_fields_url, params: { meth_tool_custom_field: invalid_attributes }
        }.to change(Meth::ToolCustomField, :count).by(0)
      end

      it 'redirects to the tool_custom_fields list' do
        post meth_tool_custom_fields_url, params: { meth_tool_custom_field: invalid_attributes }
        expect(response).to redirect_to(meth_tool_custom_fields_url(step_tool: step_tool.id))
      end
    end
  end

  # describe 'PATCH /update' do
  #   context 'with valid parameters' do
  #     let(:new_attributes) { {custom_field_desc_id: custom_field_desc2.id} }
  #     let(:custom_field_desc2) { FactoryBot.create(:meth_custom_field_desc, custom_field_type: custom_field_type) }

  #     it 'updates the requested meth_tool_custom_field' do
  #       meth_tool_custom_field = Meth::ToolCustomField.create! valid_attributes
  #       patch meth_tool_custom_field_url(meth_tool_custom_field), params: { meth_tool_custom_field: new_attributes }
  #       meth_tool_custom_field.reload
  #       expect(meth_tool_custom_field.custom_field_desc).to eq custom_field_desc2
  #     end

  #     it 'redirects to the meth_tool_custom_field' do
  #       meth_tool_custom_field = Meth::ToolCustomField.create! valid_attributes
  #       patch meth_tool_custom_field_url(meth_tool_custom_field), params: { meth_tool_custom_field: new_attributes }
  #       meth_tool_custom_field.reload
  #       expect(response).to redirect_to(meth_tool_custom_field_url(meth_tool_custom_field))
  #     end
  #   end

  #   context 'with invalid parameters' do
  #     it 'renders a successful response (i.e. to display the edit template)' do
  #       meth_tool_custom_field = Meth::ToolCustomField.create! valid_attributes
  #       patch meth_tool_custom_field_url(meth_tool_custom_field), params: { meth_tool_custom_field: invalid_attributes }
  #       expect(response).to be_successful
  #     end
  #   end
  # end

  describe 'DELETE /destroy' do
    it 'destroys the requested meth_tool_custom_field' do
      meth_tool_custom_field = Meth::ToolCustomField.create! valid_attributes
      expect {
        delete meth_tool_custom_field_url(meth_tool_custom_field)
      }.to change(Meth::ToolCustomField, :count).by(-1)
    end

    it 'redirects to the meth_tool_custom_fields list' do
      meth_tool_custom_field = Meth::ToolCustomField.create! valid_attributes
      delete meth_tool_custom_field_url(meth_tool_custom_field)
      expect(response).to redirect_to(meth_tool_custom_fields_url(step_tool: step_tool.id))
    end
  end

  describe 'GET /show_tool_custom_field' do
    context 'with rca' do
      let(:rca) { FactoryBot.create(:rca, methodology: methodology) }
      before :each do
        CustomField.create!(rca: rca)
      end

      it 'renders the tool custom field' do
        Meth::ToolCustomField.create! valid_attributes

        get meth_show_tool_custom_field_path(step_tool: step_tool.id, rca: rca.id)
        expect(response).to be_successful
      end
    end

    context 'with no rca' do
      it 'redirect to rcas list' do
        Meth::ToolCustomField.create! valid_attributes

        get meth_show_tool_custom_field_path(step_tool: step_tool.id)
        expect(response).to redirect_to(rcas_path)
      end
    end
  end

  describe 'PATCH /tool_custom_field_up' do
    it 'moves up the custom field' do
      cf1 = Meth::ToolCustomField.create! valid_attributes.merge(pos: 1)
      cf2 = Meth::ToolCustomField.create! valid_attributes.merge(pos: 2)
      cf3 = Meth::ToolCustomField.create! valid_attributes.merge(pos: 3)

      patch meth_tool_custom_field_up_path(id: cf1)

      expect(cf1.reload.pos).to eq 2
      expect(cf2.reload.pos).to eq 1
      expect(cf3.reload.pos).to eq 3

      expect(response).to redirect_to(meth_tool_custom_fields_path(step_tool: step_tool))
    end
  end

  describe 'PATCH /tool_custom_field_down' do
    it 'moves down the custom field' do
      cf1 = Meth::ToolCustomField.create! valid_attributes.merge(pos: 1)
      cf2 = Meth::ToolCustomField.create! valid_attributes.merge(pos: 2)
      cf3 = Meth::ToolCustomField.create! valid_attributes.merge(pos: 3)

      patch meth_tool_custom_field_down_path(id: cf2)

      expect(cf1.reload.pos).to eq 2
      expect(cf2.reload.pos).to eq 1
      expect(cf3.reload.pos).to eq 3

      expect(response).to redirect_to(meth_tool_custom_fields_path(step_tool: step_tool))
    end
  end
end
