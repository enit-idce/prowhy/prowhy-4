require 'rails_helper'

RSpec.describe Tools::DecMatVal, type: :model do
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to crit_dec_mat' do
        assc = Tools::DecMatVal.reflect_on_association(:crit_dec_mat)
        expect(assc.macro).to eq :belongs_to
      end

      it 'belongs to dec_mat_link' do
        assc = Tools::DecMatVal.reflect_on_association(:dec_mat_link)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end
end
