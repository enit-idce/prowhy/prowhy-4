class CreateToolsGenerics < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_generics do |t|
      t.references :rca, null: false, foreign_key: true
      t.integer :tool_reference
      t.integer :tool_num
      t.text :synthesis
      t.json :users_info

      t.timestamps
    end
  end
end
