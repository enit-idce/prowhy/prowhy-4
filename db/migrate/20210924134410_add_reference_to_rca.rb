class AddReferenceToRca < ActiveRecord::Migration[6.0]
  def change
    add_column :rcas, :reference, :string, after: :id, default: nil
  end
end
