require_dependency 'tools'

module Tools
  class FileVersion < ApplicationRecord
    belongs_to :element, polymorphic: true
    belongs_to :author, class_name: 'Person'

    has_one_attached :file
  end
end
