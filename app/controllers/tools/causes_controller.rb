module Tools
  class CausesController < ApplicationController
    include ProcessConcern
    before_action :set_tools_cause, only: [:show, :edit, :update, :destroy, :edit_status]
    before_action :init_rca, only: [:index, :new, :create]

    after_action :call_track_action, only: [:create, :destroy, :update]

    # GET /tools/causes
    # GET /tools/causes.json
    # def index
    #   if @rca.nil? || current_user.get_session(:active_step_tool).nil?
    #     redirect_to rcas_path
    #   elsif params[:id].nil?
    #     redirect_to edit_rca_path(@rca)
    #   else
    #     # flash[:cause_tree_head] = params[:id]
    #     @cause_tree_head = params[:id]
    #   end
    # end

    # # Création des données JSon pour jstree
    # def causes_js_tree_data
    #   if params[:cause_tree_head]
    #     # @cause = Tools::Cause.find(flash[:cause_tree_head].to_i)
    #     @cause = Tools::Cause.find(params[:cause_tree_head].to_i)

    #     data = []

    #     cause_presenter = Tools::CausePresenter.new(@cause, view_context)
    #     data << cause_presenter.cause_subtree_datas
    #   else
    #     data = [{text: 'no tree'}]
    #   end
    #   render json: data
    # end

    # GET /tools/causes/1
    # GET /tools/causes/1.json
    def show
    end

    # GET /tools/causes/new
    def new
      # authorize Cause
      skip_authorization

      # binding.pry
      if @rca.nil? || current_user.get_session(:active_step_tool).nil?
        redirect_to rcas_path
      elsif params[:tools_cause].nil?
        redirect_to edit_rca_path(@rca)
      else
        if params[:ishikawa_id]
          # set default parent => ishikawa.cause / il faudra changer quand il y aura les sous-ishikawa
          # il faudra envoyer le parent_id depuis l'ishikawa
          ishikawa = Tools::Ishikawa.find(params[:ishikawa_id])
          params[:tools_cause][:parent_id] = ishikawa&.cause&.id
        end
        @tools_cause = Tools::Cause.new(tools_cause_params)
        authorize @tools_cause

        @ishikawas, @ishikawa_names = @tools_cause.init_ishikawas_list
        @ishikawas.each do |ishi|
          # branch ishikawa if new from ishikawa
          if params[:ishikawa_id] == ishi.id.to_s && params[:cause_type_id]
            @tools_cause.tools_cause_classes.build(tools_ishikawa_id: params[:ishikawa_id],
                                                   labels_cause_type_id: params[:cause_type_id])
          # if new from Cause tree : empty branch
          else
            @tools_cause.tools_cause_classes.build(tools_ishikawa_id: ishi.id,
                                                   labels_cause_type_id: nil)
          end
        end

        step_tool_form_options
      end
    end

    # GET /tools/causes/1/edit
    def edit
      @ishi = params[:ishi]
      @ishikawas, @ishikawa_names = @tools_cause.init_ishikawas_list

      step_tool_form_options

      render 'new'
    end

    # POST /tools/causes
    # POST /tools/causes.json
    def create
      @tools_cause = Tools::Cause.new(tools_cause_params)

      authorize @tools_cause

      respond_to do |format|
        if @tools_cause.save
          format.html do
            redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca),
                        notice: t('.message_succes', cause: Cause.model_name.human)
          end
          format.json { render :show, status: :created, location: @tools_cause }
        else
          format.html { render :new }
          format.json { render json: @tools_cause.errors, status: :unprocessable_entity }
          format.js { render :new }
        end
      end
    end

    # PATCH/PUT /tools/causes/1
    # PATCH/PUT /tools/causes/1.json
    def update
      respond_to do |format|
        if @tools_cause.update(tools_cause_params)
          format.html do
            redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca),
                        notice: t('.message_succes', cause: Cause.model_name.human)
          end
          format.json { render :show, status: :ok, location: @tools_cause }
        else
          format.html { render :edit }
          format.json { render json: @tools_cause.errors, status: :unprocessable_entity }
          format.js { render :new }
        end
      end
    end

    # DELETE /tools/causes/1
    # DELETE /tools/causes/1.json
    def destroy
      @tools_cause.destroy
      respond_to do |format|
        format.html do
          redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca),
                      notice: t('.message_succes', cause: Cause.model_name.human)
        end
        format.json { head :no_content }
      end
    end

    # GET /tools/edit_status
    def edit_status
      render :edit_status
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_cause
      @tools_cause = Tools::Cause.find(params[:id])
      @rca = @tools_cause.rca

      authorize @rca, :show?
      authorize @tools_cause
    end

    # Only allow a list of trusted parameters through.
    def tools_cause_params
      params.require(:tools_cause).permit(:rca_id, :tool_reference, :tool_num,
                                          :name, :tree_head, :labels_cause_type_id,
                                          :level, :status, :parent_id, # ishi_branchs: {},
                                          tools_cause_classes_attributes: %i[labels_cause_type_id tools_ishikawa_id tools_cause_id id])
    end

    def step_tool_form_options
      step_tool = current_user.get_session(:active_step_tool) ? Meth::StepTool.find(current_user.get_session(:active_step_tool)) : nil
      @form_options = {status: true}

      return unless step_tool&.tool_options

      step_tool.tool_options[:form_options]&.each_key do |key|
        @form_options[key.intern] = step_tool.tool_options[:form_options][key]
      end
    end
  end
end
