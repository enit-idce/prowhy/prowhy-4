json.extract! tools_indicator_measure, :id, :indicator_id, :name, :description, :value, :created_at, :updated_at
json.url tools_indicator_measure_url(tools_indicator_measure, format: :json)
