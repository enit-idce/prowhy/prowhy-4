FactoryBot.define do
  factory :indicator_measure, class: Tools::IndicatorMeasure do
    name { 'Measure 1' }
    association :indicator, factory: :indicator
  end
end
