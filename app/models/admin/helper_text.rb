module Admin
  class HelperText < ApplicationRecord
    translates :help_text, fallback: :any

    validates :tool_class, presence: true
    validates :reference, presence: true
  end
end
