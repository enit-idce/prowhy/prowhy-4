module MethConcern
  extend ActiveSupport::Concern

  included do
  end

  # Class Methods
  module ClassMethods
  end

  # Instance Methods
  def locked?
    false
  end
end
