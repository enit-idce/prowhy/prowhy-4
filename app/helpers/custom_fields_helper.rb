module CustomFieldsHelper
  # ----------------------------------------------------------------------------
  # => Input Custom Field
  # Si fonction spécifique : écrire la fonction avec le nom : input_custom_field_[internal_name]
  # Sinon, ce sera :
  #   form.input custom_field_desc.internal_name, label: custom_field_desc.name
  # qui sera utilisé.

  # def input_custom_field_text(form, _custom_field, custom_field_desc)
  #   form.input custom_field_desc.internal_name, label: custom_field_desc.name
  # end

  def input_custom_field_text(form, _custom_field, custom_field_desc, params = {})
    form.input custom_field_desc.internal_name, label: custom_field_desc.name,
                                                input_html: { class: 'form-control-sm',
                                                              'saved-required': params[:obligatory] ? 'required' : false },
                                                required: params[:obligatory] && params[:css_display].nil?,
                                                wrapper_html: { style: params[:css_display] }
  end

  def input_custom_field_text_area(form, _custom_field, custom_field_desc, params = {})
    form.input custom_field_desc.internal_name, label: custom_field_desc.name,
                                                input_html: { class: 'form-control-sm',
                                                              'saved-required': params[:obligatory] ? 'required' : false },
                                                as: :text,
                                                required: params[:obligatory] && params[:css_display].nil?,
                                                wrapper_html: { style: params[:css_display] }
  end

  def input_rca_field_rich_text(form, field_name, field_label, params = {})
    # 'FIELD ACTION SCRIPT /// COMMING SOON!'
    ret = '<div class="form-group row">'
    ret += form.label field_name, label: field_label, class: 'col-sm-3'
    ret += '<div class="col-sm-9">'
    ret += form.rich_text_area field_name, label: field_label, class: 'form-control-sm form-control', style: 'height: auto;', required: params[:obligatory]
    ret += '</div></div>'
    ret.html_safe
  end

  def input_custom_field_rich_text(form, _custom_field, custom_field_desc, params = {})
    # 'FIELD ACTION SCRIPT /// COMMING SOON!'
    # ret = '<div class="form-group">'
    # ret += form.label custom_field_desc.internal_name, label: custom_field_desc.name, input_html: { class: 'col-sm-3 form-control-sm'}
    # ret += '<div class="col-sm-9">'
    # ret += form.rich_text_area custom_field_desc.internal_name, label: custom_field_desc.name, input_html: { class: 'form-control-sm'}
    # ret += '</div></div>'
    # ret.html_safe

    input_rca_field_rich_text(form, custom_field_desc.internal_name, custom_field_desc.name, params)
  end

  def input_custom_field_select(form, _custom_field, custom_field_desc, params = {})
    sort_type = custom_field_desc.options&.[]('order') == 'alpha' ? 'name' : 'pos'
    # css_display = nil

    # Le custom field est dépendant =>
    # 1. on vérifie s'il faut l'afficher ou non (css_display)
    # 2. si oui, filtrage des valeurs de la liste
    if custom_field_desc.ref_field && !params[:search_form]
      reference_value = form.object.send(custom_field_desc.ref_field.internal_name)

      # if custom_field_desc.ref_show_values
      #   css_display = 'display: none;' unless custom_field_desc.ref_show_values.include?(reference_value)
      # end

      list_contents = custom_field_desc.list_contents.where(ref_value: [0, reference_value]).sort { |a, b| a.send(sort_type) <=> b.send(sort_type) }

      name_filtered = custom_field_desc.name + ' ' + info_popup(custom_field_desc.name, t('meth.custom_field_descs.select_filtered', field: custom_field_desc.ref_field.name))

    # Le custom field n'est pas dépendant => toute la liste (ListContent)
    else
      list_contents = custom_field_desc.list_contents.sort { |a, b| a.send(sort_type) <=> b.send(sort_type) }
      name_filtered = custom_field_desc.name
    end

    # Le custom field n'a pas de dépendances
    if params[:search_form] || custom_field_desc.dependant_fields.empty?
      form.input custom_field_desc.internal_name, label: name_filtered.html_safe,
                                                  input_html: { class: 'form-control-sm',
                                                                'saved-required': params[:obligatory] ? 'required' : false},
                                                  collection: list_contents,
                                                  required: params[:obligatory] && params[:css_display].nil?,
                                                  wrapper_html: { style: params[:css_display] }
    # Le custom field a des dépendances
    else
      name_dep = name_filtered + ' ' + warning_popup(custom_field_desc.name, t('meth.custom_field_descs.select_dep', fields: custom_field_desc.dependant_fields.map(&:name).join(', ')))
      form.input custom_field_desc.internal_name, label: name_dep.html_safe, # custom_field_desc.name,
                                                  input_html: { class: 'form-control-sm filtered-class',
                                                                'saved-required': params[:obligatory] ? 'required' : false,
                                                                'filtered-url': meth_custom_field_desc_dependant_fields_refresh_path(id: custom_field_desc.id, custom_field_id: form.object.id)
                                                              },
                                                  collection: list_contents,
                                                  required: params[:obligatory] && params[:css_display].nil?,
                                                  wrapper_html: { style: params[:css_display] }
    end
  end

  def input_custom_field_radio(form, _custom_field, custom_field_desc, params = {})
    form.input custom_field_desc.internal_name, label: custom_field_desc.name,
                                                as: :radio_buttons, # collection: [t('radio_yes'), t('radio_no')],
                                                wrapper: :horizontal_collection_inline,
                                                input_html: { 'saved-required': params[:obligatory] ? 'required' : false },
                                                required: params[:obligatory] && params[:css_display].nil?,
                                                wrapper_html: { style: params[:css_display] }
  end

  def input_custom_field_tree(form, _custom_field, custom_field_desc, params = {})
    tree_contents = custom_field_desc.tree_contents.first.root.descendants.order_for_list
    form.input custom_field_desc.internal_name, label: custom_field_desc.name,
                                                input_html: { class: 'form-control-sm',
                                                              'saved-required': params[:obligatory] ? 'required' : false },
                                                collection: tree_contents.collect { |tc| [tc.name_level.html_safe, tc.id.to_s] },
                                                required: params[:obligatory] && params[:css_display].nil?,
                                                wrapper_html: { style: params[:css_display] }
  end

  # def input_custom_field_integer(form, _custom_field, custom_field_desc)
  #   form.input custom_field_desc.internal_name, label: custom_field_desc.name
  # end

  def input_custom_field_tag(form, custom_field, custom_field_desc, params = {})
    # binding.pry
    ret = '<div class="form-group row">'
    ret += form.label custom_field_desc.internal_name+'_list', label: custom_field_desc.name, class: 'col-sm-3'
    ret += '<div class="col-sm-9">'
    ret += form.text_field custom_field_desc.internal_name+'_list',
                           value: custom_field.send(custom_field_desc.internal_name+'_list').to_s,
                           'data-role': 'tagsinput',
                           input_html: { class: 'form-control-sm',
                                         'saved-required': params[:obligatory] ? 'required' : false },
                           required: params[:obligatory]
    ret += '</div></div>'
    ret.html_safe
  end

  def input_custom_field_date(form, _custom_field, custom_field_desc, params = {})
    # puts "CUSTOM FIELD DATE ==========================================>".yellow
    # toto = form.input custom_field_desc.internal_name, label: custom_field_desc.name,
    #                                                    # input_html: { class: 'form-control-sm datetimepicker'},
    #                                                    as: :date_time_picker
    # puts toto
    # puts "=======================================OK".red
    # return toto
    form.input custom_field_desc.internal_name, label: custom_field_desc.name,
                                                as: :date, html5: true,
                                                input_html: { class: 'form-control-sm',
                                                              'saved-required': params[:obligatory] ? 'required' : false },
                                                required: params[:obligatory] && params[:css_display].nil?,
                                                wrapper_html: { style: params[:css_display] }
  end

  def input_custom_field(form, custom_field, custom_field_desc, params = {})
    params[:obligatory] ||= false
    params[:search_form] ||= false
    # form.input custom_field_desc.internal_name
    # puts obligatory.to_s.green

    params[:css_display] = nil
    # if custom_field_desc.custom_field_type.field_internal == 'text_area'
    #   binding.pry
    # end
    if custom_field_desc.ref_field && !params[:search_form]
      reference_value = form.object.send(custom_field_desc.ref_field.internal_name)

      if custom_field_desc.ref_show_values && !custom_field_desc.ref_show_values.include?(reference_value)
        params[:css_display] = 'display: none;' # unless custom_field_desc.ref_show_values.include?(reference_value)
      end
    end

    method('input_custom_field_'+custom_field_desc.custom_field_type.field_internal).call(form, custom_field, custom_field_desc, params)
  rescue StandardError
    begin
      form.input custom_field_desc.internal_name, label: custom_field_desc.name,
                                                  input_html: { class: 'form-control-sm',
                                                                'saved-required': params[:obligatory] ? 'required' : false },
                                                  required: params[:obligatory] && params[:css_display].nil?,
                                                  wrapper_html: { style: params[:css_display] }
    rescue StandardError
      ret = "<div class='form-group'>"
      ret += "<label>#{custom_field_desc.name}</label><br/>"
      ret += "<i>Custom field #{custom_field_desc.internal_name} doesn't exist in database</i></div>"
      return ret.html_safe
    end
  end

  # ----------------------------------------------------------------------------
  # => Show Custom Field
  # Si fonction spécifique : écrire la fonction avec le nom : show_custom_field_[field_internal]
  # Sinon, ce sera send(internal_name) qui sera utilisé.

  def show_custom_field_tag(custom_field, custom_field_desc)
    custom_field.send(custom_field_desc.internal_name+'_list')
  end

  def show_custom_field_rich_text(custom_field, custom_field_desc)
    ret = "<a href='#' data-toggle='prowhy-openclose'
                       data-prowhy-text='Details' data-prowhy-type='open'
                       data-prowhy-content='#custom_#{custom_field_desc.internal_name}_#{custom_field.id}'></a>"
    ret += "<div id='custom_#{custom_field_desc.internal_name}_#{custom_field.id}' style='display:none;'>
            <div class='popover-body'>
              #{custom_field.send(custom_field_desc.internal_name)}
            </div>
          </div>"
    ret.html_safe
  end

  def show_custom_field_as_plain_text_rich_text(custom_field, custom_field_desc)
    custom_field.send(custom_field_desc.internal_name)&.body&.to_plain_text
  end

  def show_custom_field_inline_rich_text(custom_field, custom_field_desc)
    custom_field.send(custom_field_desc.internal_name)
  end

  def show_custom_field_select(custom_field, custom_field_desc)
    cf_value = custom_field.send(custom_field_desc.internal_name+'_value')
    return cf_value.nil? ? '-' : cf_value.name
  rescue StandardError
    'list element not found'
  end

  def show_custom_field_tree(custom_field, custom_field_desc)
    cf_value = custom_field.send(custom_field_desc.internal_name+'_value')
    return cf_value.nil? ? '-' : cf_value.name
  rescue StandardError
    'tree element not found'
  end

  def show_custom_field_date(custom_field, custom_field_desc)
    custom_field.send(custom_field_desc.internal_name).to_s
  end

  def show_custom_field(custom_field, custom_field_desc)
    method('show_custom_field_'+custom_field_desc.custom_field_type.field_internal).call(custom_field, custom_field_desc)
  rescue StandardError
    begin
      # puts 'SEND CUSTOM FIELD VALUE'.yellow
      # puts custom_field.send(custom_field_desc.internal_name)
      custom_field.send(custom_field_desc.internal_name) # .to_s
    rescue StandardError
      "<i>Custom field #{custom_field_desc.internal_name} doesn't exist in database</i>".html_safe
    end
  end

  def show_custom_field_inline(custom_field, custom_field_desc)
    method('show_custom_field_inline_'+custom_field_desc.custom_field_type.field_internal).call(custom_field, custom_field_desc)
  rescue StandardError
    show_custom_field(custom_field, custom_field_desc)
  end

  def show_custom_field_as_plain_text(custom_field, custom_field_desc)
    method('show_custom_field_as_plain_text_'+custom_field_desc.custom_field_type.field_internal).call(custom_field, custom_field_desc)
  rescue StandardError
    show_custom_field(custom_field, custom_field_desc)
  end
end
