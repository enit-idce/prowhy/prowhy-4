module ProwhyApi
  module V1
    module Entities
      class User < Grape::Entity
        expose :id, if: { with_id: true }
        expose :username
        expose :email

        # expose :person, with: Entities::Person
        # binding.pry

        expose :person, if: { with_person: true }
        # expose :person do |user, options|
        #   Entities::Person.represent(user.person, { with_user: false, with_id: true }) if options[:with_person] == true
        # end

        private

        def person
          Entities::Person.represent(object.person, { with_user: false, with_id: true })
        end
        # end
      end
    end
  end
end
