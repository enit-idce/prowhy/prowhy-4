json.extract! meth_tree_content, :id, :name_fr, :name_en, :custom_field_desc_id, :ancestry, :created_at, :updated_at
json.url meth_tree_content_url(meth_tree_content, format: :json)
