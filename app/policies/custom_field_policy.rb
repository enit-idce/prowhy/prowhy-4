class CustomFieldPolicy < AdminPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def update_db?
    user.has_role?(:super_admin)
  end

  def destroy?
    # false
    user.has_role?(:super_admin)
  end
end
