require_dependency 'tools'

module Tools
  class CauseClass < ApplicationRecord
    belongs_to :tools_cause, class_name: 'Tools::Cause' # , inverse_of: :tools_cause_classes
    belongs_to :tools_ishikawa, class_name: 'Tools::Ishikawa'
    belongs_to :labels_cause_type, class_name: 'Labels::CauseType', optional: true

    # accepts_nested_attributes_for :labels_cause
  end
end
