class ProfilsController < ApplicationController
  include AdminConcern
  before_action :set_profil, only: [:show, :edit, :update, :destroy, :raz_authorizations]

  # GET /profils
  # GET /profils.json
  def index
    @profils = Profil.all
    authorize Profil
  end

  # # GET /profils/1
  # # GET /profils/1.json
  # def show
  # end

  # GET /profils/new
  def new
    @profil = Profil.new
    authorize @profil
  end

  # GET /profils/1/edit
  def edit
  end

  # POST /profils
  # POST /profils.json
  def create
    @profil = Profil.new(profil_params)
    @profil.pos = Profil.count+1
    authorize @profil

    respond_to do |format|
      if @profil.save
        format.html { redirect_to profils_url, notice: t('.message_succes', profil: Profil.model_name.human) }
        format.json { render :show, status: :created, location: @profil }
      else
        format.html { render :new }
        format.json { render json: @profil.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profils/1
  # PATCH/PUT /profils/1.json
  def update
    respond_to do |format|
      if @profil.update(profil_params)
        format.html { redirect_to profils_url, notice: t('.message_succes', profil: Profil.model_name.human) }
        format.json { render :show, status: :ok, location: @profil }
      else
        format.html { render :edit }
        format.json { render json: @profil.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profils/1
  # DELETE /profils/1.json
  def destroy
    @profil.destroy
    respond_to do |format|
      format.html { redirect_to profils_url, notice: t('.message_succes', profil: Profil.model_name.human) }
      format.json { head :no_content }
    end
  end

  # PATCH raz_authorizations
  def raz_authorizations
    @profil.raz_authorizations

    redirect_to profil_authorizes_path(profil_id: @profil.id), notice: 'Done.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_profil
    @profil = Profil.find(params[:id])
    authorize @profil
  end

  # Only allow a list of trusted parameters through.
  def profil_params
    params.require(:profil).permit(:profil_name, :name, :name_fr, :name_en, :profil_locked)
  end
end
