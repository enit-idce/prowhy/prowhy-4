class ChangeToolOptionsFormat < ActiveRecord::Migration[6.0]
  class MigrationTool < ApplicationRecord
    self.table_name = :meth_tools
    serialize :tool_options_backup
  end

  def up
    rename_column :meth_tools, :tool_options, :tool_options_backup
    add_column :meth_tools, :tool_options, :json, after: :tool_options_backup

    say 'Convert tool_options datas.'
    MigrationTool.all.each do |tool|
      # tool.tool_options_json = tool.tool_options # .to_json
      tool.tool_options = tool.tool_options_backup # .to_json
      tool.save!
    end

    # rename_column :meth_tools, :tool_options_json, :tool_options
  end

  def down
    remove_column :meth_tools, :tool_options
    rename_column :meth_tools, :tool_options_backup, :tool_options
  end
end
