class AddTranslationsToListContents < ActiveRecord::Migration[6.0]
  def change
    # Meth::ListContent
    add_column :meth_list_contents, :name_fr, :string, after: :name
    add_column :meth_list_contents, :name_en, :string, after: :name_fr

    Meth::ListContent.all.each do |elem|
      elem.name_fr = elem.name
      elem.save!
    end

    remove_column :meth_list_contents, :name
  end
end
