
// document.addEventListener("turbolinks:load", () => {
//   // $('#home-page-chart').on('draw-chart', function(event){
//   //   console.log('trigger click ok');
//   //   draw_home_page_chart();
//   // })

//   // $('#home-page-chart').trigger('draw-chart');
//   draw_home_page_chart();
// });


// var bgcol = [ 'rgba(255, 159, 64, 0.2)', // orange
//               'rgba(255, 99, 132, 0.2)', // rose
//               'rgba(54, 162, 235, 0.2)', // bleu
//               'rgba(255, 206, 86, 0.2)', // jaune
//               'rgba(153, 102, 255, 0.2)', // violet
//               'rgba(75, 192, 192, 0.2)', // vert]
//               'rgba(230, 50, 220, 0.2)', // rose 2
//               'rgba(0, 128, 255, 0.2)', // bleu 2
//               'rgba(255, 30, 20, 0.2)', // rouge
//               'rgba(0, 180, 80, 0.2)', // vert 2
//               'rgba(128, 0, 255, 0.2)' // violet 2
//             ]
// var bdcol = [ 'rgba(255, 159, 64, 1)', // orange
//               'rgba(255, 99, 132, 1)', // rose
//               'rgba(54, 162, 235, 1)', // bleu
//               'rgba(255, 206, 86, 1)', // jaune
//               'rgba(153, 102, 255, 1)', // violet
//               'rgba(75, 192, 192, 1)', // vert
//               'rgba(230, 50, 220, 1)', //
//               'rgba(0, 128, 255, 1)', //
//               'rgba(255, 30, 20, 1)', // rouge
//               'rgba(0, 180, 80, 1)', //
//               'rgba(128, 0, 255, 1)', //
//             ]

// function draw_home_page_chart() {
//   let elements = document.getElementsByClassName('home-page-charts');

//   for (let ctx of elements) {
//     let chart_home_page = new Chart(ctx, {
//       type: 'bar',
//       data: {
//         labels: JSON.parse(ctx.getAttribute('attr-chart-labels')), // ['D1', 'D2', 'D3', 'D4', 'D5', 'D6'],
//         datasets: [{
//           label: '# of Rcas',
//           data: JSON.parse(ctx.getAttribute('attr-chart-values')), // [12, 19, 3, 5, 2, 3],
//           backgroundColor: bgcol,
//           borderColor: bdcol,
//           borderWidth: 1
//         }]
//       },
//       options: {
//         responsive: true,
//         maintainAspectRatio: false,
//           scales: {
//               y: {
//                   beginAtZero: true
//               }
//           }
//       }
//     });
//   }
// }
