json.extract! tools_efficacity, :id, :element_type, :element_id, :check_status, :percentage, :date_due_check, :date_done_check, :created_at, :updated_at
json.url tools_efficacity_url(tools_efficacity, format: :json)
