module Labels
  class CauseTypesController < ApplicationController
    include AdminConcern
    # layout 'admin'
    before_action :set_labels_cause_type, only: [:edit, :update, :destroy]

    # GET /labels/cause_types
    # GET /labels/cause_types.json
    def index
      @labels_cause_types = Labels::CauseType.all
      authorize CauseType
    end

    # # GET /labels/cause_types/1
    # # GET /labels/cause_types/1.json
    # def show
    # end

    # GET /labels/cause_types/new
    def new
      @labels_cause_type = Labels::CauseType.new
      authorize @labels_cause_type
    end

    # GET /labels/cause_types/1/edit
    def edit
    end

    # POST /labels/cause_types
    # POST /labels/cause_types.json
    def create
      @labels_cause_type = Labels::CauseType.new(labels_cause_type_params)
      authorize @labels_cause_type

      respond_to do |format|
        if @labels_cause_type.save
          format.html { redirect_to labels_cause_types_url, notice: t('.message_succes', cause_type: Labels::CauseType.model_name.human) }
          format.json { render :show, status: :created, location: @labels_cause_type }
        else
          format.html { render :new }
          format.json { render json: @labels_cause_type.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /labels/cause_types/1
    # PATCH/PUT /labels/cause_types/1.json
    def update
      respond_to do |format|
        if @labels_cause_type.update(labels_cause_type_params)
          format.html { redirect_to labels_cause_types_url, notice: t('.message_succes', cause_type: Labels::CauseType.model_name.human) }
          format.json { render :show, status: :ok, location: @labels_cause_type }
        else
          format.html { render :edit }
          format.json { render json: @labels_cause_type.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /labels/cause_types/1
    # DELETE /labels/cause_types/1.json
    def destroy
      @labels_cause_type.destroy
      respond_to do |format|
        format.html { redirect_to labels_cause_types_url, notice: t('.message_succes', cause_type: Labels::CauseType.model_name.human) }
        format.json { head :no_content }
      end
    end

    # GET => open modal to set cause types to step_tool
    # TODO : déplacer le contenu dans une fonction du model step_tool (pour tests séparés model et request)
    # ici appel de la fonction : @labels = @step_tool.list_labels
    def cause_types_list
      authorize CauseType

      @step_tool = Meth::StepTool.find(params[:step_tool])
      @list_labels = []
      # if @step_tool.tool_options[:labels]
      @step_tool&.tool_options&.[](:labels)&.keys&.sort&.each do |k|
        @list_labels << @step_tool.tool_options[:labels][k]
      end
      # end

      @labels = Labels::CauseType.all
    end

    # POST => update step_tool options (cause types list for Ishikawa)
    # TODO : déplacer le contenu dans une fonction du model (ou service ?) step_tool
    # => pour tests séparés model=test résultats et request=test fonctionnement/redirections
    # ici appel de la fonction : @labels = @step_tool.list_labels_set(params[labels_ids])
    def cause_types_list_set
      authorize CauseType

      @step_tool = Meth::StepTool.find(params[:step_tool])
      @step_tool.tool_options ||= {}
      @step_tool.tool_options[:labels] = {}
      pos = 1
      params&.[]('labels_ids')&.each do |label_id|
        @step_tool.tool_options[:labels][pos] = label_id.to_i
        pos += 1
      end
      @step_tool.save!

      redirect_to edit_meth_methodology_path(@step_tool.step.methodology, step: @step_tool.step)
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_labels_cause_type
      @labels_cause_type = Labels::CauseType.find(params[:id])
      authorize @labels_cause_type
    end

    # Only allow a list of trusted parameters through.
    def labels_cause_type_params
      params.require(:labels_cause_type).permit(:name, :name_fr, :name_en, :pos)
    end
  end
end
