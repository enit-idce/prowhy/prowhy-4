require 'doorkeeper/grape/helpers'
require 'devise'

module ProwhyApi
  module V1
    module ApiLabels
      class ApiCauseTypes < V1::Api
        # before do
        #   doorkeeper_authorize! unless user_signed_in?
        # end

        resource :cause_types do
          desc 'Return Cause Types for ishikawa linked to Rca #rca_id.'
          params do
            requires :rca_id, type: Integer, desc: 'Rca ID.'
            optional :tool_reference, type: Integer, desc: 'Ishikawa reference (default=1).', default: 1
            optional :tool_num, type: Integer, desc: 'Ishikawa num (default=1).', default: 1

            # use :locale
          end
          get do
            @rca = Rca.find(params[:rca_id])
            authorize @rca, :show?

            @ishikawa = @rca.tools_ishikawas.where(tool_reference: params[:tool_reference] || 1, tool_num: params[:tool_num] || 1)&.first

            raise ActiveRecord::RecordNotFound unless @ishikawa

            present @ishikawa.labels_cause_types, with: Entities::ApiLabels::CauseType
          end
        end
      end
    end
  end
end
