class DataAddDefaultPerimeterToAdminConfig < ActiveRecord::Migration[6.0]
  class MigrationAdminConfig < ApplicationRecord
    self.table_name = :admin_admin_configs
  end

  class MigrationPerimeter < ApplicationRecord
    self.table_name = :admin_perimeters
  end

  def up
    admin_config = MigrationAdminConfig.first
    perimeter = MigrationPerimeter.where(ancestry: nil).first

    if admin_config && perimeter
      say 'Assign first perimeter to admin config.'
      admin_config.default_perimeter_id = perimeter.id
      admin_config.save!
    else
      say 'Impossible to assign perimeter to Admin config : do not exists.'
    end
  end

  def down
    say 'Do not destroy default perimeter from admin config.'
  end
end
