module Tools
  class QqoqcpsController < ApplicationController
    include ProcessConcern
    before_action :set_tools_qqoqcp, only: [:show, :edit, :update, :destroy]
    before_action :init_rca, only: [:index, :new, :create]

    after_action :call_track_action, only: [:create, :destroy, :update]

    # GET /tools/qqoqcps
    # GET /tools/qqoqcps.json
    # def index
    #   @tools_qqoqcps = Tools::Qqoqcp.all
    # end

    # GET /tools/qqoqcps/1
    # GET /tools/qqoqcps/1.json
    # ===> sera décommentée pour show dans rapport
    # def show
    # end

    # GET /tools/qqoqcps/new
    def new
      if @rca.nil? || current_user.get_session(:active_step_tool).nil?
        authorize Qqoqcp

        redirect_to rcas_path
      else
        @tools_qqoqcp = Tools::Qqoqcp.new

        authorize @tools_qqoqcp
      end
    end

    # GET /tools/qqoqcps/1/edit
    def edit
      redirect_to rcas_path and return if @rca.nil? || current_user.get_session(:active_step_tool).nil?

      @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
    end

    # POST /tools/qqoqcps
    # POST /tools/qqoqcps.json
    def create
      @tools_qqoqcp = Tools::Qqoqcp.new(tools_qqoqcp_params)
      authorize @tools_qqoqcp

      respond_to do |format|
        if @tools_qqoqcp.save
          format.html { redirect_to edit_tools_qqoqcp_url(@tools_qqoqcp), notice: 'Qqoqcp was successfully created.' }
          format.json { render :show, status: :created, location: @tools_qqoqcp }
        else
          format.html { render :new }
          format.json { render json: @tools_qqoqcp.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /tools/qqoqcps/1
    # PATCH/PUT /tools/qqoqcps/1.json
    def update
      respond_to do |format|
        if @tools_qqoqcp.update(tools_qqoqcp_params)
          @tools_qqoqcp.initialize_qqoqcp

          # binding.pry

          format.html { redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca), notice: 'Qqoqcp was successfully updated.' }
          format.json { render :show, status: :ok, location: @tools_qqoqcp }
        else
          format.html { render :edit }
          format.json { render json: @tools_qqoqcp.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /tools/qqoqcps/1
    # DELETE /tools/qqoqcps/1.json
    def destroy
      @tools_qqoqcp.destroy
      respond_to do |format|
        format.html { redirect_to tools_qqoqcps_url(rca: @rca), notice: 'Qqoqcp was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_qqoqcp
      @tools_qqoqcp = Tools::Qqoqcp.find(params[:id])
      @rca = @tools_qqoqcp.rca

      # authorize @rca, :show?
      authorize @tools_qqoqcp
    end

    # Only allow a list of trusted parameters through.
    def tools_qqoqcp_params
      params.require(:tools_qqoqcp).permit(:rca_id, :tool_reference, :tool_num,
                                           :qui, :qui_not, :qui_info,
                                           :quoi, :quoi_not, :quoi_info,
                                           :ou, :ou_not, :ou_info,
                                           :quand, :quand_not, :quand_info,
                                           :comment, :comment_not, :comment_info,
                                           :pourquoi, :pourquoi_not, :pourquoi_info,
                                           :combien, :combien_not, :combien_info,
                                           :labels_qqoqcp_config_id,
                                           qqoqcp_values_attributes: [:id, :is_value, :isnot_value, :info_value])
    end

    # TRACK
    # def call_track_action
    #   binding.pry
    #   begin
    #     my_params = tools_qqoqcp_params
    #   rescue StandardError
    #     my_params = {}
    #   end
    #   # track_action("RCA - #{params[:action]}", {rca_id: @rca&.id}.merge(rca_p))
    #   track_action("Tools::Qqoqcp - #{params[:action]}", @rca, @tools_qqoqcp, my_params)
    # end

    # def call_track_action
    #   name_ctrl = params[:controller]
    #   begin
    #     obj_name = name_ctrl.gsub('/', '_').singularize
    #   rescue StandardError
    #     obj_name = 'no_object'
    #   end

    #   begin
    #     obj = instance_variable_get("@#{obj_name}")
    #   rescue StandardError
    #     obj = nil
    #   end

    #   begin
    #     my_params = send("#{obj_name}_params") # tools_qqoqcp_params
    #   rescue StandardError
    #     my_params = {}
    #   end

    #   # track_action("RCA - #{params[:action]}", {rca_id: @rca&.id}.merge(rca_p))
    #   track_action("#{name_ctrl.classify} - #{params[:action]}",
    #                @rca,
    #                obj,
    #                my_params)
    # end
  end
end
