json.extract! tools_decision_mat, :id, :name, :entry_class, :tool_reference, :tool_num, :created_at, :updated_at
json.url tools_decision_mat_url(tools_decision_mat, format: :json)
