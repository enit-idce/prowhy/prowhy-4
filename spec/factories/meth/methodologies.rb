FactoryBot.define do
  factory :methodology, class: Meth::Methodology do
    name { 'NC' }

    trait :with_step_info do
      after(:build) do |methodology|
        methodology.create_methodology
      end
    end

    trait :with_step_1 do
      after(:create) do |methodology|
        methodology.create_methodology
        methodology.steps << FactoryBot.create(:meth_step, :with_tool, methodology: methodology)
      end
    end
  end
end
