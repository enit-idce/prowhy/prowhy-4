class CreateLabelsConfigs < ActiveRecord::Migration[6.0]
  def change
    create_table :labels_configs do |t|
      t.string :name_fr
      t.string :name_en
      t.string :formula
      t.boolean :usable, default: true

      t.timestamps
    end

    add_reference :labels_criteria, :config, foreign_key: { to_table: :labels_configs }, after: :threshold
  end
end
