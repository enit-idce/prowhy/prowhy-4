json.extract! tools_image, :id, :rca_id, :tool_reference, :tool_num, :name, :description, :created_at, :updated_at
json.url tools_image_url(tools_image, format: :json)
