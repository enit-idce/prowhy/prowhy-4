module PolicyStandardConcern
  extend ActiveSupport::Concern

  included do
    after_action :verify_authorized, except: [:index, :reinit_all_advancements]
    after_action :verify_policy_scoped, only: [:index, :reinit_all_advancements]
  end
end
