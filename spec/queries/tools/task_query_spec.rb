require 'rails_helper'

describe Tools::TaskQuery, type: 'query' do
  subject(:query) { Tools::TaskQuery.relation }

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
  let(:cause) { Tools::Cause.create!(rca: rca, tool_reference: 1, name: 'Cause 1') }
  let(:person) { FactoryBot.create(:person) }

  before :each do
    Tools::Task.create!([{id: 1, rca: rca, tool_reference: 1, tool_num: 1, name: 'Task G 1', tools_cause: cause, task_detail_type: 'Corrective',
                          responsible_action: person, responsible_rca: person},
                         {id: 2, rca: rca, tool_reference: 1, tool_num: 1,
                          name: 'Task G 2', tools_cause: cause, task_detail_type: 'Preventive',
                          responsible_action: person, responsible_rca: person},
                         {id: 3, rca: rca, tool_reference: 1, tool_num: 1,
                          name: 'Task G 3', tools_cause: cause, task_detail_type: 'Corrective', ancestry: '1',
                          responsible_action: person, responsible_rca: person},
                         {id: 4, rca: rca, tool_reference: 1, tool_num: 1,
                          name: 'Task 3.1', tools_cause: cause, task_detail_type: 'Corrective', ancestry: '1/3',
                          responsible_action: person, responsible_rca: person},
                         {id: 5, rca: rca, tool_reference: 1, tool_num: 1,
                          name: 'Task 3.2', tools_cause: cause, task_detail_type: 'Corrective', ancestry: '1/3',
                          responsible_action: person, responsible_rca: person},
                         {id: 6, rca: rca, tool_reference: 1, tool_num: 1,
                          name: 'Task 1.1', tools_cause: cause, task_detail_type: 'Preventive', ancestry: '2',
                          responsible_action: person, responsible_rca: person},
                         {id: 7, rca: rca, tool_reference: 1, tool_num: 1,
                          name: 'Task 1.2', tools_cause: cause, task_detail_type: 'Preventive', ancestry: '2',
                          responsible_action: person, responsible_rca: person},
                         {id: 8, rca: rca, tool_reference: 1, tool_num: 1,
                          name: 'Task 3.2.1', tools_cause: cause, task_detail_type: 'Corrective', ancestry: '1/3/5',
                          responsible_action: person, responsible_rca: person},
                         {id: 9, rca: rca, tool_reference: 1, tool_num: 1,
                          name: 'Task 3.1.2', tools_cause: cause, task_detail_type: 'Corrective', ancestry: '1/3/4',
                          responsible_action: person, responsible_rca: person},
                         {id: 10, rca: rca, tool_reference: 1, tool_num: 1,
                          name: 'Task G 4', tools_cause: cause, task_detail_type: 'Preventive',
                          responsible_action: person, responsible_rca: person}])
  end

  describe '#tasks_subtree' do
    it 'returns the subtree arranged as array for list for Corrective tasks' do
      result = query.tasks_subtree(rca.id, 1, 1, 'Corrective')
      expect(result.map(&:id)).to eq [1, 3, 4, 9, 5, 8]
    end
  end

  describe '#tasks_subtree_by_task_infos' do
    it 'returns the subtree arranged as array with options = infos from task' do
      result = query.tasks_subtree_by_task_infos(Tools::Task.find(2))
      # binding.pry
      expect(result.map(&:id)).to eq [2, 6, 7, 10]
    end
  end

  describe '#tasks_head' do
    it 'returns the heads tasks for one rca type Preventive' do
      result = query.tasks_head(rca.id, 1, 1, 'Preventive')
      expect(result.map(&:id)).to eq [2, 10]
    end
  end
end
