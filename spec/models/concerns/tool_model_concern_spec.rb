# binding.pry
require 'rails_helper'

shared_examples_for ToolModelConcern, type: 'model' do
  # subject(:obj) do
  #   obj = if defined?(described_obj)
  #           described_obj
  #         else
  #           described_class.new
  #         end
  #   obj.save(validate: false)
  #   obj
  # end
  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
  let(:step_tool) { Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to rca' do
        assc = described_class.reflect_on_association(:rca)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end

  describe 'Search model : tool model concern functions' do
    it 'search_default when object does not exist' do
      obj_founded = described_class.search_default(rca, described_class.table_name, step_tool)

      if described_class.create_search_multiple?
        expect(obj_founded.length).to eq 0
      else
        expect(obj_founded).to eq nil
      end
    end

    it 'search_default when object exists' do
      described_class.create_default!({rca: rca, tool_reference: 1}, step_tool)
      step_tool.tool_reference = 2
      obj2 = described_class.create_default!({rca: rca, tool_reference: 2}, step_tool)
      obj_founded = described_class.search_default(rca, described_class.table_name, step_tool)

      if described_class.create_search_multiple?
        # expect(obj_founded.length).to eq 1
        # expect(obj_founded[0]).to eq obj2
        # Cas des Tasks => retourne [] car aucune task crée par create_default.
      else
        expect(obj_founded).to eq obj2
      end
    end

    it 'returns 1 as tool_num' do
      obj_created = described_class.create_default!({rca: rca, tool_reference: 1}, step_tool)
      expect(obj_created.tool_num).to eq 1
    end
  end
end

# describe Tools::Qqoqcp do
#   it_behaves_like ToolModelConcern do
#   end
# end

describe Tools::Cause do
  it_behaves_like ToolModelConcern do
  end
end

# describe Tools::Task do
#   it_behaves_like ToolModelConcern do
#   end
# end
