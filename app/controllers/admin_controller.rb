class AdminController < ApplicationController
  include AdminConcern

  def index
    authorize Admin, policy_class: AdminPolicy
  end

  def show_ahoy
    authorize Admin, policy_class: AdminPolicy

    @charts_range = params[:charts_range]&.to_i || 3

    respond_to do |format|
      format.html { render :show_ahoy }
      format.js { render :show_ahoy }
    end
  end
end
