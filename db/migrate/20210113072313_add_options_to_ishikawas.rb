class AddOptionsToIshikawas < ActiveRecord::Migration[6.0]
  def change
    add_column :tools_ishikawas, :options, :text, after: :tool_num
  end
end
