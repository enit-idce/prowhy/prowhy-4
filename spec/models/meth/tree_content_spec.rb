require 'rails_helper'

RSpec.describe Meth::TreeContent, type: :model do
  subject(:tree_content) do
    FactoryBot.create(:meth_tree_content, custom_field_desc: custom_field_desc)
  end

  let(:custom_field_desc) { FactoryBot.create(:meth_custom_field_desc, custom_field_type: custom_field_type) }
  let(:custom_field_type) { FactoryBot.create(:meth_custom_field_type, :type_tree) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to custom_field_desc' do
        assc = Meth::TreeContent.reflect_on_association(:custom_field_desc)
        expect(assc.macro).to eq :belongs_to
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        tree_content.name = ''
        expect(tree_content).to_not be_valid
      end

      it 'is valid with name' do
        expect(tree_content).to be_valid
      end

      it 'is not valid with custom field desc with type:field_internal different from tree' do
        tree_content
        custom_field_type.field_internal = 'text'
        custom_field_type.save!
        expect(tree_content).to_not be_valid
      end

      it 'is valid with custom field desc with type:field_internal = tree' do
        expect(tree_content).to be_valid
      end
    end
  end

  describe 'Show name' do
    it 'shows name if Tree content is not a root element' do
      tree_content2= FactoryBot.create(:meth_tree_content, custom_field_desc: custom_field_desc, parent_id: tree_content.id)
      expect(tree_content2.name_show).to eq tree_content2.name.downcase
    end
    it 'shows custom field desc name + name if Tree content is a root element' do
      expect(tree_content.name_show).to eq "#{custom_field_desc.name} : #{tree_content.name.downcase}"
    end
  end
end
