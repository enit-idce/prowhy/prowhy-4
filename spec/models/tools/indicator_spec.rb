require 'rails_helper'

RSpec.describe Tools::Indicator, type: :model do
  let(:indicator) { FactoryBot.create(:indicator, rca: rca) }

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has manyo indicator_measures' do
        assc = Tools::Indicator.reflect_on_association(:indicator_measures)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        indicator.name = ''
        expect(indicator).to_not be_valid
      end

      it 'is valid with name' do
        indicator.name = 'Le indicateur'
        expect(indicator).to be_valid
      end
    end
  end

  describe 'Create and search model : tool class functions' do
    before :each do
      indicator.save!
    end

    it 'create_default' do
      options = { rca: rca, tool_reference: 2 }
      expect(Tools::Indicator.create_default!(options, nil)).to eq []
    end

    it 'search_default' do
      step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1)
      indicators_founded = Tools::Indicator.search_default(rca, 'tools_indicators', step_tool)

      expect(indicators_founded).to eq [indicator]

      step_tool.tool_reference = 2
      indicators_founded = Tools::Indicator.search_default(rca, 'tools_indicators', step_tool)

      expect(indicators_founded).to eq []
    end
  end
end
