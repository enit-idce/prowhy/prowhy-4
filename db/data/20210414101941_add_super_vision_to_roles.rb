class AddSuperVisionToRoles < ActiveRecord::Migration[6.0]
  class MigrationRole < ApplicationRecord
    self.table_name = :roles
  end

  def up
    super_vision_role = MigrationRole.where(id: 6)
    if super_vision_role.empty?
      say 'Create role super_vision.'
      MigrationRole.create!(id: 6, name: 'super_vision')
    else
      say 'Role super_vision id 6 exists in database.'
    end
  end

  def down
    say 'Destroy role super_vision.'
    super_vision_role = MigrationRole.where(id: 6)&.first
    super_vision_role&.destroy
  rescue StandardError
    say 'Can not destroy role.'
    raise ActiveRecord::IrreversibleMigration
  end
end
