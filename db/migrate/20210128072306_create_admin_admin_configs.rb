class CreateAdminAdminConfigs < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_admin_configs do |t|
      t.text :connexions
      t.references :default_role, null: true, foreign_key: { to_table: :roles }
      t.text :task_mails

      t.timestamps
    end
  end
end
