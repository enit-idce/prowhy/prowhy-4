module Labels
  class QqoqcpLine < ApplicationRecord
    translates :name, fallback: :any

    validates :name, presence: true
    validates :internal_name, presence: true,
                              uniqueness: { case_sensitive: false },
                              format: { with: /\A[a-zA-Z0-9_]+\z/, message: 'only allows letters, numbers and underscore' }

    has_many :qqoqcp_cls # , dependent: :destroy
    has_many :qqoqcp_configs, through: :qqoqcp_cls

    has_many :tools_qqoqcp_values, class_name: 'Tools::QqoqcpValue'
  end
end
