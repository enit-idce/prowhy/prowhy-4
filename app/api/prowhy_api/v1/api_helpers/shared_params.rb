module ProwhyApi
  module V1
    module ApiHelpers
      module SharedParams
        extend Grape::API::Helpers

        # params :pagination do
        #   optional :page, type: Integer
        #   optional :per_page, type: Integer
        # end

        params :locale do
          optional :locale, type: String
        end
      end
    end
  end
end
