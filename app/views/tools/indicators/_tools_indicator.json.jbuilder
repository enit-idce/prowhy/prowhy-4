json.extract! tools_indicator, :id, :rca_id, :tool_reference, :tool_num, :name, :description, :target, :graph_options, :created_at, :updated_at
json.url tools_indicator_url(tools_indicator, format: :json)
