json.extract! tools_team, :id, :person_id, :rca_id, :profil_id, :created_at, :updated_at
json.url tools_team_url(tools_team, format: :json)
