class ChangeStepToolOptionsFormat < ActiveRecord::Migration[6.0]
  class MigrationStepTool < ApplicationRecord
    self.table_name = :meth_step_tools
    serialize :tool_options_backup
  end

  def up
    rename_column :meth_step_tools, :tool_options, :tool_options_backup
    add_column :meth_step_tools, :tool_options, :json, after: :tool_options_backup

    say 'Convert step_tool::tool_options datas.'
    MigrationStepTool.all.each do |step_tool|
      # step_tool.tool_options_json = step_tool.tool_options # .to_json
      step_tool.tool_options = step_tool.tool_options_backup # .to_json
      step_tool.save!
    end

    # rename_column :meth_step_tools, :tool_options_json, :tool_options
  end

  def down
    remove_column :meth_step_tools, :tool_options
    rename_column :meth_step_tools, :tool_options_backup, :tool_options
  end
end
