require 'rails_helper'

RSpec.describe Tools::ImagesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/tools/images').to route_to('tools/images#index')
    end

    it 'routes to #new' do
      expect(get: '/tools/images/new').to route_to('tools/images#new')
    end

    it 'routes to #show' do
      expect(get: '/tools/images/1').to route_to('tools/images#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/tools/images/1/edit').to route_to('tools/images#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/tools/images').to route_to('tools/images#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/tools/images/1').to route_to('tools/images#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/tools/images/1').to route_to('tools/images#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/tools/images/1').to route_to('tools/images#destroy', id: '1')
    end
  end
end
