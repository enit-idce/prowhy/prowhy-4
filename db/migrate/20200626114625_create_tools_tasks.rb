class CreateToolsTasks < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_tasks do |t|
      t.references :rca, null: false, foreign_key: true
      t.integer :tool_reference
      t.string :name
      t.references :tools_cause, null: false, foreign_key: true
      t.integer :advancement
      t.references :responsible_action, null: false, foreign_key: { to_table: :users }
      t.references :responsible_rca, null: false, foreign_key: { to_table: :users }
      t.text :result
      t.integer :priority, null: false, default: 0
      t.date :date_due
      t.date :date_completion

      t.timestamps
    end
  end
end
