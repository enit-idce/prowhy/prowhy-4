json.extract! list_content, :id, :custom_field_desc_id, :name, :pos, :created_at, :updated_at
json.url meth_list_content_url(list_content, format: :json)
