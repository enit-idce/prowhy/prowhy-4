require_dependency 'labels'

module Labels
  class CauseType < ApplicationRecord
    translates :name, fallback: :any
    # validates(*locale_columns(:name), presence: true)
    validates :name, presence: true
    # has_many :tools_causes

    # Liens vers les ishikawas
    has_many :tools_ishi_branchs, class_name: 'Tools::IshiBranch', foreign_key: :labels_cause_type_id, dependent: :destroy
    has_many :tools_ishikawas, class_name: 'Tools::Ishikawa', through: :tools_ishi_branchs

    # Lien vers les causes par ishikawa
    has_many :tools_cause_classes, class_name: 'Tools::CauseClass', foreign_key: :labels_cause_type_id, dependent: :destroy
    # Commenté car à priori inutile.
    # has_many :tools_ishikawas_used, class_name: 'Tools::Ishikawa', through: :tools_cause_classes, foreign_key: :tools_ishikawa_id
    has_many :tools_causes, class_name: 'Tools::Cause', through: :tools_cause_classes
  end
end
