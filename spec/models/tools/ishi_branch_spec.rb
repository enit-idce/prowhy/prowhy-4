require 'rails_helper'

RSpec.describe Tools::IshiBranch, type: :model do
  context 'describes model associations and validations' do
    describe 'Associations' do
      # it 'belongs to labels_cause_type' do
      #   assc = Tools::Cause.reflect_on_association(:labels_cause_type)
      #   expect(assc.macro).to eq :belongs_to
      # end
      it 'belongs to tools_ishikawa' do
        assc = Tools::IshiBranch.reflect_on_association(:tools_ishikawa)
        expect(assc.macro).to eq :belongs_to
      end
      it 'belongs to labels_cause_type' do
        assc = Tools::IshiBranch.reflect_on_association(:labels_cause_type)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end
end
