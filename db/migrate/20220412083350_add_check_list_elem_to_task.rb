class AddCheckListElemToTask < ActiveRecord::Migration[6.1]
  def change
    add_reference :tools_tasks, :check_list_elem, foreign_key: { to_table: :labels_check_list_elems }, after: :name
  end
end
