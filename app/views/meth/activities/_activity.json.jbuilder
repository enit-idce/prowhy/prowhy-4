json.extract! activity, :id, :name, :pos, :created_at, :updated_at
json.url activity_url(activity, format: :json)
