module Tools
  class IshikawaPolicy < ToolsPolicy
    attr_reader :user, :record, :rca

    def index?
      show?
    end

    def ishikawa_js_tree_data?
      show?
    end

    def ishikawa_drag_cause?
      update?
    end

    def ishikawa_reset_branchs?
      update?
    end

    def ishikawa_zoom?
      show?
    end

    def ishikawa_update_option?
      show?
    end

    def save_image?
      true
    end
  end
end
