require 'doorkeeper/grape/helpers'
require 'devise'

module ProwhyApi
  module V1
    class ApiProfils < Grape::API
      resource :profils do
        desc 'Return all People.'
        params do
        end
        get do
          @profils = Profil.all

          present @profils, with: Entities::Profil,
                            with_id: true
        end

        desc 'Return Person :id'
        params do
          requires :id, type: Integer, desc: 'Profil ID.'
        end
        route_param :id do
          get do
            @profil = Profil.find(params[:id])
            authorize @profil, :show?

            present @profil, with: Entities::Profil,
                             with_id: false
          end
        end
      end
    end
  end
end
