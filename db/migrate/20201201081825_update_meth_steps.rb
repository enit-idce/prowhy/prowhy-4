class UpdateMethSteps < ActiveRecord::Migration[6.0]
  def change
    # Add step_role to Step
    add_reference :meth_steps, :step_role, foreign_key: { to_table: :meth_step_roles }, after: :step_type

    # Add links for img and doc step tools to Step
    # Add locked boolean to Step
    # add_reference :meth_steps, :step_tool_doc, foreign_key: { to_table: :meth_step_tools }, after: :step_role_id
    # add_reference :meth_steps, :step_tool_img, foreign_key: { to_table: :meth_step_tools }, after: :step_tool_doc_id
    add_column :meth_steps, :locked, :boolean, default: false, after: :step_role_id

    # Add step_tool_type and locked boolean to StepTool
    add_column :meth_step_tools, :step_tool_type, :integer, default: 1, after: :tool_id
    add_column :meth_step_tools, :locked, :boolean, default: false, after: :step_tool_type
  end
end
