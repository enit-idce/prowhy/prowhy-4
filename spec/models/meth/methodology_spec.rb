require 'rails_helper'

RSpec.describe Meth::Methodology, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
  subject(:methodology) do
    Meth::Methodology.new(name: 'Method 1')
  end

  before :each do
    # Ici : load seed avec les activity + les tools => les choses nécessaires à la création de méthodo.
    # Et tester que SANS ce seed ça ne fonctionne pas.
    activity = Meth::Activity.create!(name: 'Infos')
    Meth::Tool.create!([{name: 'Tool 1', activity: activity, tool_name: 'ToolTruc'},
                        {name: 'Champs personnalisés', activity: activity, tool_name: 'CustomField'}])
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has many steps' do
        assc = Meth::Methodology.reflect_on_association(:steps)
        expect(assc.macro).to eq :has_many
      end

      it 'has many steps_menu' do
        assc = Meth::Methodology.reflect_on_association(:steps_menu)
        expect(assc.macro).to eq :has_many
      end

      it 'has many rcas' do
        assc = Meth::Methodology.reflect_on_association(:rcas)
        expect(assc.macro).to eq :has_many
      end

      # it 'belongs to step_info' do
      #   assc = Meth::Methodology.reflect_on_association(:step_info)
      #   expect(assc.macro).to eq :belongs_to
      # end

      it 'has_one to step_info' do
        assc = Meth::Methodology.reflect_on_association(:step_info)
        expect(assc.macro).to eq :has_one
      end

      it 'has_one step_trstools' do
        assc = Meth::Methodology.reflect_on_association(:step_trstools)
        expect(assc.macro).to eq :has_one
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        methodology.name = ''
        expect(methodology).to_not be_valid
      end

      it 'is valid with name & tool_name' do
        expect(methodology).to be_valid
      end
    end
  end

  context 'verify scopes' do
    let(:meth1) { Meth::Methodology.new(name: 'M1', usable: true, locked: true) }
    let(:meth2) { Meth::Methodology.new(name: 'M2', usable: true, locked: false) }
    let(:meth3) { Meth::Methodology.new(name: 'M3', usable: false, locked: true) }
    let(:meth4) { Meth::Methodology.new(name: 'M4', usable: false, locked: false) }

    it 'list methodologies usable for new RCA' do
      meth1.save!
      meth2.save!
      meth3.save!
      meth4.save!

      expect(Meth::Methodology.rca_methodologies).to contain_exactly(meth1, meth2)
    end

    it 'list all not locked OR usable methodologies (usable for config, new & old)' do
      meth1.save!
      meth2.save!
      meth3.save!
      meth4.save!

      expect(Meth::Methodology.rca_methodologies_all).to contain_exactly(meth1, meth2, meth4)
    end

    it 'list methodologies usable for Old RCA (usable methodo + rca methodo)' do
      meth1.save!
      meth2.save!
      meth3.save!
      meth4.save!

      expect(Meth::Methodology.rca_methodologies_for_rca(meth1.id)).to contain_exactly(meth1, meth2)
      expect(Meth::Methodology.rca_methodologies_for_rca(meth3.id)).to contain_exactly(meth1, meth2, meth3)
    end
  end

  describe 'Creates new methodology' do
    it 'creates and save a new methodology' do
      methodology.create_methodology
      methodology.reload

      expect(Meth::Methodology.all.count).to eq 1
      expect(methodology.name).to eq 'Method 1'
      expect(methodology.steps.count).to eq 3

      # First Step (step 0)
      expect(methodology.steps.first).to eq methodology.step_info
      expect(methodology.steps.first.tools.count).to eq 1
      expect(methodology.steps.first.tools.first.tool_name).to eq 'CustomField'

      # Second Step : trs_tools
      expect(methodology.steps.second).to eq methodology.step_trstools
      expect(methodology.steps.second.tools.count).to eq 5
      expect(methodology.steps.second.tools.first.tool_name).to eq 'Criticity'

      # Step menu / Step 1 : closure
      expect(methodology.steps_menu.count).to eq 1
      expect(methodology.steps_menu.first.tools.first.tool_name).to eq 'CustomField'
    end
  end

  describe 'Destroy Methodology' do
    it 'destroys a methodology' do
      methodology.create_methodology
      methodology.reload

      # methodology.destroy_methodology
      methodology.destroy

      expect(Meth::Step.all).to be_empty
      expect(Meth::StepTool.all).to be_empty
    end
  end

  describe 'Locked?' do
    it 'returns locked status' do
      methodology.create_methodology
      methodology.reload

      expect(methodology.locked?).to be false
    end
  end

  describe 'Copy methodology' do
    it 'copies a methodology' do
      methodology.create_methodology
      methodology.reload

      methodology2 = methodology.copy

      expect(Meth::Methodology.all.count).to eq 2
      expect(methodology2.name).to eq 'Method 1_copy'
      expect(methodology2.steps.count).to eq 3

      # First Step (step 0)
      expect(methodology2.steps.first).to eq methodology2.step_info
      expect(methodology2.steps.first.tools.count).to eq 1
      expect(methodology2.steps.first.tools.first.tool_name).to eq 'CustomField'

      # Second Step : trs_tools
      expect(methodology2.steps.second).to eq methodology2.step_trstools
      expect(methodology2.steps.second.tools.count).to eq 5
      expect(methodology2.steps.second.tools.first.tool_name).to eq 'Criticity'

      # Step menu / Step 1 : closure
      expect(methodology2.steps_menu.count).to eq 1
      expect(methodology2.steps_menu.first.tools.first.tool_name).to eq 'CustomField'
    end
  end
end
