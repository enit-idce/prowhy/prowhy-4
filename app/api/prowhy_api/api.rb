require 'grape-swagger'

module ProwhyApi
  class Api < Grape::API
    format :json

    helpers Pundit

    rescue_from Pundit::NotAuthorizedError do |e|
      # puts 'ERROR==================================================>'.red
      # puts e
      error! e || :forbidden, 403
    end

    rescue_from ActiveRecord::RecordInvalid do |e|
      error! e.record.errors, 400
    end

    rescue_from ArgumentError do |e|
      # error! e.record.errors, 400
      error! e, 400
    end

    rescue_from ActiveRecord::RecordNotFound do
      error! 'record not found', 404
    end

    rescue_from StandardError do |e|
      error! e, 400
    end

    mount ProwhyApi::V1::Api

    # add_swagger_documentation
    add_swagger_documentation \
      token_owner: 'resource_owner',
      hide_documentation_path: true,
      hide_format: true,
      format: :json,
      security_definitions: {
        oauth2: {
          type: 'oauth2',
          flow: 'password',
          tokenUrl: "#{ENV['RAILS_RELATIVE_URL_ROOT']}/api/oauth/token",
          scopes: {}
        }
      },
      security: [
        {
          oauth2: []
        }
      ],
      info: {
        title: 'Prowhy-4 API.',
        description: 'description of Prowhy API here.',
        contact_name: 'Prowhy.',
        contact_email: 'prowhy@enit.fr',
        contact_url: 'http://prowhy.org',
        license: 'AGPL v3',
        license_url: 'agpl licence link.'
        # terms_of_service_url: "www.The-URL-of-the-terms-and-service.com",
      }
  end
end
