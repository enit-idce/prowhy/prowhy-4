require 'doorkeeper/grape/helpers'

module ProwhyApi
  module V1
    module ApiHelpers
      module UsersHelpers
        extend Grape::API::Helpers

        include Doorkeeper::Grape::Helpers
        include Devise::Controllers::Helpers

        def current_user
          # @current_user ||= resource_owner
          @current_user ||= if doorkeeper_token
                              User.find(doorkeeper_token.resource_owner_id)
                            else
                              # binding.pry
                              warden.authenticate(scope: :user)
                            end
        end

        # def custom_type(cf_type)
        #   return cf_type.field_internal == 'integer' ? Integer : String
        # end
      end
    end
  end
end
