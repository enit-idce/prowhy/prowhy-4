class CreateRcaAdvances < ActiveRecord::Migration[6.0]
  def change
    create_table :rca_advances do |t|
      t.references :rca, null: false, foreign_key: true
      t.references :meth_step, null: false, foreign_key: true
      t.integer :advance, default: 0

      t.timestamps
    end
  end
end
