require_dependency 'tools'

module Tools
  class CauseLink < ApplicationRecord
    belongs_to :cause # , class_name: 'Tools::Cause'
    belongs_to :cause_tool, polymorphic: true
  end
end
