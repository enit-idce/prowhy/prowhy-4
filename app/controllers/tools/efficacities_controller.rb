module Tools
  class EfficacitiesController < ApplicationController
    include ProcessConcern
    before_action :set_tools_efficacity

    def update
      skip_authorization

      respond_to do |format|
        if @tools_efficacity.update(tools_efficacity_params)
          # binding.pry
          format.json { render :show, status: :ok, location: @tools_efficacity }
          format.js do
            if params[:from_stars]
              render :update_percentage
            else
              render :show
            end
          end
        else
          format.json { render json: @tools_efficacity.errors, status: :unprocessable_entity }
        end
      end
    end

    def edit
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_efficacity
      @tools_efficacity = Tools::Efficacity.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def tools_efficacity_params
      params.require(:tools_efficacity).permit(:element_type, :element_id, :check_status, :percentage,
                                               :date_due_check, :date_done_check, :created_at, :updated_at)
    end
  end
end
