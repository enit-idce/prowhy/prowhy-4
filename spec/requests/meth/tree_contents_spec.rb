require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to test the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator. If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails. There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.

RSpec.describe '/meth/tree_contents', type: :request do
  # Meth::TreeContent. As you add validations to Meth::TreeContent, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) { FactoryBot.attributes_for(:meth_tree_content, custom_field_desc_id: custom_field_desc.id) }
  let(:invalid_attributes) { FactoryBot.attributes_for(:meth_tree_content, name: '', custom_field_desc_id: custom_field_desc.id) }

  let(:custom_field_desc) { FactoryBot.create(:meth_custom_field_desc, custom_field_type: custom_field_type) }
  let(:custom_field_type) { FactoryBot.create(:meth_custom_field_type, :type_tree) }

  let(:user) { FactoryBot.create(:user) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }

  before :each do
    user.set_session(:active_step_tool, methodology.steps.first.step_tools.first.id)
    user.save!
    sign_in user
  end

  describe 'GET /index' do
    it 'renders a successful response' do
      Meth::TreeContent.create! valid_attributes
      get meth_tree_contents_url
      expect(response).to be_successful
    end
  end

  # describe 'GET /show' do
  #   it 'renders a successful response' do
  #     meth_tree_content = Meth::TreeContent.create! valid_attributes
  #     get meth_tree_content_url(meth_tree_content)
  #     expect(response).to be_successful
  #   end
  # end

  describe 'GET /new' do
    context 'without params' do
      it 'renders a successful response' do
        get new_meth_tree_content_url, params: {}, xhr: true
        expect(response).to be_successful
      end
    end
    context 'with params' do
      it 'renders a successful response' do
        get new_meth_tree_content_url, params: {meth_tree_content: {custom_field_desc_id: custom_field_desc.id}}, xhr: true
        expect(@controller.instance_variable_get(:@meth_tree_content).custom_field_desc_id).to eq custom_field_desc.id
        expect(response).to be_successful
      end
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      meth_tree_content = Meth::TreeContent.create! valid_attributes
      get edit_meth_tree_content_url(meth_tree_content), params: {}, xhr: true
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Meth::TreeContent' do
        expect {
          post meth_tree_contents_url, params: { meth_tree_content: valid_attributes }, xhr: true
        }.to change(Meth::TreeContent, :count).by(1)
      end

      it 'redirects to the created meth_tree_content' do
        post meth_tree_contents_url, params: { meth_tree_content: valid_attributes }, xhr: true
        expect(response).to redirect_to(meth_tree_contents_url)
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Meth::TreeContent' do
        expect {
          post meth_tree_contents_url, params: { meth_tree_content: invalid_attributes }, xhr: true
        }.to change(Meth::TreeContent, :count).by(0)
      end

      it 'renders a successful response (i.e. to display the new template)' do
        post meth_tree_contents_url, params: { meth_tree_content: invalid_attributes }, xhr: true
        expect(response).to be_successful
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      let(:new_attributes) { {name: 'tree content new name'} }

      it 'updates the requested meth_tree_content' do
        meth_tree_content = Meth::TreeContent.create! valid_attributes
        patch meth_tree_content_url(meth_tree_content), params: { meth_tree_content: new_attributes }, xhr: true
        meth_tree_content.reload

        expect(meth_tree_content.name).to eq 'tree content new name'
      end

      it 'redirects to the meth_tree_content' do
        meth_tree_content = Meth::TreeContent.create! valid_attributes
        patch meth_tree_content_url(meth_tree_content), params: { meth_tree_content: new_attributes }, xhr: true
        meth_tree_content.reload
        expect(response).to redirect_to(meth_tree_contents_url)
      end
    end

    context 'with invalid parameters' do
      it 'renders a successful response (i.e. to display the edit template)' do
        meth_tree_content = Meth::TreeContent.create! valid_attributes
        patch meth_tree_content_url(meth_tree_content), params: { meth_tree_content: invalid_attributes }, xhr: true
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested meth_tree_content' do
      meth_tree_content = Meth::TreeContent.create! valid_attributes
      expect {
        delete meth_tree_content_url(meth_tree_content)
      }.to change(Meth::TreeContent, :count).by(-1)
    end

    it 'redirects to the meth_tree_contents list' do
      meth_tree_content = Meth::TreeContent.create! valid_attributes
      delete meth_tree_content_url(meth_tree_content)
      expect(response).to redirect_to(meth_tree_contents_url)
    end
  end
end
