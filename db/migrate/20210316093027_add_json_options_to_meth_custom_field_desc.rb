class AddJsonOptionsToMethCustomFieldDesc < ActiveRecord::Migration[6.0]
  def change
    add_column :meth_custom_field_descs, :options, :json, after: :show_search
  end
end
