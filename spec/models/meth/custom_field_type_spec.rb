require 'rails_helper'

RSpec.describe Meth::CustomFieldType, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
  subject(:custom_field_type) do
    Meth::CustomFieldType.new(field_type: 'string', field_size: nil, field_internal: 'text', field_name: 'Texte')
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has many custom_field_descs' do
        assc = Meth::CustomFieldType.reflect_on_association(:custom_field_descs)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without field_internal' do
        custom_field_type.field_internal = ''
        expect(custom_field_type).to_not be_valid
      end

      it 'is not valid without field_name' do
        custom_field_type.field_name = ''
        expect(custom_field_type).to_not be_valid
      end

      it 'is valid with field_internal & field_name' do
        expect(custom_field_type).to be_valid
      end

      it 'is not valid 2 times same field_internal' do
        custom_field_type.save!
        cf2 = Meth::CustomFieldType.new(field_type: 'string', field_size: nil, field_internal: 'text', field_name: 'Texte2')

        expect(cf2).to_not be_valid
      end

      # it 'is not valid 2 times same field name' do
      #   custom_field_type.save!
      #   cf2 = Meth::CustomFieldType.new(field_type: 'string', field_size: nil, field_internal: 'text2', field_name: 'Texte')

      #   expect(cf2).to_not be_valid
      # end

      it 'is valid with differents name and internal name' do
        custom_field_type.save!
        cf2 = Meth::CustomFieldType.new(field_type: 'string', field_size: nil, field_internal: 'text2', field_name: 'Texte2')

        expect(cf2).to be_valid
      end
    end

    describe 'Show field name or no name' do
      it 'shows name' do
        expect(custom_field_type.name).to eq 'Texte'

        custom_field_type.field_name = nil
        expect(custom_field_type.name).to eq 'no name'
      end
    end
  end
end
