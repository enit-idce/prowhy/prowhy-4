class CreateMethCustomFieldTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :meth_custom_field_types do |t|
      t.string :field_type
      t.string :field_name

      t.timestamps
    end
  end
end
