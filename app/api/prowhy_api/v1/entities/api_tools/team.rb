module ProwhyApi
  module V1
    module Entities
      module ApiTools
        class Team < Grape::Entity
          expose :profil_id

          expose :profil_name

          # expose :person_id
          expose :person, merge: true do |team, _options|
            Entities::Person.represent(team.person, { with_id: 'true', with_user: 'true' })
          end

          private

          def profil_name
            return object.profil.name
          end
        end
      end
    end
  end
end
