require_dependency 'admin'

module Admin
  class Perimeter < ApplicationRecord
    include AncestryConcern

    has_many :perimeter_users, dependent: :destroy
    has_many :users, through: :perimeter_users

    has_many :perimeter_rcas, dependent: :destroy
    has_many :rcas, through: :perimeter_rcas

    validates :name, presence: true
  end
end
