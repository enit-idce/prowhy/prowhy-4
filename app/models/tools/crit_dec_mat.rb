require_dependency 'tools'

module Tools
  class CritDecMat < ApplicationRecord
    belongs_to :labels_criterium, class_name: 'Labels::Criterium'
    belongs_to :decision_mat # , class_name: 'Tools::DecisionMat'

    has_many :dec_mat_vals, dependent: :destroy
  end
end
