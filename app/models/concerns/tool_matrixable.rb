module ToolMatrixable
  extend ActiveSupport::Concern

  included do
    has_many :dec_mat_links, as: :entry, dependent: :destroy
    has_many :decision_mats, through: :dec_mat_links, source: :decision_mat
    # enum matrix_status: { in_progress: 0, validated: 1, invalidated: 2 }, _prefix: true

    has_one :matrix_stat, class_name: 'Tools::MatrixStat', as: :element, dependent: :destroy
    accepts_nested_attributes_for :matrix_stat
  end

  # Class Methods
  module ClassMethods
    def list_matrix_attributes
      return [:id, :matrix_status, :element_type, :element_id]
    end
  end

  # Instance Methods
  def matrixable?
    true
  end

  def matrix_status
    matrix_stat&.matrix_status
  end

  def matrix_status_in_progress?
    matrix_stat&.matrix_status_in_progress?
  end

  def matrix_status_validated?
    matrix_stat&.matrix_status_validated?
  end

  def matrix_status_invalidated?
    matrix_stat&.matrix_status_invalidated?
  end

  def matrix_status_in_progress!
    matrix_stat&.matrix_status_in_progress!
  end

  def matrix_status_validated!
    matrix_stat&.matrix_status_validated!
  end

  def matrix_status_invalidated!
    matrix_stat&.matrix_status_invalidated!
  end

  def matrix_status_editable?
    return dec_mat_links.empty?
  end

  def matrix_status_calculate
    return if dec_mat_links.empty?

    matrix_status_invalidated!

    dec_mat_links.each do |dec_mat_link|
      if dec_mat_link.matrix_status_validated?
        matrix_status_validated!
        break
      elsif dec_mat_link.matrix_status_in_progress?
        matrix_status_in_progress! if matrix_status_invalidated?
      end
    end
    save!
  end
end
