require_dependency 'labels'

module Labels
  class Criterium < ApplicationRecord
    translates :name, fallback: :any
    translates :description, fallback: :any

    enum crit_type: { matrix: 0, criticity: 1 }, _prefix: true

    validates :name, presence: true

    has_many :tools_crit_dec_mats, class_name: 'Tools::CritDecMat', foreign_key: :labels_criterium_id, dependent: :destroy
    has_many :tools_decision_mats, class_name: 'Tools::DecisionMat', through: :tools_crit_dec_mats

    # labels_values actuellement used by criticity not decision matrix
    has_many :values, -> { order(value: :asc) }, dependent: :destroy
    belongs_to :config, optional: true

    validates :internal_name, format: {with: /^CR[a-zA-Z0-9_.]*$/, allow_blank: true, multiline: true, message: "Format : 'CRxxx'"},
                              length: {minimum: 3, maximum: 7, allow_blank: true},
                              uniqueness: {case_sensitive: true, allow_blank: true}

    # TODO: ajouter un champ type_crit => DecisionMat/Criticity/etc et le scope corespondant
    # scope :criteria_by_type, ->(crit_type) { where(crit_type: crit_type) }
    scope :type_matrix, -> { where(crit_type: 0) }
    scope :type_criticity, -> { where(crit_type: 1) }
    scope :type_criticity_dispo, ->(config_id) { where(crit_type: 1, config_id: [nil, config_id]) }

    def criticity_name
      return name + (internal_name ? " (#{internal_name})" : '')
    end
  end
end
