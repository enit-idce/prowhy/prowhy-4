require_dependency 'admin'

module Admin
  class PerimeterUser < ApplicationRecord
    belongs_to :perimeter
    belongs_to :user
  end
end
