require 'rails_helper'

RSpec.describe Tools::Team, type: :model do
  subject(:team) do
    Tools::Team.create!(rca: rca, tool_reference: 1, tool_num: 1, profil: profil, person: person)
  end

  let(:person) { FactoryBot.create(:person) }
  let(:methodology) { FactoryBot.create(:methodology, name: 'Method 1') }
  let(:rca) { FactoryBot.create(:rca, title: 'Rca 1', methodology: methodology) }
  let(:profil) { FactoryBot.create(:profil) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      # it 'belongs to labels_cause_type' do
      #   assc = Tools::Cause.reflect_on_association(:labels_cause_type)
      #   expect(assc.macro).to eq :belongs_to
      # end
      it 'belongs to person' do
        assc = Tools::Team.reflect_on_association(:person)
        expect(assc.macro).to eq :belongs_to
      end
      it 'belongs to profil' do
        assc = Tools::Team.reflect_on_association(:profil)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end

  describe 'Create and search model : tool concern functions' do
    before :each do
      team.save!
    end

    it 'create_default' do
      expect(Tools::Team.create_default!({}, nil)).to eq []
    end

    it 'search_default' do
      step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1)
      teams = Tools::Team.search_default(rca, 'tools_teams', step_tool)

      expect(teams.length).to eq 1
      expect(teams.map(&:id)).to eq [team.id]
    end

    it 'is a multiple type class' do
      expect(Tools::Team.create_search_multiple?).to eq true
    end
  end
end
