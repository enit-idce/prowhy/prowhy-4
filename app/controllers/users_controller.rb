class UsersController < ApplicationController
  include AdminConcern
  before_action :set_user, only: [:show, :edit, :update, :destroy, :send_test_mail]

  # GET /users_admin_index
  # GET /users_admin.json
  def index
    @users = User.all
    authorize User
  end

  # # GET /users_admin/1
  # # GET /users_admin/1.json
  # def show
  # end

  # GET /users_admin/new
  def new
    @user = User.new
    authorize @user

    if params[:person_id]
      @user.person = Person.find(params[:person_id])
      @user.email = @user.person.email
    else
      @user.build_person
    end

    if params[:ldap_user]
      render :new_from_ldap
    else
      render :new
    end
    # binding.pry
  end

  def ldap_search
    @user = User.new(user_params)
    authorize @user

    @person = params[:user][:person_attributes][:id] ? Person.find(params[:user][:person_attributes][:id]) : nil
    @ldap_config = Admin::LdapConfig.find(params[:ldap_config])

    ldap_params = {}
    [:username, :email].each do |k|
      ldap_params[k] = params[:user][k]
    end
    params[:user][:person_attributes].each_key do |k|
      ldap_params[k] = params[:user][:person_attributes][k]
    end

    begin
      @ldap_users = @ldap_config.connect_and_search_by_att(ldap_params)
    rescue StandardError => e
      @message = e.message
    end
    # binding.pry
  end

  # POST
  def ldap_search_valid
    ldap_config = Admin::LdapConfig.find(params[:ldap_config])
    ldap_user = ldap_config.connect_and_search_by_uid(params[:ldap_users])

    user = User.new(username: ldap_config.att(ldap_user, 'uid'),
                    email: ldap_config.att(ldap_user, 'email'),
                    password: '********',
                    password_confirmation: '********',
                    ldap_config: ldap_config)

    if params[:person_id] && !params[:person_id].empty?
      user.person = Person.find(params[:person_id])
      user.person.firstname = ldap_config.att(ldap_user, 'firstname')
      user.person.lastname = ldap_config.att(ldap_user, 'lastname')
    else
      user.build_person({firstname: ldap_config.att(ldap_user, 'firstname'),
                         lastname: ldap_config.att(ldap_user, 'lastname')})
    end
    @user = user
    authorize @user, :create?

    respond_to do |format|
      if @user.save
        format.html { redirect_to users_admin_index_url, notice: t('.message_succes', user: User.model_name.human) }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /users_admin/1/edit
  def edit
  end

  # POST /users_admin
  # POST /users_admin.json
  def create
    @user = User.new(user_params)
    authorize @user

    respond_to do |format|
      if @user.save
        format.html { redirect_to users_admin_index_url, notice: t('.message_succes', user: User.model_name.human) }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users_admin/1
  # PATCH/PUT /users_admin/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to users_admin_index_url, notice: t('.message_succes', user: User.model_name.human) }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users_admin/1
  # PATCH/PUT /users_admin/1.json
  def update_pa_options
    # return unless current_user
    authorize User

    current_user&.update_pa_options(params)
    head :ok
  end

  # DELETE /users_admin/1
  # DELETE /users_admin/1.json
  def destroy
    # @user.roles.each do |role|
    #   authorize role, :role_is_deletable?
    # end
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_admin_index_url, notice: t('.message_succes', user: User.model_name.human) }
      format.json { head :no_content }
    end
  end

  # def user_page
  #   authorize current_user
  # end

  # def user_page_task_plan
  #   step_tool =
  # end

  def send_test_mail
    authorize User

    ProwhyMailer.with(user: @user).welcome_email.deliver_now

    redirect_to users_admin_index_url
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
    authorize @user
  end

  # Only allow a list of trusted parameters through.
  def user_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation, :person_id,
                                 person_attributes: [:id, :firstname, :lastname, :adress, :phone, :moreinfo])
  end
end
