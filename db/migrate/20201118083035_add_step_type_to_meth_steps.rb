class AddStepTypeToMethSteps < ActiveRecord::Migration[6.0]
  def change
    add_column :meth_steps, :step_type, :integer, default: 1, after: :methodology_id

    Meth::Step.all.each do |elem|
      elem.step_type = 2 if elem.name_fr == 'step0_infos' || elem.name_en == 'step0_infos'
      elem.step_type = 3 if elem.name_fr == 'step0_trstools' || elem.name_en == 'step0_trstools'
      elem.save!
    end
  end
end
