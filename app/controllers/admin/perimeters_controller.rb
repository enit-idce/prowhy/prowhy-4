module Admin
  class PerimetersController < ApplicationController
    include AdminConcern
    before_action :set_perimeter, only: [:show, :edit, :update, :destroy]

    # GET /admin/perimeters
    # GET /admin/perimeters.json
    def index
      authorize Perimeter
      # @perimeters = Admin::Perimeter.all
    end

    # Création des données JSon pour jstree
    def js_tree_data
      authorize Perimeter # , :perimeters_js_tree_data?
      perimeters = Perimeter.where(ancestry: nil)

      data = []
      set_attrib_user
      perimeter_ids = []
      if params[:attrib_class] && params[:attrib_id]
        # @attrib_user&.perimeters&.map(&:id)
        perimeter_ids = params[:attrib_class].constantize.find(params[:attrib_id])&.perimeters&.map(&:id)
      end
      # binding.pry

      perimeters.each do |perimeter|
        data << PerimeterPresenter.new(perimeter, view_context).subtree_datas(params[:show_buttons] || 'true', perimeter_ids)
      end

      # binding.pry

      render json: data
    end

    # GET /admin/perimeters/1
    # GET /admin/perimeters/1.json
    # def show
    # end

    # GET /admin/perimeters/new
    def new
      @perimeter = Admin::Perimeter.new(perimeter_params)
      authorize @perimeter

      render :new
    rescue StandardError
      @perimeter = Admin::Perimeter.new
      authorize @perimeter

      respond_to do |format|
        format.js { render js: "alert('no params: cannot create new tree content');" }
      end
    end

    # GET /admin/perimeters/1/edit
    def edit
      respond_to do |format|
        format.html
        format.js { render :new }
      end
    end

    # POST /admin/perimeters
    # POST /admin/perimeters.json
    def create
      @perimeter = Admin::Perimeter.new(perimeter_params)
      authorize @perimeter

      respond_to do |format|
        if @perimeter.save
          @perimeter&.parent&.users&.each do |user|
            user.add_perimeter(@perimeter.id)
          end

          format.html { redirect_to admin_perimeters_url, notice: "#{Admin::Perimeter.model_name.human} was successfully created." }
          format.json { render :show, status: :created, location: @perimeter }
        else
          format.html { render :new }
          format.json { render json: @perimeter.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /admin/perimeters/1
    # PATCH/PUT /admin/perimeters/1.json
    def update
      respond_to do |format|
        old_parent = @perimeter.parent
        if @perimeter.update(perimeter_params)
          # Si changement de parent : mise à jour des droits des users
          if @perimeter.parent && old_parent != @perimeter.parent
            @perimeter.reload&.parent&.users&.each do |user|
              user.add_perimeter_and_childs(@perimeter)
            end
          end

          format.html { redirect_to admin_perimeters_url, notice: "#{Admin::Perimeter.model_name.human} was successfully updated." }
          format.json { render :show, status: :ok, location: @perimeter }
        else
          format.html { render :edit }
          format.json { render json: @perimeter.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /admin/perimeters/1
    # DELETE /admin/perimeters/1.json
    def destroy
      @perimeter.destroy
      respond_to do |format|
        format.html { redirect_to admin_perimeters_url, notice: "#{Admin::Perimeter.model_name.human} was successfully destroyed." }
        format.json { head :no_content }
      end
    end

    # Users associations
    # GET
    def assign_users
      authorize Perimeter

      @users = User.all.order(:username)
      set_attrib_user

      respond_to do |format|
        format.html { render :assign_users }
      end
    end

    # PATCH
    def assign_users_valid
      authorize Perimeter, :assign_users?

      associate_perimeters_to_users

      redirect_to admin_perimeters_assign_users_path(attrib_user: params[:attrib_user]), notice: "#{Admin::Perimeter.model_name.human.pluralize} assigned to user."
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_perimeter
      @perimeter = Admin::Perimeter.find(params[:id])
      authorize @perimeter
    end

    # Only allow a list of trusted parameters through.
    def perimeter_params
      params.require(:admin_perimeter).permit(:name, :ancestry, :parent_id)
    rescue StandardError
      params.require(:perimeter).permit(:name, :ancestry, :parent_id)
    end

    # def associate_users_to_perimeter
    #   set_attrib_perimeter
    #   users_in = params&.[]('users_ids')&.map(&:to_i)

    #   # Ajout des utilisateurs sélectionnés :
    #   users_in&.each do |user_id|
    #     authorize @attrib_perimeter, :add_perimeter?
    #     User.find(user_id)&.add_perimeter(@attrib_perimeter.name)
    #   end

    #   # Suppression des utilisateurs non sélectionnés :
    #   # TODO : ne pas supprimer le perimeter si seul super_admin !!! => géré dans Pundit perimeterPolicy
    #   users_with_perimeter = User.with_perimeter(@attrib_perimeter.name)
    #   users_with_perimeter&.each do |user|
    #     next if users_in&.include?(user.id)

    #     # puts ("Delete #{@attrib_perimeter.name} for user #{user.name} : #{users_in&.include?(user.id)}").yellow
    #     authorize @attrib_perimeter, :remove_perimeter?
    #     user.remove_perimeter(@attrib_perimeter.name)
    #   end
    # end

    def associate_perimeters_to_users
      set_attrib_user
      perimeters_in = params['perimeter_ids']&.map(&:to_i) # params&.[]('perimeter_ids')&.map(&:to_i)
      user_perimeters = @attrib_user.perimeters.map(&:id)

      # Ajout des perimeters sélectionnés :
      perimeters_in&.each do |perimeter_id|
        next if user_perimeters.include?(perimeter_id)

        @attrib_user.add_perimeter(perimeter_id)
        # perimeter = Perimeter.find(perimeter_id)
        # puts "ADD Perimeter #{perimeter.name}".green
      end

      # Suppression des périmètres non sélectionnés :
      user_perimeters&.each do |perimeter_id|
        next if perimeters_in&.include?(perimeter_id)

        @attrib_user.remove_perimeter(perimeter_id)
        # perimeter = Perimeter.find(perimeter_id)
        # puts "DEL Perimeter #{perimeter.name}".red
      end
    end

    # def set_attrib_perimeter
    #   @attrib_perimeter = Admin::Perimeter.find(params[:attrib_perimeter])
    # rescue ActiveRecord::RecordNotFound # StandardError
    #   @attrib_perimeter = Admin::Perimeter.first
    # rescue StandardError
    #   @attrib_perimeter = Admin::Perimeter.create!(name: 'ALL')
    # end

    def set_attrib_user
      @attrib_user = User.find(params[:attrib_user])
    rescue StandardError
      @attrib_user = User.first
    end

  end
end
