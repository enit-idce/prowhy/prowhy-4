require 'rails_helper'

RSpec.describe Tools::Image, type: :model do
  subject(:image) do
    FactoryBot.create(:tools_image, rca_id: rca.id, file: image_ok)
    # binding.pry
    # Tools::Image.create!(rca: rca, tool_reference: 1, name: 'Image 1', file: image_ok)
  end

  let(:rca) { Rca.create!(id: 1, title: 'Rca 1', methodology_id: methodology.id) }
  let(:methodology) { Meth::Methodology.create!(id: 1, name: 'Method 1') }

  let(:image_nok1) do
    ActiveStorage::Blob.create_and_upload!(
      io: File.open(file_nok1, 'rb'),
      filename: 'image_too_big.png',
      content_type: 'image/png'
    ).signed_id
  end

  let(:image_nok2) do
    ActiveStorage::Blob.create_and_upload!(
      io: File.open(file_nok2, 'rb'),
      filename: 'not_image.pdf',
      content_type: 'file/pdf'
    ).signed_id
  end

  let(:image_ok) do
    ActiveStorage::Blob.create_and_upload!(
      io: File.open(file_ok, 'rb'),
      filename: 'image_ok.png',
      content_type: 'image/png'
    ).signed_id
  end

  let(:file_ok) { Rails.root.join('spec', 'support', 'assets', 'images', 'image_ok.png') }
  let(:file_nok1) { Rails.root.join('spec', 'support', 'assets', 'images', 'image_too_big.png') }
  let(:file_nok2) { Rails.root.join('spec', 'support', 'assets', 'images', 'not_image.pdf') }

  context 'describes model associations and validations' do
    before :each do
      config = Admin::AdminConfig.create!
      config.options['image_max_size'] = 0.5
      config.save!
    end

    describe 'Validations' do
      it 'is not valid without name' do
        image.name = ''
        expect(image).to_not be_valid
      end

      it 'is valid with name and valid image' do
        image.name = 'The image'
        expect(image).to be_valid
      end

      it 'is valid with name and valid image' do
        # config = Admin::AdminConfig.create!
        # config.options['image_max_size'] = 0.5
        # config.save!
        image.file = image_ok
        expect(image).to be_valid
      end

      it 'is not valid with a too big image' do
        image.file = image_nok1
        expect(image).to_not be_valid
      end

      it 'is not valid with a no image file' do
        image.file = image_nok2
        expect(image).to_not be_valid
      end
    end
  end
end
