class AddAdvanceToStepRoles < ActiveRecord::Migration[6.0]
  def change
    add_column :meth_step_roles, :advance_level, :integer, default: 0
  end
end
