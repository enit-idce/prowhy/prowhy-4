require 'rails_helper'

RSpec.describe '/api/v1/images', type: :request do
  # let(:email) { user.email }
  # let(:password) { user.password }
  let(:user) { FactoryBot.create(:user) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:rca) { FactoryBot.create(:rca, methodology: methodology) }

  context 'with user logged' do
    before :each do
      sign_in user

      @params_image = {name: 'IMG1', rca_id: rca.id, file: fixture_file_upload('images/img1.png', 'image/png')}
    end

    it 'creates an image linked to rca1' do
      @tools_image = Tools::Image.new(@params_image)

      expect {
        post '/api/v1/images', params: @params_image # , as: :json
      }.to change(Tools::Image, :count).by(1)
    end

    it 'returns 201 image created' do
      post '/api/v1/images', params: @params_image # , as: :json

      # json = JSON.parse(response.body)
      # binding.pry
      # puts json['error_description']&.red

      expect(response.status).to eq 201
    end
  end
end
