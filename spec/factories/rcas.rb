FactoryBot.define do
  factory :rca, class: Rca do
    # title { 'Rca 1' }
    sequence(:title) { |n| "Rca #{n}" }
    association :methodology, factory: :methodology

    # after(:create) do |rca|
    #   perimeter = Admin::Perimeter.first || FactoryBot.create(:perimeter)
    #   rca.add_perimeter perimeter.id
    #   rca.reload
    # end
  end
end
