require_dependency 'tools'

module Tools
  class RcaList < ApplicationRecord
    include ToolConcern
    # belongs_to :rca
    has_many :rcas, class_name: 'Rca', foreign_key: :tools_rca_list_id, dependent: :nullify

    # Create and search default
    def self.create_default!(options, _step_tool)
      create!(options.except!(:tool_num))
    end

    def self.search_default(rca, tool_name, step_tool)
      return nil unless rca.send(tool_name) && step_tool

      return rca.send(tool_name).where(tool_reference: step_tool.tool_reference).first
    end

  end
end
