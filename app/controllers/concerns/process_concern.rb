module ProcessConcern
  extend ActiveSupport::Concern
  include PolicyToolsConcern

  included do
    layout 'processus'

    after_action :call_track_action, only: [:create, :destroy, :update]
  end
end
