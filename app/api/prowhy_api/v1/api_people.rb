require 'doorkeeper/grape/helpers'
require 'devise'

module ProwhyApi
  module V1
    class ApiPeople < Grape::API
      resource :people do
        desc 'Return all People.'
        params do
          optional :role, type: String, desc: 'Option : Get people with associated user with role :role.'
          optional :user, type: Boolean, desc: 'True if you need User information.'
        end
        get do
          @people = nil

          if params[:role]
            # binding.pry
            @people = User.with_role(params[:role]).map(&:person)
          else
            @people = Person.all
          end

          present @people, with: Entities::Person,
                           with_id: true,
                           with_user: (params[:user] == true || params[:user] == 'true' ? true : false)
        end

        desc 'Return Person :id'
        params do
          requires :id, type: Integer, desc: 'Person ID.'
          optional :user, type: Boolean, desc: 'True if you need User information.'
        end
        route_param :id do
          get do
            @person = Person.find(params[:id])
            authorize @person, :show?

            present @person, with: Entities::Person,
                             with_id: false,
                             with_user: (params[:user] == true || params[:user] == 'true' ? true : false)
          end
        end
      end
    end
  end
end
