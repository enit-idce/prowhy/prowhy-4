module Meth
  class TreeContentsController < ApplicationController
    include AdminConcern
    before_action :set_meth_tree_content, only: [:show, :edit, :update, :destroy]

    # GET /meth/tree_contents
    # GET /meth/tree_contents.json
    def index
      authorize TreeContent
      # @meth_tree_contents = Meth::TreeContent.all
    end

    # Création des données JSon pour jstree
    def js_tree_data
      authorize TreeContent, :index?
      tree_contents = TreeContent.where(ancestry: nil)

      data = []

      tree_contents.each do |tree_content|
        data << TreeContentPresenter.new(tree_content, view_context).subtree_datas
      end

      render json: data
    end

    # GET /meth/tree_contents/1
    # GET /meth/tree_contents/1.json
    # def show
    # end

    # GET /meth/tree_contents/new
    def new
      if params[:meth_tree_content]
        @meth_tree_content = Meth::TreeContent.new(meth_tree_content_params)
        authorize @meth_tree_content

        render :new
      else
        @meth_tree_content = Meth::TreeContent.new
        authorize @meth_tree_content

        respond_to do |format|
          format.js { render js: "alert('no params: cannot create new tree content');" }
        end
      end
    end

    # GET /meth/tree_contents/1/edit
    def edit
      respond_to do |format|
        format.html
        format.js { render :new }
      end
    end

    # POST /meth/tree_contents
    # POST /meth/tree_contents.json
    def create
      @meth_tree_content = Meth::TreeContent.new(meth_tree_content_params)
      authorize @meth_tree_content

      respond_to do |format|
        if @meth_tree_content.save
          format.html { redirect_to meth_tree_contents_url, notice: 'Tree content was successfully created.' }
          format.json { render :show, status: :created, location: @meth_tree_content }
        else
          format.html { render :new }
          format.js { render :new }
          format.json { render json: @meth_tree_content.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /meth/tree_contents/1
    # PATCH/PUT /meth/tree_contents/1.json
    def update
      respond_to do |format|
        if @meth_tree_content.update(meth_tree_content_params)
          format.html { redirect_to meth_tree_contents_url, notice: 'Tree content was successfully updated.' }
          format.json { render :show, status: :ok, location: @meth_tree_content }
        else
          format.html { render :edit }
          format.js { render :new }
          format.json { render json: @meth_tree_content.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /meth/tree_contents/1
    # DELETE /meth/tree_contents/1.json
    def destroy
      @meth_tree_content.destroy
      respond_to do |format|
        format.html { redirect_to meth_tree_contents_url, notice: 'Tree content was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_meth_tree_content
      @meth_tree_content = Meth::TreeContent.find(params[:id])
      authorize @meth_tree_content
    end

    # Only allow a list of trusted parameters through.
    def meth_tree_content_params
      params.require(:meth_tree_content).permit(:name, :name_fr, :name_en, :custom_field_desc_id, :ancestry, :parent_id)
    end
  end
end
