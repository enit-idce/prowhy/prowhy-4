require_dependency 'tools'

module Tools
  class Task < ApplicationRecord
    include ToolConcern
    include AncestryConcern

    include ToolMatrixable
    include ToolEfficacitable

    belongs_to :tools_cause, class_name: 'Tools::Cause' # , optional: true => à voir si on remet il faut changer dans mysql
    belongs_to :responsible_action, class_name: 'Person', optional: true
    belongs_to :responsible_rca, class_name: 'Person', optional: true

    belongs_to :task_detail, polymorphic: true, optional: true, dependent: :destroy

    belongs_to :check_list_elem, class_name: 'Labels::CheckListElem', optional: true

    has_many :tools_reschedules, class_name: 'Tools::Reschedule', foreign_key: 'tools_task_id', dependent: :destroy

    validates :name, presence: true
    validates :task_detail_type, presence: true

    enum priority: { low: 0, medium: 1, high: 2 }, _prefix: true

    accepts_nested_attributes_for :task_detail

    # before_save :update_old_status_attributes
    after_save :update_new_status_attributes, :send_mail_task_closed

    # Scopes for User Task Plans
    scope :my_tasks, ->(user) { where(responsible_rca_id: user.person.id).or(where(responsible_action_id: user.person.id)) }
    scope :my_tasks_as_resp_action, ->(user) { where(responsible_action_id: user.person.id) }
    scope :my_tasks_as_resp_rca, ->(user) { where(responsible_rca_id: user.person.id) }

    scope :from_valid_rca, -> { includes(:rca).where(rcas: {status: 1}) }

    TASK_TYPES = ['Tools::TaskValidation',
                  'Tools::TaskEmergency',
                  'Tools::TaskContainment',
                  'Tools::TaskCorrective',
                  'Tools::TaskPreventive',
                  'Tools::TaskImprovement'].freeze

    TASK_REF = {'Tools::TaskValidation': 'Va',
                'Tools::TaskEmergency': 'Em',
                'Tools::TaskContainment': 'Cn',
                'Tools::TaskCorrective': 'Cr',
                'Tools::TaskPreventive': 'Pr',
                'Tools::TaskImprovement': 'Im'}.freeze

    def self.all_tasks_columns
      return { object: [:ref, :advancement, :responsible_action, :responsible_rca,
                        :result, :priority, :date_due, :date_completion],
               matrix: [:matrix_status],
               validation: [:status],
               efficacity: [:check_status, :percentage, :date_due_check, :date_done_check] }
    end

    def build_task_detail(params)
      self.task_detail = task_detail_type&.safe_constantize&.new(params) if task_detail_type&.safe_constantize
    end

    def self.create_default!(_options, _step_tool)
      # create!(options)
      []
    end

    def self.search_default(rca, _tool_name, step_tool)
      step_tool.tool_options ||= {}
      step_tool.tool_options[:task_options] ||= {}
      # binding.pry
      if step_tool.tool_reference == 0
        options_for_search = step_tool.tool_options[:task_options] # .merge(ancestry: nil) # Ajouté pour report
      else
        options_for_search = step_tool.tool_options[:task_options].merge(tool_reference: step_tool.tool_reference,
                                                                         tool_num: step_tool.tool_num)
                                                                         # ,
                                                                         # ancestry: nil) # Ajouté pour report
      end
      return rca.tools_tasks.where(options_for_search)
    end

    def self.create_search_multiple?
      return true
    end

    def responsibles
      ret = []
      ret << responsible_rca if responsible_rca
      ret << responsible_action if responsible_action && responsible_action != responsible_rca
      return ret
    end

    def calculate_advancement
      return unless children

      self.advancement = 4
      children.each do |ch|
        # puts ch.name.yellow
        # puts ch.advancement

        self.advancement = ch.advancement if ch.advancement < advancement
        # puts advancement
      end

      save!
      update_date_completion
    end

    def propagate_advancement
      return unless parent

      parent.calculate_advancement
      parent.propagate_advancement
    end

    def task_finished?
      return advancement == 4
    end

    def task_started?
      return advancement > 0
    end

    def task_late?
      return date_due && date_due <= Date.today && !task_finished?
    end

    def update_date_completion
      if task_finished? & !date_completion
        self.date_completion = Date.today
        save!
      elsif !task_finished? && date_completion
        self.date_completion = nil
        save!
      end
    end

    # Update cause status for TaskValidation => old cause status
    def update_old_cause(old_cause)
      old_cause.update_cause_status_from_tasks if task_detail_type == 'Tools::TaskValidation'
      # if tools_cause_id_changed?
      #   puts 'Cause ID CHANGED==========================>'.green
      #   old_cause = Tools::Cause.find(tools_cause_id_was)
      #   old_cause.update_cause_status_from_tasks
      # end
    end

    def reschedule_date_due(comment = nil)
      reschedule = Tools::Reschedule.new(date: date_due, comment: comment, tools_task: self)
      reschedule.save!
    end

    def reference
      # puts "=========================================".yellow
      # puts Task::TASK_REF
      # puts task_detail_type
      # puts Task::TASK_REF[task_detail_type.intern]
      return "#{Task::TASK_REF[task_detail_type.intern]}##{id}"
    end

    private

    # Update cause status for TaskValidation (after_save)
    def update_new_status_attributes
      tools_cause.update_cause_status_from_tasks if task_detail_type == 'Tools::TaskValidation'
    end

    # After save : If task is closed => send email to RCA responsible.
    def send_mail_task_closed
      return unless saved_change_to_advancement? && task_finished?

      admin_config = Admin::AdminConfig.first
      return unless admin_config&.task_mails&.[](:send_action_closed) == 'true'

      SendMails.task_closed_send_mail(id)
    end
  end
end
