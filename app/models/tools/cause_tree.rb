require_dependency 'tools'

module Tools
  class CauseTree < ApplicationRecord
    include ToolConcern
    # belongs_to :rca

    has_one :cause_link, as: :cause_tool, dependent: :destroy
    has_one :cause, through: :cause_link

    # options = { rca: rca, tool_reference: tool_reference }
    def self.create_default!(options, step_tool)
      # Cause Tree creation
      cause_tree = create!(options)

      # Assign or Create Head cause to Ishikawa
      la_cause = options[:rca].find_or_create_tool('tools_causes', 'Tools::Cause', step_tool)
      cause_tree.cause_link = Tools::CauseLink.create!(cause: la_cause, cause_tool: cause_tree)
      # binding.pry

      cause_tree
    end

    # def self.search_default(rca, tool_name, step_tool)
    #   rca.send(tool_name).where(tool_reference: step_tool.tool_reference, tool_num: step_tool.tool_num, tree_head: true).first
    # end

  end
end
