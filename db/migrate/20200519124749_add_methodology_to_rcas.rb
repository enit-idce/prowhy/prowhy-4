class AddMethodologyToRcas < ActiveRecord::Migration[6.0]
  def change
    add_reference :rcas, :methodology, null: false, foreign_key: { to_table: :meth_methodologies }
  end
end
