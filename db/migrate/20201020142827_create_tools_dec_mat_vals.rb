class CreateToolsDecMatVals < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_dec_mat_vals do |t|
      t.references :crit_dec_mat, null: false, foreign_key: { to_table: :tools_crit_dec_mats }
      t.references :dec_mat_link, null: false, foreign_key: { to_table: :tools_dec_mat_links }
      t.integer :value

      t.timestamps
    end
  end
end
