require 'rails_helper'

RSpec.describe Meth::StepTool, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
  subject(:step_tool) do
    Meth::StepTool.new
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to step' do
        assc = Meth::StepTool.reflect_on_association(:step)
        expect(assc.macro).to eq :belongs_to
      end

      it 'belongs to tool' do
        assc = Meth::StepTool.reflect_on_association(:tool)
        expect(assc.macro).to eq :belongs_to
      end

      it 'has many tool_custom_fields' do
        assc = Meth::StepTool.reflect_on_association(:tool_custom_fields)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      let(:methodology) { FactoryBot.create(:methodology) }
      let(:step) { FactoryBot.create(:meth_step, methodology: methodology) }
      let(:tool) { FactoryBot.create(:meth_tool) }
      it 'is not valid without name' do
        step_tool.name = ''
        step_tool.step = step
        step_tool.tool = tool
        expect(step_tool).to_not be_valid
      end

      it 'is valid with name' do
        step_tool.name = 'Name du step tool'
        step_tool.step = step
        step_tool.tool = tool
        expect(step_tool).to be_valid
      end

      it 'is not valid if step_tool_type is not in (1,2,3)' do
        step_tool.name = 'Name du step tool'
        step_tool.step = step
        step_tool.tool = tool
        step_tool.step_tool_type = 4
        expect(step_tool).to_not be_valid
      end

      it 'is valid with step_tool_type in 1, 2, 3' do
        step_tool.name = 'Name du step tool'
        step_tool.step = step
        step_tool.tool = tool
        step_tool.step_tool_type = 3
        expect(step_tool).to be_valid
      end
    end
  end

  describe 'Copy' do
    let(:methodology) { FactoryBot.create(:methodology) }
    let(:step) { FactoryBot.create(:meth_step, methodology: methodology) }
    let(:tool) { FactoryBot.create(:meth_tool) }
    let(:tool_cf) { Meth::Tool.find_or_create_custom_field }

    before :each do
      step_tool.name = 'StepTool1'
      step_tool.step = step
      step_tool.tool = tool
    end

    it 'copies a step_tool default type' do
      step_tool2 = step_tool.copy(step)

      expect(step_tool2.name).to eq 'StepTool1'
      expect(step_tool2.tool.name).to eq tool.name
    end

    it 'copies a step_tool custom field type' do
      step_tool_cf = step.add_tool(tool_cf)
      cftype = Meth::CustomFieldType.create!(field_type: 'string', field_size: nil, field_internal: 'text', field_name: 'Texte')
      cfdesc = Meth::CustomFieldDesc.create!(custom_field_type: cftype, internal_name: 'custom_1', name: 'Custom Field 1')

      Meth::ToolCustomField.create!(step_tool: step_tool_cf, custom_field_desc: cfdesc)

      step_tool_cf2 = step_tool_cf.copy(step)
      expect(step_tool_cf2.tool.tool_name).to eq 'CustomField'
      expect(step_tool_cf2.tool_custom_fields.first.custom_field_desc.name).to eq 'Custom Field 1'
    end
  end
  # Other Functions are tested in step_spec.rb
end
