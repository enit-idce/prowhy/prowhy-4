class CreateToolsQqoqcps < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_qqoqcps do |t|
      t.references :rca, null: false, foreign_key: true
      t.integer :tool_reference

      t.text :qui
      t.text :qui_not
      t.text :qui_info
      t.text :quoi
      t.text :quoi_not
      t.text :quoi_info
      t.text :ou
      t.text :ou_not
      t.text :ou_info
      t.text :quand
      t.text :quand_not
      t.text :quand_info
      t.text :comment
      t.text :comment_not
      t.text :comment_info
      t.text :pourquoi
      t.text :pourquoi_not
      t.text :pourquoi_info

      t.timestamps
    end
  end
end
