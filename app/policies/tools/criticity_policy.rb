module Tools
  class CriticityPolicy < ToolsPolicy
    attr_reader :user, :record, :rca

    # def index?
    #   user.has_any_role?(:admin, :super_admin, :manager, :user) || user.user_authorized?(rca, 'read', rca.methodology.step_info)
    # end

    # def new?
    #   user.has_any_role?(:admin, :super_admin, :manager, :user) || user.user_authorized?(rca, 'write', rca.methodology.step_info)
    # end

    # def create?
    #   new?
    # end

    # def update?
    #   new?
    # end

    def calculate?
      update?
    end
  end
end
