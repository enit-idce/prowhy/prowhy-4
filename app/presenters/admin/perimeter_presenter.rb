module Admin
  class PerimeterPresenter < BasePresenter
    include ActionView::Helpers::UrlHelper
    include IconsHelper

    # Création de la structure JSon pour jstree
    def tree_line(show_buttons = 'true')
      ret = "<span id='#{id}_link"  \
            "' data-fct-dnd='#{Rails.application.routes.url_helpers.admin_perimeter_path(id, locale: I18n.locale)}'" \
            '>'
      # ret += "<span style='width:calc(100% - 80px);text-overflow: ellipsis; overflow:hidden;'>#{name}</span>"
      ret += name
      ret += '<span>'

      ret += "<span style='padding-right: 5px; float: right;'>"

      if show_buttons == 'true'
        ret += if ancestry?
                 link_to fa_icon_delete_presenter,
                         Rails.application.routes.url_helpers.admin_perimeter_path(id, locale: I18n.locale),
                         method: :delete,
                         class: 'icon-right',
                         title: "Delete content #{name.downcase}",
                         data: { confirm: I18n.t('confirm.del_subtree') }
               else
                 fa_icon_hidden_presenter('icon-right')
               end
        ret += link_to fa_icon_add_presenter,
                       Rails.application.routes.url_helpers.new_admin_perimeter_path(locale: I18n.locale, admin_perimeter: {parent_id: id}),
                       remote: true,
                       class: 'icon-right',
                       title: "Add content after #{name.downcase}"

        ret += if ancestry?
                 link_to fa_icon_edit_presenter,
                         Rails.application.routes.url_helpers.edit_admin_perimeter_path(id, locale: I18n.locale),
                         remote: true,
                         class: 'icon-right',
                         title: 'Edit content'
               else
                 fa_icon_hidden_presenter('icon-right')
               end
      end
      ret += '</span>'

      return ret
    end

    def tree_infos(show_buttons = 'true', attrib_peris = nil)
      data = { text: tree_line(show_buttons),
               id: id,
               type: 'perimeter',
               a_attr: { style: 'width: calc(100% - 25px);' }
             }

      data['state'] = if attrib_peris&.include?(id)
                        {'checked' => true, 'selected' => true }
                      else
                        {'checked' => false, 'selected' => false }
                      end

      return data
    end

    def subtree_datas(show_buttons = 'true', attrib_peris = nil)
      data = tree_infos(show_buttons, attrib_peris)

      unless children.empty?
        data[:children] = []

        children.each do |child|
          data[:children] << PerimeterPresenter.new(child, @view).subtree_datas(show_buttons, attrib_peris)
        end
      end

      return data
    end
  end
end
