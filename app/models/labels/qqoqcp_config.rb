module Labels
  class QqoqcpConfig < ApplicationRecord
    translates :name, fallback: :any

    validates :name, presence: true

    has_many :qqoqcp_cls, -> { order(pos: :asc) }, dependent: :destroy
    has_many :qqoqcp_lines, through: :qqoqcp_cls

    accepts_nested_attributes_for :qqoqcp_cls

    has_many :tools_qqoqcps

    def add_line(line)
      return if qqoqcp_lines.include?(line)

      Labels::QqoqcpCl.create!(qqoqcp_config: self, qqoqcp_line: line, pos: qqoqcp_cls.size+1, usable: true)
      qqoqcp_cls.reload
    end

    def delete_line(line)
      cl = qqoqcp_cls.where(qqoqcp_line_id: line.id)&.first

      cl&.destroy
    end
  end
end
