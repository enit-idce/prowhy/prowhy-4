class AddMailerToAdminConfig < ActiveRecord::Migration[6.0]
  class MigrationConfig < ApplicationRecord
    self.table_name = :admin_admin_configs
    serialize :connexions_backup
    serialize :task_mails_backup
  end

  def up
    add_column :admin_admin_configs, :mailer_options, :json, after: :default_perimeter_id

    # # Connexions
    # rename_column :admin_admin_configs, :connexions, :connexions_backup
    # add_column :admin_admin_configs, :connexions, :json, after: :connexions_backup

    # # TaskMail
    # rename_column :admin_admin_configs, :task_mails, :task_mails_backup
    # add_column :admin_admin_configs, :task_mails, :json, after: :task_mails_backup
  end

  def down
    remove_column :admin_admin_configs, :mailer_options
    # remove_column :admin_admin_configs, :connexions
    # remove_column :admin_admin_configs, :task_mails

    # rename_column :admin_admin_configs, :connexions_backup, :connexions
    # rename_column :admin_admin_configs, :task_mails_backup, :task_mails
  end
end
