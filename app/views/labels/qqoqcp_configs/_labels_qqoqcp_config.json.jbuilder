json.extract! labels_qqoqcp_config, :id, :name, :show_is, :show_isnot, :show_info, :created_at, :updated_at
json.url labels_qqoqcp_config_url(labels_qqoqcp_config, format: :json)
