require 'rails_helper'

RSpec.describe Tools::RcaList, type: :model do
  subject(:rca_list) do
    Tools::RcaList.create!(rca: rca, tool_reference: 1)
  end

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
  let(:rca_sub1) { Rca.create!(title: 'Rca sub 1', methodology: methodology, tools_rca_list: rca_list) }
  let(:rca_sub2) { Rca.create!(title: 'Rca sub 2', methodology: methodology, tools_rca_list: rca_list) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has many rcas' do
        assc = Tools::RcaList.reflect_on_association(:rcas)
        expect(assc.macro).to eq :has_many
      end
    end
  end

  describe 'Create and search model : tool class functions' do
    before :each do
      rca_list.save!
      rca_sub1.save!
      rca_sub2.save!
      options = { rca: rca, tool_reference: 2, tool_num: 1 }
      @rca_list_created = Tools::RcaList.create_default!(options, nil)
    end

    it 'create_default without rcas' do
      expect(@rca_list_created.tool_reference).to eq 2
      expect(@rca_list_created.rca).to eq rca
      expect(@rca_list_created.rcas).to be_empty
    end

    it 'create_default with rcas' do
      rca_sub3 = Rca.create!(title: 'Rca sub 3', methodology: methodology, tools_rca_list: @rca_list_created)
      rca_sub4 = Rca.create!(title: 'Rca sub 4', methodology: methodology, tools_rca_list: @rca_list_created)
      expect(@rca_list_created.tool_reference).to eq 2
      expect(@rca_list_created.rca).to eq rca
      expect(@rca_list_created.rcas).to contain_exactly(rca_sub3, rca_sub4)
    end

    it 'search_default' do
      step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1)
      rca_list_founded = Tools::RcaList.search_default(rca, 'tools_rca_lists', step_tool)

      expect(rca_list_founded.tool_reference).to eq 1
      expect(rca_list_founded).to eq rca_list

      step_tool.tool_reference = 2
      rca_list_founded = Tools::RcaList.search_default(rca, 'tools_rca_lists', step_tool)

      expect(rca_list_founded.tool_reference).to eq 2
      expect(rca_list_founded).to eq @rca_list_created
    end
  end
end
