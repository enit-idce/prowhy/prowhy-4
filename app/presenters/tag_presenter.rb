class TagPresenter < BasePresenter
  include ActionView::Helpers::UrlHelper

  # Création de la structure JSon pour jstree
  def tag_tree_line
    ret = "<span id='#{id}_link'" \
          "' data-fct-dnd='#{Rails.application.routes.url_helpers.tag_path(id, locale: I18n.locale)}'>"
    ret += name ? name.downcase : '-'
    ret += '<span>'
    return ret
  end

  def tag_tree_infos
    data = { text: tag_tree_line,
             id: id,
             type: 'tag'
             # 'state' => {'concept_status' => concept.status }
           }

    # data['state'] = {'selected' => false, 'concept_status' => concept.status }
    return data
  end

  def tag_subtree_datas
    data = tag_tree_infos

    unless children.empty?
      data[:children] = []

      children.each do |child|
        data[:children] << TagPresenter.new(child, @view).tag_subtree_datas
      end
    end

    return data
  end
end
