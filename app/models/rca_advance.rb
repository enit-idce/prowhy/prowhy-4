class RcaAdvance < ApplicationRecord
  belongs_to :rca
  belongs_to :meth_step, class_name: 'Meth::Step'
end
