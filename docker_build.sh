#!/bin/bash 
 
display_usage() 
{ 
	echo "This script must be run with version as argument." 
	echo -e "\nUsage: $0 [version] \n" 
} 
# if less than two arguments supplied, display usage 
if (( $# == 1 ))
then 
  docker build -f dockerfiles/Dockerfile . -t prowhy:$1 --network=host
else
  display_usage
  exit 1
fi 