json.extract! labels_check_list_elem, :id, :name, :task_detail_type, :usable, :created_at, :updated_at
json.url labels_check_list_elem_url(labels_check_list_elem, format: :json)
