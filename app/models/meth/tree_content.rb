require_dependency 'meth'

module Meth
  class TreeContent < ApplicationRecord
    include MethConcern
    include AncestryConcern

    translates :name, fallback: :any

    belongs_to :custom_field_desc, -> { includes(:custom_field_type).where(meth_custom_field_types: {field_internal: 'tree'}) }

    validates :name, presence: true
    validate :validate_type_is_tree

    # scope :order_for_list, -> { sort { |a, b| "#{a.ancestry}/#{a.id}" <=> "#{b.ancestry}/#{b.id}" } }

    def validate_type_is_tree
      return unless custom_field_desc.custom_field_type.field_internal != 'tree'

      errors.add(:custom_field_desc, 'Need a custom field type tree !')
    end

    def name_show
      ancestry ? (name&.downcase || '-') : (custom_field_desc.name + ' : ' + (name&.downcase || '-'))
    end

    # Finalize => has_many CustomFields :
    # Permet d'ajouter le dependent: nullify pour supprimer les infos si on destroy un TreeContent
    extend Finalize

    def self.finalize
      return unless database_exists? # Obligatoire sinon db:create ne fonctionne plus...

      custom_fields_trees = Meth::CustomFieldDesc.includes(:custom_field_type).where(meth_custom_field_types: {field_internal: 'tree'})

      class_eval do
        custom_fields_trees.each do |cf|
          next unless CustomField.column_names.include?(cf.internal_name)

          has_many "#{cf.internal_name}s".intern, class_name: 'CustomField', foreign_key: cf.internal_name, dependent: :nullify
        end
      end
    rescue StandardError
      puts 'DB NOT OK TO FINALIZE !'.red
      # binding.pry
    end

    def self.database_exists?
      ActiveRecord::Base.connection
    rescue ActiveRecord::NoDatabaseError
      false
    else
      true
    end
  end
end
