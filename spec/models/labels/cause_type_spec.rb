require 'rails_helper'

RSpec.describe Labels::CauseType, type: :model do
  subject(:cause_type) do
    Labels::CauseType.create!(name: 'Method')
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      # it 'belongs to labels_cause_type' do
      #   assc = Tools::Cause.reflect_on_association(:labels_cause_type)
      #   expect(assc.macro).to eq :belongs_to
      # end
      it 'has many tools_ishi_branchs' do
        assc = Labels::CauseType.reflect_on_association(:tools_ishi_branchs)
        expect(assc.macro).to eq :has_many
      end
      it 'has many tools_ishikawas (through tools_ishi_branchs)' do
        assc = Labels::CauseType.reflect_on_association(:tools_ishikawas)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools_cause_classes' do
        assc = Labels::CauseType.reflect_on_association(:tools_cause_classes)
        expect(assc.macro).to eq :has_many
      end
      # it 'has many tools_ishikawa_used (through tools_cause_classes)' do
      #   assc = Labels::CauseType.reflect_on_association(:tools_ishikawa_used)
      #   expect(assc.macro).to eq :has_many
      # end
      it 'has many tools_causes (through tools_cause_classes)' do
        assc = Labels::CauseType.reflect_on_association(:tools_causes)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        cause_type.name = ''
        expect(cause_type).to_not be_valid
      end

      it 'is valid with name' do
        cause_type.name = 'Le type cause'
        expect(cause_type).to be_valid
      end
    end
  end
end
