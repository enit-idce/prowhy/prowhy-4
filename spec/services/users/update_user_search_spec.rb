require 'rails_helper'

RSpec.describe Users::UpdateUserSearch, type: :service do
  subject(:service) do
    Users::UpdateUserSearch.new(user)
  end

  let(:user) { FactoryBot.create(:user) }

  describe 'something_to_search?' do
    it 'returs false if there is no search filters' do
      expect(service.something_to_search?).to be false
    end

    it 'returs true if there is search filters' do
      user.usersearch[:rca_search] = { words: 'toto' }
      expect(service.something_to_search?).to be true

      user.usersearch[:rca_search] = {}
      expect(service.something_to_search?).to be false

      user.usersearch[:custom_field] = { custom_1: 'toto' }
      expect(service.something_to_search?).to be true
    end
  end

  it 'reititialize search filters for user' do
    user.usersearch[:rca_search] = { words: 'toto' }
    user.save!

    service.call(reinit_filters: true)

    objtmp = RcaSearch.new
    search_tmp = objtmp.as_json.deep_symbolize_keys

    expect(user.usersearch.deep_symbolize_keys).to eq({custom_field: {}, rca_search: search_tmp})
  end

  it 'copy params in search filters for user' do
    params = { rca_search: { words: 'toto' },
               custom_field: { custom_1: 'truc', custom_2: 3 } }

    service.call(params)
    expect(user.usersearch.deep_symbolize_keys).to eq params
  end
end
