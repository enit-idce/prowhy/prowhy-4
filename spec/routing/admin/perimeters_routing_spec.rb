require 'rails_helper'

RSpec.describe Admin::PerimetersController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/perimeters').to route_to('admin/perimeters#index')
    end

    it 'routes to #new' do
      expect(get: '/admin/perimeters/new').to route_to('admin/perimeters#new')
    end

    it 'routes to #show' do
      expect(get: '/admin/perimeters/1').to route_to('admin/perimeters#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/admin/perimeters/1/edit').to route_to('admin/perimeters#edit', id: '1')
    end


    it 'routes to #create' do
      expect(post: '/admin/perimeters').to route_to('admin/perimeters#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/admin/perimeters/1').to route_to('admin/perimeters#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/admin/perimeters/1').to route_to('admin/perimeters#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/admin/perimeters/1').to route_to('admin/perimeters#destroy', id: '1')
    end
  end
end
