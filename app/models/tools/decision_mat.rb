require_dependency 'tools'

module Tools
  class DecisionMat < ApplicationRecord
    include ToolConcern

    has_many :crit_dec_mats, dependent: :destroy # , class_name: 'Tools::CritDecMat', foreign_key: :decision_mat_id
    has_many :labels_criteria, class_name: 'Labels::Criterium', through: :crit_dec_mats

    has_many :dec_mat_links, dependent: :destroy
    has_many :tools_tasks, through: :dec_mat_links, source: :entry, source_type: 'Tools::Task'
    has_many :tools_causes, through: :dec_mat_links, source: :entry, source_type: 'Tools::Cause'

    def entries
      tools_tasks + tools_causes
    end

    validates :name, presence: true
    validates :entry_class, presence: true
    validates :entry_class, inclusion: { in: ['Tools::Task', 'Tools::Cause'] }

    # ToolConcern functions

    # def search_default(rca, tool_name, step_tool)
    #   return nil unless rca.send(tool_name) && step_tool

    #   rca.send(tool_name).where(entry_class: step_tool.tool_options[:entry_class], tool_reference: step_tool.tool_reference, tool_num: step_tool.tool_num).first
    # end

    def self.create_default!(_options, _step_tool)
      []
    end

    def self.search_default(rca, _tool_name, step_tool)
      step_tool.tool_options ||= {}
      # options_for_search = step_tool.tool_options.merge(tool_reference: step_tool.tool_reference,
      #                                                   tool_num: step_tool.tool_num)
      return rca.tools_decision_mats.where(entry_class: step_tool.tool_options[:entry_class],
                                           tool_reference: step_tool.tool_reference,
                                           tool_num: step_tool.tool_num)
    end

    def self.create_search_multiple?
      return true
    end

    # Decision mat functions
    def add_val_link_crit(link, crit)
      Tools::DecMatVal.create!(dec_mat_link: link, crit_dec_mat: crit, value: 1)
    end

    def add_criterium(criterium, weight = 1)
      # Add criterium to decision mat
      crit_dm = Tools::CritDecMat.create!(labels_criterium: criterium, decision_mat: self, weight: weight)
      # Add Dec mat vals for each entries for the new criterium
      dec_mat_links.each do |link|
        # Tools::DecMatVal.create!(dec_mat_link: link, crit_dec_mat: crit_dm, value: 1)
        add_val_link_crit(link, crit_dm)
      end
    end

    def del_criterium(criterium)
      crits = crit_dec_mats.where(labels_criterium: criterium)

      crits&.each(&:destroy)
    end

    def add_entry(entry)
      link = Tools::DecMatLink.create!(decision_mat: self, entry: entry)
      crit_dec_mats.each do |crit_dm|
        # Tools::DecMatVal.create!(dec_mat_link: link, crit_dec_mat: crit_dm, value: 1)
        add_val_link_crit(link, crit_dm)
      end
      entry.matrix_status_calculate
    end

    def del_entry(entry)
      link = Tools::DecMatLink.where(decision_mat: self, entry: entry)&.first
      return unless link

      link.destroy
      entry.matrix_status_calculate
    end

    def del_entry_from_link(link)
      entry = link.entry
      link.destroy
      entry.matrix_status_calculate
    end
  end
end
