require 'rails_helper'

RSpec.describe Tools::CriticitiesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/tools/criticities').to route_to('tools/criticities#index')
    end

    it 'routes to #new' do
      expect(get: '/tools/criticities/new').to route_to('tools/criticities#new')
    end

    it 'routes to #show' do
      expect(get: '/tools/criticities/1').to route_to('tools/criticities#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/tools/criticities/1/edit').to route_to('tools/criticities#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/tools/criticities').to route_to('tools/criticities#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/tools/criticities/1').to route_to('tools/criticities#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/tools/criticities/1').to route_to('tools/criticities#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/tools/criticities/1').to route_to('tools/criticities#destroy', id: '1')
    end
  end
end
