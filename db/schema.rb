# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2023_10_26_094441) do

  create_table "action_text_rich_texts", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", null: false
    t.text "body", size: :long
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "admin_admin_configs", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.text "connexions"
    t.bigint "default_role_id"
    t.bigint "default_perimeter_id"
    t.json "mailer_options"
    t.text "task_mails"
    t.json "options"
    t.json "home_options"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["default_perimeter_id"], name: "index_admin_admin_configs_on_default_perimeter_id"
    t.index ["default_role_id"], name: "index_admin_admin_configs_on_default_role_id"
  end

  create_table "admin_helper_texts", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "tool_class"
    t.string "reference"
    t.text "help_text_fr"
    t.text "help_text_en"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_ldap_configs", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", default: "Ldap"
    t.string "host"
    t.integer "port"
    t.string "base_dn"
    t.string "admin_login"
    t.string "admin_password"
    t.string "attr_uid", default: "uid"
    t.string "attr_firstname"
    t.string "attr_lastname", default: "sn"
    t.string "attr_email", default: "mail"
    t.string "attr_adress"
    t.string "attr_phone"
    t.boolean "authorize_signup", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "admin_perimeter_rcas", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "perimeter_id", null: false
    t.bigint "rca_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["perimeter_id"], name: "index_admin_perimeter_rcas_on_perimeter_id"
    t.index ["rca_id"], name: "index_admin_perimeter_rcas_on_rca_id"
  end

  create_table "admin_perimeter_users", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "perimeter_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["perimeter_id"], name: "index_admin_perimeter_users_on_perimeter_id"
    t.index ["user_id"], name: "index_admin_perimeter_users_on_user_id"
  end

  create_table "admin_perimeters", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name"
    t.string "ancestry"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ancestry"], name: "index_admin_perimeters_on_ancestry"
  end

  create_table "ahoy_events", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "visit_id"
    t.bigint "user_id"
    t.string "name"
    t.json "properties"
    t.datetime "time"
    t.index ["name", "time"], name: "index_ahoy_events_on_name_and_time"
    t.index ["user_id"], name: "index_ahoy_events_on_user_id"
    t.index ["visit_id"], name: "index_ahoy_events_on_visit_id"
  end

  create_table "ahoy_visits", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "visit_token"
    t.string "visitor_token"
    t.bigint "user_id"
    t.string "ip"
    t.text "user_agent"
    t.text "referrer"
    t.string "referring_domain"
    t.text "landing_page"
    t.string "browser"
    t.string "os"
    t.string "device_type"
    t.string "country"
    t.string "region"
    t.string "city"
    t.float "latitude"
    t.float "longitude"
    t.string "utm_source"
    t.string "utm_medium"
    t.string "utm_term"
    t.string "utm_content"
    t.string "utm_campaign"
    t.string "app_version"
    t.string "os_version"
    t.string "platform"
    t.datetime "started_at"
    t.index ["user_id"], name: "index_ahoy_visits_on_user_id"
    t.index ["visit_token"], name: "index_ahoy_visits_on_visit_token", unique: true
  end

  create_table "audits", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "auditable_id"
    t.string "auditable_type"
    t.integer "associated_id"
    t.string "associated_type"
    t.integer "user_id"
    t.string "user_type"
    t.string "username"
    t.string "action"
    t.text "audited_changes"
    t.integer "version", default: 0
    t.string "comment"
    t.string "remote_address"
    t.string "request_uuid"
    t.datetime "created_at"
    t.index ["associated_type", "associated_id"], name: "associated_index"
    t.index ["auditable_type", "auditable_id", "version"], name: "auditable_index"
    t.index ["created_at"], name: "index_audits_on_created_at"
    t.index ["request_uuid"], name: "index_audits_on_request_uuid"
    t.index ["user_id", "user_type"], name: "user_index"
  end

  create_table "custom_fields", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["rca_id"], name: "index_custom_fields_on_rca_id"
  end

  create_table "labels_cause_types", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name_fr"
    t.string "name_en"
    t.integer "pos"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "labels_check_list_elems", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name"
    t.string "task_detail_type"
    t.boolean "usable"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "labels_configs", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name_fr"
    t.string "name_en"
    t.string "formula"
    t.boolean "usable", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "labels_criteria", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "crit_type", default: 0
    t.string "internal_name"
    t.string "name_fr"
    t.string "name_en"
    t.text "description_fr"
    t.text "description_en"
    t.integer "threshold", default: 0
    t.bigint "config_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["config_id"], name: "index_labels_criteria_on_config_id"
  end

  create_table "labels_qqoqcp_cls", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "qqoqcp_config_id", null: false
    t.bigint "qqoqcp_line_id", null: false
    t.integer "pos", default: 0
    t.boolean "usable", default: true
    t.index ["qqoqcp_config_id"], name: "index_labels_qqoqcp_cls_on_qqoqcp_config_id"
    t.index ["qqoqcp_line_id"], name: "index_labels_qqoqcp_cls_on_qqoqcp_line_id"
  end

  create_table "labels_qqoqcp_configs", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name_fr"
    t.string "name_en"
    t.boolean "show_is", default: true
    t.boolean "show_isnot", default: true
    t.boolean "show_info", default: true
    t.boolean "usable", default: true
    t.integer "nb_lines", default: 3
  end

  create_table "labels_qqoqcp_lines", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "internal_name"
    t.string "name_fr"
    t.string "name_en"
  end

  create_table "labels_values", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "criterium_id", null: false
    t.integer "value", default: 0
    t.string "title_fr"
    t.string "title_en"
    t.text "text_fr"
    t.text "text_en"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["criterium_id"], name: "index_labels_values_on_criterium_id"
  end

  create_table "login_activities", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "scope"
    t.string "strategy"
    t.string "identity"
    t.boolean "success"
    t.string "failure_reason"
    t.string "user_type"
    t.bigint "user_id"
    t.string "context"
    t.string "ip"
    t.text "user_agent"
    t.text "referrer"
    t.string "city"
    t.string "region"
    t.string "country"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at"
    t.index ["identity"], name: "index_login_activities_on_identity"
    t.index ["ip"], name: "index_login_activities_on_ip"
    t.index ["user_type", "user_id"], name: "index_login_activities_on_user_type_and_user_id"
  end

  create_table "meth_activities", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name_fr"
    t.string "name_en"
    t.integer "pos"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "meth_custom_field_descs", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "custom_field_type_id", null: false
    t.string "internal_name"
    t.boolean "show_list", default: false
    t.boolean "show_search", default: true
    t.json "options"
    t.string "name_fr"
    t.string "name_en"
    t.bigint "ref_field_id"
    t.json "ref_show_values"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["custom_field_type_id"], name: "index_meth_custom_field_descs_on_custom_field_type_id"
    t.index ["ref_field_id"], name: "index_meth_custom_field_descs_on_ref_field_id"
  end

  create_table "meth_custom_field_types", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "field_type"
    t.integer "field_size"
    t.string "field_internal"
    t.string "field_name_fr"
    t.string "field_name_en"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "meth_list_contents", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "custom_field_desc_id", null: false
    t.string "name_fr"
    t.string "name_en"
    t.integer "pos", default: 0
    t.integer "ref_value", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["custom_field_desc_id"], name: "index_meth_list_contents_on_custom_field_desc_id"
  end

  create_table "meth_methodologies", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.boolean "locked", default: false, null: false
    t.boolean "usable", default: true
    t.string "name_fr"
    t.string "name_en"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "meth_step_roles", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "role_name"
    t.string "name_fr"
    t.string "name_en"
    t.integer "advance_level", default: 0
  end

  create_table "meth_step_tools", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "step_id", null: false
    t.bigint "tool_id", null: false
    t.integer "step_tool_type", default: 1
    t.boolean "locked", default: false
    t.integer "tool_reference", default: 1
    t.integer "tool_num", default: 1
    t.integer "pos"
    t.string "name_fr"
    t.string "name_en"
    t.text "tool_options_backup"
    t.json "tool_options"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["step_id"], name: "index_meth_step_tools_on_step_id"
    t.index ["tool_id"], name: "index_meth_step_tools_on_tool_id"
  end

  create_table "meth_steps", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name_fr"
    t.string "name_en"
    t.integer "pos"
    t.bigint "methodology_id", null: false
    t.integer "step_type", default: 1
    t.bigint "step_role_id"
    t.boolean "locked", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["methodology_id"], name: "index_meth_steps_on_methodology_id"
    t.index ["step_role_id"], name: "index_meth_steps_on_step_role_id"
  end

  create_table "meth_tool_custom_fields", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "step_tool_id", null: false
    t.bigint "custom_field_desc_id", null: false
    t.integer "pos", default: 0
    t.boolean "visible", default: true
    t.boolean "obligatory", default: false
    t.string "default_value", default: ""
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["custom_field_desc_id"], name: "index_meth_tool_custom_fields_on_custom_field_desc_id"
    t.index ["step_tool_id"], name: "index_meth_tool_custom_fields_on_step_tool_id"
  end

  create_table "meth_tools", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name_fr"
    t.string "name_en"
    t.bigint "activity_id", null: false
    t.string "tool_name"
    t.text "tool_options_backup"
    t.json "tool_options"
    t.string "tool_icon"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["activity_id"], name: "index_meth_tools_on_activity_id"
  end

  create_table "meth_tree_contents", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "custom_field_desc_id", null: false
    t.string "name_fr"
    t.string "name_en"
    t.string "ancestry"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ancestry"], name: "index_meth_tree_contents_on_ancestry"
    t.index ["custom_field_desc_id"], name: "index_meth_tree_contents_on_custom_field_desc_id"
  end

  create_table "oauth_access_grants", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "resource_owner_id", null: false
    t.bigint "application_id", null: false
    t.string "token", null: false
    t.integer "expires_in", null: false
    t.text "redirect_uri", null: false
    t.datetime "created_at", null: false
    t.datetime "revoked_at"
    t.string "scopes", default: "", null: false
    t.index ["application_id"], name: "index_oauth_access_grants_on_application_id"
    t.index ["resource_owner_id"], name: "index_oauth_access_grants_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_grants_on_token", unique: true
  end

  create_table "oauth_access_tokens", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "resource_owner_id"
    t.bigint "application_id"
    t.string "token", null: false
    t.string "refresh_token"
    t.integer "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at", null: false
    t.string "scopes"
    t.string "previous_refresh_token", default: "", null: false
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true
    t.index ["resource_owner_id"], name: "fk_rails_ee63f25419"
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true
  end

  create_table "oauth_applications", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", null: false
    t.string "uid", null: false
    t.string "secret", null: false
    t.text "redirect_uri"
    t.string "scopes", default: "", null: false
    t.boolean "confidential", default: true, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["uid"], name: "index_oauth_applications_on_uid", unique: true
  end

  create_table "people", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "firstname"
    t.string "lastname"
    t.string "email"
    t.string "adress"
    t.string "phone"
    t.text "moreinfo"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "posts", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "profil_authorizes", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "profil_id", null: false
    t.bigint "meth_step_role_id", null: false
    t.boolean "read", default: true
    t.boolean "write", default: false
    t.boolean "advance", default: false
    t.boolean "email", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["meth_step_role_id"], name: "index_profil_authorizes_on_meth_step_role_id"
    t.index ["profil_id"], name: "index_profil_authorizes_on_profil_id"
  end

  create_table "profils", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "profil_name"
    t.string "name_fr"
    t.string "name_en"
    t.boolean "profil_locked", default: false
    t.boolean "profil_modifiable", default: true
    t.integer "pos", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "rca_advances", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.bigint "meth_step_id", null: false
    t.integer "advance", default: 0
    t.date "date_start"
    t.date "date_finish"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["meth_step_id"], name: "index_rca_advances_on_meth_step_id"
    t.index ["rca_id"], name: "index_rca_advances_on_rca_id"
  end

  create_table "rcas", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "reference"
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.date "closed_at"
    t.bigint "methodology_id", null: false
    t.bigint "advance_step_id"
    t.integer "advance", default: 0
    t.integer "status", default: 1
    t.bigint "tools_rca_list_id"
    t.index ["advance_step_id"], name: "index_rcas_on_advance_step_id"
    t.index ["methodology_id"], name: "index_rcas_on_methodology_id"
    t.index ["tools_rca_list_id"], name: "index_rcas_on_tools_rca_list_id"
  end

  create_table "roles", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "taggings", id: :integer, charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "tag_id"
    t.string "taggable_type"
    t.integer "taggable_id"
    t.string "tagger_type"
    t.integer "tagger_id"
    t.string "context", limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["tag_id"], name: "index_taggings_on_tag_id"
    t.index ["taggable_id", "taggable_type", "context"], name: "taggings_taggable_context_idx"
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id"
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type"
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id"
  end

  create_table "tags", id: :integer, charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "name", collation: "utf8mb3_bin"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "taggings_count", default: 0
    t.string "ancestry"
    t.index ["ancestry"], name: "index_tags_on_ancestry"
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "tools_cause_classes", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "tools_cause_id", null: false
    t.bigint "tools_ishikawa_id", null: false
    t.bigint "labels_cause_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["labels_cause_type_id"], name: "index_tools_cause_classes_on_labels_cause_type_id"
    t.index ["tools_cause_id"], name: "index_tools_cause_classes_on_tools_cause_id"
    t.index ["tools_ishikawa_id"], name: "index_tools_cause_classes_on_tools_ishikawa_id"
  end

  create_table "tools_cause_links", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "cause_id", null: false
    t.string "cause_tool_type", null: false
    t.bigint "cause_tool_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["cause_id"], name: "index_tools_cause_links_on_cause_id"
    t.index ["cause_tool_type", "cause_tool_id"], name: "index_tools_cause_links_on_cause_tool_type_and_cause_tool_id"
  end

  create_table "tools_cause_trees", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.integer "tool_reference"
    t.integer "tool_num", default: 1
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["rca_id"], name: "index_tools_cause_trees_on_rca_id"
  end

  create_table "tools_causes", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.integer "tool_reference"
    t.string "name"
    t.string "ancestry"
    t.integer "level", default: 0
    t.integer "status", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ancestry"], name: "index_tools_causes_on_ancestry"
    t.index ["rca_id"], name: "index_tools_causes_on_rca_id"
  end

  create_table "tools_crit_dec_mats", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "labels_criterium_id", null: false
    t.bigint "decision_mat_id", null: false
    t.integer "weight"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["decision_mat_id"], name: "index_tools_crit_dec_mats_on_decision_mat_id"
    t.index ["labels_criterium_id"], name: "index_tools_crit_dec_mats_on_labels_criterium_id"
  end

  create_table "tools_criticities", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.integer "tool_reference"
    t.integer "tool_num", default: 1
    t.bigint "labels_config_id", null: false
    t.integer "value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["labels_config_id"], name: "index_tools_criticities_on_labels_config_id"
    t.index ["rca_id"], name: "index_tools_criticities_on_rca_id"
  end

  create_table "tools_criticity_values", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "criticity_id", null: false
    t.bigint "labels_criterium_id", null: false
    t.bigint "labels_value_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["criticity_id"], name: "index_tools_criticity_values_on_criticity_id"
    t.index ["labels_criterium_id"], name: "index_tools_criticity_values_on_labels_criterium_id"
    t.index ["labels_value_id"], name: "index_tools_criticity_values_on_labels_value_id"
  end

  create_table "tools_dec_mat_links", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "decision_mat_id", null: false
    t.string "entry_type", null: false
    t.bigint "entry_id", null: false
    t.integer "matrix_status", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["decision_mat_id"], name: "index_tools_dec_mat_links_on_decision_mat_id"
    t.index ["entry_type", "entry_id"], name: "index_tools_dec_mat_links_on_entry_type_and_entry_id"
  end

  create_table "tools_dec_mat_vals", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "crit_dec_mat_id", null: false
    t.bigint "dec_mat_link_id", null: false
    t.integer "value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["crit_dec_mat_id"], name: "index_tools_dec_mat_vals_on_crit_dec_mat_id"
    t.index ["dec_mat_link_id"], name: "index_tools_dec_mat_vals_on_dec_mat_link_id"
  end

  create_table "tools_decision_mats", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.string "name"
    t.string "entry_class"
    t.integer "max_value", default: 3
    t.integer "tool_reference"
    t.integer "tool_num", default: 1
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["rca_id"], name: "index_tools_decision_mats_on_rca_id"
  end

  create_table "tools_documents", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.integer "tool_reference"
    t.integer "tool_num", default: 1
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["rca_id"], name: "index_tools_documents_on_rca_id"
  end

  create_table "tools_efficacities", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "element_type", null: false
    t.bigint "element_id", null: false
    t.integer "check_status", default: 0, null: false
    t.integer "percentage"
    t.date "date_due_check"
    t.date "date_done_check"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["element_type", "element_id"], name: "index_tools_efficacities_on_element_type_and_element_id"
  end

  create_table "tools_file_versions", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "element_type", null: false
    t.bigint "element_id", null: false
    t.datetime "date"
    t.bigint "author_id"
    t.index ["author_id"], name: "index_tools_file_versions_on_author_id"
    t.index ["element_type", "element_id"], name: "index_tools_file_versions_on_element_type_and_element_id"
  end

  create_table "tools_generics", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.integer "tool_reference"
    t.integer "tool_num"
    t.text "synthesis"
    t.json "users_info"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["rca_id"], name: "index_tools_generics_on_rca_id"
  end

  create_table "tools_images", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.integer "tool_reference"
    t.integer "tool_num", default: 1
    t.string "name"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["rca_id"], name: "index_tools_images_on_rca_id"
  end

  create_table "tools_indicator_measures", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "indicator_id", null: false
    t.string "name"
    t.text "description"
    t.integer "value"
    t.integer "pos", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["indicator_id"], name: "index_tools_indicator_measures_on_indicator_id"
  end

  create_table "tools_indicators", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.integer "tool_reference", default: 1
    t.integer "tool_num", default: 1
    t.string "name"
    t.text "description"
    t.integer "target"
    t.json "graph_options"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["rca_id"], name: "index_tools_indicators_on_rca_id"
  end

  create_table "tools_ishi_branchs", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "tools_ishikawa_id", null: false
    t.bigint "labels_cause_type_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["labels_cause_type_id"], name: "index_tools_ishi_branchs_on_labels_cause_type_id"
    t.index ["tools_ishikawa_id"], name: "index_tools_ishi_branchs_on_tools_ishikawa_id"
  end

  create_table "tools_ishikawas", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.integer "tool_reference"
    t.integer "tool_num", default: 1
    t.text "options"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["rca_id"], name: "index_tools_ishikawas_on_rca_id"
  end

  create_table "tools_matrix_stats", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "element_type", null: false
    t.bigint "element_id", null: false
    t.integer "matrix_status", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["element_type", "element_id"], name: "index_tools_matrix_stats_on_element_type_and_element_id"
  end

  create_table "tools_qqoqcp_values", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "qqoqcp_id", null: false
    t.bigint "labels_qqoqcp_line_id", null: false
    t.text "is_value"
    t.text "isnot_value"
    t.text "info_value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["labels_qqoqcp_line_id"], name: "index_tools_qqoqcp_values_on_labels_qqoqcp_line_id"
    t.index ["qqoqcp_id"], name: "index_tools_qqoqcp_values_on_qqoqcp_id"
  end

  create_table "tools_qqoqcps", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.integer "tool_reference"
    t.integer "tool_num", default: 1
    t.bigint "labels_qqoqcp_config_id"
    t.text "qui"
    t.text "qui_not"
    t.text "qui_info"
    t.text "quoi"
    t.text "quoi_not"
    t.text "quoi_info"
    t.text "ou"
    t.text "ou_not"
    t.text "ou_info"
    t.text "quand"
    t.text "quand_not"
    t.text "quand_info"
    t.text "comment"
    t.text "comment_not"
    t.text "comment_info"
    t.text "pourquoi"
    t.text "pourquoi_not"
    t.text "pourquoi_info"
    t.text "combien"
    t.text "combien_not"
    t.text "combien_info"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["labels_qqoqcp_config_id"], name: "index_tools_qqoqcps_on_labels_qqoqcp_config_id"
    t.index ["rca_id"], name: "index_tools_qqoqcps_on_rca_id"
  end

  create_table "tools_rca_lists", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.integer "tool_reference"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["rca_id"], name: "index_tools_rca_lists_on_rca_id"
  end

  create_table "tools_reschedules", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "tools_task_id", null: false
    t.date "date"
    t.text "comment"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["tools_task_id"], name: "index_tools_reschedules_on_tools_task_id"
  end

  create_table "tools_task_validations", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.integer "status", default: 0, null: false
  end

  create_table "tools_tasks", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.integer "tool_reference"
    t.integer "tool_num", default: 1
    t.bigint "task_detail_id"
    t.string "task_detail_type"
    t.string "name"
    t.bigint "check_list_elem_id"
    t.bigint "tools_cause_id", null: false
    t.string "ancestry"
    t.bigint "responsible_rca_id"
    t.bigint "responsible_action_id"
    t.integer "advancement", default: 0
    t.text "result"
    t.integer "priority", default: 0, null: false
    t.date "date_due"
    t.date "date_completion"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ancestry"], name: "index_tools_tasks_on_ancestry"
    t.index ["check_list_elem_id"], name: "index_tools_tasks_on_check_list_elem_id"
    t.index ["rca_id"], name: "index_tools_tasks_on_rca_id"
    t.index ["responsible_action_id"], name: "index_tools_tasks_on_responsible_action_id"
    t.index ["responsible_rca_id"], name: "index_tools_tasks_on_responsible_rca_id"
    t.index ["task_detail_type", "task_detail_id"], name: "index_tools_tasks_on_task_detail_type_and_task_detail_id"
    t.index ["tools_cause_id"], name: "index_tools_tasks_on_tools_cause_id"
  end

  create_table "tools_teams", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "rca_id", null: false
    t.integer "tool_reference"
    t.integer "tool_num", default: 1
    t.bigint "person_id", null: false
    t.bigint "profil_id", null: false
    t.index ["person_id"], name: "index_tools_teams_on_person_id"
    t.index ["profil_id"], name: "index_tools_teams_on_profil_id"
    t.index ["rca_id"], name: "index_tools_teams_on_rca_id"
  end

  create_table "users", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "username"
    t.bigint "ldap_config_id"
    t.bigint "person_id"
    t.json "preferences"
    t.json "usersession"
    t.json "usersearch"
    t.json "pa_options"
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["ldap_config_id"], name: "index_users_on_ldap_config_id"
    t.index ["person_id"], name: "index_users_on_person_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_roles", id: false, charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "admin_admin_configs", "admin_perimeters", column: "default_perimeter_id"
  add_foreign_key "admin_admin_configs", "roles", column: "default_role_id"
  add_foreign_key "admin_perimeter_rcas", "admin_perimeters", column: "perimeter_id"
  add_foreign_key "admin_perimeter_rcas", "rcas"
  add_foreign_key "admin_perimeter_users", "admin_perimeters", column: "perimeter_id"
  add_foreign_key "admin_perimeter_users", "users"
  add_foreign_key "ahoy_events", "ahoy_visits", column: "visit_id"
  add_foreign_key "ahoy_events", "users"
  add_foreign_key "ahoy_visits", "users"
  add_foreign_key "custom_fields", "rcas"
  add_foreign_key "labels_criteria", "labels_configs", column: "config_id"
  add_foreign_key "labels_qqoqcp_cls", "labels_qqoqcp_configs", column: "qqoqcp_config_id"
  add_foreign_key "labels_qqoqcp_cls", "labels_qqoqcp_lines", column: "qqoqcp_line_id"
  add_foreign_key "labels_values", "labels_criteria", column: "criterium_id"
  add_foreign_key "meth_custom_field_descs", "meth_custom_field_descs", column: "ref_field_id"
  add_foreign_key "meth_custom_field_descs", "meth_custom_field_types", column: "custom_field_type_id"
  add_foreign_key "meth_list_contents", "meth_custom_field_descs", column: "custom_field_desc_id"
  add_foreign_key "meth_step_tools", "meth_steps", column: "step_id"
  add_foreign_key "meth_step_tools", "meth_tools", column: "tool_id"
  add_foreign_key "meth_steps", "meth_methodologies", column: "methodology_id"
  add_foreign_key "meth_steps", "meth_step_roles", column: "step_role_id"
  add_foreign_key "meth_tool_custom_fields", "meth_custom_field_descs", column: "custom_field_desc_id"
  add_foreign_key "meth_tool_custom_fields", "meth_step_tools", column: "step_tool_id"
  add_foreign_key "meth_tools", "meth_activities", column: "activity_id"
  add_foreign_key "meth_tree_contents", "meth_custom_field_descs", column: "custom_field_desc_id"
  add_foreign_key "oauth_access_grants", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_tokens", "users", column: "resource_owner_id"
  add_foreign_key "profil_authorizes", "meth_step_roles"
  add_foreign_key "profil_authorizes", "profils"
  add_foreign_key "rca_advances", "meth_steps"
  add_foreign_key "rca_advances", "rcas"
  add_foreign_key "rcas", "meth_methodologies", column: "methodology_id"
  add_foreign_key "rcas", "meth_steps", column: "advance_step_id"
  add_foreign_key "rcas", "tools_rca_lists"
  add_foreign_key "taggings", "tags"
  add_foreign_key "tools_cause_classes", "labels_cause_types"
  add_foreign_key "tools_cause_classes", "tools_causes"
  add_foreign_key "tools_cause_classes", "tools_ishikawas"
  add_foreign_key "tools_cause_links", "tools_causes", column: "cause_id"
  add_foreign_key "tools_cause_trees", "rcas"
  add_foreign_key "tools_causes", "rcas"
  add_foreign_key "tools_crit_dec_mats", "labels_criteria"
  add_foreign_key "tools_crit_dec_mats", "tools_decision_mats", column: "decision_mat_id"
  add_foreign_key "tools_criticities", "labels_configs"
  add_foreign_key "tools_criticities", "rcas"
  add_foreign_key "tools_criticity_values", "labels_criteria"
  add_foreign_key "tools_criticity_values", "labels_values"
  add_foreign_key "tools_criticity_values", "tools_criticities", column: "criticity_id"
  add_foreign_key "tools_dec_mat_links", "tools_decision_mats", column: "decision_mat_id"
  add_foreign_key "tools_dec_mat_vals", "tools_crit_dec_mats", column: "crit_dec_mat_id"
  add_foreign_key "tools_dec_mat_vals", "tools_dec_mat_links", column: "dec_mat_link_id"
  add_foreign_key "tools_decision_mats", "rcas"
  add_foreign_key "tools_documents", "rcas"
  add_foreign_key "tools_file_versions", "people", column: "author_id"
  add_foreign_key "tools_generics", "rcas"
  add_foreign_key "tools_images", "rcas"
  add_foreign_key "tools_indicator_measures", "tools_indicators", column: "indicator_id"
  add_foreign_key "tools_indicators", "rcas"
  add_foreign_key "tools_ishi_branchs", "labels_cause_types"
  add_foreign_key "tools_ishi_branchs", "tools_ishikawas"
  add_foreign_key "tools_ishikawas", "rcas"
  add_foreign_key "tools_qqoqcp_values", "labels_qqoqcp_lines"
  add_foreign_key "tools_qqoqcp_values", "tools_qqoqcps", column: "qqoqcp_id"
  add_foreign_key "tools_qqoqcps", "labels_qqoqcp_configs"
  add_foreign_key "tools_qqoqcps", "rcas"
  add_foreign_key "tools_rca_lists", "rcas"
  add_foreign_key "tools_reschedules", "tools_tasks"
  add_foreign_key "tools_tasks", "labels_check_list_elems", column: "check_list_elem_id"
  add_foreign_key "tools_tasks", "people", column: "responsible_action_id"
  add_foreign_key "tools_tasks", "people", column: "responsible_rca_id"
  add_foreign_key "tools_tasks", "rcas"
  add_foreign_key "tools_tasks", "tools_causes"
  add_foreign_key "tools_teams", "people"
  add_foreign_key "tools_teams", "profils"
  add_foreign_key "tools_teams", "rcas"
  add_foreign_key "users", "admin_ldap_configs", column: "ldap_config_id"
  add_foreign_key "users", "people"
  add_foreign_key "users_roles", "roles"
  add_foreign_key "users_roles", "users"
end
