module Admin
  class AdminConfigsController < ApplicationController
    include AdminConcern
    before_action :set_admin_config, only: [:show, :edit, :update, :destroy, :delete_customer]

    # GET /admin/admin_configs
    # GET /admin/admin_configs.json
    def index
      authorize AdminConfig
      # @admin_admin_configs = Admin::AdminConfig.all

      @admin_config = Admin::AdminConfig.first
      @admin_config ||= Admin::AdminConfig.create!

      redirect_to edit_admin_admin_config_path(@admin_config, type: params[:type])
    end

    # GET /admin/admin_configs/1/edit
    def edit
      authorize @admin_config

      @admin_config.mailer_options ||= {}

      # Custom fields
      order = @admin_config.home_options['show_custom_order'] || []
      cfd_list = Meth::CustomFieldDesc.where(show_list: true)
      order += cfd_list.map(&:id).map(&:to_s)

      @custom_field_descs_list = cfd_list.sort { |a, b| order.find_index(a.id.to_s) - order.find_index(b.id.to_s) }

      # Profils
      @profils_list_all = []
      @profils_list = Profil.where(id: @admin_config.home_options['show_profil'] || [])
      Profil.all.each do |profil|
        next if @admin_config.home_options['show_profil']&.include?(profil.id.to_s)

        @profils_list_all << profil
      end
      # binding.pry

      render :edit, locals: {type: params[:type]}
    end

    # PATCH/PUT /admin/admin_configs/1
    # PATCH/PUT /admin/admin_configs/1.json
    def update
      @admin_config.next_default_role = admin_config_params[:default_role_id].to_i
      authorize @admin_config

      params['admin_admin_config'] && params['admin_admin_config']['home_options'] && params['admin_admin_config']['home_options']['show_custom_order'] ||= []

      respond_to do |format|
        if @admin_config.update(admin_config_params)
          format.html { redirect_to admin_admin_configs_url(type: params[:type]), notice: 'Admin config was successfully updated.' }
          format.json { render :show, status: :ok, location: @admin_config }
        else
          format.html do
            redirect_to edit_admin_admin_config_path(@admin_config, type: params[:type]),
                        flash: { error: @admin_config.errors.full_messages.join(', ') }
          end
          format.json { render json: @admin_config.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE
    def delete_customer
      authorize @admin_config, :update?

      if params[:image] == 'image'
        @admin_config.customer_image.purge
      else # 'image'
        @admin_config.customer_logo.purge
      end

      respond_to do |format|
        format.html { redirect_to edit_admin_admin_config_path(@admin_config, type: 'global') }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_admin_config
      @admin_config = Admin::AdminConfig.find(params[:id])
      # @admin_admin_config = @admin_config
      # authorize @admin_config
    end

    # Only allow a list of trusted parameters through.
    def admin_config_params
      params.require(:admin_admin_config).permit(:default_role_id, :default_perimeter_id,
                                                 :customer_logo,
                                                 :customer_image,
                                                 connexions: [:database_login, :database_signup, :ldap_login],
                                                 task_mails: [:activate, :dday, :late, :ndays, :locale,
                                                              :send_action_closed],
                                                 options: [:image_max_size, :doc_max_size,
                                                           :file_versions_nb_max,
                                                           :rca_reference,
                                                           :rca_reference_txt,
                                                           :rca_reference_lock,
                                                           :rca_cod, :rca_cod_txt, :rca_cod_type,
                                                           :rca_permit_assoc],
                                                 home_options: [:show_desc, :show_methodo,
                                                                :show_adv_etape, :show_adv_role,
                                                                :show_adv_duration, :target_adv_duration,
                                                                show_custom_order: [],
                                                                show_profil: []],
                                                 mailer_options: [:from,
                                                                  :port, :domain, :address,
                                                                  :authentication, :password, :user_name,
                                                                  :more_options, :ssl_tls,
                                                                  :openssl_verify_mode, :enable_starttls_auto])
    end
  end
end
