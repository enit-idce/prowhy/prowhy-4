class ProfilPolicy < AdminPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def edit?
    return false if record.profil_locked

    super
  end

  def raz_authorizations?
    update?
  end
end
