class AddSteptrsToMethodology < ActiveRecord::Migration[6.0]
  def change
    add_reference :meth_methodologies, :step_trstools, foreign_key: { to_table: :meth_steps }, after: :step_info_id
  end
end
