json.extract! labels_criterium, :id, :name, :created_at, :updated_at
json.url labels_criterium_url(labels_criterium, format: :json)
