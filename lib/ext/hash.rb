require 'json'

module Ext
  module Hash
    module ClassMethods
      # Build a new object from string representation.
      #
      #   from_string('{"name"=>"Joe"}')
      #
      # @param the_string [String]
      # @return [Hash]
      def from_string(the_string)
        puts 'PARSE =========================================>'.yellow
        the_string.gsub!(/(?<!:|\\):(\w+)/, '"\\1"')
        puts the_string

        the_string.gsub!(/(?<!\\)"=>nil/, '":null')
        puts the_string
        # the_string.gsub!(/(?<!\\)"=>/, '":')

        the_string.gsub!('=>', ': ')

        # binding.pry
        # puts the_string
        JSON.parse(the_string).deep_symbolize_keys
      end
    end
  end
end

class Hash
  extend Ext::Hash::ClassMethods

  def deep_true_false_values
    # binding.pry
    deep_transform_values { |value| (value == 'false' ? false : value == 'true' ? true : value) rescue value }
    # binding.pry
  end

  def value_to_json(lkeys)
    if self[lkeys[0]].class == Hash
      self[lkeys[0]].value_to_json(lkeys.drop(1))
    else
      self[lkeys[0]] = Hash.from_string(self[lkeys[0]])
    end
  end
end
