require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to test the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator. If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails. There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.

RSpec.describe '/labels/values', type: :request do
  let(:valid_attributes) { {title: 'Value1', criterium_id: criterium.id} }
  let(:invalid_attributes) { {title: nil, criterium_id: criterium.id} }

  let(:params_crit) { {criterium_id: criterium.id} }

  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:user) { FactoryBot.create(:user) }
  let(:criterium) { FactoryBot.create(:labels_criterium) }

  before :each do
    user.set_session(:active_step_tool, methodology.steps.first.step_tools.first.id)
    user.save!

    sign_in user
  end

  # describe 'GET /index' do
  #   it 'renders a successful response' do
  #     Labels::Value.create! valid_attributes
  #     get labels_values_url
  #     expect(response).to be_successful
  #   end
  # end

  # describe 'GET /show' do
  #   it 'renders a successful response' do
  #     value = Labels::Value.create! valid_attributes
  #     get labels_value_url(labels_value)
  #     expect(response).to be_successful
  #   end
  # end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_labels_value_url, params: { labels_value: params_crit }, xhr: true
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      labels_value = Labels::Value.create! valid_attributes
      get edit_labels_value_url(labels_value), xhr: true
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Labels::Value' do
        expect {
          post labels_values_url, params: { labels_value: valid_attributes }, xhr: true
        }.to change(Labels::Value, :count).by(1)
      end

      it 'redirects to the created labels_value' do
        post labels_values_url, params: { labels_value: valid_attributes }, xhr: true
        expect(response).to redirect_to(edit_labels_criterium_url(criterium))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Labels::Value' do
        expect {
          post labels_values_url, params: { labels_value: invalid_attributes }, xhr: true
        }.to change(Labels::Value, :count).by(0)
      end

      it 'renders a successful response (i.e. to display the new template)' do
        post labels_values_url, params: { labels_value: invalid_attributes }, xhr: true
        expect(response).to be_successful
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      let(:new_attributes) { {title: 'New title'} }

      it 'updates the requested labels_value' do
        labels_value = Labels::Value.create! valid_attributes
        patch labels_value_url(labels_value), params: { labels_value: new_attributes }, xhr: true
        labels_value.reload

        expect(labels_value.title).to eq 'New title'
      end

      it 'redirects to the criterium edition' do
        labels_value = Labels::Value.create! valid_attributes
        patch labels_value_url(labels_value), params: { labels_value: new_attributes }, xhr: true
        labels_value.reload
        expect(response).to redirect_to(edit_labels_criterium_url(labels_value.criterium))
      end
    end

    context 'with invalid parameters' do
      it 'renders a successful response (i.e. to display the edit template)' do
        labels_value = Labels::Value.create! valid_attributes
        patch labels_value_url(labels_value), params: { labels_value: invalid_attributes }, xhr: true
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested labels_value' do
      labels_value = Labels::Value.create! valid_attributes
      expect {
        delete labels_value_url(labels_value)
      }.to change(Labels::Value, :count).by(-1)
    end

    it 'redirects to the labels_values list' do
      labels_value = Labels::Value.create! valid_attributes
      criterium = labels_value.criterium
      delete labels_value_url(labels_value)
      expect(response).to redirect_to(edit_labels_criterium_url(criterium))
    end
  end
end
