class AddAdvanceToRcas < ActiveRecord::Migration[6.0]
  def change
    # add_column :rcas, :advance, :integer, default: 0, after: :methodology_id
    add_reference :rcas, :advance_step, foreign_key: { to_table: :meth_steps }, after: :methodology_id
    add_column :rcas, :advance, :integer, default: 0, after: :advance_step_id
  end
end
