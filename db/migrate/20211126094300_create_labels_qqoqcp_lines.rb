class CreateLabelsQqoqcpLines < ActiveRecord::Migration[6.0]
  def change
    create_table :labels_qqoqcp_lines do |t|
      t.string :internal_name
      t.string :name_fr
      t.string :name_en

      # t.timestamps
    end
  end
end
