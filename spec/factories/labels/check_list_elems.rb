FactoryBot.define do
  factory :labels_check_list_elem, class: Labels::CheckListElem do
    sequence(:name) { |n| "Name #{n}" }
    task_detail_type { 'TypeTaskDetail' }
  end
end
