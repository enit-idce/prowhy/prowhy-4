FactoryBot.define do
  factory :user do
    sequence(:username) { |n| "test#{n}" }
    sequence(:email) { |n| "test-#{n}@prowhy.org" }
    password { '123456' }

    association :person, factory: :person

    # trait :role_user do
    #   after(:create) do |user|
    #     user.add_role :user
    #   end
    # end

    # trait :role_manager do
    #   after(:create) do |user|
    #     user.add_role :manager
    #   end
    # end

    # trait :role_admin do
    #   after(:create) do |user|
    #     user.add_role :admin
    #   end
    # end

    # trait :role_super_admin do
    #   after(:create) do |user|
    #     user.add_role :super_admin
    #   end
    # end

    after(:create) do |user|
      user.add_role :super_admin
      user.add_role :super_vision
      # perimeter = Admin::Perimeter.first || FactoryBot.create(:perimeter)
      # user.add_perimeter perimeter.id
      # user.set_preference(:perimeter, perimeter.id)
      # user.reload
    end
  end
end
