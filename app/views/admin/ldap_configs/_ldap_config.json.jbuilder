json.extract! ldap_config, :id, :created_at, :updated_at
json.url admin_ldap_config_url(ldap_config, format: :json)
