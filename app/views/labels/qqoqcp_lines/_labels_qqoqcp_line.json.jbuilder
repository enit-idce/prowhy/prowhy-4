json.extract! labels_qqoqcp_line, :id, :name, :created_at, :updated_at
json.url labels_qqoqcp_line_url(labels_qqoqcp_line, format: :json)
