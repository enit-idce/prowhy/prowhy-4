require 'rails_helper'

RSpec.describe Rcas::UpdateActionsTeam, type: :service do
  subject(:service) do
    Rcas::UpdateActionsTeam.new(rca)
  end

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
  let(:cause) { Tools::Cause.create!(rca: rca, tool_reference: 1, name: 'Cause 1') }
  let(:person1) { FactoryBot.create(:person) }
  let(:person2) { FactoryBot.create(:person) }
  let(:person3) { FactoryBot.create(:person) }
  let(:person4) { FactoryBot.create(:person) }
  # let(:user) { FactoryBot.create(:user, person_id: person.id) }

  before :each do
    @task1 = FactoryBot.create(:task, rca: rca, name: 'Task 1', tools_cause: cause,
                                      responsible_action: person1, responsible_rca: person2,
                                      advancement: 0)
    FactoryBot.create(:profil)
    FactoryBot.create(:profil)
    @profil_res_act = FactoryBot.create(:profil, profil_name: 'resp_act')
  end

  describe 'update team action responsibles' do
    it 'add teams' do
      service.call

      expect(rca.tools_teams.where(profil_id: @profil_res_act.id).count).to eq 2
      expect(rca.tools_teams.where(profil_id: @profil_res_act.id).map(&:person_id)).to contain_exactly(person1.id, person2.id)
    end

    it 'add one more team' do
      FactoryBot.create(:task, rca: rca, name: 'Task 1', tools_cause: cause,
                               responsible_action: person1, responsible_rca: person3,
                               advancement: 0)
      service.call

      expect(rca.tools_teams.where(profil_id: @profil_res_act.id).count).to eq 3
      expect(rca.tools_teams.where(profil_id: @profil_res_act.id).map(&:person_id)).to contain_exactly(person1.id, person2.id, person3.id)
    end

    it 'del one team' do
      service.call
      @task1.responsible_action = person4
      @task1.save!
      service.call

      expect(rca.tools_teams.where(profil_id: @profil_res_act.id).count).to eq 2
      expect(rca.tools_teams.where(profil_id: @profil_res_act.id).map(&:person_id)).to contain_exactly(person2.id, person4.id)
    end
  end
end
