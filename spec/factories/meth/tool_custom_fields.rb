FactoryBot.define do
  factory :meth_tool_custom_field, class: Meth::ToolCustomField do
    sequence(:pos) { |n| n }
    association :step_tool, factory: :meth_step_tool
    association :custom_field_desc, factory: :meth_custom_field_desc
  end
end
