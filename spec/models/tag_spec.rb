require 'rails_helper'

RSpec.describe Tag, type: :model do
  subject(:tag) do
    Tag.create!(name: 'Tag 1')
  end

  # Tests has_many :tools + dependant: destroy
  context 'describes model ancestry' do
    it 'tag parent is nil when it has has no parent' do
      expect(tag.parent).to be_nil
    end

    it 'tag2 parent is tag1 when ancestry is set' do
      tag2 = Tag.create!(name: 'Tag 2', ancestry: tag.id.to_s)
      expect(tag2.parent).to eq tag
      expect(tag.children.count).to eq 1
    end
  end
end
