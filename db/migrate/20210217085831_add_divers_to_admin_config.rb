class AddDiversToAdminConfig < ActiveRecord::Migration[6.0]
  def change
    add_column :admin_admin_configs, :options, :json, after: :task_mails

    # TODO: déplacer dans la migration de labels_criteria
    # add_column :labels_criteria, :crit_type, :integer, default: 0, after: :id
  end
end
