# TODO : Déplacer dans des services
# Dossier mail_services
# mails_services/tasks_services
# mails_services/rcas_services

module SendMails
  # Send a reminder email to task responsibles (from action plan page)
  def self.task_send_one_email(task, type_resp, message = '')
    person = task.send("responsible_#{type_resp}")
    email = person&.user&.email
    email ||= person&.email

    # puts "Send a task email to #{type_resp} responsible : #{email}".yellow
    ProwhyMailer.with(person: person, task: task, email: email, message: message).task_email.deliver_now if email
  end

  def self.task_send_email(task_id, options = {})
    task = Tools::Task.find(task_id)
    options[:rca] = true if options[:rca].nil?
    options[:action] = true if options[:action].nil?
    task_send_one_email(task, 'rca', options[:message]) if options[:rca] && task.responsible_rca

    return unless options[:action] && task.responsible_action && (task.responsible_action!=task.responsible_rca || !options[:rca])

    # puts "Send email to #{task.responsible_action.name} as responsible ACTION."
    task_send_one_email(task, 'action', options[:message])
  end

  # Send an email to recipients list (from report page)
  def self.rca_report_send_mail(recipients, rca, options, pdf_attached)
    emails = []
    recipients.each do |person_id|
      person = Person.find(person_id)
      email = person.the_email
      emails << email unless email.nil? || email.empty?
    end

    ProwhyMailer.with(emails: emails, rca: rca, options: options, pdf: pdf_attached).report_email.deliver_now
  end

  # =================================== Test pour exporter les PDF des sous-rca (tool rca-lists)
  # def self.rca_report_send_mail(recipients, rca, options, all_pdf_attached)
  #   emails = []
  #   recipients.each do |person_id|
  #     person = Person.find(person_id)
  #     email = person.the_email
  #     emails << email unless email.nil? || email.empty?
  #   end

  #   ProwhyMailer.with(emails: emails, rca: rca, options: options, pdf_all: all_pdf_attached).report_email.deliver_now
  # end

  # Send Email to RCA responsible when an action is closed
  def self.task_closed_send_mail(task_id)
    task = Tools::Task.find(task_id)

    team_resps = task.rca.rca_responsibles
    emails = []
    team_resps.each do |team_resp|
      person = team_resp.person
      email = person.the_email
      emails << email unless email.nil? || email.empty?
    end

    ProwhyMailer.with(task: task, emails: emails, message: '').task_closed_email.deliver_now unless emails.empty?
  end
end
