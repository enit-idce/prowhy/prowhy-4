require 'doorkeeper/grape/helpers'
require 'devise'

module ProwhyApi
  module V1
    module ApiMeth
      class ApiListContents < V1::Api
        resource :list_contents do
          desc 'Return List content for custom_field desc #id.'
          params do
            requires :id, type: Integer, desc: 'CustomFieldDesc ID.'
            # use :locale
          end
          route_param :id do
            get do
              custom_field_desc = Meth::CustomFieldDesc.find(params[:id])

              raise ArgumentError, 'Invalid custom_field_desc ID.' unless custom_field_desc

              raise ArgumentError, "Custom_field_desc #{custom_field_desc.name} is not a list." unless custom_field_desc.custom_field_type.field_internal == 'select'

              @list_contents = custom_field_desc.list_contents

              present @list_contents, with: Entities::ApiMeth::ListContent
            end
          end
        end
      end
    end
  end
end
