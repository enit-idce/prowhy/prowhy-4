class CreateToolsDecisionMats < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_decision_mats do |t|
      t.references :rca, null: false, foreign_key: true
      t.string :name
      t.string :entry_class
      t.integer :tool_reference
      t.integer :tool_num, default: 1

      t.timestamps
    end
  end
end
