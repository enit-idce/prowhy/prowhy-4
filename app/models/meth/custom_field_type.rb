require_dependency 'meth'

module Meth
  class CustomFieldType < ApplicationRecord
    include MethConcern

    translates :field_name, fallback: :any

    has_many :custom_field_descs

    validates :field_name, presence: true
    # validates :field_name, uniqueness: { case_sensitive: true }

    validates :field_internal, presence: true
    validates :field_internal, uniqueness: { case_sensitive: true }

    def name
      field_name || 'no name'
    end
  end
end
