require 'rails_helper'

RSpec.describe Labels::CheckListElem, type: :model do
  subject(:check_list_elem) do
    FactoryBot.create(:labels_check_list_elem)
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has many tasks' do
        assc = Labels::CheckListElem.reflect_on_association(:tools_tasks)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without task_detail_type' do
        check_list_elem.task_detail_type = ''
        expect(check_list_elem).to_not be_valid
      end

      it 'is valid with task_detail_type' do
        check_list_elem.task_detail_type = 'TaskTypeMachin'
        expect(check_list_elem).to be_valid
      end

      it 'is not valid without name' do
        check_list_elem.name = ''
        expect(check_list_elem).to_not be_valid
      end

      it 'is valid with name' do
        check_list_elem.name = 'Action de faire ceci'
        expect(check_list_elem).to be_valid
      end
    end
  end
end
