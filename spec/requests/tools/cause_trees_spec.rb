require 'rails_helper'

RSpec.describe '/tools/cause_trees', type: :request do
  let(:valid_attributes) { {rca_id: rca.id, tool_reference: 1, tool_num: 1} }
  let(:invalid_attributes) { {rca_id: nil, tool_reference: 1, tool_num: 1} }

  let(:rca) { FactoryBot.create(:rca, methodology: methodology, title: 'RCA1') }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:user) { FactoryBot.create(:user) }
  let(:tools_cause) { FactoryBot.create(:cause, rca: rca) }

  let(:cause_tree) { FactoryBot.create(:cause_tree, rca: rca) }

  context 'From logged user' do
    before :each do
      # binding.pry
      user.set_session(:active_step_tool, methodology.steps.first.step_tools.first.id)
      user.save!

      sign_in user
    end

    describe 'GET /index' do
      context 'without rca defined' do
        it 'renders a redirection response' do
          get '/tools/cause_trees'
          # redirection car rca non défini
          expect(response).to have_http_status(:redirect)
          expect(response).to redirect_to(rcas_path)
        end
      end

      context 'with rca and step_tool defined' do
        context 'without ishikawa id defined' do
          it 'renders a redirection response' do
            get '/tools/cause_trees', params: {rca: rca.id}
            # redirection car rca non défini
            expect(response).to have_http_status(:redirect)
            expect(response).to redirect_to(edit_rca_path(rca))
          end
        end
        context 'with cause tree head id defined' do
          it 'renders a successful response' do
            get '/tools/cause_trees', params: {id: cause_tree.id, rca: rca.id}
            expect(response).to be_successful
          end
        end
      end
    end

    describe 'GET /cause_trees_js_tree_data' do
      context 'with cause tree defined' do
        it 'return cause tree datas in Json format' do
          Tools::CauseLink.create!(cause: tools_cause, cause_tool: cause_tree)
          Tools::Cause.create!(rca: rca, parent: tools_cause, tool_reference: cause_tree.tool_reference, name: 'cause2')
          headers = { 'ACCEPT' => 'application/json' }
          get tools_cause_trees_js_tree_data_path, params: {cause_tree: cause_tree.id}, headers: headers
          expect(response.content_type).to eq('application/json; charset=utf-8')

          # vérification du contenu Json
          cause_presenter = Tools::CausePresenter.new(cause_tree.cause, nil)
          cause_presenter.cause_subtree_datas

          # expect(response.body).to eq [cause_presenter.cause_subtree_datas]
          expect(JSON.parse(response.body)).to eq JSON.parse([cause_presenter.cause_subtree_datas].to_json)
        end
      end

      context 'without ishikawa defined' do
        it 'return no datas in Json format' do
          headers = { 'ACCEPT' => 'application/json' }
          get tools_cause_trees_js_tree_data_path, params: {}, headers: headers
          expect(response.content_type).to eq('application/json; charset=utf-8')
          expect(response.body).to eq '[{"text":"no tree"}]'
        end
      end
    end
  end
end
