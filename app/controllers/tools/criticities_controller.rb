module Tools
  class CriticitiesController < ApplicationController
    layout 'processus'
    before_action :set_tools_criticity, only: [:show, :edit, :update, :destroy]
    before_action :init_rca, only: [:index, :new, :create]

    # GET /tools/criticities
    # GET /tools/criticities.json
    def index
      # authorize Criticity
      skip_authorization

      if @rca.nil? || current_user.get_session(:active_step_tool).nil?
        redirect_to rcas_path
      else
        authorize @rca, :show?, policy_class: Tools::ToolsPolicy
        # RcaPolicy.new(current_user, @rca).rca_show_step_with_raise?(@rca.methodology.step_info)

        @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
        @tools_criticities = @rca.tools_criticities.where(tool_reference: @step_tool.tool_reference,
                                                          tool_num: @step_tool.tool_num)
                                 .includes(criticity_values: [:labels_value, labels_criterium: [:values]])
      end
    end

    # GET /tools/criticities/1
    # GET /tools/criticities/1.json
    # def show
    # end

    # GET /tools/criticities/new
    def new
      @tools_criticity = Tools::Criticity.new(tools_criticity_params)
      authorize @tools_criticity
    end

    # GET /tools/criticities/1/edit
    # def edit
    # end

    # POST /tools/criticities
    # POST /tools/criticities.json
    def create
      @tools_criticity = Tools::Criticity.new(tools_criticity_params)
      authorize @tools_criticity

      respond_to do |format|
        if @tools_criticity.save
          @tools_criticity.create_criticity_values

          format.html do
            redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca),
                        notice: 'Criticity was successfully created.'
          end
          format.json { render :show, status: :created, location: @tools_criticity }
        else
          format.html { render :new }
          format.json { render json: @tools_criticity.errors, status: :unprocessable_entity }
          format.js { render :new }
        end
      end
    end

    # PATCH/PUT /tools/criticities/1
    # PATCH/PUT /tools/criticities/1.json
    def update
      if @tools_criticity.update(tools_criticity_params)
        calculator = Dentaku::Calculator.new
        # @tools_criticity.labels_config.criteria.each do |crit|
        #   calculator.store(crit.internal_name => 2)
        # end
        @tools_criticity.criticity_values.each do |crit_val|
          val = crit_val.labels_value ? crit_val.labels_value.value : 0
          calculator.store(crit_val.labels_criterium.internal_name => val)
        end

        begin
          result = calculator.evaluate!(@tools_criticity.labels_config.formula)

          @tools_criticity.update(value: result)
          respond_to do |format|
            format.js { render 'calculate', locals: {tools_criticity: @tools_criticity} }
          end
        rescue StandardError => e
          error_message = " Error : #{e.message}"
          respond_to do |format|
            format.js { render js: "alert('#{error_message}');" }
          end
        end

      else
        respond_to do |format|
          # if @tools_criticity.update(tools_criticity_params)
          #   format.html { redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca), notice: 'Criticity was successfully updated.' }
          #   format.json { render :show, status: :ok, location: @tools_criticity }
          # else
          format.html do
            redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca),
                        notice: 'Error update.'
          end
          format.json { render json: @tools_criticity.errors, status: :unprocessable_entity }
          # end
        end
      end
    end

    # DELETE /tools/criticities/1
    # DELETE /tools/criticities/1.json
    def destroy
      @tools_criticity.destroy
      respond_to do |format|
        format.html { redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca), notice: 'Criticity was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    # PATCH
    # def calculate
    #   calculator = Dentaku::Calculator.new
    #   # @tools_criticity.labels_config.criteria.each do |crit|
    #   #   calculator.store(crit.internal_name => 2)
    #   # end
    #   @tools_criticity.criticity_values.each do |crit_val|
    #     val = crit_val.labels_value ? crit_val.labels_value.value : 0
    #     calculator.store(crit_val.labels_criterium.internal_name => val)
    #   end

    #   begin
    #     result = calculator.evaluate!(@tools_criticity.labels_config.formula)
    #     @tools_criticity.update(value: result)
    #     respond_to do |format|
    #       format.js { render 'calculate', locals: {tools_criticity: @tools_criticity} }
    #     end
    #   rescue StandardError => e
    #     error_message = " Error : #{e.message}"
    #     respond_to do |format|
    #       format.js { render js: "alert('#{error_message}');" }
    #     end
    #   end
    # end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_criticity
      @tools_criticity = Tools::Criticity.find(params[:id])
      @rca = @tools_criticity.rca

      # authorize @rca, :show?
      authorize @tools_criticity
    end

    # Only allow a list of trusted parameters through.
    def tools_criticity_params
      params.require(:tools_criticity).permit(:rca_id, :tool_reference, :tool_num, :labels_config_id, :value,
                                              criticity_values_attributes: [:id, :labels_criterium_id, :labels_value_id])
    end
  end
end
