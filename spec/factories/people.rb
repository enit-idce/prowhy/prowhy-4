FactoryBot.define do
  factory :person do
    sequence(:firstname) { |n| "Nom#{n}" }
    sequence(:lastname) { |n| "Prenom#{n}" }
    sequence(:email) { |n| "personemail-#{n}@prowhy.org" }
  end
end
