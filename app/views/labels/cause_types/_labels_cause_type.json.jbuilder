json.extract! labels_cause_type, :id, :name, :pos, :created_at, :updated_at
json.url labels_cause_type_url(labels_cause_type, format: :json)
