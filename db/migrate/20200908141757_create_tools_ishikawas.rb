class CreateToolsIshikawas < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_ishikawas do |t|
      t.references :rca, null: false, foreign_key: true
      # t.references :tools_cause, foreign_key: true # null: false,
      t.integer :tool_reference

      t.timestamps
    end
  end
end
