# binding.pry
require 'rails_helper'

shared_examples_for AncestryConcern, type: 'model' do
  # subject(:obj) do
  #   obj = if defined?(described_obj)
  #           described_obj
  #         else
  #           described_class.new
  #         end
  #   obj.save(validate: false)
  #   obj
  # end
  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to rca' do
        assc = described_class.reflect_on_association(:rca)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end

  describe 'Show name with ancestry' do
    context 'object is tree head' do
      it 'shows name with level as &...' do
        expect(described_obj.root.name_level).to eq(described_obj.root.name)
      end

      it 'shows name with level as =>' do
        expect(described_obj.root.name_level(1)).to eq(described_obj.root.name)
      end
    end
    context 'object is not tree head' do
      it 'shows name with level as &...' do
        expect(described_obj.name_level).to eq(('&#8618; '*described_obj.depth)+described_obj.name)
      end

      it 'shows name with level as =>' do
        expect(described_obj.name_level(1)).to eq(('==> '*described_obj.depth)+described_obj.name)
      end
    end
  end

  it 'get causes racines' do
    o1 = FactoryBot.create(name_facotory.intern, rca: rca, parent: described_obj)
    o2 = FactoryBot.create(name_facotory.intern, rca: rca, parent: described_obj)
    o3 = FactoryBot.create(name_facotory.intern, rca: rca, parent: o1)
    o4 = FactoryBot.create(name_facotory.intern, rca: rca, parent: o2)

    expect(described_obj.root_descendants).to contain_exactly(o3, o4)
  end

  # Function arrange_as_array tested in task & cause queries.
end

describe Tools::Cause do
  it_behaves_like AncestryConcern do
    subject(:described_obj) do
      FactoryBot.create(:cause, rca: rca, parent: head_cause)
    end

    let(:head_cause) { FactoryBot.create(:cause, rca: rca) }
    let(:methodology) { FactoryBot.create(:methodology) }
    let(:rca) { FactoryBot.create(:rca, methodology: methodology) }
    let(:name_facotory) { 'cause' }
  end
end

describe Tools::Task do
  it_behaves_like AncestryConcern do
    subject(:described_obj) do
      FactoryBot.create(:task, rca: rca, tools_cause: cause, parent: head_task)
    end

    let(:head_task) { FactoryBot.create(:task, rca: rca, tools_cause: cause) }
    let(:methodology) { FactoryBot.create(:methodology) }
    let(:rca) { FactoryBot.create(:rca, methodology: methodology) }
    let(:cause) { FactoryBot.create(:cause, rca: rca) }
    let(:name_facotory) { 'task' }
  end
end
