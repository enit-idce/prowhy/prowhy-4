class CreateMethCustomFieldDescs < ActiveRecord::Migration[6.0]
  def change
    create_table :meth_custom_field_descs do |t|
      t.references :custom_field_type, null: false, foreign_key: { to_table: :meth_custom_field_types }
      t.string :internal_name
      t.string :name

      t.timestamps
    end
  end
end
