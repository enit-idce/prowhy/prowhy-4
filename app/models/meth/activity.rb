require_dependency 'meth'

module Meth
  class Activity < ApplicationRecord
    include MethConcern

    translates :name, fallback: :any

    has_many :tools, dependent: :destroy # , class_name: 'Meth::Tool'

    validates :name, presence: true

    def self.find_or_create_infos
      return Activity.where(name_fr: 'Infos').first || Activity.create!(name_fr: 'Infos', name_en: 'Infos', pos: Activity.count)
    end
  end
end
