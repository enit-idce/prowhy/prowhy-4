require 'rails_helper'

RSpec.describe Labels::Criterium, type: :model do
  subject(:criterium) do
    # Labels::Criterium.create!(name: 'Efficacity', threshold: 1)
    FactoryBot.create(:labels_criterium, name: 'Efficacity')
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has many tools_crit_dec_mats' do
        assc = Labels::Criterium.reflect_on_association(:tools_crit_dec_mats)
        expect(assc.macro).to eq :has_many
      end
      it 'has many tools_decision_mats (through tools_crit_dec_mats)' do
        assc = Labels::Criterium.reflect_on_association(:tools_decision_mats)
        expect(assc.macro).to eq :has_many
      end
      it 'has many values' do
        assc = Labels::Criterium.reflect_on_association(:values)
        expect(assc.macro).to eq :has_many
      end
      it 'belongs to config' do
        assc = Labels::Criterium.reflect_on_association(:config)
        expect(assc.macro).to eq :belongs_to
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        criterium.name = ''
        expect(criterium).to_not be_valid
      end

      it 'is valid with name' do
        criterium.name = 'Cost'
        expect(criterium).to be_valid
      end

      it 'is not valid with invalid internal name' do
        criterium.internal_name = 'Truc machin bidule'
        expect(criterium).to_not be_valid
        criterium.internal_name = 'CR A'
        expect(criterium).to_not be_valid
        criterium.internal_name = 'CR_AZERTY'
        expect(criterium).to_not be_valid
      end
      it 'is not valid with invalid internal name' do
        criterium.internal_name = 'CR_AZ'
        expect(criterium).to be_valid
      end
    end
  end

  describe 'criticity_name' do
    it 'show name and internal name' do
      criterium.internal_name = 'CR_AZ'
      expect(criterium.criticity_name).to eq 'Efficacity (CR_AZ)'
    end
  end
end
