FactoryBot.define do
  factory :labels_criterium, class: Labels::Criterium do
    sequence(:name) { |n| "Criterium #{n}" }
    threshold { 1 }
  end
end
