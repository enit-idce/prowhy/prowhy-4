module Tools
  # < ToolsPolicy
  class TeamPolicy < ToolsPolicy
    attr_reader :user, :record, :rca

    # def initialize(user, record)
    #   @user = user
    #   @record = record
    #   @rca = record.rca
    # end

    # def create?
    #   edit? # edit from RcaPolicy
    #   # user.has_any_role?(:admin, :super_admin, :manager) || rca.people.include?(user.person)
    # end

    # class Scope
    #   def initialize(user, scope)
    #     @user  = user
    #     @scope = scope
    #   end

    #   def resolve
    #     # if user.has_any_role?(:admin, :super_admin, :manager)
    #     #   scope.all
    #     # else
    #     #   scope.where(person_id: user.person_id)
    #     # end
    #     scope.all
    #   end

    #   private

    #   attr_reader :user, :scope
    # end

    def destroy?
      # binding.pry
      return false unless super

      return false if record.profil.profil_name == 'resp_rca' && rca&.rca_responsibles&.size&.<=(1)

      return true
    end
  end
end
