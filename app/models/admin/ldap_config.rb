require_dependency 'admin'
require 'net/ldap'

module Admin
  class LdapConfig < ApplicationRecord
    has_many :users

    validates :name, presence: true
    validates :port, presence: true
    validates :host, presence: true
    validates :base_dn, presence: true

    validates :attr_uid, presence: true
    validates :attr_email, presence: true, format: { with: /[A-Za-z0-9._%+-]/ }
    # validates :attr_lastname, presence: true

    # Connexion au LDap
    # Si connexion ok : retourne l'objet ldap
    # Sinon : raise l'erreur de connexion
    def connect
      begin
        ldap = initialize_ldap_connection # test_connection
        ldap.open {}
        puts 'Connexion reussie'.green
      rescue StandardError => e
        puts 'Connexion impossible : '.blue + e.message
        ldap = nil
        raise(e.message)
      end
      return ldap
    end

    def connect_test
      begin
        ldap = test_connection
        puts 'Connexion reussie'.green
      rescue StandardError => e
        puts 'Connexion impossible : '.blue + e.message
        ldap = nil
        raise(e.message)
      end
      return ldap
    end

    def connect_and_auth(login, password)
      # puts 'Test Ldap connexion for '
      # puts login.yellow
      ldap = initialize_ldap_connection

      return ldap.bind_as(
        base: base_dn,
        filter: "(#{attr_uid}=#{login})",
        password: password
      )
    end

    # Get Ldap user by login
    def connect_and_search_by_uid(uid)
      ldap = connect
      return nil unless ldap

      filter = Net::LDAP::Filter.eq(attr_uid, uid)
      return ldap.search(base: base_dn, filter: filter).first
    end

    # Get Ldap users by attribut
    def connect_and_search_by_att(params)
      ldap = connect
      return nil unless ldap

      filter = nil
      params[:uid] = params[:username] unless params[:uid]
      params.each_key do |k|
        # Parcours des parametres pour la recherche (filtre) dans le LDAP
        next if params[k].nil? || params[k] == ''

        begin
          ldap_attribute = send("attr_#{k}")

          f = Net::LDAP::Filter.eq(ldap_attribute, "#{params[k]}*")
          filter = filter.nil? ? f : filter & f
        rescue StandardError
          puts "#{k} is not an attribute !".red
        end
      end

      ldap_users = []
      if filter.nil?
        message = 'Entrez au moins une valeur (login, nom, prenom) pour effectuer une recherche dans le LDAP.'
        raise message
        # puts message.yellow
      else
        # puts filter
        # Recherche dans le LDAP
        ldap.search(base: base_dn, filter: filter) do |entry|
          ldap_users << entry
          puts "    #{entry.send(attr_uid).first}"
        end

        # Tri de la liste
        if ldap_users.empty?
          message = 'Aucune entree trouvee'.red
          raise message
          # puts message
        elsif !attr_lastname.nil? && attr_lastname != ''
          begin
            # puts 'TRI PAR NOM'.red
            ldap_users.sort! { |a, b| a.send(attr_lastname).first <=> b.send(attr_lastname).first }
          rescue StandardError
            # puts 'SEARCH : TRI PAR NOM IMPOSSIBLE => Tri par UID'.red
            ldap_users.sort! { |a, b| a.send(attr_uid).first <=> b.send(attr_uid).first }
          end
        end
      end

      # ldap_users.each do |u|
      #   puts u.send(attr_lastname).first + ' ' + u.send(attr_firstname).first
      # end

      return ldap_users
    end

    # Get user attribute
    def att(ldap_user, attrib)
      return ldap_user.send(send("attr_#{attrib}")).first # first car les ldap attributs sont des tableaux
    rescue StandardError
      return '-'
    end

    # Private functions for connection testing
    private

    def with_timeout(&_block)
      timeout = 2
      Timeout.timeout(timeout) do
        return yield
      end
    rescue Timeout::Error => e
      raise(e.message)
    end

    def initialize_ldap_connection
      options = if admin_login && admin_password && admin_login != ''
                  { host: host,
                    port: port,
                    auth: { method: :simple,
                            username: admin_login,
                            password: admin_password
                          }
                  }
                else
                  { host: host,
                    port: port
                  }
                end
      Net::LDAP.new options
    end

    def initialize_ldap_host
      # puts "Initialize Host : #{host} #{port}".yellow
      ldap = Net::LDAP.new
      ldap.host = host
      ldap.port = port
      return ldap
    end

    def initialize_ldap_auth(ldap)
      if ldap && admin_login && admin_password && admin_login !=''
        # puts "Initialize AUTH : #{admin_login}".yellow
        ldap.auth admin_login, admin_password
      end
      return ldap
    end

    # Test connection to LDAP
    # Return connection object (ldap_con) or raise connection error
    def test_connection
      with_timeout do
        ldap = initialize_ldap_host
        ldap.open {}
        puts 'Connexion (host+port) ok'.yellow

        if ldap && admin_login && admin_password && admin_login !=''
          ldap = initialize_ldap_auth(ldap)

          raise Net::LDAP::Error, 'Problème d authentification.' unless ldap.bind

          puts 'Authentification ok'.yellow
          return ldap
        end

        return ldap # Retour si pas d'authentification nécessaire
      end
    rescue Net::LDAP::Error => e
      raise(e.message)
    end

  end
end
