require 'rails_helper'

RSpec.describe Meth::Step, type: :model do
  subject(:step) do
    # Meth::Step.new(name: 'Step 1', methodology: methodology)
    FactoryBot.create(:meth_step, name: 'Step 1', methodology: methodology)
  end

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:activity) { Meth::Activity.create!(name: 'Activity 1') }
  let(:tool1) { Meth::Tool.create!(name: 'Tool 1', tool_name: 'Action', activity: activity) }
  let(:tool2) { Meth::Tool.create!(name: 'Tool 2', tool_name: 'Action', activity: activity) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to methodology' do
        assc = Meth::Step.reflect_on_association(:methodology)
        expect(assc.macro).to eq :belongs_to
      end

      it 'has many step_tools' do
        assc = Meth::Step.reflect_on_association(:step_tools)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools' do
        assc = Meth::Step.reflect_on_association(:tools)
        expect(assc.macro).to eq :has_many
      end

      it 'belongs to step_role' do
        assc = Meth::Step.reflect_on_association(:step_role)
        expect(assc.macro).to eq :belongs_to
      end

      # it 'belongs to step_tool_img' do
      #   assc = Meth::Step.reflect_on_association(:step_tool_img)
      #   expect(assc.macro).to eq :belongs_to
      # end

      # it 'belongs to step_tool_doc' do
      #   assc = Meth::Step.reflect_on_association(:step_tool_doc)
      #   expect(assc.macro).to eq :belongs_to
      # end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        step.name = ''
        expect(step).to_not be_valid
      end

      it 'is valid with name' do
        expect(step).to be_valid
      end

      it 'is not valid if step_type is not in (1,2,3)' do
        step.step_type = 4
        expect(step).to_not be_valid
      end

      it 'is valid with step_type in 1, 2, 3' do
        step.step_type = 3
        expect(step).to be_valid
      end
    end
  end

  describe 'Shows name with form' do
    it 'name_show' do
      expect(step.name_show).to eq 'Method 1 / Step 1'
    end
    it 'name_show_html' do
      expect(step.name_show_html).to eq '<b>Method 1</b> / Step 1'
    end
  end

  describe 'Adds a tool to step' do
    it 'add_tool' do
      step.add_tool(tool1)
      expect(step.reload.step_tools.size).to eq 1
      expect(step.step_tools.first.tool).to eq tool1
      expect(step.tools.size).to eq 1
      expect(step.tools.first).to eq tool1
      expect(step.tools).to contain_exactly(tool1)
      # Le contenu peut aussi s'écrire comme ça (avec *) : *Meth::Tool.where(name: 'Tool 1'))

      step.add_tool(tool2)
      expect(step.reload.tools).to contain_exactly(tool1, tool2)
    end
  end

  describe 'Shows step_tool name with form' do
    before :each do
      step.add_tool(tool1)
      @step_tool = step.step_tools.first
    end

    it 'name_show_all' do
      expect(@step_tool.name_show_all).to eq 'Method 1 / Step 1 / Tool 1'
    end

    it 'name_show' do
      expect(@step_tool.name_show).to eq 'Step 1 / Tool 1'
    end

    it 'name_show_short' do
      expect(@step_tool.name_show_short).to eq 'Tool 1'
      @step_tool.name = 'StepToolName'
      expect(@step_tool.name_show_short).to eq 'StepToolName'
    end

    it 'name_show_activity' do
      expect(@step_tool.name_show_activity).to eq 'Activity 1 => Tool 1'
    end
  end

  describe 'Copy' do
    it 'copies a step' do
      step.add_tool(tool1)
      step.add_tool(tool2)

      step2 = step.copy(methodology)

      expect(step2.step_tools.size).to eq 2
      expect(step2.tools.size).to eq 2

      expect(step2.tools.first.name).to eq 'Tool 1'
      expect(step2.tools.second.name).to eq 'Tool 2'
    end
  end
end
