module Tools
  class IndicatorMeasurePolicy < ToolsPolicy
    attr_reader :user, :record, :rca

    def initialize(user, record)
      @user = user
      @record = record
      # binding.pry
      @rca = record.class == Class ? nil : record.indicator&.rca
    end

    def indicator_measure_up?
      update?
    end

    def indicator_measure_down?
      update?
    end
  end
end
