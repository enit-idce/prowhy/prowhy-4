module Labels
  class ConfigsController < ApplicationController
    include AdminConcern
    before_action :set_labels_config, only: [:show, :edit, :update, :destroy, :add_criteria, :add_criteria_valid, :formula_test]

    # GET /labels/configs
    # GET /labels/configs.json
    def index
      authorize Config
      @labels_configs = Labels::Config.all.includes([:file_attachment])
    end

    # GET /labels/configs/1
    # GET /labels/configs/1.json
    # def show
    # end

    # GET /labels/configs/new
    def new
      @labels_config = Labels::Config.new
      authorize @labels_config
    end

    # GET /labels/configs/1/edit
    def edit
      # @labels_config.includes(:criteria)
    end

    # POST /labels/configs
    # POST /labels/configs.json
    def create
      @labels_config = Labels::Config.new(labels_config_params)
      authorize @labels_config

      respond_to do |format|
        if @labels_config.save
          format.html { redirect_to edit_labels_config_path(@labels_config), notice: 'Config was successfully created.' }
          format.json { render :show, status: :created, location: @labels_config }
        else
          format.html { render :new }
          format.json { render json: @labels_config.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /labels/configs/1
    # PATCH/PUT /labels/configs/1.json
    def update
      respond_to do |format|
        if @labels_config.update(labels_config_params)
          format.html { redirect_to labels_configs_url, notice: 'Config was successfully updated.' }
          format.json { render :show, status: :ok, location: @labels_config }
        else
          format.html { render :edit }
          format.json { render json: @labels_config.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /labels/configs/1
    # DELETE /labels/configs/1.json
    def destroy
      @labels_config.destroy
      respond_to do |format|
        format.html { redirect_to labels_configs_url, notice: 'Config was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    # GET
    def add_criteria
      criteria = Labels::Criterium.type_criticity_dispo(@labels_config.id)
      render 'add_criteria', locals: {labels_config: @labels_config, criteria: criteria}
    end

    # PATCH
    def add_criteria_valid
      criteria = Labels::Criterium.type_criticity_dispo(@labels_config.id)
      criteria.each do |crit|
        crit_in_config =  @labels_config.criteria.include?(crit)
        # Ajout / Supression de critères
        if crit_in_config && params[:dec_mat_crit][crit.id.to_s] == 'false'
          # Delete criterium
          crit.update(config_id: nil)
        elsif !crit_in_config && params[:dec_mat_crit][crit.id.to_s] == 'true'
          # Add criterium
          # @labels_config.add_criterium(crit, weight)
          crit.update(config_id: @labels_config.id)
        end
      end

      respond_to do |format|
        format.html { redirect_to edit_labels_config_path(@labels_config) }
      end
    end

    # GET
    def formula_test
      formula_test = params[:formula]

      calculator = Dentaku::Calculator.new
      @labels_config.criteria.each do |crit|
        calculator.store(crit.internal_name => 2)
      end

      message = "Formule : #{formula_test}"
      begin
        result = calculator.evaluate!(formula_test)
        message += " => Ok ! Result (variables replaced by 2) = #{result}"
      rescue StandardError => e
        message += " ===> Error : #{e.message}"
      end

      # binding.pry

      respond_to do |format|
        format.js { render js: "alert('#{message}');" }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_labels_config
      @labels_config = Labels::Config.find(params[:id])
      authorize @labels_config
    end

    # Only allow a list of trusted parameters through.
    def labels_config_params
      params.require(:labels_config).permit(:name, :name_fr, :name_en, :formula, :usable, :file)
    end
  end
end
