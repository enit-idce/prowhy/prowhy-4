require_dependency 'meth/custom_field_desc'

class CustomField < ApplicationRecord
  belongs_to :rca
  accepts_nested_attributes_for :rca

  has_many :meth_custom_field_descs, -> { Meth::CustomFieldDesc.all }

  extend Finalize

  def self.finalize
    # puts '============================================> ADD Rich text fields to CustomField class'.yellow

    custom_fields_rich_texts = Meth::CustomFieldDesc.includes(:custom_field_type).where(meth_custom_field_types: {field_internal: 'rich_text'})
    custom_fields_tags = Meth::CustomFieldDesc.includes(:custom_field_type).where(meth_custom_field_types: {field_internal: 'tag'})
    custom_fields_trees = Meth::CustomFieldDesc.includes(:custom_field_type).where(meth_custom_field_types: {field_internal: 'tree'})
    custom_fields_selects = Meth::CustomFieldDesc.includes(:custom_field_type).where(meth_custom_field_types: {field_internal: 'select'})

    class_eval do
      custom_fields_rich_texts.each do |cf|
        has_rich_text cf.internal_name
        # puts "ADD RICH TEXT FOR #{cf.internal_name}".red
      end
      custom_fields_tags.each do |cf|
        acts_as_taggable_on cf.internal_name
        # puts "ADD TAG FOR #{cf.internal_name}".red
      end
      custom_fields_trees.each do |cf|
        belongs_to "#{cf.internal_name}_value".intern, class_name: 'Meth::TreeContent', foreign_key: cf.internal_name, optional: true
      end
      custom_fields_selects.each do |cf|
        belongs_to "#{cf.internal_name}_value".intern, class_name: 'Meth::ListContent', foreign_key: cf.internal_name, optional: true
      end

      # binding.pry
    end
  rescue StandardError
    puts 'INFO: error in Custom Field finalize.'.blue
    # binding.pry
  end
end
