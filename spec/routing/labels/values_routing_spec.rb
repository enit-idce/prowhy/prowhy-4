require 'rails_helper'

RSpec.describe Labels::ValuesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/labels/values').to route_to('labels/values#index')
    end

    it 'routes to #new' do
      expect(get: '/labels/values/new').to route_to('labels/values#new')
    end

    it 'routes to #show' do
      expect(get: '/labels/values/1').to route_to('labels/values#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/labels/values/1/edit').to route_to('labels/values#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/labels/values').to route_to('labels/values#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/labels/values/1').to route_to('labels/values#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/labels/values/1').to route_to('labels/values#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/labels/values/1').to route_to('labels/values#destroy', id: '1')
    end
  end
end
