require 'rails_helper'

RSpec.feature 'Decision mat crud', type: :feature do
  before :all do
    load "#{Rails.root}/db/seed_tests.rb"
  end

  before :each do
    @methodology = Meth::Methodology.rca_methodologies.where(name_fr: 'Méthode 8D').first
    user = User.first
    user.usersession[:active_step_tool] = @methodology.steps.first.step_tools.first.id
    user.save!
    sign_in user

    @rca = Rca.create!(title: 'Mon premier Rca', methodology: @methodology)
    @rca.rca_creation_init(user)
  end

  after :all do
    load "#{Rails.root}/db/seed_empty.rb"
  end

  scenario 'User creates a decision matrix', js: true do
    visit edit_rca_path(@rca)
    # step_tool = Meth::StepTool.where(name_fr: 'Matrice Décision').or(Meth::StepTool.where(name_en: 'Matrice Décision')).first
    step_tool = Meth::StepTool.joins(:tool).where(meth_tools: {tool_name: 'DecisionMat'}).first
    # Open D5 menu
    find("a[id='header-step-#{step_tool.step.id}']").click

    # Open Decision matrix Tool
    find("a[href='#{meth_step_tool_show_path(id: step_tool.id, rca: @rca.id)}']").click

    # Create new matrix
    click_link 'New'
    fill_in 'Name', with: 'Ma première matrice'

    # click_button 'Create Decision mat'
    find('input[name="commit"]').click
    click_button 'Valid' # Add entries
    # find('input[name="commit"]').click

    expect(page).to have_text(I18n.t('tools.decision_mats.valid_entries.message_succes')) # ('Entries were successfully updated')
    expect(page).to have_current_path(tools_decision_mats_path(rca: @rca.id, step_tool: step_tool.id))
  end

  context 'With an existing decision matrix' do
    let(:step_tool) { Meth::StepTool.joins(:tool).where(meth_tools: {tool_name: 'DecisionMat'}).first }
    let(:dec_mat)   { Tools::DecisionMat.create!(name: 'Matrix 1', rca: @rca, tool_reference: 1, tool_num: 1, entry_class: 'Tools::Task') }
    let(:task_init) { FactoryBot.create('task', name: 'Task Init', rca: @rca) }
    let(:crit_init) { Labels::Criterium.create!(name: 'Mon critere') }

    before :each do
      dec_mat.add_criterium(crit_init)
      dec_mat.add_entry(task_init)
      visit meth_step_tool_show_path(id: step_tool.id, rca: @rca.id)
    end

    scenario 'User add and del entries for a decision matrix', js: true do
      task1 = FactoryBot.create('task', rca: @rca, task_detail_type: 'Tools::TaskCorrective')
      task2 = FactoryBot.create('task', rca: @rca, task_detail_type: 'Tools::TaskCorrective')

      # Add entries to decision matrix
      find("a[href='#{tools_decision_mat_add_entry_path(id: dec_mat.id)}']").click
      find("label[for='_dec_mat_entry_#{task1.id}_true']").click
      find("label[for='_dec_mat_entry_#{task2.id}_true']").click
      expect {
        click_button 'Valid'
      }.to change(Tools::DecMatLink.where(decision_mat: dec_mat), :count).by(2)
      # Del entry
      find("a[href='#{tools_decision_mat_add_entry_path(id: dec_mat.id)}']").click
      find("label[for='_dec_mat_entry_#{task1.id}_false']").click
      expect {
        click_button 'Valid'
      }.to change(Tools::DecMatLink.where(decision_mat: dec_mat), :count).by(-1)

      # save_and_open_page
      expect(page).to have_css("th[class='mat-entry first-2']", count: 2)
    end

    scenario 'User add and del criteria for a decision matrix', js: true do
      crit1 = Labels::Criterium.create!(name: 'New critere')

      # Add criterium to decision matrix
      find("a[href='#{tools_decision_mat_add_criteria_path(id: dec_mat.id)}']").click
      find("label[for='_dec_mat_crit_#{crit1.id}_true']").click
      expect {
        click_button 'Valid'
      }.to change(Tools::CritDecMat.where(decision_mat: dec_mat), :count).by(1)
      # Del criterium
      find("a[href='#{tools_decision_mat_add_criteria_path(id: dec_mat.id)}']").click
      find("label[for='_dec_mat_crit_#{crit1.id}_false']").click
      expect {
        click_button 'Valid'
      }.to change(Tools::CritDecMat.where(decision_mat: dec_mat), :count).by(-1)
    end

    scenario 'User change a value and save', js: true do
      dmv = dec_mat.crit_dec_mats.first.dec_mat_vals.first

      fill_in("[dec_mat_val][#{dmv.id}]", with: '3').send_keys(:tab)
      # expect(find("input[value='Enregistrer'")).to have_css('btn-warning')
      expect(page.find('#tool-dec-mat-'+dec_mat.id.to_s)).to have_css('.btn-warning')
      # click_button 'Enregistrer'
      find('input[name="commit"]').click

      expect(dmv.reload.value).to eq 3
      expect(page).to have_text(I18n.t('tools.decision_mats.save_values.message_succes',
                                       decision_mat: Tools::DecisionMat.model_name.human)) # ('Decison Matrix values were successfully updated')
      # save_and_open_page
    end
  end
end
