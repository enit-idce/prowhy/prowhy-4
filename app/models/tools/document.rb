require_dependency 'tools'

module Tools
  class Document < ApplicationRecord
    include ToolConcern

    has_one_attached :file
    validates :name, presence: true
    validate :acceptable_document

    def self.create_default!(_options, _step_tool)
      []
    end

    def self.search_default(rca, _tool_name, step_tool)
      step_tool.tool_options ||= {}

      options_for_search = if step_tool.tool_reference != 0
                             step_tool.tool_options.merge(tool_reference: step_tool.tool_reference,
                                                          tool_num: step_tool.tool_num)
                           else
                             step_tool.tool_options
                           end

      # .merge(tool_reference: step_tool.tool_reference,
      # tool_num: step_tool.tool_num)
      # binding.pry
      return rca.tools_documents.where(options_for_search).order(:tool_reference)
    end

    def self.create_search_multiple?
      return true
    end

    private

    def acceptable_document
      config = Admin::AdminConfig.first
      max_size = config&.options&.[]('image_max_size') || 1
      errors.add(:file, 'File must be present') unless file.attached?
      return unless file.attached?

      errors.add(:file, "File is too big (max = #{max_size.to_f} Mo)") unless file.byte_size <= max_size.to_f.megabyte
    end
  end
end
