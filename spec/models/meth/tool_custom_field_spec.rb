require 'rails_helper'

RSpec.describe Meth::ToolCustomField, type: :model do
  subject(:tool_custom_field) do
    FactoryBot.new(:meth_tool_custom_field) # Meth::ToolCustomField.new
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to step_tool' do
        assc = Meth::ToolCustomField.reflect_on_association(:step_tool)
        expect(assc.macro).to eq :belongs_to
      end

      it 'belongs to custom_field_desc' do
        assc = Meth::ToolCustomField.reflect_on_association(:custom_field_desc)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end
end
