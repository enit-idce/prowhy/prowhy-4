require 'rails_helper'

RSpec.describe Tools::Cause, type: :model do
  subject(:cause) do
    Tools::Cause.create!(rca: rca, tool_reference: 1, name: 'Cause 1')
  end

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      # it 'belongs to labels_cause_type' do
      #   assc = Tools::Cause.reflect_on_association(:labels_cause_type)
      #   expect(assc.macro).to eq :belongs_to
      # end
      it 'has many tools_cause_classes' do
        assc = Tools::Cause.reflect_on_association(:tools_cause_classes)
        expect(assc.macro).to eq :has_many
      end

      it 'has many labels_cause_types (through)' do
        assc = Tools::Cause.reflect_on_association(:labels_cause_types)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools_ishikawas (through)' do
        assc = Tools::Cause.reflect_on_association(:tools_ishikawas)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools_tasks' do
        assc = Tools::Cause.reflect_on_association(:tools_tasks)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        cause.name = ''
        expect(cause).to_not be_valid
      end

      it 'is valid with name' do
        cause.name = 'La cause'
        expect(cause).to be_valid
      end

      it 'is valid with status in_progress, validated, or invalidated' do
        cause.status = 'in_progress'
        expect(cause).to be_valid

        cause.status = 'validated'
        expect(cause).to be_valid

        cause.status = 'invalidated'
        expect(cause).to be_valid
      end

      it 'is not valid with invalid status' do
        expect { cause.status = 'toto' }.to raise_error(ArgumentError)
      end

      # it 'is valid with level in -, --, 0, +, ++' do
      it 'is valid with level in -, +, ++' do
        cause.level = '-'
        expect(cause).to be_valid

        # cause.level = '--'
        # expect(cause).to be_valid

        # cause.level = '0'
        # expect(cause).to be_valid

        cause.level = '+'
        expect(cause).to be_valid

        cause.level = '++'
        expect(cause).to be_valid
      end

      it 'is not valid with invalid level' do
        expect { cause.level = 'toto' }.to raise_error(ArgumentError)
      end
    end
  end

  describe 'Create and search model : tool class functions' do
    before :each do
      # cause.tree_head = true
      cause.save!
      options = { rca: cause.rca, tool_reference: 2 }
      @cause_created = Tools::Cause.create_default!(options, nil)
    end

    it 'create_default' do
      expect(@cause_created.name).to eq "#{cause.rca.reference} - Rca 1"
      expect(@cause_created.tool_reference).to eq 2
      expect(@cause_created.parent).to be nil
      expect(@cause_created.status_validated?).to be true
    end

    it 'search_default' do
      step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1)
      cause_founded = Tools::Cause.search_default(rca, 'tools_causes', step_tool)

      expect(cause_founded.name).to eq 'Cause 1'
      expect(cause_founded.tool_reference).to eq 1

      step_tool.tool_reference = 2
      cause_founded = Tools::Cause.search_default(rca, 'tools_causes', step_tool)

      expect(cause_founded.name).to eq "#{rca.reference} - Rca 1"
      expect(cause_founded.tool_reference).to eq 2
    end

    it 'returns 1 as tool_num' do
      expect(cause.tool_num).to eq 1
    end
  end

  describe 'Cause type for ishikawa' do
    before :each do
      # Create Labels
      Labels::CauseType.create!([
                                  {id: 1, name: 'Mesure'},
                                  {id: 2, name: 'Matière'},
                                  {id: 3, name: 'Méthode'},
                                  {id: 4, name: 'Main d\'oeuvre'}
                                ])
      # Create cause
      # cause.tree_head = true
      cause.save!
      options = { rca: cause.rca, tool_reference: 2 }
      step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 2, tool_num: 1)
      # Create Ishikawa
      @ishikawa = Tools::Ishikawa.create_default!(options, step_tool)
      @cause = @ishikawa.cause
    end

    it 'returns cause type id' do
      # ishi_cause_type_id
      @cause.add_ishi_cause_type(@ishikawa, 3)
      expect(@cause.ishi_cause_type_id(@ishikawa)).to eq 3
    end
    it 'returns cause type label' do
      @cause.add_ishi_cause_type(@ishikawa, 3)
      expect(@cause.ishi_cause_type(@ishikawa)).to eq Labels::CauseType.find(3)
    end
    it 'returns cause class object' do
      @cause.add_ishi_cause_type(@ishikawa, 3)
      expect(@cause.ishi_cause_class(@ishikawa)).to eq @cause.tools_cause_classes.first
    end

    describe 'set a type cause to cause' do
      # add_ishi_cause_type
      it 'set a new value (no value before)' do
        @cause.add_ishi_cause_type(@ishikawa, 3)
        expect(@cause.ishi_cause_type_id(@ishikawa)).to eq 3
      end
      it 'changes value with a valid cause type' do
        @cause.add_ishi_cause_type(@ishikawa, 3)
        expect(@cause.ishi_cause_type_id(@ishikawa)).to eq 3
        @cause.add_ishi_cause_type(@ishikawa, 1)
        expect(@cause.ishi_cause_type_id(@ishikawa)).to eq 1
      end
      it 'changes value with an invalid cause type' do
        @cause.add_ishi_cause_type(@ishikawa, 3)
        expect(@cause.ishi_cause_type_id(@ishikawa)).to eq 3
        @cause.add_ishi_cause_type(@ishikawa, 6)
        expect(@cause.ishi_cause_type_id(@ishikawa)).to eq 0 # delete cause type if invalid
      end
      it 'delete values (set 0)' do
        @cause.add_ishi_cause_type(@ishikawa, 3)
        expect(@cause.ishi_cause_type_id(@ishikawa)).to eq 3
        @cause.add_ishi_cause_type(@ishikawa, 0)
        expect(@cause.ishi_cause_type_id(@ishikawa)).to eq 0
      end
    end
  end

  describe 'Update cause status' do
    context 'Cause has no validation tasks' do
      it 'does not change the status' do
        cause.status_in_progress!
        cause.save!
        cause.update_cause_status_from_tasks
        expect(cause.reload.status).to eq 'in_progress'

        cause.status_validated!
        cause.save!
        cause.update_cause_status_from_tasks
        expect(cause.reload.status).to eq 'validated'

        cause.status_invalidated!
        cause.save!
        cause.update_cause_status_from_tasks
        expect(cause.reload.status).to eq 'invalidated'
      end
    end

    context 'Cause has validation tasks associated' do
      before :each do
        @task1 = FactoryBot.create(:task, rca: rca, name: 'Task 1', tools_cause: cause)
        @task1.task_detail = Tools::TaskValidation.create!
        @task1.save!

        @task2 = FactoryBot.create(:task, rca: rca, name: 'Task 2', tools_cause: cause)
        @task2.task_detail = Tools::TaskValidation.create!
        @task2.save!
      end

      it 'set in_progress status from tasks in_progress + in_progress' do
        cause.status_validated!
        cause.save!
        cause.update_cause_status_from_tasks
        expect(cause.reload.status).to eq 'in_progress'
      end

      it 'set in_progress status from tasks in_progress + invalidated' do
        @task1.task_detail.status_invalidated!
        @task1.task_detail.save!
        cause.update_cause_status_from_tasks
        expect(cause.reload.status).to eq 'in_progress'
      end

      it 'set invalidated status from tasks invalidated + invalidated' do
        @task1.task_detail.status_invalidated!
        @task1.task_detail.save!
        @task2.task_detail.status_invalidated!
        @task2.task_detail.save!
        cause.update_cause_status_from_tasks
        expect(cause.reload.status).to eq 'invalidated'
      end

      it 'set validated status from tasks in_progress + validated' do
        @task1.task_detail.status_in_progress!
        @task1.task_detail.save!
        @task2.task_detail.status_validated!
        @task2.task_detail.save!
        cause.update_cause_status_from_tasks
        expect(cause.reload.status).to eq 'validated'
      end

      it 'set validated status from tasks invalidated + validated' do
        @task1.task_detail.status_invalidated!
        @task1.task_detail.save!
        @task2.task_detail.status_validated!
        @task2.task_detail.save!
        cause.update_cause_status_from_tasks
        expect(cause.reload.status).to eq 'validated'
      end

      it 'set validated status from tasks validated + validated' do
        @task1.task_detail.status_validated!
        @task1.task_detail.save!
        @task2.task_detail.status_validated!
        @task2.task_detail.save!
        cause.update_cause_status_from_tasks
        expect(cause.reload.status).to eq 'validated'
      end
    end
  end
end
