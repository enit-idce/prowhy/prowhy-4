module Tools
  module Tasks
    class TaskPlanOptions
      attr_accessor :step_tool

      def initialize(step_tool)
        @step_tool = step_tool
      end

      # def init_options
      #   task_options = init_task_options
      #   plan_options = init_plan_options(task_options)

      #   return task_options, plan_options
      # end

      def init_task_options
        return step_tool.tool_options[:task_options] || {}
      end

      def init_plan_options(task_options)
        plan_options = step_tool.tool_options[:plan_options] || {}
        # binding.pry

        # Default values (if not defined in tool_options)
        plan_options[:causes] ||= false
        plan_options[:rcas] ||= false
        plan_options[:type_plan] ||= 'rca' # 'user' / 'global'
        plan_options[:sub_type_plan] ||= 'normal' # 'normal' / 'rca-list'
        plan_options[:blocks] ||= false
        # plan_options[:blocks_column] ||= 'task_detail_type'
        plan_options[:blocks_column] = 'task_detail_type' if plan_options[:blocks_column].blank?
        plan_options[:efficacity] ||= false
        plan_options[:matrix] ||= false
        plan_options[:validation] ||= init_plan_option_validation(task_options)
        plan_options[:check_list] ||= false

        # binding.pry
        return plan_options
      end

      private

      def init_plan_option_validation(task_options)
        po_validation = (task_options[:task_detail_type].class == Array &&
                         task_options[:task_detail_type].include?('Tools::TaskValidation')) ||
                        (task_options[:task_detail_type].class == String &&
                         task_options[:task_detail_type] == 'Tools::TaskValidation')

        return po_validation
      end
    end
  end
end
