source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

# ruby '2.6.6'
ruby '2.7.5'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.1.4', '>= 6.1.4.4', '< 6.1.5'
# Use mysql as the database for Active Record
gem 'mysql2', '>= 0.4.4'
# Use Puma as the app server
gem 'puma', '~> 4.1'
# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 4.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

group :development, :test do
  # Find N+1 requests
  gem 'bullet'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Pry
  gem 'pry-rails'
  gem 'pry-nav'
end

group :development do
  # Find N+1 requests
  # gem 'bullet'
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :development, :test do
  gem 'rspec-rails', '~> 5.0.0'
end

group :test do
  # gem 'rspec-rails'
  gem 'factory_bot_rails', '~> 4.0'
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 3.39'
  # gem 'capybara-screenshot'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'launchy' # `save_and_open_page` in integration specs
  gem 'selenium-webdriver'
  gem 'webdrivers'

  gem 'simplecov', require: false, group: :test
  # gem 'database_cleaner-active_record'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# Check code quality
# gem 'rubocop' # , '~> 0.62.0', require: false
gem 'rubocop', '~> 0.92.0', require: false
gem 'rubycritic', require: false

# Internationalisation
gem 'i18n-js'
gem 'rails-i18n'
gem 'traco'

# Login / Droits d'accès
# gem 'cancancan'
gem 'devise'
gem 'net-ldap'
gem 'pundit'
gem 'rolify'

# Fontawesome-Rails
gem 'font_awesome5_rails'

# ActionText
gem 'actiontext', require: 'action_text'
gem 'image_processing', '~> 1.2'

# Divers*
gem 'acts-as-taggable-on' # , '~> 6.0'
gem 'ancestry'
gem 'best_in_place', git: "https://github.com/mmotherwell/best_in_place" # '~> 3.1.1'
gem 'chunky_png'
gem 'colorize'
gem 'dentaku'
gem 'jquery-ui-rails'
gem 'pagy', '~> 6.4'
gem 'simple_form'

# BarCode & QrCodes
gem 'barby'
gem 'rqrcode'

# Seed
gem 'seed_dump'

# Export pdf
gem 'wicked_pdf', '~> 2'
# gem 'wkhtmltopdf-binary'
gem 'wkhtmltopdf-binary-edge', '0.12.4.0'

# prod
gem 'passenger'

# cron job
gem 'whenever', require: false

# Datas
gem 'data_migrate'

# API
gem 'doorkeeper', '~> 5.4.0'
gem 'grape'
gem 'grape-entity'
# gem 'grape-pagy'

gem 'grape-swagger'
gem 'grape-swagger-entity'
gem 'rswag-api'
gem 'rswag-ui'
# gem 'wine_bouncer', github: 'antek-drzewiecki/wine_bouncer', ref: 'c82b88f73c7adc43a8e89ca73f7e18952ec30de4'

# Api pagination (with grape / pagy)
gem 'api-pagination'

# ODS Export
# Attention ! nécessite l'installation de libxml2-dev (et peut-être ruby-libxml => à priori non !)
# sudo apt-get update -y && sudo apt-get install -y libxml2-dev
gem 'libxml-ruby', '3.0'
gem 'rspreadsheet'

# Tracking users
gem 'ahoy_matey'
# gem 'audited', '~> 4.9'
gem 'authtrail'
# gem 'blazer'
gem 'chartkick'
gem 'groupdate'

# NCTool
# gem 'active_model_serializers'
