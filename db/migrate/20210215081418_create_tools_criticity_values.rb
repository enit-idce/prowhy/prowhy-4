class CreateToolsCriticityValues < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_criticity_values do |t|
      t.references :criticity, null: false, foreign_key: { to_table: :tools_criticities }
      t.references :labels_criterium, null: false, foreign_key: true
      t.references :labels_value, null: true, foreign_key: true

      t.timestamps
    end
  end
end
