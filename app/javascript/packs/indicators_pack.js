import { draw_indicator_chart } from '../application/javascript/indicators.js.erb';
import { draw_home_page_charts } from '../application/javascript/indicators.js.erb';

export { draw_indicator_chart }
export { draw_home_page_charts }
