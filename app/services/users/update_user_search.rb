module Users
  class UpdateUserSearch
    def initialize(user)
      @user = user
    end

    def call(params)
      if params[:reinit_filters]
        reinit_filters
        return
      end

      return unless params[:custom_field] && params[:rca_search]

      @user.usersearch = {}
      copy_custom_fields(params)
      copy_rca_searchs(params)

      @user.save!
    end

    def something_to_search?
      search_params = @user.usersearch.deep_symbolize_keys
      return false unless search_params

      # binding.pry
      search_params[:custom_field]&.each_key do |key|
        return true if search_params[:custom_field][key] != ''
      end

      objtmp = RcaSearch.new
      search_tmp = objtmp.as_json
      search_tmp.deep_symbolize_keys!

      # binding.pry

      search_params[:rca_search]&.each_key do |key|
        # return true if search_params[:rca_search][key] != '' && !(key == :status_adv && search_params[:rca_search][key] == 1)
        # return true unless ['', '0', [], ['']].include?(search_params[:rca_search][key]) || (key == :status_adv && ['1', 1].include?(search_params[:rca_search][key]))
        # binding.pry
        return true unless search_params[:rca_search][key] == search_tmp[key]
      end

      return false
    end

    private

    def reinit_filters
      objtmp = RcaSearch.new
      @user.usersearch = {custom_field: {}, rca_search: objtmp.as_json}
      # @user.usersearch = {custom_field: {}, rca_search: {words_one: true, words_type: ['', '1'], status_adv: 1}} # , items_pp: items_pp || Pagy::VARS[:items]}}

      @user.save!
    end

    def copy_custom_fields(params)
      @user.usersearch[:custom_field] = {}
      params[:custom_field].each_key do |k|
        @user.usersearch[:custom_field][k] = params[:custom_field][k] unless params[:custom_field][k].nil? || params[:custom_field][k] == ''
      end
    end

    def copy_rca_searchs(params)
      @user.usersearch[:rca_search] = {}
      params[:rca_search]&.each_key do |k|
        @user.usersearch[:rca_search][k] = params[:rca_search][k] unless params[:rca_search][k].nil? || params[:rca_search][k] == ''
      end
    end
  end
end
