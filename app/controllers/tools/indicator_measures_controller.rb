module Tools
  class IndicatorMeasuresController < ApplicationController
    include ProcessConcern

    before_action :set_tools_indicator_measure, only: [:show, :edit, :update, :destroy, :indicator_measure_up, :indicator_measure_down]
    before_action :init_rca, only: [:new, :create]

    # GET /tools/indicator_measures
    # GET /tools/indicator_measures.json
    # def index
    #   authorize IndicatorMeasure
    #   @tools_indicator_measures = Tools::IndicatorMeasure.all
    # end

    # GET /tools/indicator_measures/1
    # GET /tools/indicator_measures/1.json
    # def show
    # end

    # GET /tools/indicator_measures/new
    def new
      # authorize IndicatorMeasure
      skip_authorization

      if @rca.nil? || current_user.get_session(:active_step_tool).nil?
        redirect_to rcas_path
      elsif params[:tools_indicator_measure].nil?
        redirect_to edit_rca_path(@rca)
      else
        @tools_indicator_measure = Tools::IndicatorMeasure.new(tools_indicator_measure_params)
        authorize @tools_indicator_measure
      end
    end

    # GET /tools/indicator_measures/1/edit
    def edit
      render :new
    end

    # POST /tools/indicator_measures
    # POST /tools/indicator_measures.json
    def create
      # authorize IndicatorMeasure
      skip_authorization

      redirect_to rcas_path and return if @rca.nil?

      @tools_indicator_measure = Tools::IndicatorMeasure.new(tools_indicator_measure_params)
      authorize @tools_indicator_measure

      respond_to do |format|
        if @tools_indicator_measure.save
          Meth::Positions::MoveService.new(nil, @tools_indicator_measure.indicator.indicator_measures).reinit_pos

          format.html { redirect_to tools_indicators_url(rca: @rca), notice: 'Indicator measure was successfully created.' }
          format.json { render :show, status: :created, location: @tools_indicator_measure }
        else
          format.html { render :new }
          format.json { render json: @tools_indicator_measure.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /tools/indicator_measures/1
    # PATCH/PUT /tools/indicator_measures/1.json
    def update
      respond_to do |format|
        if @tools_indicator_measure.update(tools_indicator_measure_params)
          format.html { redirect_to tools_indicators_url(rca: @rca), notice: 'Indicator measure was successfully updated.' }
          format.json { render :show, status: :ok, location: @tools_indicator_measure }
        else
          format.html { render :edit }
          format.json { render json: @tools_indicator_measure.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /tools/indicator_measures/1
    # DELETE /tools/indicator_measures/1.json
    def destroy
      indicator = @tools_indicator_measure.indicator
      @tools_indicator_measure.destroy
      Meth::Positions::MoveService.new(nil, indicator.indicator_measures).reinit_pos

      respond_to do |format|
        format.html { redirect_to tools_indicators_url(rca: @rca), notice: 'Indicator measure was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    # PATCH
    def indicator_measure_up
      indicator_measures = @tools_indicator_measure.indicator.indicator_measures
      Meth::Positions::MoveService.new(@tools_indicator_measure, indicator_measures).call('up')

      redirect_to tools_indicators_url(rca: @rca)
    end

    def indicator_measure_down
      indicator_measures = @tools_indicator_measure.indicator.indicator_measures
      Meth::Positions::MoveService.new(@tools_indicator_measure, indicator_measures).call('down')

      redirect_to tools_indicators_url(rca: @rca)
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_indicator_measure
      @tools_indicator_measure = Tools::IndicatorMeasure.find(params[:id])
      authorize @tools_indicator_measure
      @rca = @tools_indicator_measure&.indicator&.rca
    end

    # Only allow a list of trusted parameters through.
    def tools_indicator_measure_params
      params.require(:tools_indicator_measure).permit(:indicator_id, :name, :description, :value, :pos)
    end
  end
end
