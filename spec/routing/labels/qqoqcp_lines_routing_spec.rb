require 'rails_helper'

RSpec.describe Labels::QqoqcpLinesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/labels/qqoqcp_lines').to route_to('labels/qqoqcp_lines#index')
    end

    it 'routes to #new' do
      expect(get: '/labels/qqoqcp_lines/new').to route_to('labels/qqoqcp_lines#new')
    end

    it 'routes to #show' do
      expect(get: '/labels/qqoqcp_lines/1').to route_to('labels/qqoqcp_lines#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/labels/qqoqcp_lines/1/edit').to route_to('labels/qqoqcp_lines#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/labels/qqoqcp_lines').to route_to('labels/qqoqcp_lines#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/labels/qqoqcp_lines/1').to route_to('labels/qqoqcp_lines#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/labels/qqoqcp_lines/1').to route_to('labels/qqoqcp_lines#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/labels/qqoqcp_lines/1').to route_to('labels/qqoqcp_lines#destroy', id: '1')
    end
  end
end
