json.extract! rca, :id, :title, :created_at, :updated_at
json.url rca_url(rca, format: :json)
