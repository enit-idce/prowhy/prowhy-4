class AddPaOptionsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :pa_options, :json, after: :usersearch
  end
end
