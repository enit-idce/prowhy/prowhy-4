class CreateMethStepTools < ActiveRecord::Migration[6.0]
  def change
    create_table :meth_step_tools do |t|
      t.references :step, null: false, foreign_key: { to_table: :meth_steps }
      t.references :tool, null: false, foreign_key: { to_table: :meth_tools }
      t.integer :pos

      t.timestamps
    end
  end
end
