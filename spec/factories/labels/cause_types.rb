FactoryBot.define do
  factory :cause_type, class: Labels::CauseType do
    sequence(:name) { |n| "cause_type_#{n}" }
  end
end
