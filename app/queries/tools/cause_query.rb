module Tools
  # Cause queries
  class CauseQuery < BaseQuery
    def self.relation(base_relation = nil)
      super(base_relation, Tools::Cause)
    end

    # Arbre des concepts triés pour un select
    # si ctid = 0 => tout l'arbre des concepts
    # sinon => le sous-arbre avec concept_type = type_c
    # def causes_subtree(type_c, statuses)
    #   return type_c == 0 ? Concept.arrange_as_array : Concept.arrange_as_array(status: statuses, concept_type_id: ConceptType.where(label: type_c).first.id)
    # end

    # Query to get concept subtree of tpe type_c_id
    # statuses = [ 'status1', 'status2', ... ]
    def causes_subtree_by_id(rca_id, tool_reference)
      return rca_id == 0 ? Tools::Cause.arrange_as_array : Tools::Cause.arrange_as_array(rca_id: rca_id, tool_reference: tool_reference)
    end
  end
end
