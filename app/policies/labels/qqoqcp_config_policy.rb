module Labels
  class QqoqcpConfigPolicy < AdminPolicy
    attr_reader :user, :record

    def initialize(user, record)
      @user = user
      @record = record
    end

    def add_line?
      edit?
    end

    def add_line_valid?
      update?
    end
  end
end
