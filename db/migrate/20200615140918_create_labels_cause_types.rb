class CreateLabelsCauseTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :labels_cause_types do |t|
      t.string :name
      t.integer :pos

      t.timestamps
    end
  end
end
