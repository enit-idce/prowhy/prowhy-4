class AddDetailsToToolCustomFields < ActiveRecord::Migration[6.0]
  def change
    add_column :meth_tool_custom_fields, :pos, :integer, after: :custom_field_desc_id, default: 0
    add_column :meth_tool_custom_fields, :visible, :boolean, after: :pos, default: true
    add_column :meth_tool_custom_fields, :obligatory, :boolean, after: :visible, default: false
    add_column :meth_tool_custom_fields, :default_value, :string, after: :obligatory, default: ''
  end
end
