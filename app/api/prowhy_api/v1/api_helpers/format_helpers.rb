module ProwhyApi
  module V1
    module ApiHelpers
      module FormatHelpers
        extend Grape::API::Helpers

        Grape::Entity.format_with :date_ok do |date|
          date&.strftime('%Y-%m-%d')
        end
      end
    end
  end
end
