require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to test the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator. If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails. There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.

RSpec.describe '/meth/custom_field_descs', type: :request do
  # Meth::CustomFieldDesc. As you add validations to Meth::CustomFieldDesc, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) { FactoryBot.attributes_for(:meth_custom_field_desc, custom_field_type_id: custom_field_type.id) }
  let(:invalid_attributes) { FactoryBot.attributes_for(:meth_custom_field_desc, name: '', custom_field_type_id: custom_field_type.id) }

  let(:custom_field_type) { FactoryBot.create(:meth_custom_field_type) }
  let(:user) { FactoryBot.create(:user) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }

  before :each do
    user.set_session(:active_step_tool, methodology.steps.first.step_tools.first.id)
    user.save!
    sign_in user
  end

  describe 'GET /index' do
    it 'renders a successful response' do
      Meth::CustomFieldDesc.create! valid_attributes
      get meth_custom_field_descs_url
      expect(response).to be_successful
    end
  end

  # describe 'GET /show' do
  #   it 'renders a successful response' do
  #     meth_custom_field_desc = Meth::CustomFieldDesc.create! valid_attributes
  #     get meth_custom_field_desc_url(meth_custom_field_desc)
  #     expect(response).to be_successful
  #   end
  # end

  describe 'GET /new' do
    it 'renders a successful response' do
      get new_meth_custom_field_desc_url
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      meth_custom_field_desc = Meth::CustomFieldDesc.create! valid_attributes
      get edit_meth_custom_field_desc_url(meth_custom_field_desc)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Meth::CustomFieldDesc' do
        expect {
          post meth_custom_field_descs_url, params: { meth_custom_field_desc: valid_attributes }
        }.to change(Meth::CustomFieldDesc, :count).by(1)
      end

      it 'redirects to the meth_custom_field_desc list' do
        post meth_custom_field_descs_url, params: { meth_custom_field_desc: valid_attributes }
        expect(response).to redirect_to(meth_custom_field_descs_url)
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Meth::CustomFieldDesc' do
        expect {
          post meth_custom_field_descs_url, params: { meth_custom_field_desc: invalid_attributes }
        }.to change(Meth::CustomFieldDesc, :count).by(0)
      end

      it 'renders a successful response (i.e. to display the new template)' do
        post meth_custom_field_descs_url, params: { meth_custom_field_desc: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      let(:new_attributes) { {name: 'new cfd name'} }

      it 'updates the requested meth_custom_field_desc' do
        meth_custom_field_desc = Meth::CustomFieldDesc.create! valid_attributes
        patch meth_custom_field_desc_url(meth_custom_field_desc), params: { meth_custom_field_desc: new_attributes }
        meth_custom_field_desc.reload
        expect(meth_custom_field_desc.name).to eq 'new cfd name'
      end

      it 'redirects to meth_custom_field_desc list' do
        meth_custom_field_desc = Meth::CustomFieldDesc.create! valid_attributes
        patch meth_custom_field_desc_url(meth_custom_field_desc), params: { meth_custom_field_desc: new_attributes }
        meth_custom_field_desc.reload
        expect(response).to redirect_to(meth_custom_field_descs_url)
      end
    end

    context 'with invalid parameters' do
      it 'renders a successful response (i.e. to display the edit template)' do
        meth_custom_field_desc = Meth::CustomFieldDesc.create! valid_attributes
        patch meth_custom_field_desc_url(meth_custom_field_desc), params: { meth_custom_field_desc: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested meth_custom_field_desc' do
      meth_custom_field_desc = Meth::CustomFieldDesc.create! valid_attributes
      expect {
        delete meth_custom_field_desc_url(meth_custom_field_desc)
      }.to change(Meth::CustomFieldDesc, :count).by(-1)
    end

    it 'redirects to the meth_custom_field_descs list' do
      meth_custom_field_desc = Meth::CustomFieldDesc.create! valid_attributes
      delete meth_custom_field_desc_url(meth_custom_field_desc)
      expect(response).to redirect_to(meth_custom_field_descs_url)
    end
  end
end
