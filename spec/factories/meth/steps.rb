FactoryBot.define do
  factory :meth_step, class: Meth::Step do
    name { 'Step 1' }
    association :step_role, factory: :meth_step_role

    trait :with_tool do
      after(:create) do |step|
        step.add_tool(FactoryBot.create(:meth_tool))
      end
    end
  end
end
