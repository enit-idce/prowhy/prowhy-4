json.extract! tools_cause_tree, :id, :rca_id, :tool_reference, :tool_num, :created_at, :updated_at
json.url tools_cause_tree_url(tools_cause_tree, format: :json)
