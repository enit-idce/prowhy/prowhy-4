require 'rails_helper'

RSpec.feature 'Methodology step creation', type: :feature do
  before :all do
    load "#{Rails.root}/db/seed_tests.rb"

    @meth = Meth::Methodology.rca_methodologies.where(name_fr: 'Méthode 8D').first
    @step = @meth.steps_menu.first
  end

  # Il faut logguer le user avant chaque scénario
  before :each do
    user = User.first
    user.usersession[:active_step_tool] = @meth.steps.first.step_tools.first.id
    user.save!
    sign_in user
  end

  after :all do
    load "#{Rails.root}/db/seed_empty.rb"
  end

  scenario 'User creates a new step', js: true do
    visit "/meth/methodologies/#{@meth.id}/edit"
    # save_and_open_page

    click_link 'Create Step'

    fill_in 'Name (EN)', with: 'Step just created'
    select 'Info', from: 'Step role'
    click_button 'Create Step'

    expect(page).to have_text('Step was successfully created.')
    expect(page).to have_current_path(edit_meth_methodology_path(@meth.id, step: Meth::Step.last.id))
  end

  scenario 'User updates a step (change name, add tool, edit tool name, del tool)', js: true do
    visit "/meth/methodologies/#{@meth.id}/edit?step=#{@step.id}"
    # save_and_open_page

    # Update step name
    bip_text @step, :name, 'Blablabla'

    expect(page).to have_text('Blablabla')

    # Add a tool to step
    tool = Meth::Activity.first.tools.first
    expect {
      within("a[href='#{meth_step_add_tool_path(id: @step.id, tool: tool.id)}']") { find('svg').click }
    }.to change { @step.step_tools.count }.by 1

    # Add a second tool to step
    tool = Meth::Activity.last.tools.last
    expect {
      within("a[href='#{meth_step_add_tool_path(id: @step.id, tool: tool.id)}']") { find('svg').click }
    }.to change { @step.step_tools.count }.by 1

    # Edit first step_tool name
    find("a[href='#{edit_meth_step_tool_path(@step.step_tools.first.id)}']").click
    fill_in 'Name (EN)', with: 'Step tool new name'
    # click_button 'Update Step Tool'
    find('input[name="commit"]').click
    expect(page).to have_text('Step tool new name')

    # Del first created tool
    # step_tool = @step.step_tools[@step.step_tools.count - 2]
    # expect {
    #   accept_confirm do
    #     within("a[href='#{meth_step_del_tool_path(id: @step.id, step_tool: step_tool.id)}'][data-method='delete']") { find('svg').click }
    #   end
    # }.to change { @step.step_tools.count }.by(-1)
  end
end
