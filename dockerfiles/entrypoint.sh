#!/bin/bash
set -e

# Wait for MYSQL service to start
OUTPUT="Can't connect"

while [[ $OUTPUT == *"Can't connect"* ]]
do
    OUTPUT=$(mysql -h $DB_HOST -P 3306 -u $DB_USERNAME --password=$DB_PASSWORD -e "show databases" 2>&1)
    sleep 3
done

# Redirect logs (apache and prowhy)
ln -sf /proc/1/fd/1 /var/log/apache2/error.log
ln -sf /proc/1/fd/1 /prowhy/log/production.log

# Delete server.pid if exist
rm -f /prowhy/tmp/pids/server.pid

# Set rights on storage volume
chmod -R u+rwX,g+rwX,o+rwX /prowhy/storage

echo "=============================================>PROWHY DATABASE"
# Create database and load datas if db creation
# rails db:create RAILS_ENV=production
# rails db:migrate:with_data RAILS_ENV=production
RAILS_ENV=production rake db:docker_update

# Start Cron in foreground
# TODO : test with arask
# env >> /etc/environment
# exec "cron -f"

# Start Apache in foreground
source /etc/apache2/envvars
/usr/sbin/apache2 -DFOREGROUND

exec "$@"
