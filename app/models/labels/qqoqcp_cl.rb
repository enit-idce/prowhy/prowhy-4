module Labels
  class QqoqcpCl < ApplicationRecord
    belongs_to :qqoqcp_config
    belongs_to :qqoqcp_line
  end
end
