require 'rails_helper'

RSpec.describe Tools::DecMatLink, type: :model do
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to decision_mat' do
        assc = Tools::DecMatLink.reflect_on_association(:decision_mat)
        expect(assc.macro).to eq :belongs_to
      end

      it 'belongs to entry' do
        assc = Tools::DecMatLink.reflect_on_association(:entry)
        expect(assc.macro).to eq :belongs_to
      end

      it 'has many dec_mat_vals' do
        assc = Tools::DecMatLink.reflect_on_association(:dec_mat_vals)
        expect(assc.macro).to eq :has_many
      end
    end
  end
end
