require 'rails_helper'

RSpec.describe Tools::CauseLink, type: :model do
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to cause' do
        assc = Tools::CauseLink.reflect_on_association(:cause)
        expect(assc.macro).to eq :belongs_to
      end
      it 'belongs to cause_tool (polymorphic)' do
        assc = Tools::CauseLink.reflect_on_association(:cause_tool)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end
end
