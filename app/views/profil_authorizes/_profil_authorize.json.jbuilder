json.extract! profil_authorize, :id, :profil_id, :meth_step_role_id, :read, :write, :advance, :email, :created_at, :updated_at
json.url profil_authorize_url(profil_authorize, format: :json)
