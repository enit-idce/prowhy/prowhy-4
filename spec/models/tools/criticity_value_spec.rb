require 'rails_helper'

RSpec.describe Tools::CriticityValue, type: :model do
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to criticity' do
        assc = Tools::CriticityValue.reflect_on_association(:criticity)
        expect(assc.macro).to eq :belongs_to
      end
      it 'belongs to labels_criterium' do
        assc = Tools::CriticityValue.reflect_on_association(:labels_criterium)
        expect(assc.macro).to eq :belongs_to
      end
      it 'belongs to labels_value' do
        assc = Tools::CriticityValue.reflect_on_association(:labels_value)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end
end
