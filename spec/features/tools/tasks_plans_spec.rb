require 'rails_helper'

RSpec.feature 'Rca open menus', type: :feature do
  let(:rca) { FactoryBot.create(:rca, methodology: methodology) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:person) { FactoryBot.create(:person) }
  let(:user) { FactoryBot.create(:user, person_id: person.id) }

  let(:activity) { FactoryBot.create(:meth_activity) }
  let(:tool) { FactoryBot.create(:meth_tool, :task) }

  let(:cause1) { FactoryBot.create(:cause, rca: rca, tool_reference: 1) }
  let(:cause2) { FactoryBot.create(:cause, rca: rca, tool_reference: 2) }

  let(:rca2) { FactoryBot.create(:rca, methodology: methodology) }
  let(:cause1_2) { FactoryBot.create(:cause, rca: rca2, tool_reference: 1) }

  before :each do
    user.set_session(:active_step_tool, methodology.steps.first.step_tools.first.id)
    user.save!

    sign_in user

    rca.add_person_to_team(user.person, 'author')
    rca.initialize_rca_infos

    # Add tool task to methodology
    @step = methodology.steps_menu.first
    @step_tool1 = @step.add_tool(tool, tool_reference: 1)

    # Add cause & task for tool_reference 1
    FactoryBot.create(:task, rca_id: rca.id, name: 'TC1-1', task_detail_type: 'Tools::TaskCorrective', tools_cause: cause1)
    FactoryBot.create(:task, rca_id: rca.id, name: 'TC1-2', task_detail_type: 'Tools::TaskCorrective', tools_cause: cause1)
    FactoryBot.create(:task, rca_id: rca.id, name: 'TP1-1', task_detail_type: 'Tools::TaskPreventive', tools_cause: cause1)
    FactoryBot.create(:task, rca_id: rca.id, name: 'TCo1-1', task_detail_type: 'Tools::TaskContainment', tools_cause: cause1)

    # Add cause & task for tool_reference 2
    FactoryBot.create(:task, rca_id: rca.id, name: 'TC2-1', task_detail_type: 'Tools::TaskCorrective', tool_reference: 2, tools_cause: cause2)
    FactoryBot.create(:task, rca_id: rca.id, name: 'TP2-1', task_detail_type: 'Tools::TaskPreventive', tool_reference: 2, tools_cause: cause2)
    FactoryBot.create(:task, rca_id: rca.id, name: 'TCo2-1', task_detail_type: 'Tools::TaskContainment', tool_reference: 2, tools_cause: cause2)

    # Add causes to RCA2 / reference 1
    FactoryBot.create(:task, rca_id: rca2.id, name: 'R2TC1-1', task_detail_type: 'Tools::TaskCorrective', tools_cause: cause1_2)
    FactoryBot.create(:task, rca_id: rca2.id, name: 'R2TC1-2', task_detail_type: 'Tools::TaskCorrective', tools_cause: cause1_2)
    FactoryBot.create(:task, rca_id: rca2.id, name: 'R2TP1-1', task_detail_type: 'Tools::TaskPreventive', tools_cause: cause1_2)
    FactoryBot.create(:task, rca_id: rca2.id, name: 'R2TCo1-1', task_detail_type: 'Tools::TaskContainment', tools_cause: cause1_2)

    # binding.pry
  end

  scenario 'User opens Task plan for reference 1, 2 and 0', js: true do
    # Open Task plan Reference 1
    visit meth_step_tool_show_path(id: @step_tool1.id, rca: rca.id)
    expect(page).to have_css('td.task-title', count: 4)
    # save_and_open_page

    # Open Task plan Reference 2
    @step_tool1.tool_reference = 2
    @step_tool1.save!

    visit meth_step_tool_show_path(id: @step_tool1.id, rca: rca.id)
    expect(page).to have_css('td.task-title', count: 3)

    # Open Task plan Reference 0 (1+2)
    @step_tool1.tool_reference = 0
    @step_tool1.save!

    visit meth_step_tool_show_path(id: @step_tool1.id, rca: rca.id)
    expect(page).to have_css('td.task-title', count: 7)

    # @step_tool1.tool_options = {task_options: nil,
    #                             plan_options: {type_plan: 'global'}}
  end

  scenario 'User opens Task plan for all actions type and corrective type', js: true do
    # Open Task plan with all Corrective Tasks (ref 0)
    @step_tool1.tool_reference = 0
    @step_tool1.tool_options = @step_tool1.tool_options.merge({task_options: {task_detail_type: 'Tools::TaskCorrective'}})
    @step_tool1.save!

    visit meth_step_tool_show_path(id: @step_tool1.id, rca: rca.id)
    expect(page).to have_css('td.task-title', count: 3)

    # Open Task plan with Corrective Tasks Ref 1
    @step_tool1.tool_reference = 1
    @step_tool1.save!

    visit meth_step_tool_show_path(id: @step_tool1.id, rca: rca.id)
    expect(page).to have_css('td.task-title', count: 2)

    # binding.pry

    # @step_tool1.tool_options = {task_options: nil,
    #                             plan_options: {type_plan: 'global'}}
  end

  scenario 'User opens Global Task plan', js: true do
    # Open Global Task plan for all types
    @step_tool1.tool_reference = 0
    @step_tool1.tool_options = {task_options: nil,
                                plan_options: {type_plan: 'global'}}
    @step_tool1.save!

    # set rca1 & rca2 in global task plan (user session)
    user.set_session(:plan_rcas_ids, [rca.id, rca2.id])

    visit meth_step_tool_show_path(id: @step_tool1.id)
    # sleep(1)
    # save_and_open_page
    expect(page).to have_css('td.task-title', count: 11)

    # Open Global Task plan - Ref 1
    @step_tool1.tool_reference = 1
    @step_tool1.save!

    visit meth_step_tool_show_path(id: @step_tool1.id)
    expect(page).to have_css('td.task-title', count: 8)
  end
end
