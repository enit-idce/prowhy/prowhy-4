require 'doorkeeper/grape/helpers'
require 'devise'

module ProwhyApi
  module V1
    module ApiTools
      class ApiCauses < Grape::API
        # helpers Helpers::ApiHelpers

        # before do
        #   doorkeeper_authorize! unless user_signed_in?
        # end

        desc "Tools cause requests\n" \
             " - causes list for one Rca\n" \
             " - create / update one cause\n" \
             " - update ishikawa branch for one cause\n"
        resource :causes do
          desc 'Return all Tools Causes linked to Rca #rca_id.'
          params do
            requires :rca_id, type: Integer, desc: 'Rca ID.'
          end
          get do
            # 1. On test si le Rca existe => raise RecordNotFound if not.
            Rca.find(params[:rca_id])

            # 2. On test si le Rca existe dans la liste des Rca autorisés (policy_scope) => raise NotAuthorized if not.
            begin
              @rca = policy_scope(Rca).find(params[:rca_id])
            rescue ActiveRecord::RecordNotFound
              raise Pundit::NotAuthorizedError, 'not authorized to show this Rca.'
            end

            authorize @rca, :show?

            @causes = @rca.tools_causes.where(ancestry: nil)

            present @causes, with: Entities::ApiTools::Cause, with_id: 'true', with_child: 'true'
          end

          desc 'Return all Tools Causes accessible by connected user.'
          params do
            optional :options, type: Hash do
              optional :ishikawa, type: Boolean, desc: 'True : show ishikawas branches.', default: true
            end
          end
          get :all do
            # Filtrage des rcas autorisés (policy_scope)
            list_ids_ok = policy_scope(Rca).uniq.pluck(:id)

            @causes = Tools::Cause.where(rca_id: list_ids_ok).includes([tools_cause_classes: [:labels_cause_type]])

            present @causes, with: Entities::ApiTools::Cause,
                             with_id: 'true',
                             with_parent_id: true,
                             with_child: 'false',
                             with_ishikawas: false,
                             with_ishikawas_inline: (params[:options][:ishikawa] == true || params[:options][:ishikawa] == 'true' ? true : false)
          end

          desc 'Return Tools Cause :id'
          params do
            requires :id, type: Integer, desc: 'Cause ID.'
          end
          route_param :id do
            get do
              @cause = Tools::Cause.find(params[:id])
              begin
                policy_scope(Rca).find(@cause.rca_id)
              rescue ActiveRecord::RecordNotFound
                raise Pundit::NotAuthorizedError, 'not authorized to show this cause.'
              end
              authorize @cause, :show?

              present @cause, with: Entities::ApiTools::Cause, with_id: 'false'
            end
          end

          desc 'Create a Tools Cause.'
          params do
            requires :tools_cause, type: Hash, documentation: {param_type: 'body'} do
              requires :name, type: String, desc: 'Cause title.'
              requires :rca_id, type: Integer, desc: 'Rca ID.'
              optional :parent_id, type: Integer, default: nil, desc: 'Cause Parent ID.'
              optional :tool_reference, type: Integer, default: 1, desc: 'Cause tool reference (default=1).'
              optional :status, type: Integer, default: 0, desc: 'Cause status : in_progress: 0, validated: 1, invalidated: 2.'
              optional :level, type: Integer, default: -1, desc: "Cause level : '-': -1, '+': 1, '++': 2."
            end
          end
          post do
            unless params[:tools_cause][:parent_id]
              rca = Rca.find(params[:tools_cause][:rca_id])
              step_tool = Meth::StepTool.new(tool_reference: params[:tools_cause][:tool_reference] || 1)
              cause_parent = rca.find_or_create_tool('tools_causes', 'Tools::Cause', step_tool)
              params[:tools_cause][:parent_id] = cause_parent.id
            end

            @tools_cause = Tools::Cause.new(params[:tools_cause])

            begin
              policy_scope(Rca).find(@tools_cause.rca_id)
            rescue ActiveRecord::RecordNotFound
              raise Pundit::NotAuthorizedError, 'not authorized to create a cause linked to this Rca.'
            end

            authorize @tools_cause, :create?

            @tools_cause.save!

            present @tools_cause, with: Entities::ApiTools::Cause, with_id: 'true'
          end

          desc 'Update ishikawas branch for cause.'
          params do
            requires :id, type: Integer, desc: 'Cause ID.'

            optional :ishikawas, type: Hash, documentation: {param_type: 'body'}
          end
          route_param :id do
            patch do
              @cause = Tools::Cause.find(params[:id])
              begin
                policy_scope(Rca).find(@cause.rca_id)
              rescue ActiveRecord::RecordNotFound
                raise Pundit::NotAuthorizedError, 'not authorized to update this cause.'
              end
              authorize @cause, :update?

              params[:ishikawas]&.each_key do |ishi_key|
                ishi_id = ishi_key.split('_')[1].to_i

                next unless ishi_id

                ishikawa = Tools::Ishikawa.find(ishi_id)
                # 409 ???
                raise StandardError, "Invalid params : Ishikawa ID #{ishi_id} - RCA #{ishikawa.rca_id} / Cause #{@cause.id} - Rca ID #{@cause.rca_id}" if ishikawa.rca_id != @cause.rca_id

                @cause.add_ishi_cause_type(ishikawa, params[:ishikawas][ishi_key])
              end

              present @cause, with: Entities::ApiTools::Cause, with_id: 'false'
            end
          end

          # desc 'Update a Rca.'
          # params do
          #   requires :id, type: Integer, desc: 'Rca ID.'

          #   optional :title, type: String, desc: 'Rca ID.', documentation: {param_type: 'body', desc: 'Rca ID.'}
          #   optional :description, desc: 'Rca Detailed description.', documentation: {param_type: 'body'}

          #   optional :custom_field_attributes, type: Hash, documentation: {param_type: 'body'} do
          #     CustomField.column_names.each do |cf|
          #       next unless cf.include? 'custom_'

          #       cfdesc = Meth::CustomFieldDesc.where(internal_name: cf)&.first
          #       next unless cfdesc

          #       optional (cf+"-#{cfdesc.name}").intern,
          #                type: cfdesc.meth_custom_field_type_api,
          #                documentation: { desc: cfdesc.meth_custom_field_desc_api, # cfdesc.name,
          #                                 param_type: 'body' }
          #     end
          #   end
          # end
          # route_param :id do
          #   patch do
          #     # binding.pry
          #     @rca = Rca.find(params[:id])

          #     new_params = {}
          #     new_params[:title] = params[:title] if params[:title]
          #     new_params[:description] = params[:description] if params[:description]
          #     new_params[:custom_field_attributes] = {}
          #     new_params[:custom_field_attributes][:id] = @rca.custom_field&.id

          #     params[:custom_field_attributes]&.keys&.each do |key|
          #       new_params[:custom_field_attributes][key.split('-')[0]] = params[:custom_field_attributes][key]
          #     end

          #     @rca.update(new_params)

          #     present @rca, with: RcasPresenter, with_id: 'true'
          #   end
          # end

          # # DELETE
          # desc 'Delete Rca :id. Be carefull, no undo !'
          # params do
          #   requires :id, type: Integer, desc: 'Rca ID.'
          # end
          # route_param :id do
          #   delete do
          #     @rca = Rca.find(params[:id])
          #     authorize @rca, :destroy?

          #     @rca.destroy
          #   end
          # end
        end
      end
    end
  end
end
