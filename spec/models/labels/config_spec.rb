require 'rails_helper'

RSpec.describe Labels::Config, type: :model do
  subject(:config) do
    # Labels::Criterium.create!(name: 'Efficacity', threshold: 1)
    FactoryBot.create(:labels_config)
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has many criteria' do
        assc = Labels::Config.reflect_on_association(:criteria)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        config.name = ''
        expect(config).to_not be_valid
      end

      it 'is valid with name' do
        config.name = 'Cost'
        expect(config).to be_valid
      end
    end
  end
end
