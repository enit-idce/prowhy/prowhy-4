class AddAncestryToToolsCauses < ActiveRecord::Migration[6.0]
  def change
    add_column :tools_causes, :ancestry, :string, after: :name
    add_index :tools_causes, :ancestry
  end
end
