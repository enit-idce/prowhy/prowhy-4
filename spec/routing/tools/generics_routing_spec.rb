require 'rails_helper'

RSpec.describe Tools::GenericsController, type: :routing do
  describe 'routing' do
    # it 'routes to #index' do
    #   expect(get: '/tools/generics').to route_to('tools/generics#index')
    # end

    # it 'routes to #new' do
    #   expect(get: '/tools/generics/new').to route_to('tools/generics#new')
    # end

    # it 'routes to #show' do
    #   expect(get: '/tools/generics/1').to route_to('tools/generics#show', id: '1')
    # end

    it 'routes to #edit' do
      expect(get: '/tools/generics/1/edit').to route_to('tools/generics#edit', id: '1')
    end

    # it 'routes to #create' do
    #   expect(post: '/tools/generics').to route_to('tools/generics#create')
    # end

    it 'routes to #update via PUT' do
      expect(put: '/tools/generics/1').to route_to('tools/generics#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/tools/generics/1').to route_to('tools/generics#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/tools/generics/1').to route_to('tools/generics#destroy', id: '1')
    end
  end
end
