module Labels
  class ValuesController < ApplicationController
    include AdminConcern
    before_action :set_labels_value, only: [:show, :edit, :update, :destroy]

    # GET /labels/values
    # GET /labels/values.json
    # def index
    #   authorize Value
    #   @labels_values = Labels::Value.all
    # end

    # GET /labels/values/1
    # GET /labels/values/1.json
    # def show
    # end

    # GET /labels/values/new
    def new
      @labels_value = Labels::Value.new(labels_value_params)
      authorize @labels_value
    end

    # GET /labels/values/1/edit
    def edit
      respond_to do |format|
        format.html
        format.js { render :new }
      end
    end

    # POST /labels/values
    # POST /labels/values.json
    def create
      @labels_value = Labels::Value.new(labels_value_params)
      authorize @labels_value

      respond_to do |format|
        # binding.pry
        if @labels_value.save
          format.html { redirect_to edit_labels_criterium_path(id: @labels_value.criterium), notice: 'Value was successfully created.' }
          format.json { render :show, status: :created, location: @labels_value }
        else
          format.js { render :new }
          format.json { render json: @labels_value.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /labels/values/1
    # PATCH/PUT /labels/values/1.json
    def update
      respond_to do |format|
        if @labels_value.update(labels_value_params)
          format.html { redirect_to edit_labels_criterium_path(id: @labels_value.criterium), notice: 'Value was successfully updated.' }
          format.json { render :show, status: :ok, location: @labels_value }
        else
          format.js { render :new }
          format.json { render json: @labels_value.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /labels/values/1
    # DELETE /labels/values/1.json
    def destroy
      criterium = @labels_value.criterium
      @labels_value.destroy
      respond_to do |format|
        format.html { redirect_to edit_labels_criterium_path(id: criterium), notice: 'Value was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_labels_value
      @labels_value = Labels::Value.find(params[:id])
      authorize @labels_value
    end

    # Only allow a list of trusted parameters through.
    def labels_value_params
      params.require(:labels_value).permit(:criterium_id, :value, :title, :title_fr, :title_en, :text_fr, :text_en)
    end
  end
end
