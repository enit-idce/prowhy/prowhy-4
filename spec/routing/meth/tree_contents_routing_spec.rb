require 'rails_helper'

RSpec.describe Meth::TreeContentsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/meth/tree_contents').to route_to('meth/tree_contents#index')
    end

    it 'routes to #new' do
      expect(get: '/meth/tree_contents/new').to route_to('meth/tree_contents#new')
    end

    it 'routes to #show' do
      expect(get: '/meth/tree_contents/1').to route_to('meth/tree_contents#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/meth/tree_contents/1/edit').to route_to('meth/tree_contents#edit', id: '1')
    end


    it 'routes to #create' do
      expect(post: '/meth/tree_contents').to route_to('meth/tree_contents#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/meth/tree_contents/1').to route_to('meth/tree_contents#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/meth/tree_contents/1').to route_to('meth/tree_contents#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/meth/tree_contents/1').to route_to('meth/tree_contents#destroy', id: '1')
    end
  end
end
