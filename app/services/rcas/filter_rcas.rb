module Rcas
  class FilterRcas
    def initialize(rcas)
      @rcas = rcas
    end

    def call(search_params)
      # Filter Rca Reference
      @rcas = @rcas.where('reference LIKE :words', words: "%#{search_params[:rca_search][:reference]}%") if search_params[:rca_search][:reference]

      # FilterCustom fields
      @rcas = @rcas.where(custom_fields: search_params[:custom_field]) unless search_params[:custom_field].empty?

      # Created date
      @rcas = @rcas.where('rcas.created_at >= :date', date: search_params[:rca_search][:date_begin]) if search_params[:rca_search][:date_begin]
      @rcas = @rcas.where('rcas.created_at <= :date', date: search_params[:rca_search][:date_end]) if search_params[:rca_search][:date_end]

      # Status advancement
      filter_advancement(search_params) if search_params[:rca_search][:status_adv] == '2' || search_params[:rca_search][:status_adv] == '3'

      # Duration advancement
      filter_duration(search_params) if search_params[:rca_search][:adv_duration] && search_params[:rca_search][:adv_duration]&.to_i != 0

      # Methodologies
      filter_methodology(search_params) if search_params[:rca_search][:methodology]

      # Teams
      filter_team(search_params) if search_params[:rca_search][:person_id] && search_params[:rca_search][:person_id] != '0'

      # Words
      filter_words(search_params) if search_params[:rca_search][:words_type] && search_params[:rca_search][:words]

      @rcas = @rcas.distinct

      return @rcas
    end

    private

    def filter_methodology(search_params)
      search_params[:rca_search][:methodology]&.delete('')
      @rcas = @rcas.where(methodology_id: search_params[:rca_search][:methodology]) unless search_params[:rca_search][:methodology].empty?
    end

    def filter_advancement(search_params)
      # @rcas = @rcas.left_joins(rca_advances: [meth_step: [:step_role]])
      # @rcas = if search_params[:rca_search][:status_adv] == '3' # Closed
      #           @rcas.where(rca_advances: {meth_steps: {meth_step_roles: {role_name: 'Close'}}, advance: 2})
      #         else # In progress
      #           @rcas.where(rca_advances: {meth_steps: {meth_step_roles: {role_name: 'Close'}}, advance: [0, 1]})
      #         end
      @rcas = if search_params[:rca_search][:status_adv] == '3' # Closed
                @rcas.where(advance: Meth::StepRole.where(role_name: 'Close')&.first&.advance_level)
              else # In progress
                @rcas.where('advance != ?', Meth::StepRole.where(role_name: 'Close')&.first&.advance_level)
              end
    end

    def filter_duration(search_params)
      duration_max = search_params[:rca_search][:adv_duration].to_i
      where_txt1 = "rcas.closed_at IS NOT NULL AND DATEDIFF(rcas.closed_at, rcas.created_at) > #{duration_max}"
      where_txt2 = "rcas.closed_at IS NULL AND DATEDIFF('#{Date.today}', rcas.created_at) > #{duration_max}"

      @rcas = @rcas.where("(#{where_txt1}) OR (#{where_txt2})")
    end

    def filter_team(search_params)
      @rcas = @rcas.left_joins(:tools_teams)

      @rcas = if search_params[:rca_search][:person_role]
                @rcas.where(tools_teams: {profil_id: search_params[:rca_search][:person_role].to_i,
                                          person_id: search_params[:rca_search][:person_id].to_i})
              else # All roles
                @rcas.where(tools_teams: {person_id: search_params[:rca_search][:person_id].to_i})
              end
      # binding.pry
    end

    def filter_words(search_params)
      search_params[:rca_search][:words_type]&.delete('')
      return unless search_params[:rca_search][:words] && search_params[:rca_search][:words] != '' && !search_params[:rca_search][:words_type].empty?

      if search_params[:rca_search][:words_one] == 'true'
        filter_words_at_leat_one(search_params)
      else
        filter_words_all(search_params)
      end
    end

    def filter_words_all(search_params)
      words = search_params[:rca_search][:words].gsub(' ', '%')

      # Search in RCA title and descriptions
      where_txt = ''
      do_or = false
      if search_params[:rca_search][:words_type].include?('1')
        @rcas = @rcas.left_joins(:action_text_rich_text)
        do_or = true
        where_txt += 'title LIKE :words OR action_text_rich_texts.body LIKE :words'
      end
      # Search in Tasks
      if search_params[:rca_search][:words_type].include?('2')
        @rcas = @rcas.left_joins(:tools_tasks)
        where_txt += ' OR ' if do_or
        where_txt += 'tools_tasks.name LIKE :words'
        do_or = true
      end
      # Search in Causes
      if search_params[:rca_search][:words_type].include?('3')
        @rcas = @rcas.left_joins(:tools_causes)
        where_txt += ' OR ' if do_or
        where_txt += 'tools_causes.name LIKE :words'
      end
      # @rcas = @rcas.where(where_txt, words: "%#{search_params[:rca_search][:words]}%")
      @rcas = @rcas.where(where_txt, words: "%#{words}%")
    end

    def filter_words_at_leat_one(search_params)
      words = search_params[:rca_search][:words].split(' ')
      all_words = {}
      words.each_with_index do |w, i|
        all_words["word_#{i}".intern] = "%#{w}%"
      end

      # Search in RCA title and descriptions
      where_txt = ''
      do_or = false
      if search_params[:rca_search][:words_type].include?('1')
        @rcas = @rcas.left_joins(:action_text_rich_text)
        all_words.each_key do |k|
          where_txt += ' OR ' if do_or
          where_txt += "title LIKE :#{k} OR action_text_rich_texts.body LIKE :#{k}"
          do_or = true
        end
      end
      # Search in Tasks
      if search_params[:rca_search][:words_type].include?('2')
        @rcas = @rcas.left_joins(:tools_tasks)
        all_words.each_key do |k|
          where_txt += ' OR ' if do_or
          where_txt += "tools_tasks.name LIKE :#{k}"
          do_or = true
        end
      end
      # Search in Causes
      if search_params[:rca_search][:words_type].include?('3')
        @rcas = @rcas.left_joins(:tools_causes)
        all_words.each_key do |k|
          where_txt += ' OR ' if do_or
          where_txt += "tools_causes.name LIKE :#{k}"
          do_or = true
        end
      end
      # @rcas = @rcas.where(where_txt, words: "%#{search_params[:rca_search][:words]}%")
      # binding.pry
      @rcas = @rcas.where(where_txt, all_words)
    end
  end
end
