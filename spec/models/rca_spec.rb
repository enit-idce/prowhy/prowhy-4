require 'rails_helper'

RSpec.describe Rca, type: :model do
  subject(:rca) do
    FactoryBot.create(:rca, methodology: methodology)
  end

  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }

  # Tests has_many :tools + dependant: destroy
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to methodology' do
        assc = Rca.reflect_on_association(:methodology)
        expect(assc.macro).to eq :belongs_to
      end

      it 'has one custom_field' do
        assc = Rca.reflect_on_association(:custom_field)
        expect(assc.macro).to eq :has_one
      end

      it 'has many tools_qqoqcps' do
        assc = Rca.reflect_on_association(:tools_qqoqcps)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools_causes' do
        assc = Rca.reflect_on_association(:tools_causes)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools_tasks' do
        assc = Rca.reflect_on_association(:tools_tasks)
        expect(assc.macro).to eq :has_many
      end

      it 'has many steps' do
        assc = Rca.reflect_on_association(:steps)
        expect(assc.macro).to eq :has_many
      end

      it 'has many steps exclude step_info' do
        # binding.pry
        expect(rca.steps.count).to eq 2 # step1 + step closure
        expect(rca.steps).to_not include(rca.methodology.step_info)
      end

      it 'has many tools_teams' do
        assc = Rca.reflect_on_association(:tools_teams)
        expect(assc.macro).to eq :has_many
      end

      it 'has many people' do
        assc = Rca.reflect_on_association(:people)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools_documents' do
        assc = Rca.reflect_on_association(:tools_documents)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools_documents by step_role_id' do
        file_ok = Rails.root.join('spec', 'support', 'assets', 'images', 'not_image.pdf')
        doc_ok = ActiveStorage::Blob.create_and_upload!(
          io: File.open(file_ok, 'rb'),
          filename: 'not_image.pdf',
          content_type: 'file/pdf'
        ).signed_id

        Tools::Document.create!([{name: 'Doc1', rca: rca, tool_reference: 2, file: doc_ok},
                                 {name: 'Doc2', rca: rca, tool_reference: 0, file: doc_ok},
                                 {name: 'Doc3', rca: rca, tool_reference: 1, file: doc_ok},
                                 {name: 'Doc4', rca: rca, tool_reference: 1, file: doc_ok}])

        expect(rca.reload.tools_documents.by_step_role_id(1).map(&:name)).to contain_exactly('Doc3', 'Doc4')
      end

      it 'has many tools_images' do
        assc = Rca.reflect_on_association(:tools_images)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools_images by step_role_id' do
        file_ok = Rails.root.join('spec', 'support', 'assets', 'images', 'image_ok.png')
        doc_ok = ActiveStorage::Blob.create_and_upload!(
          io: File.open(file_ok, 'rb'),
          filename: 'image_ok.png',
          content_type: 'image/png'
        ).signed_id

        Tools::Image.create!([{name: 'Img1', rca: rca, tool_reference: 2, file: doc_ok},
                              {name: 'Img2', rca: rca, tool_reference: 0, file: doc_ok},
                              {name: 'Img3', rca: rca, tool_reference: 1, file: doc_ok},
                              {name: 'Img4', rca: rca, tool_reference: 1, file: doc_ok}])

        expect(rca.reload.tools_images.by_step_role_id(1).map(&:name)).to contain_exactly('Img3', 'Img4')
      end

      it 'has many tools_criticities' do
        assc = Rca.reflect_on_association(:tools_criticities)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools_indicators' do
        assc = Rca.reflect_on_association(:tools_indicators)
        expect(assc.macro).to eq :has_many
      end

      it 'has many tools_generics' do
        assc = Rca.reflect_on_association(:tools_generics)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without title' do
        rca.title = ''
        expect(rca).to_not be_valid
      end

      it 'is valid with title' do
        rca.title = 'Team'
        expect(rca).to be_valid
      end
    end
  end

  describe '#long_title' do
    it 'shows reference - title' do
      expect(rca.long_title).to eq "#{format("%05d", rca.id)} - #{rca.title}"
    end
  end

  describe '#find_or_create_tool' do
    context 'Without existing tools' do
      it 'creates task tool' do
        activity = Meth::Activity.create!(name_fr: 'toto')
        tool = Meth::Tool.create!(name_fr: 'Task', name_en: 'Task', tool_name: 'Action', activity: activity)
        # binding.pry
        methodology.steps_menu.first.add_tool(tool) # FactoryBot.create(:meth_tool, :task))
        tasks = rca.find_or_create_tool('tools_tasks', 'Tools::Task', methodology.steps_menu.first.step_tools.first)
        expect(tasks).to eq []
      end

      it 'creates qqoqcp tool' do
        methodology.steps_menu.first.add_tool(FactoryBot.create(:meth_tool, :qqoqcp))
        qqoqcp = rca.find_or_create_tool('tools_qqoqcps', 'Tools::Qqoqcp', methodology.steps_menu.first.step_tools.first)
        expect(qqoqcp.class).to eq Tools::Qqoqcp
        expect(qqoqcp.rca).to eq rca
      end

      it 'creates cause tool' do
        methodology.steps_menu.first.add_tool(FactoryBot.create(:meth_tool, :cause))
        cause = rca.find_or_create_tool('tools_causes', 'Tools::Cause', methodology.steps_menu.first.step_tools.first)
        expect(cause.class).to eq Tools::Cause
        expect(cause.rca).to eq rca
      end
    end

    context 'With existing tools' do
      before :each do
        methodology.steps_menu.first.add_tool(FactoryBot.create(:meth_tool))
        @head_cause = Tools::Cause.create_default!({rca: rca, tool_reference: 1}, nil) # FactoryBot.create(:cause, rca: rca)
        3.times do
          FactoryBot.create(:cause, rca: rca, ancestry: @head_cause.id.to_s)
        end
        5.times do
          FactoryBot.create(:task, rca: rca, tools_cause: @head_cause)
        end
        @qqoqcp = FactoryBot.create(:qqoqcp, rca: rca)
      end

      it 'finds task tool' do
        tasks = rca.find_or_create_tool('tools_tasks', 'Tools::Task', methodology.steps_menu.first.step_tools.first)
        expect(tasks).to contain_exactly(*Tools::Task.all.to_a)
      end

      it 'finds qqoqcp tool' do
        qqoqcp = rca.find_or_create_tool('tools_qqoqcps', 'Tools::Qqoqcp', methodology.steps_menu.first.step_tools.first)
        expect(qqoqcp.class).to eq Tools::Qqoqcp
        expect(qqoqcp.rca).to eq rca
        expect(qqoqcp).to eq @qqoqcp
      end

      it 'finds cause tool' do
        cause = rca.find_or_create_tool('tools_causes', 'Tools::Cause', methodology.steps_menu.first.step_tools.first)
        expect(cause.class).to eq Tools::Cause
        expect(cause.rca).to eq rca
        expect(cause).to eq @head_cause
      end
    end
  end

  describe 'initialize rca infos' do
    it 'creates custom field and rca advances' do
      rca.initialize_rca_infos
      rca.reload

      expect(rca.custom_field).to be_truthy
      expect(rca.rca_advances).to be_truthy
      expect(rca.rca_advances.count).to eq rca.methodology.steps_menu.count
    end

    it 'reinitialize custom field and rca advances : creates if does not exist' do
      keep_cf = rca.custom_field = CustomField.create!(rca: rca)
      rca.reload
      rca.reinitialize_rca_infos

      expect(rca.custom_field).to eq keep_cf
      expect(rca.rca_advances).to be_truthy
      expect(rca.rca_advances.count).to eq rca.methodology.steps_menu.count

      rca.reinitialize_rca_infos

      expect(rca.custom_field).to eq keep_cf
      expect(rca.rca_advances).to be_truthy
      expect(rca.rca_advances.count).to eq rca.methodology.steps_menu.count
      expect(RcaAdvance.all.count).to eq rca.rca_advances.count
    end
  end

  describe 'Teams' do
    let(:current_user) { FactoryBot.create(:user, person: person1) }
    let(:person1) { FactoryBot.create(:person) }
    let(:person2) { FactoryBot.create(:person) }
    let(:person3) { FactoryBot.create(:person) }
    let(:person4) { FactoryBot.create(:person) }

    before :each do
      current_user.save!
      @profil_auteur = Profil.create!(profil_name: 'author', name: 'Auteur')
      Profil.create!(profil_name: 'resp_rca', name: 'Responsabe Rca')
      rca.rca_creation_init(current_user)
      rca.reload
      # binding.pry
    end

    it 'set and get current user as author and responsible' do
      expect(rca.rca_author).to eq person1
      expect(rca.rca_first_responsible).to eq person1
      expect(rca.rca_responsibles.size).to eq 1
    end

    it 'add people to team and get it' do
      rca.add_rca_responsible(person2)
      rca.add_person_to_team(person3, 'author')
      rca.add_person_to_team_by_id(person4, @profil_auteur.id)

      expect(rca.tools_teams.size).to eq 5
      expect(rca.rca_responsibles.map(&:person_id)).to contain_exactly(person1.id, person2.id)
      expect(rca.rca_responsible?(person3)).to be false
      expect(rca.rca_responsible?(person2)).to be true

      expect(rca.get_people_from_profil(@profil_auteur.id)).to contain_exactly(person1, person3, person4)

      expect(rca.get_person_from_team(@profil_auteur.id).person).to eq person1
    end
  end

  describe 'advancement' do
    before :each do
      # add step2 to methodology
      # methodology.steps << FactoryBot.create(:meth_step, :with_tool, methodology: methodology)
      @step1 = methodology.steps_menu.first
      @step2 = methodology.steps_menu.second
    end

    context 'without initialization' do
      it 'return 0 for step advancement' do
        expect(rca.advancement(@step1)).to eq 0
      end

      it 'return false for step finished' do
        expect(rca.step_finished?(@step1)).to eq false
      end

      it 'return false for step started' do
        expect(rca.step_started?(@step1)).to eq false
      end

      it 'set initialize rca and set advancement to step' do
        rca.advancement_set(@step2, 2)
        expect(rca.advancement(@step2)).to eq 2
      end

      it 'return the role name of last finished step' do
        expect(rca.advance_role_name).to eq '-'
      end

      it 'return the step name of last finished step' do
        expect(rca.advance_step_name).to eq '-'
      end
    end

    context 'with initialization' do
      before :each do
        rca.initialize_rca_infos
        @rca_advance1 = rca.rca_advances.where(meth_step: @step1).first
        @rca_advance1.update(advance: 2)

        @rca_advance2 = rca.rca_advances.where(meth_step: @step2).first
        @rca_advance2.update(advance: 1)

        @step1.step_role = FactoryBot.create(:meth_step_role, role_name: 'Team', name: 'Team')
        @step1.save!
        @step2.step_role = Meth::StepRole.find_or_create_role_close
        # FactoryBot.create(:meth_step_role, role_name: 'Close', name: 'Close')
        @step2.save!
      end

      it 'return step advancement' do
        expect(rca.advancement(@step1)).to eq 2
        expect(rca.advancement(@step2)).to eq 1
      end

      it 'return true if step is finished' do
        expect(rca.step_finished?(@step1)).to eq true
        expect(rca.step_finished?(@step2)).to eq false
      end

      it 'return true if step is started' do
        expect(rca.step_started?(@step1)).to eq false
        expect(rca.step_started?(@step2)).to eq true
      end

      it 'set advancement to step' do
        rca.advancement_set(@step2, 0)
        expect(rca.advancement(@step2)).to eq 0
        expect(rca.step_advancement(@step2)).to eq 'not started'

        rca.advancement_set(@step2, 2)
        expect(rca.advancement(@step2)).to eq 2
        expect(rca.step_advancement(@step2)).to eq 'finished'
        expect(rca.closed_at).to eq Date.today

        rca.advancement_set(@step2, 1)
        expect(rca.advancement(@step2)).to eq 1
        expect(rca.step_advancement(@step2)).to eq 'in progress'
        expect(rca.closed_at).to eq nil
      end

      it 'return the role name of last finished step' do
        rca.advance_step = @step1
        rca.save!

        expect(rca.advance_role_name).to eq 'Team'
      end

      it 'return the step name of last finished step' do
        rca.advance_step = @step1
        rca.save!

        expect(rca.advance_step_name).to eq @step1.name
      end
    end
  end

  describe 'parent/child (rca-list)' do
    context 'without existing parent relation' do
      before :each do
        rca.initialize_rca_infos
        @rca_parent = FactoryBot.create(:rca, methodology: methodology, title: 'RCA PARENT')
      end

      it 'create rca-lst tool and asociate a parent' do
        rca.set_rca_parent(@rca_parent)

        expect(rca.tools_rca_list&.rca.title).to eq "RCA PARENT"
        expect(@rca_parent.tools_rca_lists&.first&.rcas).to contain_exactly(rca)
      end

      it 'delete association : do nothing' do
        expect { rca.del_rca_parent }.not_to change { rca.tools_rca_list }
      end
    end

    context 'with an existing parent relation' do
      before :each do
        rca.initialize_rca_infos
        @rca_child2 = FactoryBot.create(:rca, methodology: methodology, title: 'RCA CHILD 2')
        @rca_parent = FactoryBot.create(:rca, methodology: methodology, title: 'RCA PARENT')
        @rca_old_parent =FactoryBot.create(:rca, methodology: methodology, title: 'RCA OLD PARENT')

        rca.set_rca_parent(@rca_old_parent)
        rca.reload
      end
      
      it 'create rca-list tool and asociate a parent' do
        # rca.set_rca_parent(@rca_parent)
        expect { rca.set_rca_parent(@rca_parent) }.to change { rca.tools_rca_list&.rca&.title }.from('RCA OLD PARENT').to('RCA PARENT')

        expect(rca.tools_rca_list&.rca.title).to eq "RCA PARENT"
        expect(@rca_parent.tools_rca_lists&.first&.rcas).to contain_exactly(rca)

        expect(@rca_old_parent.tools_rca_lists&.first&.rcas).to be_empty
      end

      it 'add a second child to RCA PARENT' do
        rca.set_rca_parent(@rca_parent)
        @rca_child2.set_rca_parent(@rca_parent)

        expect(@rca_child2.tools_rca_list&.rca.title).to eq "RCA PARENT"
        expect(@rca_parent.tools_rca_lists&.first&.rcas).to contain_exactly(rca, @rca_child2)
        expect(@rca_parent.tools_rca_lists&.size).to eq 1
      end

      it 'delete association (and keep rca-tool)' do
        rca.set_rca_parent(@rca_parent)
        @rca_child2.set_rca_parent(@rca_parent)
        rca.reload

        rca.del_rca_parent
        expect(rca.tools_rca_list).to be_nil
        expect(@rca_parent.tools_rca_lists&.first&.rcas).to contain_exactly(@rca_child2)

        @rca_child2.del_rca_parent
        expect(@rca_parent.tools_rca_lists&.first&.rcas).to be_empty
      end
    end
  end
end
