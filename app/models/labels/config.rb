require_dependency 'labels'

module Labels
  class Config < ApplicationRecord
    translates :name, fallback: :any
    has_one_attached :file

    validates :name, presence: true

    has_many :criteria, dependent: :destroy # , class_name: 'Labels::Criterium', dependent: :destroy
    has_many :tools_criticities, class_name: 'Tools::Criticity'

    scope :usables, -> { where(usable: true) }
  end
end
