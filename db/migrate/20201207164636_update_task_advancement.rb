class UpdateTaskAdvancement < ActiveRecord::Migration[6.0]
  def change
    change_column :tools_tasks, :advancement, :integer, default: 0
  end
end
