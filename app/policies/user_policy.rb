class UserPolicy < AdminPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def send_test_mail?
    true
  end

  def ldap_search?
    create?
  end

  def destroy?
    return false unless index?

    authorize_destroy_roles = true
    record.roles.each do |role|
      authorize_destroy_roles = RolePolicy.new(user, role).role_is_deletable?
      break unless authorize_destroy_roles
    end

    return authorize_destroy_roles
  end

  def update_pa_options?
    return true
  end
end
