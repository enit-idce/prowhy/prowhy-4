FactoryBot.define do
  factory :labels_config, class: Labels::Config do
    sequence(:name) { |n| "config_#{n}" }
  end
end
