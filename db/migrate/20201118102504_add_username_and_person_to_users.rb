class AddUsernameAndPersonToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :username, :string, after: :email
    add_reference :users, :person, foreign_key: true, after: :username
  end
end
