module Meth
  class TreeContentPresenter < BasePresenter
    include ActionView::Helpers::UrlHelper
    include IconsHelper

    # Création de la structure JSon pour jstree
    def tree_line
      ret = "<span id='#{id}_link"  \
            "' data-fct-dnd='#{Rails.application.routes.url_helpers.meth_tree_content_path(id, locale: I18n.locale)}'>"
      ret += name_show
      ret += '<span>'

      ret += "<span style='padding-right: 5px; float: right;'>"

      ret += if ancestry?
               link_to fa_icon_delete_presenter,
                       Rails.application.routes.url_helpers.meth_tree_content_path(id, locale: I18n.locale),
                       method: :delete, remote: true,
                       class: 'icon-right',
                       title: "Delete content #{name.downcase}",
                       data: { confirm: I18n.t('confirm.del_subtree') }
             else
               fa_icon_hidden_presenter('icon-right')
             end
      ret += link_to fa_icon_add_presenter,
                     Rails.application.routes.url_helpers.new_meth_tree_content_path(locale: I18n.locale, meth_tree_content: {parent_id: id, custom_field_desc_id: custom_field_desc_id}),
                     remote: true,
                     class: 'icon-right',
                     title: "Add content after #{name.downcase}"

      ret += if ancestry?
               link_to fa_icon_edit_presenter,
                       Rails.application.routes.url_helpers.edit_meth_tree_content_path(id, locale: I18n.locale),
                       remote: true,
                       class: 'icon-right',
                       title: 'Edit content'
             else
               fa_icon_hidden_presenter('icon-right')
             end
      ret += '</span>'
      return ret
    end

    def tree_infos
      data = { text: tree_line,
               id: id,
               type: 'tree_content',
               a_attr: { style: 'width: calc(100% - 25px);' }
             }

      # data['state'] = {'selected' => false, 'concept_status' => concept.status }
      return data
    end

    def subtree_datas
      data = tree_infos

      unless children.empty?
        data[:children] = []

        children.each do |child|
          data[:children] << TreeContentPresenter.new(child, @view).subtree_datas
        end
      end

      return data
    end
  end
end
