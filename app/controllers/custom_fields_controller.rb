class CustomFieldsController < ApplicationController
  include AdminConcern # à virer plus tard
  # layout 'admin'
  before_action :set_custom_field, only: [:update] # :show, :edit, :update, :destroy]

  # GET /custom_fields
  # GET /custom_fields.json
  def index
    custom_preload_list = Meth::CustomFieldDesc.custom_preload_all_values
    @custom_fields = CustomField.includes(:rca).includes(custom_preload_list).where(rcas: {status: 1})

    # binding.pry
    # , :taggings
    authorize CustomField
  end

  # GET /custom_fields/1
  # GET /custom_fields/1.json
  # def show
  # end

  # GET /custom_fields/new
  # def new
  #   @custom_field = CustomField.new
  #   authorize @custom_field
  # end

  # GET /custom_fields/1/edit
  # def edit
  # end

  # POST /custom_fields
  # POST /custom_fields.json
  # def create
  #   @custom_field = CustomField.new(custom_field_params)
  #   authorize @custom_field

  #   respond_to do |format|
  #     if @custom_field.save
  #       format.html { redirect_to custom_fields_url, notice: 'Custom field was successfully created.' }
  #       format.json { render :show, status: :created, location: @custom_field }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @custom_field.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PATCH/PUT /custom_fields/1
  # PATCH/PUT /custom_fields/1.json
  def update
    authorize @rca, :update?, policy_class: Tools::ToolsPolicy

    # binding.pry

    respond_to do |format|
      if @custom_field.update(custom_field_params)
        format.html do
          if params[:step_tool]
            step_tool = Meth::StepTool.find(params[:step_tool])
            redirect_to meth_step_tool_show_path(id: step_tool.id, rca: @custom_field.rca.id)
          else
            redirect_to custom_fields_url, notice: 'Custom field was successfully updated.'
          end
        end
        format.json { render :show, status: :ok, location: @custom_field }
      else
        format.html { render :edit }
        format.json { render json: @custom_field.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /custom_fields/1
  # DELETE /custom_fields/1.json
  # def destroy
  #   @custom_field.destroy
  #   respond_to do |format|
  #     format.html { redirect_to custom_fields_url, notice: 'Custom field was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  # Modification de la BDD en fonction de la configuration de CustomFieldDesc
  # GET / custom_fields_update_db
  def update_db
    authorize CustomField

    custom_field_descs = Meth::CustomFieldDesc.includes(:custom_field_type)
    custom_fields_desc_names = custom_field_descs.map(&:internal_name)
    actual_columns = CustomField.column_names

    # msg = 'Database updated. <br/>'
    msg = 'Database updated.'

    puts 'Delete unused columns ============================================= BEGIN'.yellow
    actual_columns.each do |column|
      next unless column.start_with?('custom_')

      next if custom_fields_desc_names.include?(column) # Ici vérifier aussi que c'est le bon type (si la colonne a été modifiée)

      msg += "Delete unused column : #{column} <br/>"
      ActiveRecord::Base.connection.remove_column :custom_fields, column.intern
    end

    puts 'Create columns ==================================================== BEGIN'.yellow
    custom_field_descs.each do |cfd|
      # msg += "<br/> Check field : #{cfd.internal_name}"
      next if actual_columns.include?(cfd.internal_name)

      msg += "Add column #{cfd.internal_name} / #{cfd.custom_field_type.field_type} => #{cfd.name} <br/>"
      ActiveRecord::Base.connection.add_column :custom_fields, cfd.internal_name.intern, cfd.custom_field_type.field_type.intern

      if cfd.custom_field_type.field_internal == 'tree'
        ActiveRecord::Base.connection.add_foreign_key :custom_fields, :meth_tree_contents, column: cfd.internal_name.intern, validate: false
      elsif cfd.custom_field_type.field_internal == 'select'
        ActiveRecord::Base.connection.add_foreign_key :custom_fields, :meth_list_contents, column: cfd.internal_name.intern, validate: false
      end
    end
    puts 'Create columns ==================================================== END'.yellow

    CustomField.reset_column_information
    CustomField.finalize

    puts 'RESET COLUMNS INFO ==================================================== END'.yellow
    flash[:notice] = msg.html_safe
    respond_to do |format|
      # format.html { redirect_to custom_fields_url } # notice: msg.html_safe }
      format.html { redirect_to meth_custom_field_descs_url }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_custom_field
    @custom_field = CustomField.find(params[:id])
    @rca = @custom_field.rca

    # authorize @custom_field
  end

  # Only allow a list of trusted parameters through.
  def custom_field_params
    # tab_permit = [:rca_id]
    # Meth::CustomFieldDesc.all.each do |cfield|
    #   tab_permit << if cfield.custom_field_type.field_internal == 'tag'
    #                   cfield.internal_name + '_list'
    #                 else
    #                   cfield.internal_name
    #                 end
    # end
    tab_permit = create_tab_permit_custom
    # puts tab_permit
    tab_permit << {rca_attributes: [:id, :created_at, :updated_at, :closed_at,
                                    rca_advances_attributes: [:id, :date_start, :date_finish]]}
    params.require(:custom_field).permit(tab_permit) # :rca_id)
  end
end
