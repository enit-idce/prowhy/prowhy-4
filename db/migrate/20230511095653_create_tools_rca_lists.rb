class CreateToolsRcaLists < ActiveRecord::Migration[6.1]
  def change
    create_table :tools_rca_lists do |t|
      t.references :rca, null: false, foreign_key: true
      t.integer :tool_reference
      t.string :description

      t.timestamps
    end

    add_reference :rcas, :tools_rca_list, foreign_key: { to_table: :tools_rca_lists }
  end
end
