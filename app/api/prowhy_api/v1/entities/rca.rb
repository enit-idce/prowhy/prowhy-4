module ProwhyApi
  module V1
    module Entities
      class Rca < Grape::Entity
        # format_with :date_ok do |date|
        #   date&.strftime('%Y-%m-%d')
        # end

        expose :id, if: { with_id: true}
        expose :reference
        expose :title # , documentation: { type: 'string', desc: 'Rca tilte.', required: true }
        expose :description, using: Entities::ActionText, merge: true # , documentation: { type: 'string', desc: 'Rca detailed description.' }
        expose :methodology_id
        expose :created_at, format_with: :date_ok # , documentation: { type: 'date', desc: 'Rca creation date.' }
        expose :closed_at, format_with: :date_ok

        # expose custom fields
        expose :custom_field, using: Entities::CustomField, as: :custom_field_attributes, if: { with_custom_field: true } # , merge: true
        # expose :details, merge: true

        expose :tools_teams, using: Entities::ApiTools::Team, as: :teams_attributes, if: { with_teams: true }

        expose :perimeters_list, if: { with_perimeters: true }

        expose :referent, unless: { with_referent: 0 }

        expose :advancement_all, as: :advancement, if: { with_advancement: true }

        private

        def referent
          referent = object.get_person_from_team(options[:with_referent])

          return nil unless referent&.person

          # return {id: referent.person.id, firstname: referent.person.firstname, lastname: referent.person.lastname}
          return {person_id: referent.person.id, profil_id: options[:with_referent]}
        end

        def perimeters_list
          perimeters = object.perimeters.map(&:name)
          return perimeters
        end

        def advancement_all
          ret = {}

          ret[:step] = object.advance_step_name
          ret[:role] = object.advance_role_name
          ret[:duration] = object.advance_duration
          object.rca_advances_used.each do |adv|
            ret["step_#{adv.meth_step.name}"] = {status: adv.advance, date_start: adv.date_start, date_finish: adv.date_finish}
          end

          ret
        end

        # def details
        #   ret = {}
        #   object.methodology.step_info.step_tools_with_list.each do |step_tool|
        #     step_tool.tool_custom_fields.each do |tool_custom_field|
        #       cf_internal_name = tool_custom_field.custom_field_desc.internal_name.intern
        #       cf_name = tool_custom_field.custom_field_desc.name.intern

        #       begin
        #         ret[cf_name] = object.custom_field.send(cf_internal_name)
        #       rescue StandardError
        #         ret[cf_name] = 'This field do not exist in db.'
        #       end
        #     end
        #   end
        #   # puts ret
        #   ret
        # end

        # def details
        #   # details_list = {}
        #   # list_of_fields = %w[first_key second_key third_key]
        #   # list_of_fields.each do |field_name|
        #   #   details_list[field_name.to_sym] = "#{field_name} value" # OR object.send(field_name)
        #   # end
        #   # return details_list
        #   {"#{object.id}_#{object.title}": 'my dynamic value'}
        # end
      end
    end
  end
end
