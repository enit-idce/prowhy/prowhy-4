module Tools
  class QqoqcpValue < ApplicationRecord
    belongs_to :qqoqcp
    belongs_to :labels_qqoqcp_line, class_name: 'Labels::QqoqcpLine'
  end
end
