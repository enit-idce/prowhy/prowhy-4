module IconsHelper
  include FontAwesome5::Rails::IconHelper

  # Icons
  def fa_icon_edit(more_class = '')
    fa_icon('edit', class: "icon-blue #{more_class}")
  end

  def fa_icon_edit_presenter(more_class = '')
    content_tag(:i, '', class: "fa fa-edit icon-blue #{more_class}")
  end

  def fa_icon_list(more_class = '')
    fa_icon('list-alt', type: :regular, class: "icon-blue #{more_class}")
  end

  def fa_icon_delete(more_class = '')
    fa_icon('trash-alt', type: :regular, class: "icon-red #{more_class}")
  end

  def fa_icon_delete_presenter(more_class = '')
    content_tag(:i, '', class: "far fa-trash-alt icon-red #{more_class}")
  end

  def fa_icon_hidden_presenter(more_class = '')
    # fa_icon('edit', style: 'visibility: none;')
    content_tag(:i, '', class: "far fa-trash-alt #{more_class}", style: 'visibility: hidden;')
  end

  def fa_icon_add(more_class = '')
    fa_icon('plus-circle', class: "icon-blue #{more_class}")
  end

  def fa_icon_download(more_class = '')
    fa_icon('download', class: "icon-blue #{more_class}")
  end

  def fa_icon_upload(more_class = '')
    fa_icon('file-upload', class: "icon-blue #{more_class}")
  end

  def fa_icon_add_presenter(more_class = '')
    content_tag(:i, '', class: "fa fa-plus-circle icon-blue #{more_class}")
  end

  def fa_icon_arrow_up(more_class = '')
    fa_icon('arrow-up', class: "icon-blue #{more_class}")
  end

  def fa_icon_arrow_down(more_class = '')
    fa_icon('arrow-down', class: "icon-blue #{more_class}")
  end

  def fa_icon_help(more_class = '')
    fa_icon('question-circle', class: "icon-blue #{more_class}")
  end

  def fa_icon_info(more_class = '')
    fa_icon('info-circle', class: "icon-blue #{more_class}")
  end

  def fa_icon_warning(more_class = '')
    fa_icon('exclamation-triangle', class: "icon-orange #{more_class}")
  end

  def fa_icon_zoom_p(more_class = '')
    fa_icon('search-plus', class: "icon-blue #{more_class}")
  end

  def fa_icon_zoom_m(more_class = '')
    fa_icon('search-minus', class: "icon-blue #{more_class}")
  end

  def fa_icon_open_door(more_class = '')
    fa_icon('door-open', class: "icon-blue #{more_class}")
  end

  def fa_icon_status_presenter(more_class = '')
    content_tag(:i, '', class: "fa fa-circle #{more_class}")
  end

  def fa_icon_pen_presenter(more_class = '')
    content_tag(:i, '', class: "fa fa-pen icon-blue #{more_class}")
  end

  def fa_icon_copy(more_class = '')
    fa_icon('copy', class: "icon-blue #{more_class}")
  end

  def fa_icon_xmark(more_class = '')
    fa_icon('times', class: "icon-red #{more_class}")
  end

  def fa_icon_reschedule(more_class = '')
    fa_icon('redo-alt', class: "icon-blue #{more_class}")
  end

  def fa_icon_reschedule_presenter(more_class = '')
    content_tag(:i, '', class: "fa fa-redo-alt #{more_class}")
  end

end
