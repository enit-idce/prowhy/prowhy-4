require 'rails_helper'

RSpec.describe 'Users', type: :request do
  let(:valid_attributes) { {username: 'toto', email: 'toto@truc.fr', password: 'toto1234', password_confirmation: 'toto1234', person_id: person.id} }
  let(:invalid_attributes) { {username: '', email: 'toto@truc.fr', password: 'toto1234', password_confirmation: 'toto1234', person_id: person.id} }

  let(:user) { FactoryBot.create(:user) }
  let(:person) { FactoryBot.create(:person) }

  before :each do
    sign_in user
  end

  describe 'GET /index' do
    it 'renders a successful response' do
      User.create! valid_attributes
      # binding.pry
      get users_admin_index_url
      expect(response).to be_successful
    end
  end

  # describe 'GET /show' do
  #   it 'renders a successful response' do
  #     user = User.create! valid_attributes
  #     get user_url(user)
  #     expect(response).to be_successful
  #   end
  # end

  describe 'GET /new' do
    context 'without existing person' do
      it 'renders a successful response' do
        get new_users_admin_url
        expect(response).to be_successful
      end
    end

    context 'with existing person' do
      let(:person2) { FactoryBot.create(:person) }
      it 'renders a successful response' do
        get new_users_admin_url(person_id: person2.id)
        expect(response).to be_successful
      end
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      user = User.create! valid_attributes
      get edit_users_admin_url(user)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new User' do
        expect {
          post users_admin_index_url, params: { user: valid_attributes }
        }.to change(User, :count).by(1)
      end

      it 'redirects to the users list' do
        post users_admin_index_url, params: { user: valid_attributes }
        expect(response).to redirect_to(users_admin_index_url)
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new User' do
        expect {
          post users_admin_index_url, params: { user: invalid_attributes }
        }.to change(User, :count).by(0)
      end

      it 'renders a successful response (i.e. to display the new template)' do
        post users_admin_index_url, params: { user: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      let(:new_attributes) { {username: 'jonnhy'} }

      it 'updates the requested user' do
        user = User.create! valid_attributes
        patch users_admin_url(user), params: { user: new_attributes }
        user.reload
        expect(user.username).to eq 'jonnhy'
      end

      it 'redirects to the users list' do
        user = User.create! valid_attributes
        patch users_admin_url(user), params: { user: new_attributes }
        user.reload
        expect(response).to redirect_to(users_admin_index_url)
      end
    end

    context 'with invalid parameters' do
      it 'renders a successful response (i.e. to display the edit template)' do
        user = User.create! valid_attributes
        patch users_admin_url(user), params: { user: invalid_attributes }
        expect(user.username).to eq 'toto'
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested user' do
      user = User.create! valid_attributes
      expect {
        delete users_admin_url(user)
      }.to change(User, :count).by(-1)
    end

    it 'redirects to the users list' do
      user = User.create! valid_attributes
      delete users_admin_url(user)
      expect(response).to redirect_to(users_admin_index_url)
    end
  end
end
