class DataAddReferenceToRca < ActiveRecord::Migration[6.0]
  class MigrationRca < ApplicationRecord
    self.table_name = :rcas
  end

  def up
    MigrationRca.all.each do |rca|
      next if rca.reference

      say "Copy ID in reference for RCA #{rca.title}."
      rca.reference = rca.id.to_s
      rca.save!

      # say "Copy TITLE in reference for RCA #{rca.title}."
      # rca.reference = rca.title
      # rca.save!
    end
  end

  def down
    # raise ActiveRecord::IrreversibleMigration
    say 'Do nothing.'
  end
end
