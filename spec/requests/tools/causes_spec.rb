require 'rails_helper'
# require_relative '../../support/devise'
require_relative '../../support/controller_macros'

RSpec.describe '/tools/causes', type: :request do
  let(:valid_attributes) { {rca_id: rca.id, tool_reference: 1, name: 'Cause 1'} }
  let(:invalid_attributes) { {rca_id: rca.id, tool_reference: 1, name: nil} }

  let(:rca) { FactoryBot.create(:rca, methodology: methodology) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:user) { FactoryBot.create(:user) }
  let(:tools_cause) { FactoryBot.create(:cause, rca: rca) }

  let(:ishikawa) { FactoryBot.create(:ishikawa, rca: rca) }
  let(:cause_type) { FactoryBot.create(:cause_type) }

  # before :each do
  #   activity = Meth::Activity.create!(name: 'Infos')
  #   Meth::Tool.create!([{name: 'Champs personnalisés', activity: activity, tool_name: 'CustomField', tool_options: {} },
  #                       {name: 'PA', activity: activity, tool_name: 'Action', tool_options: {}}])
  #   methodology.create_methodology
  # end

  context 'From logged user' do
    before :each do
      # binding.pry
      user.set_session(:active_step_tool, methodology.steps.first.step_tools.first.id)
      user.save!

      sign_in user
    end

    # describe 'GET /index' do
    #   context 'without rca defined' do
    #     it 'renders a redirection response' do
    #       get '/tools/causes'
    #       # redirection car rca non défini
    #       expect(response).to have_http_status(:redirect)
    #       expect(response).to redirect_to(rcas_path)
    #     end
    #   end

    #   context 'with rca and step_tool defined' do
    #     context 'without cause tree head id defined' do
    #       it 'renders a redirection response' do
    #         get '/tools/causes', params: {rca: rca.id}
    #         # redirection car rca non défini
    #         expect(response).to have_http_status(:redirect)
    #         expect(response).to redirect_to(edit_rca_path(rca))
    #       end
    #     end
    #     context 'with cause tree head id defined' do
    #       it 'renders a successful response' do
    #         get '/tools/causes', params: {id: tools_cause.id, rca: rca.id}
    #         expect(response).to be_successful
    #       end
    #     end
    #   end
    # end

    describe 'GET /new' do
      context 'without rca defined' do
        it 'renders a redirection response' do
          get new_tools_cause_path, params: {}, xhr: true
          expect(response).to have_http_status(:redirect)
          expect(response).to redirect_to(rcas_path)
        end
      end

      context 'with rca and step_tool defined' do
        context 'without parameters' do
          it 'renders a redirection response to edit rca path' do
            get new_tools_cause_path, params: { rca: rca.id }, xhr: true
            expect(response).to have_http_status(:redirect)
            expect(response).to redirect_to(edit_rca_path(rca))
          end
        end
        context 'with valid parameters' do
          it 'renders a successful response' do
            get new_tools_cause_path, params: { tools_cause: valid_attributes, rca: rca.id }, xhr: true
            expect(response).to be_successful
          end
        end
        context 'with valid parameters new from ishikawa' do
          context 'with root cause of ishikawa not defined' do
            it 'renders a successful response' do
              get new_tools_cause_path, params: { tools_cause: valid_attributes, rca: rca.id,
                                                  ishikawa_id: ishikawa.id, cause_type_id: cause_type.id }, xhr: true
              expect(response).to be_successful
            end
          end
          context 'with root cause of ishikawa defined' do
            it 'renders a successful response' do
              ishikawa.assign_root_cause(tools_cause)
              get new_tools_cause_path, params: { tools_cause: valid_attributes, rca: rca.id,
                                                  ishikawa_id: ishikawa.id, cause_type_id: cause_type.id }, xhr: true
              expect(response).to be_successful
            end
          end
        end
      end
    end

    describe 'GET /edit' do
      context 'without rca defined' do
        it 'renders a redirection response' do
          get edit_tools_cause_path(tools_cause.id), params: {}, xhr: true
          expect(response).to be_successful
          # expect(response).to render_template(:new)
        end
      end

      context 'with rca and step_tool defined' do
        it 'renders a successful response' do
          get edit_tools_cause_path(tools_cause.id), params: { tools_cause: valid_attributes, rca: rca.id }, xhr: true
          expect(response).to be_successful
        end
      end
    end

    describe 'GET /edit_status' do
      it 'renders a successful response' do
        tools_cause_child = FactoryBot.create(:cause, rca: rca, parent_id: tools_cause.id)
        get tools_cause_edit_status_path, params: {id: tools_cause_child.id}, xhr: true
        expect(response).to be_successful
      end
    end

    describe 'POST /create' do
      context 'with valid parameters' do
        it 'creates a new Tools::Cause' do
          expect {
            post tools_causes_url, params: { tools_cause: valid_attributes }
          }.to change(Tools::Cause, :count).by(1)
        end

        it 'redirects to the created tools_cause' do
          post tools_causes_url, params: { tools_cause: valid_attributes }
          expect(response).to redirect_to(meth_step_tool_show_path(id: user.get_session(:active_step_tool), rca: rca))
        end
      end

      context 'with invalid parameters' do
        it 'does not create a new Tools::Cause' do
          expect {
            post tools_causes_url, params: { tools_cause: invalid_attributes }, xhr: true
          }.to change(Tools::Cause, :count).by(0)
        end

        it 'renders a successful response' do
          post tools_causes_url, params: { tools_cause: invalid_attributes }, xhr: true
          expect(response).to be_successful
        end
      end
    end

    describe 'PATCH /update' do
      context 'with valid parameters' do
        let(:valid_update_attributes) { {name: 'Cause 2'} }

        it 'updates the requested tools_cause' do
          tools_cause = Tools::Cause.create! valid_attributes
          patch tools_cause_url(tools_cause), params: { tools_cause: valid_update_attributes }
          tools_cause.reload
          # skip('Add assertions for updated state')
          expect(tools_cause.name).to eq('Cause 2')
        end

        it 'redirects to the tools_cause' do
          tools_cause = Tools::Cause.create! valid_attributes
          patch tools_cause_url(tools_cause), params: { tools_cause: valid_update_attributes }
          tools_cause.reload
          expect(response).to redirect_to(meth_step_tool_show_path(id: user.get_session(:active_step_tool), rca: rca))
        end
      end

      context 'with invalid parameters' do
        let(:invalid_update_attributes) { {name: nil} }

        it 'renders a successful response (i.e. to display the edit template)' do
          tools_cause = Tools::Cause.create! valid_attributes
          patch tools_cause_url(tools_cause), params: { tools_cause: invalid_update_attributes }, xhr: true
          expect(response).to be_successful
        end
      end
    end

    describe 'DELETE /destroy' do
      it 'destroys the requested tools_cause' do
        tools_cause2 = Tools::Cause.create! valid_attributes
        expect {
          delete tools_cause_url(tools_cause2)
        }.to change(Tools::Cause, :count).by(-1)
      end

      it 'redirects to the tools_causes list' do
        tools_cause2 = Tools::Cause.create! valid_attributes
        delete tools_cause_url(tools_cause2)
        expect(response).to redirect_to(meth_step_tool_show_path(id: user.get_session(:active_step_tool), rca: rca))
      end
    end

    # describe 'GET /cause_js_tree_data' do
    #   context 'with cause tree head' do
    #     it 'return jstree datas in Json format' do
    #       headers = { 'ACCEPT' => 'application/json' }
    #       get tools_causes_js_tree_data_path, params: {cause_tree_head: tools_cause.id}, headers: headers
    #       expect(response.content_type).to eq('application/json; charset=utf-8')
    #       # ici ce serait d'ajouter un test pour vérifier que le retour json correspond bien au schémas (avec le bon contenu)
    #     end
    #   end

    #   context 'without cause tree head' do
    #     it 'return jstree datas in Json format' do
    #       headers = { 'ACCEPT' => 'application/json' }
    #       get tools_causes_js_tree_data_path, params: {}, headers: headers
    #       expect(response.content_type).to eq('application/json; charset=utf-8')
    #       expect(response.body).to eq '[{"text":"no tree"}]'
    #     end
    #   end
    # end
  end
end
