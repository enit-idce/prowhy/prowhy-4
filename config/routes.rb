Rails.application.routes.draw do

  # use_doorkeeper
  scope 'api' do
    use_doorkeeper do
      skip_controllers :authorizations, :applications, :authorized_applications
    end
  end

  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'

  mount ProwhyApi::Api => '/api'

  scope '(:locale)', locale: /#{I18n.available_locales.join("|")}/ do
    # mount ProwhyApi::Api => '/api'

    # your routes here...
    namespace :labels do
      resources :cause_types
      get 'cause_types_list', to: 'cause_types#cause_types_list'
      post 'cause_types_list_set', to: 'cause_types#cause_types_list_set'
      resources :criteria
      resources :configs
      get 'config_add_criteria', to: 'configs#add_criteria'
      patch 'config_add_criteria_valid', to: 'configs#add_criteria_valid'
      get 'config_formula_test', to: 'configs#formula_test'
      resources :values
      resources :qqoqcp_lines
      resources :qqoqcp_configs
      get 'qqoqcp_config_add_line', to: 'qqoqcp_configs#add_line'
      patch 'qqoqcp_config_add_line_valid', to: 'qqoqcp_configs#add_line_valid'
      resources :check_list_elems
    end

    # scope module: 'tools' do
    namespace :tools do
      resources :qqoqcps
      resources :causes, only: [:new, :create, :edit, :update, :destroy]
      get 'cause_edit_status', to: 'causes#edit_status'
      resources :tasks
      resources :decision_mats
      # Cause Tree
      resources :cause_trees, only: [:index]
      get 'cause_trees_js_tree_data', to: 'cause_trees#cause_trees_js_tree_data'
      # Cause Tree => 5Why
      get 'cause_trees_index_5why', to: 'cause_trees#index_5why'
      # Ishikawa
      resources :ishikawas, only: [:index]
      get 'ishikawa_js_tree_data', to: 'ishikawas#ishikawa_js_tree_data'
      post 'ishikawa_drag_cause', to: 'ishikawas#ishikawa_drag_cause'
      get 'ishikawa_reset_branchs', to: 'ishikawas#ishikawa_reset_branchs'
      get 'ishikawa_zoom', to: 'ishikawas#ishikawa_zoom'
      post 'ishikawa_update_option', to: 'ishikawas#ishikawa_update_option'
      post 'save_canvas_to_png', to: 'ishikawas#save_canvas_to_png'
      post 'save_image', to: 'ishikawas#save_image'
      # Tasks
      get 'tasks_index_js', to: 'tasks#index_js'
      get 'tasks_export_pdf', to: 'tasks#export_pdf'
      get 'task_get_cause', to: 'tasks#task_get_cause'
      patch 'task_update_advance', to: 'tasks#update_advance'
      get 'tasks_send_emails', to: 'tasks#send_emails'
      post 'tasks_index_posts', to: 'tasks#index'
      get 'tasks_index_posts', to: 'tasks#index'

      get 'tasks_check_list', to: 'tasks#check_list'
      post 'tasks_check_list_valid', to: 'tasks#check_list_valid'

      get 'tasks_reschedule', to: 'tasks#reschedule'

      # Decision Mats
      # get 'decision_mat_open', to: 'decision_mats#open'
      patch 'decision_mat_save_values', to: 'decision_mats#save_values'
      delete 'decision_mat_delete_entry', to: 'decision_mats#delete_entry'
      get 'decision_mat_add_entry', to: 'decision_mats#add_entry'
      patch 'decision_mat_valid_entries', to: 'decision_mats#valid_entries'
      get 'decision_mat_add_criteria', to: 'decision_mats#add_criteria'
      patch 'decision_mat_valid_criteria', to: 'decision_mats#valid_criteria'
      # Teams
      resources :teams
      # Documents
      resources :documents
      # RcaRport
      resources :rca_reports, only: [:index]
      get 'rca_report_export', to: 'rca_reports#rca_report_export'
      get 'rca_report_comm', to: 'rca_reports#rca_report_comm'
      get 'rca_report_add_person', to: 'rca_reports#rca_report_add_person'
      get 'rca_report_add_person_do', to: 'rca_reports#rca_report_add_person_do'
      # Criticity
      resources :criticities
      patch 'criticity_calculate', to: 'criticities#calculate'
      # Images
      resources :images
      # Indicators
      resources :indicators
      post 'indicator_save_image', to: 'indicators#save_image'
      resources :indicator_measures
      patch 'indicator_measure_up', to: 'indicator_measures#indicator_measure_up'
      patch 'indicator_measure_down', to: 'indicator_measures#indicator_measure_down'
      # Check Efficacity
      resources :check_efficacity, only: [:index]
      get 'check_efficacity_edit_efficacity', to: 'check_efficacity#edit_efficacity'
      # Efficacity
      resources :efficacities
      # Generic
      resources :generics
      get 'generics_download_last_doc', to: 'generics#download_last_doc'
      # RcaList
      resources :rca_lists
    end

    namespace :meth do
      resources :tools
      resources :activities
      resources :list_contents # , only: [:index]
      resources :custom_field_descs
      resources :custom_field_types
      resources :steps
      resources :methodologies
      resources :step_tools
      resources :tool_custom_fields
      get 'step_tool_show', to: 'step_tools#step_tool_show'
      get 'step_tool_import', to: 'step_tools#step_tool_import'
      post 'step_tool_import_do', to: 'step_tools#step_tool_import_do'
      # Fonctions step à mettre dans des services
      get 'step_import', to: 'steps#step_import'
      post 'step_import_do', to: 'steps#step_import_do'
      get 'step_copy', to: 'steps#step_copy'
      post 'step_copy_do', to: 'steps#step_copy_do'
      delete 'step_del_tool', to: 'steps#step_del_tool'
      post 'step_add_tool', to: 'steps#step_add_tool'
      patch 'step_tool_up', to: 'steps#step_tool_up'
      patch 'step_tool_down', to: 'steps#step_tool_down'
      patch 'step_up', to: 'steps#step_up'
      patch 'step_down', to: 'steps#step_down'

      get 'tool_active', to: 'tool#tool_active'

      patch 'list_content_up', to: 'list_contents#list_content_up'
      patch 'list_content_down', to: 'list_contents#list_content_down'
      get 'list_content_new_from_csv', to: 'list_contents#new_from_csv'
      post 'list_content_create_from_csv', to: 'list_contents#create_from_csv'
      get 'list_content_export_csv', to: 'list_contents#export_csv'
      get 'list_content_export_save_csv', to: 'list_contents#export_save_csv'

      delete 'custom_field_desc_delete_all_list', to: 'custom_field_descs#delete_all_list'
      get 'custom_field_desc_dependant_fields_refresh', to: 'custom_field_descs#dependant_fields_refresh'
      get 'custom_field_desc_update_list_dependencies', to: 'custom_field_descs#update_list_dependencies'

      get 'show_tool_custom_field', to: 'tool_custom_fields#show_tool'
      patch 'tool_custom_field_up', to: 'tool_custom_fields#tool_custom_field_up'
      patch 'tool_custom_field_down', to: 'tool_custom_fields#tool_custom_field_down'

      get 'methodology_edit_meth', to: 'methodologies#edit_meth'
      post 'methodology_copy', to: 'methodologies#copy'

      # Tree Cntent for Custom fields
      resources :tree_contents
      get 'tree_contents_js_tree_data', to: 'tree_contents#js_tree_data'
    end

    namespace :admin do
      resources :admin_configs, only: [:index, :edit, :update]
      delete 'admin_config_delete_customer', to: 'admin_configs#delete_customer'
      resources :ldap_configs
      get 'ldap_config_test', to: 'ldap_configs#ldap_config_test'
      resources :perimeters
      get 'perimeters_assign_users', to: 'perimeters#assign_users'
      get 'perimeters_js_tree_data', to: 'perimeters#js_tree_data'
      patch 'perimeters_assign_users_valid', to: 'perimeters#assign_users_valid'
      resources :helper_texts
    end

    resources :custom_fields
    devise_for :users, controllers: { registrations: 'users/registrations',
                                      sessions: 'users/sessions' }
    devise_scope :user do
      get 'edit_user_preferences_registration', to: 'users/registrations#edit_preferences'
      patch 'update_user_preferences_registration', to: 'users/registrations#update_preferences'
    end
    resources :users_admin, controller: 'users', only: [:index, :new, :create, :edit, :update, :destroy]
    get 'users_admin_send_test_mail', to: 'users#send_test_mail'
    get 'users_admin_ldap_search', to: 'users#ldap_search'
    post 'users_admin_ldap_search_valid', to: 'users#ldap_search_valid'
    patch 'users_admin_update_pa_options', to: 'users#update_pa_options'
    resources :rcas
    patch 'rca_advancement_set', to: 'rcas#advancement_set'
    patch 'rca_reinit_all_advancements', to: 'rcas#reinit_all_advancements'
    get 'rca_closure', to: 'rcas#rca_closure'
    # patch 'rcas_search', to: 'rcas#rcas_search'
    patch 'rcas', to: 'rcas#index'
    patch 'rcas_reinit_filters_index', to: 'rcas#reinit_filters_index'
    get 'rcas_reinit_filters', to: 'rcas#reinit_filters'
    patch 'rcas_items_pp', to: 'rcas#rcas_items_pp'
    # get 'rcas_action_plan', to: 'rcas#rcas_action_plan'
    get 'rcas_assign_perimeters', to: 'rcas#assign_perimeters'
    patch 'rcas_assign_perimeters_valid', to: 'rcas#assign_perimeters_valid'
    get 'rca_recovery', to: 'rcas#recovery'
    patch 'rca_recovery_do', to: 'rcas#recovery_do'
    get 'rca_export_ods', to: 'rcas#export_ods'
    get 'rca_add_parent', to: 'rcas#rca_add_parent'
    patch 'rca_add_parent_valid', to: 'rcas#rca_add_parent_valid'
    delete 'rca_delete_parent', to: 'rcas#rca_delete_parent'

    resources :posts
    resources :tags, only: [:index, :show, :update]
    resources :admin, only: [:index]
    get 'admin_show_ahoy', to: 'admin#show_ahoy'
    resources :people
    get 'person_choose', to: 'people#person_choose'
    get 'person_choose_valid', to: 'people#person_choose_valid'
    resources :profils
    patch 'profil_raz_authorizations', to: 'profils#raz_authorizations'
    resources :profil_authorizes, only: [:index, :create, :update]
    resources :roles, only: [:index, :create]

    get 'tagged/:tag', to: 'posts#index', as: :tagged

    get 'tags_list', to: 'tags#tags_list'
    get 'tags_js_tree_data', to: 'tags#tags_js_tree_data'

    # get 'create_route_js', to: 'rcas#create_route_js'

    # get 'causes_js_tree_data', to: 'causes#causes_js_tree_data'

    resources :home, only: [:index]
    # get 'step_menu_tool', to: 'rcas#step_menu_tool'

    get 'custom_fields_update_db', to: 'custom_fields#update_db'

    # Home page
    get 'home_prowhy_version', to: 'home#prowhy_version'
    # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
    #
    root to: 'rcas#index'
    # root to: 'home#index'
  end
end
