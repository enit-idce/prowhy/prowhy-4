class CreateToolsCauseLinks < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_cause_links do |t|
      t.references :cause, null: false, foreign_key: { to_table: :tools_causes }
      t.references :cause_tool, polymorphic: true, null: false

      t.timestamps
    end
  end
end
