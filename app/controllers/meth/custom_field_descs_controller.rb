module Meth
  class CustomFieldDescsController < ApplicationController
    include AdminConcern
    # layout 'admin'
    before_action :set_meth_custom_field_desc, only: [:show, :edit, :update, :destroy, :delete_all_list]

    # GET /custom_field_descs
    # GET /custom_field_descs.json
    def index
      @custom_field_descs = CustomFieldDesc.all
      authorize CustomFieldDesc
    end

    # GET /custom_field_descs/1
    # GET /custom_field_descs/1.json
    # def show
    # end

    # GET /custom_field_descs/new
    def new
      @custom_field_desc = CustomFieldDesc.new
      authorize @custom_field_desc
    end

    # GET /custom_field_descs/1/edit
    def edit
    end

    # POST /custom_field_descs
    # POST /custom_field_descs.json
    def create
      @custom_field_desc = CustomFieldDesc.new(meth_custom_field_desc_params)
      authorize @custom_field_desc

      @custom_field_desc.internal_name = 'internal_name_for_create'
      # @custom_field_desc.save!
      # @custom_field_desc.internal_name = "custom_#{@custom_field_desc.id}"

      respond_to do |format|
        if @custom_field_desc.save
          # Create internal_name
          internal_id = @custom_field_desc.id
          internal_name_new = "custom_#{internal_id}"
          until CustomFieldDesc.where(internal_name: internal_name_new).empty?
            internal_id += 1
            internal_name_new = "custom_#{internal_id}"
          end

          @custom_field_desc.internal_name = internal_name_new # "custom_#{@custom_field_desc.id}"
          @custom_field_desc.save!

          if @custom_field_desc.custom_field_type.field_internal == 'tree' && @custom_field_desc.tree_contents.empty?
            Meth::TreeContent.create!(custom_field_desc_id: @custom_field_desc.id, ancestry: nil,
                                      name_fr: @custom_field_desc.internal_name, name_en: @custom_field_desc.internal_name)
          end

          format.html { redirect_to meth_custom_field_descs_url, notice: t('.message_succes', cust_field: CustomField.model_name.human) }
          format.json { render :show, status: :created, location: @custom_field_desc }
        else
          format.html do
            # @custom_field_desc.destroy
            render :new
          end
          format.json { render json: @custom_field_desc.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /custom_field_descs/1
    # PATCH/PUT /custom_field_descs/1.json
    def update
      # binding.pry

      respond_to do |format|
        # if @custom_field_desc.update(meth_custom_field_desc_params)
        if @custom_field_desc.update_with_ref_values(meth_custom_field_desc_params, params['meth_custom_field_desc']['ref_show_values'])
          format.html { redirect_to meth_custom_field_descs_url, notice: t('.message_succes', cust_field: CustomField.model_name.human) }
          format.json { render :show, status: :ok, location: @custom_field_desc }
        else
          format.html { render :edit }
          format.json { render json: @custom_field_desc.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /custom_field_descs/1
    # DELETE /custom_field_descs/1.json
    def destroy
      @custom_field_desc.destroy
      respond_to do |format|
        format.html { redirect_to meth_custom_field_descs_url, notice: t('.message_succes', cust_field: CustomField.model_name.human) }
        format.json { head :no_content }
      end
    end

    # DELETE /delete_all_list
    def delete_all_list
      @custom_field_desc.list_contents.destroy_all

      respond_to do |format|
        format.html { redirect_to meth_list_contents_url, notice: t('.message_succes', list_content: ListContent.model_name.human) }
        format.json { head :no_content }
      end
    end

    # List Content Filtered
    def dependant_fields_refresh
      skip_authorization
      puts "OK IN FUNCTION ID=#{params[:id]} CUSTOM_FIELD=#{params[:custom_field_id]} VALUE=#{params[:value]}".yellow

      @custom_field_desc = CustomFieldDesc.find(params[:id])
      @custom_field_descs = @custom_field_desc.dependant_fields
      # @custom_field = CustomField.find(params[:custom_field_id])
      # @value = params[:value]

      json_options = {fields: []}
      @custom_field_descs.each do |cf_desc|
        # List Content
        le_field = { internal_name: cf_desc.internal_name,
                     options: [{value: '', text: ''}],
                     type: cf_desc.custom_field_type.field_internal }

        # Custom field
        if cf_desc.ref_show_values
          display_value = cf_desc.ref_show_values.include?(params[:value]&.to_i) ? 'block' : 'none'
          le_field[:display] = display_value
        end

        # List Content
        sort_type = cf_desc.options&.[]('order') == 'alpha' ? 'name' : 'pos'
        list_contents = cf_desc.list_contents.where(ref_value: [0, params[:value]]).sort { |a, b| a.send(sort_type) <=> b.send(sort_type) }
        list_contents.each do |content|
          le_field[:options] << {value: content.id, text: content.name}
        end
        json_options[:fields] << le_field
      end

      # binding.pry
      puts 'RETURN VALUES ============================================>'.yellow
      puts json_options
      puts '==========================================================>'.yellow

      respond_to do |format|
        format.json { render json: json_options }
      end
    end

    def update_list_dependencies
      skip_authorization

      custom_field_desc = CustomFieldDesc.find(params[:value])
      json_options = {}

      if custom_field_desc
        json_options = custom_field_desc.list_contents.map { |lc| [lc.name, lc.id] }
      end

      respond_to do |format|
        format.json { render json: json_options }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_meth_custom_field_desc
      @custom_field_desc = CustomFieldDesc.find(params[:id])
      authorize @custom_field_desc
    end

    # Only allow a list of trusted parameters through.
    def meth_custom_field_desc_params
      params.require(:meth_custom_field_desc).permit(:custom_field_type_id, :internal_name, :name, :name_fr, :name_en, :show_list, :show_search,
                                                     :ref_field_id, :ref_show_values,
                                                     options: [:order])
    end
  end
end
