require 'rails_helper'

RSpec.describe Tools::MatrixStat, type: :model do
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to element' do
        assc = Tools::MatrixStat.reflect_on_association(:element)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end
end
