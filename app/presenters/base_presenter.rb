class BasePresenter < SimpleDelegator
  def initialize(model, view)
    @model = model
    @view = view
    super(model)
  end

  def h
    @view
  end

  # private

  # def h
  #   ApplicationController.helpers
  # end
end
