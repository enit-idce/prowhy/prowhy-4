require_dependency 'tools'

module Tools
  class Generic < ApplicationRecord
    include ToolConcern

    belongs_to :rca

    has_one_attached :doc
    has_many :file_versions, -> { order(date: :desc) }, as: :element, dependent: :destroy

    has_many_attached :images

    before_destroy :purge_files_and_images

    def file
      # retourne la version la plus récente (version en cours) de files
      # Tester : files.last
    end

    # Create & Search

    def self.create_default!(options, step_tool)
      generic = create!(options)

      generic.doc.attach(step_tool.generic_file.blob) if step_tool&.generic_file&.attached?

      generic
    end

    private

    def purge_files_and_images
      doc&.purge

      images.each(&:purge)
    end
  end
end
