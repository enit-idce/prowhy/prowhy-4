module Tools
  class QqoqcpPolicy < ToolsPolicy
    attr_reader :user, :record, :rca

    def edit?
      show?
    end
  end
end
