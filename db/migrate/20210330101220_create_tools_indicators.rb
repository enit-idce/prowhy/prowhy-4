class CreateToolsIndicators < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_indicators do |t|
      t.references :rca, null: false, foreign_key: true
      t.integer :tool_reference, default: 1
      t.integer :tool_num, default: 1
      t.string :name
      t.text :description
      t.integer :target
      t.json :graph_options

      t.timestamps
    end
  end
end
