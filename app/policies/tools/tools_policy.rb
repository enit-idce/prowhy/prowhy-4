# Tools policy :
# Si admin ou manager => tous les droits
# Sinon : si membre de la team => tous les droits
# Sinon : si user : droits en lecture
module Tools
  class ToolsPolicy < RcaPolicy
    attr_reader :user, :record, :rca

    def initialize(user, record)
      @user = user
      @record = record
      # binding.pry
      @rca = record.nil? || record.class == Class ? nil : (record.class == Rca ? record : record.rca)
      # binding.pry
    end

    def index?
      # true in rca_policy
      show?
    end

    def create?
      update? # update from RcaPolicy
      # user.has_any_role?(:admin, :super_admin, :manager) || rca.people.include?(user.person)
    end

    def edit?
      update?
    end

    def destroy?
      update?
    end

    class Scope
      def initialize(user, scope)
        @user  = user
        @scope = scope
      end

      def resolve
        scope.all
      end

      private

      attr_reader :user, :scope
    end

  end
end
