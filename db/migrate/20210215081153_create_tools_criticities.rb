class CreateToolsCriticities < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_criticities do |t|
      t.references :rca, null: false, foreign_key: true
      t.integer :tool_reference
      t.integer :tool_num, default: 1

      t.references :labels_config, null: false, foreign_key: true
      t.integer :value

      t.timestamps
    end
  end
end
