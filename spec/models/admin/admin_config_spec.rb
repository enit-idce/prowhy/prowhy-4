require 'rails_helper'

RSpec.describe Admin::AdminConfig, type: :model do
  subject(:admin_config) do
    Admin::AdminConfig.create!
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to default usre role' do
        assc = Admin::AdminConfig.reflect_on_association(:default_user_role)
        expect(assc.macro).to eq :belongs_to
      end
    end

    describe 'Validations' do
      it 'is valid after creation (options are present)' do
        admin_config
        expect(admin_config.connexions.class).to eq Hash
        expect(admin_config.task_mails.class).to eq Hash
      end
    end
  end

  describe 'Get attributes' do
    it 'returns true or false for connexion attribute' do
      admin_config.connexions = {database_login: 'true', database_signup: 'false', ldap_login: 'true'}

      admin_config.save!
      admin_config.reload

      expect(admin_config.connexion_attribute(:database_login)).to eq true
      expect(admin_config.connexion_attribute(:database_signup)).to eq false
    end
    it 'returns true or false for task mail attribute' do
      admin_config.task_mails = { activate: 'false',
                                  dday: 'true',
                                  late: 2,
                                  ndays: [1, 2, 7] }
      admin_config.save!
      admin_config.reload

      expect(admin_config.task_mail_attribute(:activate)).to eq false
      expect(admin_config.task_mail_attribute(:dday)).to eq true
      expect(admin_config.task_mail_attribute(:late)).to eq 2
    end
  end
end
