class AddUsableToMethodology < ActiveRecord::Migration[6.0]
  def change
    add_column :meth_methodologies, :usable, :boolean, default: true, after: :locked
  end
end
