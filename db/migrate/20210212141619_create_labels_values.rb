class CreateLabelsValues < ActiveRecord::Migration[6.0]
  def change
    create_table :labels_values do |t|
      t.references :criterium, null: false, foreign_key: { to_table: :labels_criteria }
      t.integer :value, default: 0
      t.string :title_fr
      t.string :title_en
      t.text :text_fr
      t.text :text_en

      t.timestamps
    end
  end
end
