module Users
  class SessionsController < Devise::SessionsController
    # GET /resource/sign_in
    # def new
    #   super
    # end
    after_action :call_track_action, only: [:update]

    def create
      puts 'HERE IN MY CREATE SESSION CONTROLLER !!!!!!!!!!!!!!!!!!!'.yellow

      connected = false
      ldap_connection = false

      admin_config = Admin::AdminConfig.first
      admin_config ||= Admin::AdminConfig.create!

      # Search if user is in DB
      @user = User.where(username: params[:user][:login]).or(User.where(email: params[:user][:login]))&.first

      if @user
        if @user.ldap_config
          ldap_connection = true

          if admin_config.connexion_attribute(:ldap_login) == true
            puts 'LDAP User. Try to connect.'.yellow

            if @user.ldap_config.connect_and_auth(params[:user][:login], params[:user][:password])
              puts 'Connexion ldap ok'.green
              params[:user][:password] = @user.encrypted_password
              sign_in(@user) # Fonction issue de devise/lib/devise/controllers/helpers.rb : sign_in sans passer par l'authentification.
              connected = true
            end
          end
        end
      elsif admin_config.connexion_attribute(:ldap_login) == true
        puts 'User does not exist in database. Try to create from LDAP.'.yellow
        Admin::LdapConfig.all.each do |ldap|
          next unless ldap.authorize_signup

          next unless (ldap_con = ldap.connect_and_auth(params[:user][:login], params[:user][:password]))

          puts 'Connexion ldap ok => nouvel utilisateur'.green
          ldap_user = ldap_con.first
          @user = User.create!(username: ldap.att(ldap_user, 'uid'),
                               email: ldap.att(ldap_user, 'email'),
                               password: '********',
                               password_confirmation: '********',
                               ldap_config: ldap,
                               person_attributes: {firstname: ldap.att(ldap_user, 'firstname'),
                                                   lastname: ldap.att(ldap_user, 'lastname')})

          # binding.pry
          next unless @user

          params[:user][:password] = @user.encrypted_password
          sign_in(@user)
          connected = true
        end
      end

      if connected
        puts 'Connection with LDAP succes !'.green
        respond_with :user, location: after_sign_in_path_for(:user)
        # TODO: AuthTrail => trouver une meilleure solution que update pour avoir la bonne strategy.
        LoginActivity.where(identity: @user.email, success: 1)&.last&.update(strategy: 'ldap_connexion')
      elsif ldap_connection
        # AuthTrail : enregistre l'échec de connexion.
        AuthTrail.track(strategy: 'ldap_connection',
                        scope: 'user',
                        identity: params[:user][:login],
                        success: 0,
                        request: request,
                        user: @user,
                        failure_reason: 'invalid')
        puts 'Invalid connexion with LDAP'.red
        flash[:error] = 'Invalid connection to LDAP'
        redirect_to new_user_session_path
      else
        puts 'Not connected from LDAP. Try to connect to locale DB.'.red
        if admin_config.connexion_attribute(:database_login) == true
          super
        else
          # TODO : ici faire un message spécifique type ce mode de connexion est bloqué, contactez l'administrateur
          # respond_with :user, location: after_sign_in_path_for(:user)
          flash[:error] = 'Database connexion not authorized.'
          redirect_to new_user_session_path
        end
      end

      # if current_user
      #   puts "ça y vient ici !!!!!!!!!!!==========================+>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>".yellow
      #   I18n.locale = current_user&.get_preference(:locale) || I18n.default_locale
      #   binding.pry
      # end
    end

    # protected

    # def create_from_ldap
    #   ldap = @user.ldap_config

    #   if ldap.connect_and_auth(params[:user][:login], params[:user][:password])
    #     puts 'Connexion ldap ok'
    #     params[:user][:password] = @user.encrypted_password
    #     sign_in(@user) # Fonction issue de devise/lib/devise/controllers/helpers.rb : sign_in sans passer par l'authentification.
    #     respond_with :user, location: after_sign_in_path_for(:user)
    #   else
    #     raise 'invalid'
    #   end
    # end
  end
end
