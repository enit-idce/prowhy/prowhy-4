class AddClosedAtToRcas < ActiveRecord::Migration[6.0]
  def change
    add_column :rcas, :closed_at, :date, after: :updated_at
  end
end
