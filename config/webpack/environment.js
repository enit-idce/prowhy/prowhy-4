const { environment } = require('@rails/webpacker')
const erb = require('./loaders/erb')

const webpack = require('webpack')
environment.plugins.append('Provide', new webpack.ProvidePlugin({
  $: 'jquery',
  jQuery: 'jquery',
  // window.jQuery: 'jquery',
  Popper: ['popper.js', 'default'],
  moment: 'moment/moment'
}))

const aliasConfig = {
    'jquery': 'jquery-ui-dist/external/jquery/jquery.js',
    'jquery-ui': 'jquery-ui-dist/jquery-ui.js'
};

environment.config.set('resolve.alias', aliasConfig);

// Preventing Babel from transpiling NodeModules packages => for jstree
environment.loaders.delete('nodeModules');

environment.loaders.prepend('erb', erb)
module.exports = environment


// Configuration pour permettre d'exporter les packs et de les utiliser dans les views directement.
environment.config.merge({
  output: {
    library: ['Packs', '[name]'], // exports to "Packs.application" from application pack
    libraryTarget: 'var',
  }
})
