FactoryBot.define do
  factory :profil do
    sequence(:profil_name) { |n| "profil#{n}" }
    sequence(:name_en) { |n| "Profil #{n}" }
  end
end
