module Admin
  class PerimeterPolicy < AdminPolicy
    attr_reader :user, :record

    def initialize(user, record)
      @user = user
      @record = record
    end

    def assign_users?
      edit?
    end

    def js_tree_data?
      true
    end
  end
end
