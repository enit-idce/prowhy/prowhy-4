FactoryBot.define do
  factory :meth_tool, class: Meth::Tool do
    name_fr { 'Tool 1' }
    tool_name { 'ToolName1' }
    association :activity, factory: :meth_activity

    trait :custom_field do
      name_fr { 'Custom Tool' }
      tool_name { 'CustomField' }
    end

    trait :task do
      name_fr { 'Actions' }
      tool_name { 'Action' }
    end

    trait :cause do
      name_fr { 'Cause tree' }
      tool_name { 'Cause' }
    end

    trait :ishikawa do
      name_fr { 'Ishikawa' }
      tool_name { 'Ishikawa' }
    end

    trait :qqoqcp do
      name_fr { 'Qqoqcp' }
      tool_name { 'Qqoqcp' }
    end
  end
end
