module ProwhyApi
  module V1
    module Entities
      module ApiMeth
        class ListContent < Grape::Entity
          expose :id
          expose :name
        end
      end
    end
  end
end
