module Tools
  class RcaReportsController < ApplicationController
    include ProcessConcern
    before_action :init_rca, only: [:index, :rca_report_comm, :rca_report_add_person, :rca_report_add_person_do]
    # layout 'pdf'

    after_action :call_track_action, only: [:index]

    def call_track_action
      return unless @export_pdf == true

      params[:controller] = 'report'
      params[:action] = 'ExportPDF'
      super
    end

    def index
      # authorize Rca
      skip_authorization

      redirect_to rcas_path and return if @rca.nil? || current_user.get_session(:active_step_tool).nil?

      # authorize @rca # TODO : à améliorer
      authorize @rca, :show?, policy_class: Tools::ToolsPolicy

      set_all_export_values
      @export_pdf = false

      respond_to do |format|
        format.html do
          @export_docker = false
          render 'index' # , locals: {export_values: export_values}
        end
        format.pdf do
          @export_pdf = true
          params['page_format'] ||= {'orientation': 'Portrait', 'page_size': 'A4'}

          # call_track_action(controller: 'report', action: 'ExportPDF')

          render pdf: 'report_pdf', # Excluding ".pdf" extension.
                 # disposition: 'attachment',
                 layout: 'layouts/pdf',
                 viewport_size: '1280x1024',
                 orientation: params['page_format']['orientation'], # 'Landscape', # 'Portrait'
                 page_size: params['page_format']['page_size'],
                 margin: { left: 6, right: 6 },
                 footer: {
                   # right: 'page [page] / [topage]'
                   html: {  template:'tools/rca_reports/footer_template',
                            formats: [:html],
                            locals:  { truc: 1 } },
                 },
                 template: 'tools/rca_reports/index',
                 formats: [:html],
                 show_as_html: false,
                 encoding: 'utf8'

          # V2 TEST
          # cmd = 'http://http://localhost/prowhy/fr/tools/rca_reports?rca=1&step_tool=38'

          # html_output = render_to_string(template: 'tools/rca_reports/index.html', layout: 'pdf')

          # command = "wkhtmltopdf --load-error-handling skip --encoding utf-8 - " + 'titi.pdf'

          # IO.popen(command, 'r+') do |f|
          #   # Writing the html previously rendered in a string
          #   f.write(html_output) # Ecriture de la sortie de la commande dans le fichier name_pdf (dans public/pdf/user)
          #   f.close_write
          # end

          # V1-2 TEST
          # html = render_to_string template: 'tools/rca_reports/index.html.erb', layout: 'pdf'
          # file_name = 'report_pdf'

          # pdf = WickedPdf.new.pdf_from_string(html)

          # save_path = Rails.root.join("public/export/", "#{file_name}.pdf")

          # File.open(save_path, 'wb') do |file|
          #   file << pdf
          # end

          # redirect_to finish_download_content_index_path, notice: t('download_link_will_be_sent_to_your_email')
        end
      end
    end

    def rca_report_export
    end

    def rca_report_comm
      authorize @rca, :communication?

      if (params[:mail][:pdf])
        @export_pdf = true
        set_all_export_values # (params)
        pdf_attached = WickedPdf.new.pdf_from_string(render_to_string(template: 'tools/rca_reports/index',
                                                                      layout: 'pdf',
                                                                      encoding: 'utf8'),
                                                     footer: {
                                                      content: render_to_string(
                                                                  template: 'tools/rca_reports/footer_template',
                                                                  layout: 'pdf'),
                                                     },
                                                     encoding: 'utf8')
      else
        pdf_attached = nil
      end

      # # Pour test
      # save_path = Rails.root.join("public/exports/", "test_pdf.pdf")

      # File.open(save_path, 'wb') do |file|
      #   file << pdf_attached
      # end
      # # End Tests

      SendMails.rca_report_send_mail(params[:person].keys, @rca, params[:mail], pdf_attached)

      respond_to do |format|
        format.js { render 'shared/email_response', locals: {message: 'Success : Email(s) sent.', color: '#282'} }
      end
    rescue StandardError => e
      respond_to do |format|
        puts 'ERROR'.red
        puts e
        puts 'OK'.red
        format.js { render 'shared/email_response', locals: {message: "Error : #{e.to_s.gsub(/\R+/, ' ')}.", color: '#922'} }
      end
    end

    # =================================== Test pour exporter les PDF des sous-rca (tool rca-lists)
    # def rca_report_comm
    #   authorize @rca, :communication?

    #   all_pdf_attached = []
    #   if (params[:mail][:pdf])
    #     @export_pdf = true
    #     set_all_export_values # (params)
    #     all_pdf_attached << WickedPdf.new.pdf_from_string(render_to_string(template: 'tools/rca_reports/index.html.erb',
    #                                                                        layout: 'pdf',
    #                                                                        encoding: 'utf8'),
    #                                                       encoding: 'utf8')

    #     # all_pdf_attached = [pdf_attached]
    #     puts 'Wait 5 seconds please...........................................'.green
    #     sleep(5)

    #     le_rca = @rca
    #     # binding.pry
    #     le_rca.tools_rca_lists&.includes([:rcas])&.each do |rca_list|
    #       rca_list&.rcas&.each do |sub_rca|
    #         @rca = sub_rca
    #         puts "Generating PDF for sub RCA ====> #{sub_rca.id}".yellow
    #         all_pdf_attached << WickedPdf.new.pdf_from_string(render_to_string(template: 'tools/rca_reports/index.html.erb',
    #                                                                            layout: 'pdf',
    #                                                                            encoding: 'utf8'),
    #                                                           encoding: 'utf8')
    #         # all_pdf_attached << pdf_attached

    #         puts 'Wait 5 seconds please...........................................'.green
    #         sleep(5)
    #       end
    #     end

    #     @rca = le_rca
    #   end

    #   SendMails.rca_report_send_mail(params[:person].keys, @rca, params[:mail], all_pdf_attached)

    #   respond_to do |format|
    #     format.js { render 'shared/email_response', locals: {message: 'Success : Email(s) sent.', color: '#282'} }
    #   end
    # rescue StandardError => e
    #   respond_to do |format|
    #     puts 'ERROR'.red
    #     puts e
    #     puts 'OK'.red
    #     format.js { render 'shared/email_response', locals: {message: "Error : #{e.to_s.gsub(/\R+/, ' ')}.", color: '#922'} }
    #   end
    # end

    # def rca_report_add_person
    #   authorize @rca, :communication?

    #   respond_to do |format|
    #     format.js { render 'shared/people_list', locals: {url_submit: tools_rca_report_add_person_do_path(rca: @rca.id)} }
    #   end
    # end

    # GET / rca: rca_id, person: person_id
    def rca_report_add_person_do
      # authorize Rca, :show?
      # binding.pry
      # authorize @rca, :communication?
      skip_authorization

      person = params[:list][:person] ? Person.find(params[:list][:person]) : nil
      respond_to do |format|
        format.js { render 'add_person', locals: {person: person} }
      end
    end

    private

    def set_all_export_values
      @export_values = {}
      # @export_pdf = false

      # binding.pry

      if params[:export_values]
        @export_values = params[:export_values].deep_dup
      else
        # Rca info blocs
        @export_values['rca'] = {}
        @rca.methodology.step_info.step_tools.each do |step_tool|
          @export_values['rca'][step_tool.id.to_s] = true
        end

        # Docs RCA
        @export_values['docs'] = {}
        @export_values['docs']['all'] = true
        tools_documents = @rca.tools_documents.by_step_role_id(0)
        tools_documents.each do |tools_doc|
          @export_values['docs'][tools_doc.id.to_s] = true
        end

        # Images RCA
        @export_values['images'] = {}
        @export_values['images']['all'] = true
        tools_images = @rca.tools_images.by_step_role_id(0)
        tools_images.each do |tools_img|
          @export_values['images'][tools_img.id.to_s] = true
        end

        # Steps
        @rca.steps.each do |step|
          @export_values[step.id.to_s] = {}
          @export_values[step.id.to_s]['step'] = true

          # Step Tools
          step.step_tools.each do |step_tool|
            @export_values[step.id.to_s][step_tool.id.to_s] = {}
            @export_values[step.id.to_s][step_tool.id.to_s]['all'] = true

            next unless step_tool.tool.tool_name == 'Graph'

            tools_indicators = Tools::Indicator.search_default(@rca, 'tools_indicators', step_tool)
            tools_indicators.each do |indicator|
              @export_values[step.id.to_s][step_tool.id.to_s][indicator.id.to_s] = true
            end
          end

          # Documents
          @export_values[step.id.to_s]['docs'] = {}
          @export_values[step.id.to_s]['docs']['all'] = true
          tools_documents = @rca.tools_documents.by_step_role_id(step.step_role_id)
          tools_documents.each do |tools_doc|
            @export_values[step.id.to_s]['docs'][tools_doc.id.to_s] = true
          end

          # Images
          @export_values[step.id.to_s]['images'] = {}
          @export_values[step.id.to_s]['images']['all'] = true
          tools_images = @rca.tools_images.by_step_role_id(step.step_role_id)
          tools_images.each do |tools_img|
            @export_values[step.id.to_s]['images'][tools_img.id.to_s] = true
          end
        end
      end
    end
  end
end
