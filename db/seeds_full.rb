ActiveRecord::Base.establish_connection
ActiveRecord::Base.connection.execute('SET FOREIGN_KEY_CHECKS = 0')

ActiveRecord::Base.connection.execute('TRUNCATE action_text_rich_texts')
ActiveRecord::Base.connection.execute('TRUNCATE active_storage_attachments')
ActiveRecord::Base.connection.execute('TRUNCATE active_storage_blobs')
ActiveRecord::Base.connection.execute('TRUNCATE ar_internal_metadata')
ActiveRecord::Base.connection.execute('TRUNCATE admin_admin_configs')
ActiveRecord::Base.connection.execute('TRUNCATE admin_helper_texts')
ActiveRecord::Base.connection.execute('TRUNCATE admin_ldap_configs')
ActiveRecord::Base.connection.execute('TRUNCATE admin_perimeters')
ActiveRecord::Base.connection.execute('TRUNCATE admin_perimeter_rcas')
ActiveRecord::Base.connection.execute('TRUNCATE admin_perimeter_users')
ActiveRecord::Base.connection.execute('TRUNCATE custom_fields')
ActiveRecord::Base.connection.execute('TRUNCATE labels_cause_types')
ActiveRecord::Base.connection.execute('TRUNCATE labels_criteria')
ActiveRecord::Base.connection.execute('TRUNCATE labels_values')
ActiveRecord::Base.connection.execute('TRUNCATE labels_configs')
ActiveRecord::Base.connection.execute('TRUNCATE meth_activities')
ActiveRecord::Base.connection.execute('TRUNCATE meth_custom_field_descs')
ActiveRecord::Base.connection.execute('TRUNCATE meth_custom_field_types')
ActiveRecord::Base.connection.execute('TRUNCATE meth_list_contents')
ActiveRecord::Base.connection.execute('TRUNCATE meth_methodologies')
ActiveRecord::Base.connection.execute('TRUNCATE meth_steps')
ActiveRecord::Base.connection.execute('TRUNCATE meth_step_tools')
ActiveRecord::Base.connection.execute('TRUNCATE meth_tools')
ActiveRecord::Base.connection.execute('TRUNCATE meth_tool_custom_fields')
ActiveRecord::Base.connection.execute('TRUNCATE meth_step_roles')
ActiveRecord::Base.connection.execute('TRUNCATE posts')
ActiveRecord::Base.connection.execute('TRUNCATE rcas')
ActiveRecord::Base.connection.execute('TRUNCATE rca_advances')
ActiveRecord::Base.connection.execute('TRUNCATE taggings')
ActiveRecord::Base.connection.execute('TRUNCATE tags')
ActiveRecord::Base.connection.execute('TRUNCATE tools_causes')
ActiveRecord::Base.connection.execute('TRUNCATE tools_cause_classes')
ActiveRecord::Base.connection.execute('TRUNCATE tools_cause_links')
ActiveRecord::Base.connection.execute('TRUNCATE tools_cause_trees')
ActiveRecord::Base.connection.execute('TRUNCATE tools_criticities')
ActiveRecord::Base.connection.execute('TRUNCATE tools_criticity_values')
ActiveRecord::Base.connection.execute('TRUNCATE tools_crit_dec_mats')
ActiveRecord::Base.connection.execute('TRUNCATE tools_decision_mats')
ActiveRecord::Base.connection.execute('TRUNCATE tools_dec_mat_links')
ActiveRecord::Base.connection.execute('TRUNCATE tools_dec_mat_vals')
ActiveRecord::Base.connection.execute('TRUNCATE tools_documents')
ActiveRecord::Base.connection.execute('TRUNCATE tools_efficacities')
ActiveRecord::Base.connection.execute('TRUNCATE tools_images')
ActiveRecord::Base.connection.execute('TRUNCATE tools_indicators')
ActiveRecord::Base.connection.execute('TRUNCATE tools_indicator_measures')
ActiveRecord::Base.connection.execute('TRUNCATE tools_ishikawas')
ActiveRecord::Base.connection.execute('TRUNCATE tools_ishi_branchs')
ActiveRecord::Base.connection.execute('TRUNCATE tools_matrix_stats')
ActiveRecord::Base.connection.execute('TRUNCATE tools_qqoqcps')
ActiveRecord::Base.connection.execute('TRUNCATE tools_tasks')
ActiveRecord::Base.connection.execute('TRUNCATE tools_task_validations')
ActiveRecord::Base.connection.execute('TRUNCATE tools_teams')
ActiveRecord::Base.connection.execute('TRUNCATE users')
ActiveRecord::Base.connection.execute('TRUNCATE users_roles')
ActiveRecord::Base.connection.execute('TRUNCATE people')
ActiveRecord::Base.connection.execute('TRUNCATE profils')
ActiveRecord::Base.connection.execute('TRUNCATE profil_authorizes')
ActiveRecord::Base.connection.execute('TRUNCATE roles')

Role.create!([{id: 1, name: 'super_admin'},
              {id: 2, name: 'admin'},
              {id: 3, name: 'manager'},
              {id: 4, name: 'user'},
              {id: 5, name: 'visitor'},
              {id: 6, name: 'super_vision'},
              {id: 7, name: 'external'}])

# Perimeters : create first perimeter
Admin::Perimeter.create!(id: 1, name: 'ALL', ancestry: nil)

# Create admin config (associate default role and perimeter)
admin_config = Admin::AdminConfig.create!

Person.create!(id: 1, firstname: 'Admin', lastname: 'Admin')

# Person.create!(id: 2, firstname: 'Donald', lastname: 'Duck')
# Person.create!(id: 3, firstname: 'Riri', lastname: 'Duck')
# Person.create!(id: 4, firstname: 'Fifi', lastname: 'Duck')
# Person.create!(id: 5, firstname: 'Loulou', lastname: 'Duck')
# Person.create!(id: 6, firstname: 'Daisy', lastname: 'Duck')
# Person.create!(id: 7, firstname: 'Picsou', lastname: 'Duck')
# Person.create!(id: 8, firstname: 'Mikey', lastname: 'Mouse')
# Person.create!(id: 9, firstname: 'Minie', lastname: 'Mouse')

# Create super-admin user and assign role and perimeter
User.create!([{id: 1, username: 'admin', email: 'admin@prowhy.org',
               password: 'admin++', password_confirmation: 'admin++', reset_password_token: nil,
               reset_password_sent_at: nil, remember_created_at: nil,
               person_id: 1}])
User.first.add_role :super_admin
User.first.add_perimeter(1)

# Methodologies : Activities and Tools
Meth::Activity.create!([
  {id: 1, name_fr: 'Actions d\'urgence',     name_en: 'Urgent Actions', pos: 1},
  {id: 2, name_fr: 'Actions conservatoires', name_en: 'Containment Actions', pos: 2},
  {id: 3, name_fr: 'Actions correctives',    name_en: 'Correction Actions', pos: 3},
  {id: 4, name_fr: 'Actions préventives',    name_en: 'Prevent Actions', pos: 4},
  {id: 6, name_fr: 'Equipe',                 name_en: 'Team', pos: 6},
  {id: 7, name_fr: 'Description',            name_en: 'Description', pos: 7},
  {id: 8, name_fr: 'Recherche de causes',    name_en: 'Root causes', pos: 8},
  {id: 9, name_fr: 'Cloture',                name_en: 'Closure', pos: 9},
  {id: 10, name_fr: 'Infos',                 name_en: 'Infos', pos: 10},
  {id: 11, name_fr: 'Améliorations',         name_en: 'Improvements', pos: 11},
  {id: 12, name_fr: 'Divers',                name_en: 'Divers', pos: 12}
])

# Criteria for Decision Matrix
Labels::Criterium.create!([
                            {id: 1, name_fr: 'Prix',       name_en: 'Price', crit_type: 0},
                            {id: 2, name_fr: 'Durée',      name_en: 'Duration', crit_type: 0},
                            {id: 3, name_fr: 'Facilité',   name_en: 'Facility', crit_type: 0},
                            {id: 4, name_fr: 'Efficacité', name_en: 'Efficacity', crit_type: 0}
                          ])

# Criticity : criteria, values, configurations
Labels::Config.create!([
                         {id: 1, name_fr: 'Configuration Non Conformités', name_en: 'NC Configuration',
                          formula: '3*CR_SA + 3*CR_CU + 2*CR_CO + CR_PR'},
                         {id: 2, name_fr: 'Configuration Amélioration Continue', name_en: 'Improvement Configuration', formula: ''}
                       ])
Labels::Criterium.create!([
                            {id: 11, internal_name: 'CR_SA', config_id: 1, name_fr: 'Impact sur la Sécurité', name_en: 'Safety Impact', crit_type: 1, threshold: 3},
                            {id: 12, internal_name: 'CR_CU', config_id: 1, name_fr: 'Impact sur le Client', name_en: 'Customer Impact', crit_type: 1, threshold: 3},
                            {id: 13, internal_name: 'CR_CO', config_id: 1, name_fr: 'Impact sur les Coût', name_en: 'Cost Impact', crit_type: 1, threshold: 5},
                            {id: 14, internal_name: 'CR_PR', config_id: 1, name_fr: 'Impact sur la Production', name_en: 'Production Impact', crit_type: 1, threshold: 0}
                          ])
Labels::Value.create!([
                        {id: 1, criterium_id: 11, title_fr: 'Aucun impact', title_en: 'No impact', value: 0},
                        {id: 2, criterium_id: 11, title_fr: 'Impact faible', title_en: 'Low Impact', value: 1},
                        {id: 3, criterium_id: 11, title_fr: 'Impact modéré', title_en: 'Middle Impact', value: 3},
                        {id: 4, criterium_id: 11, title_fr: 'Impact fort', title_en: 'High Impact', value: 5},
                        {id: 10, criterium_id: 12, title_fr: 'Impact faible', title_en: 'Low Impact', value: 1},
                        {id: 11, criterium_id: 12, title_fr: 'Impact modéré', title_en: 'Middle Impact', value: 3},
                        {id: 12, criterium_id: 12, title_fr: 'Impact fort', title_en: 'High Impact', value: 5},
                        {id: 20, criterium_id: 13, title_fr: 'Impact faible', title_en: 'Low Impact', value: 1},
                        {id: 21, criterium_id: 13, title_fr: 'Impact modéré', title_en: 'Middle Impact', value: 3},
                        {id: 22, criterium_id: 13, title_fr: 'Impact fort', title_en: 'High Impact', value: 5},
                        {id: 30, criterium_id: 14, title_fr: 'Impact faible', title_en: 'Low Impact', value: 1},
                        {id: 31, criterium_id: 14, title_fr: 'Impact modéré', title_en: 'Middle Impact', value: 3},
                        {id: 32, criterium_id: 14, title_fr: 'Impact fort', title_en: 'High Impact', value: 5}
                      ])

# Tools
Meth::Tool.create!([
  {id: 1, name_fr: 'PA Urgence',                     name_en: 'Urgent PA', activity_id: 1, tool_name: 'Action',
   tool_icon: 'sitemap',
   tool_options: {task_options: {task_detail_type: 'Tools::TaskEmergency'}}},
  {id: 2, name_fr: 'Vérif efficacité',               name_en: 'Check efficiency', activity_id: 1, tool_name: 'ActionCheck',
   tool_icon: 'check-double',
   tool_options: {task_options: {task_detail_type: 'Tools::TaskEmergency'}}},
  {id: 3, name_fr: 'PA Conservatoires',              name_en: 'Containment PA', activity_id: 2, tool_name: 'Action',
   tool_icon: 'sitemap',
   tool_options: {task_options: {task_detail_type: 'Tools::TaskContainment'}}},
  {id: 4, name_fr: 'Vérification efficacité',        name_en: 'Check efficiency', activity_id: 2, tool_name: 'ActionCheck',
   tool_icon: 'check-double',
   tool_options: {task_options: {task_detail_type: 'Tools::TaskContainment'}}},
  {id: 5, name_fr: 'PA Correctives',                 name_en: 'Corrective PA', activity_id: 3, tool_name: 'Action',
   tool_icon: 'sitemap',
   tool_options: {task_options: {task_detail_type: 'Tools::TaskCorrective'},
                  plan_options: {efficacity: true, matrix: true, blocks: true, blocks_column: 'matrix_status'}}},
  {id: 6, name_fr: 'Matrice décision',               name_en: 'Decision matrix', activity_id: 3, tool_name: 'DecisionMat',
   tool_icon: 'calculator',
   tool_options: {criteria: [{id: 1, weight: 3, values: [1, 2, 3]},
                             {id: 2, weight: 1, values: [1, 2, 3]},
                             {id: 3, weight: 2, values: [1, 2, 3]}],
                  entry_class: 'Tools::Task',
                  entry: {task_detail_type: 'Tools::TaskCorrective'}}},
  {id: 7, name_fr: 'PA Préventives',                 name_en: 'Preventives PA', activity_id: 4, tool_name: 'Action',
   tool_icon: 'sitemap',
   tool_options: {task_options: {task_detail_type: 'Tools::TaskPreventive'},
                  plan_options: {efficacity: true}}},
  {id: 8, name_fr: 'Vérification efficacité',        name_en: 'Check efficiency', activity_id: 4, tool_name: 'ActionCheck',
   tool_icon: 'check-double',
   tool_options: {task_options: {task_detail_type: 'Tools::TaskPreventive'}}},
  {id: 9, name_fr: 'Graphe Vérification efficacité', name_en: 'Check efficiency graph', activity_id: 3, tool_name: 'Graph',
   tool_icon: 'chart-bar'},
  {id: 10, name_fr: 'Equipe',                        name_en: 'Team', activity_id: 6, tool_name: 'Team',
   tool_icon: 'users'},
  {id: 11, name_fr: 'QQOQCP',                        name_en: 'WWWWHW', activity_id: 7, tool_name: 'Qqoqcp',
   tool_icon: 'th-list'},
  {id: 12, name_fr: 'EST/N\'EST PAS',                name_en: 'IS, IS NOT', activity_id: 7, tool_name: 'Qoqc',
   tool_icon: 'th-list'},
  {id: 13, name_fr: 'Arbre des causes',              name_en: 'Causes tree', activity_id: 8, tool_name: 'CauseTree',
   tool_icon: 'project-diagram'},
  {id: 14, name_fr: 'Ishikawa',                      name_en: 'Ishikawa', activity_id: 8, tool_name: 'Ishikawa',
   tool_icon: 'fish'},
  {id: 15, name_fr: '5 Pourquoi',                    name_en: '5 Why', activity_id: 8, tool_name: 'Why',
   tool_icon: 'question-circle'},
  {id: 16, name_fr: 'PA Validation',                 name_en: 'Validation PA', activity_id: 8, tool_name: 'Action',
   tool_icon: 'sitemap',
   tool_options: {task_options: {task_detail_type: 'Tools::TaskValidation'},
                  plan_options: {causes: true}}},
  # {id: 17, name_fr: 'Clotûre',                       name_en: 'Closure', activity_id: 9, tool_name: 'Closure',
  # tool_icon: 'step-forward'},
  {id: 18, name_fr: 'Champs personnalisés',          name_en: 'Personalised fields', activity_id: 10, tool_name: 'CustomField',
   tool_icon: 'bars'},
  {id: 19, name_fr: 'Etiquettes',                    name_en: 'Tags', activity_id: 10, tool_name: 'Tags',
   tool_icon: 'tags'},
  {id: 20, name_fr: 'PA Améliorations',              name_en: 'Improvement PA', activity_id: 11, tool_name: 'Action',
   tool_icon: 'sitemap',
   tool_options: {task_options: {task_detail_type: 'Tools::TaskImprovement'},
                  plan_options: {efficacity: true, matrix: false}}},
  {id: 21, name_fr: 'Vérification efficacité',       name_en: 'Check efficiency', activity_id: 11, tool_name: 'ActionCheck',
   tool_icon: 'check-double',
   tool_options: {task_options: {task_detail_type: 'Tools::TaskImprovement'}}},
  {id: 22, name_fr: 'Check Liste Actions',           name_en: 'Check List Actions', activity_id: 1, tool_name: 'Action',
   tool_icon: 'tasks'},
  {id: 23, name_fr: 'Check Liste Actions',           name_en: 'Check List Actions', activity_id: 2, tool_name: 'Action',
   tool_icon: 'tasks'},
  {id: 24, name_fr: 'Méthode 5S',                    name_en: '5S Method', activity_id: 11, tool_name: 'Improve5s',
   tool_icon: 'digital-ocean'},
  {id: 25, name_fr: 'Documents',                     name_en: 'Documents', activity_id: 12, tool_name: 'Document',
   tool_icon: 'folder-open'},
  {id: 26, name_fr: 'Images',                        name_en: 'Pictures', activity_id: 12, tool_name: 'Image',
   tool_icon: 'image'},
  {id: 27, name_fr: 'Plan Actions',                  name_en: 'Action Plan', activity_id: 12, tool_name: 'Action',
   tool_icon: 'sitemap',
   tool_options: {task_options: {task_detail_type: ['Tools::TaskEmergency', 'Tools::TaskContainment',
                                                    'Tools::TaskCorrective', 'Tools::TaskPreventive',
                                                    'Tools::TaskValidation', 'Tools::TaskImprovement']},
                  plan_options: {efficacity: 'show', matrix: 'show', blocks: true, blocks_column: 'task_detail_type'}}},
  {id: 28, name_fr: 'Rapport',                       name_en: 'Reporting', activity_id: 12, tool_name: 'Report',
   tool_icon: 'edit', tool_options: {}},
  {id: 29, name_fr: 'Criticité',                     name_en: 'Criticity', activity_id: 12, tool_name: 'Criticity',
   tool_icon: 'chart-line', tool_options: {}},
  {id: 30, name_fr: 'Vérification efficacité',       name_en: 'Check efficiency', activity_id: 4, tool_name: 'ActionCheck',
   tool_icon: 'check-double',
   tool_options: {task_options: {task_detail_type: 'Tools::TaskCorrective'},
                  more_options: {tools_matrix_stats: {matrix_status: '1'}}}},
  {id: 31, name_fr: 'Générique',                     name_en: 'Generic', activity_id: 12, tool_name: 'Generic',
   tool_icon: 'file-download', tool_options: {}}
])
Meth::CustomFieldType.create!([
  {id: 1, field_type: 'string', field_size: nil, field_internal: 'text', field_name_fr: 'Texte', field_name_en: 'Text'},
  {id: 2, field_type: 'text', field_size: nil, field_internal: 'text_area', field_name_fr: 'Zone de texte', field_name_en: 'Text Area'},
  {id: 3, field_type: 'text', field_size: nil, field_internal: 'rich_text', field_name_fr: 'Texte mis en forme', field_name_en: 'Text'},
  {id: 4, field_type: 'bigint', field_size: nil, field_internal: 'select', field_name_fr: 'Liste déroulante', field_name_en: 'Select List'},
  {id: 5, field_type: 'boolean', field_size: nil, field_internal: 'radio', field_name_fr: 'Boolean (oui/non)', field_name_en: 'Boolean (yes/no)'},
  {id: 6, field_type: 'bigint', field_size: nil, field_internal: 'tree', field_name_fr: 'Taxonomie', field_name_en: 'Taxonomy'},
  {id: 7, field_type: 'integer', field_size: nil, field_internal: 'integer', field_name_fr: 'Nombre (entier)', field_name_en: 'Integer (number)'},
  {id: 8, field_type: 'string', field_size: nil, field_internal: 'tag', field_name_fr: 'Tags', field_name_en: 'Tag'},
  {id: 9, field_type: 'date', field_size: nil, field_internal: 'date', field_name_fr: 'Date', field_name_en: 'Date'}
])
Meth::CustomFieldDesc.create!([
  {id: 1, custom_field_type_id: 4, internal_name: 'custom_1', show_list: true, show_search: true, name_fr: 'Site'},
  {id: 2, custom_field_type_id: 4, internal_name: 'custom_2', show_list: false, show_search: true, name_fr: 'Activité Détection NC'},
  {id: 3, custom_field_type_id: 9, internal_name: 'custom_3', show_list: false, show_search: false, name_fr: 'Date de détection'},
  {id: 4, custom_field_type_id: 4, internal_name: 'custom_4', show_list: true, show_search: true, name_fr: 'Secteur'},
  {id: 5, custom_field_type_id: 1, internal_name: 'custom_5', show_list: false, show_search: true, name_fr: 'Référence (OF)'},
  {id: 6, custom_field_type_id: 4, internal_name: 'custom_6', show_list: false, show_search: true, name_fr: 'Client'},
  {id: 7, custom_field_type_id: 4, internal_name: 'custom_7', show_list: false, show_search: true, name_fr: 'Fournisseur'},
  {id: 8, custom_field_type_id: 1, internal_name: 'custom_8', show_list: false, show_search: true, name_fr: 'Référence externe'},
  {id: 9, custom_field_type_id: 4, internal_name: 'custom_9', show_list: true, show_search: true, name_fr: 'Activité Origine NC'},
  {id: 10, custom_field_type_id: 2, internal_name: 'custom_10', show_list: false, show_search: false, name_fr: 'Informations complémentaire NC'},
  {id: 11, custom_field_type_id: 2, internal_name: 'custom_11', show_list: false, show_search: false, name_fr: 'Avis Qualité'}
])

Meth::ListContent.create!([
  {custom_field_desc_id: 1, name_fr: 'Tarbes Ouest', pos: 1},
  {custom_field_desc_id: 1, name_fr: 'Tarbes Nord', pos: 2},
  {custom_field_desc_id: 1, name_fr: 'Lourdes', pos: 3},
  {custom_field_desc_id: 1, name_fr: 'Argelès Gazost', pos: 4},
  {custom_field_desc_id: 1, name_fr: 'Pau Nord', pos: 6},
  {custom_field_desc_id: 1, name_fr: 'Pau Sud', pos: 5},
  {custom_field_desc_id: 2, name_fr: 'Fabrication', pos: 3},
  {custom_field_desc_id: 2, name_fr: 'Expédition', pos: 5},
  {custom_field_desc_id: 2, name_fr: 'Approvisionnement', pos: 1},
  {custom_field_desc_id: 2, name_fr: 'Conception', pos: 2},
  {custom_field_desc_id: 2, name_fr: 'Contrôle', pos: 4},
  {custom_field_desc_id: 4, name_fr: 'Parapente', pos: 1},
  {custom_field_desc_id: 4, name_fr: 'Ski/Snow', pos: 2},
  {custom_field_desc_id: 4, name_fr: 'Escalade', pos: 3},
  {custom_field_desc_id: 4, name_fr: 'Rafting/Canyoning', pos: 4},
  {custom_field_desc_id: 6, name_fr: 'Client 1', pos: 1},
  {custom_field_desc_id: 6, name_fr: 'Client 2', pos: 2},
  {custom_field_desc_id: 6, name_fr: 'Client 3', pos: 3},
  {custom_field_desc_id: 6, name_fr: 'Client 4', pos: 4},
  {custom_field_desc_id: 6, name_fr: 'Client 5', pos: 5},
  {custom_field_desc_id: 7, name_fr: 'Fournisseur 1', pos: 1},
  {custom_field_desc_id: 7, name_fr: 'Fournisseur 2', pos: 2},
  {custom_field_desc_id: 7, name_fr: 'Fournisseur 3', pos: 3},
  {custom_field_desc_id: 7, name_fr: 'Fournisseur 4', pos: 4},
  {custom_field_desc_id: 7, name_fr: 'Fournisseur 5', pos: 5},
  {custom_field_desc_id: 9, name_fr: 'Conception', pos: 2},
  {custom_field_desc_id: 9, name_fr: 'Fabrication', pos: 3},
  {custom_field_desc_id: 9, name_fr: 'Contrôle', pos: 4},
  {custom_field_desc_id: 9, name_fr: 'Approvisionnement', pos: 1},
  {custom_field_desc_id: 9, name_fr: 'Expédition', pos: 5}
])

Meth::StepRole.create!([
                         {id: 1, role_name: 'Info', name_fr: 'Info', name_en: 'Info', advance_level: 0},
                         {id: 2, role_name: 'Trs', name_fr: 'Outils Transversaux', name_en: 'Tranverse Tools', advance_level: 0},
                         {id: 3, role_name: 'Emergency', name_fr: 'Urgence', name_en: 'Emergency', advance_level: 1},
                         {id: 4, role_name: 'Team', name_fr: 'Equipe', name_en: 'Team', advance_level: 2},
                         {id: 5, role_name: 'Description', name_fr: 'Description', name_en: 'Description', advance_level: 3},
                         {id: 6, role_name: 'Containment', name_fr: 'Conservatoire', name_en: 'Containment', advance_level: 4},
                         {id: 7, role_name: 'Cause', name_fr: 'Recherche de causes', name_en: 'Cause Search', advance_level: 5},
                         {id: 8, role_name: 'Correction', name_fr: 'Correction', name_en: 'Correction', advance_level: 6},
                         {id: 9, role_name: 'Check', name_fr: 'Vérifiation', name_en: 'Check', advance_level: 7},
                         {id: 10, role_name: 'Prevent', name_fr: 'Prévention', name_en: 'Prevent recurency', advance_level: 8},
                         {id: 11, role_name: 'Close', name_fr: 'Clôture', name_en: 'Close', advance_level: 9},
                         {id: 12, role_name: 'Improve', name_fr: 'Améliorations', name_en: 'Improvement', advance_level: 0},
                         {id: 13, role_name: 'Other', name_fr: 'Autre', name_en: 'Something Else', advance_level: 0}
                       ])

# METHOD USERS (User Action Plan)
Meth::Methodology.create!([
                            {id: 1, name_fr: 'Utilisateurs', name_en: 'Users', locked: true, usable: false}
                          ])
# Step for User Action Plan
Meth::Step.create!([
                     {id: 1, name: 'Users', pos: 1, methodology_id: 1, step_role_id: 2, step_type: 3}
                   ])

Meth::StepTool.create!([
                         {id: 1, step_id: 1, tool_id: 27, pos: 0, name: 'User Action Plan', tool_reference: 0,
                          tool_options: Meth::Tool.find(27).tool_options.deep_merge({plan_options: {type_plan: 'user'}})}
                       ])
# Step for Global Action Plan
Meth::Step.create!([
                     {id: 2, name: 'Global', pos: 2, methodology_id: 1, step_role_id: 2, step_type: 3}
                   ])

Meth::StepTool.create!([
                         {id: 2, step_id: 2, tool_id: 27, pos: 0, name: 'Global Action Plan', tool_reference: 0,
                          tool_options: Meth::Tool.find(27).tool_options.deep_merge({plan_options: {type_plan: 'global'}})}
                       ])
# Meth::StepTool.first.tool_options[:plan_options][:type_plan] = 'user'
# Meth::StepTool.first.save!

# METHOD WAITING FOR DECISION (Default methodo waiting for criticity CALCUL)
Meth::Methodology.create!([
                            {id: 2, name_fr: 'En attente de décision', name_en: 'Waiting for decision', locked: true, usable: true}
                          ])
Meth::Step.create!([
                     {id: 5, name: 'Infos', pos: 0, methodology_id: 2, step_role_id: 1, step_type: 2},
                     {id: 6, name: 'Tools Trs', pos: 0, methodology_id: 2, step_role_id: 2, step_type: 3}
                   ])

Meth::StepTool.create!([{id: 3, step_id: 6, tool_id: 29, pos: 1, name_fr: 'Criticité', name_en: 'Criticity', tool_reference: 0}])

admin_config.options['default_methodology'] = 2
admin_config.save!

# METHOD 8D
Meth::Methodology.create!([
                            {id: 10, name_fr: 'Méthode 8D', name_en: '8D Method'}
                          ])
Meth::Step.create!([
                     # Methodo 8D
                     {id: 11, name: 'Infos', pos: 0, methodology_id: 10, step_role_id: 1, step_type: 2},
                     {id: 12, name: 'Tools Trs', pos: 0, methodology_id: 10, step_role_id: 2, step_type: 3},
                     {id: 21, name: 'D1', pos: 1, methodology_id: 10, step_role_id: 4},
                     {id: 22, name: 'D2', pos: 2, methodology_id: 10, step_role_id: 5},
                     {id: 23, name: 'D3', pos: 3, methodology_id: 10, step_role_id: 6},
                     {id: 24, name: 'D4', pos: 4, methodology_id: 10, step_role_id: 7},
                     {id: 25, name: 'D5', pos: 5, methodology_id: 10, step_role_id: 8},
                     {id: 26, name: 'D6', pos: 6, methodology_id: 10, step_role_id: 9},
                     {id: 27, name: 'D7', pos: 7, methodology_id: 10, step_role_id: 10},
                     {id: 28, name: 'D8', pos: 8, methodology_id: 10, step_role_id: 11}
                   ])
Meth::StepTool.create!([
                         # Methodo 8D
                         {id: 21, step_id: 21, tool_id: 10, pos: 1, name: 'Equipe', tool_reference: 1},
                         {id: 22, step_id: 23, tool_id: 3, pos: 1, name: 'PA Conservatoire', tool_reference: 1},
                         {id: 23, step_id: 24, tool_id: 13, pos: 1, name: 'Cause Tree', tool_reference: 1},
                         {id: 24, step_id: 24, tool_id: 14, pos: 2, name: 'Ishikawa', tool_reference: 1},
                         {id: 25, step_id: 24, tool_id: 15, pos: 3, name: '5 Why', tool_reference: 1},
                         {id: 26, step_id: 25, tool_id: 5, pos: 1, name: 'PA Corrective', tool_reference: 1},
                         {id: 27, step_id: 25, tool_id: 6, pos: 2, name: 'Matrice Décision', tool_reference: 1},
                         {id: 28, step_id: 26, tool_id: 9, pos: 1, name: 'Graph vérif', tool_reference: 1},
                         {id: 29, step_id: 27, tool_id: 7, pos: 1, name: 'PA Préventive', tool_reference: 1},
                         {id: 30, step_id: 27, tool_id: 8, pos: 2, name: 'Vérif efficacité', tool_reference: 1},
                         {id: 31, step_id: 28, tool_id: 18, pos: 1, name: 'Clôture', tool_reference: 1, tool_options: {closure: true}},
                         {id: 32, step_id: 11, tool_id: 18, pos: 1, name: 'Infos', tool_reference: 1},
                         {id: 33, step_id: 22, tool_id: 11, pos: 1, name: 'QQOQCP', tool_reference: 1},
                         {id: 34, step_id: 24, tool_id: 16, pos: 4, name: 'PA Validation', tool_reference: 1},
                         {id: 35, step_id: 11, tool_id: 18, pos: 2, name: 'NC', tool_reference: 1},
                         {id: 36, step_id: 11, tool_id: 18, pos: 3, name: 'Client/Fournisseur', tool_reference: 1},
                         {id: 37, step_id: 12, tool_id: 27, pos: 2, name: 'Action Plan', tool_reference: 0},
                         {id: 38, step_id: 12, tool_id: 28, pos: 3, name: 'Report', tool_reference: 0},
                         {id: 39, step_id: 12, tool_id: 25, pos: 5, name: 'Documents', tool_reference: 0},
                         {id: 40, step_id: 12, tool_id: 29, pos: 1, name_fr: 'Criticité', name_en: 'Criticity', tool_reference: 0},
                         {id: 41, step_id: 12, tool_id: 26, pos: 4, name: 'Images', tool_reference: 0},
                         {id: 42, step_id: 26, tool_id: 8, pos: 2, name: 'Efficacité', tool_reference: 1}
                       ])
# Assign custom fields to StepTools
Meth::ToolCustomField.create!([
                                {step_tool_id: 32, custom_field_desc_id: 1, pos: 1, visible: true, obligatory: false},
                                {step_tool_id: 32, custom_field_desc_id: 4, pos: 2, visible: true, obligatory: false},
                                {step_tool_id: 32, custom_field_desc_id: 5, pos: 3, visible: true, obligatory: false},
                                {step_tool_id: 35, custom_field_desc_id: 3, pos: 1, visible: true, obligatory: false},
                                {step_tool_id: 35, custom_field_desc_id: 2, pos: 2, visible: true, obligatory: false},
                                {step_tool_id: 35, custom_field_desc_id: 9, pos: 3, visible: true, obligatory: false},
                                {step_tool_id: 35, custom_field_desc_id: 10, pos: 4, visible: true, obligatory: false},
                                {step_tool_id: 36, custom_field_desc_id: 6, pos: 1, visible: true, obligatory: false},
                                {step_tool_id: 36, custom_field_desc_id: 7, pos: 2, visible: true, obligatory: false},
                                {step_tool_id: 36, custom_field_desc_id: 8, pos: 3, visible: true, obligatory: false},
                                {step_tool_id: 31, custom_field_desc_id: 11, pos: 1, visible: true, obligatory: false}
                              ])
# METHOD NC
Meth::Methodology.create!([
                            {id: 11, name_fr: 'NC - Anomalie', name_en: 'NC - Anomaly'}
                          ])
Meth::Step.create!([
                     # Methodo NC
                     {id: 101, name: 'Infos', pos: 0, methodology_id: 11, step_role_id: 1, step_type: 2},
                     {id: 102, name: 'Tools Trs', pos: 0, methodology_id: 11, step_role_id: 2, step_type: 3},
                     {id: 103, name: 'Analyse', pos: 1, methodology_id: 11, step_role_id: 7},
                     {id: 104, name: 'Clôture', pos: 8, methodology_id: 11, step_role_id: 11}
                   ])
Meth::StepTool.create!([
                         # Methodo NC
                         {id: 101, step_id: 103, tool_id: 13, pos: 1, name: 'Cause Tree', tool_reference: 1},
                         {id: 102, step_id: 103, tool_id: 14, pos: 2, name: 'Ishikawa', tool_reference: 1},
                         {id: 103, step_id: 103, tool_id: 15, pos: 3, name: '5 Why', tool_reference: 1},
                         {id: 104, step_id: 103, tool_id: 5, pos: 4, name: 'PA Corrective', tool_reference: 1},
                         {id: 105, step_id: 104, tool_id: 18, pos: 1, name: 'Clôture', tool_reference: 1, tool_options: {closure: true}},
                         {id: 106, step_id: 101, tool_id: 18, pos: 1, name: 'Infos', tool_reference: 1},
                         {id: 107, step_id: 101, tool_id: 18, pos: 2, name: 'NC', tool_reference: 1},
                         {id: 108, step_id: 101, tool_id: 18, pos: 3, name: 'Client/Fournisseur', tool_reference: 1},
                         {id: 109, step_id: 102, tool_id: 27, pos: 2, name: 'Action Plan', tool_reference: 0},
                         {id: 110, step_id: 102, tool_id: 28, pos: 3, name: 'Report', tool_reference: 0},
                         {id: 111, step_id: 102, tool_id: 25, pos: 5, name: 'Documents', tool_reference: 0},
                         {id: 112, step_id: 102, tool_id: 29, pos: 1, name: 'Criticité', tool_reference: 0},
                         {id: 113, step_id: 102, tool_id: 26, pos: 4, name: 'Images', tool_reference: 0},
                         {id: 114, step_id: 103, tool_id: 8, pos: 5, name: 'Efficacité', tool_reference: 1}
                       ])

# Assign custom fields to StepTools
Meth::ToolCustomField.create!([
                                {step_tool_id: 106, custom_field_desc_id: 1, pos: 1, visible: true, obligatory: false},
                                {step_tool_id: 106, custom_field_desc_id: 4, pos: 2, visible: true, obligatory: false},
                                {step_tool_id: 106, custom_field_desc_id: 5, pos: 3, visible: true, obligatory: false},
                                {step_tool_id: 107, custom_field_desc_id: 3, pos: 1, visible: true, obligatory: false},
                                {step_tool_id: 107, custom_field_desc_id: 2, pos: 2, visible: true, obligatory: false},
                                {step_tool_id: 107, custom_field_desc_id: 9, pos: 3, visible: true, obligatory: false},
                                {step_tool_id: 107, custom_field_desc_id: 10, pos: 4, visible: true, obligatory: false},
                                {step_tool_id: 108, custom_field_desc_id: 6, pos: 1, visible: true, obligatory: false},
                                {step_tool_id: 108, custom_field_desc_id: 7, pos: 2, visible: true, obligatory: false},
                                {step_tool_id: 108, custom_field_desc_id: 8, pos: 3, visible: true, obligatory: false},
                                {step_tool_id: 105, custom_field_desc_id: 11, pos: 1, visible: true, obligatory: false}
                              ])

# METHOD 9STEPS
Meth::Methodology.create!([
                            {id: 12, name_fr: '9 STEPS', name_en: '9 STEPS'}
                          ])
Meth::Step.create!([
                     # Methodo 9STEPS
                     {id: 211, name: 'Infos', pos: 0, methodology_id: 12, step_role_id: 1, step_type: 2},
                     {id: 212, name: 'Tools Trs', pos: 0, methodology_id: 12, step_role_id: 2, step_type: 3},
                     {id: 220, name: 'S0', pos: 1, methodology_id: 12, step_role_id: 3},
                     {id: 221, name: 'S1', pos: 1, methodology_id: 12, step_role_id: 4},
                     {id: 222, name: 'S2', pos: 2, methodology_id: 12, step_role_id: 5},
                     {id: 223, name: 'S3', pos: 3, methodology_id: 12, step_role_id: 6},
                     {id: 224, name: 'S4', pos: 4, methodology_id: 12, step_role_id: 7},
                     {id: 225, name: 'S5', pos: 5, methodology_id: 12, step_role_id: 8},
                     {id: 226, name: 'S6', pos: 6, methodology_id: 12, step_role_id: 9},
                     {id: 227, name: 'S7', pos: 7, methodology_id: 12, step_role_id: 10},
                     {id: 228, name: 'S8', pos: 8, methodology_id: 12, step_role_id: 11}
                   ])
Meth::StepTool.create!([
                         # Methodo 9STEPS
                         {id: 221, step_id: 221, tool_id: 10, pos: 1, name: 'Equipe', tool_reference: 1},
                         {id: 222, step_id: 223, tool_id: 3, pos: 1, name: 'PA Conservatoire', tool_reference: 1},
                         {id: 223, step_id: 224, tool_id: 13, pos: 1, name: 'Cause Tree', tool_reference: 1},
                         {id: 224, step_id: 224, tool_id: 14, pos: 2, name: 'Ishikawa', tool_reference: 1},
                         {id: 225, step_id: 224, tool_id: 15, pos: 3, name: '5 Why', tool_reference: 1},
                         {id: 226, step_id: 225, tool_id: 5, pos: 1, name: 'PA Corrective', tool_reference: 1},
                         {id: 227, step_id: 225, tool_id: 6, pos: 2, name: 'Matrice Décision', tool_reference: 1},
                         {id: 228, step_id: 226, tool_id: 9, pos: 1, name: 'Graph vérif', tool_reference: 1},
                         {id: 229, step_id: 227, tool_id: 7, pos: 1, name: 'PA Préventive', tool_reference: 1},
                         {id: 230, step_id: 227, tool_id: 8, pos: 2, name: 'Vérif efficacité', tool_reference: 1},
                         {id: 231, step_id: 228, tool_id: 18, pos: 1, name: 'Clôture', tool_reference: 1, tool_options: {closure: true}},
                         {id: 232, step_id: 211, tool_id: 18, pos: 1, name: 'Infos', tool_reference: 1},
                         {id: 233, step_id: 222, tool_id: 11, pos: 1, name: 'QQOQCP', tool_reference: 1},
                         {id: 234, step_id: 224, tool_id: 16, pos: 4, name: 'PA Validation', tool_reference: 1},
                         {id: 235, step_id: 211, tool_id: 18, pos: 2, name: 'NC', tool_reference: 1},
                         {id: 236, step_id: 211, tool_id: 18, pos: 3, name: 'Client/Fournisseur', tool_reference: 1},
                         {id: 237, step_id: 212, tool_id: 27, pos: 2, name: 'Action Plan', tool_reference: 0},
                         {id: 238, step_id: 212, tool_id: 28, pos: 3, name: 'Report', tool_reference: 0},
                         {id: 239, step_id: 212, tool_id: 25, pos: 5, name: 'Documents', tool_reference: 0},
                         {id: 240, step_id: 220, tool_id: 1, pos: 1, name: 'PA Urgence', tool_reference: 1},
                         {id: 241, step_id: 212, tool_id: 29, pos: 1, name: 'Criticité', tool_reference: 0},
                         {id: 242, step_id: 212, tool_id: 26, pos: 4, name: 'Images', tool_reference: 0},
                         {id: 243, step_id: 226, tool_id: 8, pos: 2, name: 'Efficacité', tool_reference: 1}
                       ])
# Assign custom fields to StepTools
Meth::ToolCustomField.create!([
                                {step_tool_id: 232, custom_field_desc_id: 1, pos: 1, visible: true, obligatory: false},
                                {step_tool_id: 232, custom_field_desc_id: 4, pos: 2, visible: true, obligatory: false},
                                {step_tool_id: 232, custom_field_desc_id: 5, pos: 3, visible: true, obligatory: false},
                                {step_tool_id: 235, custom_field_desc_id: 3, pos: 1, visible: true, obligatory: false},
                                {step_tool_id: 235, custom_field_desc_id: 2, pos: 2, visible: true, obligatory: false},
                                {step_tool_id: 235, custom_field_desc_id: 9, pos: 3, visible: true, obligatory: false},
                                {step_tool_id: 235, custom_field_desc_id: 10, pos: 4, visible: true, obligatory: false},
                                {step_tool_id: 236, custom_field_desc_id: 6, pos: 1, visible: true, obligatory: false},
                                {step_tool_id: 236, custom_field_desc_id: 7, pos: 2, visible: true, obligatory: false},
                                {step_tool_id: 236, custom_field_desc_id: 8, pos: 3, visible: true, obligatory: false},
                                {step_tool_id: 231, custom_field_desc_id: 11, pos: 1, visible: true, obligatory: false}
                              ])

# METHOD AMELIORATION
Meth::Methodology.create!([
                            {id: 20, name_fr: 'Amélioration', name_en: 'Improvement'}
                          ])
Meth::Step.create!([
                     {id: 301, name: 'Infos', pos: 0, methodology_id: 20, step_role_id: 1, step_type: 2},
                     {id: 302, name: 'Tools Trs', pos: 0, methodology_id: 20, step_role_id: 2, step_type: 3},
                     {id: 303, name: 'Improvement', pos: 1, methodology_id: 20, step_role_id: 7},
                     {id: 304, name: 'Clôture', pos: 8, methodology_id: 20, step_role_id: 11}
                   ])
Meth::StepTool.create!([
                         {id: 303, step_id: 303, tool_id: 20, pos: 4, name: 'PA Amélioration', tool_reference: 1},
                         {id: 305, step_id: 304, tool_id: 18, pos: 1, name: 'Clôture', tool_reference: 1, tool_options: {closure: true}},
                         {id: 306, step_id: 301, tool_id: 18, pos: 1, name: 'Infos', tool_reference: 1},
                         {id: 308, step_id: 301, tool_id: 18, pos: 3, name: 'Client/Fournisseur', tool_reference: 1},
                         {id: 309, step_id: 302, tool_id: 27, pos: 2, name: 'Action Plan', tool_reference: 0},
                         {id: 310, step_id: 302, tool_id: 28, pos: 3, name: 'Report', tool_reference: 0},
                         {id: 311, step_id: 302, tool_id: 25, pos: 5, name: 'Documents', tool_reference: 0},
                         {id: 312, step_id: 302, tool_id: 29, pos: 1, name: 'Criticité', tool_reference: 0},
                         {id: 313, step_id: 302, tool_id: 26, pos: 4, name: 'Images', tool_reference: 0}
                       ])

# Assign custom fields to StepTools
Meth::ToolCustomField.create!([
                                {step_tool_id: 306, custom_field_desc_id: 1, pos: 1, visible: true, obligatory: false},
                                {step_tool_id: 306, custom_field_desc_id: 4, pos: 2, visible: true, obligatory: false},
                                {step_tool_id: 306, custom_field_desc_id: 5, pos: 3, visible: true, obligatory: false},
                                {step_tool_id: 308, custom_field_desc_id: 6, pos: 1, visible: true, obligatory: false},
                                {step_tool_id: 308, custom_field_desc_id: 7, pos: 2, visible: true, obligatory: false},
                                {step_tool_id: 308, custom_field_desc_id: 8, pos: 3, visible: true, obligatory: false},
                                {step_tool_id: 305, custom_field_desc_id: 11, pos: 1, visible: true, obligatory: false}
                              ])

# METHOD PDCA
Meth::Methodology.create!([
                            {id: 13, name_fr: 'PDCA', name_en: 'PDCA'}
                          ])
Meth::Step.create!([
                     # Methodo PDCA
                     {id: 411, name: 'Infos', pos: 0, methodology_id: 13, step_role_id: 1, step_type: 2},
                     {id: 412, name: 'Tools Trs', pos: 0, methodology_id: 13, step_role_id: 2, step_type: 3},
                     {id: 422, name: 'P (Plan)', pos: 2, methodology_id: 13, step_role_id: 5},
                     {id: 424, name: 'D (DO)', pos: 4, methodology_id: 13, step_role_id: 7},
                     {id: 425, name: 'C (CHECK)', pos: 5, methodology_id: 13, step_role_id: 8},
                     {id: 426, name: 'A (ACT)', pos: 6, methodology_id: 13, step_role_id: 9},
                     {id: 428, name: 'CLOTÛRE', pos: 8, methodology_id: 13, step_role_id: 11}
                   ])
Meth::StepTool.create!([
                         # Methodo PDCA
                         {id: 421, step_id: 422, tool_id: 10, pos: 1, name: 'Equipe', tool_reference: 1},
                         # {id: 22, step_id: 23, tool_id: 3, pos: 1, name: 'PA Conservatoire', tool_reference: 1},
                         {id: 423, step_id: 424, tool_id: 13, pos: 1, name: 'Cause Tree', tool_reference: 1},
                         {id: 424, step_id: 424, tool_id: 14, pos: 2, name: 'Ishikawa', tool_reference: 1},
                         {id: 425, step_id: 424, tool_id: 15, pos: 3, name: '5 Why', tool_reference: 1},
                         {id: 426, step_id: 425, tool_id: 5, pos: 1, name: 'PA Corrective', tool_reference: 1},
                         {id: 427, step_id: 425, tool_id: 6, pos: 2, name: 'Matrice Décision', tool_reference: 1},
                         {id: 428, step_id: 426, tool_id: 9, pos: 1, name: 'Graph vérif', tool_reference: 1},
                         # {id: 29, step_id: 27, tool_id: 7, pos: 1, name: 'PA Préventive', tool_reference: 1},
                         # {id: 30, step_id: 427, tool_id: 8, pos: 2, name: 'Vérif efficacité', tool_reference: 1},
                         {id: 431, step_id: 428, tool_id: 18, pos: 1, name: 'Clôture', tool_reference: 1, tool_options: {closure: true}},
                         {id: 432, step_id: 411, tool_id: 18, pos: 1, name: 'Infos', tool_reference: 1},
                         {id: 433, step_id: 422, tool_id: 11, pos: 2, name: 'QQOQP', tool_reference: 1},
                         {id: 434, step_id: 424, tool_id: 16, pos: 4, name: 'PA Validation', tool_reference: 1},
                         {id: 435, step_id: 411, tool_id: 18, pos: 2, name: 'NC', tool_reference: 1},
                         {id: 436, step_id: 411, tool_id: 18, pos: 3, name: 'Client/Fournisseur', tool_reference: 1},
                         {id: 437, step_id: 412, tool_id: 27, pos: 2, name: 'Action Plan', tool_reference: 0},
                         {id: 438, step_id: 412, tool_id: 28, pos: 3, name: 'Report', tool_reference: 0},
                         {id: 439, step_id: 412, tool_id: 25, pos: 5, name: 'Documents', tool_reference: 0},
                         {id: 440, step_id: 412, tool_id: 29, pos: 1, name: 'Criticité', tool_reference: 0},
                         {id: 441, step_id: 412, tool_id: 26, pos: 4, name: 'Images', tool_reference: 0},
                         {id: 442, step_id: 426, tool_id: 8, pos: 2, name: 'Efficacité', tool_reference: 1}
                       ])
# Assign custom fields to StepTools
Meth::ToolCustomField.create!([
                                {step_tool_id: 432, custom_field_desc_id: 1, pos: 1, visible: true, obligatory: false},
                                {step_tool_id: 432, custom_field_desc_id: 4, pos: 2, visible: true, obligatory: false},
                                {step_tool_id: 432, custom_field_desc_id: 5, pos: 3, visible: true, obligatory: false},
                                {step_tool_id: 435, custom_field_desc_id: 3, pos: 1, visible: true, obligatory: false},
                                {step_tool_id: 435, custom_field_desc_id: 2, pos: 2, visible: true, obligatory: false},
                                {step_tool_id: 435, custom_field_desc_id: 9, pos: 3, visible: true, obligatory: false},
                                {step_tool_id: 435, custom_field_desc_id: 10, pos: 4, visible: true, obligatory: false},
                                {step_tool_id: 436, custom_field_desc_id: 6, pos: 1, visible: true, obligatory: false},
                                {step_tool_id: 436, custom_field_desc_id: 7, pos: 2, visible: true, obligatory: false},
                                {step_tool_id: 436, custom_field_desc_id: 8, pos: 3, visible: true, obligatory: false},
                                {step_tool_id: 431, custom_field_desc_id: 11, pos: 1, visible: true, obligatory: false}
                              ])

# Tags
Tag.create!([
              {id: 1, name: 'Univers', taggings_count: 0, ancestry: nil}
            ])

Labels::CauseType.create!([
                            {id: 1, name_fr: 'Mesure',         name_en: 'Measurement'},
                            {id: 2, name_fr: 'Matière',        name_en: 'Material'},
                            {id: 3, name_fr: 'Méthode',        name_en: 'Method'},
                            {id: 4, name_fr: 'Main d\'oeuvre', name_en: 'Employee'},
                            {id: 5, name_fr: 'Milieu',         name_en: 'Environment'},
                            {id: 6, name_fr: 'Monnaie',        name_en: 'Money'},
                            {id: 7, name_fr: 'Management',     name_en: 'Management'},
                            {id: 8, name_fr: 'Machine',        name_en: 'Machine'}
                          ])

Profil.create!([
                 {id: 1, profil_name: 'author', name_fr: 'Auteur', name_en: 'Author', profil_locked: 1, profil_modifiable: 0, pos: 1},
                 {id: 2, profil_name: 'resp_rca', name_fr: 'Responsable RCA', name_en: 'RCA Responsible', profil_locked: 1, profil_modifiable: 1, pos: 2},
                 {id: 3, profil_name: 'pilote', name_fr: 'Pilote', name_en: 'Pilote', profil_locked: 0, profil_modifiable: 1, pos: 3},
                 {id: 4, profil_name: 'project', name_fr: 'Responsable Projet', name_en: 'Project Manager', profil_locked: 0, profil_modifiable: 1, pos: 4},
                 {id: 5, profil_name: 'quality', name_fr: 'Responsable Qualité', name_en: 'Quality Manager', profil_locked: 0, profil_modifiable: 1, pos: 5},
                 {id: 6, profil_name: 'customer', name_fr: 'Correspondant Client', name_en: 'Customer Contact', profil_locked: 0, profil_modifiable: 1, pos: 6},
                 {id: 7, profil_name: 'supplye', name_fr: 'Correspondant Fournisseur', name_en: 'Supplyer Contact', profil_locked: 0, profil_modifiable: 1, pos: 7},
                 {id: 8, profil_name: 'member', name_fr: "Membre de l'équipe", name_en: 'Team Member', profil_locked: 0, profil_modifiable: 1, pos: 8},
                 {id: 9, profil_name: 'resp_act', name_fr: 'Responsable Action', name_en: 'Action Responsible', profil_locked: 1, profil_modifiable: 0, pos: 9}
               ])

# Help texts
Admin::HelperText.create!([
                            {id: 1, tool_class: 'Tools::Qqoqcp', reference: 'who', help_text_fr: 'Quel est le défaut ? Décrivez le, ses caractéristiques ?'},
                            {tool_class: 'Tools::Qqoqcp', reference: 'what', help_text_fr: 'Qui à détecter le défaut ? Qui est impacté par le défaut ?'},
                            {tool_class: 'Tools::Qqoqcp', reference: 'where', help_text_fr: 'Où le défaut a-t-il été détecté ? Où sur la pièce ?'},
                            {tool_class: 'Tools::Qqoqcp', reference: 'when', help_text_fr: 'Quand a-t-il été détecté ?'},
                            {tool_class: 'Tools::Qqoqcp', reference: 'how', help_text_fr: 'Comment a-t-il été détecté ?'},
                            {tool_class: 'Tools::Qqoqcp', reference: 'why', help_text_fr: 'Pourquoi est-ce un problème pour le client ?'},
                            {tool_class: 'Tools::Qqoqcp', reference: 'howmuch', help_text_fr: 'Combien de produits sont concernés ?'}
                          ])

# Reactivation foreign_key check<
ActiveRecord::Base.connection.execute('SET FOREIGN_KEY_CHECKS = 1')
