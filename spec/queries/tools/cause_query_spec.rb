require 'rails_helper'

describe Tools::CauseQuery, type: 'query' do
  subject(:query) { Tools::CauseQuery.relation }

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }

  before :each do
    Tools::Cause.create!([
                           {id: 1, rca: rca, tool_reference: 1, name: 'Rca 1'},
                           {id: 2, rca: rca, tool_reference: 2, name: 'Rca 2 / Cause 1'},
                           {id: 3, rca: rca, tool_reference: 1, name: 'Cause 2', ancestry: '1'},
                           {id: 4, rca: rca, tool_reference: 1, name: 'Cause 2.1', ancestry: '1/3'},
                           {id: 5, rca: rca, tool_reference: 1, name: 'Cause 2.2', ancestry: '1/3'},
                           {id: 6, rca: rca, tool_reference: 2, name: 'Cause 1.1', ancestry: '2'},
                           {id: 7, rca: rca, tool_reference: 2, name: 'Cause 1.2', ancestry: '2'},
                           {id: 8, rca: rca, tool_reference: 1, name: 'Cause 2.2.1', ancestry: '1/3/5'},
                           {id: 9, rca: rca, tool_reference: 1, name: 'Cause 2.1.1', ancestry: '1/3/4'}
                         ])
  end

  describe '#causes_subtree_by_id' do
    it 'returns the subtree arranged as array for list' do
      result = query.causes_subtree_by_id(rca.id, 1)
      # expect(result).to eq [Tools::Cause.find(1), Tools::Cause.find(3), Tools::Cause.find(4), Tools::Cause.find(9), Tools::Cause.find(5), Tools::Cause.find(8)]
      expect(result.map(&:id)).to eq [1, 3, 4, 9, 5, 8]
    end
  end
end
