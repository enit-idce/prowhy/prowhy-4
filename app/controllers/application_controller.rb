class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  # before_action :sign_in_admin
  before_action :set_locale

  before_action :configure_permitted_parameters, if: :devise_controller?

  # Track all actions
  # after_action :track_action
  after_action :call_track_action, only: [:create, :destroy, :update]

  include Pagy::Backend
  include Pundit::Authorization
  # after_action :verify_authorized, except: :index
  # after_action :verify_policy_scoped, only: :index

  # FOR API !!! => Pundit Autorize
  # def current_user
  #   # @current_user ||= resource_owner
  #   @current_user ||= if doorkeeper_token
  #                       User.find(doorkeeper_token.resource_owner_id)
  #                     else
  #                       warden.authenticate(scope: :user)
  #                     end
  # end

  def sign_in_admin
    sign_in User.first
  end

  def init_rca
    if !params[:rca].nil?
      # @rca = Rca.find(params[:rca])
      @rca = policy_scope(Rca).find(params[:rca])
    else
      # Cherche rca ou rca_id dans les params (dans les parametres de l'objt, pour les fonctions new et create)
      params.each_key do |k|
        if !params[k][:rca].nil?
          # @rca = Rca.find(params[key][:rca])
          @rca = policy_scope(Rca).find(params[key][:rca])
          break
        elsif !params[k][:rca_id].nil?
          # @rca = Rca.find(params[k][:rca_id])
          @rca = policy_scope(Rca).find(params[k][:rca_id])
          break
        end
      rescue StandardError
        next
      end
    end

    # authorize @rca, :show? if @rca
  end

  # Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  # rescue_from Pundit::AuthorizationNotPerformedError, with: :pundit_not_performed

  def user_not_authorized(exception)
    # Here I've got the exception with :policy, :record and :query,
    # also I can access :current_user so I could go for a condition,
    # but that would include duplicated code from  ItemPolicy#show?.

    # policy_name = exception.policy.class.to_s.underscore
    # binding.pry
    # message = exception.reason ? I18n.t("pundit.errors.#{exception.reason}") : exception.message

    policy_name = exception.policy.class.to_s.underscore
    message = t("pundit.errors.#{policy_name}.#{exception.query}", default: exception)
    # binding.pry

    flash.now[:error] = "You are not allowed to perform this action : #{message}"
    flash[:error] = "You are not allowed to perform this action : #{message}"

    puts "You've been rejected by Pundit : #{message} => Go back !".red
    # redirect_back(fallback_location: root_path)
    respond_to do |format|
      format.html { redirect_back(fallback_location: root_path) }
      format.js { render js: "window.location='#{request.referrer || root_path}'" }
      # format.js { render html: 'index' }
    end
    # render('layouts/no_authorize.html')
  end

  # def pundit_not_performed(_exception)
  #   # Here I've got the exception with :policy, :record and :query,
  #   # also I can access :current_user so I could go for a condition,
  #   # but that would include duplicated code from  ItemPolicy#show?.
  #   flash.now[:error] = "Pundit' is not performed ! Go away !"
  #   puts "Pundit' is not performed ! Go away !".red
  #   # redirect_back(fallback_location: root_path) and return
  # end

  # def create_route_js
  #   # testfct = { url: eval(params[:fct]+'path', params[:id]) }
  #   testfct = { url: Rails.application.routes.url_helpers.send(params[:fct]+'_path', id: params[:id]) }
  #   puts testfct

  #   render json: testfct
  # end

  # def self.default_url_options
  #   binding.pry
  #   { locale: I18n.locale }
  # end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up,
                                      keys: [:username, :email, :password, :password_confirmation,
                                             person_attributes: [:id, :firstname, :lastname, :adress, :phone, :moreinfo]])
    devise_parameter_sanitizer.permit(:sign_in,
                                      keys: [:login, :password, :password_confirmation])
    devise_parameter_sanitizer.permit(:account_update,
                                      keys: [:username, :email, :password_confirmation, :current_password,
                                             person_attributes: [:id, :firstname, :lastname, :adress, :phone, :moreinfo]])
  end

  # Tracking / Ahoy
  def track_action(msg = nil, rca = nil, detail_class_name = nil, detail_id = nil, params_in = {})
    #   ahoy.track msg || 'Default', params_in.merge(rca_id: rca&.id, detail_type: detail&.class&.name, detail_id: detail&.id)
    # rescue StandardError
    #   ahoy.track msg || 'Default', {rca_id: rca&.id, detail_type: detail&.class&.name, detail_id: detail&.id}
    ahoy.track msg || 'Default', params_in.merge(rca_id: rca&.id, detail_type: detail_class_name, detail_id: detail_id)
  rescue StandardError
    ahoy.track msg || 'Default', {rca_id: rca&.id, detail_type: detail_class_name, detail_id: detail_id}
  end

  def call_track_action
    name_ctrl = params[:controller]
    begin
      obj_name = name_ctrl.gsub('/', '_').singularize
    rescue StandardError
      obj_name = 'no_object'
    end

    begin
      obj = instance_variable_get("@#{obj_name}")
      obj_class_name = obj.class.name
      obj_id = obj.id
    rescue StandardError
      # obj = nil
      obj_class_name = params[:controller]&.split('/')&.first&.classify
      obj_id = nil
    end

    begin
      my_params = send("#{obj_name}_params") # tools_qqoqcp_params
    rescue StandardError
      my_params = {}
    end

    # binding.pry
    # track_action("RCA - #{params[:action]}", {rca_id: @rca&.id}.merge(rca_p))
    begin
      track_action("#{name_ctrl.classify} - #{params[:action]}",
                   @rca,
                   obj_class_name, obj_id,
                   my_params)
    rescue StandardError
      puts 'Track action ERROR.'.red
    end
  end

  private

  def set_locale
    I18n.locale = extract_locale || I18n.default_locale
    # binding.pry
  end

  def extract_locale
    parsed_locale = params[:locale]
    I18n.available_locales.map(&:to_s).include?(parsed_locale) ? parsed_locale : nil
  end

  def default_url_options
    # binding.pry
    { locale: I18n.locale }
  end

  def create_tab_permit_custom
    tab_permit = [:rca_id]
    Meth::CustomFieldDesc.all.each do |cfield|
      # if cfield.custom_field_type.field_internal == 'tag'
      #   tab_permit << cfield.internal_name + '_list'
      # else
      #   tab_permit << cfield.internal_name
      # end

      tab_permit << if cfield.custom_field_type.field_internal == 'tag'
                      "#{cfield.internal_name}_list"
                    else
                      cfield.internal_name
                    end
    end
    tab_permit
  end

end
