require_dependency 'admin'

module Admin
  class AdminConfig < ApplicationRecord
    belongs_to :default_user_role, class_name: 'Role', foreign_key: 'default_role_id', optional: true
    belongs_to :default_perimeter, class_name: 'Perimeter', optional: true
    serialize :connexions
    serialize :task_mails

    has_one_attached :customer_logo
    has_one_attached :customer_image

    validates :connexions, presence: true
    validates :task_mails, presence: true

    validate :acceptable_images

    attr_accessor :next_default_role

    after_initialize do
      if new_record?
        # Init configurations
        self.connexions = { database_login: true,
                            database_signup: true,
                            ldap_login: true }
        self.default_user_role = Role.where(name: 'visitor')&.first
        self.task_mails = { activate: false,
                            dday: true,
                            late: 2,
                            ndays: [1, 2, 7],
                            locale: 'en',
                            send_action_closed: false }
        self.options = { image_max_size: 1,
                         doc_max_size: 1,
                         file_versions_nb_max: 0,
                         # Référence (rca.reference)
                         rca_reference: false,
                         rca_reference_txt: '',
                         rca_reference_lock: false,
                         # Code barre & Qr code activation & options
                         rca_cod: false,
                         rca_cod_txt: '',
                         rca_cod_type: '',
                         # Association Rca parent/enfant (tool_rcalist)
                         rca_permit_assoc: false
                       }
        self.default_perimeter = Perimeter.all&.first
        self.home_options = { show_desc: true,
                              show_methodo: true,
                              show_adv_etape: true,
                              show_adv_role: true,
                              show_adv_duration: true,
                              target_adv_duration: 0,
                              show_custom_order: [],
                              show_profil: []
                            }
      end
    end

    def connexion_attribute(attr)
      val = connexions[attr]
      val = true if connexions[attr] == 'true'
      val = false if connexions[attr] == 'false'

      val
    end

    def task_mail_attribute(attr)
      val = task_mails[attr]
      val = true if task_mails[attr] == 'true'
      val = false if task_mails[attr] == 'false'

      val
    end

    def option_attribute(attr)
      val = options[attr]
      val = true if val == 'true'
      val = false if val == 'false'

      val
    end

    def self.get_option_attribute(attr)
      admin_config = self.first
      return admin_config ? admin_config.option_attribute(attr.to_s) : nil
    end

    private

    def acceptable_images
      acceptable_types = ['image/jpeg', 'image/png']
      errors.add(:customer_logo, 'File must be a JPEG or PNG image format') unless !customer_logo.attached? || acceptable_types.include?(customer_logo.content_type)
      errors.add(:customer_image, 'File must be a JPEG or PNG image format') unless !customer_image.attached? || acceptable_types.include?(customer_image.content_type)
    end
  end
end
