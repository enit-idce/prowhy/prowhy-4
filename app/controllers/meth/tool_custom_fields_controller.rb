module Meth
  class ToolCustomFieldsController < ApplicationController
    include AdminConcern
    # layout 'admin'
    before_action :set_meth_tool_custom_field, only: [:show, :edit, :update, :destroy, :tool_custom_field_up, :tool_custom_field_down]
    before_action :init_rca, only: [:show_tool]

    # GET /tool_custom_fields
    # GET /tool_custom_fields.json
    def index
      authorize ToolCustomField
      @step_tool = params[:step_tool]&.to_i || nil
      @tool_custom_fields = if @step_tool
                              ToolCustomField.where(step_tool_id: @step_tool).order(:pos)
                            else
                              ToolCustomField.all.order(:step_tool_id).order(:pos)
                            end
      @custom_field_descs = CustomFieldDesc.all
    end

    # GET /tool_custom_fields/1
    # GET /tool_custom_fields/1.json
    def show
    end

    # GET /tool_custom_fields/new
    def new
      @tool_custom_field = ToolCustomField.new
      authorize @tool_custom_field
    end

    # GET /tool_custom_fields/1/edit
    def edit
    end

    # POST /tool_custom_fields
    # POST /tool_custom_fields.json
    def create
      @tool_custom_field = ToolCustomField.new(meth_tool_custom_field_params)
      authorize @tool_custom_field

      step_tool_id = @tool_custom_field.step_tool_id

      respond_to do |format|
        if @tool_custom_field.save
          Positions::MoveService.new(@tool_custom_field, @tool_custom_field.step_tool.tool_custom_fields).call('reinit')
          format.html { redirect_to meth_tool_custom_fields_url(step_tool: step_tool_id), notice: 'Tool custom field was successfully created.' }
          format.json { render :show, status: :created, location: @tool_custom_field }
        else
          # binding.pry
          format.html { redirect_to meth_tool_custom_fields_url(step_tool: step_tool_id) }
          format.json { render json: @tool_custom_field.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /tool_custom_fields/1
    # PATCH/PUT /tool_custom_fields/1.json
    def update
      respond_to do |format|
        if @tool_custom_field.update(meth_tool_custom_field_params)
          format.html { redirect_to @tool_custom_field, notice: 'Tool custom field was successfully updated.' }
          format.json { render :show, status: :ok, location: @tool_custom_field }
        else
          format.html { render :edit }
          format.json { render json: @tool_custom_field.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /tool_custom_fields/1
    # DELETE /tool_custom_fields/1.json
    def destroy
      step_tool_id = @tool_custom_field.step_tool_id
      @tool_custom_field.destroy
      respond_to do |format|
        format.html { redirect_to meth_tool_custom_fields_url(step_tool: step_tool_id), notice: 'Tool custom field was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    # GET show_tool_custom_field
    def show_tool
      # authorize ToolCustomField
      skip_authorization

      if @rca.nil?
        redirect_to rcas_path
      else
        authorize @rca, :show?, policy_class: Tools::ToolsPolicy

        # tool = Tool.find(params[:tool])
        # step = Step.find(params[:step])
        # tool_custom_fields = ToolCustomField.includes(:step_tool).where(step_tool: {step: step, tool: tool})

        step_tool = StepTool.includes(:tool_custom_fields).find(params[:step_tool])
        tool_custom_fields = step_tool&.tool_custom_fields # ToolCustomField.where(step_tool: step_tool)

        custom_field = @rca.custom_field # .includes(:custom_field_desc)

        render 'show_tool_custom_field', layout: 'processus', locals: {tool_custom_fields: tool_custom_fields, custom_field: custom_field, step_tool: step_tool}
      end
    end

    # PATCH
    def tool_custom_field_up
      tool_custom_fields = @tool_custom_field.step_tool.tool_custom_fields
      Positions::MoveService.new(@tool_custom_field, tool_custom_fields).call('up')

      redirect_to meth_tool_custom_fields_path(step_tool: @tool_custom_field.step_tool)
    end

    def tool_custom_field_down
      tool_custom_fields = @tool_custom_field.step_tool.tool_custom_fields
      Positions::MoveService.new(@tool_custom_field, tool_custom_fields).call('down')

      redirect_to meth_tool_custom_fields_path(step_tool: @tool_custom_field.step_tool)
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_meth_tool_custom_field
      @tool_custom_field = ToolCustomField.find(params[:id])
      authorize @tool_custom_field
    end

    # Only allow a list of trusted parameters through.
    def meth_tool_custom_field_params
      params.require(:meth_tool_custom_field).permit(:step_tool_id, :custom_field_desc_id, :obligatory)
    end
  end
end
