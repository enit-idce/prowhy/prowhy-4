class UpdateMethCustomFieldTypes < ActiveRecord::Migration[6.0]
  def change
    add_column :meth_custom_field_types, :field_size, :integer, after: :field_type
    add_column :meth_custom_field_types, :field_internal, :string, after: :field_size
  end
end
