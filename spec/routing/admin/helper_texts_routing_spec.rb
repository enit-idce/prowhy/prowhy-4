require 'rails_helper'

RSpec.describe Admin::HelperTextsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/helper_texts').to route_to('admin/helper_texts#index')
    end

    it 'routes to #new' do
      expect(get: '/admin/helper_texts/new').to route_to('admin/helper_texts#new')
    end

    it 'routes to #show' do
      expect(get: '/admin/helper_texts/1').to route_to('admin/helper_texts#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/admin/helper_texts/1/edit').to route_to('admin/helper_texts#edit', id: '1')
    end


    it 'routes to #create' do
      expect(post: '/admin/helper_texts').to route_to('admin/helper_texts#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/admin/helper_texts/1').to route_to('admin/helper_texts#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/admin/helper_texts/1').to route_to('admin/helper_texts#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/admin/helper_texts/1').to route_to('admin/helper_texts#destroy', id: '1')
    end
  end
end
