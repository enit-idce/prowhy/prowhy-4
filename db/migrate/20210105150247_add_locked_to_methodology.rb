class AddLockedToMethodology < ActiveRecord::Migration[6.0]
  def change
    add_column :meth_methodologies, :locked, :boolean, null: false, default: false, after: :id
  end
end
