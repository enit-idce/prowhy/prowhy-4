module FilesHelper
  def file_preview(file, params = {})
    params[:resize] = true if params[:resize].nil?
    params[:sx] ||= 200
    params[:sy] ||= 200
    params[:show_filename] = true if params[:show_filename].nil?

    ret = ''
    show_doc = false
    if file&.attached?
      show_doc = true
      ret += "<p><b>Filename : </b>#{file.filename}</p>" if params[:show_filename]

      if file&.previewable?
        ret += if params[:resize]
                 image_tag file.preview(resize_to_limit: [params[:sx], params[:sy]])
               else
                 image_tag file
               end
      elsif file.variable?
        ret += if params[:resize]
                 image_tag file.variant(resize_to_limit: [params[:sx], params[:sy]])
               else
                 image_tag file
               end
      else
        ret += 'No preview available'
        show_doc = false
      end
    end

    return ret, show_doc
  end

  def file_preview_multiple(files, file, params = {})
    params[:resize] = true if params[:resize].nil?
    params[:sx] ||= 200
    params[:sy] ||= 200
    params[:show_filename] = true if params[:show_filename].nil?

    ret = ''
    show_doc = false
    if files&.attached?
      show_doc = true
      ret += "<p><b>Filename : </b>#{file.filename}</p>" if params[:show_filename]

      if file&.previewable?
        ret += if params[:resize]
                 image_tag file.preview(resize_to_limit: [params[:sx], params[:sy]])
               else
                 image_tag file
               end
      elsif file.variable?
        ret += if params[:resize]
                 image_tag file.variant(resize_to_limit: [params[:sx], params[:sy]])
               else
                 image_tag file
               end
      else
        ret += 'No preview available'
        show_doc = false
      end
    end

    return ret, show_doc
  end
end
