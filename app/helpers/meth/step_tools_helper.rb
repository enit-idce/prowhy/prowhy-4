module Meth
  module StepToolsHelper
    # Helper
    #  affichage des infos d'un CustomField
    #  édition du CustomField
    def step_tool_edit_custom_field(step_tool)
      return '' unless step_tool&.tool&.tool_name == 'CustomField'

      content = ' '
      step_tool.tool_custom_fields.each do |tcf|
        content += tcf.custom_field_desc.name + '<br/>'
      end
      # content = step_tool.tool_custom_fields.map do |tcf|
      #             tcf.custom_field_desc.name + '<br/>'
      #           end

      ret = ' ' + info_popup('Custom Fields', content) + ' '
      ret += link_to fa_icon_list,
                     meth_tool_custom_fields_path(step_tool: step_tool),
                     title: 'Edit Custom fields list' # , 'data-toggle' => 'tooltip'

      ret.html_safe
    end

    #  affichage des infos d'un Ishikawa
    #  édition Ishikawa
    def step_tool_edit_ishikawa(step_tool)
      return '' unless step_tool&.tool&.tool_name == 'Ishikawa'

      content = ''
      # if step_tool.tool_options[:labels]
      step_tool&.tool_options&.[](:labels)&.each do |_k, v|
        label = Labels::CauseType.find(v)
        content += (label ? label.name : '-- label does not exist --') + '<br/>'
      end
      # end
      ret = ' ' + info_popup('Ishikawa', content) + ' '

      ret += link_to fa_icon_list,
                     labels_cause_types_list_path(step_tool: step_tool),
                     title: 'Edit cause types list',
                     remote: true # , 'data-toggle' => 'tooltip'

      ret.html_safe
    end

    # Helper affichage nom du step tool + icone pour édition
    def step_tool_show_name(step_tool)
      step_tool.name ||= step_tool.tool.name
      ret = step_tool.name_show_activity + ' '

      ret += link_to fa_icon_edit, edit_meth_step_tool_path(step_tool),
                     remote: true,
                     title: 'Edit StepTool name and description' # , 'data-toggle' => 'tooltip'

      # ret += best_in_place step_tool, :name,
      #                      as: :input,
      #                      ok_button: '&#x2714;'.html_safe,
      #                      cancel_button: '&#x2718;'.html_safe,
      #                      display_with: lambda { |val| best_in_place_display(val)&.html_safe },
      #                      html_attrs: { class: 'bip-input' },
      #                      place_holder: step_tool.tool.name + ' (click to change)',
      #                      class: 'bip-span'

      ret.html_safe
    end

    # Menus helpers
    def step_menu_name_show(step_tool)
      ret = "<span style='width:20px;display:inline-block;'>".html_safe
      ret += step_tool.tool.tool_icon ? fa_icon(step_tool.tool.tool_icon) : ''
      ret += '</span>'.html_safe
      ret += step_tool.name_show_short

      ret.html_safe
    end

    def step_menu_trs_name_show(step_tool)
      step_tool.tool.tool_icon ? fa_icon(step_tool.tool.tool_icon) : step_tool.name
    end

    # Report

    def step_tool_show_report(rca, step_tool, export_values)
      if step_tool.tool.tool_name == 'CustomField'
        # 'CUSTOM FIELDS'
        render partial: 'meth/tool_custom_fields/show_report', locals: {custom_field: rca.custom_field, step_tool: step_tool}
      elsif step_tool.tool.tool_name == 'Qqoqcp'
        qqoqcp = Tools::Qqoqcp.search_default(rca, 'tools_qqoqcps', step_tool)
        if qqoqcp
          render partial: 'tools/qqoqcps/show', locals: {tools_qqoqcp: qqoqcp}
        else
          'No QQOQCP'
        end
      elsif step_tool.tool.tool_name == 'CauseTree'
        # cause_tree = @rca.find_or_create_tool('tools_cause_trees', 'Tools::CauseTree', step_tool)
        tools_causes = Tools::CauseQuery.relation.causes_subtree_by_id(rca.id, step_tool.tool_reference)
        if tools_causes && !tools_causes.empty?
          @ishikawas, @ishikawa_names = tools_causes.first.init_ishikawas_list

          render partial: 'tools/causes/show', locals: {tools_causes: tools_causes}
        else
          'No Cause defined'
        end
      elsif step_tool.tool.tool_name == 'Why'
        @cause_tree = Tools::CauseTree.search_default(rca, 'tools_cause_trees', step_tool)
        if @cause_tree
          render partial: 'tools/cause_trees/index_5why', locals: {show_buttons: false}
        else
          'No Cause defined'
        end
      elsif step_tool.tool.tool_name == 'Action'
        # tasks = Tools::Task.search_default(rca, '', step_tool)

        # if tasks && !tasks.empty?
        task_options = step_tool.tool_options[:task_options] || {}
        plan_options = step_tool.tool_options[:plan_options] || {}

        # Default values (if not defined in tool_options)
        plan_options[:causes] ||= false
        plan_options[:rcas] ||= false
        # plan_options[:user] ||= false # true
        plan_options[:type_plan] ||= 'rca' # 'user' / 'global'
        plan_options[:blocks] ||= false
        plan_options[:blocks_column] ||= 'task_detail_type'
        plan_options[:efficacity] ||= false
        plan_options[:matrix] ||= false
        plan_options[:validation] ||= (task_options[:task_detail_type].class == Array &&
                                       task_options[:task_detail_type].include?('Tools::TaskValidation')) ||
                                      (task_options[:task_detail_type].class == String &&
                                       task_options[:task_detail_type] == 'Tools::TaskValidation')

        # TEST POUR PLAN ACTION AUDIT - rca-list
        if plan_options[:sub_type_plan] == 'rca-list' && rca
          plan_options[:rcas] = true
          rcas_ids = [rca.id]
          tools_rca_lists = if step_tool.tool_reference == 0
                              rca.tools_rca_lists.includes(:rcas)
                            else
                              rca.tools_rca_lists.where(tool_reference: step_tool.tool_reference).includes(:rcas)
                            end

          tools_rca_lists.each do |tool_rca_l|
            tool_rca_l.rcas.each do |subrca|
              rcas_ids << subrca.id
            end
          end
          tasks = Tools::Tasks::TaskPlanGetTasks.new(rca, rcas_ids, current_user).init_tasks(plan_options, task_options)
          # binding.pry
        else
          tasks = Tools::Task.search_default(rca, '', step_tool)
        end

        if tasks && !tasks.empty?
          @tools_tasks = tasks
          @step_tool = step_tool

          @task_columns_type = task_options[:task_detail_type].class == Array ? 'TaskGlobal' : task_options[:task_detail_type]
          @task_columns = current_user.columns_options(task_options[:task_detail_type])

          render partial: 'tools/tasks/index_task_plan', locals: {rca: rca, plan_options: plan_options, task_options: task_options, show_buttons: false}
        else
          'No Tasks defined'
        end
        # redirect_to tools_tasks_path(step_tool: @step_tool, rca: @rca)
      elsif step_tool.tool.tool_name == 'Ishikawa'
        # ishikawa = @rca.find_or_create_tool('tools_ishikawas', 'Tools::Ishikawa', @step_tool)
        ishikawa = Tools::Ishikawa.search_default(rca, 'tools_ishikawas', step_tool)
        if ishikawa
          @causes = ishikawa.cause&.descendants
          @tools_ishikawa = ishikawa
          render partial: 'tools/ishikawas/show'
        else
          'No Ishikawa defined'
        end
        # redirect_to tools_ishikawas_path(id: ishikawa.id, step_tool: @step_tool, rca: @rca)
      # elsif step_tool.tool.tool_name == 'DecisionMat'
      #   step_tool.tool.name
      #   # redirect_to tools_decision_mats_path(step_tool: @step_tool, rca: @rca)
      elsif step_tool.tool.tool_name == 'Team'
        teams = Tools::Team.search_default(rca, '', step_tool)
        if teams && !teams.empty?
          render partial: 'tools/teams/show', locals: {tools_teams: teams}
        else
          'No Team defined'
        end
        # redirect_to tools_teams_path(step_tool: @step_tool, rca: @rca)
      elsif step_tool.tool.tool_name == 'Document'
        step_tool.tool.name
        # redirect_to tools_documents_path(step_tool: @step_tool, rca: @rca)
      elsif step_tool.tool.tool_name == 'Graph'
        @tools_indicators = Tools::Indicator.search_default(rca, 'tools_indicators', step_tool).includes(:indicator_measures)
        if @tools_indicators && !@tools_indicators.empty?
          render partial: 'tools/indicators/show', locals: {export_values: export_values, step_tool: step_tool}
        else
          'No indicator defined.'
        end
      elsif step_tool.tool.tool_name == 'DecisionMat'
        @tools_decision_mats = Tools::DecisionMat.search_default(rca, 'tools_decision_mats', step_tool)
        if @tools_decision_mats && !@tools_decision_mats.empty?
          render partial: 'tools/decision_mats/show', locals: {export_values: export_values, step_tool: step_tool}
        else
          'No matrix defined.'
        end
      elsif step_tool.tool.tool_name == 'ActionCheck'
        # Get options to get datas
        task_options = step_tool.tool_options[:task_options] || {}
        task_options = if step_tool.tool_reference != 0
                         task_options.merge!(tool_reference: step_tool.tool_reference,
                                             tool_num: step_tool.tool_num,
                                             ancestry: nil)
                       else
                         task_options.merge!(ancestry: nil)
                       end
        more_options = step_tool.tool_options[:more_options] || {}

        # Load tasks : more_options = where matrix_status => Implemented
        @tools_tasks = Tools::TaskQuery.relation.rca_tasks_by_column(rca.id, task_options, {})
                                       .includes(:efficacity)
                                       .left_joins(:matrix_stat)
                                       .where(more_options)
        if @tools_tasks && !@tools_tasks.empty?
          render partial: 'tools/check_efficacity/index', locals: {show_buttons: false}
        else
          'No action defined.'
        end
      elsif step_tool.tool.tool_name == 'RcaList'
        @tools_rca_list = Tools::RcaList.search_default(rca, 'tools_rca_lists', step_tool)
        if @tools_rca_list
          render partial: 'tools/rca_lists/show'
        else
          'No Rca List defined'
        end
      else # render JS
        step_tool.tool.name
        # render 'layouts/step_menu_tool', layout: 'processus', locals: {step: @step_tool.step, tool: @step_tool.tool}
      end
    end

    # Admin / Edit step_tool options.
    def helper_tool_options_field(step_tool, key)
      ret = ""
      if key[:values]
        # TODO : write tests !
        toptions_string = step_tool.tool_option_string(key[:keys])
        toptions_value = step_tool.tool_option_value(key[:keys])
        toptions_value = toptions_value.to_s unless key[:multiple] == true

        # puts "OPTIONS #{toptions_string}".yellow
        # puts "   VALUES = #{key[:values]}"
        # puts "   VALUE = #{toptions_value}"
        # puts "   DEFAULT = #{key[:default]}"
        # puts "   => VAL = #{toptions_value.to_s.empty? ? key[:default].to_s : toptions_value}".green

        good_value = toptions_value.to_s.empty? ? key[:default].to_s : toptions_value


        ret = select_tag "tool_options#{toptions_string}",
                   options_for_select(key[:values].map { |v| [v, v == 'true' ? true : (v == 'false' ? false : v)] },
                                      good_value),
                   # options_for_select(key[:values].each { |v| ([v, (v == 'true' ? true : v == 'false' ? false : v)]) },
                   #                    step_tool.tool_option_value(key[:keys]) || key[:default]),
                   multiple: (key[:multiple] == true),
                   class: 'form-control select optional'
      else
        ret = text_field_tag "tool_options#{step_tool.tool_option_string(key[:keys])}",
                             step_tool.tool_option_value(key[:keys]).to_s || key[:default].to_s, class: 'form-control'
      end

      return ret
    end
  end
end
