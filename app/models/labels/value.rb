require_dependency 'labels'

module Labels
  class Value < ApplicationRecord
    translates :title, fallback: :any
    translates :text, fallback: :any

    validates :title, presence: true

    belongs_to :criterium # , class_name: 'Labels::Criterium'

    def name
      return value.to_s + ' => ' + (text ? title + ' : ' + text : title)
    end
  end
end
