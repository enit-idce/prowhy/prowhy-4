class CreateMethActivities < ActiveRecord::Migration[6.0]
  def change
    create_table :meth_activities do |t|
      t.string :name
      t.integer :pos

      t.timestamps
    end
  end
end
