module Labels
  class CauseTypePolicy < AdminPolicy
    attr_reader :user, :record

    def initialize(user, record)
      @user = user
      @record = record
    end

    def cause_types_list?
      show?
    end

    def cause_types_list_set?
      update?
    end
  end
end
