require 'rails_helper'

RSpec.describe '/meth/step_tools', type: :request do
  let(:valid_attributes) { FactoryBot.attributes_for(:meth_step_tool).merge(step: step, tool: tool) }
  let(:invalid_attributes) { FactoryBot.attributes_for(:meth_step_tool).merge(name: '', step: step, tool: tool) }

  let(:user) { FactoryBot.create(:user) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:step) { FactoryBot.create(:meth_step, methodology: methodology) }
  let(:tool) { FactoryBot.create(:meth_tool) }

  before :each do
    user.set_session(:active_step_tool, methodology.steps.first.step_tools.first.id)
    user.save!
    sign_in user
  end

  # describe 'GET /index' do
  #   it 'renders a successful response' do
  #     Meth::StepTool.create! valid_attributes
  #     get meth_step_tools_url
  #     expect(response).to be_successful
  #   end
  # end

  # describe 'GET /show' do
  #   it 'renders a successful response' do
  #     meth_step_tool = Meth::StepTool.create! valid_attributes
  #     get meth_step_tool_url(meth_step_tool)
  #     expect(response).to be_successful
  #   end
  # end

  # describe 'GET /new' do
  #   it 'renders a successful response' do
  #     get new_meth_step_tool_url
  #     expect(response).to be_successful
  #   end
  # end

  describe 'GET /edit' do
    it 'render a successful response' do
      meth_step_tool = Meth::StepTool.create! valid_attributes
      get edit_meth_step_tool_url(meth_step_tool), xhr: true
      expect(response).to be_successful
    end
  end

  # describe 'POST /create' do
  #   context 'with valid parameters' do
  #     it 'creates a new Meth::StepTool' do
  #       expect {
  #         post meth_step_tools_url, params: { meth_step_tool: valid_attributes }
  #       }.to change(Meth::StepTool, :count).by(1)
  #     end

  #     it 'redirects to the created meth_step_tool' do
  #       post meth_step_tools_url, params: { meth_step_tool: valid_attributes }
  #       expect(response).to redirect_to(meth_step_tool_url(@meth_step_tool))
  #     end
  #   end

  #   context 'with invalid parameters' do
  #     it 'does not create a new Meth::StepTool' do
  #       expect {
  #         post meth_step_tools_url, params: { meth_step_tool: invalid_attributes }
  #       }.to change(Meth::StepTool, :count).by(0)
  #     end

  #     it 'renders a successful response (i.e. to display the new template)' do
  #       post meth_step_tools_url, params: { meth_step_tool: invalid_attributes }
  #       expect(response).to be_successful
  #     end
  #   end
  # end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      let(:new_attributes) { {name: 'Step Tool nouveau nom'} }

      it 'updates the requested meth_step_tool' do
        meth_step_tool = Meth::StepTool.create! valid_attributes
        patch meth_step_tool_url(meth_step_tool), params: { meth_step_tool: new_attributes }
        meth_step_tool.reload
        expect(meth_step_tool.name).to eq 'Step Tool nouveau nom'
      end

      it 'redirects to the meth_step_tool' do
        meth_step_tool = Meth::StepTool.create! valid_attributes
        patch meth_step_tool_url(meth_step_tool), params: { meth_step_tool: new_attributes }
        meth_step_tool.reload

        expect(response).to redirect_to(edit_meth_methodology_path(id: meth_step_tool.step.methodology, step: meth_step_tool.step))
      end
    end

    context 'with invalid parameters' do
      let(:new_invalid_attributes) { {name: ''} }

      it 'renders a successful response (i.e. to display the edit template)' do
        meth_step_tool = Meth::StepTool.create! valid_attributes
        patch meth_step_tool_url(meth_step_tool), params: { meth_step_tool: new_invalid_attributes }

        expect(meth_step_tool.name).to_not eq ''
        expect(response).to redirect_to(edit_meth_methodology_path(id: meth_step_tool.step.methodology, step: meth_step_tool.step))
        expect(flash[:error]).to include('Update Failed')
      end
    end
  end

  # describe 'DELETE /destroy' do
  #   it 'destroys the requested meth_step_tool' do
  #     meth_step_tool = Meth::StepTool.create! valid_attributes
  #     expect {
  #       delete meth_step_tool_url(meth_step_tool)
  #     }.to change(Meth::StepTool, :count).by(-1)
  #   end

  #   it 'redirects to the meth_step_tools list' do
  #     meth_step_tool = Meth::StepTool.create! valid_attributes
  #     delete meth_step_tool_url(meth_step_tool)
  #     expect(response).to redirect_to(meth_step_tools_url)
  #   end
  # end

  describe 'GET /step_tool_show' do
    let(:rca) { FactoryBot.create(:rca, methodology: methodology) }
    it 'render a successful response' do
      meth_step_tool = Meth::StepTool.create! valid_attributes
      get meth_step_tool_show_path(id: meth_step_tool.id), params: {rca: rca.id}
      expect(response).to be_successful # have_http_status(:redirect) #
    end
  end
end
