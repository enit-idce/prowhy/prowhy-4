module Tools
  # Task queries
  class TaskQuery < BaseQuery
    def self.relation(base_relation = nil)
      super(base_relation, Tools::Cause)
    end

    # Get subtree organized as array
    def tasks_subtree(rca_id, tool_reference, tool_num, task_detail_type)
      if rca_id == 0
        Tools::Task.arrange_as_array
      else
        Tools::Task.arrange_as_array(rca_id: rca_id, tool_reference: tool_reference, tool_num: tool_num, task_detail_type: task_detail_type)
      end
    end

    def tasks_subtree_by_task_infos(task)
      if task.rca_id == 0
        Tools::Task.arrange_as_array
      else
        Tools::Task.arrange_as_array(rca_id: task.rca_id, tool_reference: task.tool_reference, tool_num: task.tool_num, task_detail_type: task.task_detail_type)
      end
    end

    def tasks_head(rca_id, tool_reference, tool_num, task_detail_type)
      Tools::Task.where(ancestry: nil, rca_id: rca_id, tool_reference: tool_reference, tool_num: tool_num, task_detail_type: task_detail_type)
    end

    # Get Tasks for Tasks Plans

    # Rca Tasks organized by matrix statuses or by column
    def rca_tasks_by_matrix_status(rca_id, task_options)
      tt = Tools::Task.includes(:matrix_stat).where(rca_id: rca_id).where(task_options).order('tools_matrix_stats.matrix_status')
      # tt = Tools::Task.includes(:matrix_stat).where(rca_id: rca_id).where(task_options).order(:ancestry)

      # tt = Tools::Task.includes(:matrix_stat).where(rca_id: rca_id).where(task_options).order('tools_matrix_stats.matrix_status').sort_by_ancestry(Tools::Task.all)

      # tt.each do |task|
      #   puts("#{task.name} => #{task.ancestry}")
      # end

      # binding.pry



      return tt
    end

    def rca_tasks_by_column(rca_id, task_options, column)
      Tools::Task.where(rca_id: rca_id).where(task_options).order(column)
    end

    # Cause Tasks
    def cause_tasks_by_matrix_status(cause_id, task_options)
      Tools::Task.includes(:matrix_stat).where(tools_cause_id: cause_id).where(task_options).order('tools_matrix_stats.matrix_status')
    end

    def cause_tasks_by_column(cause_id, task_options, column)
      Tools::Task.where(tools_cause_id: cause_id).where(task_options).order(column)
    end

    # User Tasks
    # def rca_tasks_by_user(rca_id, task_options, user, column)
    #   Tools::Task.my_tasks(user).where(rca_id: rca_id).where(task_options).order(column)
    # end

    def tasks_by_user(task_options, user, column)
      Tools::Task.my_tasks(user).includes(rca: :methodology).where(rcas: {status: 1}).where(task_options).order(column)
    end

    def tasks_by_user_order_rca(task_options, user, column)
      Tools::Task.my_tasks(user).includes(rca: :methodology).where(rcas: {status: 1}).where(task_options).order(:rca_id).order(column)
    end

    # Global Tasks
    # def rca_tasks_global(rca_id, task_options, column)
    #   Tools::Task.where(rca_id: rca_id).where(task_options).order(column)
    # end

    def tasks_global(rcas_ids, task_options, column)
      Tools::Task.includes(rca: :methodology).where(rca_id: rcas_ids).where(task_options).order(column)
    end

    def tasks_global_order_rca(rcas_ids, task_options, column)
      Tools::Task.includes(rca: :methodology).where(rca_id: rcas_ids).where(task_options).order(:rca_id).order(column)
    end
  end
end
