module Tools
  class DecisionMatPolicy < ToolsPolicy
    attr_reader :user, :record, :rca

    def index?
      show?
    end

    def save_values?
      update?
    end

    def delete_entry?
      update?
    end

    def add_entry?
      update?
    end

    def valid_entries?
      update?
    end

    def add_criteria?
      update?
    end

    def valid_criteria?
      update?
    end
  end
end
