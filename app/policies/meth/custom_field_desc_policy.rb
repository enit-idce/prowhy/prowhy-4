module Meth
  class CustomFieldDescPolicy < Meth::MethPolicy
    attr_reader :user, :record

    def initialize(user, record)
      @user = user
      @record = record
    end

    # def index?
    #   show?
    # end

    # def show?
    #   user.has_role?(:admin) || user.has_role?(:super_admin)
    # end

    def create?
      user.has_role?(:admin) || user.has_role?(:super_admin)
    end

    def delete_all_list?
      destroy?
    end

    # def new?
    #   create?
    # end

    # def update?
    #   create?
    # end

    # def edit?
    #   create?
    # end

    # def destroy?
    #   create?
    # end
  end
end
