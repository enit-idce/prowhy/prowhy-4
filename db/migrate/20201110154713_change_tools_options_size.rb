class ChangeToolsOptionsSize < ActiveRecord::Migration[6.0]
  def change
    change_column :meth_tools, :tool_options, :text
    change_column :meth_step_tools, :tool_options, :text
  end
end
