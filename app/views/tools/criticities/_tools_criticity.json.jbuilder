json.extract! tools_criticity, :id, :rca_id, :labels_config_id, :value, :created_at, :updated_at
json.url tools_criticity_url(tools_criticity, format: :json)
