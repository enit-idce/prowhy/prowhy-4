require 'rails_helper'

RSpec.describe '/api/v1/rcas', type: :request do
  # let(:email) { user.email }
  # let(:password) { user.password }
  let(:user) { FactoryBot.create(:user) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:profil) { FactoryBot.create(:profil) }
  let(:referent) { FactoryBot.create(:person) }

  context 'with user logged' do
    before :each do
      sign_in user
      @rca1 = FactoryBot.create(:rca, methodology_id: methodology.id)
      @rca2 = FactoryBot.create(:rca, methodology_id: methodology.id, title: 'le deuxième RCA')
    end

    it 'returns rcas list' do
      get '/api/v1/rcas'

      expect(response.status).to eq 200

      json = JSON.parse(response.body)
      expect(json.size).to eq 2
    end

    it 'returns one rca' do
      get "/api/v1/rcas/#{@rca2.id}"

      expect(response.status).to eq 200

      json = JSON.parse(response.body)
      expect(json['title']).to eq 'le deuxième RCA'
    end

    context 'with valid parameters' do
      it 'creates a rca' do
        expect {
          post '/api/v1/rcas', params: {title: 'Rca created by API', methodology_id: methodology.id}
        }.to change(Rca, :count).by(1)
      end

      it 'returns 201 rca created' do
        post '/api/v1/rcas', params: {title: 'Rca created by API', methodology_id: methodology.id}
        expect(response.status).to eq 201
      end

      it 'updates a rca' do
        patch "/api/v1/rcas/#{@rca2.id}", params: {title: 'New title !'}
        expect(@rca2.reload.title).to eq 'New title !'
      end

      it 'returns 200 rca updated' do
        patch "/api/v1/rcas/#{@rca2.id}", params: {title: 'New title !'}
        expect(response.status).to eq 200
      end

      it 'destroy a rca' do
        expect {
          delete "/api/v1/rcas/#{@rca2.id}"
        }.to change(Rca, :count).by(-1)
      end
      it 'returns 200 rca destroyed' do
        delete "/api/v1/rcas/#{@rca2.id}"
        expect(response.status).to eq 200
      end

      it 'creates, updates, and delete a rca from API' do
        post '/api/v1/rcas', params: {title: 'Rca created by API', methodology_id: methodology.id}

        rca = Rca.where(title: 'Rca created by API')&.first
        expect(rca.title).to eq 'Rca created by API'

        # Change title
        patch "/api/v1/rcas/#{rca.id}", params: {title: 'New title !'}
        expect(rca.reload.title).to eq 'New title !'

        # Add referent
        patch "/api/v1/rcas/#{rca.id}", params: {referent: {profil_id: profil.id, person_id: referent.id}}
        expect(rca.get_person_from_team(profil.id)&.person).to eq referent

        # Add custom field
        # TODO: create custom field_desc and type and update DB and add one custom field in API

        delete "/api/v1/rcas/#{rca.id}"
        rca = Rca.where(title: 'Rca created by API')&.first

        expect(rca).to eq nil
      end
    end

    context 'with invalid parameters' do
      it 'do not creates a rca' do
        expect {
          post '/api/v1/rcas', params: {title: 'Rca created by API'}
        }.to change(Rca, :count).by(0)
      end

      it 'returns 400 params is missing' do
        post '/api/v1/rcas', params: {title: 'Rca created by API'}
        expect(response.body).to include('methodology_id')
        expect(response.body).to include('is missing')
        expect(response.status).to eq 400
      end

      it 'do not updates a rca' do
        patch "/api/v1/rcas/#{@rca2.id}", params: {title: nil}
        expect(@rca2.reload.methodology_id).to eq methodology.id
      end
      it 'returns 400 invalid params' do
        patch "/api/v1/rcas/#{@rca2.id}", params: {title: nil}
        expect(response.body).to include("Title can't be blank")
        expect(response.status).to eq 400
      end
    end

    context 'with invalid rca id' do
      it 'do not destroy a rca' do
        expect {
          patch '/api/v1/rcas/2000', params: {title: 'toto'}
        }.to change(Rca, :count).by(0)
      end
      it 'return 404 Record not found' do
        patch '/api/v1/rcas/2000', params: {title: 'toto'}
        expect(response.status).to eq 404
      end

      it 'do not destroy a rca' do
        expect {
          delete '/api/v1/rcas/2000'
        }.to change(Rca, :count).by(0)
      end
      it 'returns 404 Record not found' do
        delete "/api/v1/rcas/#{@rca2.id}"
        expect(response.status).to eq 200
      end
    end
  end

  context 'without user logged' do
    it 'returns 401 Unauthorized' do
      get '/api/v1/rcas'

      expect(response.status).to eq 401
      expect(response.body).to include('The access token is invalid')
    end
  end
end
