class DatasAddExternalRole < ActiveRecord::Migration[6.0]
  class MigrationRole < ApplicationRecord
    self.table_name = :roles
  end

  def up
    external_role = MigrationRole.where(id: 7) # name: 'external')
    if external_role.empty?
      say 'Create role external.'
      MigrationRole.create!(id: 7, name: 'external')
    else
      say 'Role external_role id 7 exists in database.'
    end
  end

  def down
    say 'Destroy role external.'
    external_role = MigrationRole.where(id: 7)&.first
    external_role&.destroy
  rescue StandardError
    say 'Can not destroy role.'
    raise ActiveRecord::IrreversibleMigration
  end
end
