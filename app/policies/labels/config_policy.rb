module Labels
  class ConfigPolicy < AdminPolicy
    attr_reader :user, :record

    def initialize(user, record)
      @user = user
      @record = record
    end

    # def update?
    #   # raise Pundit::NotAuthorizedError, reason: 'labels.config.config_used' if record.tools_criticities
    #   return false if record.tools_criticities

    #   super
    # end

    def add_criteria?
      edit?
    end

    def add_criteria_valid?
      update?
    end

    def formula_test?
      edit?
    end
  end
end
