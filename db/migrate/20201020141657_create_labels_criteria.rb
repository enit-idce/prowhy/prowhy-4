class CreateLabelsCriteria < ActiveRecord::Migration[6.0]
  def change
    create_table :labels_criteria do |t|
      t.integer :crit_type, default: 0
      t.string :internal_name, default: nil
      t.string :name
      t.integer :threshold, default: 0

      t.timestamps
    end
  end
end
