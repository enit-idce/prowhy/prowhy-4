module Tools
  module IshikawasHelper
    def helper_ishikawa_label(tools_ishikawa, ishikawas, ishikawa_names)
      ishikawas.each_with_index do |ishi, index|
        next unless ishi.id == tools_ishikawa.id

        return ishikawa_names[index]
      end

      'Ishikawa -'
    end
  end
end
