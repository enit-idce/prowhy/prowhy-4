require 'rails_helper'

RSpec.describe Tools::DecisionMatsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/tools/decision_mats').to route_to('tools/decision_mats#index')
    end

    it 'routes to #new' do
      expect(get: '/tools/decision_mats/new').to route_to('tools/decision_mats#new')
    end

    it 'routes to #show' do
      expect(get: '/tools/decision_mats/1').to route_to('tools/decision_mats#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/tools/decision_mats/1/edit').to route_to('tools/decision_mats#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/tools/decision_mats').to route_to('tools/decision_mats#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/tools/decision_mats/1').to route_to('tools/decision_mats#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/tools/decision_mats/1').to route_to('tools/decision_mats#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/tools/decision_mats/1').to route_to('tools/decision_mats#destroy', id: '1')
    end
  end
end
