class CreateMethTreeContents < ActiveRecord::Migration[6.0]
  def change
    create_table :meth_tree_contents do |t|
      t.references :custom_field_desc, null: false, foreign_key: { to_table: :meth_custom_field_descs }
      t.string :name_fr
      t.string :name_en
      t.string :ancestry

      t.timestamps
    end
    add_index :meth_tree_contents, :ancestry
  end
end
