require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to test the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator. If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails. There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.

RSpec.describe '/admin/perimeters', type: :request do
  # Admin::Perimeter. As you add validations to Admin::Perimeter, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) { FactoryBot.attributes_for(:perimeter) }
  let(:invalid_attributes) { FactoryBot.attributes_for(:perimeter, name: nil) }

  let(:user) { FactoryBot.create(:user) }
  before :each do
    sign_in user
  end

  describe 'GET /index' do
    it 'renders a successful response' do
      Admin::Perimeter.create! valid_attributes
      get admin_perimeters_url
      expect(response).to be_successful
    end
  end

  # describe 'GET /show' do
  #   it 'renders a successful response' do
  #     admin_perimeter = Admin::Perimeter.create! valid_attributes
  #     get admin_perimeter_url(admin_perimeter)
  #     expect(response).to be_successful
  #   end
  # end

  describe 'GET /new' do
    context 'without admin_perimeter params' do
      it 'renders a successful response' do
        get new_admin_perimeter_url, params: {}, xhr: true
        expect(response).to be_successful
      end
    end

    context 'with admin_perimeter params' do
      it 'renders a successful response' do
        get new_admin_perimeter_url, params: {admin_perimeter: {ancestry: nil}}, xhr: true
        expect(response).to be_successful
      end
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      admin_perimeter = Admin::Perimeter.create! valid_attributes
      get edit_admin_perimeter_url(admin_perimeter), params: {admin_perimeter: {}}, xhr: true
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Admin::Perimeter' do
        expect {
          post admin_perimeters_url, params: { admin_perimeter: valid_attributes }
        }.to change(Admin::Perimeter, :count).by(1)
      end

      it 'redirects to the created admin_perimeter' do
        post admin_perimeters_url, params: { admin_perimeter: valid_attributes }
        expect(response).to redirect_to(admin_perimeters_url)
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new Admin::Perimeter' do
        expect {
          post admin_perimeters_url, params: { admin_perimeter: invalid_attributes }
        }.to change(Admin::Perimeter, :count).by(0)
      end

      it 'renders a successful response (i.e. to display the new template)' do
        post admin_perimeters_url, params: { admin_perimeter: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      let(:new_attributes) { {name: 'new name'} }

      it 'updates the requested admin_perimeter' do
        admin_perimeter = Admin::Perimeter.create! valid_attributes
        patch admin_perimeter_url(admin_perimeter), params: { admin_perimeter: new_attributes }
        admin_perimeter.reload

        expect(admin_perimeter.name).to eq 'new name'
      end

      it 'redirects to the admin_perimeter' do
        admin_perimeter = Admin::Perimeter.create! valid_attributes
        patch admin_perimeter_url(admin_perimeter), params: { admin_perimeter: new_attributes }
        admin_perimeter.reload
        expect(response).to redirect_to(admin_perimeters_url)
      end
    end

    context 'with invalid parameters' do
      it 'renders a successful response (i.e. to display the edit template)' do
        admin_perimeter = Admin::Perimeter.create! valid_attributes
        patch admin_perimeter_url(admin_perimeter), params: { admin_perimeter: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested admin_perimeter' do
      admin_perimeter = Admin::Perimeter.create! valid_attributes
      expect {
        delete admin_perimeter_url(admin_perimeter)
      }.to change(Admin::Perimeter, :count).by(-1)
    end

    it 'redirects to the admin_perimeters list' do
      admin_perimeter = Admin::Perimeter.create! valid_attributes
      delete admin_perimeter_url(admin_perimeter)
      expect(response).to redirect_to(admin_perimeters_url)
    end
  end
end
