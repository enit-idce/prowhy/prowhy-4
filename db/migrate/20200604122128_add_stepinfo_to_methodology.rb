class AddStepinfoToMethodology < ActiveRecord::Migration[6.0]
  def change
    add_reference :meth_methodologies, :step_info, foreign_key: { to_table: :meth_steps }, after: :name
  end
end
