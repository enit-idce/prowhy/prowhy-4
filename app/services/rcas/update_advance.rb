module Rcas
  class UpdateAdvance
    attr_accessor :rca

    def initialize(rca)
      @rca = rca
    end

    def call
      adv = 0
      adv_step = nil
      rca.methodology.steps_menu.each_with_index do |step, _index|
        break unless rca.step_finished?(step)

        adv_step = step&.id # step.step_role&.id || index
        adv = step&.step_role&.advance_level
      end

      rca.advance_step_id = nil
      rca.advance_step_id = adv_step if adv_step
      rca.advance = adv
      rca.save!
    end

    # private

    # def all_team_actions
    #   return @obj.tools_teams.includes(:person).joins(:profil).where(profils: { profil_name: 'resp_act'})
    # end
  end
end
