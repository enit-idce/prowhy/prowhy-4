class CreateToolsMatrixStats < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_matrix_stats do |t|
      t.references :element, polymorphic: true, null: false
      t.integer :matrix_status, default: 0

      t.timestamps
    end

    remove_column :tools_tasks, :matrix_status, :integer, default: 0, after: :date_completion
    remove_column :tools_causes, :matrix_status, :integer, default: 0, after: :status
  end
end
