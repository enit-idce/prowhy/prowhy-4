module PickStarsHelper
  def star_icon(val, percentage, css_class = '', css_styles = '')
    return fa_icon('star', type: (percentage && percentage < val ? :regular : :solid), class: css_class, style: css_styles)
  end

  # Helper pour afficher un sélecteur d'étoiles.
  #   element = l'objet
  #   values = tableau de valeurs : 1 valeur = 1 étoile. exemple : [20, 40, 60, 80, 100]
  #   options = options pour l'affichage (css_class et css_style) : passé à fa_icon
  # Nécessaire dans le contrôleur :
  #   dans update / render format JS (voir tools/efficacities_controller update)
  #   si params[:from_stars] => render update stars js (voir tools/efficacity/update_percentage.js.erb)
  def pick_stars_helper(element, model_name, col_name, values, options = {})
    return '-' unless element

    options[:css_class] ||= 'icon-status-validated'
    options[:css_styles] ||= 'font-size: 18px; width:16px !important;'

    col_value = element.send(col_name)

    ret = "<div id='#{model_name}-#{col_name}-#{element.id}'>"
    values.each do |val|
      ret += link_to star_icon(val, col_value, options[:css_class], options[:css_styles]),
                     public_send("#{model_name}_path", element, model_name => {col_name => val}, from_stars: true), method: :patch, remote: true
    end
    ret += '</div>'

    return ret.html_safe
  end

  def show_stars_helper(element, model_name, col_name, values, options = {})
    return '-' unless element

    options[:css_class] ||= 'icon-status-validated'
    options[:css_styles] ||= '' # font-size: 18px; width:16px !important;'

    col_value = element.send(col_name)

    ret = "<div id='#{model_name}-#{col_name}-#{element.id}'>"
    values.each do |val|
      ret += star_icon(val, col_value, options[:css_class], options[:css_styles])
    end
    ret += '</div>'

    return ret.html_safe
  end
end
