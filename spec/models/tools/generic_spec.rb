require 'rails_helper'

RSpec.describe Tools::Generic, type: :model do
  subject(:generic) do
    Tools::Generic.create!(rca: rca, tool_reference: 1, tool_num: 1, doc: doc_ok)
  end

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
  let(:doc_ok) do
    ActiveStorage::Blob.create_and_upload!(
      io: File.open(file_ok, 'rb'),
      filename: 'not_image.pdf',
      content_type: 'file/pdf'
    ).signed_id
  end
  let(:doc_ok2) do
    ActiveStorage::Blob.create_and_upload!(
      io: File.open(file_ok, 'rb'),
      filename: 'not_image.pdf',
      content_type: 'file/pdf'
    ).signed_id
  end

  let(:file_ok) { Rails.root.join('spec', 'support', 'assets', 'images', 'not_image.pdf') }

  # Tests has_many :tools + dependant: destroy
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to rca' do
        assc = Tools::Generic.reflect_on_association(:rca)
        expect(assc.macro).to eq :belongs_to
      end

      it 'has many file_versions' do
        assc = Tools::Generic.reflect_on_association(:file_versions)
        expect(assc.macro).to eq :has_many
      end
    end
  end

  describe 'Create and search model : tool class functions' do
    before :each do
      generic.save!
      options = { rca: rca, tool_reference: 2, tool_num: 1 }
      @generic_created = Tools::Generic.create_default!(options, nil)
    end

    it 'create_default without attached doc' do
      expect(@generic_created.tool_reference).to eq 2
      expect(@generic_created.rca).to eq rca
      expect(@generic_created.doc.attached?).to be false
    end

    it 'create_default with attached doc' do
      step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1,
                                     generic_file: doc_ok2)
      options = { rca: rca, tool_reference: 3, tool_num: 1 }
      @generic_created = Tools::Generic.create_default!(options, step_tool)
      expect(@generic_created.tool_reference).to eq 3
      expect(@generic_created.rca).to eq rca
      expect(@generic_created.doc.attached?).to be true
      expect(@generic_created.doc.blob).to eq step_tool.generic_file.blob
    end

    it 'search_default' do
      step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1)
      generic_founded = Tools::Generic.search_default(rca, 'tools_generics', step_tool)

      expect(generic_founded.tool_reference).to eq 1
      expect(generic_founded).to eq generic

      step_tool.tool_reference = 2
      generic_founded = Tools::Generic.search_default(rca, 'tools_generics', step_tool)

      expect(generic_founded.tool_reference).to eq 2
      expect(generic_founded).to eq @generic_created
    end
  end
end
