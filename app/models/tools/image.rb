require_dependency 'tools'
require_dependency 'admin/admin_config'

module Tools
  class Image < ApplicationRecord
    include ToolConcern

    # attr_reader :image

    has_one_attached :file
    validates :name, presence: true
    validate :acceptable_image

    def self.create_default!(_options, _step_tool)
      []
    end

    def self.search_default(rca, _tool_name, step_tool)
      step_tool.tool_options ||= {}

      options_for_search = if step_tool.tool_reference != 0
                             step_tool.tool_options.merge(tool_reference: step_tool.tool_reference,
                                                          tool_num: step_tool.tool_num)
                           else
                             step_tool.tool_options
                           end
      # .merge(tool_reference: step_tool.tool_reference,
      # tool_num: step_tool.tool_num)
      # binding.pry
      return rca.tools_images.where(options_for_search).order(:tool_reference)
    end

    def self.create_search_multiple?
      return true
    end

    private

    def acceptable_image
      config = Admin::AdminConfig.first
      max_size = config&.options&.[]('image_max_size') || 1
      errors.add(:file, 'must be present') unless file.attached?
      return unless file.attached?

      errors.add(:file, "is too big : #{file.byte_size/1_000_000.0} Mo (max = #{max_size.to_f} Mo)") unless file.byte_size <= max_size.to_f.megabyte

      acceptable_types = ['image/jpeg', 'image/png']
      errors.add(:file, 'File must be a JPEG or PNG image format') unless acceptable_types.include?(file.content_type)
    end
  end
end
