module Labels
  class QqoqcpLinesController < ApplicationController
    include AdminConcern
    before_action :set_labels_qqoqcp_line, only: [:show, :edit, :update, :destroy]

    # GET /labels/qqoqcp_lines
    # GET /labels/qqoqcp_lines.json
    def index
      authorize QqoqcpLine
      @labels_qqoqcp_lines = Labels::QqoqcpLine.all
    end

    # GET /labels/qqoqcp_lines/1
    # GET /labels/qqoqcp_lines/1.json
    # def show
    # end

    # GET /labels/qqoqcp_lines/new
    def new
      @labels_qqoqcp_line = Labels::QqoqcpLine.new
      authorize @labels_qqoqcp_line
    end

    # GET /labels/qqoqcp_lines/1/edit
    def edit
    end

    # POST /labels/qqoqcp_lines
    # POST /labels/qqoqcp_lines.json
    def create
      @labels_qqoqcp_line = Labels::QqoqcpLine.new(labels_qqoqcp_line_params)
      authorize @labels_qqoqcp_line

      respond_to do |format|
        if @labels_qqoqcp_line.save
          format.html { redirect_to labels_qqoqcp_lines_url, notice: 'Qqoqcp line was successfully created.' }
          format.json { render :show, status: :created, location: @labels_qqoqcp_line }
        else
          format.html { render :new }
          format.json { render json: @labels_qqoqcp_line.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /labels/qqoqcp_lines/1
    # PATCH/PUT /labels/qqoqcp_lines/1.json
    def update
      respond_to do |format|
        if @labels_qqoqcp_line.update(labels_qqoqcp_line_params)
          format.html { redirect_to labels_qqoqcp_lines_url, notice: 'Qqoqcp line was successfully updated.' }
          format.json { render :show, status: :ok, location: @labels_qqoqcp_line }
        else
          format.html { render :edit }
          format.json { render json: @labels_qqoqcp_line.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /labels/qqoqcp_lines/1
    # DELETE /labels/qqoqcp_lines/1.json
    def destroy
      @labels_qqoqcp_line.destroy
      respond_to do |format|
        format.html { redirect_to labels_qqoqcp_lines_url, notice: 'Qqoqcp line was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_labels_qqoqcp_line
      @labels_qqoqcp_line = Labels::QqoqcpLine.find(params[:id])
      authorize @labels_qqoqcp_line
    end

    # Only allow a list of trusted parameters through.
    def labels_qqoqcp_line_params
      params.require(:labels_qqoqcp_line).permit(:name, :name_fr, :name_en, :internal_name)
    end
  end
end
