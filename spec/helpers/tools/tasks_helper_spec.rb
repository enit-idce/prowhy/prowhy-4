require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the Tools::TasksHelper. For example:
#
# describe Tools::TasksHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe Tools::TasksHelper, type: :helper do
  let(:task) do
    FactoryBot.create(:task, id: 1, rca: rca, name: 'Task 1', tools_cause: cause,
                             responsible_action: person, responsible_rca: person,
                             advancement: 0)
  end
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
  let(:cause) { Tools::Cause.create!(rca: rca, tool_reference: 1, name: 'Cause 1') }
  let(:person) { FactoryBot.create(:person) }
  let(:user) { FactoryBot.create(:user, person_id: person.id) }

  before :each do
    user.usersession[:active_step_tool] = methodology.steps.first.step_tools.first.id
    user.save!
    sign_in user
  end

  describe 'advancement helpers' do
    it 'returns the text for advancement' do
      helper_content = helper.task_link_next_advancement(task)
      expect(helper_content).to include('Valid step 1')

      helper_content = helper.task_link_prev_advancement(task)
      expect(helper_content).to eq ''

      task.advancement = 1
      helper_content = helper.task_link_next_advancement(task)
      expect(helper_content).to include('Valid step 2')

      helper_content = helper.task_link_prev_advancement(task)
      expect(helper_content).to include('step 0')

      task.advancement = 2
      helper_content = helper.task_link_next_advancement(task)
      expect(helper_content).to include('Valid step 3')

      helper_content = helper.task_link_prev_advancement(task)
      expect(helper_content).to include('step 1')

      task.advancement = 3
      helper_content = helper.task_link_next_advancement(task)
      expect(helper_content).to include('Valid step 4')

      helper_content = helper.task_link_prev_advancement(task)
      expect(helper_content).to include('step 2')

      task.advancement = 4
      helper_content = helper.task_link_next_advancement(task)
      expect(helper_content).to eq ''

      helper_content = helper.task_link_prev_advancement(task)
      expect(helper_content).to include('step 3')
    end

    it 'returns the circle for advancement' do
      task.advancement = 2
      helper_content = helper.task_advance_helper(task)

      expect(helper_content).to include('jquery-circle-p')
      expect(helper_content).to include("data-value='0.5'")
    end

    it 'returns the form for advancement' do
      # include Pundit

      task.advancement = 2
      helper_content = helper.task_advance_helper_form(task)

      expect(helper_content).to include(task_link_prev_advancement(task))
      expect(helper_content).to include(task_link_next_advancement(task))
    end
  end

  describe 'dates helpers' do
    it 'return field for date completion if task is finished' do
      task.advancement = 2
      helper_content = helper.task_date_completion_helper_form(task)
      expect(helper_content).to eq ''

      task.advancement = 4
      helper_content = helper.task_date_completion_helper_form(task)
      expect(helper_content).to include('input')
      expect(helper_content).to include('date')
    end

    it 'return date due in orangered if task is late' do
      task.advancement = 2
      task.date_due = Date.tomorrow
      helper_content = helper.task_date_due_helper(task)
      expect(helper_content).to eq task.date_due

      task.date_due = Date.yesterday
      helper_content = helper.task_date_due_helper(task)
      expect(helper_content).to include('orangered')
    end

    describe 'shows reschedule button if task is created and there is a date due' do
      it 'do not show button if task is not created' do
        task2 = Tools::Task.new(rca: rca, name: 'Task 2', tools_cause: cause)
        expect(reschedule_button(task2)).to eq ''
      end

      it 'do not show button if task is created but with no date due' do
        expect(reschedule_button(task)).to eq ''
      end

      it 'show button if task is created with a date due' do
        task.date_due = Date.tomorrow
        expect(reschedule_button(task)).to include tools_tasks_reschedule_path(id: task.id)
      end
    end
  end

  describe 'status matrix' do
    it 'returns nothing if there is no matrix' do
      helper_content = helper.task_status_matrix(task)
      expect(helper_content).to eq '-'
    end

    it 'returns matrix status if matrix linked' do
      # helper_content = helper.task_status_matrix(task)
      # expect(helper_content).to eq '-'
    end
  end
  # Specific columns : TaskValidation

  describe 'TaskValidation helpers' do
    let(:task_val) do
      Tools::Task.create!(rca: rca, name: 'Task Validation', task_detail: task_validation,
                          tools_cause: cause,
                          responsible_action: person, responsible_rca: person,
                          advancement: 0)
    end

    let(:task_validation) { Tools::TaskValidation.create! }

    it 'returns task validation status' do
      helper_content = helper.task_validation_status(task_val)
      expect(helper_content).to include 'icon-status-inprogress'

      task_validation.status_validated!
      helper_content = helper.task_validation_status(task_val)
      expect(helper_content).to include 'icon-status-validated'

      task_validation.status_invalidated!
      helper_content = helper.task_validation_status(task_val)
      expect(helper_content).to include 'icon-status-invalidated'
    end
  end

  describe 'Task Efficacity helpers' do
    before :each do
      task.build_efficacity
      task.save!
    end
    it 'returns efficacity status' do
      helper_content = helper.efficacity_status(task)
      expect(helper_content).to include 'icon-status-inprogress'

      task.efficacity.check_status_validated!
      helper_content = helper.efficacity_status(task)
      expect(helper_content).to include 'icon-status-validated'

      task.efficacity.check_status_invalidated!
      helper_content = helper.efficacity_status(task)
      expect(helper_content).to include 'icon-status-invalidated'
    end
    it 'returns efficacity date due' do
      # when efficacity is not late
      helper_content = helper.efficacity_date_due(task)
      expect(helper_content).to eq nil

      task.efficacity.date_due_check = Date.tomorrow
      task.efficacity.save!
      helper_content = helper.efficacity_date_due(task)
      expect(helper_content).to eq Date.tomorrow

      # when efficacity is late
      task.efficacity.date_due_check = Date.today
      task.efficacity.save!
      helper_content = helper.efficacity_date_due(task)
      expect(helper_content).to include 'color:orangered'
    end
    it 'returns efficacity date done' do
      helper_content = helper.efficacity_date_done(task)
      expect(helper_content).to eq nil

      task.efficacity.date_done_check = Date.today
      task.efficacity.save!
      helper_content = helper.efficacity_date_done(task)
      expect(helper_content).to eq Date.today
    end
  end
end
