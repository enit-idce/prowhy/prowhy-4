class CreateMethListContents < ActiveRecord::Migration[6.0]
  def change
    create_table :meth_list_contents do |t|
      t.references :custom_field_desc, null: false, foreign_key: { to_table: :meth_custom_field_descs }
      t.string :name
      t.integer :pos, default: 0

      t.timestamps
    end
  end
end
