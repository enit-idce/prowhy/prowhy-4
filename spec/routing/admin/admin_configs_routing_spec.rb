require 'rails_helper'

RSpec.describe Admin::AdminConfigsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/admin_configs').to route_to('admin/admin_configs#index')
    end

    it 'routes to #edit' do
      expect(get: '/admin/admin_configs/1/edit').to route_to('admin/admin_configs#edit', id: '1')
    end

    it 'routes to #update via PUT' do
      expect(put: '/admin/admin_configs/1').to route_to('admin/admin_configs#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/admin/admin_configs/1').to route_to('admin/admin_configs#update', id: '1')
    end
  end
end
