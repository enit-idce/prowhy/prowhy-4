module Labels
  class CriteriaController < ApplicationController
    include AdminConcern
    # layout 'admin'
    before_action :set_labels_criterium, only: [:show, :edit, :update, :destroy]

    # GET /labels/criteria
    # GET /labels/criteria.json
    def index
      @labels_criteria = if params[:labels_criterium]
                           Labels::Criterium.all.where(labels_criterium_params).order(:crit_type)
                         else
                           Labels::Criterium.all.order(:crit_type)
                         end
      @crit_type ||= params[:labels_criterium]&.[](:crit_type) # &.to_i
      authorize Criterium
    end

    # GET /labels/criteria/1
    # GET /labels/criteria/1.json
    # def show
    # end

    # GET /labels/criteria/new
    def new
      # params[:labels_criterium][:crit_type] = params[:labels_criterium][:crit_type].to_i if params[:labels_criterium]&.[](:crit_type)
      @labels_criterium = if params[:labels_criterium]
                            Labels::Criterium.new(labels_criterium_params)
                          else
                            Labels::Criterium.new
                          end
      @crit_type = params[:labels_criterium]&.[](:crit_type)

      authorize @labels_criterium
    end

    # GET /labels/criteria/1/edit
    def edit
    end

    # POST /labels/criteria
    # POST /labels/criteria.json
    def create
      @labels_criterium = Labels::Criterium.new(labels_criterium_params)
      @crit_type = params[:labels_criterium]&.[](:crit_type)

      authorize @labels_criterium

      respond_to do |format|
        if @labels_criterium.save
          format.html { redirect_to edit_labels_criterium_path(id: @labels_criterium), notice: t('.message_succes', critere: Labels::Criterium.model_name.human) }
          format.json { render :show, status: :created, location: @labels_criterium }
        else
          format.html { render :new }
          format.json { render json: @labels_criterium.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /labels/criteria/1
    # PATCH/PUT /labels/criteria/1.json
    def update
      respond_to do |format|
        if @labels_criterium.update(labels_criterium_params)
          format.html do
            if params[:redirect_to]
              redirect_to params[:redirect_to]
            else
              redirect_to labels_criteria_url(labels_criterium: {crit_type: @crit_type}), notice: t('.message_succes', critere: Labels::Criterium.model_name.human)
            end
          end
          format.json { render :show, status: :ok, location: @labels_criterium }
        else
          format.html { render :edit }
          format.json { render json: @labels_criterium.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /labels/criteria/1
    # DELETE /labels/criteria/1.json
    def destroy
      @labels_criterium.destroy
      respond_to do |format|
        format.html { redirect_to labels_criteria_url(labels_criterium: {crit_type: @crit_type}), notice: t('.message_succes', critere: Labels::Criterium.model_name.human) }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_labels_criterium
      @labels_criterium = Labels::Criterium.find(params[:id])
      @crit_type = @labels_criterium.crit_type
      authorize @labels_criterium
    end

    # Only allow a list of trusted parameters through.
    def labels_criterium_params
      params.require(:labels_criterium).permit(:crit_type, :name, :name_fr, :name_en, :description_fr, :description_en, :threshold,
                                               :internal_name, :config_id)
    end
  end
end
