# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

ActiveRecord::Base.establish_connection
ActiveRecord::Base.connection.execute('SET FOREIGN_KEY_CHECKS = 0')
# I18n.locale = :fr

ActiveRecord::Base.connection.execute('TRUNCATE activities')
ActiveRecord::Base.connection.execute('TRUNCATE tools')
# ActiveRecord::Base.connection.execute('TRUNCATE steps')
# ActiveRecord::Base.connection.execute('TRUNCATE methodologies')
# ActiveRecord::Base.connection.execute('TRUNCATE step_tools')

User.create!([
  {id: 1, email: "admin@prowhy.org", password: "admin++", password_confirmation: "admin++", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil}
])

Activity.create!([
                   {id: 1, name: "Actions d'urgence", pos: 1 },
                   {id: 2, name: "Actions conservatoires", pos: 2 },
                   {id: 3, name: "Actions correctives", pos: 3 },
                   {id: 4, name: "Actions préventives", pos: 4 },
                   {id: 5, name: "Graphs", pos: 5 },
                   {id: 6, name: "Equipe", pos: 6 },
                   {id: 7, name: "Description", pos: 7 },
                   {id: 8, name: "Recherche de causes", pos: 8 },
                   {id: 9, name: "Cloture", pos: 9 },
                   {id: 10, name: "Champs personnalisés", pos: 10 },
                   {id: 11, name: "Améliorations", pos: 11 },
                 ])

Tool.create!([
               {id: 1, name: "PA Urgence", activity_id: 1, tool_name: 'Action'},
               {id: 2, name: "Vérif efficacité", activity_id: 1, tool_name: 'ActionCheck'},
               {id: 3, name: "PA Conservatoires", activity_id: 2, tool_name: 'Action'},
               {id: 4, name: "Vérif efficacité", activity_id: 2, tool_name: 'ActionCheck'},
               {id: 5, name: "PA Correctives", activity_id: 3, tool_name: 'Action'},
               {id: 6, name: "Matrice décision", activity_id: 3, tool_name: 'DecisionMatrix'},
               {id: 7, name: "PA Préventives", activity_id: 4, tool_name: 'Action'},
               {id: 8, name: "Vérif efficacité", activity_id: 4, tool_name: 'ActionCheck'},
               {id: 9, name: "Vérif efficacité", activity_id: 5, tool_name: 'Graph'},
               {id: 10, name: "Equipe", activity_id: 6, tool_name: 'Team'},
               {id: 11, name: "QQOQCP", activity_id: 7, tool_name: 'Qqoqcp'},
               {id: 12, name: "EST/N'EST PAS", activity_id: 7, tool_name: 'Qoqc'},
               {id: 13, name: "Arbre des causes", activity_id: 8, tool_name: 'CauseTree'},
               {id: 14, name: "Ishikawa", activity_id: 8, tool_name: 'Ishikawa'},
               {id: 15, name: "5 Pourquoi", activity_id: 8, tool_name: 'Why'},
               {id: 16, name: "PA Validation", activity_id: 8, tool_name: 'Action'},
               {id: 17, name: "Clotûre", activity_id: 9, tool_name: 'Closure'},
               {id: 18, name: "Champs personnalisés", activity_id: 10, tool_name: 'CustomField'},
               {id: 19, name: "Tags", activity_id: 10, tool_name: 'Tags'},
               {id: 20, name: "PA Améliorations", activity_id: 11, tool_name: 'ActionImprove'},
               {id: 21, name: "Vérif efficacité", activity_id: 11, tool_name: 'ActionCheck'}
             ])

# m8d = Methodology.create!(id: 1, name: 'Méthode 8D')

# Step.create!([
#                {id: 1, name: 'D1', methodology: m8d, pos: 1},
#                {id: 2, name: 'D2', methodology: m8d, pos: 2},
#                {id: 3, name: 'D3', methodology: m8d, pos: 3},
#                {id: 4, name: 'D4', methodology: m8d, pos: 4},
#                {id: 5, name: 'D5', methodology: m8d, pos: 5},
#                {id: 6, name: 'D6', methodology: m8d, pos: 6},
#                {id: 7, name: 'D7', methodology: m8d, pos: 7},
#                {id: 8, name: 'D8', methodology: m8d, pos: 8}
#              ])

# StepTool.create!([
#                    {step_id: 1, tool_id: 10, pos: 1},
#                    {step_id: 2, tool_id: 11, pos: 1},
#                    {step_id: 2, tool_id: 12, pos: 2},
#                    {step_id: 3, tool_id: 3, pos: 1},
#                    {step_id: 4, tool_id: 13, pos: 1},
#                    {step_id: 4, tool_id: 14, pos: 2},
#                    {step_id: 4, tool_id: 15, pos: 3},
#                    {step_id: 5, tool_id: 5, pos: 1},
#                    {step_id: 5, tool_id: 6, pos: 2},
#                    {step_id: 6, tool_id: 9, pos: 1},
#                    {step_id: 7, tool_id: 7, pos: 1},
#                    {step_id: 7, tool_id: 8, pos: 2},
#                    {step_id: 8, tool_id: 17, pos: 1}
#                  ])

# Reactivation foreign_key check
ActiveRecord::Base.connection.execute('SET FOREIGN_KEY_CHECKS = 1')
