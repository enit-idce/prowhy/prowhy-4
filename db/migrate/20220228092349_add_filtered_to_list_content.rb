class AddFilteredToListContent < ActiveRecord::Migration[6.0]
  def change
    # create_table :meth_list_content_filtereds do |t|
    #   t.references :custom_field_desc, null: false, foreign_key: { to_table: :meth_custom_field_descs }
    #   t.string :name_fr
    #   t.string :name_en
    #   t.integer :pos, default: 0
    #   t.integer :ref_value

    #   t.timestamps
    # end
    add_column :meth_list_contents, :ref_value, :integer, default: 0, after: :pos

    add_reference :meth_custom_field_descs, :ref_field, foreign_key: { to_table: :meth_custom_field_descs }, after: :name_en
    add_column :meth_custom_field_descs, :ref_show_values, :json, after: :ref_field_id
  end
end
