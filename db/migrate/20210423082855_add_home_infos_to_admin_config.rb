class AddHomeInfosToAdminConfig < ActiveRecord::Migration[6.0]
  def change
    add_column :admin_admin_configs, :home_options, :json, after: :options
  end
end
