require_dependency 'admin/admin_config'

class User < ApplicationRecord
  rolify # before_add: :prepare_add_role, before_remove: :prepare_remove_role
  after_create :assign_default_role
  after_create :assign_default_perimeter

  attr_accessor :login

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  belongs_to :ldap_config, class_name: 'Admin::LdapConfig', optional: true

  # serialize :preferences => JSON
  # serialize :usersession => JSON
  # serialize :usersearch => JSON

  validates :username, presence: true, uniqueness: { case_sensitive: false }
  validates_format_of :username, with: /^[a-zA-Z0-9_.\-@]*$/, multiline: true

  belongs_to :person
  accepts_nested_attributes_for :person

  # Perimeters
  has_many :perimeter_users, class_name: 'Admin::PerimeterUser', dependent: :destroy
  has_many :perimeters, through: :perimeter_users, class_name: 'Admin::Perimeter'

  # Track
  has_many :visits, class_name: 'Ahoy::Visit', dependent: :nullify
  has_many :events, class_name: 'Ahoy::Event', dependent: :nullify

  # Pour permettre de créer un nouvel utilisateur sur une Personne existante avec les nested attributes.
  def person_attributes=(attributes)
    self.person = Person.find(attributes['id']) if attributes['id'].present?
    super
  end

  after_initialize do
    if new_record?
      # Init user preferences
      self.preferences ||= {}
      self.usersession ||= {}
      self.usersearch ||= {custom_field: {}, rca_search: {}}
    end
  end

  # Sessions
  def set_session(key, value)
    self.usersession ||= {}
    self.usersession[key.to_s] = value
    save!
  end

  def get_session(key)
    usersession ? usersession[key.to_s] : nil
  end

  # Preferences
  def set_preference(key, value)
    self.preferences ||= {}
    self.preferences[key.to_s] = value
    save!
  end

  def get_preference(key)
    preferences ? preferences[key.to_s] : nil
  end

  # PA Options
  # TODO: liste des types de tasks => dans une config.
  # TODO: tout ce qui concerne les options pa/colonees => dans un service.
  def init_pa_options
    init_columns = Tools::Task.all_tasks_columns

    self.pa_options ||= {}

    # ['TaskGlobal', 'Tools::TaskEmergency', 'Tools::TaskContainment',
    #  'Tools::TaskCorrective', 'Tools::TaskPreventive',
    #  'Tools::TaskValidation', 'Tools::TaskImprovement']
    (['TaskGlobal']+Tools::Task::TASK_TYPES).each do |pa_type|
      self.pa_options[pa_type] ||= {}
      self.pa_options[pa_type]['columns'] ||= init_columns
    end

    save!
  end

  # pa_options
  def columns_options(task_detail_type)
    pa_type = task_detail_type.class == Array ? 'TaskGlobal' : task_detail_type

    init_pa_options unless pa_options && pa_options[pa_type]

    return pa_options[pa_type] ? pa_options[pa_type]['columns'] : {} # []
  end

  def update_pa_options(params)
    init_pa_options unless pa_options && pa_options[params['pa_type']]

    if pa_options[params['pa_type']]['columns'][params['block']].include?(params['column']) && params['value'] == 'false'
      self.pa_options[params['pa_type']]['columns'][params['block']].delete(params['column'])
      save!
    elsif !pa_options[params['pa_type']]['columns'][params['block']].include?(params['column']) && params['value'] == 'true'
      self.pa_options[params['pa_type']]['columns'][params['block']] << params['column']
      save!
    end

    # binding.pry
    return true
    # rescue StandardError
    # return false
  end

  # Utilitaires
  def name
    username || email
  end

  def full_name
    (username || email) + ' : ' + person.name
  end

  # LOGIN
  # For login with email OR username
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if (login = conditions.delete(:login))
      where(conditions.to_h).where(['username = :value OR email = :value', { value: login }]).first
    elsif conditions.key?(:username) || conditions.key?(:email)
      where(conditions.to_h).first
    end
  end

  def user_password_is_valid_for_prowhy?(pwd)
    return valid_password?(pwd) unless ldap_config

    admin_config = Admin::AdminConfig.first || Admin::AdminConfig.create!
    return false unless admin_config.connexion_attribute(:ldap_login) == true

    puts 'LDAP User. Try to connect.'.yellow

    return ldap_config.connect_and_auth(username, pwd) ? true : false
  end

  # Perimeters
  def add_perimeter(perimeter_id)
    return unless perimeter_users.where(perimeter_id: perimeter_id).empty?

    Admin::PerimeterUser.create!(user: self, perimeter_id: perimeter_id)
  end

  def remove_perimeter(perimeter_id)
    perimeter_users.where(perimeter_id: perimeter_id)&.first&.destroy
  end

  # Perimeters
  def add_perimeter_and_childs(perimeter)
    add_perimeter(perimeter.id)

    perimeter.children&.each do |child|
      add_perimeter_and_childs(child)
    end
  end

  # Authorizations (profil)
  def user_authorized?(rca, type, step_in = nil)
    step = step_in || Meth::StepTool.find(get_session(:active_step_tool))&.step

    return false unless step && rca

    ret = false
    teams_profils = rca.tools_teams.where(person_id: person_id)
    profil_authorizes = ProfilAuthorize.includes(:meth_step_role, :profil)
                                       .where(meth_step_role_id: step.step_role_id, profil_id: teams_profils.map(&:profil_id))

    profil_authorizes.each do |profil_auth|
      ret = profil_auth.send(type)
      # puts "AUthorization NOT OK for #{profil_auth.profil.name} / #{profil_auth.meth_step_role.name} / #{type}".red unless ret
      # puts "AUthorization OK for #{profil_auth.profil.name} / #{profil_auth.meth_step_role.name} / #{type}".green if ret

      break if ret
    end

    # binding.pry

    ret
  end

  # API Authentication
  # the authenticate method from devise documentation
  # def self.authenticate(username, password)
  #   user = User.find_for_authentication(username: username)
  #   user&.valid_password?(password) ? user : nil
  # end

  private

  # Set default Role (called by after_create)
  def assign_default_role
    return unless roles.blank?

    role = Admin::AdminConfig.first&.default_user_role
    add_role(role.name) if role
  end

  # Set default Perimeter (called by after_create)
  def assign_default_perimeter
    return unless perimeters.blank?

    perimeter = Admin::AdminConfig.first&.default_perimeter
    add_perimeter_and_childs(perimeter) if perimeter
  end
end
