module ToolModelConcern
  extend ActiveSupport::Concern
  include ToolConcern

  # Class Methods
  module ClassMethods
    def search_default(rca, tool_name, step_tool)
      return nil unless rca.send(tool_name) && step_tool

      rca.send(tool_name).where(tool_reference: step_tool.tool_reference).first
    end
  end

  # Instance Methods
  # This methods must be redefined in each model

  def tool_num
    return 1
  end
end
