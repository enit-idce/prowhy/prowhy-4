module Tools
  class DocumentsController < ApplicationController
    include ProcessConcern

    before_action :set_tools_document, only: [:show, :edit, :update, :destroy]
    before_action :init_rca, only: [:index, :new, :create]

    # GET /tools/documents
    # GET /tools/documents.json
    def index
      # @tools_documents = Tools::Document.all
      # authorize Document
      skip_authorization

      redirect_to rcas_path and return if @rca.nil? || current_user.get_session(:active_step_tool).nil?

      authorize @rca, :show?, policy_class: Tools::ToolsPolicy

      @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
      @tools_documents = Tools::Document.search_default(@rca, 'tools_documents', @step_tool)
    end

    # # GET /tools/documents/1
    # # GET /tools/documents/1.json
    # def show
    # end

    # GET /tools/documents/new
    def new
      @tools_document = Tools::Document.new(tools_document_params)
      all_steps_types

      authorize @tools_document
    end

    # GET /tools/documents/1/edit
    def edit
      all_steps_types
    end

    # POST /tools/documents
    # POST /tools/documents.json
    def create
      @tools_document = Tools::Document.new(tools_document_params)
      authorize @tools_document

      respond_to do |format|
        if @tools_document.save
          format.html { redirect_to tools_documents_url(rca: @rca), notice: 'Document was successfully created.' }
          format.json { render :show, status: :created, location: @tools_document }
        else
          format.html { render :new }
          format.json { render json: @tools_document.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /tools/documents/1
    # PATCH/PUT /tools/documents/1.json
    def update
      respond_to do |format|
        if @tools_document.update(tools_document_params)
          format.html { redirect_to tools_documents_url(rca: @rca), notice: 'Document was successfully updated.' }
          format.json { render :show, status: :ok, location: @tools_document }
        else
          format.html { render :edit }
          format.json { render json: @tools_document.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /tools/documents/1
    # DELETE /tools/documents/1.json
    def destroy
      @tools_document.file.purge
      @tools_document.destroy
      respond_to do |format|
        format.html { redirect_to tools_documents_url(rca: @rca), notice: 'Document was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_document
      @tools_document = Tools::Document.find(params[:id])
      @rca = @tools_document.rca

      # authorize @rca, :show?
      authorize @tools_document
    end

    # Only allow a list of trusted parameters through.
    def tools_document_params
      params.require(:tools_document).permit(:rca_id, :tool_reference, :tool_num, :name, :description, :file)
    end

    def all_steps_types
      # Meth::StepRole.all.map { |role| [role.name, role.id] }
      @all_steps_types = []
      return unless @tools_document&.rca

      @tools_document&.rca&.methodology&.steps_menu&.includes(:step_role)&.each do |step|
        @all_steps_types << [step.name + ' ' + step.step_role.name, step.step_role.id]
      end
    end
  end
end
