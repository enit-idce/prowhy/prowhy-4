module Labels
  module CheckListElemsHelper
    def elem_tasks_nb(elem, rca)
      nb = elem.tools_tasks.where(rca: rca).size
      nb == 0 ? fa_icon_xmark : "<span class='icon-status-validated' style='border:1px solid #05ae05; padding: 0 7px; border-radius: 0.25rem;'>#{nb}</span>".html_safe
    end
  end
end
