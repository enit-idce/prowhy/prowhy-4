class CreateLabelsQqoqcpConfigs < ActiveRecord::Migration[6.0]
  def change
    create_table :labels_qqoqcp_configs do |t|
      t.string :name_fr
      t.string :name_en
      t.boolean :show_is, default: true
      t.boolean :show_isnot, default: true
      t.boolean :show_info, default: true
      t.boolean :usable, default: true

      # t.timestamps
    end
  end
end
