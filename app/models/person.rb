class Person < ApplicationRecord
  has_one :user, dependent: :destroy

  before_destroy :can_destroy?, prepend: true

  validates :firstname, presence: true
  validates :lastname, presence: true

  validates :lastname, uniqueness: { scope: :firstname, case_sensitive: false, message: 'in Association with this First Name has already been taken' }

  has_many :tools_teams, class_name: 'Tools::Team' # , dependent: :destroy
  has_many :rcas, through: :tools_teams

  has_many :tasks_resp_action, class_name: 'Tools::Task', foreign_key: :responsible_action_id # , dependent: :nullify
  has_many :tasks_resp_rca, class_name: 'Tools::Task', foreign_key: :responsible_rca_id # , dependent: :nullify
  # Fonctionnel. A décommenter si utile.
  # has_many :tasks, ->(person) { unscope(:where).where(responsible_action: person).or(where(responsible_rca: person)) },
  #          class_name: 'Tools::Task'

  before_save do
    self.email = user.email if user
  end

  def name
    # (firstname || '') + ' ' + (lastname || '')
    (lastname || '') + ' ' + (firstname || '')
  end

  def name_fl
    (firstname || '') + ' ' + (lastname || '')
  end

  def the_email
    return user&.email || email
  end

  private

  def can_destroy?
    return unless tools_teams.present? || tasks_resp_action.present? || tasks_resp_rca.present?

    # errors.add(:base, "Person #{name} is included in RCA teams, can not be deleted.")
    throw :abort
  end
end
