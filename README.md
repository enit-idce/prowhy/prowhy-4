# ProWhy V4

@Ecole Nationale d'Ingénieurs de Tarbes - CRC IDCE - 2021

Contact : prowhy@enit.fr

ProWhy est un logiciel libre et gratuit, support des processus de résolution de problèmes en entreprise. Il permet de faciliter la mise en oeuvre de démarches standard utilisées en résolution de problèmes pour l'amélioration continue des produits et des processus.

ProWhy est un logiciel conçu pour structurer, mémoriser et partager les informations issues de la résolution de problèmes et favoriser ainsi le retour d'expérience.

Il prend en charge de manière intégrée, visuelle et collaborative les démarches de résolution les plus répandues (PDCA, 8D, 9S). Depuis la simple anomalie jusqu'au problème le plus complexe, ProWhy offre une approche de traitement harmonieuse.


ProWhy est un logiciel libre distribué sous licence GNU Affero General Public License, Version 3, (https://www.gnu.org/licenses/agpl- 3.0.html). Vous pouvez l'utiliser et le distribuer librement et gratuitement.


_Info ancienne version (doc, site de démo, etc) :_ [www.prowhy.org](www.prowhy.org)


## Install Application and Dependencies (debian / Ubuntu)

WIKI/Installation documentation

## Api

WIKI/Api documentation
