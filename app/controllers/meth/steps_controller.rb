module Meth
  class StepsController < ApplicationController
    include AdminConcern
    # layout 'admin'
    before_action :set_meth_step, only: [:show, :edit, :update, :destroy, :step_add_tool, :step_del_tool, :step_tool_up, :step_tool_down, :step_up, :step_down]

    # GET /steps
    # GET /steps.json
    def index
      authorize Step
      @steps = Step.all
    end

    # GET /steps/1
    # GET /steps/1.json
    def show
    end

    # GET /steps/new
    def new
      @step = Step.new
      authorize @step

      @step.methodology = if params[:methodology]
                            Methodology.find(params[:methodology])
                          else
                            Methodology.first
                          end
      @step.pos = @step.methodology ? @step.methodology.steps.count : 1
    end

    # GET /steps/1/edit
    def edit
    end

    # POST /steps
    # POST /steps.json
    def create
      @step = Step.new(meth_step_params)
      @step.pos = (@step.methodology&.steps_menu&.size || 0) + 1
      authorize @step

      respond_to do |format|
        if @step.save
          # format.html { redirect_to @step, notice: 'Step was successfully created.' }
          format.html { redirect_to edit_meth_methodology_path(id: @step.methodology, step: @step), notice: 'Step was successfully created.' }
          format.json { render :show, status: :created, location: @step }
        else
          format.js   { render :new }
          format.html { redirect_to edit_meth_methodology_path(id: meth_step_params[:methodology_id]) }
          format.json { render json: @step.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /steps/1
    # PATCH/PUT /steps/1.json
    def update
      respond_to do |format|
        if @step.update(meth_step_params)
          format.html { redirect_to @step, notice: 'Step was successfully updated.' }
          format.json { render :show, status: :ok, location: @step }
        else
          format.html { render :edit }
          format.json { render json: @step.errors, status: :unprocessable_entity }
        end
      end
    end

    # GET /step_import
    def step_import
      @methodology = Methodology.find(params[:methodology])
      authorize @methodology, :update?

      # @steps = Step.includes(:methodology).where(step_type: 1, meth_methodologies: {usable: true}).order(:methodology_id)

      render :step_import # , locals: {methodology: params[:methodology]}
    end

    # POST /step_import
    def step_import_do
      methodology2 = Methodology.find(params[:methodology])
      authorize methodology2, :update?

      step = Step.find(params[:step])

      @step = step.copy(methodology2)
      if @step
        @step.pos = methodology2.steps_menu.size+1
        @step.name = (@step.name || 'no name') + ' copy'
        @step.save!
        Positions::MoveService.new(nil, methodology2.steps_menu).call('reinit')
      end

      respond_to do |format|
        if @step
          format.html { redirect_to edit_meth_methodology_path(id: @step.methodology, step: @step), notice: 'Step was successfully imported.' }
        else
          format.html { render :edit }
          format.json { render json: @step&.errors, status: :unprocessable_entity }
        end
      end
    end

    # GET /step_import
    def step_copy
      @step = Step.find(params[:step])
      authorize @step, :update?

      # @steps = Step.includes(:methodology).where(step_type: 1, meth_methodologies: {usable: true}).order(:methodology_id)

      render :step_copy # , locals: {methodology: params[:methodology]}
    end

    # POST /step_import
    def step_copy_do
      @step = Step.find(params[:step])
      authorize @step, :update?

      step_info = Step.find(params[:step_info])

      step_info.copy_step_tools(@step)

      respond_to do |format|
        if @step
          format.html { redirect_to edit_meth_methodology_path(id: @step.methodology, step: @step), notice: 'Step was successfully copied.' }
        else
          format.html { render :edit }
          format.json { render json: @step&.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /steps/1
    # DELETE /steps/1.json
    def destroy
      methodology = @step.methodology
      @step.destroy
      Positions::MoveService.new(nil, methodology.steps_menu).call('reinit')

      respond_to do |format|
        format.html { redirect_to edit_meth_methodology_path(methodology), notice: 'Step was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    # DELETE
    def step_del_tool
      step_tool = StepTool.find(params[:step_tool])
      step_tool.destroy

      redirect_to edit_meth_methodology_path(id: @step.methodology, step: @step)
    end

    # POST
    def step_add_tool
      tool = Tool.find(params[:tool])
      @step.add_tool(tool) if @step && tool

      redirect_to edit_meth_methodology_path(id: @step.methodology, step: @step)
    end

    # PATCH
    def step_tool_up
      step_tool = StepTool.find(params[:step_tool])
      step_tools = @step.step_tools

      Positions::MoveService.new(step_tool, step_tools).call('up')

      redirect_to edit_meth_methodology_path(id: @step.methodology, step: @step)
    end

    def step_tool_down
      step_tool = StepTool.find(params[:step_tool])
      step_tools = @step.step_tools
      Positions::MoveService.new(step_tool, step_tools).call('down')

      redirect_to edit_meth_methodology_path(id: @step.methodology, step: @step)
    end

    # PATCH
    def step_up
      steps = @step.methodology.steps_menu
      Positions::MoveService.new(@step, steps).call('up')

      redirect_to edit_meth_methodology_path(id: @step.methodology, step: @step)
    end

    def step_down
      steps = @step.methodology.steps_menu

      Positions::MoveService.new(@step, steps).call('down')

      redirect_to edit_meth_methodology_path(id: @step.methodology, step: @step)
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_meth_step
      @step = Step.find(params[:id])
      authorize @step
    end

    # Only allow a list of trusted parameters through.
    def meth_step_params
      params.require(:meth_step).permit(:name, :name_fr, :name_en, :pos, :methodology_id, :step_role_id)
    end
  end
end
