require 'rails_helper'

RSpec.describe Tools::IndicatorMeasuresController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/tools/indicator_measures').to route_to('tools/indicator_measures#index')
    end

    it 'routes to #new' do
      expect(get: '/tools/indicator_measures/new').to route_to('tools/indicator_measures#new')
    end

    it 'routes to #show' do
      expect(get: '/tools/indicator_measures/1').to route_to('tools/indicator_measures#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/tools/indicator_measures/1/edit').to route_to('tools/indicator_measures#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/tools/indicator_measures').to route_to('tools/indicator_measures#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/tools/indicator_measures/1').to route_to('tools/indicator_measures#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/tools/indicator_measures/1').to route_to('tools/indicator_measures#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/tools/indicator_measures/1').to route_to('tools/indicator_measures#destroy', id: '1')
    end
  end
end
