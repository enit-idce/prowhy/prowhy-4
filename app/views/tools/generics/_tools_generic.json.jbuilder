json.extract! tools_generic, :id, :rca_id, :tool_reference, :tool_num, :synthesis, :created_at, :updated_at
json.url tools_generic_url(tools_generic, format: :json)
