class ChangeUserPrefsToJson < ActiveRecord::Migration[6.0]
  def self.up
    change_table :users do |t|
      t.remove :preferences
      t.remove :usersession
      t.json :preferences, after: :person_id
      t.json :usersession, after: :preferences
      t.change :usersearch, :json, after: :usersession
    end

    # admin_config = Admin::AdminConfig.first
    # return unless admin_config&.default_perimeter

    # User.all.each do |user|
    #   user.set_preference(:perimeter, admin_config.default_perimeter.id)
    #   # user.usersession ||= {}
    #   # user.usersearch ||= {custom_field: {}, rca_search: {}}
    #   # user.save!
    # end
  end

  def self.down
    change_table :users do |t|
      t.remove :preferences
      t.remove :usersession
      t.text :preferences, after: :person_id
      t.text :usersession, after: :preferences
    end
  end
end
