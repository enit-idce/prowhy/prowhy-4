module Meth
  module CustomFieldDescsHelper
    def custom_field_desc_name_edit(custom_field_desc)
      ret = custom_field_desc.custom_field_type.name

      if custom_field_desc.custom_field_type.field_internal == 'select'
        content = ''
        custom_field_desc.list_contents.each do |lc|
          content += lc.name + '<br/>'
        end
        ret += ' ' + info_popup('Content', content)
        ret += link_to fa_icon_list('icon-right'), meth_list_contents_path(custom_field_desc: custom_field_desc), remote: true
      end

      return ret.html_safe
    end

    def custom_field_desc_used(custom_field_desc)
      content = ''
      custom_field_desc.tool_custom_fields.each do |tcf|
        content += tcf.step_tool.step.name_show_html + '<br/>'
      end
      (custom_field_desc.tool_custom_fields.size.to_s+ ' ' + info_popup('StepTools', content)).html_safe
    end
  end
end
