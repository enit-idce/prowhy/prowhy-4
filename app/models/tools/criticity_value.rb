require_dependency 'tools'

module Tools
  class CriticityValue < ApplicationRecord
    belongs_to :criticity
    belongs_to :labels_criterium, class_name: 'Labels::Criterium'
    belongs_to :labels_value, class_name: 'Labels::Value', optional: true
  end
end
