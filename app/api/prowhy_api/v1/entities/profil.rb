module ProwhyApi
  module V1
    module Entities
      class Profil < Grape::Entity
        expose :id, if: { with_id: true}
        expose :profil_name
        expose :name
      end
    end
  end
end
