module Tools
  class TeamsController < ApplicationController
    include ProcessConcern
    before_action :set_tools_team, only: [:show, :edit, :update, :destroy]
    before_action :init_rca, only: [:index, :new, :create]

    # GET /tools/teams
    # GET /tools/teams.json
    def index
      # authorize Team

      if @rca.nil? || current_user.get_session(:active_step_tool).nil?
        redirect_to rcas_path
      else
        @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))

        # TODO : A voir si on laisse ici la mise à jour de la liste des responsables actions.
        Rcas::UpdateActionsTeam.new(@rca).call
      end
      @tools_teams = @rca.tools_teams.includes(:person)
      begin
        authorize @tools_teams.first
      rescue StandardError
        authorize Team
      end
      # @tools_teams = policy_scope(Team).where(rca_id: @rca.id).includes(:person)
    end

    # # GET /tools/teams/1
    # # GET /tools/teams/1.json
    # def show
    # end

    # GET /tools/teams/new
    def new
      if @rca.nil? || current_user.get_session(:active_step_tool).nil?
        authorize Team

        redirect_to rcas_path
      else
        @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
        @tools_team = Tools::Team.new(tools_team_params)

        authorize @tools_team
      end
    end

    # GET /tools/teams/1/edit
    def edit
      render :new
    end

    # POST /tools/teams
    # POST /tools/teams.json
    def create
      @tools_team = Tools::Team.new(tools_team_params)
      authorize @tools_team

      respond_to do |format|
        if @tools_team.save
          format.html { redirect_to tools_teams_url(rca: @rca), notice: 'Team was successfully created.' }
          format.json { render :show, status: :created, location: @tools_team }
        else
          format.html { render :new }
          format.json { render json: @tools_team.errors, status: :unprocessable_entity }
          format.js { render :new }
        end
      end
    end

    # PATCH/PUT /tools/teams/1
    # PATCH/PUT /tools/teams/1.json
    def update
      respond_to do |format|
        if @tools_team.update(tools_team_params)
          format.html { redirect_to tools_teams_url(rca: @rca), notice: 'Team was successfully updated.' }
          format.json { render :show, status: :ok, location: @tools_team }
        else
          format.html { render :new }
          format.json { render json: @tools_team.errors, status: :unprocessable_entity }
          format.js { render :new }
        end
      end
    end

    # DELETE /tools/teams/1
    # DELETE /tools/teams/1.json
    def destroy
      @tools_team.destroy
      respond_to do |format|
        format.html { redirect_to tools_teams_url(rca: @rca), notice: 'Team was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_team
      @tools_team = Tools::Team.find(params[:id])
      @rca = @tools_team.rca

      authorize @rca, :show?
      authorize @tools_team
    end

    # Only allow a list of trusted parameters through.
    def tools_team_params
      params.require(:tools_team).permit(:rca_id, :tool_reference, :tool_num, :person_id, :profil_id)
    end
  end
end
