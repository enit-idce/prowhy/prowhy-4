module ProwhyApi
  module V1
    class Api < Grape::API
      version 'v1', using: :path, format: :json # using: :header, format: :json #
      format :json

      helpers ApiHelpers::UsersHelpers # , ApiHelpers::SharedParams
      helpers ApiHelpers::SharedParams
      helpers ApiHelpers::FormatHelpers

      before do
        doorkeeper_authorize! unless user_signed_in?
      end

      before do
        # parsed_locale = params[:locale]

        # binding.pry
        parsed_locale = headers['Accept-Language']
        user_locale = current_user&.get_preference(:locale)
        I18n.locale = I18n.available_locales.map(&:to_s).include?(parsed_locale) ? parsed_locale : (!user_locale.to_s.empty? ? user_locale : I18n.default_locale)

        # binding.pry
      end

      mount ProwhyApi::V1::ApiUsers
      mount ProwhyApi::V1::ApiPeople
      mount ProwhyApi::V1::ApiProfils
      mount ProwhyApi::V1::ApiRcas
      mount ProwhyApi::V1::ApiTools::ApiTasks
      mount ProwhyApi::V1::ApiTools::ApiCauses
      mount ProwhyApi::V1::ApiLabels::ApiCauseTypes
      mount ProwhyApi::V1::ApiTools::ApiImages
      mount ProwhyApi::V1::ApiTools::ApiTeams
      mount ProwhyApi::V1::ApiTools::ApiQqoqcps
      mount ProwhyApi::V1::ApiMeth::ApiMethodologies
      mount ProwhyApi::V1::ApiMeth::ApiCustomFieldDescs
      mount ProwhyApi::V1::ApiMeth::ApiListContents

      # get '/' do
      #   [{api: 'welcome!'}]
      # end
    end
  end
end
