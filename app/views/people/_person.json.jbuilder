json.extract! person, :id, :firstname, :lastname, :email, :adress, :phone, :moreinfo, :created_at, :updated_at
json.url person_url(person, format: :json)
