require_dependency 'tools'

module Tools
  class Qqoqcp < ApplicationRecord
    include ToolConcern
    # belongs_to :rca

    belongs_to :labels_qqoqcp_config, class_name: 'Labels::QqoqcpConfig', optional: true
    has_many :qqoqcp_values, dependent: :destroy

    accepts_nested_attributes_for :qqoqcp_values

    def initialize_qqoqcp
      return unless labels_qqoqcp_config

      labels_qqoqcp_config.qqoqcp_lines.each do |line|
        next unless qqoqcp_values&.where(labels_qqoqcp_line_id: line.id)&.empty?

        Tools::QqoqcpValue.create!(qqoqcp_id: id, labels_qqoqcp_line_id: line.id, is_value: nil, isnot_value: nil, info_value: nil)
      end
    end

    def self.create_default!(options, step_tool)
      qqoqcp = super(options, step_tool)

      begin
        # if step_tool.tool_options['qqoqcp_config']
        qqoqcp.labels_qqoqcp_config = Labels::QqoqcpConfig.find(step_tool.tool_options[:qqoqcp_config])
        qqoqcp.initialize_qqoqcp
        qqoqcp.save!
      rescue StandardError
        # puts 'INFO: Qqoqcp => no configuration / no initialisation.'.blue
      end

      qqoqcp
    end

    def qqoqcp_values_from_config
      lines = labels_qqoqcp_config&.qqoqcp_lines&.map(&:id) || []

      # qqoqcp_values.where(labels_qqoqcp_line_id: lines)
      qqoqcp_values.where(labels_qqoqcp_line_id: lines).order(Arel.sql("field(labels_qqoqcp_line_id, #{lines.join ','})"))
    end
  end
end
