json.extract! profil, :id, :profil_name, :name_fr, :name_en, :profil_locked, :created_at, :updated_at
json.url profil_url(profil, format: :json)
