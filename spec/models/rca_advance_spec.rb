require 'rails_helper'

RSpec.describe RcaAdvance, type: :model do
  # subject(:rca_advance) do
  #   rca.rca_advances.first
  # end

  # let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  # let(:rca) { FactoryBot.create(:rca, methodology: methodology) }

  # Tests has_many :tools + dependant: destroy
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to rca' do
        assc = RcaAdvance.reflect_on_association(:rca)
        expect(assc.macro).to eq :belongs_to
      end

      it 'belongs to meth_step' do
        assc = RcaAdvance.reflect_on_association(:meth_step)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end
end
