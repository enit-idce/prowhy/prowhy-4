module Rcas
  class DataGraph
    def initialize(rcas)
      @rcas = rcas
    end

    def call
    end

    def create_datas
      advance_count = @rcas.group(:methodology_id, :advance).count(:id)

      @advance_labels = {}
      @advance_values = {}
      @advance_steproles = {}

      # Labels for all methodologies
      @advance_labels[0] = [I18n.t('not_started')] + Meth::StepRole.where.not(advance_level: 0).order(:advance_level).map(&:name)
      @advance_steproles[0] = [0] + Meth::StepRole.where.not(advance_level: 0).order(:advance_level).map(&:advance_level)

      # Values (initialize to 0) for all methodologies
      @advance_values[0] = []
      (0..@advance_labels[0].length-1).each do |i|
        @advance_values[0][i] = 0
      end

      # For each methodology
      Meth::Methodology.rca_methodologies_all.includes(:steps_menu).each do |methodology|
        next if methodology.locked

        # Labels & correspondance with step_role advance level
        # @advance_labels[methodology.id] = ['Not started'] + methodology.steps_menu.includes(:step_role).map { |m| m.step_role.name }
        @advance_labels[methodology.id] = [I18n.t('not_started')] + methodology.steps_menu.map(&:name)
        @advance_steproles[methodology.id] = [0] + methodology.steps_menu.includes(:step_role).map { |m| m.step_role.advance_level }

        # Values (initialize to 0)
        @advance_values[methodology.id] = []
        (0..@advance_labels[methodology.id].length-1).each do |i|
          @advance_values[methodology.id][i] = 0
        end
      end

      # Values
      advance_count.each do |key, val|
        methodo = key[0]
        adv_level = key[1]

        next unless @advance_steproles[methodo] && @advance_values[methodo]

        @advance_values[methodo][@advance_steproles[methodo].index(adv_level)] = val
        @advance_values[0][adv_level] += val
      end

      return @advance_labels, @advance_values
    end
  end
end
