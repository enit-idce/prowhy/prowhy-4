require 'rails_helper'

RSpec.feature 'Open user page', type: :feature do
  before :all do
    load "#{Rails.root}/db/seed_tests.rb"
    @methodology = Meth::Methodology.rca_methodologies.where(name_fr: 'Méthode 8D').first

    @user = User.first
    @user.save!
    sign_in @user

    @rca = Rca.create!(title: 'Mon premier Rca', methodology: @methodology)
    @rca.rca_creation_init(@user)

    # Add elements
    step_tool = @methodology.steps_menu.first.step_tools.first
    cause_tree = @rca.find_or_create_tool('tools_cause_trees', 'Tools::CauseTree', step_tool)
    FactoryBot.create(:task, rca_id: @rca.id, task_detail_type: 'Tools::TaskCorrective', tools_cause_id: cause_tree.cause_link.id, responsible_action_id: @user.id)
    FactoryBot.create(:task, rca_id: @rca.id, task_detail_type: 'Tools::TaskCorrective', tools_cause_id: cause_tree.cause_link.id, responsible_action_id: @user.id)
    FactoryBot.create(:task, rca_id: @rca.id, task_detail_type: 'Tools::TaskCorrective', tools_cause_id: cause_tree.cause_link.id, responsible_action_id: @user.id)
    # FactoryBot.create(:task, rca_id: @rca.id, task_detail_type: 'Tools::TaskCorrective', tools_cause_id: cause_tree.cause_link.id, responsible_action_id: @user.id)
  end

  after :all do
    load "#{Rails.root}/db/seed_empty.rb"
  end

  scenario 'Open user page', js: true do
    visit '/rcas'

    click_link @user.username
    find("a[href='#{edit_user_registration_path}']").click

    expect(page).to have_current_path(edit_user_registration_path)

    # expect(page).to have_text(@user.username)
    expect(page).to have_field('Username', with: @user.username)
    expect(page).to have_field('Email', with: @user.email)

    # Open  preferences page
    find("a[href='#{edit_user_preferences_registration_path}']").click

    expect(page).to have_current_path(edit_user_preferences_registration_path)
    expect(page).to have_text('Preferences Locale')

    # Open User action plan page
    # binding.pry
    # save_and_open_page
    find("a[href='#{meth_step_tool_show_path(id: 1)}']").click

    expect(page).to have_current_path(tools_tasks_path(step_tool: 1))

    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count: 6) # nb tasks + 3 (title, search, 'action type')
    end
  end
end
