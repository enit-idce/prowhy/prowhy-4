module Tools
  class DecisionMatsController < ApplicationController
    include ProcessConcern
    before_action :set_tools_decision_mat, only: [:show, :edit, :update, :destroy,
                                                  :save_values, :delete_entry, :add_entry, :valid_entries,
                                                  :add_criteria, :valid_criteria]
    before_action :init_rca, only: [:index, :new, :create]

    # Track
    after_action :call_track_action, only: [:create, :destroy, :update, :valid_entries, :valid_criteria, :save_values]

    # GET /tools/decision_mats
    # GET /tools/decision_mats.json
    def index
      # authorize DecisionMat
      skip_authorization

      if @rca.nil? || current_user.get_session(:active_step_tool).nil?
        redirect_to rcas_path
      else
        # authorize DecisionMat
        authorize @rca, :show?, policy_class: Tools::ToolsPolicy

        @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
        @step_tool.tool_options ||= {}

        @tools_decision_mats = @rca.tools_decision_mats.where(entry_class: @step_tool.tool_options[:entry_class],
                                                              tool_reference: @step_tool.tool_reference,
                                                              tool_num: @step_tool.tool_num)

        render :index_show_all
      end
    end

    # GET /tools/decision_mats/1
    # GET /tools/decision_mats/1.json
    # def show
    # end

    # GET /tools/decision_mats/new
    def new
      @tools_decision_mat = Tools::DecisionMat.new(tools_decision_mat_params)

      authorize @tools_decision_mat
    end

    # GET /tools/decision_mats/1/edit
    def edit
      render :new
    end

    # POST /tools/decision_mats
    # POST /tools/decision_mats.json
    def create
      @tools_decision_mat = Tools::DecisionMat.new(tools_decision_mat_params)

      authorize @tools_decision_mat

      respond_to do |format|
        if @tools_decision_mat.save
          @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))

          if @step_tool.tool_options[:criteria]
            @step_tool.tool_options[:criteria].each do |option_crit|
              begin
                crit = Labels::Criterium.find(option_crit[:id])
              rescue StandardError
                puts "No criterium with id = #{option_crit[:id]} => pass.".red
                next
              end
              weight = option_crit[:weight]
              @tools_decision_mat.add_criterium(crit, weight)
            end
          else
            # Add all criteria with weight = 1
            Labels::Criterium.type_matrix.each do |crit|
              @tools_decision_mat.add_criterium(crit, 1)
            end
          end

          # Add Entries
          # entries = entries_list
          # entries.each do |entry|
          #   @tools_decision_mat.add_entry(entry)
          # end

          # Ok render
          format.html do
            redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca),
                        notice: t('.message_succes', decision_mat: Tools::DecisionMat.model_name.human)
          end
          format.js { add_entry }
          format.json { render :show, status: :created, location: @tools_decision_mat }
        else
          format.html { render :new }
          format.js { render :new }
          format.json { render json: @tools_decision_mat.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /tools/decision_mats/1
    # PATCH/PUT /tools/decision_mats/1.json
    def update
      respond_to do |format|
        if @tools_decision_mat.update(tools_decision_mat_params)
          format.html do
            redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca),
                        notice: t('.message_succes', decision_mat: Tools::DecisionMat.model_name.human)
          end
          format.json { render :show, status: :ok, location: @tools_decision_mat }
        else
          # format.html { render :edit }
          format.js { render :new }
          format.json { render json: @tools_decision_mat.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /tools/decision_mats/1
    # DELETE /tools/decision_mats/1.json
    def destroy
      @tools_decision_mat.destroy
      respond_to do |format|
        format.html do
          redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca),
                      notice: t('.message_succes', decision_mat: Tools::DecisionMat.model_name.human)
        end
        format.json { head :no_content }
      end
    end

    # # GET open
    # def open
    #   # @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
    #   # @tools_tasks = @rca.tools_tasks.where(@step_tool.tool_options[:entry].merge(rca_id: @rca.id,
    #   #                                                                             tool_reference: @step_tool.tool_reference))
    #   # voir pour ajouter tool_num dans le where si tool_num != 0 (si 0 on prend tous)
    # end

    # PATCH save_values
    def save_values
      # puts 'SAVE VALUES==============>'.yellow
      # p(params)
      params[:dec_mat_val].each_key do |key|
        dec_mat_val = Tools::DecMatVal.find(key)
        dec_mat_val.value = params[:dec_mat_val][key].to_i
        dec_mat_val.save!
      end

      params[:dec_mat_checked].each_key do |key|
        dec_mat_link = Tools::DecMatLink.find(key)
        dec_mat_link.matrix_status = params[:dec_mat_checked][key]
        dec_mat_link.save!

        dec_mat_link.entry.matrix_status_calculate
      end

      respond_to do |format|
        format.html do
          redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca),
                      notice: t('.message_succes', decision_mat: Tools::DecisionMat.model_name.human)
        end
      end
    end

    # DELETE delete_entry => UNUSED !
    # def delete_entry
    #   dec_mat_link = Tools::DecMatLink.find(params[:entry])
    #   # entry = dec_mat_link.entry
    #   # dec_mat_link.destroy
    #   # entry.matrix_status_calculate
    #   @tools_decision_mat.del_entry_from_link(dec_mat_link)

    #   respond_to do |format|
    #     format.html { redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca), notice: 'Entry was successfully destroyed.' }
    #   end
    # end

    # GET add_entry (JS)
    def add_entry
      entries = entries_list
      render :add_entry, locals: {tools_decision_mat: @tools_decision_mat, entries: entries}
    end

    # PATCH valid_entries
    # Ajoute et/ou supprime les entrées en fonction des sélections
    # Recalcule le status des entrées modifiées => Pour del et add
    def valid_entries
      entries = entries_list
      entries.each do |entry|
        entry_in_mat =  entry.decision_mats.include?(@tools_decision_mat)
        if entry_in_mat && params[:dec_mat_entry][entry.id.to_s] == 'false'
          # Delete entry
          @tools_decision_mat.del_entry(entry)
        elsif !entry_in_mat && params[:dec_mat_entry][entry.id.to_s] == 'true'
          # Add entry
          @tools_decision_mat.add_entry(entry)
        end
      end

      respond_to do |format|
        format.html { redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca), notice: t('.message_succes') }
      end
    end

    # GET add_criteria (JS)
    def add_criteria
      criteria = Labels::Criterium.type_matrix
      render :add_criteria, locals: {tools_decision_mat: @tools_decision_mat, criteria: criteria}
    end

    # PATCH valid_criteria
    # Ajoute et/ou supprime les entrées en fonction des sélections
    # Recalcule le status des entrées modifiées => Pour del et add
    def valid_criteria
      criteria = Labels::Criterium.type_matrix
      # binding.pry
      criteria.each do |crit|
        crit_in_mat =  @tools_decision_mat.labels_criteria.include?(crit)
        weight = params[:dec_mat_weight][crit.id.to_s]

        # Ajout / Supression de critèeres
        if crit_in_mat && params[:dec_mat_crit][crit.id.to_s] == 'false'
          # Delete criterium
          @tools_decision_mat.del_criterium(crit)
        elsif !crit_in_mat && params[:dec_mat_crit][crit.id.to_s] == 'true'
          # Add criterium
          @tools_decision_mat.add_criterium(crit, weight)
        elsif params[:dec_mat_crit][crit.id.to_s] == 'true' # set weight to criterium
          crit_dm = @tools_decision_mat.crit_dec_mats.where(labels_criterium: crit)&.first
          crit_dm.update!(weight: weight)
        end
      end

      respond_to do |format|
        format.html { redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca), notice: t('.message_succes') }
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_decision_mat
      @tools_decision_mat = Tools::DecisionMat.find(params[:id])
      @rca = @tools_decision_mat.rca

      # authorize @rca, :show?
      authorize @tools_decision_mat
    end

    # Only allow a list of trusted parameters through.
    def tools_decision_mat_params
      params.require(:tools_decision_mat).permit(:rca_id, :name, :entry_class, :max_value, :tool_reference, :tool_num)
    end

    def entries_list
      @step_tool ||= Meth::StepTool.find(current_user.get_session(:active_step_tool))

      entries = if @rca
                  @rca.send(@tools_decision_mat.entry_class.constantize.table_name)
                      &.where(@step_tool.tool_options[:entry]
                      &.merge(rca_id: @rca.id, tool_reference: @step_tool.tool_reference))
                else
                  []
                end

      return entries
    end
  end
end
