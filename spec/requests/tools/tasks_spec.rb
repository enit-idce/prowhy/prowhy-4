require 'rails_helper'
# require_relative '../../support/devise'
# require_relative '../../support/controller_macros'

RSpec.describe '/tools/tasks', type: :request do
  let(:valid_attributes) do
    { rca_id: rca.id, task_detail_type: 'Corrective', tool_reference: 1,
      name: 'Task 1', tools_cause_id: tools_cause.id,
      responsible_action_id: person.id, responsible_rca_id: person.id }
  end
  let(:invalid_attributes) do
    { rca_id: rca.id, tool_reference: 1,
      name: nil, tools_cause_id: tools_cause.id,
      responsible_action_id: person.id, responsible_rca_id: person.id }
  end

  let(:rca) { FactoryBot.create(:rca, methodology: methodology) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:person) { FactoryBot.create(:person) }
  let(:user) { FactoryBot.create(:user, person_id: person.id) }
  let(:tools_cause) { FactoryBot.create(:cause, rca: rca) }

  # before :each do
  #   activity = Meth::Activity.create!(name: 'Infos')
  #   Meth::Tool.create!([{name: 'Champs personnalisés', activity: activity, tool_name: 'CustomField', tool_options: {} },
  #                       {name: 'PA', activity: activity, tool_name: 'Action', tool_options: {}}])
  #   methodology.create_methodology
  # end

  # context 'Test PA queries and display epending on PA type' do
  #   before :each do
  #     # binding.pry
  #     user.set_session(:active_step_tool, methodology.steps.first.step_tools.first.id)
  #     user.save!

  #     sign_in user

  #     # Create datas

  #   end

  #   describe '' do
  #     it 'query return all actions related to one RCA' do
  #     end

  #     it 'display actions related to one RCA' do
  #     end
  #   end
  # end

  context 'From logged user' do
    before :each do
      # binding.pry
      user.set_session(:active_step_tool, methodology.steps.first.step_tools.first.id)
      user.save!

      sign_in user
    end

    describe 'GET /index' do
      context 'without step tool defined' do
        it 'renders a redirection response' do
          user.set_session(:active_step_tool, nil)
          get '/tools/tasks'
          # redirection car rca non défini
          expect(response).to have_http_status(:redirect)
          expect(response).to redirect_to(rcas_path)
        end
      end

      context 'with rca and step_tool defined' do
        it 'renders a successful response' do
          get('/tools/tasks', params: {rca: rca.id})
          expect(response).to be_successful
        end
      end
    end

    describe 'GET /new' do
      context 'without rca defined' do
        it 'renders a redirection response' do
          get new_tools_task_path, params: {}, xhr: true
          expect(response).to have_http_status(:redirect)
          expect(response).to redirect_to(rcas_path)
        end
      end

      context 'with rca and step_tool defined' do
        it 'renders a successful response' do
          get new_tools_task_path, params: { tools_task: valid_attributes, rca: rca.id }, xhr: true # , session: {rca: rca.id, active_step_tool: Meth::StepTool.first.id}
          expect(response).to be_successful
        end
      end
    end

    describe 'GET /edit' do
      it 'render a successful response' do
        tools_task = Tools::Task.create! valid_attributes
        get edit_tools_task_url(tools_task), params: {}, xhr: true
        expect(response).to be_successful
      end
    end

    describe 'POST /create' do
      context 'with valid parameters' do
        it 'creates a new Tools::Task' do
          expect {
            post tools_tasks_url, params: { tools_task: valid_attributes } # , xhr: true
          }.to change(Tools::Task, :count).by(1)
        end

        it 'redirects to the created tools_task' do
          post tools_tasks_url, params: { tools_task: valid_attributes } # , xhr: true
          expect(response).to redirect_to(meth_step_tool_show_path(id: user.get_session(:active_step_tool), rca: rca))
        end
      end

      context 'with invalid parameters' do
        it 'does not create a new Tools::Task' do
          expect {
            post tools_tasks_url, params: { tools_task: invalid_attributes }, xhr: true
          }.to change(Tools::Task, :count).by(0)
        end

        it 'renders a successful response' do
          post tools_tasks_url, params: { tools_task: invalid_attributes }, xhr: true
          expect(response).to be_successful
        end
      end
    end

    describe 'PATCH /update' do
      context 'with valid parameters' do
        let(:valid_update_attributes) { {name: 'Task 2'} }

        it 'updates the requested tools_task' do
          tools_task = Tools::Task.create! valid_attributes
          patch tools_task_url(tools_task), params: { tools_task: valid_update_attributes }, xhr: true
          tools_task.reload
          # skip('Add assertions for updated state')
          expect(tools_task.name).to eq('Task 2')
        end

        it 'redirects to the tools_task' do
          tools_task = Tools::Task.create! valid_attributes
          patch tools_task_url(tools_task), params: { tools_task: valid_update_attributes } # , xhr: true
          tools_task.reload
          expect(response).to redirect_to(meth_step_tool_show_path(id: user.get_session(:active_step_tool), rca: rca))
        end
      end

      context 'with invalid parameters' do
        let(:invalid_update_attributes) { {name: nil} }

        it 'renders a successful response (i.e. to display the edit template)' do
          tools_task = Tools::Task.create! valid_attributes
          patch tools_task_url(tools_task), params: { tools_task: invalid_update_attributes }, xhr: true
          expect(response).to be_successful
        end
      end
    end

    describe 'DELETE /destroy' do
      it 'destroys the requested tools_task' do
        tools_task = Tools::Task.create! valid_attributes
        expect {
          delete tools_task_url(tools_task)
        }.to change(Tools::Task, :count).by(-1)
      end

      it 'redirects to the tools_tasks list' do
        tools_task = Tools::Task.create! valid_attributes
        delete tools_task_url(tools_task)
        expect(response).to redirect_to(meth_step_tool_show_path(id: user.get_session(:active_step_tool), rca: rca))
      end
    end

    describe 'GET /task_get_cause' do
      it 'return cause_id in Json format' do
        headers = { 'ACCEPT' => 'application/json' }
        tools_task = Tools::Task.create! valid_attributes
        get tools_task_get_cause_path, params: {id: tools_task.id}, headers: headers
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end

    describe 'PATCH /update_advance' do
      it 'update task advancement (no save)' do
        tools_task = Tools::Task.create! valid_attributes.merge({advancement: 1})
        patch tools_task_update_advance_url, params: { id: tools_task.id, tools_task: {advancement: 3} }
        # expect(tools_task.advancement).to eq 3
        expect(tools_task.reload.advancement).to eq 1
      end

      it 'render a succesfull response in JS' do
        tools_task = Tools::Task.create! valid_attributes.merge({advancement: 1})
        patch tools_task_update_advance_url, params: { id: tools_task.id, tools_task: {advancement: 3} }, xhr: true
        expect(response).to be_successful
      end

      it 'redirect to tasks index in html' do
        tools_task = Tools::Task.create! valid_attributes.merge({advancement: 1})
        patch tools_task_update_advance_url, params: { id: tools_task.id, tools_task: {advancement: 3} }
        expect(response).to redirect_to(tools_tasks_url(rca: rca))
      end
    end
  end
end
