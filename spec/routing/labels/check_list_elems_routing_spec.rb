require "rails_helper"

RSpec.describe Labels::CheckListElemsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/labels/check_list_elems").to route_to("labels/check_list_elems#index")
    end

    it "routes to #new" do
      expect(get: "/labels/check_list_elems/new").to route_to("labels/check_list_elems#new")
    end

    it "routes to #show" do
      expect(get: "/labels/check_list_elems/1").to route_to("labels/check_list_elems#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/labels/check_list_elems/1/edit").to route_to("labels/check_list_elems#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/labels/check_list_elems").to route_to("labels/check_list_elems#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/labels/check_list_elems/1").to route_to("labels/check_list_elems#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/labels/check_list_elems/1").to route_to("labels/check_list_elems#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/labels/check_list_elems/1").to route_to("labels/check_list_elems#destroy", id: "1")
    end
  end
end
