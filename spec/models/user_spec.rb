require 'rails_helper'

RSpec.describe User, type: :model do
  subject(:user) do
    FactoryBot.create(:user)
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to person' do
        assc = User.reflect_on_association(:person)
        expect(assc.macro).to eq :belongs_to
      end
    end

    describe 'Validations' do
      it 'is not valid without username' do
        user.username = ''
        expect(user).to_not be_valid
      end

      it 'is not valid with an invalid username (username with &)' do
        user.username = 'toto&truc'
        expect(user).to_not be_valid
      end

      it 'is valid with a valid username' do
        user.username = 'toto'
        expect(user).to be_valid
      end

      it 'is valid with a valid username containing -, @ and .' do
        user.username = 'toto-tata@bidule.com'
        expect(user).to be_valid
      end
    end
  end

  describe 'Person Attributes' do
    pending "trouver une solution pour ce test : fonctionne dans l'application mais pas dans le test."
    # it 'initialize associated person if exist' do
    #   person = FactoryBot.create(:person)
    #   user2 = User.new(username: 'toto', person_attributes: {id: person.id})
    #   # user2.person_attributes=(attributes)
    #   expect(user2.person).to eq person
    # end

    it 'do not initialize person if it does not exist' do
      attributes = {}
      user2 = User.new(username: 'toto')
      user2.person_attributes=(attributes)
      expect(user2.person.id).to eq nil
    end
  end

  # Tests has_many :tools + dependant: destroy
  describe 'User Session' do
    it 'initialize usersession when user is created' do
      expect(user.usersession).to eq({})
    end

    it 'get value of a usersession parameter' do
      user.usersession['param1'] = 'toto'
      user.usersession['param2'] = 'titi'

      # expect(user.usersession).to eq(param1: 'toto', param2: 'titi')
      expect(user.get_session(:param1)).to eq 'toto'
      expect(user.get_session(:param2)).to eq 'titi'
    end

    it 'set value of a usersession parameter' do
      user.set_session(:param1, 'toto')
      user.set_session(:param2, 'titi')

      # expect(user.usersession).to eq(param1: 'toto', param2: 'titi')
      expect(user.get_session(:param1)).to eq 'toto'
      expect(user.get_session(:param2)).to eq 'titi'
    end
  end

  describe 'User name' do
    it 'returs username as user name' do
      expect(user.name).to eq user.username
    end
  end

  describe 'Find db authentication' do
    it 'find user with login=username' do
      expect(User.find_for_database_authentication(login: user.username)).to eq user
    end

    it 'find user with username=username' do
      expect(User.find_for_database_authentication(username: user.username)).to eq user
    end

    it 'find user with login=email' do
      expect(User.find_for_database_authentication(login: user.email)).to eq user
    end

    it 'find user with email=email' do
      expect(User.find_for_database_authentication(email: user.email)).to eq user
    end

    it 'do not find user with login=nimportequoi' do
      expect(User.find_for_database_authentication(login: 'nimportequoi')).to eq nil
    end
  end
end
