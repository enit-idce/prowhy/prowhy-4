class CreatePerimeterAndAssignUsers < ActiveRecord::Migration[6.0]
  class MigrationAdminPerimeter < ApplicationRecord
    self.table_name = :admin_perimeters
  end

  class MigrationAdminPerimeterUser < ApplicationRecord
    self.table_name = :admin_perimeter_users
  end

  class MigrationUser < ApplicationRecord
    self.table_name = :users
  end

  def up
    perimeter = MigrationAdminPerimeter.where(ancestry: nil).first
    if perimeter.nil?
      say 'Create first perimeter ALL.'
      perimeter = MigrationAdminPerimeter.create!(name: 'ALL', ancestry: nil)
    else
      say 'First perimeter exists : creates nothing.'
    end

    say 'Assign first perimeter to all users.'
    MigrationUser.all.each do |user|
      MigrationAdminPerimeterUser.create!(user_id: user.id, perimeter_id: perimeter.id)
    end
  end

  def down
    say 'Do not destroy perimeter and users links.'
  end
end
