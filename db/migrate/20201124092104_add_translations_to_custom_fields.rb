class AddTranslationsToCustomFields < ActiveRecord::Migration[6.0]
  def change
    # Meth::CustomFieldType
    add_column :meth_custom_field_types, :field_name_fr, :string, after: :field_name
    add_column :meth_custom_field_types, :field_name_en, :string, after: :field_name_fr

    Meth::CustomFieldType.all.each do |elem|
      elem.field_name_fr = elem.field_name
      elem.save!
    end

    remove_column :meth_custom_field_types, :field_name

    # Meth::CustomFieldDesc
    add_column :meth_custom_field_descs, :name_fr, :string, after: :name
    add_column :meth_custom_field_descs, :name_en, :string, after: :name_fr

    Meth::CustomFieldDesc.all.each do |elem|
      elem.name_fr = elem.name
      elem.save!
    end

    remove_column :meth_custom_field_descs, :name

    # # Meth::ListContent
    # add_column :meth_list_contents, :name_fr, :string, after: :name
    # add_column :meth_list_contents, :name_en, :string, after: :name_fr

    # Meth::ListContent.all.each do |elem|
    #   elem.name_fr = elem.name
    #   elem.save!
    # end

    # remove_column :meth_list_contents, :name
  end
end
