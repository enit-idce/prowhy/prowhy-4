class PermitLabelCauseTypeNullToToolsCauseClasses < ActiveRecord::Migration[6.0]
  def up
    change_column :tools_cause_classes, :labels_cause_type_id, :bigint, null: true
  end

  def down
    change_column :tools_cause_classes, :labels_cause_type_id, :bigint, null: false
  end
end
