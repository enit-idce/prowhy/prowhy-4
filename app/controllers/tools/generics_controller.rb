module Tools
  class GenericsController < ApplicationController
    include ProcessConcern

    before_action :set_tools_generic, only: [:show, :edit, :update, :destroy, :download_last_doc]
    before_action :init_rca, only: [:index, :new, :create]

    # GET /tools/generics
    # GET /tools/generics.json
    # def index
    #   @tools_generics = Tools::Generic.all
    # end

    # GET /tools/generics/1
    # GET /tools/generics/1.json
    # def show
    # end

    # GET /tools/generics/new
    # def new
    #   @tools_generic = Tools::Generic.new
    # end

    # GET /tools/generics/1/edit
    def edit
    end

    # POST /tools/generics
    # POST /tools/generics.json
    # def create
    #   @tools_generic = Tools::Generic.new(tools_generic_params)

    #   respond_to do |format|
    #     if @tools_generic.save
    #       format.html { redirect_to @tools_generic, notice: 'Generic was successfully created.' }
    #       format.json { render :show, status: :created, location: @tools_generic }
    #     else
    #       format.html { render :new }
    #       format.json { render json: @tools_generic.errors, status: :unprocessable_entity }
    #     end
    #   end
    # end

    # PATCH/PUT /tools/generics/1
    # PATCH/PUT /tools/generics/1.json
    def update
      respond_to do |format|
        if @tools_generic.update(tools_generic_params)
          message = 'Tool successfully updated.'
          if params[:tools_generic][:images].present?
            params[:tools_generic][:images].each do |image|
              @tools_generic.images.attach(image)
            end
            message += ' - Images sent.'
          end
          if params[:tools_generic][:doc].present?
            file_v = Tools::FileVersion.create!(element: @tools_generic, date: Time.now, author: current_user&.person)
            file_v.file = params[:tools_generic][:doc]
            file_v.save!
            message += ' - Document sent.'

            config = Admin::AdminConfig.first
            if config&.options&.[]('file_versions_nb_max') && config.options['file_versions_nb_max'].to_i != 0 && @tools_generic.file_versions.size > config.options['file_versions_nb_max'].to_i
              @tools_generic.file_versions.last.destroy
              message += ' - Oldest doc version deleted.'
            end
          end
          format.html { redirect_to edit_tools_generic_path(@tools_generic), notice: message }
          format.json { render :show, status: :ok, location: @tools_generic }
        else
          format.html { render :edit }
          format.json { render json: @tools_generic.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /tools/generics/1
    # DELETE /tools/generics/1.json
    def destroy
      notice_message = 'Nothing done.'
      if params[:image_attachment_id]
        @tools_generic.images.find_by_id(params[:image_attachment_id]).purge
        notice_message = 'Image was successfully destroyed.'
      end

      # @tools_generic.destroy
      respond_to do |format|
        format.html { redirect_to edit_tools_generic_path(@tools_generic), notice: notice_message }
        format.json { head :no_content }
      end
    end

    def download_last_doc
      current_user.set_preference("tools-gen-#{@tools_generic.id}", helpers.convert_time(Time.now))

      if !@tools_generic.file_versions.empty? && @tools_generic.file_versions&.first&.file&.attached?
        rails_blob_path(@tools_generic.file_versions.first.file, disposition: 'attachment')
      else
        rails_blob_path(@tools_generic.doc, disposition: 'attachment')
      end

      render :edit
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_generic
      @tools_generic = Tools::Generic.find(params[:id])
      @rca = @tools_generic.rca

      authorize @tools_generic
    end

    # Only allow a list of trusted parameters through.
    def tools_generic_params
      params.require(:tools_generic).permit(:rca_id, :tool_reference, :tool_num, :synthesis)
    end
  end
end
