class DatasCreateProfilAuthorize < ActiveRecord::Migration[6.0]
  class MigrationProfil < ApplicationRecord
    self.table_name = :profils
  end
  class MigrationStepRole < ApplicationRecord
    self.table_name = :meth_step_roles
  end

  def up
    MigrationProfil.all.each do |profil|
      say "Create Authorization for profil #{profil.profil_name}"

      MigrationStepRole.all.each do |step_role|
        next unless ProfilAuthorize.where(profil_id: profil.id, meth_step_role_id: step_role.id).empty?

        say "- for role #{step_role.id}"
        ProfilAuthorize.create!(profil_id: profil.id, meth_step_role_id: step_role.id)
      end
    end
  end

  def down
    say 'Do not delete anything (Profil Authorize)!'
    # raise ActiveRecord::IrreversibleMigration
  end
end
