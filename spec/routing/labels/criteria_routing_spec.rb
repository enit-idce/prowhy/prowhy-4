require 'rails_helper'

RSpec.describe Labels::CriteriaController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/labels/criteria').to route_to('labels/criteria#index')
    end

    it 'routes to #new' do
      expect(get: '/labels/criteria/new').to route_to('labels/criteria#new')
    end

    it 'routes to #show' do
      expect(get: '/labels/criteria/1').to route_to('labels/criteria#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/labels/criteria/1/edit').to route_to('labels/criteria#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/labels/criteria').to route_to('labels/criteria#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/labels/criteria/1').to route_to('labels/criteria#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/labels/criteria/1').to route_to('labels/criteria#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/labels/criteria/1').to route_to('labels/criteria#destroy', id: '1')
    end
  end
end
