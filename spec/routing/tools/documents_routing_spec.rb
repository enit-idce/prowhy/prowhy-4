require 'rails_helper'

RSpec.describe Tools::DocumentsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/tools/documents').to route_to('tools/documents#index')
    end

    it 'routes to #new' do
      expect(get: '/tools/documents/new').to route_to('tools/documents#new')
    end

    it 'routes to #show' do
      expect(get: '/tools/documents/1').to route_to('tools/documents#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/tools/documents/1/edit').to route_to('tools/documents#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/tools/documents').to route_to('tools/documents#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/tools/documents/1').to route_to('tools/documents#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/tools/documents/1').to route_to('tools/documents#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/tools/documents/1').to route_to('tools/documents#destroy', id: '1')
    end
  end
end
