require_dependency 'meth'
require_dependency 'meth/custom_field_desc'

module Meth
  # class CustomTypeSelectValidator < ActiveModel::Validator
  #   def validate(record)
  #     unless record.custom_field_desc.custom_field_type.field_internal == 'select'
  #       record.errors[:custom_field_desc] << 'Need a custom field type select (=list)!'
  #     end
  #   end
  # end

  class ListContent < ApplicationRecord
    include MethConcern

    translates :name, fallback: :any

    belongs_to :custom_field_desc, -> { includes(:custom_field_type).where(meth_custom_field_types: {field_internal: 'select'}) }

    validates :name, presence: true
    # validates_with CustomTypeSelectValidator
    validate :validate_type_is_select

    def validate_type_is_select
      return unless custom_field_desc.custom_field_type.field_internal != 'select'

      errors.add(:custom_field_desc, 'Need a custom field type select (=list)!')
    end

    def reference_show
      return '' unless custom_field_desc&.ref_field && ref_value && ref_value != 0

      return "#{custom_field_desc.ref_field.name} / #{Meth::ListContent.find(ref_value).name}"
    end

    def reference_show_field
      return '' unless custom_field_desc&.ref_field

      return custom_field_desc.ref_field.name
    end

    def reference_collection
      # return nil unless custom_field_desc&.ref_field&.custom_field_type&.field_internal == 'select'

      return custom_field_desc.reference_collection # .ref_field.list_contents&.map { |lc| [lc.id, lc.name] }
    end

    # Finalize => has_many CustomFields :
    # Permet d'ajouter le dependent: nullify pour supprimer les infos si on destroy un ListContent
    extend Finalize

    def self.finalize
      return unless database_exists? # Obligatoire sinon db:create ne fonctionne plus...

      custom_fields_selects = Meth::CustomFieldDesc.includes(:custom_field_type).where(meth_custom_field_types: {field_internal: 'select'})

      class_eval do
        custom_fields_selects.each do |cf|
          next unless CustomField.column_names.include?(cf.internal_name)

          has_many "#{cf.internal_name}s".intern, class_name: 'CustomField', foreign_key: cf.internal_name, dependent: :nullify
        end
      end
    rescue StandardError
      puts 'ListContent : db not initialized => not ok to finalize.'.blue
      # binding.pry
    end

    # private_class_method

    def self.database_exists?
      ActiveRecord::Base.connection
    rescue ActiveRecord::NoDatabaseError
      false
    else
      true
    end
  end
end
