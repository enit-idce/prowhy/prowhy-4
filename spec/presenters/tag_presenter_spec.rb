require 'rails_helper'

RSpec.describe TagPresenter, type: 'presenter' do
  subject(:tag_presenter) { TagPresenter.new(tag, view) }

  let(:view) { ActionController::Base.new.view_context }
  let(:tag) { Tag.create!(name: 'Tag 1') }

  # before :all do
  #   I18n.locale = 'fr'
  # end

  describe '#h from base presenter' do
    it 'return the view object' do
      expect(tag_presenter.h).to eq view
    end
  end

  describe '#tag_tree_line' do
    it 'returns one tag tree line text' do
      tag.name = 'Le nom Du Tag'
      tag.save!
      result = tag_presenter.tag_tree_line

      expect(result).to include('le nom du tag')

      expect(result).to include("data-fct-dnd='/en/tags/#{tag.id}'")
    end
  end

  describe '#tag_tree_infos' do
    it 'returns json infos for one tree tag' do
      result = tag_presenter.tag_tree_infos
      # binding.pry
      expect(result[:text]).to eq(tag_presenter.tag_tree_line)
      expect(result[:id]).to eq tag.id
      expect(result[:type]).to eq 'tag'
    end
  end

  describe '#tag_subtree_datas' do
    before :each do
      @tag1 = Tag.create!(name: 'Tag 2', parent: tag)
      @tag2 = Tag.create!(name: 'Tag 3', parent: tag)
    end

    it 'returns json infos for tag_subtree_datas' do
      result = tag_presenter.tag_subtree_datas

      expect(result[:text]).to eq(tag_presenter.tag_tree_line)
      expect(result[:children].size).to eq 2
      expect(result[:children][0][:text]).to include('tag 2')
      expect(result[:children][1][:text]).to include('tag 3')
    end
  end
end
