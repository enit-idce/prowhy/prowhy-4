require 'rails_helper'

def modal_visible(my_id)
  wait_until { find("##{my_id}").visible? }
rescue Capybara::TimeoutError
  flunk 'Expected modal to be visible.'
end


RSpec.feature 'Open user page', type: :feature do
  before :all do
    load "#{Rails.root}/db/seed_tests.rb"
    @methodology = Meth::Methodology.rca_methodologies.where(name_fr: 'Méthode 8D').first


    @user2 = FactoryBot.create('user')
    @user3 = FactoryBot.create('user')
    @user4 = FactoryBot.create('user')

    @user = User.first
    @user.save!
  end

  before :each do
    sign_in @user
    # Create first rca
    @rca = Rca.create!(reference: "0001", title: 'Mon premier Rca', methodology: @methodology)
    @rca.rca_creation_init(@user)
    # Add elements
    @step_tool = @methodology.steps_menu.first.step_tools.first
    cause_tree = @rca.find_or_create_tool('tools_cause_trees', 'Tools::CauseTree', @step_tool)
    @task1 = FactoryBot.create(:task, rca_id: @rca.id, task_detail_type: 'Tools::TaskCorrective', tools_cause_id: cause_tree.cause_link.id,
                             responsible_action_id: @user.id, responsible_rca_id: @user.id)
    @task1b = FactoryBot.create(:task, rca_id: @rca.id, task_detail_type: 'Tools::TaskCorrective', tools_cause_id: cause_tree.cause_link.id,
                             responsible_action_id: @user2.id, responsible_rca_id: @user.id)
    FactoryBot.create(:task, rca_id: @rca.id, task_detail_type: 'Tools::TaskPreventive', tools_cause_id: cause_tree.cause_link.id,
                             responsible_action_id: @user3.id, responsible_rca_id: @user.id)
    FactoryBot.create(:task, rca_id: @rca.id, task_detail_type: 'Tools::TaskPreventive', tools_cause_id: cause_tree.cause_link.id,
                             responsible_action_id: @user4.id, responsible_rca_id: @user.id)

    # Create second rca
    @rca2 = Rca.create!(reference: "0002", title: 'Mon deuxième Rca', methodology: @methodology)
    @rca2.rca_creation_init(@user2)

    # Add elements
    cause_tree_rca2 = @rca2.find_or_create_tool('tools_cause_trees', 'Tools::CauseTree', @step_tool)
    @task2 = FactoryBot.create(:task, rca_id: @rca2.id, task_detail_type: 'Tools::TaskCorrective', tools_cause_id: cause_tree_rca2.cause_link.id,
                             responsible_action_id: @user.id, responsible_rca_id: @user2.id)
    FactoryBot.create(:task, rca_id: @rca2.id, task_detail_type: 'Tools::TaskCorrective', tools_cause_id: cause_tree_rca2.cause_link.id,
                             responsible_action_id: @user2.id, responsible_rca_id: @user2.id)
    FactoryBot.create(:task, rca_id: @rca2.id, task_detail_type: 'Tools::TaskCorrective', tools_cause_id: cause_tree_rca2.cause_link.id,
                             responsible_action_id: @user3.id, responsible_rca_id: @user2.id)
  end

  after :all do
    load "#{Rails.root}/db/seed_empty.rb"
  end

  # GLOBAL Action Plan : chack actions index / check edit & delete action / check return ok
  scenario 'Open global task plan page', js: true do
    visit '/rcas'

    button = find('button', text: 'Action Plan')
    button.click
    expect(page).to have_current_path(tools_tasks_index_posts_path(step_tool: 2))

    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count: 11) # 7+4 # 6# nb tasks + 3 (title, search, 2X'action type')
    end
    # save_and_open_page

    # Clic to edit Task1
    click_link(href: "/en/tools/tasks/#{@task1.id}/edit?efficacity=show&matrix=show")
    modal_visible("edit_tools_task_#{@task1.id}")
    fill_in 'Title', with: 'New RCA Name-v1'
    button = find_button(value: 'Update Action')
    button.click
    # sleep(2)

    # Check si le retour est correct : la bonne liste d'actions, avec la mise à jour.
    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count:11)
      expect(page).to have_text('New RCA Name-v1')
    end

    # Delete Task1
    accept_confirm do
      click_link(href: "/en/tools/tasks/#{@task1.id}")
    end

    # Check si le retour est correct : la bonne liste d'actions, avec la mise à jour du delete
    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count:10)
      expect(page).to_not have_text('New RCA Name-v1')
    end

    # save_and_open_page
  end

  # USER Action Plan : chack actions index / check edit & delete action / check return ok
  scenario 'Open User (@user) task plan page', js: true do
    visit '/users/edit'

    button = find('a', text: 'My action plan')
    button.click
    expect(page).to have_current_path(tools_tasks_path(step_tool: 1))

    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count:9) # 5+4 nb tasks + 3 (title, search, 2X'action type')
    end

    # Clic to edit Task2
    click_link(href: "/en/tools/tasks/#{@task2.id}/edit?efficacity=show&matrix=show")
    modal_visible("edit_tools_task_#{@task2.id}")
    fill_in 'Title', with: 'New RCA Name-v2'
    button = find_button(value: 'Update Action')
    button.click
    # sleep(2)

    # Check si le retour est correct : la bonne liste d'actions, avec la mise à jour.
    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count:9)
      expect(page).to have_text('New RCA Name-v2')
    end


    # Delete Task2
    accept_confirm do
      click_link(href: "/en/tools/tasks/#{@task2.id}")
    end

    # Check si le retour est correct : la bonne liste d'actions, avec la mise à jour du delete
    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count:8)
      expect(page).to_not have_text('New RCA Name-v2')
    end
    # save_and_open_page
  end

  # RCA
  scenario 'Open RCA Action plan', js: true do
    visit '/rcas'

    # Open RCA
    find("a[href='#{edit_rca_path(@rca)}']").click
    expect(page).to have_current_path(edit_rca_path(@rca))

    # Open RCA Action Plan
    # sleep(1)
    button = find("a[title='Action Plan']")
    button.click

    # sleep(1)
    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count:8) # 4+2+1+1 nb tasks + 3 (title, search, 2X'action type')
    end

    # Clic to edit Task1
    click_link(href: "/en/tools/tasks/#{@task1.id}/edit?efficacity=show&matrix=show")
    modal_visible("edit_tools_task_#{@task1.id}")
    fill_in 'Title', with: 'New RCA Name-FIRST'
    button = find_button(value: 'Update Action')
    button.click

    # Check si le retour est correct : la bonne liste d'actions, avec la mise à jour.
    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count:8)
      expect(page).to have_text('New RCA Name-FIRST')
    end

    # Delete Task1
    accept_confirm do
      click_link(href: "/en/tools/tasks/#{@task1.id}")
    end

    # Check si le retour est correct : la bonne liste d'actions, avec la mise à jour du delete
    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count:7)
      expect(page).to_not have_text('New RCA Name-FIRST')
    end

    # Open Menu actions Corrective
    button = find("a[id='header-step-25']")
    button.click
    # sleep(1)
    button = find("a[title='PA Corrective']")
    button.click

    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count:4) # 1+3 nb tasks + 3 (title, search, 'action type')
      expect(page).to have_text(@task1b.name)
    end

    # sleep(1)
    # Clic to edit Task1B
    click_link(href: "/en/tools/tasks/#{@task1b.id}/edit?efficacity=true&matrix=true")
    modal_visible("edit_tools_task_#{@task1b.id}")
    fill_in 'Title', with: 'LAST MODIFICATION'
    button = find_button(value: 'Update Action')
    button.click

    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count:4) # 1+3 nb tasks + 3 (title, search, 'action type')
      expect(page).to have_text('LAST MODIFICATION')
    end

    # sleep(1)
    # save_and_open_page
  end

  # RCA-LIST PA
  scenario 'Open RCA-LIST Action plan', js: true do
    # Create more datas
    # Create rca3 with one action
    @rca3 = Rca.create!(reference: "0003", title: 'Mon troisième Rca', methodology: @methodology)
    @rca3.rca_creation_init(@user3)
    cause_tree_rca3 = @rca3.find_or_create_tool('tools_cause_trees', 'Tools::CauseTree', @step_tool)
    @task_rca3 = FactoryBot.create(:task, rca_id: @rca3.id, task_detail_type: 'Tools::TaskCorrective', tools_cause_id: cause_tree_rca3.cause_link.id,
                                          responsible_action_id: @user.id, responsible_rca_id: @user3.id)

    @rca2.set_rca_parent(@rca)
    @rca3.set_rca_parent(@rca)

    # Go
    visit '/rcas'

    # Open RCA
    find("a[href='#{edit_rca_path(@rca)}']").click
    expect(page).to have_current_path(edit_rca_path(@rca))

    # Open RCA-LIST Action Plan
    # sleep(1)
    button = find("a[id='header-step-29']") # AUDIT Menu
    button.click
    # sleep(1)
    button = find("a[title='AUDIT PA']")
    button.click


    # sleep(1)
    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count:12) # 8+2+1+1 nb tasks + 3 (title, search, 2X'action type')
      expect(page).to have_text(@rca.reference)
      expect(page).to have_text(@rca2.reference)
      expect(page).to have_text(@rca3.reference)
    end

    # sleep(1)
    # save_and_open_page


    # Clic to edit Task1
    click_link(href: "/en/tools/tasks/#{@task1.id}/edit?efficacity=show&matrix=show")
    modal_visible("edit_tools_task_#{@task1.id}")
    fill_in 'Title', with: 'TASK RCA1 MODIFIED'
    button = find_button(value: 'Update Action')
    button.click

    # Check si le retour est correct : la bonne liste d'actions, avec la mise à jour.
    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count:12)
      expect(page).to have_text('TASK RCA1 MODIFIED')
    end

    # Delete Task1
    accept_confirm do
      click_link(href: "/en/tools/tasks/#{@task1.id}")
    end

    # Check si le retour est correct : la bonne liste d'actions, avec la mise à jour du delete
    within('table#table-tasks') do
      expect(page).to have_xpath('.//tr', count:11)
      expect(page).to_not have_text('TASK RCA1 MODIFIED')
    end

    # PAS DE LIEN Edit et Delete vers les actions des rca2 et rca3
    within('table#table-tasks') do
      expect(page).to_not have_link(href: "/en/tools/tasks/#{@task2.id}")
      expect(page).to_not have_link(href: "/en/tools/tasks/#{@task_rca3.id}")
    end
  end
end
