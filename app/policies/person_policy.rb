class PersonPolicy < AdminPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def edit?
    user.has_any_role?(:admin, :super_admin, :manager) || user.person == record
  end

  def update?
    user.has_any_role?(:admin, :super_admin, :manager) || user.person == record
  end

  def person_choose?
    true
  end

  def person_choose_valid?
    person_choose?
  end

  def destroy?
    index? && (record.user.nil? || UserPolicy.new(user, record.user).destroy?)
  end
end
