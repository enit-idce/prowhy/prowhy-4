module Tools
  class TasksController < ApplicationController
    include ProcessConcern
    before_action :set_tools_task, only: [:show, :edit, :update, :destroy, :update_advance, :reschedule]
    before_action :init_rca, only: [:index, :index_js, :new, :create, :export_pdf, :check_list, :check_list_valid]

    # TODO: REFACTOR index et index_js !!! => déplacer dans des services
    def index
      # authorize Task
      skip_authorization
      # authorize Task, :index_user?

      # IF Global Action Plan (step_tool in params / and rcas list in params if rcas) => initialisation
      # Tools::Tasks::TaskPlanGlobal.new(current_user, params[:step_tool]).init_rcas(params) if params[:step_tool] # if params[:rcas]
      Tools::Tasks::TaskPlanGlobal.new(current_user, params[:step_tool]).init_rcas(params) if params[:step_tool] && params[:rcas]
      # Indispensable pour en cas de "Back" après avoir ouvert un RCA
      # Sinon le current step tool est sur le RCA
      current_user.set_session(:active_step_tool, params[:step_tool]) if params[:step_tool]
      # binding.pry

      if current_user.get_session(:active_step_tool).nil?
        redirect_to rcas_path
      else
        @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
        @step_tool.tool_options ||= {}

        task_plan_options_service = Tools::Tasks::TaskPlanOptions.new(@step_tool)

        task_options = task_plan_options_service.init_task_options
        plan_options = task_plan_options_service.init_plan_options(task_options)

        # Si type_plan = user OR global, reset rca (when return from update/delete/...)
        if plan_options[:type_plan] != 'rca' && plan_options[:sub_type_plan] != 'rca-list' # == user ou global
          params[:rca] = nil
          @rca = nil
        end
        # binding.pry

        @task_columns = current_user.columns_options(task_options[:task_detail_type]) || {}

        # TEST POUR PLAN ACTION AUDIT - rca-list
        if plan_options[:sub_type_plan] == 'rca-list' && @rca
          rcas_ids = [@rca.id]
          tools_rca_lists = if @step_tool.tool_reference == 0
                              @rca.tools_rca_lists.includes(:rcas)
                            else
                              @rca.tools_rca_lists.where(tool_reference: @step_tool.tool_reference).includes(:rcas)
                            end

          tools_rca_lists.each do |tool_rca_l|
            tool_rca_l.rcas.each do |rca|
              rcas_ids << rca.id
            end
          end
          current_user.set_session(:plan_rcas_ids, rcas_ids)
          # plan_options[:type_plan] = 'global'
          # binding.pry
        end

        # il faut annuler le rca si mode plan action user ou global (pour retour de update action)
        # @rca = nil unless plan_options[:type_plan] == 'rca'

        if @rca
          authorize @rca, :show?, policy_class: Tools::ToolsPolicy
        elsif plan_options[:type_plan] == 'global'
          authorize Task, :index_global?
        else
          authorize Task, :index_user?
        end

        @set_check_box_email = authorize_send_emails?

        respond_to do |format|
          format.html do
            render :index,
                   # layout: plan_options[:type_plan] == 'user' ? 'user_page' : (plan_options[:type_plan] == 'global' ? 'application' : 'processus'),
                   layout: plan_options[:type_plan] == 'user' ? 'user_page' : (plan_options[:type_plan] == 'global' && plan_options[:sub_type_plan] != 'rca-list' ? 'application' : 'processus'),
                   locals: { plan_options: plan_options, task_types_options: (task_options[:task_detail_type].class == Array && task_options[:task_detail_type].size > 1) }
          end
          format.pdf do
            @export_pdf = true

            render pdf: 'report_pdf', # Excluding ".pdf" extension.
                   layout: 'layouts/pdf.html.erb',
                   # page_size: 'A4',
                   template: 'tools/tasks/index.html.erb',
                   locals: { plan_options: plan_options, task_types_options: (task_options[:task_detail_type].class == Array && task_options[:task_detail_type].size > 1) },
                   show_as_html: false,
                   encoding: 'utf8'
          end
        end
      end
    end

    def index_js
      # authorize Task
      # authorize @rca, :show?, policy_class: Tools::ToolsPolicy
      authorize Task, :index_user?

      @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
      @step_tool.tool_options ||= {}
      task_options = @step_tool.tool_options[:task_options] || {}
      # plan_options = @step_tool.tool_options[:plan_options] || {}

      # params[:tools_task] = if @step_tool.tool_reference != 0
      #                         task_options.merge!(tool_reference: @step_tool.tool_reference,
      #                                             tool_num: @step_tool.tool_num,
      #                                             ancestry: nil)
      #                       else
      #                         task_options.merge!(ancestry: nil)
      #                       end
      params[:tools_task] = if @step_tool.tool_reference != 0
                              task_options.merge!(tool_reference: @step_tool.tool_reference,
                                                  tool_num: @step_tool.tool_num)
                            # else
                            #   task_options.merge!(ancestry: nil)
                            end

      # assigns plan options from users params
      plan_options = params[:user_plan_options]
      plan_options.each_key do |k|
        plan_options[k] = true if plan_options[k] == 'true'
        plan_options[k] = false if plan_options[k] == 'false'
      end

      # Initialise rcas_ids if Global Action Plan => liste des racs_ids créée dans le service dans la fonction index
      begin
        rcas = current_user.get_session(:plan_rcas_ids)
      rescue StandardError
        rcas = nil
      end
      @rcas_ids = rcas

      # Pour export PDF
      # TODO: Trouver une meilleure solution
      session[:plan_options] = plan_options
      session[:task_options] = task_options # tools_task_params

      @task_columns_type = task_options[:task_detail_type].class == Array ? 'TaskGlobal' : task_options[:task_detail_type]
      @task_columns = current_user.columns_options(task_options[:task_detail_type])
      @tools_tasks = Tools::Tasks::TaskPlanGetTasks.new(@rca, @rcas_ids, current_user).init_tasks(plan_options, task_options) # tools_task_params)

      # binding.pry
      # Pagy : pagination
      # if @tools_tasks != []
      #   @pagy, @tools_tasks = pagy(@tools_tasks, link_extra: 'data-remote="true"',
      #                                            items: 50,
      #                                            steps: { 0 => [1, 2, 2, 1],
      #                                                     540 => [2, 3, 3, 2],
      #                                                     720 => [3, 5, 5, 3] })
      # end

      @set_check_box_email = authorize_send_emails?

      # binding.pry
      respond_to do |format|
        format.js do
          render :index,
                 locals: { plan_options: plan_options, task_options: task_options, # tools_task_params,
                           show_buttons: true }
        end
      end
    end

    # def export_pdf
    #   authorize Task, :index?

    #   @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
    #   plan_options = session[:plan_options].deep_symbolize_keys
    #   task_options_params = session[:task_options].deep_symbolize_keys

    #   name_pdf = 'public/pdf/test.pdf'
    #   # url1 = url_for(only_path: false)
    #   # url2 = params[:controller]+'/'+params[:action]
    #   # @local_host_value = url1.gsub(url2, '')

    #   # html_output = render_to_string(template: 'tools/tasks/index',
    #   #                                locals: { plan_options: plan_options, task_types_options: task_options[:task_detail_type].class == Array },
    #   #                                layout: 'export_pdf_test')
    #   html_output = render_to_string(template: 'tools/tasks/export_pdf.html.erb',
    #                                  locals: { plan_options: plan_options, task_options: task_options_params, show_buttons: false },
    #                                  layout: 'export_pdf_test')

    #   command = "wkhtmltopdf --load-error-handling skip --encoding utf-8 - #{name_pdf}"

    #   IO.popen(command, 'r+') do |f|
    #     # Writing the html previously rendered in a string
    #     f.write(html_output) # Ecriture de la sortie de la commande dans le fichier name_pdf (dans public/pdf/user)
    #     f.close_write
    #   end

    #   send_data File.read(name_pdf), filename: 'test.pdf', type: 'application/pdf', disposition: 'attachment'
    # end

    def export_pdf
      authorize Task, :index?

      @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
      plan_options = session[:plan_options].deep_symbolize_keys
      tools_task_params = session[:task_options].deep_symbolize_keys

      @task_columns_type = if @step_tool&.tool_options&.[](:task_detail_type)
                             @step_tool.tool_options[:task_detail_type].class == Array ? 'TaskGlobal' : @step_tool.tool_options[:task_detail_type]
                           else
                             'TaskGlobal'
                           end
      @task_columns = current_user.columns_options(@task_columns_type)
      # binding.pry

      begin
        rcas = current_user.get_session(:plan_rcas_ids)
      rescue StandardError
        rcas = nil
      end
      @rcas_ids = rcas

      @tools_tasks = Tools::Tasks::TaskPlanGetTasks.new(@rca, @rcas_ids, current_user).init_tasks(plan_options, tools_task_params)

      # Initialise rcas_ids if Global Action Plan => liste des racs_ids créée dans le service dans la fonction index
      begin
        rcas = current_user.get_session(:plan_rcas_ids)
      rescue StandardError
        rcas = nil
      end
      @rcas_ids = rcas

      respond_to do |format|
        format.pdf do
          @export_pdf = true

          render pdf: 'report_pdf', # Excluding ".pdf" extension.
                 layout: 'layouts/pdf.html.erb',
                 # page_size: 'A4',
                 template: 'tools/tasks/export_pdf.html.erb',
                 locals: { plan_options: plan_options, task_options: tools_task_params, show_buttons: false },
                 show_as_html: false,
                 encoding: 'utf8'
        end
      end
    end

    # GET /tools/tasks/1
    # GET /tools/tasks/1.json
    def show
    end

    # GET /tools/tasks/new
    # TODO : create service for new Task
    def new
      if @rca.nil? || current_user.get_session(:active_step_tool).nil?
        authorize Task
        redirect_to rcas_path
      else
        @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))

        @tools_task = Tools::Task.new(tools_task_params)
        @tools_task.created_at = Date.today

        authorize @tools_task

        # Build TaskDetail if possible
        if params[:tools_task][:task_detail_type].class == String
          @tools_task.build_task_detail(nil)
          # @tools_task.task_detail = params[:tools_task][:task_detail_type]&.safe_constantize&.new
          # Si pas Build => on enregistre le type dans task_detail_type (dans ce cas il n'y a pas d'objet corespondant)
          @tools_task.task_detail_type = params[:tools_task][:task_detail_type] unless @tools_task.task_detail
        end

        # Build Matrix status
        @tools_task.build_matrix_stat if params[:matrix] == 'true'

        # Build Efficacity
        @tools_task.build_efficacity if params[:efficacity] == 'true'

        @step_tool.tool_reference = 1 if @step_tool.tool_reference == 0
        @step_tool.tool_num = 1
        @tools_cause = @rca.find_or_create_tool('tools_causes', 'Tools::Cause', @step_tool)

        @tools_task.tools_cause = @tools_cause if @tools_task.tools_cause.nil?
        # binding.pry
        @task_detail_types = params[:tools_task][:task_detail_type] if params[:tools_task][:task_detail_type].class == Array
        @modal_title = "Action : #{t(@tools_task.task_detail_type, scope: [:simple_form, :options, :tools_task, :task_detail_type])}" if @tools_task.task_detail_type

        # binding.pry
        step_tool_form_options
      end
    end

    # GET /tools/tasks/1/edit
    def edit
      # Build Efficacity
      @tools_task.build_matrix_stat if params[:matrix] == 'true' && @tools_task.matrix_stat.nil?

      # Build Efficacity
      @tools_task.build_efficacity if params[:efficacity] == 'true' && @tools_task.efficacity.nil?

      @modal_title = "Action : #{t(@tools_task.task_detail_type, scope: [:simple_form, :options, :tools_task, :task_detail_type])}" if @tools_task.task_detail_type

      step_tool_form_options

      render :new
    end

    # POST /tools/tasks
    # POST /tools/tasks.json
    def create
      @tools_task = Tools::Task.new(tools_task_params)

      # Build TaskDetail if possible => DOIT ETRE AJOUTE ICI CAR SINON PB EN MODE CREATION / PA GLOBAL.
      if @tools_task.task_detail_type && !@tools_task.task_detail
        @tools_task.build_task_detail(nil)
      end

      authorize @tools_task

      @tools_task.tool_reference = @tools_task.tools_cause.tool_reference if @tools_task.tool_reference == 0
      @tools_task.tool_num = @tools_task.tools_cause.tool_num if @tools_task.tool_num == 0

      @step_tool = Meth::StepTool.find(current_user.get_session(:active_step_tool))
      # @tools_task.responsible_rca = current_user unless @tools_task.responsible_rca
      # @tools_task.responsible_action = current_user unless @tools_task.responsible_action

      respond_to do |format|
        if @tools_task.save
          @tools_task.propagate_advancement
          format.html do
            redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca),
                        notice: t('.message_succes', task: Tools::Task.model_name.human)
          end
          format.json { render :show, status: :created, location: @tools_task }
        else
          format.html { render :new }
          format.json { render json: @tools_task.errors, status: :unprocessable_entity }
          format.js { render :new }
        end
      end
    end

    # PATCH/PUT /tools/tasks/1
    # PATCH/PUT /tools/tasks/1.json
    def update
      # binding.pry
      old_cause = @tools_task.tools_cause

      # Reschedule
      @tools_task.reschedule_date_due(params[:reschedule_comment]) if params['custom_action'] == 'reschedule'

      respond_to do |format|
        if @tools_task.update(tools_task_params)

          if params['custom_action'] == 'reschedule'
            format.js { render :reschedule_do }
          else
            @tools_task.update_date_completion
            @tools_task.propagate_advancement
            @tools_task.update_old_cause(old_cause) if old_cause != @tools_task.tools_cause
            format.html do
              # binding.pry
              redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca),
                          notice: t('.message_succes', task: Tools::Task.model_name.human)
            end
            format.json { render :show, status: :ok, location: @tools_task }
          end
        else
          format.html { render :edit }
          format.json { render json: @tools_task.errors, status: :unprocessable_entity }
          format.js { render :new }
        end
      end
    end

    # DELETE /tools/tasks/1
    # DELETE /tools/tasks/1.json
    def destroy
      parent = @tools_task.parent
      @tools_task.destroy

      parent&.children&.first&.propagate_advancement
      respond_to do |format|
        # format.html { redirect_to tools_tasks_url(rca: @rca), notice: t('.message_succes', task: Tools::Task.model_name.human) }
        format.html do
          redirect_to meth_step_tool_show_path(id: current_user.get_session(:active_step_tool), rca: @rca),
                      notice: t('.message_succes', task: Tools::Task.model_name.human)
        end
        format.json { head :no_content }
      end
    end

    def task_get_cause
      authorize Task, :show?

      data = {}
      data[:cause_id] = params[:id]=='' ? 0 : Tools::Task.find(params[:id]).tools_cause_id
      respond_to do |format|
        format.json { render json: data }
      end
    end

    def update_advance
      # @tools_task.update(tools_task_params)
      @tools_task.advancement = tools_task_params[:advancement]
      # @tools_task.propagate_advancement

      respond_to do |format|
        format.html { redirect_to tools_tasks_url(rca: @rca) }
        format.js { render 'update_advance', locals: {task: @tools_task} }
        # format.js { render :new }
      end
    end

    def send_emails
      # authorize Task
      skip_authorization

      raise StandardError, 'No action selected' if params[:task_email].nil? || params[:task_email].empty?

      params[:task_email].each_key do |task_id|
        SendMails.task_send_email(task_id, rca: false, action: true) # task_email.deliver_now
      end

      respond_to do |format|
        format.js { render 'shared/email_response', locals: {message: 'Success : Email(s) sent.', color: '#282'} }
      end
    rescue StandardError => e
      respond_to do |format|
        puts 'ERROR'.red
        puts e
        puts 'OK'.red
        format.js { render 'shared/email_response', locals: {message: "Error : #{e.to_s.gsub(/\R+/, ' ')}.", color: '#922'} }
      end
    end

    # GET /tools/tasks/check_list
    def check_list
      authorize Task

      @step_tool = Meth::StepTool.find(params[:id])
      @check_list_elems = Labels::CheckListElem.where(task_detail_type: @step_tool.tool_options[:task_options][:task_detail_type])
      # binding.pry

      respond_to do |format|
        format.js { render 'check_list' }
      end
    end

    # POST /tools/tasks/check_list_valid
    def check_list_valid
      authorize Task

      @step_tool = Meth::StepTool.find(params[:step_tool_id])
      tools_cause = @rca.find_or_create_tool('tools_causes', 'Tools::Cause', @step_tool)

      params[:check_list_elem].each_key do |key|
        check_list_elem = Labels::CheckListElem.find(key.to_i)

        @tools_task = Tools::Task.create!(name: check_list_elem.name,
                                          task_detail_type: check_list_elem.task_detail_type,
                                          rca: @rca,
                                          tool_reference: @step_tool.tool_reference,
                                          tool_num: @step_tool.tool_num,
                                          tools_cause: tools_cause,
                                          created_at: Date.today,
                                          responsible_rca: @rca.rca_first_responsible,
                                          responsible_action: current_user.person,
                                          check_list_elem: check_list_elem)

        # Build TaskDetail if possible => DOIT ETRE AJOUTE ICI CAR SINON PB EN MODE CREATION / PA GLOBAL.
        @tools_task.build_task_detail(nil) if @tools_task.task_detail_type && !@tools_task.task_detail
      end

      respond_to do |format|
        format.html { redirect_to meth_step_tool_show_path(id: @step_tool.id, rca: @rca) }
      end
    end

    def reschedule
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_tools_task
      @tools_task = Tools::Task.find(params[:id])
      @rca = @tools_task.rca

      # authorize @rca, :show?
      authorize @tools_task
    end

    def authorize_send_emails?
      return policy(@rca).send_emails? if @rca

      return policy(Task).send_emails?
    end

    # Only allow a list of trusted parameters through.
    def tools_task_params
      params.require(:tools_task).permit(:rca_id, :tool_reference, :tool_num,
                                         :name, :tools_cause_id, :advancement,
                                         :responsible_action_id, :responsible_rca_id,
                                         :result, :priority, :date_due, :date_completion, :created_at,
                                         :task_detail_id, :task_detail_type, :parent_id, :ancestry,
                                         task_detail_attributes: [:id, :status],
                                         efficacity_attributes: Tools::Task.list_efficacity_attributes,
                                         matrix_stat_attributes: Tools::Task.list_matrix_attributes)
    end

    def step_tool_form_options
      step_tool = current_user.get_session(:active_step_tool) ? Meth::StepTool.find(current_user.get_session(:active_step_tool)) : nil
      @form_options = {efficacity: true}

      return unless step_tool&.tool_options

      step_tool.tool_options[:form_options]&.each_key do |key|
        @form_options[key.intern] = step_tool.tool_options[:form_options][key]
      end
    end
  end
end
