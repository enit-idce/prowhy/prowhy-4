require 'rails_helper'

RSpec.describe Tools::Task, type: :model do
  subject(:task) do
    FactoryBot.create(:task, id: 1, rca: rca, name: 'Task 1', tools_cause: cause,
                             responsible_action: person, responsible_rca: person,
                             advancement: 0)
  end

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
  let(:cause) { Tools::Cause.create!(rca: rca, tool_reference: 1, name: 'Cause 1') }
  let(:person) { FactoryBot.create(:person) }
  let(:user) { FactoryBot.create(:user, person_id: person.id) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to tools_cause' do
        assc = Tools::Task.reflect_on_association(:tools_cause)
        expect(assc.macro).to eq :belongs_to
      end

      it 'belongs to responsible_action' do
        assc = Tools::Task.reflect_on_association(:responsible_action)
        expect(assc.macro).to eq :belongs_to
      end

      it 'belongs to responsible_rca' do
        assc = Tools::Task.reflect_on_association(:responsible_rca)
        expect(assc.macro).to eq :belongs_to
      end

      it 'belongs to task_detail' do
        assc = Tools::Task.reflect_on_association(:task_detail)
        expect(assc.macro).to eq :belongs_to
      end

      it 'has many tools_reschedules' do
        assc = Tools::Task.reflect_on_association(:tools_reschedules)
        expect(assc.macro).to eq :has_many
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        task.name = ''
        expect(task).to_not be_valid
      end

      it 'is valid with name' do
        task.name = 'La task'
        expect(task).to be_valid
      end
    end
  end

  describe 'Create and search model : tool concern functions' do
    before :each do
      task.save!
      Tools::Task.create!([{id: 2, rca: rca, tool_reference: 1, name: 'Task 2', tools_cause: cause, task_detail_type: 'Corrective',
                            responsible_action: person, responsible_rca: person},
                           {id: 3, rca: rca, tool_reference: 2, name: 'Task 3', tools_cause: cause, task_detail_type: 'Corrective',
                            responsible_action: person, responsible_rca: person},
                           {id: 4, rca: rca, tool_reference: 1, name: 'Task 4', tools_cause: cause, task_detail_type: 'Preventive',
                            responsible_action: person, responsible_rca: person}])
    end

    it 'create_default' do
      expect(Tools::Task.create_default!({}, nil)).to eq []
    end

    it 'search_default without action type' do
      step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1)
      tasks_founded = Tools::Task.search_default(rca, 'tools_tasks', step_tool)

      expect(tasks_founded.length).to eq 3
      expect(tasks_founded.map(&:id)).to eq [1, 2, 4]

      step_tool.tool_reference = 2
      tasks_founded = Tools::Task.search_default(rca, 'tools_tasks', step_tool)

      expect(tasks_founded.length).to eq 1
      expect(tasks_founded.map(&:id)).to eq [3]
    end

    it 'search_default with action type' do
      step_tool = Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new,
                                     tool_options: {task_options: {task_detail_type: 'Corrective'}},
                                     tool_reference: 1)
      tasks_founded = Tools::Task.search_default(rca, 'tools_tasks', step_tool)

      expect(tasks_founded.length).to eq 1
      expect(tasks_founded.map(&:id)).to eq [2]

      step_tool.tool_reference = 2
      tasks_founded = Tools::Task.search_default(rca, 'tools_tasks', step_tool)

      expect(tasks_founded.length).to eq 1
      expect(tasks_founded.map(&:id)).to eq [3]
    end
  end

  describe 'advancement' do
    before :each do
      task.advancement = 4
      task.save!
      @task2 = FactoryBot.create(:task, id: 2, rca: rca, name: 'SousTask 1.1', tools_cause: cause,
                                        responsible_action: person, responsible_rca: person,
                                        advancement: 4, parent: task)
      @task3 = FactoryBot.create(:task, id: 3, rca: rca, name: 'SousTask 1.2', tools_cause: cause,
                                        responsible_action: person, responsible_rca: person,
                                        advancement: 0, parent: task)
      @task4 = FactoryBot.create(:task, id: 4, rca: rca, name: 'SousTask 1.1.1', tools_cause: cause,
                                        responsible_action: person, responsible_rca: person,
                                        advancement: 0, parent: @task2)
      @task5 = FactoryBot.create(:task, id: 5, rca: rca, name: 'SousTask 1.1.2', tools_cause: cause,
                                        responsible_action: person, responsible_rca: person,
                                        advancement: 0, parent: @task2)
    end

    describe 'propagate advancement' do
      it 'advance parent when advance child' do
        @task3.update(advancement: 2)
        # task.calculate_advancement
        @task3.propagate_advancement

        expect(task.reload.advancement).to eq 2
      end

      it 'advance parent and grand-parents when advance child' do
        @task3.update(advancement: 2)
        @task4.update(advancement: 1)
        @task5.update(advancement: 3)

        # task.calculate_advancement
        @task5.propagate_advancement

        expect(@task2.reload.advancement).to eq 1
        expect(task.reload.advancement).to eq 1
      end

      it 'set date_done if advancement is set to closed by childs' do
        @task3.update(advancement: 4)
        @task4.update(advancement: 4)
        @task5.update(advancement: 4)

        @task5.propagate_advancement

        expect(@task2.reload.date_completion).to eq Date.today
        expect(task.reload.date_completion).to eq Date.today

        # Revert avancement
        @task5.update(advancement: 3)
        @task5.propagate_advancement

        expect(@task2.reload.date_completion).to eq nil
        expect(task.reload.date_completion).to eq nil
      end
    end

    it 'returns started status of task' do
      @task3.advancement = 2
      expect(@task2.task_started?).to eq true
      expect(@task3.task_started?).to eq true
      expect(@task4.task_started?).to eq false
    end

    it 'returns finish status of task' do
      @task3.advancement = 2
      expect(@task2.task_finished?).to eq true
      expect(@task3.task_finished?).to eq false
      expect(@task4.task_finished?).to eq false
    end

    it 'returns laste status of task' do
      @task3.advancement = 2
      @task2.date_due = @task3.date_due = @task4.date_due = Date.yesterday
      @task2.date_completion = @task3.date_completion = @task4.date_completion = nil
      expect(@task2.task_late?).to eq false
      expect(@task3.task_late?).to eq true
      expect(@task4.task_late?).to eq true
    end

    it 'set completion date if task is finished, nil othercase' do
      @task3.advancement = 2
      @task3.date_completion = Date.yesterday
      @task2.update_date_completion
      @task3.update_date_completion
      @task4.update_date_completion

      expect(@task2.date_completion).to eq Date.today
      expect(@task3.date_completion).to eq nil
      expect(@task4.date_completion).to eq nil
    end
  end

  describe 'responsibles' do
    let(:person2) { FactoryBot.create(:person) }

    it 'returns one person if responsibles are same person' do
      # binding.pry
      expect(task.responsibles).to eq [person]
    end
    it 'returns two person if responsibles are differents' do
      task.responsible_rca = person2
      task.save!
      expect(task.responsibles).to contain_exactly(person, person2)
    end
  end
end
