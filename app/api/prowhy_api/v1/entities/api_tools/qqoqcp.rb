module ProwhyApi
  module V1
    module Entities
      module ApiTools
        class Qqoqcp < Grape::Entity
          expose :id
          expose :config_name, merge: true
          expose :tool_reference # , if: { with_reference: 'true' }
          expose :rca_id, if: { with_rca_id: 'true' }

          # expose :lines

          expose :qqoqcp_values, using: Entities::ApiTools::QqoqcpValue, as: :values

          private

          def config_name
            {
              config_name: object&.labels_qqoqcp_config&.name || 'no qqoqcp name'
            }
          end
        end
      end
    end
  end
end
