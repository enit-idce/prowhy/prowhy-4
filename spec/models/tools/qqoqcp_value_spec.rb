require 'rails_helper'

RSpec.describe Tools::QqoqcpValue, type: :model do
  subject(:qqoqcp_value) do
    Tools::QqoqcpValue.new
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to qqoqcp' do
        assc = Tools::QqoqcpValue.reflect_on_association(:qqoqcp)
        expect(assc.macro).to eq :belongs_to
      end
      it 'belongs to labels_qqoqcp_line' do
        assc = Tools::QqoqcpValue.reflect_on_association(:labels_qqoqcp_line)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end
end
