class CreateProfilAuthorizes < ActiveRecord::Migration[6.0]
  def change
    create_table :profil_authorizes do |t|
      t.references :profil, null: false, foreign_key: true
      t.references :meth_step_role, null: false, foreign_key: true
      t.boolean :read, default: true
      t.boolean :write, default: false
      t.boolean :advance, default: false
      t.boolean :email, default: false

      t.timestamps
    end
  end
end
