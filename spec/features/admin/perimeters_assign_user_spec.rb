require 'rails_helper'

RSpec.feature 'Perimeters CRUD', type: :feature do
  let(:user) { FactoryBot.create(:user) }
  # let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:perim_all) { FactoryBot.create(:perimeter, name: 'ALL') }
  let(:perimeter) { FactoryBot.create(:perimeter, name: 'Interne', parent_id: perim_all.id) }

  before :each do
    perim_all.reload
    perimeter.reload
    # user.add_perimeter perimeter.id
    # user.set_preference(:perimeter, perimeter.id)
    # user.reload
    user.save!
    sign_in user
  end

  scenario 'Assign a user to a perimeter', js: true do
    visit '/admin/perimeters_assign_users'
    expect(user.reload.perimeters.size).to eq 0

    find("a[id='#{perimeter.id}_anchor']").find('i').click
    click_button 'Validate'

    # save_and_open_page

    expect(user.reload.perimeters.map(&:id)).to contain_exactly(perim_all.id, perimeter.id)
    expect(page).to have_current_path(admin_perimeters_assign_users_path(attrib_user: user.id))
  rescue StandardError
    puts 'TIME OUT !!!'.red
  end

  scenario 'Assign a user to a perimeter with childs', js: true do
    peri_custo = FactoryBot.create(:perimeter, name: 'Customers', parent_id: perim_all.id)
    peri_custo1 = FactoryBot.create(:perimeter, name: 'Customer1', parent_id: peri_custo.id)
    peri_custo2 = FactoryBot.create(:perimeter, name: 'Customer2', parent_id: peri_custo.id)

    visit '/admin/perimeters_assign_users'
    expect(user.reload.perimeters.size).to eq 0

    # on coche custo => custo1 et custo2 cochés aussi
    find("a[id='#{peri_custo.id}_anchor']").find('i').click
    # find li element parent of custo to verify custo is checked
    le_li = page.find("li[id='#{peri_custo.id}']")
    expect(le_li).to have_css('.jstree-checked')
    click_button 'Validate'

    expect(page).to have_content('Scopes assigned to user.')
    expect(page).to have_current_path(admin_perimeters_assign_users_path(attrib_user: user.id))
    expect(user.reload.perimeters.map(&:id)).to contain_exactly(peri_custo.id, peri_custo1.id, peri_custo2.id)

    # On décoche custo2 => il reste custo1 seulement
    find("a[id='#{peri_custo2.id}_anchor']").find('i').click

    # find a to verify custo (i element) is undetermined type
    expect(find("a[id='#{peri_custo.id}_anchor']")).to have_css('.jstree-undetermined')
    click_button 'Validate'

    expect(page).to have_current_path(admin_perimeters_assign_users_path(attrib_user: user.id))
    expect(page).to have_content('Scopes assigned to user.')

    user.reload
    # puts 'PERIMETERS : '.red
    # puts user.perimeters
    # puts user.perimeters.map(&:id)
    # save_and_open_page
    # binding.pry

    expect(user.reload.perimeters.map(&:id)).to contain_exactly(peri_custo1.id)
  end
end
