class CreateToolsIndicatorMeasures < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_indicator_measures do |t|
      t.references :indicator, null: false, foreign_key: { to_table: :tools_indicators }
      t.string :name
      t.text :description
      t.integer :value
      t.integer :pos, default: 0

      t.timestamps
    end
  end
end
