class CreateToolsCauseClasses < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_cause_classes do |t|
      t.references :tools_cause, null: false, foreign_key: true
      t.references :tools_ishikawa, null: false, foreign_key: true
      t.references :labels_cause_type, null: false, foreign_key: true

      t.timestamps
    end
  end
end
