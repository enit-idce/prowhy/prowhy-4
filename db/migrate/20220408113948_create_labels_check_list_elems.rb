class CreateLabelsCheckListElems < ActiveRecord::Migration[6.1]
  def change
    create_table :labels_check_list_elems do |t|
      t.string :name
      t.string :task_detail_type
      t.boolean :usable

      t.timestamps
    end
  end
end
