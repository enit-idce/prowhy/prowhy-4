module ProwhyApi
  module V1
    module Entities
      class ActionText < Grape::Entity
        expose :body, as: :description
      end
    end
  end
end
