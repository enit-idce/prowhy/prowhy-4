FactoryBot.define do
  factory :tools_document, class: Tools::Document do
    name { 'Document 1' }
    tool_reference { 1 }
    tool_num { 1 }
    association :rca, factory: :rca
  end
end
