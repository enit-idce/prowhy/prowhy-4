class AddToolOptionsToTools < ActiveRecord::Migration[6.0]
  def change
    add_column :meth_tools, :tool_options, :string, after: :tool_name
  end
end
