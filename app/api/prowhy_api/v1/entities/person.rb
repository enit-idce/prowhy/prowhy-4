module ProwhyApi
  module V1
    module Entities
      class Person < Grape::Entity
        expose :id, if: { with_id: true}
        expose :firstname
        expose :lastname
        expose :email, if: { with_email: true }

        # expose :user, with: Entities::User, if: { with_user: 'true' }

        expose :user, if: { with_user: true }
        # expose :user do |person, options|
        #   Entities::User.represent(person.user, { with_person: 'false', with_id: 'true' }) if options[:with_user] == 'true'
        # end

        private

        def user
          Entities::User.represent(object.user, { with_person: false, with_id: true })
        end
      end
    end
  end
end
