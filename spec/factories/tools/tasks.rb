FactoryBot.define do
  factory :task, class: Tools::Task do
    # name { 'Task 1' }
    sequence(:name) { |n| "Task#{n}" }
    tool_reference { 1 }
    task_detail_type { 'TypeTruc' }
    association :tools_cause, factory: :cause
    association :responsible_action, factory: :person
    association :responsible_rca, factory: :person
  end
end
