FactoryBot.define do
  factory :qqoqcp, class: Tools::Qqoqcp do
    tool_reference { 1 }
  end
end
