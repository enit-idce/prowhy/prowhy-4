json.extract! custom_field, :id, :rca_id, :created_at, :updated_at
json.url custom_field_url(custom_field, format: :json)
