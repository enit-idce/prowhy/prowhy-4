# frozen_string_literal: true

module Meth
  module Positions
    class MoveService
      def initialize(obj, objs)
        @obj = obj
        @objs = objs.order(:pos)
      end

      def call(updown)
        if updown == 'up'
          move_up
        elsif updown == 'down'
          move_down
        else
          reinit_pos
        end
      end

      def move_down
        return if @obj.pos <= 1

        @objs.each_with_index do |obj, i|
          next if obj != @obj

          obj.pos -= 1
          obj.save!
          @objs[i-1].pos += 1
          @objs[i-1].save!
          break
        end
      end

      def move_up
        return if @obj.pos >= @objs.size

        @objs.each_with_index do |obj, i|
          next if obj != @obj

          obj.pos += 1
          obj.save!
          @objs[i+1].pos -= 1
          @objs[i+1].save!
          break
        end
      end

      def reinit_pos
        @objs.each_with_index do |obj, i|
          obj.pos = i+1
          obj.save!
        end
      end
    end
  end
end
