class Rca < ApplicationRecord
  # audited

  has_rich_text :description
  # used to query the attached ActionText directly
  has_one :action_text_rich_text, class_name: 'ActionText::RichText', as: :record

  validates :title, presence: true
  validates :reference, presence: true
  validates :created_at, :updated_at, presence: true
  before_validation :set_reference, on: :create
  before_validation :set_dates, on: :create
  after_create :init_reference
  # after_save :generate_code

  enum status: { active: 1, deleted: 0 }, _prefix: true

  default_scope { where(status: 1) }
  scope :deleted, -> { where(status: 0) }

  # Methodology
  belongs_to :methodology, class_name: 'Meth::Methodology'
  has_many :steps, -> { includes(step_tools: :tool) }, through: :methodology, class_name: 'Meth::Step', source: :steps_menu

  # Custom fields
  has_one :custom_field, dependent: :destroy
  accepts_nested_attributes_for :custom_field

  # Advancement
  has_many :rca_advances, dependent: :destroy
  belongs_to :advance_step, class_name: 'Meth::Step', optional: true

  accepts_nested_attributes_for :rca_advances

  acts_as_taggable # Alias for acts_as_taggable_on :tags

  # Perimeters
  has_many :perimeter_rcas, class_name: 'Admin::PerimeterRca', dependent: :destroy
  has_many :perimeters, through: :perimeter_rcas, class_name: 'Admin::Perimeter'

  # Rca List (as model)
  belongs_to :tools_rca_list, class_name: 'Tools::RcaList', optional: true

  # Tools
  has_many :tools_qqoqcps, class_name: 'Tools::Qqoqcp', dependent: :destroy
  has_many :tools_causes, class_name: 'Tools::Cause', dependent: :destroy
  has_many :tools_cause_trees, class_name: 'Tools::CauseTree', dependent: :destroy
  has_many :tools_ishikawas, class_name: 'Tools::Ishikawa', dependent: :destroy
  has_many :tools_tasks, class_name: 'Tools::Task', dependent: :destroy
  has_many :tools_decision_mats, class_name: 'Tools::DecisionMat', dependent: :destroy
  has_many :tools_teams, -> { order(profil_id: :asc) }, class_name: 'Tools::Team', dependent: :destroy
  has_many :people, through: :tools_teams
  has_many :tools_documents, class_name: 'Tools::Document', dependent: :destroy do
    def by_step_role_id(step_role_id)
      if step_role_id == 0 || step_role_id.nil?
        where(tool_reference: [0, nil])
      else
        where(tool_reference: step_role_id)
      end
    end
  end
  has_many :tools_images, class_name: 'Tools::Image', dependent: :destroy do
    def by_step_role_id(step_role_id)
      if step_role_id == 0 || step_role_id.nil?
        where(tool_reference: [0, nil])
      else
        where(tool_reference: step_role_id)
      end
    end
  end
  has_many :tools_criticities, class_name: 'Tools::Criticity', dependent: :destroy
  has_many :tools_indicators, class_name: 'Tools::Indicator', dependent: :destroy
  has_many :tools_generics, class_name: 'Tools::Generic', dependent: :destroy
  has_many :tools_rca_lists, class_name: 'Tools::RcaList', dependent: :destroy

  accepts_nested_attributes_for :tools_teams

  # FIND & CREATE
  def find_or_create_tool(tool_name, tool_class_name, step_tool)
    # the_tool = send(tool_name).where(tool_reference: tool_reference).first
    the_tool = tool_class_name.constantize.search_default(self, tool_name, step_tool)
    return the_tool if the_tool

    the_tool = tool_class_name.constantize.create_default!({rca: self,
                                                            tool_reference: step_tool.tool_reference,
                                                            tool_num: step_tool.tool_num}, step_tool)
    return the_tool
  end

  # Title and reference
  def long_title
    (reference || '') + ' - ' + (title || '')
  end

  # Team
  def add_person_to_team(person, profil_name)
    profil = Profil.where(profil_name: profil_name)&.first

    Tools::Team.create!(rca_id: id, person: person, profil: profil, tool_reference: 1, tool_num: 1) if profil && person
  end

  def add_person_to_team_by_id(person, profil_id)
    profil = Profil.find(profil_id)

    Tools::Team.create!(rca: self, person: person, profil: profil, tool_reference: 1, tool_num: 1) if profil && person
  end

  def get_person_from_team(profil_id)
    return tools_teams.where(profil_id: profil_id)&.first
  end

  def get_people_from_profil(profil_id)
    return tools_teams.where(profil_id: profil_id).includes(:person).map(&:person)
  end

  def add_rca_author(person)
    add_person_to_team(person, 'author')
  end

  def rca_author
    return tools_teams.includes(:profil).where(profils: {profil_name: 'author'})&.first&.person
  end

  def add_rca_responsible(person)
    add_person_to_team(person, 'resp_rca')
  end

  def rca_responsibles
    return tools_teams.includes(:profil).where(profils: {profil_name: 'resp_rca'})
  end

  def rca_first_responsible
    return tools_teams.includes(:profil).where(profils: {profil_name: 'resp_rca'})&.first&.person
  end

  def rca_responsible?(person)
    return tools_teams.includes(:profil).where(person_id: person.id, profils: {profil_name: 'resp_rca'}).empty? ? false : true
  end

  # TODO : CHECK / Not used
  # Ishikawas : initialise (création) tous les ishikawas de la méthodo
  def init_ishikawas_list_all
    ishikawas = []
    ishikawa_names = []
    methodology.steps.includes(step_tools: [:tool]).each do |step|
      step.step_tools.each do |step_tool|
        next unless step_tool.tool.tool_name == 'Ishikawa'

        ishikawa = find_or_create_tool('tools_ishikawas', 'Tools::Ishikawa', step_tool)
        ishikawas << ishikawa
        ishikawa_names << step_tool.name
      end
    end
    return ishikawas, ishikawa_names
  end

  # Ishikawas : initialise (création) les ishikawas de tool_reference
  def init_ishikawas_list_ref(tool_reference)
    ishikawas = []
    ishikawa_names = []
    methodology.steps.includes(step_tools: [:tool]).each do |step|
      step.step_tools.each do |step_tool|
        next unless step_tool.tool.tool_name == 'Ishikawa' && step_tool.tool_reference == tool_reference

        ishikawa = find_or_create_tool('tools_ishikawas', 'Tools::Ishikawa', step_tool)
        ishikawas << ishikawa
        ishikawa_names << step_tool.name
      end
    end
    return ishikawas, ishikawa_names
  end

  # A déplacer dans un service

  # Appel de toutes les initialisations
  # Appel depuis controlleur rcas#create et api POST
  def rca_creation_init(current_user)
    # binding.pry
    if reference.nil? || reference == ''
      # self.reference = id.to_s # "RCA ##{id}"
      init_reference
      save!
    end

    add_rca_author(current_user.person)
    add_rca_responsible(current_user.person)
    # Initialisation rca
    initialize_rca_infos(current_user.preferences)
    # Init default perimeter
    default_perimeter_id = current_user.get_preference(:perimeter)
    # si pas de perimetre utilisateur par défaut : premier périmètre de l'utilisateur
    default_perimeter_id = current_user.perimeters.order(ancestry: :asc)&.first&.id unless default_perimeter_id.present?
    # si pas de perimetre utilisateur : ALL
    default_perimeter_id = Admin::Perimeter.where(ancestry: nil)&.first&.id unless default_perimeter_id.present?
    return unless default_perimeter_id.present?

    default_perimeter = Admin::Perimeter.find(default_perimeter_id)
    add_perimeter_and_childs(default_perimeter)
  end

  def initialize_rca_infos(preferences = nil)
    unless custom_field
      cf = CustomField.create!(rca: self)

      # User preferences
      if preferences
        CustomField.content_columns.each do |col|
          next unless preferences&.[](col.name) && preferences&.[](col.name) != ''

          cf.send("#{col.name}=", preferences[col.name])
        end
        cf.save!
      end
    end

    # Advancemenet
    steps.each do |step|
      RcaAdvance.create!(rca: self, meth_step: step) if rca_advances.where(meth_step: step).empty?
    end
  end

  def reinitialize_rca_infos
    CustomField.create!(rca: self) unless custom_field

    steps.each do |step|
      RcaAdvance.create!(rca: self, meth_step: step) if rca_advances.where(meth_step: step).empty?
    end
  end

  # Step Advancement
  def rca_advances_used
    rca_advances.includes(:meth_step).where(meth_steps: {methodology_id: methodology_id}).order('meth_steps.pos')
  end

  # Retourne la valeur d'avancement de l'étape step
  def advancement(step)
    rca_advances.where(meth_step: step).first&.advance || 0
  end

  # Retourne true si step "step" is finished
  def step_finished?(step)
    return rca_advances.where(meth_step: step).first&.advance == 2 || false
  end

  # Retourne true si step "step" is started (& not finished)
  def step_started?(step)
    return rca_advances.where(meth_step: step).first&.advance == 1 || false
  end

  # idem advancement mais en version texte
  def step_advancement(step)
    adv = rca_advances.where(meth_step: step).first&.advance
    return adv == 0 ? 'not started' : (adv == 1 ? 'in progress' : 'finished')
  end

  # assigne la valeur adv_value à l'avancement du step "step"
  # si adv n'existe pas => on crée les avancements
  def advancement_set(step, adv_value)
    adv = rca_advances.where(meth_step: step).first
    reinitialize_rca_infos unless adv
    adv ||= rca_advances.where(meth_step: step).first
    adv.advance = adv_value.to_i

    # met à jour date_start et/ou date_finish.
    if adv.advance == 0
      adv.date_start = nil
      adv.date_finish = nil
    elsif adv.advance == 1
      adv.date_start = Date.today unless adv.date_start
      adv.date_finish = nil
    elsif adv.advance == 2
      adv.date_start = created_at unless adv.date_start
      adv.date_finish = Date.today
    end

    adv.save!

    return unless step.step_role.role_name == 'Close'

    # Si on assign l'avancement du step "closed" => assigne la date de cloture.
    if step_finished?(step) && closed_at.nil?
      self.closed_at = Date.today
      save!
    elsif !step_finished?(step) && !closed_at.nil?
      self.closed_at = nil
      save!
    end
  end

  # Global advancement
  def advance_role_name
    return advance_step&.step_role&.name || '-'
  end

  def advance_step_name
    return advance_step&.name || '-'
  end

  def rca_closed?
    return advance == Meth::StepRole.where(role_name: 'Close')&.first&.advance_level
  end

  def advance_duration
    # binding.pry
    return rca_closed? && closed_at ? (closed_at.to_date - created_at.to_date).to_i : (Date.today - created_at.to_date).to_i
    # return rca_closed? ? distance_of_time_in_words(created_at.to_date, closed_at.to_date) : distance_of_time_in_words(created_at.to_date, Date.today)
  end

  # Perimeters
  def add_perimeter(perimeter_id)
    Admin::PerimeterRca.create!(rca: self, perimeter_id: perimeter_id)
  end

  def remove_perimeter(perimeter_id)
    perimeter_rcas.where(perimeter_id: perimeter_id)&.first&.destroy
  end

  def add_perimeter_and_childs(perimeter)
    add_perimeter(perimeter.id)

    perimeter.children&.each do |child|
      add_perimeter_and_childs(child)
    end
  end

  # Parent/Child (RcaList)
  def set_rca_parent(rca_parent, tool_reference: 1)
    if rca_parent
      tool_rcalist = rca_parent.tools_rca_lists.where(tool_reference: tool_reference)&.first

      # create the tool if it does not exist
      tool_rcalist = Tools::RcaList.create_default!({rca_id: rca_parent.id, tool_reference: tool_reference}, nil) unless tool_rcalist

      if tool_rcalist
        self.tools_rca_list = tool_rcalist
        self.save!
      end
    end
  end

  def del_rca_parent
    if self.tools_rca_list
      self.tools_rca_list = nil
      self.save!
    end
  end

  private

  def set_reference
    self.reference = 'REF#' if reference.nil? || reference == ''
  end

  def set_dates
    self.created_at = Time.current if self.created_at.nil?
    self.updated_at = Time.current if self.updated_at.nil?
  end

  def init_reference
    # self.reference = id.to_s if reference == 'REF#'

    return unless reference == 'REF#' || reference.nil? || reference == ''

    size = id.num_digits <= 5 ? 5 : id.num_digits
    self.reference = format("%0#{size}d", id) # id.to_s

    config = Admin::AdminConfig.first
    self.reference = "#{config.options['rca_reference_txt']} #{reference}" if config&.options&.[]('rca_reference_txt')
    # save!
  end

  # def generate_code
  #   Rcas::GenerateCode.new(self).call
  # end
end
