require_relative 'boot'

require 'rails/all'
require 'wicked_pdf'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Prowhy
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    # Configuration from initialize/new_framework_defaults_6_1

    # TODO : changer tous les form_with avec local: true|false
    Rails.application.config.action_view.form_with_generates_remote_forms = true

    # ATTENTION !!! EN MODE DEV cette option doit peut-être être désactivée si "HTTP response header overflow"
    # Rails.application.config.action_view.preload_links_header = true => Dans environments/development.rb

    # End config Rails 6.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # Config pour que seed:dump fonctionne, sinon il ne fait qu'une table...
    # config.autoloader = :classic # celui là pour le seed:dump
    # config.autoloader = :zeitwerk

    config.i18n.available_locales = [:en, :fr]
    config.i18n.default_locale = :en
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

    # config.middleware.use WickedPdf::Middleware
    # config.middleware.use I18n::Middleware

    # config.relative_url_root = '/prowhy'=> dans production.rb
  end
end
