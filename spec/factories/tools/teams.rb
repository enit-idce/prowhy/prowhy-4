FactoryBot.define do
  factory :tools_team, class: Tools::Team do
    tool_reference { 1 }
    tool_num { 1 }
    association :rca, factory: :rca
    association :person, factory: :person
    association :profil, factory: :profil
  end
end
