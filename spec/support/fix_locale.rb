# ActionController::TestCase::Behavior.module_eval do
#   alias_method :process_old, :process

#   def process(action, *args)
#     puts "=============================================================>PROCESS !!!"
#     binding.pry
#     if (params = args.first[:params])
#       params['locale'] = I18n.default_locale
#     end
#     process_old(action, *args)
#   end
# end

# module FixLocalesSpecs
#   module ::ActionController::TestCase::Behavior
#     alias_method :process_without_logging, :process

#     def process(action, http_method = 'GET', *args)
#       e = Array.wrap(args).compact
#       e[0] ||= {}
#       e[0].merge!({locale: I18n.locale})
#       process_without_logging(action, http_method, *e)
#     end
#   end
# end

# workaround, to set default locale for ALL spec (ok for request tests)

# For type: :helper
class ActionView::TestCase::TestController
  def default_url_options(_options = {})
    { locale: I18n.default_locale }
  end
end

# class ActionDispatch::Routing::RouteSet
#   def default_url_options(_options = {})
#     { locale: I18n.default_locale }
#   end
# end

# class ActionController::TestCase # ::UrlFor
#   def default_url_options(_options = {})
#     { locale: I18n.default_locale }
#   end
# end

puts "=============================================================>LOADED !!!"
