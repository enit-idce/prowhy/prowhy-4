require 'rails_helper'

RSpec.describe Meth::CustomFieldDesc, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
  subject(:custom_field_desc) do
    Meth::CustomFieldDesc.new(custom_field_type: cf_type, internal_name: 'custom_1', name: 'Custom Field 1')
  end

  let(:cf_type) { Meth::CustomFieldType.create!(field_type: 'string', field_size: nil, field_internal: 'text', field_name: 'Texte') }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to custom_field_type' do
        assc = Meth::CustomFieldDesc.reflect_on_association(:custom_field_type)
        expect(assc.macro).to eq :belongs_to
      end

      it 'has many tool_custom_fields' do
        assc = Meth::CustomFieldDesc.reflect_on_association(:tool_custom_fields)
        expect(assc.macro).to eq :has_many
      end

      it 'has many list_contents' do
        assc = Meth::CustomFieldDesc.reflect_on_association(:list_contents)
        expect(assc.macro).to eq :has_many
      end

      it 'belongs to ref_field' do
        assc = Meth::CustomFieldDesc.reflect_on_association(:ref_field)
        expect(assc.macro).to eq :belongs_to
      end
    end

    describe 'Validations' do
      it 'is not valid without name' do
        custom_field_desc.name = ''
        expect(custom_field_desc).to_not be_valid
      end

      it 'is not valid without internal name' do
        custom_field_desc.internal_name = ''
        expect(custom_field_desc).to_not be_valid
      end

      it 'is valid with name & internal_name' do
        expect(custom_field_desc).to be_valid
      end

      # it 'is not valid 2 times same name' do
      #   custom_field_desc.save!
      #   cf2 = Meth::CustomFieldDesc.new(custom_field_type: cf_type, internal_name: 'custom_2', name: 'Custom Field 1')

      #   expect(cf2).to_not be_valid
      # end

      it 'is not valid 2 times same internal name' do
        custom_field_desc.save!
        cf2 = Meth::CustomFieldDesc.new(custom_field_type: cf_type, internal_name: 'custom_1', name: 'Custom Field 2')

        expect(cf2).to_not be_valid
      end

      it 'is valid with differents name and internal name' do
        custom_field_desc.save!
        cf2 = Meth::CustomFieldDesc.new(custom_field_type: cf_type, internal_name: 'custom_2', name: 'Custom Field 2')

        expect(cf2).to be_valid
      end
    end
  end
end
