class CreateToolsTaskValidations < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_task_validations do |t|
      t.integer :status, null: false, default: 0
    end

    Tools::Task.all.each do |task|
      task.task_detail_type.gsub!('Tools::Task', '')
      task.task_detail_type = "Tools::Task#{task.task_detail_type}"
      task.save!

      next unless task.task_detail_type == 'Tools::TaskValidation'

      task.task_detail = Tools::TaskValidation.create!
      task.save!
    end
  end
end
