require_dependency 'tools'

module Tools
  class DecMatVal < ApplicationRecord
    belongs_to :crit_dec_mat
    belongs_to :dec_mat_link
  end
end
