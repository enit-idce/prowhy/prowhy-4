class CreateToolsDecMatLinks < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_dec_mat_links do |t|
      t.references :decision_mat, null: false, foreign_key: { to_table: :tools_decision_mats }
      t.references :entry, polymorphic: true, null: false

      t.timestamps
    end
  end
end
