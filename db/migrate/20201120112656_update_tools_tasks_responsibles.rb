class UpdateToolsTasksResponsibles < ActiveRecord::Migration[6.0]
  def change
    remove_reference :tools_tasks, :responsible_action, index: true, foreign_key: { to_table: :users }
    remove_reference :tools_tasks, :responsible_rca, index: true, foreign_key: { to_table: :users }

    add_reference :tools_tasks, :responsible_action, foreign_key: { to_table: :people }, after: :ancestry
    add_reference :tools_tasks, :responsible_rca, foreign_key: { to_table: :people }, after: :ancestry
  end
end
