require "rails_helper"

RSpec.describe Tools::RcaListsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/tools/rca_lists").to route_to("tools/rca_lists#index")
    end

    it "routes to #new" do
      expect(get: "/tools/rca_lists/new").to route_to("tools/rca_lists#new")
    end

    it "routes to #show" do
      expect(get: "/tools/rca_lists/1").to route_to("tools/rca_lists#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/tools/rca_lists/1/edit").to route_to("tools/rca_lists#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/tools/rca_lists").to route_to("tools/rca_lists#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/tools/rca_lists/1").to route_to("tools/rca_lists#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/tools/rca_lists/1").to route_to("tools/rca_lists#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/tools/rca_lists/1").to route_to("tools/rca_lists#destroy", id: "1")
    end
  end
end
