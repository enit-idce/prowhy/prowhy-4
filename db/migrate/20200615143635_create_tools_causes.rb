class CreateToolsCauses < ActiveRecord::Migration[6.0]
  def change
    create_table :tools_causes do |t|
      t.references :rca, null: false, foreign_key: true
      t.integer :tool_reference
      t.string :name
      # t.boolean :tree_head, default: 0
      t.integer :level, default: 0
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
