class AddDefaultPerimeterToAdminConfig < ActiveRecord::Migration[6.0]
  def change
    add_reference :admin_admin_configs, :default_perimeter, null: true, foreign_key: { to_table: :admin_perimeters }, after: :default_role_id
  end
end
