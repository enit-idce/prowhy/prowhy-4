class DataAdminConfigOptions < ActiveRecord::Migration[6.0]
  class MigrationConfig < ApplicationRecord
    self.table_name = :admin_admin_configs
    # serialize :connexions_backup
    # serialize :task_mails_backup
  end

  def up
    # Mailer options
    say 'Copy mail smtp configuration.'
    MigrationConfig.all.each do |config|
      config.mailer_options = InfoconnectMailer.smtp_settings
      config.mailer_options[:authentication] ||= 'none'
      config.mailer_options[:more_options] ||= false
      config.mailer_options[:ssl_tls] = config.mailer_options[:ssl] == true ? 'ssl' : config.mailer_options[:tls] == true ? 'tls' : 'none'
      config.save!
    end

    # # Connexions
    # say 'Convert connexions datas.'
    # MigrationConfig.all.each do |config|
    #   config.connexions = config.connexions_backup
    #   config.save!
    # end

    # # Task mails
    # say 'Convert task_mails datas.'
    # MigrationConfig.all.each do |config|
    #   config.task_mails = config.task_mails_backup
    #   config.save!
    # end
  end

  def down
    say 'Do nothing.'
    # raise ActiveRecord::IrreversibleMigration
  end
end
