require_dependency 'meth'

module Meth
  class StepTool < ApplicationRecord
    include MethConcern

    translates :name, fallback: :any

    belongs_to :step, class_name: 'Meth::Step'
    belongs_to :tool, class_name: 'Meth::Tool'
    has_many :tool_custom_fields, -> { order(pos: :asc) }, dependent: :destroy

    has_rich_text :description

    validates :name, presence: true
    validates :step_tool_type, inclusion: { in: [1, 2, 3] }
    # 1 => menu / 2 => doc / 3 => img

    # serialize :tool_options

    # Generic step-tool => generic file.
    has_one_attached :generic_file

    after_create :assign_tool_options

    def tool_options
      @tool_options ||= super&.deep_symbolize_keys!
    end

    # TOOL_OPTIONS_KEYS = {
    def self.get_tool_options_keys
      return {
        'Action': [{keys: [:task_options, :task_detail_type], default: '', values: Tools::Task::TASK_TYPES, multiple: true},
                   {keys: [:plan_options, :efficacity], default: 'show', values: ['show', 'true', 'false']},
                   {keys: [:plan_options, :causes], default: 'false', values: ['show', 'true', 'false']},
                   {keys: [:plan_options, :matrix], default: 'false', values: ['show', 'true', 'false']},
                   {keys: [:plan_options, :blocks], default: 'false', values: ['true', 'false']},
                   {keys: [:plan_options, :blocks_column], default: ''},
                   {keys: [:plan_options, :validation], default: 'false', values: ['true', 'false']},
                   {keys: [:plan_options, :rcas], default: 'false', values: ['true', 'false']},
                   {keys: [:plan_options, :type_plan], default: 'rca', values: ['rca', 'user', 'global']},
                   {keys: [:plan_options, :sub_type_plan], default: 'normal', values: ['normal', 'rca-list']},
                   {keys: [:plan_options, :check_list], default: 'false', values: ['true', 'false']},
                   {keys: [:form_options, :efficacity], default: 'true', values: ['date', 'true', 'false']}],
        'ActionCheck': [{keys: [:task_options, :task_detail_type], default: '', values: Tools::Task::TASK_TYPES},
                        {keys: [:more_options], default: '{}', json: true}], # , :tools_matrix_stats => matrix_status: 1
        'DecisionMat': [{keys: [:entry_class], default: 'Tools::Task', values: ['Tools::Task', 'Tools::Cause']},
                        {keys: [:entry, :task_detail_type], default: '', values: Tools::Task::TASK_TYPES}],
                        # {keys: [:criteria], default: '{}', json: true}],
        'Graph': [],
        'Team': [],
        'Qqoqcp': [{keys: [:qqoqcp_config], default: 1}],
        'CauseTree': [{keys: [:form_options, :status], default: 'false', values: ['true', 'false']}],
        'Ishikawa': [{keys: [:form_options, :status], default: 'false', values: ['true', 'false']}],
        'Why': [],
        'CustomField': [{keys: [:closure], default: 'false', values: ['true', 'false']}],
        'Tags': [],
        'Improve5s': [],
        'Document': [],
        'Image': [],
        'Report': [],
        'Criticity': [],
        'Generic': [],
        'RcaList': [{keys: [:methodology], default: '', values: Meth::Methodology.rca_methodologies_all.map { |m| "#{m.id} #{m.name}" } }]
      }.freeze
    end

    def tool_option_value(keys)
      toto = tool_options
      keys.each do |key|
        toto = toto[key]
        break unless toto.class == Hash
      end
      # binding.pry
      toto # .to_s
    rescue StandardError
      # binding.pry
      return nil
    end

    def tool_option_string(keys)
      toto = ''
      keys.each do |key|
        toto += "[#{key}]"
      end
      toto
    end
    # after_initialize do
    #   if new_record?
    #     # Initialisation des tool_options
    #     self.tool_options ||= {}
    #   end
    # end

    def locked?
      step&.locked?
    end

    def update_options(options_params)
      option_params_hash = options_params ? (options_params.class == Hash ? options_params : options_params.to_unsafe_h) : nil
      tool_options.deep_symbolize_keys!.merge!(option_params_hash&.deep_symbolize_keys&.deep_true_false_values || {})
      # tool_options.deep_symbolize_keys!.merge!(options_params&.to_unsafe_h&.deep_symbolize_keys&.deep_true_false_values || {})
      # binding.pry

      # TOOL_OPTIONS_KEYS[tool.tool_name.intern]&.each do |line|
      tool_options_keys = Meth::StepTool.get_tool_options_keys
      tool_options_keys[tool.tool_name.intern]&.each do |line|
        next unless line[:json] == true

        # Transform le string en hash
        tool_options.value_to_json(line[:keys].dup)
      end

      # Set task_detail_type as string if one type defined.
      td_type = tool_options&.[](:task_options)&.[](:task_detail_type)
      tool_options[:task_options][:task_detail_type] = td_type[0] if td_type&.class == Array && td_type&.size == 1

      return true
    rescue StandardError => e
      errors.add(:base, e.message)
      return false
    end

    def update_with_options(step_tool_params, options_params)
      # binding.pry

      # step_tool_params[:tool_options] = Hash.from_string(step_tool_params[:tool_options]) if step_tool_params[:tool_options] && !step_tool_params[:tool_options].empty?
      assign_attributes(step_tool_params)

      return update_options(options_params) && save!
    rescue StandardError => e
      errors.add(:base, e.message)
      return false
    end

    def name_show_all
      step.methodology.name + ' / ' + step.name + ' / ' + (name || tool.name)
    end

    def name_show
      step.name + ' / ' + (name || tool.name)
    end

    def name_show_short
      name || tool.name
    end

    def name_show_activity
      tool.activity.name + ' => ' + (name || tool.name)
    end

    def copy(step2)
      step_tool = StepTool.create!(step: step2, tool: tool,
                                   name_fr: name_fr, name_en: name_en,
                                   tool_reference: tool_reference,
                                   tool_num: tool_num,
                                   pos: pos,
                                   step_tool_type: step_tool_type,
                                   locked: locked,
                                   tool_options: tool_options&.clone)

      # Si générique => copy doc

      # Si custom field => copy ToolCustomField
      if tool.tool_name == 'CustomField'
        tool_custom_fields.each do |tcf|
          tcf.copy(step_tool)
        end
      end
      # step_tool = copy_details(step_tool, copy_options: false)

      return step_tool
    end

    # def copy_details(step_tool, copy_options: false)
    #   step_tool.tool_options = tool_options&.clone if copy_options

    #   if tool.tool_name == 'CustomField'
    #     tool_custom_fields.each do |tcf|
    #       tcf.copy(step_tool)
    #     end
    #   end

    #   step_tool.save!

    #   return step_tool
    # end

    private

    def assign_tool_options
      self.tool_options = tool.tool_options if tool.tool_options && !tool_options
      self.tool_options = {} unless tool_options
      save!
    end
  end
end
