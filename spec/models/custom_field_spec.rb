require 'rails_helper'

RSpec.describe CustomField, type: :model do
  subject(:custom_field) do
    # FactoryBot.create(:custom_field)
    CustomField.create!(rca: FactoryBot.create(:rca))
  end

  # Tests has_many :tools + dependant: destroy
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to rca' do
        custom_field
        assc = CustomField.reflect_on_association(:rca)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end

  context 'describe finalize class' do
    before :each do
      @cfd1 = FactoryBot.create(:meth_custom_field_desc, :type_rich_text)
      @cfd2 = FactoryBot.create(:meth_custom_field_desc, :type_tag)
      @cfd3 = FactoryBot.create(:meth_custom_field_desc)
      @cfd4 = FactoryBot.create(:meth_custom_field_desc)
      @cfd5 = FactoryBot.create(:meth_custom_field_desc, :type_select)
      @cfd6 = FactoryBot.create(:meth_custom_field_desc, :type_tree)
    end

    it '#finalize class' do
      CustomField.finalize
      # test @cfd1 respond_to rich_text function
      expect(CustomField.all).to respond_to('with_rich_text_'+@cfd1.internal_name)
      # test @cfd2 respond_to act_as_taggable function
      expect(custom_field).to respond_to(@cfd2.internal_name+'_list')
      # test @cfd 5 et @cfd6 respond_to _value function (list & tree)
      expect(custom_field).to respond_to(@cfd5.internal_name+'_value')
      expect(custom_field).to respond_to(@cfd6.internal_name+'_value')

      # fields 3 & 4 are not tag and rich text
      expect(CustomField.all).to_not respond_to('with_rich_text_'+@cfd3.internal_name)
      expect(CustomField.all).to_not respond_to('with_rich_text_'+@cfd4.internal_name)
      expect(custom_field).to_not respond_to(@cfd3.internal_name+'_list')
      expect(custom_field).to_not respond_to(@cfd4.internal_name+'_list')
    end
  end
end
