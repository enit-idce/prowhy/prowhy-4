require 'rails_helper'

RSpec.describe Tools::CauseTree, type: :model do
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to rca' do
        assc = Tools::CauseTree.reflect_on_association(:rca)
        expect(assc.macro).to eq :belongs_to
      end

      it 'has one cause_link' do
        assc = Tools::CauseTree.reflect_on_association(:cause_link)
        expect(assc.macro).to eq :has_one
      end

      it 'has one cause (through cause_link)' do
        assc = Tools::CauseTree.reflect_on_association(:cause)
        expect(assc.macro).to eq :has_one
      end
    end
  end
end
