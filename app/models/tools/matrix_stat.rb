require_dependency 'tools'

module Tools
  class MatrixStat < ApplicationRecord
    belongs_to :element, polymorphic: true

    enum matrix_status: { in_progress: 0, validated: 1, invalidated: 2 }, _prefix: true
  end
end
