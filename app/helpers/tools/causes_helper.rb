module Tools
  module CausesHelper
    include IconsHelper

    # Creates causes list and colors list (couleur fonction de status)
    def causes_without_colors(rca, tool_reference)
      # cause = rca&.tools_causes&.where(tree_head: true, tool_reference: tool_reference)&.first
      root_causes = []
      if tool_reference != 0
        root_causes << rca&.tools_causes&.where(tool_reference: tool_reference)&.first&.root
        root_causes&.compact!
      else
        root_causes = rca&.tools_causes&.where(ancestry: nil)
      end

      return [] if root_causes.nil? || root_causes.empty?

      # causes = cause.descendants
      causes = []
      root_causes.each do |rcause|
        causes += Tools::CauseQuery.relation.causes_subtree_by_id(rca.id, rcause.tool_reference)
      end

      return causes
    end

    # Creates causes list and colors list (couleur fonction de status)
    def causes_with_colors(rca, tool_reference)
      # root_causes = []
      # if tool_reference != 0
      #   root_causes << rca&.tools_causes&.where(tool_reference: tool_reference)&.first&.root
      #   root_causes&.compact!
      # else
      #   root_causes = rca&.tools_causes&.where(ancestry: nil)
      # end

      # return [], [] if root_causes.nil? || root_causes.empty?

      # # causes = cause.descendants
      # causes = []
      # root_causes.each do |rcause|
      #   causes += Tools::CauseQuery.relation.causes_subtree_by_id(rca.id, rcause.tool_reference)
      # end
      causes = causes_without_colors(rca, tool_reference)

      causes_colors = {}
      colors = { 'in_progress' => 'icon-status-inprogress', 'validated' => 'icon-status-validated', 'invalidated' => 'icon-status-invalidated' }
      causes.each do |c|
        causes_colors[c.id] = colors[c.status]
      end

      return causes, causes_colors
    end

    #######################################
    # Causes
    #
    # Création d'un select contenant la liste hiérarchisée des causes
    # select_id : id du <select>
    # select_name : name du <select>
    # causes : tableau des causes (pour les récupérer à partir d'un rca :
    #          Tools::CauseQuery.relation.causes_subtree_by_id(rca.id)
    # selected : id de la cause sélectionnée ou 0
    # colors : tableau ou hash des classes css de couleurs des causes (clé = id) ou rien si pas de coloration
    # TODO : changer liste d'options par options = {} avec valeurs par défaut dns la fonction.
    # def helper_causes_list(causes, c_name, c_name2 = nil, selected = 0, colors = nil, disable = '', add_empty = false)
    def helper_causes_list_default_options(options = {})
      options[:selected] ||= 0
      options[:colors] ||= nil
      options[:disable] ||= ''
      options[:add_empty] ||= false

      options
    end

    def helper_causes_list(causes, c_name, c_name2 = nil, options = {})
      # Valeurs par défaut des options
      options = helper_causes_list_default_options(options)

      n_name = c_name2 ? c_name+'['+c_name2+']' : c_name
      n_id = c_name2 ? c_name+'_'+c_name2 : c_name

      ret = ''

      ret += "<select id='#{n_id}' name='#{n_name}' class='form-control'> #{options[:disable]}"

      if options[:add_empty]
        ret += '<option '
        ret += "selected='selected' " if (options[:selected] == 0)
        ret += '></option>'
      end

      causes.each do |c|
        ret+= '<option '
        ret += "selected='selected' " if (options[:selected] == c.id)
        # ret += "style='color:#{colors[c.id]}' " if colors
        ret += "class='#{options[:colors][c.id]}' " if options[:colors]
        ret += "value=#{c.id} >"
        ret += c.name_level
        ret += '</option>'
      end
      ret += '</select>'

      return ret.html_safe
    end

    def helper_doc_cause_validation
      img1 = @export_pdf ? export_status_icon("in_progress") : fa_icon_status_presenter('icon-status-inprogress')
      img2 = @export_pdf ? export_status_icon("validated") : fa_icon_status_presenter('icon-status-validated')
      img3 = @export_pdf ? export_status_icon("invalidated") : fa_icon_status_presenter('icon-status-invalidated')
      return "<span class='doc-validation'>" + t("activerecord.attributes.tools/task_validation.status") + " : " +
              img1 + " " + t("simple_form.options.tools_cause.status.pending") + " " +
              img2 + " " + t("simple_form.options.tools_cause.status.validated") + " " +
              img3 + " " + t("simple_form.options.tools_cause.status.invalidated") +
              "</span>"
    end
  end
end
