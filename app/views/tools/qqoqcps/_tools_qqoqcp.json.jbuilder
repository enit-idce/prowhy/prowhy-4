json.extract! tools_qqoqcp, :id,
              :qui, :qui_not, :qui_info,
              :quoi, :quoi_not, :quoi_info,
              :ou, :ou_not, :ou_info,
              :quand, :quand_not, :quand_info,
              :comment, :comment_not, :comment_info,
              :pourquoi, :pourquoi_not, :pourquoi_info,
              :created_at, :updated_at
json.url tools_qqoqcp_url(tools_qqoqcp, format: :json)
