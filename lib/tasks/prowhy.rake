namespace :prowhy do
  desc 'Send emails for late and comming soon tasks'
  task send_reminders: :environment do
    SendReminders.run_reminders
  end
end
