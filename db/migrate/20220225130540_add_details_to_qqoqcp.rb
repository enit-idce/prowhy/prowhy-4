class AddDetailsToQqoqcp < ActiveRecord::Migration[6.0]
  def change
    add_column :labels_qqoqcp_configs, :nb_lines, :integer, after: :usable, default: 3
  end
end
