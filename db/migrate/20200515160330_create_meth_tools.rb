class CreateMethTools < ActiveRecord::Migration[6.0]
  def change
    create_table :meth_tools do |t|
      t.string :name
      t.references :activity, null: false, foreign_key: { to_table: :meth_activities }
      t.string :tool_name

      t.timestamps
    end
  end
end
