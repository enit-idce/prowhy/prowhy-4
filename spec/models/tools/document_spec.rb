require 'rails_helper'

RSpec.describe Tools::Document, type: :model do
  subject(:document) do
    Tools::Document.create!(rca: rca, tool_reference: 1, name: 'Doc 1', file: doc_ok)
  end

  let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }

  let(:doc_ok) do
    ActiveStorage::Blob.create_and_upload!(
      io: File.open(file_ok, 'rb'),
      filename: 'not_image.pdf',
      content_type: 'file/pdf'
    ).signed_id
  end

  let(:file_ok) { Rails.root.join('spec', 'support', 'assets', 'images', 'not_image.pdf') }

  context 'describes model associations and validations' do
    describe 'Validations' do
      it 'is not valid without name' do
        document.name = ''
        expect(document).to_not be_valid
      end

      it 'is valid with name and file' do
        document.name = 'Le document'
        expect(document).to be_valid
      end
    end
  end
end
