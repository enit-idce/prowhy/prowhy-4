module Tools
  module GenericsHelper
    # TODO: move in application Helper
    def convert_time(datetime)
      datetime.in_time_zone('Paris').strftime('%Y-%m-%d: %H:%M')
    end

    def helper_generic_alert_doc(tools_generic)
      return if tools_generic.file_versions.empty?

      helper_content = ''
      helper_content += t('.message_1') + ' : ' + (current_user.get_preference("tools-gen-#{@tools_generic.id}") || 'none')
      helper_content += '<br/>' + t('.message_2') +  " [#{tools_generic.file_versions.first.author.name}] : " + convert_time(tools_generic.file_versions.first.date)

      # binding.pry

      if tools_generic.file_versions.first.author_id != current_user.person_id && (current_user.get_preference("tools-gen-#{@tools_generic.id}") || '') < convert_time(@tools_generic.file_versions.first.date)
        helper_content += "<br/>#{t('.message_3')}"
        warning_popup('Warning', helper_content, '')
      else
        # helper_content += '<br/>Ok à jour.'
        info_popup('Info', helper_content, '')
      end
    end
  end
end
