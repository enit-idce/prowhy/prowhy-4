require 'rails_helper'

RSpec.feature 'Rca open menus', type: :feature do
  let(:tools_done) { %w[CustomField Qqoqcp CauseTree Why Action Ishikawa DecisionMat Team Graph ActionCheck RcaList] }

  before :all do
    puts "================================> IN BEFORE all".green
    load "#{Rails.root}/db/seed_tests.rb"
    @methodology = Meth::Methodology.rca_methodologies.where(name_fr: 'Méthode 8D').first
    @user = User.first
    # @user.usersession[:active_step_tool] = @methodology.steps.first.step_tools.first.id
    @user.save!
    sign_in @user

    @rca = Rca.create(title: 'Mon premier Rca', methodology: @methodology)
    @rca.add_person_to_team(@user.person, 'author')
    @rca.initialize_rca_infos

    # Add elements
    step_tool = @methodology.steps_menu.first.step_tools.first
    @rca.find_or_create_tool('tools_qqoqcps', 'Tools::Qqoqcp', step_tool)
    cause_tree = @rca.find_or_create_tool('tools_cause_trees', 'Tools::CauseTree', step_tool)
    @rca.find_or_create_tool('tools_ishikawas', 'Tools::Ishikawa', step_tool)
    @rca.find_or_create_tool('tools_qqoqcps', 'Tools::Qqoqcp', step_tool)
    FactoryBot.create(:indicator, rca_id: @rca.id)
    FactoryBot.create(:task, rca_id: @rca.id, task_detail_type: 'Tools::TaskCorrective', tools_cause_id: cause_tree.cause_link.cause.id)
    FactoryBot.create(:task, rca_id: @rca.id, task_detail_type: 'Tools::TaskCorrective', tools_cause_id: cause_tree.cause_link.cause.id)
    FactoryBot.create(:task, rca_id: @rca.id, task_detail_type: 'Tools::TaskCorrective', tools_cause_id: cause_tree.cause_link.cause.id)
    FactoryBot.create(:task, rca_id: @rca.id, task_detail_type: 'Tools::TaskCorrective', tools_cause_id: cause_tree.cause_link.cause.id)
    puts "================================> END BEFORE all".green
  end

  after :all do
    puts "================================> IN AFTER all".green
    load "#{Rails.root}/db/seed_empty.rb"
    puts "================================> IN AFTER all".green
  end

  scenario 'User opens steps menus and tools', js: true do
    visit '/rcas'
    # save_and_open_page
    find("a[href='#{edit_rca_path(@rca)}']").click
    expect(page).to have_current_path(edit_rca_path(@rca))

    @methodology.steps_menu.each do |step|
      # save_and_open_page
      puts "SEARCH HEADER #{step.id} - #{step.name}".yellow
      expect(page).to have_selector(:css, "a[id='header-step-#{step.id}']")
      # have_link("#header-step-#{step.id}")
      step_button = find("a[id='header-step-#{step.id}']")
      step_button.click

      # expect(button).to_not have_css('.collapsed')

      step.step_tools.each do |step_tool|
        puts "  -- SEARCH LINK #{step_tool.id} - #{step_tool.name}".blue
        begin
          button = find("a[href='#{meth_step_tool_show_path(id: step_tool.id, rca: @rca.id)}']")
        rescue Capybara::ElementNotFound, Selenium::WebDriver::Error::StaleElementReferenceError, StandardError
          puts '  ---- retry'.red
          button = find("a[id='header-step-#{step.id}']")
          button.click

          button = find("a[href='#{meth_step_tool_show_path(id: step_tool.id, rca: @rca.id)}']")
        end

        button.click

        if tools_done.include?(step_tool.tool.tool_name)
          # do nothing => show tool
        else
          expect(page).to have_text("Step #{step.name}")
          expect(page).to have_text("Tool #{step_tool.tool.name}")
        end
      end

      step_button.click
      # rescue Capybara::ElementNotFound, Selenium::WebDriver::Error::StaleElementReferenceError, StandardError
      # puts "  -- Rescue not find link : panel-collapse-#{step.id}"
    end
  end

  scenario 'User opens transverse tools', js: true do
    sign_in @user
    visit '/rcas'
    # save_and_open_page
    find("a[href='#{edit_rca_path(@rca)}']").click

    @methodology.step_trstools.step_tools.each do |step_tool|
      find("a[href='#{meth_step_tool_show_path(id: step_tool.id, rca: @rca.id)}']").click

      next unless step_tool.tool.tool_name == 'Report' # Rca Reporting

      find("a[href='#report-bloc-0']").click
      # save_and_open_page
      find("a[id='person_link']").click
      # save_and_open_page
      select 'Admin Admin', from: 'list_person'
      click_button 'OK'

      expect(page).to have_selector(:link_or_button, 'Send Emails')
      click_button 'Send Emails'
    end
  end

  scenario 'User export Repport in pdf format', js: true do
    puts "================ In export PDF".blue
    sign_in @user
    visit '/rcas'
    puts "================ /rcas".blue
    # save_and_open_page
    find("a[href='#{edit_rca_path(@rca)}']").click

    puts "================ Rca opended.".blue
    @methodology.step_trstools.step_tools.each do |step_tool|
      next unless step_tool.tool.tool_name == 'Report' # Rca Reporting

      puts "================ In report menu.".blue
      find("a[href='#{meth_step_tool_show_path(id: step_tool.id, rca: @rca.id)}']").click

      expect(page).to have_selector(:link_or_button, 'Export PDF')
      puts "================ Go export.".blue
      click_button 'Export PDF'
      puts "================ Export ok.".blue
      # save_and_open_page
      # page.go_back
      # puts "================ Back ok.".blue
    end
    puts "================ END FCT.".blue
  end

  scenario 'User export Action Plan in pdf format', js: true do
    puts "================ In Action Plan PDF".blue
    sign_in @user
    visit '/rcas'
    # save_and_open_page
    find("a[href='#{edit_rca_path(@rca)}']").click

    @methodology.step_trstools.step_tools.each do |step_tool|
      next unless step_tool.tool.tool_name == 'Action' # Action Plan

      button = find("a[href='#{meth_step_tool_show_path(id: step_tool.id, rca: @rca.id)}']")
      button.click
      # save_and_open_page
      expect(page).to have_selector(:link_or_button, 'Export PDF')

      begin
        button = find('a', text: 'Export PDF')
      rescue Capybara::ElementNotFound, Selenium::WebDriver::Error::StaleElementReferenceError, StandardError
        puts '  ---- retry'.red
        button = find("a[href='#{meth_step_tool_show_path(id: step_tool.id, rca: @rca.id)}']")
        button.click
        # save_and_open_page
        button = find('a', text: 'Export PDF')
      end
      # save_and_open_page
      # click_button 'Export PDF'
      button.click
      # page.go_back
    end
    puts "================ END ACTION PLAN.".blue
  end
end
