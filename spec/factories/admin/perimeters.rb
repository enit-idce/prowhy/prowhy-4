FactoryBot.define do
  factory :perimeter, class: Admin::Perimeter do
    name { 'ALL' }
    ancestry { nil }
  end
end
