require 'rails_helper'

RSpec.describe Labels::QqoqcpConfigsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/labels/qqoqcp_configs').to route_to('labels/qqoqcp_configs#index')
    end

    it 'routes to #new' do
      expect(get: '/labels/qqoqcp_configs/new').to route_to('labels/qqoqcp_configs#new')
    end

    it 'routes to #show' do
      expect(get: '/labels/qqoqcp_configs/1').to route_to('labels/qqoqcp_configs#show', id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/labels/qqoqcp_configs/1/edit').to route_to('labels/qqoqcp_configs#edit', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/labels/qqoqcp_configs').to route_to('labels/qqoqcp_configs#create')
    end

    it 'routes to #update via PUT' do
      expect(put: '/labels/qqoqcp_configs/1').to route_to('labels/qqoqcp_configs#update', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/labels/qqoqcp_configs/1').to route_to('labels/qqoqcp_configs#update', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/labels/qqoqcp_configs/1').to route_to('labels/qqoqcp_configs#destroy', id: '1')
    end
  end
end
