require 'rails_helper'

RSpec.feature 'Rca edition', type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1, name: '8D') }
  let(:rca) { FactoryBot.create(:rca, methodology_id: methodology.id) }
  # let(:perimeter) { FactoryBot.create(:perimeter) }

  before :each do
    # I18n.locale = 'en'
    # default_url_options[:locale] = I18n.locale
    user.usersession[:active_step_tool] = methodology.steps.first.step_tools.first.id
    user.save!
    sign_in user

    rca
  end

  scenario 'User edit and update rca' do
    visit '/rcas'
    # save_and_open_page
    find("a[href='#{edit_rca_path(rca)}']").click

    fill_in 'Title', with: 'Mon premier Rca'
    # select '8D', from: 'Methodology'
    click_button 'Save RCA', match: :first

    expect(page).to have_text('Rca was successfully updated.')
    expect(page).to have_current_path(edit_rca_path(rca))
  end

  scenario 'User delete rca', js: true do
    rca2 = FactoryBot.create(:rca, title: 'Mon deuxième Rca', methodology_id: methodology.id)
    visit '/rcas'
    expect(page).to have_css('tr', count: 4)

    page.accept_alert 'Are you sure ?' do
      find("a[href='#{rca_path(rca2)}'][data-method='delete']").click
    end

    expect(page).to have_text('Rca was successfully destroyed.')
    expect(page).to have_current_path(rcas_path)
    expect(page).to have_css('tr', count: 3)
  end
end
