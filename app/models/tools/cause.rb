require_dependency 'tools'

module Tools
  class Cause < ApplicationRecord
    # has_ancestry
    include ToolModelConcern
    include AncestryConcern

    include ToolMatrixable

    enum status: { in_progress: 0, validated: 1, invalidated: 2 }, _prefix: true
    # enum level: { '0': 0, '-': -1, '--': -2, '+': 1, '++': 2 }, _prefix: true
    enum level: { '-': -1, '+': 1, '++': 2 }, _prefix: true
    # belongs_to :rca
    validates :name, presence: true
    validate :parent_from_same_rca

    before_save :name_valid_html

    # Liens vers les tools (tree & ishi) existent uniquement pour les Head Causes
    has_many :cause_links, dependent: :destroy # , foreign_key: :cause_id, class_name: 'Tools::CauseLink'
    # has_many :cause_tools, through: :cause_links

    # vérifier si on garde dependent destroy ou si on crée une fonction qui déconnecte l'action de la cause
    has_many :tools_tasks, foreign_key: :tools_cause_id, class_name: 'Tools::Task', dependent: :destroy

    # Lien vers les branches d'ishikawas
    has_many :tools_cause_classes, class_name: 'Tools::CauseClass', foreign_key: :tools_cause_id,
                                   dependent: :destroy, inverse_of: :tools_cause
    # inverse_of: :tools_cause => pour que le create fonctionne avec les nested attributes, sinon tool_cause_id est nul
    has_many :tools_ishikawas, class_name: 'Tools::Ishikawa', through: :tools_cause_classes
    has_many :labels_cause_types, class_name: 'Labels::CauseType', through: :tools_cause_classes

    accepts_nested_attributes_for :tools_cause_classes

    # options = { rca: rca, tool_reference: tool_reference }
    def self.create_default!(options, _step_tool)
      create!(options.except!(:tool_num).merge!(name: options[:rca].long_title, status: :validated))
    end

    def self.search_default(rca, tool_name, step_tool)
      return nil unless rca.send(tool_name) && step_tool

      return rca.send(tool_name).where(tool_reference: step_tool.tool_reference, ancestry: nil).first
    end

    # def tool_num
    #   1
    # end

    def name_with_level
      level.nil? ? name : "(#{level}) #{name}"
    end

    # Ishikawa => peut-être à mettre dans des services ?

    # return cause type id if cause is classed for this ishikawa
    def ishi_cause_type_id(ishikawa)
      tcc = tools_cause_classes&.where(tools_ishikawa: ishikawa)&.first
      return tcc&.labels_cause_type ? tcc.labels_cause_type_id : 0
    end

    def ishi_cause_type(ishikawa)
      tcc = tools_cause_classes&.where(tools_ishikawa: ishikawa)&.first
      return tcc ? tcc.labels_cause_type : nil
    end

    def ishi_cause_class(ishikawa)
      tools_cause_classes&.where(tools_ishikawa: ishikawa)&.first
    end

    def add_ishi_cause_type(ishikawa, ctype_id)
      if ctype_id == 0
        del_ishi_cause_type(ishikawa)
      else
        tcc = ishi_cause_class(ishikawa)

        if tcc
          tcc.labels_cause_type_id = ctype_id
          # del_ishi_cause_type(ishikawa) unless tcc.save
          begin
            tcc.save
          rescue StandardError
            del_ishi_cause_type(ishikawa)
          end
        else
          tcc = Tools::CauseClass.create!(tools_cause: self, tools_ishikawa: ishikawa, labels_cause_type_id: ctype_id)
        end
      end
      return tcc
    end

    def del_ishi_cause_type(ishikawa)
      tcc = ishi_cause_class(ishikawa)

      # tcc&.destroy
      tcc.labels_cause_type = nil
      tcc.save!
    end

    def update_cause_status_from_tasks
      tasks_v = tools_tasks.where(task_detail_type: 'Tools::TaskValidation')
      return if tasks_v.empty?

      new_status = 'invalidated'
      tools_tasks.where(task_detail_type: 'Tools::TaskValidation').each do |task_v|
        next unless task_v.task_detail

        case task_v.task_detail.status
        when 'in_progress'
          new_status = new_status == 'validated' ? 'validated' : 'in_progress'
        when 'validated'
          new_status = 'validated'
        when 'invalidated'
          new_status = new_status == 'invalidated' ? 'invalidated' : new_status
          # else
          #   puts 'PROBLEME DE TYPE DE STATUS ===============================>'.yellow
        end
      end
      update(status: new_status) unless new_status == status
    end

    def init_ishikawas_list
      ishikawas, ishikawa_names = rca.init_ishikawas_list_ref(tool_reference)

      return ishikawas, ishikawa_names
    end

    # A PRIORI INULTILES => A VOIR...

    # def init_ishikawas_cause_classes
    #   ishikawas, _ishikawa_names = rca.init_ishikawas_list
    #   ishikawas&.each do |ishikawa|
    #     init_ishikawa_cause_class(ishikawa)
    #   end
    # end

    # def init_ishikawa_cause_class(ishikawa)
    #   add_ishi_cause_type(ishikawa, nil)
    # end

    private

    def name_valid_html
      # puts "NAME".yellow
      # puts name.red
      name.gsub!(/([<>])/, ' \1 ')
      name.gsub!(/( +)/, ' ')
      # binding.pry
      # puts name.blue
    end

    def parent_from_same_rca
      return unless parent && rca && parent.rca != rca

      errors.add(:parent_id, 'Parent cause is not assigned to same rca as current cause.')
    end
  end
end
