require 'rails_helper'

RSpec.describe '/tools/check_efficacities', type: :request do
  let(:rca) { FactoryBot.create(:rca, methodology: methodology) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:person) { FactoryBot.create(:person) }
  let(:user) { FactoryBot.create(:user, person_id: person.id) }
  let(:tools_cause) { FactoryBot.create(:cause, rca: rca) }

  before :each do
    # methodology.create_methodology
    @task = FactoryBot.create(:task, rca_id: rca.id)
    @task.create_efficacity
  end

  context 'From logged user' do
    before :each do
      user.set_session(:active_step_tool, methodology.steps.first.step_tools.first.id)
      user.save!

      sign_in user
    end

    describe 'GET /index' do
      context 'without step tool defined' do
        it 'renders a redirection response' do
          user.set_session(:active_step_tool, nil)
          get '/tools/check_efficacity'
          # redirection car rca non défini
          expect(response).to have_http_status(:redirect)
          expect(response).to redirect_to(rcas_path)
        end
      end

      context 'with rca and step_tool defined' do
        it 'renders a successful response' do
          get('/tools/check_efficacity', params: {rca: rca.id})
          expect(response).to be_successful
        end
      end
    end

    describe 'GET /edit_efficacity' do
      it 'renders a successful response' do
        get tools_check_efficacity_edit_efficacity_path, params: {id: @task.id}, xhr: true
        expect(response).to be_successful
      end
    end
  end
end
