json.extract! step_tool, :id, :name, :step_id, :tool_id
json.url step_tool_url(step_tool, format: :json)
