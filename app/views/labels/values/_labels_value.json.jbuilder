json.extract! labels_value, :id, :labels_criterium_id, :value, :title_fr, :title_en, :text_fr, :text_en, :created_at, :updated_at
json.url labels_value_url(labels_value, format: :json)
