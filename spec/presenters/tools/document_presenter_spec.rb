require 'rails_helper'

RSpec.describe Tools::DocumentPresenter, type: 'presenter' do
  subject(:document_presenter) { Tools::DocumentPresenter.new(document) }
  let(:view) { ActionController::Base.new.view_context }
  let(:user) { FactoryBot.create(:user) }
  let(:document) { FactoryBot.create(:tools_document, rca_id: rca.id, tool_reference: 1, file: doc_ok) }
  let(:rca) { FactoryBot.create(:rca) }
  let(:doc_ok) do
    ActiveStorage::Blob.create_and_upload!(
      io: File.open(file_ok, 'rb'),
      filename: 'not_image.pdf',
      content_type: 'file/pdf'
    ).signed_id
  end
  let(:file_ok) { Rails.root.join('spec', 'support', 'assets', 'images', 'not_image.pdf') }

  describe '#show_document_step_role' do
    context 'with inexistant step' do
      it 'returns the Role Step text : Other' do
        result = document_presenter.show_document_step_role
        expect(result).to eq('Other')
      end
    end

    context 'with existant step' do
      before :each do
        Meth::StepRole.create!(id: 10, role_name: 'steprole1', name: 'StepRole1')
      end
      it 'returns the Role Step text : with existant step' do
        document.tool_reference = 10

        result = document_presenter.show_document_step_role
        expect(result).to eq('StepRole1')
      end
    end
  end
end
