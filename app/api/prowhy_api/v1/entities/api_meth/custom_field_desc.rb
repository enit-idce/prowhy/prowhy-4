module ProwhyApi
  module V1
    module Entities
      module ApiMeth
        class CustomFieldDesc < Grape::Entity
          expose :id, unless: { with_id: 'false'}
          expose :internal_name
          expose :name
          expose :custom_field_type, using: Entities::ApiMeth::CustomFieldType, merge: true
        end
      end
    end
  end
end
