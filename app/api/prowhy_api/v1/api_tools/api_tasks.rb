require 'doorkeeper/grape/helpers'
require 'devise'

module ProwhyApi
  module V1
    module ApiTools
      class ApiTasks < Grape::API
        # helpers Helpers::ApiHelpers

        # before do
        #   doorkeeper_authorize! unless user_signed_in?
        # end

        desc "Tools task (actions) requests\n" \
             " - tasks list for one Rca\n" \
             " - create / update one task\n"
        resource :tasks do
          desc 'Return all Tools Tasks linked to Rca #rca_id.'
          params do
            requires :rca_id, type: Integer, desc: 'Rca ID.'
            optional :causes, type: Boolean, desc: 'True if you need cause information.'
            optional :reschedules, type: Boolean, desc: 'True if you need reschedules information.'
          end
          get do
            # 1. On test si le Rca existe => raise RecordNotFound if not.
            Rca.find(params[:rca_id])

            # 2. On test si le Rca existe dans la liste des Rca autorisés (policy_scope) => raise NotAuthorized if not.
            begin
              @rca = policy_scope(Rca).find(params[:rca_id])
            rescue ActiveRecord::RecordNotFound
              raise Pundit::NotAuthorizedError, 'not authorized to show this Rca.'
            end
            # authorize @rca, :show?

            @tasks = @rca.tools_tasks.where(ancestry: nil)

            present @tasks, with: Entities::ApiTools::Task,
                            with_id: true,
                            with_child: true,
                            with_cause: (params[:causes] == true || params[:causes] == 'true' ? true : false),
                            with_reschedules: (params[:reschedules] == true || params[:reschedules] == 'true' ? true : false)
          end

          desc 'Return all Tools Tasks accessible by connected user.'
          params do
            optional :options, type: Hash do
              optional :reponsible_names, type: Boolean, desc: 'True : names (default) / False : structures.', default: true
              optional :causes, type: Boolean, desc: 'True if you need cause information.', default: false
              optional :efficacity, type: Boolean, desc: 'True if you need efficacity information.', default: true
              optional :reschedules, type: Boolean, desc: 'True if you need reschedules information.'
            end
          end
          get :all do
            # Filtrage des rcas autorisés (policy_scope)
            list_ids_ok = policy_scope(Rca).uniq.pluck(:id)

            @tasks = Tools::Task.where(rca_id: list_ids_ok).includes([:responsible_action, :responsible_rca])

            present @tasks, with: Entities::ApiTools::Task,
                            with_id: true,
                            with_parent_id: true,
                            with_child: false,
                            with_reschedules: (params[:options][:reschedules] == true || params[:options][:reschedules] == 'true' ? true : false),
                            with_cause: (params[:options][:causes] == true || params[:options][:causes] == 'true' ? true : false),
                            reponsible_names: (params[:options][:reponsible_names] == true || params[:options][:reponsible_names] == 'true' ? true : false),
                            with_efficacity: (params[:options][:efficacity] == true || params[:options][:efficacity] == 'true' ? true : false)
          end

          desc 'Return all Tools Tasks linked to Rcas list.'
          params do
            requires :rca_ids, type: String, desc: 'Rca IDs. Exemple : [1, 2, 10] OR 1, 10, 12'
            optional :causes, type: Boolean, desc: 'True if you need cause information.'
          end
          get :list do
            rca_ids = params[:rca_ids].gsub('[', '')
            rca_ids.gsub!(']', '')
            rca_ids = rca_ids.split(',')

            # @rcas = Rca.where(id: rca_ids)
            # Filtrage des rcas autorisés (policy_scope)
            list_ids_ok = policy_scope(Rca).where(id: rca_ids).uniq.pluck(:id)

            @tasks = Tools::Task.where(rca_id: list_ids_ok, ancestry: nil)

            present @tasks, with: Entities::ApiTools::Task,
                            with_id: true,
                            with_child: true,
                            with_cause: (params[:causes] == true || params[:causes] == 'true' ? true : false)
          end

          desc 'Return Tools Task :id'
          params do
            requires :id, type: Integer, desc: 'Task ID.'
            optional :children, type: Boolean, desc: 'True if you need children tasks information.'
            optional :causes, type: Boolean, desc: 'True if you need cause information.'
            optional :reschedules, type: Boolean, desc: 'True if you need reschedules information.'
          end
          route_param :id do
            get do
              @task = Tools::Task.find(params[:id])

              begin
                policy_scope(Rca).find(@task.rca_id)
              rescue ActiveRecord::RecordNotFound
                raise Pundit::NotAuthorizedError, 'not authorized to show this task.'
              end

              authorize @task, :show?

              present @task, with: Entities::ApiTools::Task,
                             with_id: true,
                             with_child: (params[:children] == true || params[:children] == 'true' ? true : false),
                             with_cause: (params[:causes] == true || params[:causes] == 'true' ? true : false),
                             with_reschedules: (params[:reschedules] == true || params[:reschedules] == 'true' ? true : false)
            end
          end

          # desc 'Create a Tools Task.'
          # params do
          #   requires :tools_task, type: Hash, documentation: {param_type: 'body'} do
          #     requires :name, type: String, desc: 'Task title.'
          #     requires :rca_id, type: Integer, desc: 'Rca ID.'
          #     requires :task_detail_type, type: String, desc: "Task type : Tools::TaskEmergency\n" \
          #                                                     "Tools::TaskPreventive\n" \
          #                                                     "Tools::TaskValidation\n" \
          #                                                     "Tools::TaskCorrective\n" \
          #                                                     "Tools::TaskContainment\n" \
          #                                                     "Tools::TaskImprovement\n"
          #     optional :parent_id, type: Integer, desc: 'If task is a sub-task : Task Parent ID.'
          #     optional :tool_reference, type: Integer, default: 1, desc: 'Task tool reference (default=1).'
          #     optional :tool_num, type: Integer, default: 1, desc: 'Task tool num (default=1).'
          #     optional :tools_cause_id, type: Integer, desc: 'Cause ID. If empty, attach Task to default first cause of RCA.'
          #     optional :priority, type: Integer, default: 0, desc: 'Task priority : low: 0, medium: 1, high: 2'
          #     optional :responsible_action_id, type: Integer, desc: 'Responsible action (person ID). Default : current user.'
          #     optional :responsible_rca_id, type: Integer, desc: 'Responsible action (person ID). Default : current RCA responsible.'
          #   end
          # end
          # post do
          #   unless params[:tools_task][:parent_id]
          #     rca = Rca.find(params[:tools_task][:rca_id])
          #     step_tool = Meth::StepTool.new(tool_reference: params[:tools_task][:tool_reference] || 1)
          #     task_parent = rca.find_or_create_tool('tools_tasks', 'Tools::Task', step_tool)
          #     params[:tools_task][:parent_id] = task_parent.id
          #   end

          #   @tools_task = Tools::Task.new(params[:tools_task])

          #   authorize @tools_task, :create?

          #   @tools_task.save!

          #   present @tools_task, with: Entities::ApiTools::Task, with_id: true
          # end
        end
      end
    end
  end
end
