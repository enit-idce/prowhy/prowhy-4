require_dependency 'meth'

module Meth
  class CustomFieldDesc < ApplicationRecord
    include MethConcern

    translates :name, fallback: :any

    belongs_to :custom_field_type
    has_many :tool_custom_fields, -> { includes(step_tool: :step) }
    has_many :list_contents, -> { order(pos: :asc) }, dependent: :destroy
    # has_many :list_contents, dependent: :destroy do
    #   def order_pos
    #     order(pos: :asc)
    #   end

    #   def order_alpha
    #     order("name_#{I18n.locale} ASC")
    #   end
    # end
    has_many :tree_contents, dependent: :destroy

    validates :name, presence: true
    # validates :name, uniqueness: { case_sensitive: true }

    validates :internal_name, presence: true
    validates :internal_name, uniqueness: { case_sensitive: true }

    # scope :tree_free, -> { includes(:custom_field_type).where(meth_custom_field_types: {field_internal: 'tree'}) }

    # Used for preload values for lists(select) and taxonomies(tree) custom fields in Rca home page
    scope :custom_preload_list, -> { joins(:custom_field_type).where(show_list: true, meth_custom_field_types: {field_internal: ['select', 'tree']}) }
    scope :custom_preload_all, -> { joins(:custom_field_type).where(meth_custom_field_types: {field_internal: ['select', 'tree']}) }

    # Filtered
    belongs_to :ref_field, class_name: 'CustomFieldDesc', optional: true
    has_many :dependant_fields, class_name: 'CustomFieldDesc', foreign_key: :ref_field_id

    def self.custom_preload_list_values
      list = all.custom_preload_list

      list_fields = list.map { |cfd| cfd.internal_name.intern }
      list_values = list.map { |cfd| "#{cfd.internal_name}_value".intern }

      list_ret = []

      list_values.zip(list_fields) do |lvalue, lfield|
        list_ret << lvalue if CustomField.method_defined?(lvalue) && CustomField.method_defined?(lfield)
      end
      # binding.pry

      list_ret
    end

    def self.custom_preload_all_values
      list = all.custom_preload_all

      list_fields = list.map { |cfd| cfd.internal_name.intern }
      list_values = list.map { |cfd| "#{cfd.internal_name}_value".intern }

      list_ret = []

      list_values.zip(list_fields) do |lvalue, lfield|
        list_ret << lvalue if CustomField.method_defined?(lvalue) && CustomField.method_defined?(lfield)
      end
      # binding.pry

      list_ret
    end

    def update_with_ref_values(cf_desc_params, ref_values_params)
      return update(cf_desc_params) unless ref_values_params

      assign_attributes(cf_desc_params)
      self.ref_show_values = ref_values_params.map(&:to_i)
      ref_show_values&.delete(0)
      self.ref_show_values = nil if ref_show_values && ref_show_values.empty?
      # binding.pry

      return save!
    rescue StandardError => e
      errors.add(:base, e.message)
      return false
    end

    def reference_collection
      return nil unless ref_field&.custom_field_type&.field_internal == 'select'

      return ref_field.list_contents&.map { |lc| [lc.id, lc.name] }
    end

    # # Test pour API
    # def meth_custom_field_type_api
    #   # binding.pry
    #   if %w[bigint integer].include?(custom_field_type.field_type)
    #     Integer
    #   elsif custom_field_type.field_type == 'date'
    #     Date
    #   else
    #     String
    #   end
    # end

    # def meth_custom_field_desc_api
    #   return "Description du champs #{name}."
    # end
  end
end
