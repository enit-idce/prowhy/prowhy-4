# binding.pry
require 'rails_helper'

shared_examples_for ToolMatrixable, type: 'model' do
  subject(:obj) do
    obj = if defined?(described_obj)
            described_obj
          else
            described_class.new
          end
    obj.save(validate: false)
    obj
  end

  before :each do
    Tools::MatrixStat.create!(element: described_obj, matrix_status: 0)
  end

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'has many dec_mat_links' do
        assc = described_class.reflect_on_association(:dec_mat_links)
        expect(assc.macro).to eq :has_many
      end

      it 'has many decision_mats' do
        assc = described_class.reflect_on_association(:decision_mats)
        expect(assc.macro).to eq :has_many
      end
    end

    # describe 'Validations' do
    #   it 'is valid with matrix_status in_progress, validated, or invalidated' do
    #     described_obj.matrix_status = 'in_progress'
    #     expect(described_obj).to be_valid

    #     described_obj.matrix_status = 'validated'
    #     expect(described_obj).to be_valid

    #     described_obj.matrix_status = 'invalidated'
    #     expect(described_obj).to be_valid
    #   end

    #   it 'is not valid with invalid status' do
    #     expect { described_obj.matrix_status = 'toto' }.to raise_error(ArgumentError)
    #   end
    # end
  end

  it 'is matrixable' do
    expect(described_obj.matrixable?).to eq true
  end

  describe 'returns matrix status from matrix_stat' do
    let(:decision_mat) { Tools::DecisionMat.create!(rca: rca, tool_reference: 1, name: 'Matrix 1', entry_class: 'Tools::Task') }
    it 'returns matrix status' do
      decision_mat.add_entry(described_obj)

      expect(described_obj.matrix_status_in_progress?).to be true

      described_obj.matrix_status_invalidated!
      expect(described_obj.matrix_status_invalidated?).to be true

      described_obj.matrix_status_validated!
      expect(described_obj.matrix_status_validated?).to be true
    end
  end

  describe 'matrix_status_editable' do
    let(:decision_mat) { Tools::DecisionMat.create!(rca: rca, tool_reference: 1, name: 'Matrix 1', entry_class: 'Tools::Task') }
    it 'returns true when object is not in matrix' do
      decision_mat
      expect(described_obj.matrix_status_editable?).to be true
    end
    it 'returns false when object is in matrix' do
      decision_mat
      decision_mat.add_entry(described_obj)
      expect(described_obj.matrix_status_editable?).to be false
    end
  end

  describe 'calculates entry matrix status' do
    let(:decision_mat) { Tools::DecisionMat.create!(rca: rca, tool_reference: 1, name: 'Matrix 1', entry_class: 'Tools::Task') }
    let(:decision_mat2) { Tools::DecisionMat.create!(rca: rca, tool_reference: 1, name: 'Matrix 2', entry_class: 'Tools::Task') }
    let(:criterium) { Labels::Criterium.create(name: 'Efficacity', threshold: 1) }

    it 'calculates matrix status with one matrix' do
      decision_mat.add_criterium(criterium)
      decision_mat.add_entry(described_obj)

      described_obj.matrix_status_invalidated!
      described_obj.matrix_status_calculate
      # binding.pry
      expect(described_obj.matrix_status).to eq 'in_progress'
    end

    it 'calculates matrix status with two matrix' do
      decision_mat.add_criterium(criterium)
      decision_mat.add_entry(described_obj)
      decision_mat2.add_criterium(criterium)
      decision_mat2.add_entry(described_obj)

      decision_mat.reload
      decision_mat2.reload

      # with one valid & one in progress
      decision_mat.dec_mat_links.first.matrix_status_validated!
      decision_mat2.dec_mat_links.first.matrix_status_in_progress!

      # binding.pry

      described_obj.matrix_status_in_progress!
      described_obj.reload.matrix_status_calculate
      expect(described_obj.matrix_status).to eq 'validated'

      # with one valid & one invalid
      decision_mat.dec_mat_links.first.matrix_status_validated!
      decision_mat2.dec_mat_links.first.matrix_status_invalidated!

      described_obj.matrix_status_in_progress!
      described_obj.reload.matrix_status_calculate
      expect(described_obj.matrix_status).to eq 'validated'

      # with one invalid & one in progress
      decision_mat.dec_mat_links.first.matrix_status_invalidated!
      decision_mat2.dec_mat_links.first.matrix_status_in_progress!

      described_obj.matrix_status_in_progress!
      described_obj.reload.matrix_status_calculate
      expect(described_obj.matrix_status).to eq 'in_progress'

      # with two invalid
      decision_mat.dec_mat_links.first.matrix_status_invalidated!
      decision_mat2.dec_mat_links.first.matrix_status_invalidated!

      described_obj.matrix_status_in_progress!
      described_obj.reload.matrix_status_calculate
      expect(described_obj.matrix_status).to eq 'invalidated'
    end
  end
end

describe Tools::Cause do
  it_behaves_like ToolMatrixable do
    subject(:described_obj) { FactoryBot.create('cause', name: 'Cause1', rca: rca) }

    let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
    let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
    # let(:step_tool) { Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1) }
  end
end

describe Tools::Task do
  it_behaves_like ToolMatrixable do
    subject(:described_obj) { FactoryBot.create('task', name: 'Task1', rca: rca) }

    let(:methodology) { Meth::Methodology.create!(name: 'Method 1') }
    let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }
    # let(:step_tool) do
    #   Meth::StepTool.new(step: Meth::Step.new, tool: Meth::Tool.new, tool_reference: 1, tool_num: 1,
    #                      tool_options: {task_detail_type: 'Corrective'})
    # end
  end
end
