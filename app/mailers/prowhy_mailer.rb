class ProwhyMailer < ConfigMailer # InfoconnectMailer
  # default from: 'Prowhy-4 <no-reply@enit.fr>'
  helper :application

  # Send welcome email to user
  def welcome_email
    @user = params[:user]
    @url = rcas_url
    puts "Send a welcome email to : #{@user.email}".yellow
    # binding.pry
    mail(to: @user.email, subject: 'Welcome to Prowhy-4')
  end

  # Send RCA link email to user
  def rca_email
    @user = params[:user]
    @rca = params[:rca]
    @url = edit_rca_url(@rca)

    puts "Send a rca email to : #{@user.email}".yellow
    mail(to: @user.email, subject: "Prowhy-4 - rca #{@rca.reference} - #{@rca.title}")
  end

  # Send Task email to user
  def task_email
    # binding.pry
    @task = params[:task]
    @person = params[:person]
    @message = params[:message] || ''
    email = params[:email]
    mail(to: email, subject: "Prowhy-4 - task #{@task.name}")
  end

  # Send Email to Rca responsible (when an action is closed)
  def task_closed_email
    # binding.pry
    @task = params[:task]
    @message = params[:message] || ''
    emails = params[:emails]
    mail(to: emails, subject: "Prowhy-4 - task #{@task.name}")
  end

  # Send Reporting message email to a list of persons
  def report_email
    @content_message = params[:options][:content]
    @rca = params[:rca]
    @url = params[:options][:rca] ? edit_rca_url(id: @rca.id) : nil
    emails = params[:emails]

    attachments['rca.pdf'] = params[:pdf] if params[:pdf] # WickedPdf.new.pdf_from_string(params[:pdf])
    mail(to: emails, subject: "Prowhy-4 - RCA #{@rca.reference} - #{@rca.title}")
  end

  # =================================== Test pour exporter les PDF des sous-rca (tool rca-lists)
  # def report_email
  #   @content_message = params[:options][:content]
  #   @rca = params[:rca]
  #   @url = params[:options][:rca] ? edit_rca_url(id: @rca.id) : nil
  #   emails = params[:emails]

  #   # attachments['rca.pdf'] = params[:pdf] if params[:pdf] # WickedPdf.new.pdf_from_string(params[:pdf])

  #   params[:pdf_all]&.each_with_index do |pdf, indx|
  #     attachments["rca_#{indx}.pdf"] = pdf
  #   end

  #   # attachments['rca.pdf'] = params[:pdf_all][0]

  #   # binding.pry
  #   mail(to: emails, subject: "Prowhy-4 - RCA #{@rca.reference} - #{@rca.title}")
  # end
end
