module Tools
  class Reschedule < ApplicationRecord
    belongs_to :tools_task, class_name: 'Tools::Task'
  end
end
