require 'rails_helper'

RSpec.feature 'Methodology creation', type: :feature do
  before :all do
    load "#{Rails.root}/db/seed_tests.rb"
    methodology = Meth::Methodology.rca_methodologies.where(name_fr: 'Méthode 8D').first

    user = User.first
    user.usersession[:active_step_tool] = methodology.steps.first.step_tools.first.id
    user.save!
    sign_in user

    @rca = Rca.create(title: 'Mon premier Rca', methodology: methodology)
  end

  after :all do
    load "#{Rails.root}/db/seed_empty.rb"
  end

  scenario 'User creates a methodology' do
    visit '/meth/methodologies'
    # save_and_open_page

    # click_link 'New Methodology'
    find("a[href='#{new_meth_methodology_path}']").click

    fill_in 'Name (EN)', with: 'Nouvelle methodo'
    click_button 'Create Methodology'

    expect(page).to have_text('Methodology was successfully created.')
    expect(page).to have_current_path(edit_meth_methodology_path(Meth::Methodology.last))
  end
end
