class CreateAdminPerimeterRcas < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_perimeter_rcas do |t|
      t.references :perimeter, null: false, foreign_key: { to_table: :admin_perimeters }
      t.references :rca, null: false, foreign_key: true

      t.timestamps
    end
  end
end
