class ProfilAuthorize < ApplicationRecord
  belongs_to :profil
  belongs_to :meth_step_role, class_name: 'Meth::StepRole'

  def self.policy_class
    ProfilPolicy
  end
end
