require 'rails_helper'

RSpec.describe Tools::Reschedule, type: :model do
  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to tools_task' do
        assc = Tools::Reschedule.reflect_on_association(:tools_task)
        expect(assc.macro).to eq :belongs_to
      end
    end
  end
end
