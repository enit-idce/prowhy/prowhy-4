class DatasAddHomeInfosToAdminConfig < ActiveRecord::Migration[6.0]
  class MigrationAdminConfig < ApplicationRecord
    self.table_name = :admin_admin_configs
  end

  def up
    say 'Initialize home_options.'
    admin_config = MigrationAdminConfig.first

    return unless admin_config

    unless admin_config.home_options
      admin_config.home_options = { show_desc: true,
                                    show_methodo: true,
                                    show_adv_etape: true,
                                    show_adv_role: true,
                                    show_adv_duration: true,
                                    show_custom_order: [] }
    end
    admin_config.save!
  end

  def down
    say 'Do not destroy home_options.'
    # raise ActiveRecord::IrreversibleMigration
  end
end
