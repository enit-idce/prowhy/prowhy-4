module Tools
  module Tasks
    class TaskPlanGetTasks
      attr_accessor :rca, :current_user, :rcas_ids

      def initialize(rca, rcas_ids, current_user)
        @rca = rca
        @rcas_ids = rcas_ids
        @current_user = current_user
      end

      def init_tasks(plan_options, task_params)
        # Order by cause
        return [] if plan_options[:causes]

        # binding.pry

        # PA d'un RCA
        if rca && plan_options[:sub_type_plan] != 'rca-list'
          return Tools::TaskQuery.relation.rca_tasks_by_matrix_status(rca.id, task_params) if plan_options[:blocks_column] == 'matrix_status'

          return Tools::TaskQuery.relation.rca_tasks_by_column(rca.id, task_params, plan_options[:blocks_column])
        end

        # Si Action Plan type user
        if plan_options[:type_plan] == 'user'
          return Tools::TaskQuery.relation.tasks_by_user_order_rca(task_params, current_user, plan_options[:blocks_column]) if plan_options[:rcas]

          return Tools::TaskQuery.relation.tasks_by_user(task_params, current_user, plan_options[:blocks_column])
        end

        # Si Action Plan type global (inclu action plan avec sub_type_plan rca-list')
        if plan_options[:type_plan] == 'global'
          # Si le PA type rca-list, on supprime le rappel à la ref car la ref représente la liste de rca, et non la ref des actions.
          task_params.delete(:tool_reference) if plan_options[:sub_type_plan] == 'rca-list'

          return Tools::TaskQuery.relation.tasks_global_order_rca(rcas_ids, task_params, plan_options[:blocks_column]) if plan_options[:rcas]

          return Tools::TaskQuery.relation.tasks_global(rcas_ids, task_params, plan_options[:blocks_column])
        end

        # # PA d'un RCA
        # if rca && plan_options[:sub_type_plan] != 'rca-list'
        #   return Tools::TaskQuery.relation.rca_tasks_by_matrix_status(rca.id, task_params) if plan_options[:blocks_column] == 'matrix_status'

        #   return Tools::TaskQuery.relation.rca_tasks_by_column(rca.id, task_params, plan_options[:blocks_column])
        # end

        return [] # ActiveRecord::NullRelation
      end
    end
  end
end
