require 'rails_helper'

RSpec.describe Labels::Value, type: :model do
  subject(:value) do
    FactoryBot.create(:labels_value, criterium_id: criterium.id)
  end
  let(:criterium) { FactoryBot.create(:labels_criterium) }

  context 'describes model associations and validations' do
    describe 'Associations' do
      it 'belongs to criterium' do
        assc = Labels::Value.reflect_on_association(:criterium)
        expect(assc.macro).to eq :belongs_to
      end
    end

    describe 'Validations' do
      it 'is not valid without title' do
        value.title = ''
        expect(value).to_not be_valid
      end

      it 'is valid with title' do
        value.title = 'Middle'
        expect(value).to be_valid
      end
    end
  end

  describe 'name' do
    it 'show value, title and text' do
      value.title = 'Middle'
      expect(value.name).to eq "#{value.value} => Middle"

      value.text = 'Cost middle'
      expect(value.name).to eq "#{value.value} => Middle : Cost middle"
    end
  end
end
