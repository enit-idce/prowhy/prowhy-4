require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the Labels::CheckListElemsHelper. For example:
#
# describe Labels::CheckListElemsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe Labels::CheckListElemsHelper, type: :helper do
  let(:check_list_elem) { FactoryBot.create(:labels_check_list_elem) }
  let(:methodology) { FactoryBot.create(:methodology, :with_step_1) }
  let(:rca) { Rca.create!(title: 'Rca 1', methodology: methodology) }

  let(:task1) { FactoryBot.create(:task, id: 1, rca: rca, name: 'Task 1', advancement: 0) }
  let(:task2) { FactoryBot.create(:task, id: 2, rca: rca, name: 'Task 1', advancement: 0) }
  let(:task3) { FactoryBot.create(:task, id: 3, rca: rca, name: 'Task 1', advancement: 0) }

  describe 'elem_tasks_nb' do
    it 'show a cross icon if no tasks are defined' do
      task1.save!
      task2.save!
      task3.save!
      helper_content = helper.elem_tasks_nb(check_list_elem, rca)
      expect(helper_content).to include('times') # fa_icon
    end

    it 'show nb tasks linked to cehcklist elem if tasks are defined' do
      task1.update(check_list_elem: check_list_elem)
      task2.update(check_list_elem: nil)
      task3.update(check_list_elem: check_list_elem)
      # task1.save!
      # task2.save!
      # task3.save!
      helper_content = helper.elem_tasks_nb(check_list_elem.reload, rca)
      expect(helper_content).to include('2')
    end
  end
end
