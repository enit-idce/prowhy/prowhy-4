json.extract! custom_field_desc, :id, :custom_field_type_id, :internal_name, :name, :created_at, :updated_at
json.url meth_custom_field_desc_url(custom_field_desc, format: :json)
