module ToolEfficacitable
  extend ActiveSupport::Concern

  included do
    has_one :efficacity, class_name: 'Tools::Efficacity', as: :element, dependent: :destroy
    accepts_nested_attributes_for :efficacity
  end

  # Class Methods
  module ClassMethods
    def list_efficacity_attributes
      return [:id, :date_due_check, :date_done_check, :check_status, :element_type, :element_id]
    end
  end

  # Instance Methods
end
