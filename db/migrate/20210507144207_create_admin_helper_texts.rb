class CreateAdminHelperTexts < ActiveRecord::Migration[6.0]
  def change
    create_table :admin_helper_texts do |t|
      t.string :tool_class
      t.string :reference
      t.text :help_text_fr
      t.text :help_text_en

      t.timestamps
    end
  end
end
