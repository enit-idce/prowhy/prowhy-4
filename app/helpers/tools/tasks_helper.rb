module Tools
  module TasksHelper
    include Pundit::Authorization
    include IconsHelper

    def task_link_next_advancement(task)
      case task.advancement
      when 0
        'Valid step 1 : action planified'
      when 1
        'Valid step 2 : work in progress'
      when 2
        'Valid step 3 : work done'
      when 3
        'Valid step 4 : action closed'
      else
        ''
      end
    end

    def task_link_prev_advancement(task)
      case task.advancement
      when 1
        'Back to step 0'
      when 2
        'Back to step 1'
      when 3
        'Back to step 2'
      when 4
        'Back to step 3'
      else
        ''
      end
    end

    def task_advance_helper_form(task)
      @tools_task = task

      ret = task_advance_helper(task, animation: '{ "duration": 700 }')

      # if () # current_user is authorized for update_advancement
      ret += hidden_field 'tools_task', :advancement

      return ret if task.new_record?

      # binding.pry
      if policy(task).update_advance?
        if task.children.empty?
          if task.advancement > 0
            ret += link_to task_link_prev_advancement(task),
                           tools_task_update_advance_path(id: task.id, tools_task: {advancement: task.advancement-1}),
                           method: :patch, remote: true,
                           class: 'btn btn-advancement'
          end
          if task.advancement < 4
            ret += link_to task_link_next_advancement(task),
                           tools_task_update_advance_path(id: task.id, tools_task: {advancement: task.advancement+1}),
                           method: :patch, remote: true,
                           class: 'btn btn-advancement'
          end
        else
          ret += "<span class='info-advancement'>[open sub actions to set advancement]</span>"
        end
      end
      # end

      return ret
    end

    # def task_advance_helper_links(task)
    #   ret = ''
    #   if task.children.empty?
    #     if task.advancement > 0
    #       ret += link_to "back #{task.advancement-1}",
    #                      tools_task_update_advance_path(id: task.id, tools_task: {advancement: task.advancement-1}),
    #                      method: :patch,
    #                      class: 'btn btn-advancement btn-advancement-small'
    #     end
    #     if task.advancement < 4
    #       ret += link_to "=> #{task.advancement+1}",
    #                      tools_task_update_advance_path(id: task.id, tools_task: {advancement: task.advancement+1}),
    #                      method: :patch,
    #                      class: 'btn btn-advancement btn-advancement-small'
    #     end
    #   end
    #   return ret
    # end

    def task_advance_helper(task, options = {})
      return export_task_adv_icon(task.advancement) if @export_pdf

      # ret = ''
      # ret += fa_icon('times-circle', class: 'task-adv-1')
      # ret += fa_icon('play-circle', class: 'task-adv-2')
      # ret += fa_icon('circle-notch', class: 'task-adv-3')
      # ret += fa_icon('check-circle', class: 'task-adv-4')

      circle_options = {}
      circle_options[:size] = options[:size] || 30
      circle_options[:thickness] = options[:thickness] || 11
      circle_options[:fill] = options[:fill] || '{"color": "#27AD0B"}' # orange
      circle_options[:animation] = options[:animation] || false

      ret = "<div class='jquery-circle-p' style='display: inline-block;' \
                                          data-value='#{task.advancement / 4.0}' \
                                          data-size='#{circle_options[:size]}' \
                                          data-thickness= '#{circle_options[:thickness]}' \
                                          data-fill='#{circle_options[:fill]}' \
                                          data-animation='#{circle_options[:animation]}' ></div>"

      return ret
    end

    # Date completion
    def task_date_completion_helper_form(task)
      ret = ''
      task.date_completion = Date.today if task.task_finished? && !task.date_completion
      # ret = date_field :tools_task, :date_completion, class: 'form-control mx-1 is-valid date optional' if task.task_finished?
      ret = date_field :tools_task, :date_completion, class: 'form-control is-valid date optional' if task.task_finished?

      return ret
    end

    # Date due
    def task_date_due_helper(task)
      ret = if task.task_late?
              "<span style='color:orangered;'>#{task.date_due}</span>".html_safe
            else
              task.date_due
            end
      return ret
    end

    # Reschedule (icon access to function)
    def reschedule_button(task)
      return '' unless (task.id && task.date_due)

      link_to fa_icon_reschedule, tools_tasks_reschedule_path(id: task.id), remote: true, title: t('tools.tasks.form_modal.reschedule')
    end

    # Status Matrix
    def task_status_matrix(task)
      # return task.dec_mat_links.empty? ? '-' : icon_status(task, 'matrix_status') # task.matrix_status
      return task.matrix_stat ? icon_status(task, 'matrix_status') : '-'
    end

    # Specific columns : TaskValidation
    def task_validation_status(task)
      return '-' unless task.task_detail_type == 'Tools::TaskValidation'

      return icon_status(task.task_detail, 'status')
    end

    # Specific columns : Efficacity
    def efficacity_status(elem)
      return '-' unless elem.efficacity

      return icon_status(elem.efficacity, 'check_status')
    end

    # def star_icon(val, percentage, css_class = '', css_styles = '')
    #   return fa_icon('star', type: (percentage < val ? :regular : :solid), class: css_class, style: css_styles)
    # end

    # def star_helper(element, model_name, col_name, values, options = {})
    #   return '-' unless element

    #   options[:css_class] ||= 'icon-status-validated'
    #   options[:css_styles] ||= 'font-size: 18px; width:16px !important;'

    #   col_value = element.send(col_name)

    #   ret = ''
    #   values.each do |val|
    #     ret += link_to star_icon(val, col_value, options[:css_class], options[:css_styles]),
    #                    public_send("#{model_name}_path", element, model_name => {col_name => val}), method: :patch, remote: true
    #   end

    #   return ret.html_safe
    # end

    def efficacity_percentage_edit(elem)
      return '-' unless elem&.efficacity&.check_status_validated?

      return pick_stars_helper(elem.efficacity, 'tools_efficacity', 'percentage', [20, 40, 60, 80, 100])
    end

    def efficacity_percentage_show(elem)
      return '-' unless elem&.efficacity&.check_status_validated?

      return show_stars_helper(elem.efficacity, 'tools_efficacity', 'percentage', [20, 40, 60, 80, 100])
    end

    def efficacity_date_due(elem)
      return '-' unless elem.efficacity

      eff = elem.efficacity
      ret = if eff.date_due_check && eff.date_due_check <= Date.today && !eff.date_done_check
              "<span style='color:orangered;'>#{eff.date_due_check}</span>".html_safe
            else
              eff.date_due_check
            end
      return ret
    end

    def efficacity_date_due_icon(elem)
      return '-' unless elem.efficacity

      eff = elem.efficacity
      return '' unless eff.date_due_check && eff.date_due_check <= Date.today && !eff.date_done_check

      return fa_icon_warning
    end

    def efficacity_date_done(elem)
      return '-' unless elem.efficacity

      return elem.efficacity.date_done_check
    end

    def icon_status(element, status)
      return export_status_icon(element.send(status)) if @export_pdf

      ret = if element.send(status) == 'validated'
              fa_icon_status_presenter('icon-status-validated')
            elsif element.send(status) == 'invalidated'
              fa_icon_status_presenter('icon-status-invalidated')
            else
              fa_icon_status_presenter('icon-status-inprogress') # status_in_progress
            end
      return ret
    end

    # Columns : return true or false if column (task) is visible or not int user's preferences (in @task_columns)
    def show_column(col_block, col)
      return @task_columns ? @task_columns[col_block.to_s]&.include?(col.to_s) : true
    end

    def show_column_style(col_block, col)
      return show_column(col_block, col) ? '' : 'display: none;'
    end

    def task_column_object_title(col, nb_columns)
      str = ''
      if show_column('object', col)
        nb_columns += 1
        str = "<th column-title='#{col}'>#{Tools::Task.human_attribute_name col.intern}</th>"
      end

      return str, nb_columns
    end

    def task_column_matrix_title(col, nb_columns)
      str = ''
      if show_column('matrix', col)
        nb_columns += 1
        str = "<th column-title='#{col}'>#{Tools::MatrixStat.human_attribute_name col.intern}</th>"
      end

      return str, nb_columns
    end

    def task_column_validation_title(col, nb_columns)
      str = ''
      if show_column('validation', col)
        nb_columns += 1
        str = "<th column-title='#{col}'>#{Tools::TaskValidation.human_attribute_name col.intern}</th>"
      end

      return str, nb_columns
    end

    def task_column_efficacity_title(col, nb_columns)
      str = ''
      if show_column('efficacity', col)
        nb_columns += 1
        str = "<th column-title='#{col}'>#{Tools::Efficacity.human_attribute_name col.intern}</th>"
      end

      return str, nb_columns
    end

    def task_type_show(ttype)
      t(ttype&.tableize&.singularize, scope: [:activerecord, :models])
    end

    def task_types_collection(ttypes = nil)
      (ttypes || Tools::Task::TASK_TYPES).map { |tdt| [t(tdt.tableize, scope: [:activerecord, :models]), tdt] }
    end
  end
end
